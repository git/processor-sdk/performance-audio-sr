#
#  Copyright (c) 2012-2015 Texas Instruments Incorporated - http://www.ti.com
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  *  Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#
#  *  Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#
#  *  Neither the name of Texas Instruments Incorporated nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
#  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
#  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
#  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

#
#  ======== products.mak ========
#

.PHONY: .show

ifneq ($(OS),Windows_NT)
DEPOT = /home/frank/ti
else
#DEPOT = c:/ti_prsdk_5.00.00.15
DEPOT = c:/ti
endif

#### PASDK installation directories ####
ifneq ($(OS),Windows_NT)
export PAROOT      ?= $(DEPOT)/processor_sdk_audio_1_03_00_00
else
export PAROOT      ?= T:
endif
PASDK_INSTALL_DIR       = $(PAROOT)/pasrc
PAF_INSTALL_DIR         = $(PASDK_INSTALL_DIR)/paf

#### Dependencies ####
BIOS_INSTALL_DIR       ?= $(DEPOT)/bios_6_52_00_12
XDC_INSTALL_DIR        ?= $(DEPOT)/xdctools_3_50_03_33_core
UIA_INSTALL_DIR        ?= $(DEPOT)/uia_2_21_02_07
IPC_INSTALL_DIR        ?= $(DEPOT)/ipc_3_47_01_00
PDK_INSTALL_DIR        ?= $(DEPOT)/pdk_k2g_1_0_10
EDMA3_INSTALL_DIR      ?= $(DEPOT)/edma3_lld_2_12_05_30C
XDAIS_INSTALL_DIR      ?= $(DEPOT)/xdais_7_24_00_04
DSPLIB_INSTALL_DIR     ?= $(DEPOT)/dsplib_c66x_3_4_0_0

#### Tool chains ####
gnu_targets_arm_A15F   ?= $(DEPOT)/gcc-arm-none-eabi-6-2017-q1-update
ti_targets_elf_C66     ?= $(DEPOT)/ti-cgt-c6000_8.2.2

# Use this goal to print your product variables.
.show:
	@echo "DEPOT                = $(DEPOT)"
	@echo "PAROOT               = $(PAROOT)"
	@echo "PASDK_INSTALL_DIR    = $(PASDK_INSTALL_DIR)"
	@echo "PAF_INSTALL_DIR      = $(PAF_INSTALL_DIR)"
	@echo "BIOS_INSTALL_DIR     = $(BIOS_INSTALL_DIR)"
	@echo "XDC_INSTALL_DIR      = $(XDC_INSTALL_DIR)"
	@echo "UIA_INSTALL_DIR      = $(UIA_INSTALL_DIR)"
	@echo "IPC_INSTALL_DIR      = $(IPC_INSTALL_DIR)"
	@echo "PDK_INSTALL_DIR      = $(PDK_INSTALL_DIR)"
	@echo "EDMA3_INSTALL_DIR    = $(EDMA3_INSTALL_DIR)"
	@echo "XDAIS_INSTALL_DIR    = $(XDAIS_INSTALL_DIR)"
	@echo "DSPLIB_INSTALL_DIR   = $(DSPLIB_INSTALL_DIR)"
	@echo "gnu_targets_arm_A15F = $(gnu_targets_arm_A15F)"
	@echo "ti_targets_elf_C66   = $(ti_targets_elf_C66)"
