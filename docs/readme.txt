ReadMe: 

This is the Performance Audio demo.

Description of the directory structure: 
   - prebuilt-binaries: pre-built C66x and CortexA15 binaries
   - src: source code and scripts/makefile to rebuild the demo
   - tools: windows based tools for run-time reconfiguration of the demo  
   - docs: documentation about audio I/O and framework which this demo is built on

For detailed instructions to run the demo and rebuild it from the source code, please refer to 
http://software-dl.ti.com/processor-sdk-rtos/esd/docs/latest/rtos/Examples_and_Demonstrations.html#performance-audio-demo

