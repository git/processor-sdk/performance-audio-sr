@echo off
:: *************************************************************************
::  FILE           : setup_env.bat
::  DESCRIPTION    :
::
::     Setup tools environment.
::
:: *************************************************************************

@echo Executing:  %~fn0


:: *************************************************************************
:: *** Specify install locations
:: *************************************************************************
:: Tools location
set TOOLS_DRIVE=C:
set TI_TOOLS_DIR=%TOOLS_DRIVE%\ti
set PA_TOOLS_DIR=%TOOLS_DRIVE%\PA_Tools

:: *************************************************************************
:: *** Specify tool versions
:: *************************************************************************
::
:: PRSDK component versions
::
:: XDC tools version for PA
set XDC_VERSION=3_50_03_33_core
:: SYSBIOS version for PA
set BIOS_VERSION=6_52_00_12
:: UIA version
set UIA_VERSION=2_21_02_07
:: IPC version
set IPC_VERSION=3_47_02_00
:: PDK version
set PDK_VERSION=1_0_10
:: EDMA3 LLD version
set EDMA3_VERSION=2_12_05_30C
:: XDAIS version
set XDAIS_VERSION=7_24_00_04
:: DSPLIB version
set DSPLIB_VERSION=3_4_0_0
:: CCS version
set CCS_VER=7
:: SED version
set SED_VER_DOT=4.2.1
:: Python version
set PYTHON_VER=27
:: Pkzip version
::set PKZIP_VER_DOT=9.20

:: Codegen tools
:: ARM CGT for PA
set ARM_CGT_VERSION=6-2017-q1-update
:: C6X CGT for PA
set C6X_CGT_VER_DOT=8.2.2

:: *************************************************************************
:: *** Specify install locations
:: *************************************************************************

::
:: PA dependency install locations
::
set CCS_INSTALL_DIR=%TI_TOOLS_DIR%\ccsv%CCS_VER%
set XDC_INSTALL_DIR=%TI_TOOLS_DIR%\xdctools_%XDC_VERSION%
set BIOS_INSTALL_DIR=%TI_TOOLS_DIR%\bios_%BIOS_VERSION%
set UIA_INSTALL_DIR=%TI_TOOLS_DIR%\uia_%UIA_VERSION%
set IPC_INSTALL_DIR=%TI_TOOLS_DIR%\ipc_%IPC_VERSION%
set PDK_INSTALL_DIR=%TI_TOOLS_DIR%\pdk_k2g_%PDK_VERSION%
set EDMA3_INSTALL_DIR=%TI_TOOLS_DIR%\edma3_lld_%EDMA3_VERSION%
set XDAIS_INSTALL_DIR=%TI_TOOLS_DIR%\xdais_%XDAIS_VERSION%
set SED_INSTALL_DIR=%PA_TOOLS_DIR%\GnuWin32
set PYTHON_INSTALL_DIR=%PA_TOOLS_DIR%\Python%PYTHON_VER%
set ZIP_INSTALL_DIR=%PA_TOOLS_DIR%
set CYGWIN_INSTALL_DIR=%TOOLS_DRIVE%\cygwin\bin

::
:: Codegen tools install locations
::
set ARM_CGT_INSTALL_DIR=%TI_TOOLS_DIR%\gcc-arm-none-eabi-%ARM_CGT_VERSION%
set C6X_CGT_INSTALL_DIR=%TI_TOOLS_DIR%\ti-cgt-c6000_%C6X_CGT_VER_DOT%

::
:: PA install location
::
if "%PAROOT%" NEQ "" (
    set PAROOT=%PAROOT_DIR%
) else (
    set PAROOT=%~dp0..
)

:: 
:: PAF install locations
:: 
set CG_TOOLS_a15=%ARM_CGT_INSTALL_DIR%
set CG_TOOLS_c66x=%C6X_CGT_INSTALL_DIR%
set BIOSROOT=%TI_TOOLS_DIR%\bios_%BIOS_VERSION%
set XDCROOT=%TI_TOOLS_DIR%\xdctools_%XDC_VERSION%
set IPCROOT=%TI_TOOLS_DIR%\ipc_%IPC_VERSION%
set PDKROOT=%TI_TOOLS_DIR%\pdk_k2g_%PDK_VERSION%
set EDMA3LLDROOT=%TI_TOOLS_DIR%\edma3_lld_%EDMA3_VERSION%
set XDAISROOT=%TI_TOOLS_DIR%\xdais_%XDAIS_VERSION%
set DSPLIBROOT=%TI_TOOLS_DIR%\dsplib_c66x_%DSPLIB_VERSION%
set ROOTDIR=%PAROOT%\pasrc\paf

rem @echo #######################################################################
rem @echo ##  All Required Tools Installed
rem @echo #######################################################################
rem @echo.

:: *************************************************************************
:: ** Set the PATH
:: *************************************************************************
for %%i in (git.exe) do set GIT_PATH=%TOOLS_DRIVE%%%~sp$PATH:i
set PATH=%SystemRoot%;%SystemRoot%\system32;%SystemRoot%\system32\Wbem
set PATH=%XDC_INSTALL_DIR%;%PATH%
set PATH=%XDC_INSTALL_DIR%\jre\bin;%PATH%
set PATH=%PATH%;%CCS_INSTALL_DIR%\eclipse
set PATH=%PATH%;%SED_INSTALL_DIR%\bin
set PATH=%PATH%;%PYTHON_INSTALL_DIR%;%PYTHON_INSTALL_DIR%\Scripts
set PATH=%PATH%;%GIT_PATH%
set PATH=%PATH%;%ZIP_INSTALL_DIR%\7-Zip
set GIT_PATH=


:: *************************************************************************
:: ** Create XDC environment variables
:: *************************************************************************
set xdc=%XDC_INSTALL_DIR%\xdc.exe $*
::set XDCPATH=%CCS_INSTALL_DIR%/ccsv%CCS_VER%/packages
::set XDCPATH=%XDCPATH%;%XDC_INSTALL_DIR%/packages
set XDCPATH=%XDC_INSTALL_DIR%/packages


:: *************************************************************************
:: ** Clean Up
:: *************************************************************************
set XDC_VERSION=
set XDC_VERSION_DH=
set XDC_VERSION_DTSX=
set BIOS_VERSION=
set BIOS_VERSION_DH=
set BIOS_VERSION_DTSX=
set IPC_VERSION=
set XDAIS_VERSION=
set CCS_VER=
set CCS_VER_CGT=
set SED_VER_DOT=
set PYTHON_VER=
::set PKZIP_VER_DOT=
set C6X_CGT_VER_DOT=
set ARM_CGT_VERSION=
set C6X_CGT_VER_DOT_PARMA=


:: *************************************************************************
:: ** Show the build environment
:: *************************************************************************
@echo.
@echo #######################################################################
@echo ##  Build Environment Variables (Start)
@echo #######################################################################
@set
@echo #######################################################################
@echo ##  Build Environment Variables (Stop)
@echo #######################################################################
@echo.


:end
