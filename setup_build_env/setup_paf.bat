@echo off
:: *************************************************************************
::  FILE           : setup_paf.bat
::  DESCRIPTION    :
::
::     Setup build environment for PAF libraries
::
:: *************************************************************************

@echo Executing:  %~fn0

set PAF_ROOT_DIR=%PAROOT%\pasrc\paf

rem
rem Temporary  
rem Copy Windows based make files to "working" make files.
rem This is a workaround until PAF library make files are improved to detect OS.
rem
pushd %PAF_ROOT_DIR%\pa\build
copy /y rules_a15_windows.mk rules_a15.mk 
copy /y rules_windows.mk rules.mk 
copy /y target_windows.mk target.mk 
copy /y tools_windows.mk tools.mk 
popd

:end
