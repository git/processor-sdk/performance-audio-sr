/*
 * k2g_cfg.c
 *
 * Platform specific EDMA3 hardware related information like number of transfer
 * controllers, various interrupt ids etc. It is used while interrupts
 * enabling / disabling. It needs to be ported for different SoCs.
 *
 * Copyright (C) 2012-2017 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <ti/sdo/edma3/rm/edma3_rm.h>

/* Number of EDMA3 controllers present in the system */
#define NUM_EDMA3_INSTANCES			2u
const unsigned int numEdma3Instances = NUM_EDMA3_INSTANCES;

#define BUILD_K2G_DSP

#ifdef BUILD_K2G_DSP
/* Number of DSPs present in the system */
#define NUM_DSPS					1u
#define NUM_CORES                   NUM_DSPS

#define CGEM_REG_START                  (0x01800000)

extern cregister volatile unsigned int DNUM;

#define MAP_LOCAL_TO_GLOBAL_ADDR(addr) ((1<<28)|(DNUM<<24)|(((unsigned int)addr)&0x00ffffff))
#else
#define NUM_A15S					1u
#define NUM_CORES                   NUM_A15S
#endif

/* Determine the processor id by reading DNUM register. */
unsigned short determineProcId()
	{
#ifdef BUILD_K2G_DSP
  	volatile unsigned int *addr;
  	unsigned int core_no;

    /* Identify the core number */
    addr = (unsigned int *)(CGEM_REG_START+0x40000);
    core_no = ((*addr) & 0x000F0000)>>16;

	return core_no;
#else
    return 0;
#endif
	}

signed char*  getGlobalAddr(signed char* addr)
{
#ifdef BUILD_K2G_DSP
    if (((unsigned int)addr & (unsigned int)0xFF000000) != 0)
    {
        return (addr); /* The address is already a global address */
    }

    return((signed char*)(MAP_LOCAL_TO_GLOBAL_ADDR(addr)));
#else
    return (addr);
#endif
}
/** Whether global configuration required for EDMA3 or not.
 * This configuration should be done only once for the EDMA3 hardware by
 * any one of the masters (i.e. DSPs).
 * It can be changed depending on the use-case.
 */
unsigned int gblCfgReqdArray [NUM_CORES] = {
#ifdef BUILD_K2G_DSP
									0,	/* DSP#0 is Master, will do the global init */
#else
									0,	/* ARM#0 is Master, will do the global init */
#endif
									};

unsigned short isGblConfigRequired(unsigned int dspNum)
	{
	return gblCfgReqdArray[dspNum];
	}

/* Semaphore handles */
EDMA3_OS_Sem_Handle semHandle[NUM_EDMA3_INSTANCES] = {NULL,NULL};


/* Variable which will be used internally for referring number of Event Queues. */
unsigned int numEdma3EvtQue[NUM_EDMA3_INSTANCES] = {2u, 2u};

/* Variable which will be used internally for referring number of TCs. */
unsigned int numEdma3Tc[NUM_EDMA3_INSTANCES] = {2u, 2u};

/**
 * Variable which will be used internally for referring transfer completion
 * interrupt. Completion interrupts for all the shadow regions and all the
 * EDMA3 controllers are captured since it is a multi-DSP platform.
 */
unsigned int ccXferCompInt[NUM_EDMA3_INSTANCES][EDMA3_MAX_REGIONS] = {
#ifdef BUILD_K2G_DSP
													{
													136u, 137u, 138u, 139u,
													140u, 141u, 142u, 143u,
													},
													{
													144u, 145u, 146u, 147u,
													148u, 149u, 150u, 151u,
													},
#else
    {
        (200u + 32u), (201u + 32u), (202u + 32u), (203u + 32u), /* EDMACC_0_TC_0_INT - EDMACC_0_TC_3_INT */
        (204u + 32u), (205u + 32u), (206u + 32u), (207u + 32u), /* EDMACC_0_TC_4_INT - EDMACC_0_TC_7_INT */
    },
    {
        (208u + 32u), (209u + 32u), (210u + 32u), (211u + 32u), /* EDMACC_1_TC_0_INT - EDMACC_1_TC_3_INT */
        (212u + 32u), (213u + 32u), (214u + 32u), (215u + 32u), /* EDMACC_1_TC_4_INT - EDMACC_1_TC_7_INT */
    },
#endif
												};

/**
 * Variable which will be used internally for referring channel controller's
 * error interrupt.
 */
unsigned int ccErrorInt[NUM_EDMA3_INSTANCES] = {0x99, 0x9c}; /* CIC0 EDMACC_n_ERRINT */

/**
 * Variable which will be used internally for referring transfer controllers'
 * error interrupts.
 */
unsigned int tcErrorInt[NUM_EDMA3_INSTANCES][EDMA3_MAX_TC] =    {
													{
													0xA0,0xA1, 0u, 0u,
													0u, 0u, 0u, 0u,
													}, /* CIC0 EDMACC_0_TC_n_ERRINT */
													{
													0xA4,0xA5, 0u, 0u,
													0u, 0u, 0u, 0u,
													}, /* CIC0 EDMACC_1_TC_n_ERRINT */
												};


/* End of File */
