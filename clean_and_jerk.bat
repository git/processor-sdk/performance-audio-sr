@echo off

:: %1: Build type: DH or DTSX
@echo Executing:  %~fn0 %1

:: *************************************************************************
:: *** Check command line input
:: *************************************************************************
if "%1"=="" goto use_err1
goto cont1

:use_err1
:: USAGE ERROR
echo ERROR: Supplied options are invalid.
echo .
echo . Usage: %0 ^<buildType^>
echo .
echo .     buildType        : DH or DTSX
echo .
goto end


:cont1
set PATH_ORG=%path%
set path=c:\cygwin\bin;%path%

git clean -fdx --exclude=clean_and_jerk.bat
git submodule foreach git clean -fdx

git ls-files -m | xargs git checkout --
git submodule foreach "git ls-files -m | xargs git checkout --"

git branch -vv > repos_before_build.txt && git submodule foreach git branch -vv >> repos_before_build.txt

call build_all.bat %1 SuperRepo Debug > build_all_log.txt 2>&1

git branch -vv > repos_after_build.txt && git submodule foreach git branch -vv >> repos_after_build.txt

set path=%PATH_ORG%


:end
