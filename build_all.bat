@echo off

:: *************************************************************************
::  FILE           : build_all.bat
::  DESCRIPTION    :
::
::      Builds all for Dolby Harmony or DTS:X project for DA10x. This includes:
::          (1) DSP PDK LLD libraries: UART, SPI, I2C
::          (2) ARM/DSP PAF libraries
::          (3) ARM/DSP algorithm components libraries, either of:
::              - Dolby Harmony component libraries: BMDA, CAR, DDP, MAT-THD, OAR
::              - DTS:X component libraries: DTS:X Decoder, PARMA
::          (4) DSP PDK platform library
::          (5) ARM/DSP applications
::      Creates .zip packages for Dolby Harmony component libraries.
::
:: *************************************************************************

@echo Executing:  %~fn0 %1 %2 %3

:: *************************************************************************
:: *** Check command line input
:: *************************************************************************
if "%1"=="" goto use_err1
if "%2"=="" goto use_err1
if "%3"=="" goto use_err1
goto cont1


:use_err1
:: USAGE ERROR
echo ERROR: Supplied options are invalid.
echo .
echo . Usage: %0 ^<buildType^> SuperRepo ^<buildProfile^>
echo . -OR-
echo . Usage: %0 ^<buildType^> ^<buildTag^> ^<buildProfile^>
echo .
echo .     buildType        : DH, DTSX, NOIP, ALLIP, ALL
echo .     SuperRepo        : Use Git super-repository to track submodules for build targets
echo .     buildTag         : Use Git tag to track submodules for build targets 
echo .     buildProfile     : Debug, Release, or SDF
echo .
goto end


:cont1
:: Set PASDK root directory
set PAROOT_DIR=%~dp0
if "%PAROOT_DIR:~-1%"=="\" set PAROOT_DIR=%PAROOT_DIR:~0,-1%

if "%3" NEQ "SDF" goto install_tools
:: Set PASDK target and version
set PASDK_TARGET_PLATFORM=k2g
set PASDK_VERSION=01_03_00_00
:: Set FD package version
set FD_PKG_VERSION=01_03_00_00

:: Set release directory
set RELEASE_DIR=%PAROOT_DIR%\release
:: Set FD package directory
set FD_PKG_DIR=%RELEASE_DIR%\fd_package
:: Set OS package directory
set OS_PKG_DIR=%RELEASE_DIR%\os_package

:: Set DH-IP package directory
set DHIP_PKG_DIR=%RELEASE_DIR%\dhip_package
:: Set DTSX-IP package directory
set DTSXIP_PKG_DIR=%RELEASE_DIR%\dtsxip_package
:: Set AAC package directory
set AACIP_PKG_DIR=%RELEASE_DIR%\k2gx_a15_aac
:: Set AAC testapp package directory
set AACIPTEST_PKG_DIR=%RELEASE_DIR%\k2gx_a15_aac_test

if exist %RELEASE_DIR%\nul ( rmdir /S / Q %RELEASE_DIR% )


:install_tools
:: *************************************************************************
:: *** Install tools
:: *************************************************************************
:: FL: tool installation not yet supported
::@call scripts\install_tools.bat
@call scripts\setup_env.bat

:: *************************************************************************
:: *** Install code
:: *************************************************************************
@call scripts\install_code.bat %2

:: *************************************************************************
:: *** Build
:: *************************************************************************
:: Build DSP PDK LLD libraries
rem @pushd scripts
rem @cmd /c build_pdk_libs.bat
rem @popd

:: Build ARM/DSP PAF libraries
@pushd scripts
@call build_paf_libs.bat
@popd

:: Build ARM/DSP component libraries
:build_dh_libs
set TRUE=
if "%1"=="DH" set TRUE=1
if "%1"=="ALLIP" set TRUE=1
if "%1"=="ALL" set TRUE=1
if "%1"=="ALLDH" set TRUE=1
if defined TRUE (
    rem Build Dolby Harmony component libraries
    @pushd scripts
	@call build_dh_libs_prsdk.bat
    @popd

    if "%3"=="SDF" (
        rem Build DH-IP packages
        @pushd scripts
        @call build_dhip_pkgs %DHIP_PKG_DIR%
        @popd
        
        rem Install DH-IP packages
        @pushd scripts
        @call install_dhip_pkgs %DHIP_PKG_DIR%
        @popd
    )
)

:build_dtsx_libs
set TRUE=
if "%1"=="DTSX" set TRUE=1
if "%1"=="ALLIP" set TRUE=1
if "%1"=="ALL" set TRUE=1
if "%1"=="ALLDTSX" set TRUE=1
if defined TRUE (
    rem Build DTS:X component libraries
    @pushd scripts
    @call build_dtsx_libs_prsdk.bat
    @call build_parma_libs_prsdk.bat
    @popd

    if "%3"=="SDF" (
        rem Build DTSX-IP packages
        @pushd scripts
        @call build_dtsxip_pkgs %DTSXIP_PKG_DIR%
        @popd
        
        rem Install DTSX-IP packages
        @pushd scripts
        @call install_dtsxip_pkgs %DTSXIP_PKG_DIR%
        @popd
    )
)

:build_ccs_projects
:: Build CCS projects
@pushd scripts
@call build_ccs_projects.bat %1 %3
@popd

if "%3" NEQ "SDF" goto cleanupenv
:: Build FD package
@pushd scripts
@call build_fd_pkg.bat %FD_PKG_DIR%
@popd

:: Build OS package
@pushd scripts
@call build_os_pkg.bat %OS_PKG_DIR%
@popd

:: Build AAC testapp and library
@pushd scripts
@call setup_env.bat
@call build_aac.bat testapp
@call build_aacip_pkg.bat aactestapp %AACIPTEST_PKG_DIR%
@call setup_env.bat
@call build_aac.bat lib
@call build_aacip_pkg.bat aacdec %AACIP_PKG_DIR%
@popd

:build_installer_package
@pushd installer_builder
python generate_installer.py pasdk
@popd
set TRUE=
if "%1"=="DTSX" set TRUE=1
if "%1"=="ALLIP" set TRUE=1
if "%1"=="COMBINEDIP" set TRUE=1
if "%1"=="ALLDTSX" set TRUE=1
if defined TRUE (
	@pushd installer_builder
	python generate_installer.py dts_withwrappersrc
	@popd
)

set TRUE=
if "%1"=="DH" set TRUE=1
if "%1"=="ALLIP" set TRUE=1
if "%1"=="ALL" set TRUE=1
if "%1"=="ALLDH" set TRUE=1
if defined TRUE (
	@pushd installer_builder
	python generate_installer.py dolby
	python generate_installer.py dolby_withwrappersrc
	@popd
)

@pushd installer_builder
python generate_installer.py aac
@popd

:cleanupenv
set PAROOT_DIR=
set PASDK_TARGET_PLATFORM=
set PASDK_VERSION=
set FD_PKG_VERSION=
set RELEASE_DIR=
set FD_PKG_DIR=
set OS_PKG_DIR=
set DHIP_PKG_DIR=
set DTSXIP_PKG_DIR=
set TRUE=

:end
