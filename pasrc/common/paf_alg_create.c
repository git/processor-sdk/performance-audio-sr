
/*
Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// Algorithm Creation
//

#include <stdlib.h>
#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/Memory.h>
#include "alg.h"

//#include "pafhjt.h"
#include "paf_alg.h"
#include "paf_alg_priv.h"

//extern far int SDRAM;
//far int *mem_mallocseg_ptr=&SDRAM;
//#define MALLOC(size) MEM_alloc(*mem_mallocseg_ptr,(size), 8)
//#define FREE(addr,size) MEM_free(*mem_mallocseg_ptr,(addr),(size))
extern HeapMem_Handle heapMemDdr3;
#define HEAPMALLOC (IHeap_Handle)heapMemDdr3

/*
 * The PAF_ALG_alloc_ function collects the Common Memory Data Requirements
 * from all the Algorithms. The function returns 0 on success. If the 
 * call to malloc() fails then it returns PAF_ALGERR_MEMTAB_MALLOC. If a 
 * particular Algorithm requests 0 or less memTabs then the function returns 
 * an error value of PAF_ALGERR_MEMTAB_COUNT. 
 */

Int 
PAF_ALG_alloc_ (
    const PAF_ALG_AllocInit *pInit, 
    Int sizeofInit, 
    IALG_MemRec common[])
{
    IALG_MemRec *memTab;
    Int commonSize[PAF_IALG_COMMON_MEMN+1];
    Int commonCount[PAF_IALG_COMMON_MEMN+1];
    Int i,n;
    Error_Block eb;

    // Initialize error block
    Error_init(&eb); 

    for ( ; pInit && pInit->ialg_fxns; 
        pInit=(const PAF_ALG_AllocInit *)((char *)pInit+sizeofInit)) {
#ifdef _TMS320C6X
#pragma UNROLL(1)
#endif
        for(i=PAF_IALG_COMMON_MEM0;i<=PAF_IALG_COMMON_MEMN;++i) {
            commonSize[i]=0;
            commonCount[i]=0;
        }
        n = pInit->ialg_fxns->algNumAlloc != NULL ? pInit->ialg_fxns->algNumAlloc() 
                                                  : IALG_DEFMEMRECS;
        //if((memTab = (IALG_MemRec *)MALLOC(n * sizeof(IALG_MemRec)))) {
        if ((memTab = (IALG_MemRec *)Memory_alloc(HEAPMALLOC, n*sizeof(IALG_MemRec), 8, &eb)))
        {
            n=pInit->ialg_fxns->algAlloc(pInit->ialg_prms,
                                         (IALG_Fxns ** )&(pInit->ialg_fxns),memTab);
            if(n<=0)
                return PAF_ALGERR_MEMTAB_COUNT;

            for(i=0;i<n;++i){
                if((memTab[i].attrs == PAF_IALG_COMMON_MEM0) || 
                   (memTab[i].attrs >= PAF_IALG_COMMON_MEM1 && 
                    memTab[i].attrs <= PAF_IALG_COMMON_MEMN)) {
                    commonSize[memTab[i].attrs] += memTab[i].size + 
		        (commonCount[memTab[i].attrs] ? memTab[i].alignment : 0);
                    if(!commonCount[memTab[i].attrs] && 
                        memTab[i].alignment > common[memTab[i].attrs].alignment)
		        common[memTab[i].attrs].alignment = memTab[i].alignment;
                    commonCount[memTab[i].attrs]++;
                    if (common[memTab[i].attrs].space == PAF_IALG_NONE)
                        /* creating common memory section */
                        common[memTab[i].attrs].space = memTab[i].space;
                    else if (common[memTab[i].attrs].space == memTab[i].space)
                        /* existing common memory section: space okay */
                        ;
                    else {
                        /* existing common memory section: space conflict */
                        /* Greater Memory Space Rule (tends to external)  */
                        if (common[memTab[i].attrs].space < memTab[i].space)
                            common[memTab[i].attrs].space = memTab[i].space;
                        else 
                            ;
                    }
                }
            }
            //FREE(memTab,(n * sizeof(IALG_MemRec)));
            Memory_free(HEAPMALLOC,memTab,(n*sizeof(IALG_MemRec)));
            if(common){
                for(i=PAF_IALG_COMMON_MEM0;i<=PAF_IALG_COMMON_MEMN;++i) {
                    if(commonSize[i] > common[i].size) {
                        common[i].size = commonSize[i];
                        common[i].attrs = (IALG_MemAttrs )i;
                    }
                }
            }
        }
        else
            return PAF_ALGERR_MEMTAB_MALLOC;
    }
    return 0;
}

/*
 * The PAF_ALG_create_ is derived from standard XDAIS ALG_create function defined in the 
 * file alg_create.c version "XDAS 2.2.1 12-07-01". 
 * The PAF_ALG_create_ is designed to handle the Performance Audio Specific "Common"
 * memory and also to share the scratch memory accross the Performance Audio
 * System.
 * The PAF_ALG_create_ function returns the Handle to the Algorithm if the Algorithm is 
 * created successfully otherwise it returns NULL.
 */

ALG_Handle PAF_ALG_create_ (
    const IALG_Fxns *fxns,
    IALG_Handle p,
    const IALG_Params *params,
    const IALG_MemRec common[],
    PAF_IALG_Config *pafConfig)
{
    IALG_MemRec *memTab;
    Int n;
    ALG_Handle alg;
    IALG_Fxns *fxnsPtr;
    Error_Block eb;

    // Initialize error block
    Error_init(&eb); 

    if(fxns != NULL){
        n = fxns->algNumAlloc != NULL ? fxns->algNumAlloc() : IALG_DEFMEMRECS;
        //if((memTab = (IALG_MemRec *)MALLOC(n * sizeof (IALG_MemRec)))) 
        if((memTab = (IALG_MemRec *)Memory_alloc(HEAPMALLOC, n*sizeof (IALG_MemRec), 8, &eb)))
        {
            n = fxns->algAlloc(params, &fxnsPtr, memTab);
            if(n<=0)
                return NULL;
            if(!PAF_ALG_allocMemory(memTab,n,common,pafConfig)){
                alg = (IALG_Handle)memTab[0].base;
                alg->fxns = (IALG_Fxns * )fxns;
                if(fxns->algInit(alg, memTab, p, params) == IALG_EOK){
                    //FREE(memTab,(n * sizeof(IALG_MemRec)));
                    Memory_free(HEAPMALLOC,memTab,(n * sizeof(IALG_MemRec)));
                    return alg;
                }
                fxns->algFree(alg, memTab);
                PAF_ALG_freeMemory(memTab, n);
            }
            //FREE(memTab,(n * sizeof(IALG_MemRec)));
            Memory_free(HEAPMALLOC,memTab,(n * sizeof(IALG_MemRec)));
        }
    }
    return (NULL);
}

/*
 * The PAF_ALG_delete is derived from standard XDAIS ALG_delete function defined in the 
 * file alg_create.c version "XDAS 2.2.1 12-07-01". 
 * The PAF_ALG_create is designed to handle the Performance Audio Specific "Common"
 * memory and also scratch memory.
 */

/* DO NOT REMOVE THIS CODE_SECTION. --Kurt */
#ifdef _TMS320C6X
#pragma CODE_SECTION(PAF_ALG_delete,".text:_PAF_ALG_delete")
#endif

Void PAF_ALG_delete(ALG_Handle alg)
{
    IALG_MemRec *memTab;
    Int n;
    IALG_Fxns *fxns;
    Error_Block eb;

#if defined(_TMS320C6X) && !defined(__TI_EABI__)
    asm (" .clink");
#endif    

    // Initialize error block
    Error_init(&eb); 

    if (alg != NULL && alg->fxns != NULL) {
        fxns = alg->fxns;
        n = fxns->algNumAlloc != NULL ? fxns->algNumAlloc() : IALG_DEFMEMRECS;

        //if ((memTab = (IALG_MemRec *)MALLOC(n * sizeof (IALG_MemRec)))) 
        if ((memTab = (IALG_MemRec *)Memory_alloc(HEAPMALLOC, n*sizeof (IALG_MemRec), 8, &eb)))
        {
            memTab[0].base = alg;
            n = fxns->algFree(alg, memTab);
            PAF_ALG_freeMemory(memTab, n);
            //FREE(memTab,(n * sizeof(IALG_MemRec)));
            Memory_free(HEAPMALLOC,memTab,(n * sizeof(IALG_MemRec)));
        }
    }
}

struct PAF_ALG_Fxns PAF_ALG_fxns =
{
    PAF_ALG_alloc_,
    PAF_ALG_create_,
    PAF_ALG_init_,
    PAF_ALG_allocMemory_,
    PAF_ALG_mallocMemory_,
    PAF_ALG_memSpaceToHeapId_,    
    PAF_ALG_memSpaceToHeap_,
    PAF_ALG_setup_
};
