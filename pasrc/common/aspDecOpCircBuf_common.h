
/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _ASP_DECOP_CB_COMMON_H_
#define _ASP_DECOP_CB_COMMON_H_

#include <xdc/std.h>
#include <ti/ipc/GateMP.h>
#include "paftyp.h"
#include "pafdec.h"

//#include "dbgBenchmark.h" // PCM high-sampling rate + SRC + CAR benchmarking

//#define CB_RW_OP_CAP_PP 1// debug
#ifdef CB_RW_OP_CAP_PP
#define CB_OP_NONE        0
#define CB_OP_W           1     // ARM write
#define CB_OP_R           2     // DSP read
#define CB_OP_COUNT_MAX   15000
#endif

// CB control
#define ASP_DECODE_CB_GATE_NAME                 ( "AspDecOpCbGate" ) // name of GateMP used for circular buffer shared memory protection
#define ASP_DECODE_CB_GATE_REGION_ID            ( 0 )                // IPC shared region ID used for CB gate allocation   

// error return codes
#define ASP_DECOP_CB_SOK                        ( 0 )                                       // ok
#define ASP_DECOP_CB_CTL_INIT_INV_GATE          ( ASP_DECOP_CB_SOK-1 )                      // error: invalid gate handle
#define ASP_DECOP_CB_RESET_INV_SOURCE_SEL       ( ASP_DECOP_CB_CTL_INIT_INV_GATE - 1 )      // error: invalid source selection on init
#define ASP_DECOP_CB_ERR_START                  ( ASP_DECOP_CB_RESET_INV_SOURCE_SEL )       // start error return ID for master & slave error return definitions

#define ASP_DECOP_CB_MAX_NUM_AF                 ( 30 )      // decoder output circular buffer maximum number audio frames

#define ASP_DECOP_CB_MAX_NUM_PCM_CH             ( 16 )      // decoder output circular buffer maximum number audio PCM channels
#define ASP_DECOP_CB_MAX_NUM_PCM_FRAMES         ( 15 )      // decoder output circular buffer maximum number PCM frames //Qin - Increased to prevent cb overflow for ddp.
#define ASP_DECOP_CB_MAX_PCM_FRAME_LEN          ( 6*256 )   // decoder output circular buffer maximum PCM frame length

#define ASP_DECOP_CB_MAX_NUM_PCM_CH_AAC         ( 16 )      // decoder output circular buffer maximum number audio PCM channels for AAC
#define ASP_DECOP_CB_MAX_PCM_FRAME_LEN_48kAAC   ( 6*256 )   // decoder output circular buffer maximum PCM frame length at 48kHz sampling rate

#define ASP_DECOP_CB_MAX_NUM_PCM_CH_DDP         ( 16 )      // decoder output circular buffer maximum number audio PCM channels for DDP
#define ASP_DECOP_CB_MAX_PCM_FRAME_LEN_48kDDP   ( 6*256 )   // decoder output circular buffer maximum PCM frame length at 48kHz sampling rate

#define ASP_DECOP_CB_MAX_NUM_PCM_CH_MAT         ( 32 )      // decoder output circular buffer maximum number audio PCM channels for MAT
#define ASP_DECOP_CB_MAX_PCM_FRAME_LEN_48kMAT   ( 4*256 )   // decoder output circular buffer maximum PCM frame length at 48kHz sampling rate

#define ASP_DECOP_CB_MAX_NUM_PCM_CH_DTS        ( 32 )      // decoder output circular buffer maximum number audio PCM channels for MAT
#define ASP_DECOP_CB_MAX_PCM_FRAME_LEN_48kDTS  ( 16*256 )  // decoder output circular buffer maximum PCM frame length at 48kHz sampling rate

// (***) FL: !!! REVISIT!!! THD 192 kHz "fudge factor" for CB PCM buffer size.
// Using this factor in PCM buffer size definition, buffer large enough to execute 192 kHz w/o CB OVR/UND.
// Need to define CB size in different terms than existing macros.
// (***) FL: !!! REVISIT!!! Allocating memory for max # channels (e.g. 32 for THD).
// GROSS over allocation for THD 192 kHz, 6ch max.
#define THD_HSR_FUDGE_FACTOR                    ( 368640 + 240000 + 384000 )  
#define ASP_DECOP_CB_PCM_BUF_SZ                 ( ASP_DECOP_CB_MAX_NUM_PCM_CH * ASP_DECOP_CB_MAX_NUM_PCM_FRAMES * ASP_DECOP_CB_MAX_PCM_FRAME_LEN + THD_HSR_FUDGE_FACTOR)
// FL: unused
//#define ASP_DECOP_CB_PCM_BUF_SZ_32CH48kMAT      ( ASP_DECOP_CB_MAX_NUM_PCM_CH_MAT * ASP_DECOP_CB_MAX_NUM_PCM_FRAMES * ASP_DECOP_CB_MAX_PCM_FRAME_LEN_48kMAT )

#define ASP_DECOP_CB_MAX_NUM_AF_PCM             ( 4 )
#if (ASP_DECOP_CB_MAX_NUM_AF_PCM > ASP_DECOP_CB_MAX_NUM_AF)
    #error "CB Max AF PCM error"
#endif
#define ASP_DECOP_CB_INIT_WRTIDX_PCM            ( 0 )
#define ASP_DECOP_CB_INIT_RDIDX_PCM             ( 0 )
//#define ASP_DECOP_CB_INIT_LAG_PCM               ( 2 )
#define ASP_DECOP_CB_TARGET_ND_SAMPS_48kPCM     ( (4+1) * 256 ) // PCM target Nominal Delay in number of samples, fs=48 kHz

#define ASP_DECOP_CB_MAX_NUM_AF_AAC             ( 4 )
#define ASP_DECOP_CB_INIT_WRTIDX_AAC            ( 0 )
#define ASP_DECOP_CB_INIT_RDIDX_AAC             ( 0 )
#define ASP_DECOP_CB_TARGET_ND_SAMPS_AAC        ( (4+1) * 256 )

#define ASP_DECOP_CB_MAX_NUM_AF_DDP             ( 3 )
#if (ASP_DECOP_CB_MAX_NUM_AF_DDP > ASP_DECOP_CB_MAX_NUM_AF)
    #error "CB Max AF DDP error"
#endif
#define ASP_DECOP_CB_INIT_WRTIDX_DDP            ( 0 )
#define ASP_DECOP_CB_INIT_RDIDX_DDP             ( 0 )
//#define ASP_DECOP_CB_INIT_LAG_DDP               ( 1 )
#define ASP_DECOP_CB_TARGET_ND_SAMPS_DDP        ( (6+6) * 256 ) // DDP target Nominal Delay in number of samples

#define ASP_DECOP_CB_MAX_NUM_AF_THD             ( 30 ) //( 15 )
#if (ASP_DECOP_CB_MAX_NUM_AF_THD > ASP_DECOP_CB_MAX_NUM_AF)
    #error "CB Max AF THD error"
#endif
#define ASP_DECOP_CB_INIT_WRTIDX_THD            ( 0 )
#define ASP_DECOP_CB_INIT_RDIDX_THD             ( 0 )
//#define ASP_DECOP_CB_INIT_LAG_THD               ( 5 ) // 5*20ms = 100 ms, For the worst case of 100 ms
#define ASP_DECOP_CB_TARGET_ND_SAMPS_48kTHD     ( 3600 ) //( 3504 ) //( 2224 ) // THD target Nominal Delay in number of samples, fs=48 kHz
#define ASP_DECOP_CB_TARGET_ND_SAMPS_96kTHD     ( 2*ASP_DECOP_CB_TARGET_ND_SAMPS_48kTHD ) // THD target Nominal Delay in number of samples, fs=96 kHz
#define ASP_DECOP_CB_TARGET_ND_SAMPS_192kTHD    ( 4*ASP_DECOP_CB_TARGET_ND_SAMPS_48kTHD ) // THD target Nominal Delay in number of samples, fs=192 kHz
//#define DEF_SOURCE_SEL                          ( PAF_SOURCE_PCM )          // default source select
//#define DEF_DEC_OP_FRAME_LEN                    ( PAF_SYS_FRAMELENGTH )     // default decoder output frame length
//#define DEF_STR_FRAME_LEN                       ( PAF_SYS_FRAMELENGTH )     // default stream frame length

// DTS - update for DTS
#define ASP_DECOP_CB_MAX_NUM_AF_DTS         ( 14 )
#if (ASP_DECOP_CB_MAX_NUM_AF_DTS > ASP_DECOP_CB_MAX_NUM_AF)
    #error "CB Max AF DTS error"
#endif
//#define ASP_DECOP_CB_INIT_LAG_DTS           ( 2 )
#define ASP_DECOP_CB_INIT_WRTIDX_DTS        ( 7 )
#define ASP_DECOP_CB_INIT_RDIDX_DTS         ( 0 )

#define ASP_DECOP_CB_MAX_NUM_AF_DSD ( 10 )
#if (ASP_DECOP_CB_MAX_NUM_AF_DSD > ASP_DECOP_CB_MAX_NUM_AF)
    #error "CB Max AF DSD error"
#endif
#define ASP_DECOP_CB_INIT_WRTIDX_DSD ( 0 )
#define ASP_DECOP_CB_INIT_RDIDX_DSD ( 0 )
#define ASP_DECOP_CB_TARGET_ND_SAMPS_DSD ( (4+1) * 256 )


// Decoder output circular buffer status
typedef struct PAF_AST_DecOpCircBufStatus
{
    Int size;
    Int16 strFrameLen;                      // stream frame length (output transaction size)
} PAF_AST_DecOpCircBufStatus;

// Decoder output circular buffer
typedef struct PAF_AST_DecOpCircBuf 
{
    PAF_AudioFrame *afCb;                   // audio frame CB
    PAF_AudioData *pcmBuf;                  // PCM buffer, contains PCM data associated with audio frames
    UInt8 *metaBuf;                         // metadata buffer, contains metadata associated with audio frames
    Int8 sourceSel;                         // selected source
    Int8 primedFlag;                        // flag indicated whether CB is primed: FALSE - CB not primed, TRUE - CB primed
    Int16 targetNDSamps;                    // target CB Nominal Delay number of samples
    Int16 deltaSamps;                       // number of 256 (N) sample output frames to block reader
    Int8 afRdIdx;                           // audio frame CB read index
    Int8 afWrtIdx;                          // audio frame CB write index
    Int16 pcmRdIdx;                         // pcm buffer read index
    Int8 prvMdRdIdx;                        // private metadata read index
    Int8 numAfCb;                           // current number frames in CB
    Int8 maxNumAfCb;                        // maximum number of audio frames in CB
    Int32 numPcmSampsPerCh;                 // current number of PCM samples per channel in CB
    Int32 maxNumPcmSampsPerCh;              // max number of PCM samples per channel in CB
    Int16 decOpFrameLen;                    // selected decoder output frame length (input transaction size)
    Int16 strFrameLen;                      // stream frame length (output transaction size)
    Int8 writerActiveFlag;                  // flag indicates whether CB writer is active: FALSE - writer not active, TRUE - writer active
    Int8 readerActiveFlag;                  // flag indicates whether CB reader is active: FALSE - reader not active, TRUE - reader active
    Int8 drainFlag;                         // flag indicates whether reader should drain (empty) remaining frames in CB: FALSE - reader should not drain, TRUE - reader should drain
    Int32 readAfWriterInactiveCnt;          // count of read audio frames with inactive writer & not draining
    Int32 readAfNdCnt;                      // count of read audio frames for nominal delay 
    Int32 wrtAfReaderInactiveCnt;           // count of write audio frames with inactive reader
    Int32 wrtAfZeroSampsCnt;                // count of write audio frames with zero samples
    Int32 errAfUndCnt;                      // read error AF underflow count
    Int32 errAfOvrCnt;                      // write error AF overflow count
    Int32 errPcmUndCnt;                     // read error PCM underflow count
    Int32 errPcmOvrCnt;                     // write error PCM overflow count
    PAF_AudioData *pcmBufEnd;               // PCM buffer, contains PCM data associated with audio frames end
    UInt8 *metaBufEnd;                      // metadata buffer, contains metadata associated with audio frames end
    Int8 maxAFChanNum;                      // maximum number of audio data channels in audio frame
    Int16 maxAFSampCount;                   // maximum number of sample counts in audio frame
    PAF_AudioFrame lastAf;                  // used to store the last valid read out audio frame info to generate mute frame
    PAF_AST_DecOpCircBufStatus cbStatus;    // CB status    
#ifdef CB_RW_OP_CAP_PP // debug
    UInt32 *cb_samples_op;                  // should be in shared memory, preferred NON cache
    UInt8 *cb_op_owner;                     // should be in shared memory, preferred NON cache
    UInt8 *cb_afRdIdx;                      // should be in shared memory, preferred NON cache
    UInt8 *cb_afWrtIdx;                     // should be in shared memory, preferred NON cache
    UInt8 *cb_numAfCb;                      // should be in shared memory, preferred NON cache
    UInt32 cb_opCnt;                        // range (0 - CB_OP_COUNT_MAX)
#endif
} PAF_AST_DecOpCircBuf;

// Decoder output circular buffer statistics
typedef struct PAF_AST_DecOpCircBufStats
{
    Int32 readAfWriterInactiveCnt;  // count of read audio frames with inactive writer & not draining
    Int32 readAfNdCnt;              // count of read audio frames for nominal delay
    Int32 wrtAfReaderInactiveCnt;   // count of write audio frames with inactive reader
    Int32 wrtAfZeroSampsCnt;        // count of write audio frames with zero samples
    Int32 errAfUndCnt;              // read error AF underflow count
    Int32 errAfOvrCnt;              // write error AF overflow count
    Int32 errPcmUndCnt;             // read error PCM underflow count
    Int32 errPcmOvrCnt;             // write error PCM overflow count
} PAF_AST_DecOpCircBufStats;

// Decoder output circular buffer control
typedef struct PAF_AST_DecOpCircBufCtl
{
    GateMP_Handle gateHandle;           // circular buffer gate handle
    Int8 numDecOpCb;                    // number of circular buffers
    PAF_AST_DecOpCircBuf **pXDecOpCb;   // address of decoder output circular buffer base pointer
} PAF_AST_DecOpCircBufCtl;

// Initialize circular buffer control
Int cbCtlInit(
    PAF_AST_DecOpCircBufCtl *pCbCtl,    // decoder output circular buffer control
    Int8 numDecOpCb,                    // number of circular buffers
    PAF_AST_DecOpCircBuf **pXDecOpCb    // address of decoder output circular buffer base pointer
);

// Reset circular buffer
Int cbReset(
    PAF_AST_DecOpCircBufCtl *pCbCtl,
    Int8 cbIdx
);

// Get circular buffer statistics
Int cbGetStats(
    PAF_AST_DecOpCircBufCtl *pCbCtl,    // decoder output circular buffer control
    Int8 cbIdx,                         // decoder output circular buffer index
    PAF_AST_DecOpCircBufStats *pCbStats // decoder output circular buffer statistics
    
);

// Output log of circular buffer control variables (debug)
Int cbLog(
    PAF_AST_DecOpCircBufCtl *pCbCtl,
    Int8 cbIdx, 
    Int8 fullLog, 
    char *locInfo
);


#endif /* _ASP_DECOP_CB_COMMON_H_ */
