
/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _PSDKAF_HEAPMGR_H_
#define _PSDKAF_HEAPMGR_H_

#include <xdc/std.h>
#include <xdc/runtime/IHeap.h>

#define PAF_NUM_PRVMEM_HEAPS                ( 3 )   // number of private (local) heaps
#define PAF_NUM_SHMEM_HEAPS                 ( 3 )   // number of shared heaps
#define PAF_NUM_HEAPS                       ( PAF_NUM_PRVMEM_HEAPS + PAF_NUM_SHMEM_HEAPS ) // total number of heaps

// heap IDs
#define PAF_HEAP_ID_INT                     ( 0 )   // L2 SRAM
#define PAF_HEAP_ID_INT1                    ( 1 )   // MSMC SRAM
#define PAF_HEAP_ID_EXT                     ( 2 )   // DDR3
#define PAF_HEAP_ID_INT1_SHM                ( 3 )   // MSMC SRAM, Shared
#define PAF_HEAP_ID_EXT_SHM                 ( 4 )   // DDR3, Shared
#define PAF_HEAP_ID_EXT_NON_CACHED_SHM      ( 5 )   // DDR3 non-cached, Shared

// macros for translating heap IDs to handles
#define PAF_HEAP_INT                        (HeapMem_Handle)pafHeapMgr_readHeapHandle(gPafHeapIdInt)
#define PAF_HEAP_INT1                       (HeapMem_Handle)pafHeapMgr_readHeapHandle(gPafHeapIdInt1)
#define PAF_HEAP_EXT                        (HeapMem_Handle)pafHeapMgr_readHeapHandle(gPafHeapIdExt)
#define PAF_HEAP_INT1_SHM                   (HeapMem_Handle)pafHeapMgr_readHeapHandle(gPafHeapIdInt1Shm)
#define PAF_HEAP_EXT_SHM                    (HeapMem_Handle)pafHeapMgr_readHeapHandle(gPafHeapIdExtShm)
#define PAF_HEAP_EXT_NONCACHED_SHM          (HeapMem_Handle)pafHeapMgr_readHeapHandle(gPafHeapIdExtNonCachedShm)

// exported heap IDs
extern Int gPafHeapIdInt;               // L2 SRAM
extern Int gPafHeapIdInt1;              // MSMC SRAM
extern Int gPafHeapIdExt;               // DDR3
extern Int gPafHeapIdInt1Shm;           // MSMC SRAM, Shared
extern Int gPafHeapIdExtShm;            // DDR3, Shared
extern Int gPafHeapIdExtNonCachedShm;   // DDR3 non-cached, Shared

/* Initialize PAF heap manager */
Void pafHeapMgr_init(
    IHeap_Handle hIntHeap,
    IHeap_Handle hIntHeap1,
    IHeap_Handle hExtHeap,
    IHeap_Handle hIntHeap1Shm,
    IHeap_Handle hExtHeapShm,
    IHeap_Handle hExtHeapNonCachedShm
);

/* Write heap handle to PAF heap manager for provided index */
Void pafHeapMgr_writeHeapHandle(
    Int heapId,
    IHeap_Handle hHeap
);

/* Read heap handle from PAF heap manager for provided index */
IHeap_Handle pafHeapMgr_readHeapHandle(
    Int heapId
);

#endif /* _PSDKAF_HEAPMGR_H_ */
