/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamProc_common.h ========
 */

#ifndef _ASP_COMMON_H_
#define _ASP_COMMON_H_
 
#include <xdc/std.h>
#include "audioStreamProc_config.h"
#include "paf_heapMgr.h"

// .............................................................................

/* ---------------------------------------------------------------- */
/* For historical reasons, macro definitions (#define ...) are used */
/* to hide parameter references (pP->...) throughout this file, but */
/* only for select quantities and not for all parameter elements.   */
/*                                                                  */
/*             Parameter macro definitions start here.              */
/* ---------------------------------------------------------------- */

//
// Audio Topology (Zone) Definitions
//
//   The ZONE, a historical term here, is a letter identifying the Audio
//   Topology with a single letter or "*" for a variable quantization of
//   same.
//
//   The Zone Elements listed here indicate the cardinality of the corre-
//   sponding element:
//
//     INPUTS   Number of inputs.
//     DECODES  Number of decodes.
//     STREAMS  Number of streams.
//     ENCODES  Number of encodes.
//     OUTPUTS  Number of outputs.
//
//   The Zone Element Counts listed here indicate the first (1) and after-
//   last (N) values suitable for use in a loop:
//
//     INPUTS[1N]  for inputs.
//     DECODES[1N] for decodes.
//     STREAMS[1N] for streams.
//     ENCODES[1N] for encodes.
//     OUTPUTS[1N] for outputs.
//
//   The Zone Element Count Limits listed here establish array sizes:
//
//     DECODEN_MAX for decodes.
//     STREAMN_MAX for streams.
//     ENCODEN_MAX for encodes.
//
//   The Zone Master is important in multi-input frameworks:
//
//     MASTER   In a uni-input zone, the count of the input.
//              In a multi-input zone, the count of the primary which controls
//              the secondary or secondaries.
//

#ifndef ZONE

#define ZONE "*" /* 19.53 kB of 38.80 kB FW */
//
//#define MASTER  pP->zone.master
//#define INPUTS  pP->zone.inputs
//#define DECODES pP->zone.decodes
#define STREAMS pP->zone.streams
//#define ENCODES pP->zone.encodes
//#define OUTPUTS pP->zone.outputs

#endif /* ZONE */

#ifndef ZONE

#define ZONE "I" /* ~2.5 kB less than above */

#define MASTER  0
#define INPUTS  1
#define DECODES 1
#define STREAMS 1
#define ENCODES 1
#define OUTPUTS 1

#endif /* ZONE */

#define INPUT1  pP->zone.input1
#define INPUTN  pP->zone.inputN
//
#define DECODE1 pP->zone.decode1
#define DECODEN pP->zone.decodeN
//#define DECODEN_MAX 3
//
#define STREAM1 0
#define STREAMN STREAMS
//#define STREAMN_MAX 5
//
#define ENCODE1 pP->zone.encode1
#define ENCODEN pP->zone.encodeN
//#define ENCODEN_MAX 3
//
#define OUTPUT1 pP->zone.output1
#define OUTPUTN pP->zone.outputN

//
// Audio Data Representation Definitions
//

/* audio frame "width" in channels */
#define numchan pP->z_numchan

/* audio frame "length" in samples */
#define FRAMELENGTH pP->framelength

#define PA_MODULO       8   // also defined independently in ARC2 code, and may be hard coded other places.


// .............................................................................

//
// Heap & Memory Allocation Definitions
//
#define HEAP_ID_INTERNAL                *pP->heap.pHeapIdIntern
#define HEAP_ID_INTERNAL1               *pP->heap.pHeapIdIntern1
#define HEAP_ID_EXTERNAL                *pP->heap.pHeapIdExtern
#define HEAP_ID_INTERNAL1_SHM           *pP->heap.pHeapIdInt1Shm
#define HEAP_ID_EXTERNAL_SHM            *pP->heap.pHeapIdExtShm
#define HEAP_ID_EXTERNAL_NONCACHED_SHM  *pP->heap.pHeapIdExtNonCachedShm

#define HEAP_INTERNAL                   (HeapMem_Handle)pafHeapMgr_readHeapHandle(HEAP_ID_INTERNAL)
#define HEAP_INTERNAL1                  (HeapMem_Handle)pafHeapMgr_readHeapHandle(HEAP_ID_INTERNAL1)
#define HEAP_EXTERNAL                   (HeapMem_Handle)pafHeapMgr_readHeapHandle(HEAP_ID_EXTERNAL)
#define HEAP_INTERNAL1_SHM              (HeapMem_Handle)pafHeapMgr_readHeapHandle(HEAP_ID_INTERNAL1_SHM)
#define HEAP_EXTERNAL_SHM               (HeapMem_Handle)pafHeapMgr_readHeapHandle(HEAP_ID_EXTERNAL_SHM)
#define HEAP_EXTERNAL_NONCACHED_SHM     (HeapMem_Handle)pafHeapMgr_readHeapHandle(HEAP_ID_EXTERNAL_NONCACHED_SHM)
#define HEAP_CLEAR                      pP->heap.clear

#define HEAP_ID_INPBUF                  *pP->heap.pHeapIdInpbuf
#define HEAP_ID_OUTBUF                  *pP->heap.pHeapIdOutbuf
#define HEAP_ID_FRMBUF                  *pP->heap.pHeapIdFrmbuf
#define HEAP_INPBUF                     (HeapMem_Handle)pafHeapMgr_readHeapHandle(HEAP_ID_INPBUF)
#define HEAP_OUTBUF                     (HeapMem_Handle)pafHeapMgr_readHeapHandle(HEAP_ID_OUTBUF)
#define HEAP_FRMBUF                     (HeapMem_Handle)pafHeapMgr_readHeapHandle(HEAP_ID_FRMBUF)

#define HEAP_ID_MDBUF                   *pP->pMetadataBufStatus->pHeapIdMdbuf
#define HEAP_MDBUF                      (HeapMem_Handle)pafHeapMgr_readHeapHandle(HEAP_ID_MDBUF)


#define COMMONSPACE                     pP->common.space

// .............................................................................

// -----------------------------------------------------------------------------
// Debugging Trace Control
//#define TRACE_TERSE(a)              LOG_printf a
#define TRACE_TERSE0(a)             Log_info0(a)
#define TRACE_TERSE1(a,b)           Log_info1(a,b)
#define TRACE_TERSE2(a,b,c)         Log_info2(a,b,c)
#define TRACE_TERSE3(a,b,c,d)       Log_info3(a,b,c,d)
#define TRACE_TERSE4(a,b,c,d,e)     Log_info4(a,b,c,d,e)

//#define TRACE_GEN(a)                LOG_printf a
#define TRACE_GEN0(a)               Log_info0(a)
#define TRACE_GEN1(a,b)             Log_info1(a,b)
#define TRACE_GEN2(a,b,c)           Log_info2(a,b,c)
#define TRACE_GEN3(a,b,c,d)         Log_info3(a,b,c,d)
#define TRACE_GEN4(a,b,c,d,e)       Log_info4(a,b,c,d,e)

//#define TRACE_VERBOSE(a)            LOG_printf a
#define TRACE_VERBOSE0(a)           Log_info0(a)
#define TRACE_VERBOSE1(a,b)         Log_info1(a,b)
#define TRACE_VERBOSE2(a,b,c)       Log_info2(a,b,c)
#define TRACE_VERBOSE3(a,b,c,d)     Log_info3(a,b,c,d)
#define TRACE_VERBOSE4(a,b,c,d,e)   Log_info4(a,b,c,d,e)

#define TRACE_MSG0(a)               Log_info0(a)
#define TRACE_MSG1(a,b)             Log_info1(a,b)
#define TRACE_MSG2(a,b,c)           Log_info2(a,b,c)
#define TRACE_MSG3(a,b,c,d)         Log_info3(a,b,c,d)
#define TRACE_MSG4(a,b,c,d,e)       Log_info4(a,b,c,d,e)


#define DEC_Handle PCM_Handle /* works for all: SNG, PCM, AC3, DTS, AAC */


/* Common (shared) configuration */
extern PAF_AST_Config gPAF_AST_config;

#endif /* _ASP_COMMON_H_ */
