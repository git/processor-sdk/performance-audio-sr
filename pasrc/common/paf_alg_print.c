
/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// Algorithm RAM_REPORT definitions
//

#include <stdlib.h> // malloc
//#include <stdio.h>  // printf
#include <paf_alg_print.h>

#include <xdc/runtime/Memory.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/heaps/HeapMem.h>

Int
PAF_ALG_allocPrint(const PAF_ALG_AllocInit *pInit, Int sizeofInit, PAF_IALG_Config *p)
{
    IALG_MemRec *memTab;
    Int commonSize[PAF_IALG_COMMON_MEMN+1];
    Int commonCount[PAF_IALG_COMMON_MEMN+1];
    Int commonSpace[PAF_IALG_COMMON_MEMN+1];
    Int i,n;

    for ( ; pInit && pInit->ialg_fxns; pInit=(const PAF_ALG_AllocInit *)((char *)pInit+sizeofInit)) 
    {
#ifdef _TMS320C6X
#pragma UNROLL(1)
#endif
        for (i=PAF_IALG_COMMON_MEM0; i<=PAF_IALG_COMMON_MEMN; ++i) 
        {
            commonSize[i]=0;
            commonCount[i]=0;
            commonSpace[i]=-1;
        }
        n = pInit->ialg_fxns->algNumAlloc != NULL ? pInit->ialg_fxns->algNumAlloc() : IALG_DEFMEMRECS;
        if ((memTab = (IALG_MemRec *)malloc(n * sizeof(IALG_MemRec))))
        {
            n = pInit->ialg_fxns->algAlloc(pInit->ialg_prms, (IALG_Fxns **)&(pInit->ialg_fxns),memTab);
            if (n <= 0)
            {
                return PAF_ALGERR_MEMTAB_COUNT;
            }

            for (i=0; i<n; ++i)
            {
                if ((((Int)memTab[i].attrs >= PAF_IALG_COMMON_MEM0) && ((Int)memTab[i].attrs <= PAF_IALG_COMMON_MEMN))) 
                {
                    commonSize[memTab[i].attrs] += memTab[i].size + 
                        (commonCount[memTab[i].attrs] ? memTab[i].alignment : 0);
                    commonCount[memTab[i].attrs]++;
                }
                // FL: This is broken for printout of persistent memory
                //  The code was broken before addition of persist memory in INT1 SHM (MSMC).
                //  commonSize[] and commonCount[] are correct, but here persist is treated like common memory, i.e.
                //      persistent memory is treated as through it's combined into a single memory space.
                //      For example, printout can only show persist memory as belonging to one space.
                //      This isn't true for persistent memory since it can be in multiple spaces.
                if (commonSpace[memTab[i].attrs] < (Int)memTab[i].space) 
                {
                    commonSpace[memTab[i].attrs] = (Int)memTab[i].space;
                }
            }
            free(memTab);

            System_printf("\n0x%-3x\t", ((PAF_ASP_LinkInit*)pInit)->thisCode.part.beta);
            for (i=PAF_IALG_COMMON_MEM0; i<=PAF_IALG_COMMON_MEMN; ++i)
            {
                if (commonSize[i] == 0)
                {
                    System_printf("%9d\t",0);
                }
                else
                {
                    System_printf("%6d(%1d)\t", commonSize[i], 
                        PAF_ALG_memSpaceToHeapId(p, (IALG_MemSpace)commonSpace[i]));                    
                }                    
            }
            
            commonSize[IALG_SCRATCH] = commonSize[IALG_PERSIST] = commonSize[IALG_WRITEONCE] = 0;
            
            System_flush();
        }
        else
        {
            return 1;            
        }
    }
    return 0;
}

Void
PAF_ALG_commonPrint(IALG_MemRec common[], PAF_IALG_Config *p)
{

    Int i;

    System_printf("\n\n");
    for (i=0; i<130; i++)
    {
        System_printf ("--");
    }

    System_printf ("\nCOMMON                         \t\t\t\t");
    for (i=PAF_IALG_COMMON_MEM0; i <= PAF_IALG_COMMON_MEMN; i++) 
    {
        if (i <= IALG_WRITEONCE)
        {
            ; //printf(""); // print empty string??            
        }
        else if (common[i].size == 0)
        {
            System_printf("%9d\t",0);
        }
        else
        {
            System_printf("%6d(%1d)\t", common[i].size, 
                PAF_ALG_memSpaceToHeapId(p, common[i].space));
        }            
    }
    
    System_flush();
}

Void
PAF_ALG_memStatusPrint(
    CString header, 
    HeapMem_Handle hInternalHeap, 
    HeapMem_Handle hInternal1Heap,
    HeapMem_Handle hExternalHeap, 
    HeapMem_Handle hInternal1HeapShm,
    HeapMem_Handle hExternalHeapShm,
    HeapMem_Handle hExternalNonCachedShm
)
{
    Memory_Stats stats;

    System_printf("\n\n%s\n", header);
    System_printf("%-12s%-12s%-12s%-12s", "Heap", "Size", "Used", "Available");
    
    if (hInternalHeap != NULL)
    {
        Memory_getStats((IHeap_Handle)hInternalHeap, &stats);
        System_printf("\n%-12s0x%08x  0x%08x  0x%08x", "INT", stats.totalSize, stats.totalSize-stats.totalFreeSize, stats.largestFreeSize);
    }
    if (hInternal1Heap != NULL)
    {
        Memory_getStats((IHeap_Handle)hInternal1Heap, &stats);
        System_printf("\n%-12s0x%08x  0x%08x  0x%08x", "INT1", stats.totalSize, stats.totalSize-stats.totalFreeSize, stats.largestFreeSize);    
    }
    if (hExternalHeap != NULL)
    {
        Memory_getStats((IHeap_Handle)hExternalHeap, &stats);
        System_printf("\n%-12s0x%08x  0x%08x  0x%08x", "EXT", stats.totalSize, stats.totalSize-stats.totalFreeSize, stats.largestFreeSize);
    }
    if (hInternal1HeapShm != NULL)
    {
        Memory_getStats((IHeap_Handle)hInternal1HeapShm, &stats);
        System_printf("\n%-12s0x%08x  0x%08x  0x%08x", "INT1 SHM", stats.totalSize, stats.totalSize-stats.totalFreeSize, stats.largestFreeSize);
    }
    if (hExternalHeapShm != NULL)
    {
        Memory_getStats((IHeap_Handle)hExternalHeapShm, &stats);
        System_printf("\n%-12s0x%08x  0x%08x  0x%08x", "EXT SHM", stats.totalSize, stats.totalSize-stats.totalFreeSize, stats.largestFreeSize);
    }
    if (hExternalNonCachedShm != NULL)
    {
        Memory_getStats((IHeap_Handle)hExternalNonCachedShm, &stats);
        System_printf("\n%-12s0x%08x  0x%08x  0x%08x", "EXT NC SHM", stats.totalSize, stats.totalSize-stats.totalFreeSize, stats.largestFreeSize);
    }
    
    System_flush();
}

Void
PAF_ALG_bufMemPrint(
    Int z,
    Int size, 
    Int heapId,
    Int bufType
)
{
    System_printf("\n\n%s Device Buffer (Zone %d) = %d (%d)", 
          (bufType==0 ? "Input" : bufType==1 ? "Output" : "Audio Frame"), 
          z, size, heapId);
    System_printf("\n");
      
    System_flush();
}

Void
PAF_ALG_headerPrint()
{
    Int i;

    System_printf("\n\nAlgorithm Memory requirement:\n");
    System_printf("  Key:  \n");
    System_printf("    Beta ID can be decoded from stdbeta.h, oembeta.h or cusbeta.h in pa\\sio\\acp1.\n");
    System_printf("    The number in parentheses is the heap.  (0) is INT.  (1) is INT1.  (2) is EXT.  (3) is INT1 SHM.  (4) is EXT SHM.  (5) is EXT NC SHM.\n");
    System_printf("%8s\t","BetaId");

    for (i=PAF_IALG_COMMON_MEM0; i<=PAF_IALG_COMMON_MEMN; ++i)
    {
        if (i == 0) 
        {
            System_printf("%8s\t","SCRATCH");
        }
        else if (i == 1)
        {
            System_printf("%8s\t","PERSIST");
        }            
        else if (i == 2)
        {
            System_printf("%8s \t","WRITEONCE");
        }            
        else if (i < 10)
        {
            System_printf("%-7s%d  \t","COMMON",(i==0 ? i : i-IALG_WRITEONCE));
        }
        else
        {
            System_printf("%6s%2d \t","COMMON",(i==0 ? i : i-IALG_WRITEONCE));
        }
    }

    System_flush();
}
