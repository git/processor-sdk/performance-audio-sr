/* 
 *  pfp.h: Definitions for Profile Points functions
 */
#ifndef _PFP_H
#define _PFP_H

#include <stdint.h>

#include <xdc/runtime/Error.h>

#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Swi.h>
#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/gates/GateAll.h>

/* Chaging this definition would change how many profile points can be used */
#define PFP_PPCNT_MAX   32      /* this is the maximum number of profile points */

/* Profile Point Descriptor Type */
struct pfpDescriptor_stc {
  void            *exhandle;    /* Execution object handle (hwi/swi/task) */
  int             id;           /* PP id which is the index into the pfpInst->pfpVector[] */
  int             scope;        /* BIOS_ThreadType */
  int             enable;       /* 1: enabled, 0: disabled */
  int             inside;       /* 1: inside (inside the measurement bracket), 0: outside */
  int             active;       /* 1: clock is active, 0: clock has been "paused" */
  int             latch;        /* 1: Latch the c_partial to spread over multiple PP regions */
  int             reset;        /* 1: reset stats in pfpBegin(), 0: do not reset */
  long long       c_total;      /* Total cycles (full count) */
  uint_least32_t  n_count;      /* Number of completed measurements */
  uint_least32_t  c_partial;    /* Current measurement */
  uint_least32_t  c_0;          /* Starting point time stamp */
  uint_least32_t  c_min;        /* Minimum measurement */
  uint_least32_t  c_max;        /* Maximum measurement */
  float           alpha;        /* Exponential averaging constant */
  float           c_average;    /* Exponentially averaged cycles */
  struct pfpDescriptor_stc *next;    /* Next element when in the active list */
  struct pfpDescriptor_stc *prev;    /* Previous element when in the active list */
};
typedef struct pfpDescriptor_stc pfpDescriptor_t;

/* Profile point instance type */
struct pfpInst_stc {
  GateAll_Handle    ghandle;    /* Gate handle for Critical Section */
  pfpDescriptor_t   *head;      /* head of active profile points list */
  pfpDescriptor_t   *tail;      /* tail of active profile points list */
  uint_least32_t    overhead;   /* 0: if not calibrated */
  int               depth;      /* Current length of the active profile points list */
  int               active_id;  /* id of currently active PP */

  pfpDescriptor_t   pfpVector[PFP_PPCNT_MAX]; /* Statically allocated profile point vector */
};
typedef struct pfpInst_stc pfpInst_t;

/* Statistics reporting structure */
struct pfpStats_stc {
  long long       c_total;
  uint_least32_t  n_count;
  uint_least32_t  c_min;
  uint_least32_t  c_max;
  float           alpha;
  float           c_average;
};
typedef struct pfpStats_stc pfpStats_t;

/* Global variables */
extern pfpInst_t      pfpInst;

extern int pfpTaskHookSetId;            /* Used for Task hook functions */
extern int pfpSwiHookSetId;             /* Used for Swi hook functions */
extern int pfpHwiHookSetId;             /* Used for Hwi hook functions */

/* PFP Functions */
extern void pfpCalibrate(int loopcnt, int reset);
extern void pfpCreate(void);
extern void pfpBegin(int id, void *exhandle);
extern void pfpDisable(int id);
extern void pfpEnable(int id);
extern void pfpEnd(int id, int latch);
extern void pfpGetStats(int id, pfpStats_t *pStats);
extern void pfpResetStats(int id);
extern void pfpSetAlpha(int id, float alpha);

/* PFP Hook Functions */
extern void pfpTaskRegister(int hookSetId);
extern void pfpTaskCreate(Task_Handle task, Error_Block *eb);
extern void pfpTaskReady(Task_Handle task);
extern void pfpTaskSwitch(Task_Handle prev, Task_Handle next);

extern void pfpSwiRegister(int hookSetId);
extern void pfpSwiCreate(Swi_Handle swi, Error_Block *eb);
extern void pfpSwiReady(Swi_Handle swi);
extern void pfpSwiBegin(Swi_Handle swi);
extern void pfpSwiEnd(Swi_Handle swi);

extern void pfpHwiRegister(int hookSetId);
extern void pfpHwiCreate(Hwi_Handle hwi, Error_Block *eb);
extern void pfpHwiReady(Hwi_Handle hwi);
extern void pfpHwiBegin(Hwi_Handle hwi);
extern void pfpHwiEnd(Hwi_Handle hwi);

#endif
/* nothing past this point */

