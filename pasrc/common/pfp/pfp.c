/*
 * pfp.c
 * 
 * NOTE: Throughout this code we may refer to the "active queue" or list. That is actually
 *       a queue of Profile Points (PP's) where we are currently "inside". Only one of them
 *       can be really "active" since a CPU core can only execute one task at a time. In short,
 *       whenever we enter the Begin-End bracket we place that profile point on this queue.
 *       The most recent one that is or was executing should be at the tail of the queue.
 *
 *  Created on: Apr 1, 2017
 *      Author: a0216546
 */

/* DEBUG Flags */
#define PFP_DBG_HOOKS   0

#include "pfp.h"

#define PFP_MIN(a,b)    (((a)>(b))?(b):(a))     /* Min/Max macros */
#define PFP_MAX(a,b)    (((a)<(b))?(b):(a))

#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>

#include <xdc/runtime/Timestamp.h>    /* for benchmarking/profiling */
#define Timestamp_get Timestamp_get32 /* use 32-bit time stamps */

#include <ti/sysbios/BIOS.h> 
#include <ti/sysbios/gates/GateAll.h> /* For Critical Sections */

#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Swi.h>
#include <ti/sysbios/hal/Hwi.h>

/* Global Variables */
pfpInst_t       pfpInst;          /* Define pfpInst globally. Only one can exist. */

/* Critical Section macros */
#define PFP_GATE_ENTER()      GateAll_enter(pfpInst.ghandle);
#define PFP_GATE_LEAVE(key)   GateAll_leave(pfpInst.ghandle,key)

int pfpTaskHookSetId;     /* used for Task hooks */
int pfpSwiHookSetId;      /* used for Swi hooks */
int pfpHwiHookSetId;      /* used for Hwi hooks */

/*==================================================================================*/
/* PFP Functions that are used for profile point management and actual measurements */
/*==================================================================================*/

/*============================================================================*/
/* pfpCreate: initialize all profile point descriptors and instance structure */
/*============================================================================*/

void pfpCreate (void)
{
  int k;
  pfpDescriptor_t *pfp;
  GateAll_Params  params;

  GateAll_Params_init(&params);
  pfpInst.ghandle = GateAll_create(&params,NULL);
  if (pfpInst.ghandle == NULL) {
    BIOS_exit(1);
  }
  pfpInst.head      = NULL;
  pfpInst.tail      = NULL;
  pfpInst.overhead  = 0;
  pfpInst.depth     = 0;
  pfpInst.active_id = -1;

  for (k = 0; k < PFP_PPCNT_MAX; k++) {
    pfp = &pfpInst.pfpVector[k];      /* fetch current pp-descriptor */

    pfp->exhandle   = NULL;
    pfp->id         = -1;
    pfp->scope      = BIOS_ThreadType_Main;
    pfp->enable     = 0;
    pfp->inside     = 0;
    pfp->active     = 0;
    pfp->latch      = 0;
    pfp->reset      = 0;
    pfp->c_total    = 0;
    pfp->n_count    = 0;
    pfp->c_partial  = 0;
    pfp->c_0        = 0;
    pfp->c_min      = (~0UL);      /* very large integer */
    pfp->c_max      = 0;
    pfp->alpha      = 0.0;
    pfp->c_average  = 0.0;
    pfp->next       = NULL;
    pfp->prev       = NULL;
  }
} /* pfpCreate */

/*===================================================================================*/
/* pfpSetAlpha: initialize exponential averaging constant for a single profile point */
/*
      alpha must be in [0,1] range. When 0, exponantial averaging will not be calculated.
      When 1, the average would equal to the most recent cycle measurement.
 
      If a profile point is executed every T seconds and we want our time constant to be
      tau, alpha should be T/tau. Clearly, tau > T for meaningful measurements.
*/
/*===================================================================================*/

void pfpSetAlpha (int id, float alpha)
{
  pfpDescriptor_t *pfp;
  volatile IArg key;
  BIOS_ThreadType scope = BIOS_getThreadType();

  if (scope != BIOS_ThreadType_Main) {
    key=PFP_GATE_ENTER();
  }
  pfp = &pfpInst.pfpVector[id];             /* fetch the PFP descriptor */

  pfp->alpha = alpha;       /* Store new value of exponantial averating constant */

  if (scope != BIOS_ThreadType_Main) {
    PFP_GATE_LEAVE(key);
  }

} /* pfpSetAlpha */

/*============================================================================*/
/* pfpResetStats: Reset statistics for one profile point                      */
/* 
 *      Preferably, this should be done from the lowest execution priority
 *      in order to ensure active list is empty. This should not be
 *      done in the middle of a profile point bracket.
 */ 
/*============================================================================*/

void pfpResetStats (int id)
{
  pfpDescriptor_t *pfp;
  volatile IArg key;
  BIOS_ThreadType scope = BIOS_getThreadType();

  if (scope != BIOS_ThreadType_Main) {
    key=PFP_GATE_ENTER();
  }
  pfp = &pfpInst.pfpVector[id];             /* fetch the PFP descriptor */
  /* We cannot reset stats for currently running or latched profile point. We will do it
     in next pfpBegin() making sure measurement is not latched. */
  if (pfp->inside || pfp->latch) {
    pfp->reset = 1;             /* this PP will be reset in pfpBegin() when convenient */
    if (scope != BIOS_ThreadType_Main) {
      PFP_GATE_LEAVE(key);
    }
    return;
  }

  pfp->c_total    = 0;
  pfp->n_count    = 0;
  pfp->c_partial  = 0;
  pfp->c_0        = 0;
  pfp->c_min      = (~0UL);      /* very large integer */
  pfp->c_max      = 0;
  pfp->c_average  = 0.0;

  if (scope != BIOS_ThreadType_Main) {
    PFP_GATE_LEAVE(key);
  }

} /* pfpResetStats */

/*============================================================================*/
/* pfpGetStats: Return statistics for one profile point                       */
/*============================================================================*/

void pfpGetStats(int id, pfpStats_t *pStats)
{
  pfpDescriptor_t *pfp;
  volatile IArg key;
  BIOS_ThreadType scope = BIOS_getThreadType();

  if (scope != BIOS_ThreadType_Main) {
    key=PFP_GATE_ENTER();
  }
  pfp = &pfpInst.pfpVector[id];             /* fetch the PFP descriptor */

  pStats->c_total   = pfp->c_total;
  pStats->n_count   = pfp->n_count;
  pStats->c_min     = pfp->c_min;
  pStats->c_max     = pfp->c_max;
  pStats->alpha     = pfp->alpha;
  pStats->c_average = pfp->c_average;

  if (scope != BIOS_ThreadType_Main) {
    PFP_GATE_LEAVE(key);
  }
} /* pfpGetStats */

/*============================================================================*/
/* pfpEnable: Enable one profile point                                        */
/*============================================================================*/

void pfpEnable (int id)
{
  pfpDescriptor_t *pfp;
  volatile IArg key;
  BIOS_ThreadType scope = BIOS_getThreadType();

  if (scope != BIOS_ThreadType_Main) {
    key=PFP_GATE_ENTER();
  }
  pfp = &pfpInst.pfpVector[id];     /* fetch the PFP descriptor */

  pfp->enable = 1;          /* Enable this profile point */

  if (scope != BIOS_ThreadType_Main) {
    PFP_GATE_LEAVE(key);
  }

} /* pfpEnable */

/*============================================================================*/
/* pfpDisable: Disable one profile point
 
   This function should be called from the lowest priority making sure that
   the PP ID to be disabled is not currently being measured!
*/
/*============================================================================*/

void pfpDisable (int id)
{
  pfpDescriptor_t *pfp;
  volatile IArg key;
  BIOS_ThreadType scope = BIOS_getThreadType();

  if (scope != BIOS_ThreadType_Main) {
    key=PFP_GATE_ENTER();
  }
  pfp = &pfpInst.pfpVector[id];     /* fetch the PFP descriptor */

  pfp->enable = 0;          /* Disable this profile point */

  /* WARNING: Should we check here to remove this PP from the active list in case we are disabling it? */
  /* pfpEnd is not going to remove it if it was disabled!!! */

  if (scope != BIOS_ThreadType_Main) {
    PFP_GATE_LEAVE(key);
  }
} /* pfpDisable */

/*============================================================================*/
/* pfpBegin: Begin profile point measurement                                  */
/*============================================================================*/

void pfpBegin (int id, void *exhandle)
{
  volatile uint_least32_t t1;
  uint_least32_t          dt;
  pfpDescriptor_t         *pfp;
  volatile IArg key;
  BIOS_ThreadType scope = BIOS_getThreadType();

  if (scope != BIOS_ThreadType_Main) {
    key=PFP_GATE_ENTER();
  }
  t1 = Timestamp_get();             /* fetch the current time stamp */
  pfp = &pfpInst.pfpVector[id];     /* fetch the PFP descriptor */

  if (!pfp->enable) {
    if (scope != BIOS_ThreadType_Main) {
      PFP_GATE_LEAVE(key);
    }
    return;               /* do nothing if disabled */
  }

  /* First check to see if the current tail element is nesting this one */
  if (pfpInst.tail != NULL) {
    if (pfpInst.tail->exhandle == exhandle) { /* are we nested? */
      /* We could check here to see if we have Begin followed by Begin for the same PP ID ! */
      if (pfpInst.tail == pfp) {
        /* INVALID USAGE OF THE API!!!!! */
        /* Remove this from the list and DISABLE this ID */
        pfpInst.depth--;                /* reduce the depth! */
        pfpInst.tail = pfp->prev;
        if (pfpInst.tail != NULL) {
          pfpInst.tail->next = NULL;
        }
        pfp->enable = 0;                /* DISABLE it so it would not create these problems again! */
        pfp->inside = 0;
        pfp->active = 0;
        pfp->latch = 0;
        if (scope != BIOS_ThreadType_Main) {
          PFP_GATE_LEAVE(key);
        }
        return;
      }
      pfpInst.tail->active     = 0;            /* mark as not-active */
      dt                       = t1 - pfpInst.tail->c_0;
      pfpInst.tail->c_partial += dt;
      /* c_0 will have to be initialized before coming back! (see pfpEnd()) */
    }
  }

  /* Now we can focus on the new profile point */
  pfp->exhandle   = exhandle;   /* record execution object handle */
  pfp->id         = id;         /* record your ID */
  pfp->scope      = scope;      /* record the scope */
  if (!pfp->latch) {          /* if not latched */
    pfp->c_partial  = 0;      /*   initialize partial sum */
    if (pfp->reset) {             /* execute delayed reset if necessary */
      pfp->reset      = 0;        /* no need to do it again */
      pfp->c_total    = 0;
      pfp->n_count    = 0;
      pfp->c_min      = (~0UL);
      pfp->c_max      = 0;
      pfp->c_average  = 0.0;
    }
  }

  /* Link this PP to the end of active list */
  pfp->prev = pfpInst.tail;   /* Previous is the current last element */
  pfp->next = NULL;           /* nobody after this one */
  if (pfpInst.head == NULL) {
    pfpInst.head = pfp;       /* now we are not empty */
    pfpInst.tail = pfp;
  }
  else {
    pfpInst.tail->next = pfp;     /* current last now has a next one */
    pfpInst.tail       = pfp;     /* move the last to point to this one */
  }
  pfpInst.depth++;        /* record new depth of the list */

  pfp->inside = 1;        /* indicate that we entered this PP now */
  pfp->active = 1;        /* and we are about to start running through it */
  pfpInst.active_id = id; /* record the currently active id */

  /* Record time as late as possible to reduce overhead during measurement */
  pfp->c_0 = Timestamp_get();   /* fetch the current time stamp */

  if (scope != BIOS_ThreadType_Main) {
    PFP_GATE_LEAVE(key);
  }
} /* pfpBegin */

/*============================================================================*/
/* pfpEnd: Complete profile point measurement                                 */
/*============================================================================*/

void pfpEnd (int id, int latch)
{
  volatile uint_least32_t t1;       /* completion time */
  uint_least32_t          dt;       /* completion time and delta time */
  uint_least32_t          partial;  /* partial sum */
  float                   alpha;    /* exponential averaging constant */
  pfpDescriptor_t *pfp;
  void            *exhandle;        /* Execution handle pointer which may be used */
  volatile IArg key;
  BIOS_ThreadType scope = BIOS_getThreadType();

  if (scope != BIOS_ThreadType_Main) {
    key=PFP_GATE_ENTER();
  }
  /* Record time ASAP to minimize measurement overhead */
  t1 = Timestamp_get();               /* fetch the current time stamp */
  pfp = &pfpInst.pfpVector[id];       /* fetch the PFP descriptor */

  if (!pfp->enable || !pfp->inside) {     /* if disabled or we had "end" before "begin" */
    if (scope != BIOS_ThreadType_Main) {
      PFP_GATE_LEAVE(key);
    }
    return;               /* do nothing if disabled */
  }

  pfp->active = 0;        /* We are going to stop running this measurement */
  pfp->inside = 0;        /* getting out after this */

  /* This has to be done in case we are not nested. It will be fixed below in case of nesting */
  pfpInst.active_id = -1; /* we were most likely the currently active PP, but not any more */

  dt            = t1 - pfp->c_0;
  partial       = pfp->c_partial + dt - pfpInst.overhead;
  if (!latch) {
    pfp->c_total += partial;              /* complete this measurement */
    pfp->n_count++;                       /* count it */
    pfp->c_min = PFP_MIN(pfp->c_min, partial);
    pfp->c_max = PFP_MAX(pfp->c_max, partial);
    /* do the exponential averaging */
    alpha = pfp->alpha;
    if (alpha > 0.0) {
      pfp->c_average += alpha*(partial - pfp->c_average);
    }
  }
  else {
    pfp->c_partial = partial;         /* latch the partial sum */
  }
  pfp->latch = latch;           /* record the latch flag */

  /* Remove this PP from the active list */
  if (pfpInst.head == pfp) {      /* Was I at the head of the list? */
    pfpInst.head = pfp->next;
    if (pfp->next != NULL) {
      pfp->next->prev = NULL;     /* Next one (if any) is now at the head of the list! */
    }
  }
  else {
    pfp->prev->next = pfp->next;      /* previous element should point forward to my next */
  }
  if (pfpInst.tail == pfp) {      /* Was I at the tail of the list? */
    pfpInst.tail = pfp->prev;
    if (pfp->prev != NULL) {
      pfp->prev->next = NULL;       /* Previous one (if any) is now at the tail of the list! */
    }
  }
  else {
    pfp->next->prev = pfp->prev;      /* next element should point back to my previous */
  }
  pfpInst.depth--;        /* record new depth of the list */

  /* Now check to see if the queue has a region nesting this one within the same task */
  /* We cannot look only at the tail since the tail could be from different task and
   *  it may be currently blocked without ability to run! */

  /* IMPORTANT: Here we will move the nesting region to the tail of the QUEUE
     if it is not there already!!!!! */

  exhandle = pfp->exhandle;       /* Remember current execution handle */
  pfp = pfpInst.tail;             /* Start from the tail */
  while (pfp != NULL) {           /* Do this until we hit the head */
    if (pfp->exhandle == exhandle) {  /* were we nested with this one? */
       pfp->active        = 1;                /* mark it as active */
       pfpInst.active_id  = pfp->id;          /* fix the active id */

       /* Take the new one we found and move it to the tail if it was not there */
       /* Order is A -> PFP -> B */
       if (pfpInst.tail != pfp) {
         if (pfpInst.head != pfp) {
           pfp->prev->next = pfp->next;   /* A->next = B */
         }
         else {
           pfpInst.head = pfp->next;    /* New head since pfp is going to the tail */
         }
         pfp->next->prev = pfp->prev;   /* B->prev = A (this is safe since PFP is not at the tail!) */
         pfp->prev = pfpInst.tail;      /* pfp->prev = TAIL */
         pfp->next = NULL;              /* pfp is now going to be at the tail! */
         pfpInst.tail->next = pfp;      /* TAIL->next = pfp (old tail points to pfp now) */
         pfpInst.tail = pfp;            /* new tail is pfp finally */
       }

       pfp->c_0           = Timestamp_get();  /* Record new time */

       break;       /* stop looking */
    }
    pfp = pfp->prev;      /* move back through the queue */
  }

  if (scope != BIOS_ThreadType_Main) {
    PFP_GATE_LEAVE(key);
  }
} /* pfpEnd */

/*============================================================================*/
/* pfpCalibrate: Calibrate Begin-End pair.
 
   This must run with all interrupts disabled. The best would be to run it
   during boot or in main. Although one could argue more precise measurement
   could be obtained if the GATE execution would be included.
   Currently, that is not supported. This must be called after pfpCreate.
   If the loopcnt is less than 16 calibration will not be done.
 
   If reset is used, ID #0 will be reset once the overhead has been stored.
*/
/*============================================================================*/

void pfpCalibrate(int loopcnt, int reset)
{
  int k, enable;
  float alpha;
  BIOS_ThreadType scope = BIOS_getThreadType();

  if (scope != BIOS_ThreadType_Main || loopcnt < 16) {
    pfpInst.overhead = 0;
    return;                   /* must be run before BIOS start */
  }

  /* Use PP #0 for this purpose */
  enable  = pfpInst.pfpVector[0].enable;
  alpha   = pfpInst.pfpVector[0].alpha;

  pfpResetStats(0);
  pfpInst.pfpVector[0].enable = 1;
  pfpInst.pfpVector[0].alpha  = 0.25;
  for (k = 0; k < loopcnt; k++) {
    pfpBegin(0,0);
    pfpEnd(0,0);
  }
  pfpInst.pfpVector[0].alpha  = alpha;
  pfpInst.pfpVector[0].enable = enable;

  /* Take the minimum and call it overhead */
  pfpInst.overhead = pfpInst.pfpVector[0].c_min;

  if (reset) {
    pfpResetStats(0);
  }
} /* pfpCalibrate */

/*==================================================================================*/
/* PFP Functions that are used for scheduling hooks within BIOS.                    */
/*==================================================================================*/

/*============================================================================*/
/* pfpHwiPause: Stop measurement during HWI.                                  */
/*============================================================================*/

static void pfpHwiPause(void)
{
  volatile uint_least32_t t1;
  uint_least32_t          dt;
  pfpDescriptor_t         *pfp;

  /* Find if any measurement is in progress? */
  t1 = Timestamp_get();               /* fetch the current time stamp */
  if (pfpInst.active_id < 0) {
    return;
  }
  pfp = pfpInst.tail;         /* This should be the active one! */

  pfpInst.active_id = -1;     /* all measurement brackets are inactive now */

  /* It would be nice to know if we need to accumulate dt here! (e.g. did we come from another HWI? */
  pfp->active     = 0;                /* this may be redundant */
  dt              = t1 - pfp->c_0;
  pfp->c_partial += dt;
  /* pfp->c_0 will have to be initialized before coming back! */

} /* pfpHwiPause */

/*============================================================================*/
/* pfpHwiResume: Resume measurement after HWI.                                */
/*============================================================================*/

static inline void pfpHwiResume(Hwi_Handle hwi)
{
  pfpDescriptor_t *pfp;

  /* Find the last PP in the list */
  if (pfpInst.depth < 1) {
    return;
  }
  pfp = pfpInst.tail;

  if (pfp->scope == BIOS_ThreadType_Task) {     /* Check the task mode */
    Task_Mode mode;
    mode = Task_getMode(pfp->exhandle);
    if (mode != Task_Mode_RUNNING) {    /* If not running do not take c_0, do not make it active */
      return;
    }
  }

  pfp->active       = 1;                /* this should be done only if task is not blocked! */
  pfpInst.active_id = pfp->id;
  pfp->c_0          = Timestamp_get();

} /* pfpHwiResume */

/*============================================================================*/
/* pfpSwiPause: Stop measurement during SWI.                                  */
/*============================================================================*/

static void pfpSwiPause(void)
{
  volatile uint_least32_t t1;
  uint_least32_t          dt;
  pfpDescriptor_t         *pfp;
  volatile IArg key;

  key=Hwi_disable();      /* Disable all HWI's */

  /* Find if any measurement is in progress? */
  t1 = Timestamp_get();               /* fetch the current time stamp */
  if (pfpInst.active_id < 0) {
    Hwi_restore(key);
    return;
  }
  pfp = pfpInst.tail;         /* This should be the active one! */

  pfpInst.active_id = -1;     /* all measurement brackets are inactive now */

  /* It would be nice to know if we have to accumulate dt here. (e.g. did we come from Hwi? */
  /* If we always come from HWI when entering SWI, we should not accumulate */
  pfp->active     = 0;                /* this may be redundant */
  dt              = t1 - pfp->c_0;
  pfp->c_partial += dt;
  /* pfp->c_0 will have to be initialized before coming back! */

  Hwi_restore(key);      /* restore the HWI's */

} /* pfpSwiPause */

/*============================================================================*/
/* pfpSwiResume: Resume measurement after SWI.                                */
/*============================================================================*/

static inline void pfpSwiResume(Swi_Handle swi)
{
  pfpDescriptor_t *pfp;
  volatile IArg key;

  key=Hwi_disable();      /* Disable all HWI's */

  /* Find the last PP in the list */
  if (pfpInst.depth < 1) {
    Hwi_restore(key);
    return;
  }
  pfp = pfpInst.tail;

  if (pfp->scope == BIOS_ThreadType_Task) {     /* Check the task mode */
    Task_Mode mode;
    mode = Task_getMode(pfp->exhandle);
    if (mode != Task_Mode_RUNNING) {      /* If not running do not take c_0, do not make it active */
      Hwi_restore(key);
      return;
    }
  }

  pfp->active       = 1;
  pfpInst.active_id = pfp->id;
  pfp->c_0          = Timestamp_get();

  Hwi_restore(key);      /* restore the HWI's */

} /* pfpSwiResume */

/*============================================================================*/
/* pfpTaskRegister: Task registration hook. Stores hook set ID.               */
/*============================================================================*/

void pfpTaskRegister(int hookSetId)
{
#if PFP_DBG_HOOKS
  System_printf("T-Register: id=%d\n", hookSetId);
  pfpTaskHookSetId = hookSetId;
#endif
} /* pfpTaskRegister */

/*============================================================================*/
/* pfpTaskCreate: Task creation hook. Sets the hook set context.              */
/*============================================================================*/

void pfpTaskCreate(Task_Handle task, Error_Block *eb)
{
#if PFP_DBG_HOOKS
  char *name;
  void *pEnv;

  name = Task_Handle_name(task);
  pEnv = Task_getHookContext(task, pfpTaskHookSetId);   /* We have set the hook set ID during registration */

  System_printf("T-Create: name='%s', pEnv=0x%x\n", name, pEnv);
  Task_setHookContext(task, pfpTaskHookSetId, (void*)0xdead);
#endif
} /* pfpTaskCreate */

/*============================================================================*/
/* pfpTaskReady: Task ready hook. Prepares task for run.                      */
/*============================================================================*/

void pfpTaskReady(Task_Handle task)
{
#if PFP_DBG_HOOKS
  char *name;
  void *pEnv;

  name = Task_Handle_name(task);
  pEnv = Task_getHookContext(task, pfpTaskHookSetId);   /* We have set the hook set ID during registration */

  System_printf("T-Ready: name='%s', pEnv=0x%x\n", name, pEnv);
  Task_setHookContext(task, pfpTaskHookSetId, (void*)0xc0de);
#endif
} /* pfpTaskReady */

/*============================================================================*/
/* pfpTaskSwitch: Task switch hook. Facilitates task switch operation.        */
/*============================================================================*/

void pfpTaskSwitch(Task_Handle prev, Task_Handle next)
{
#if PFP_DBG_HOOKS
  char *prevName;
  char *nextName;
  void *pPrevEnv;
  void *pNextEnv;
#endif

 volatile uint_least32_t t1;
  uint_least32_t          dt;
  int                     prev_id, next_id;

  volatile IArg key;
  pfpDescriptor_t *pfp;

  /* Using GATE has more overhead, but using just HWI disabling may not be safe if SWI's can
     be posted by the tasks. However, once inside the task hook, no other task can run until
     we're done and since we are not in SWI we are actually safe to use HWI disabling only! */

  key=Hwi_disable();        /* Enter Critical Section */
  t1 = Timestamp_get();

#if PFP_DBG_HOOKS
  if (prev == NULL) {
    System_printf("T-Switch: ignore 1st prev Task\n");
  }
  else {
    prevName = Task_Handle_name(prev);
    pPrevEnv = Task_getHookContext(prev, pfpTaskHookSetId);

    System_printf("T-Switch: prev name='%s', pPrevEnv=0x%x\n", prevName, pPrevEnv);
    Task_setHookContext(prev, pfpTaskHookSetId, (void*)0xcafec0de);
  }

  nextName = Task_Handle_name(next);
  pNextEnv = Task_getHookContext(next, pfpTaskHookSetId);

  System_printf("T-Switch: next name='%s', pNextEnv=0x%x\n", nextName, pNextEnv);
  Task_setHookContext(next, pfpTaskHookSetId, (void*)0xc001c0de);
#endif

  if (pfpInst.depth < 1) {
    Hwi_restore(key);       /* Exit critical section and return */
    return;
  }

  /* There are some active measurement PP's but we do not know where yet
     It may not be in prev nor next task so we have to do some investigation */

  /* Go through the list and find prev_id and next_id if they are inside any PP */
  prev_id = next_id = -1;
  pfp = pfpInst.tail;         /* This should be the one with the highest priority */
  while (pfp != NULL && (prev_id < 0 || next_id < 0)) {
    if (prev_id < 0 && pfp->exhandle == prev) { /* stop looking after you find the first match */
      prev_id = pfp->id;
    }
    if (next_id < 0 && pfp->exhandle == next) { /* stop looking after you find the first match */
      next_id = pfp->id;
    }
    pfp = pfp->prev;
  }

  /* Check to see if any of the two tasks are in the middle of a measurement */
  if (prev_id < 0 && next_id < 0) {
    Hwi_restore(key);
    return;

  }

  if (prev_id >= 0) {     /* Previous task may have been measuring and it has to be stopped */
    pfp = &pfpInst.pfpVector[prev_id];

    pfpInst.active_id = -1;     /* all measurement brackets are inactive now */

    if (pfp->active) {          /* This if is required in case we went through HWI/SWI */
      pfp->active     = 0;
      dt              = t1 - pfp->c_0;
      pfp->c_partial += dt;
    }
  }
  if (next_id >= 0) {     /* Next task was in the middle of measurement */
    pfp = &pfpInst.pfpVector[next_id];

    pfp->active       = 1;          /* We are about to continue measurement */
    pfpInst.active_id = next_id;

    /* Take the new one and move it to the tail if it was not there */
    /* Order is A -> PFP -> B where A could be Head and B could be NULL */
    if (pfpInst.tail != pfp) {
      if (pfpInst.head != pfp) {
        pfp->prev->next = pfp->next;   /* A->next = B */
      }
      else {
        pfpInst.head = pfp->next;    /* New head since pfp is going to the tail */
      }
      pfp->next->prev = pfp->prev;   /* B->prev = A (this could be NULL if pfp was on the Head) */
      pfp->prev = pfpInst.tail;      /* pfp->prev = TAIL */
      pfp->next = NULL;              /* pfp is now going to be at the tail! */
      pfpInst.tail->next = pfp;      /* TAIL->next = pfp (old tail points to pfp now) */
      pfpInst.tail = pfp;            /* new tail is pfp finally */
    }
    pfp->c_0          = Timestamp_get(); /* Record time */
  }
  Hwi_restore(key);
} /* pfpTaskSwitch */

/*============================================================================*/
/* pfpSwiRegister: Swi registration hook. Stores hook set ID.                 */
/*============================================================================*/

void pfpSwiRegister(int hookSetId)
{
#if PFP_DBG_HOOKS
  System_printf("S-Register: id=%d\n", hookSetId);
  pfpSwiHookSetId = hookSetId;
#endif
} /* pfpSwiRegister */

/*============================================================================*/
/* pfpSwiCreate: Swi creation hook. Sets the hook set context.                */
/*============================================================================*/

void pfpSwiCreate(Swi_Handle swi, Error_Block *eb)
{
#if PFP_DBG_HOOKS
  char *name;
  void *pEnv;

  name = Swi_Handle_name(swi);
  pEnv = Swi_getHookContext(swi, pfpSwiHookSetId);  /* We have set the hook set ID during registration */

  System_printf("S-Create: name='%s', pEnv=0x%x, time=%d\n", name, pEnv, Timestamp_get());
  Swi_setHookContext(swi, pfpSwiHookSetId, (void*)0xdead1);
#endif
} /* pfpSwiCreate */

/*============================================================================*/
/* pfpSwiReady: Swi ready hook. Prepares Swi for run.                         */
/*============================================================================*/

void pfpSwiReady(Swi_Handle swi)
{
#if PFP_DBG_HOOKS
  char *name;
  void *pEnv;

  name = Swi_Handle_name(swi);
  pEnv = Swi_getHookContext(swi, pfpSwiHookSetId);  /* We have set the hook set ID during registration */

  System_printf("S-Ready: name='%s', pEnv=0x%x, time=%d\n", name, pEnv, Timestamp_get());
  Swi_setHookContext(swi, pfpSwiHookSetId, (void*)0xbeef1);
#endif
} /* pfpSwiReady */

/*============================================================================*/
/* pfpSwiBegin: Swi begin hook. Start Swi.                                    */
/*============================================================================*/

void pfpSwiBegin(Swi_Handle swi)
{
#if PFP_DBG_HOOKS
  char *name;
  void *pEnv;
#endif

  pfpSwiPause();

#if PFP_DBG_HOOKS
  name = Swi_Handle_name(swi);
  pEnv = Swi_getHookContext(swi, pfpSwiHookSetId);  /* We have set the hook set ID during registration */

  System_printf("S-Begin: name='%s', pEnv=0x%x, time=%d\n", name, pEnv, Timestamp_get());
  Swi_setHookContext(swi, pfpSwiHookSetId, (void*)0xfeeb1);
#endif
} /* pfpSwiBegin */

/*============================================================================*/
/* pfpSwiEnd: Swi end hook. End Swi.                                          */
/*============================================================================*/

void pfpSwiEnd(Swi_Handle swi)
{
#if PFP_DBG_HOOKS
  char *name;
  void *pEnv;

  name = Swi_Handle_name(swi);
  pEnv = Swi_getHookContext(swi, pfpSwiHookSetId);  /* We have set the hook set ID during registration */

  System_printf("S-End: name='%s', pEnv=0x%x, time=%d\n", name, pEnv, Timestamp_get());
  Swi_setHookContext(swi, pfpSwiHookSetId, (void*)0xc0de1);
#endif

  pfpSwiResume(swi);

} /* pfpSwiEnd */

/*============================================================================*/
/* pfpHwiRegister: Hwi registration hook. Stores hook set ID.                 */
/*============================================================================*/

void pfpHwiRegister(int hookSetId)
{
#if PFP_DBG_HOOKS
  System_printf("H-Register: id=%d\n", hookSetId);
  pfpHwiHookSetId = hookSetId;
#endif
} /* pfpHwiRegister */

/*============================================================================*/
/* pfpHwiCreate: Hwi creation hook. Sets the hook set context.                */
/*============================================================================*/

void pfpHwiCreate(Hwi_Handle hwi, Error_Block *eb)
{
#if PFP_DBG_HOOKS
  char *name;
  void *pEnv;

  name = Hwi_Handle_name(hwi);
  pEnv = Hwi_getHookContext(hwi, pfpHwiHookSetId);  /* We have set the hook set ID during registration */

  System_printf("H-Create: name='%s', pEnv=0x%x, time=%d\n", name, pEnv, Timestamp_get());
  Hwi_setHookContext(hwi, pfpHwiHookSetId, (void*)0xdead2);
#endif
} /* pfpHwiCreate */

/*============================================================================*/
/* pfpHwiBegin: Hwi begin hook. Start Hwi.                                    */
/*============================================================================*/

void pfpHwiBegin(Hwi_Handle hwi)
{
#if PFP_DBG_HOOKS
  char *name;
  void *pEnv;
#endif

  pfpHwiPause();

#if PFP_DBG_HOOKS
  name = Hwi_Handle_name(hwi);
  pEnv = Hwi_getHookContext(hwi, pfpHwiHookSetId);  /* We have set the hook set ID during registration */

  System_printf("H-Begin: name='%s', pEnv=0x%x, time=%d\n", name, pEnv, Timestamp_get());
  Hwi_setHookContext(hwi, pfpHwiHookSetId, (void*)0xfeeb2);
#endif
} /* pfpHwiBegin */

/*============================================================================*/
/* pfpHwiEnd: Hwi end hook. End Hwi.                                          */
/*============================================================================*/

void pfpHwiEnd(Hwi_Handle hwi)
{
#if PFP_DBG_HOOKS
  char *name;
  void *pEnv;

  name = Hwi_Handle_name(hwi);
  pEnv = Hwi_getHookContext(hwi, pfpHwiHookSetId);  /* We have set the hook set ID during registration */

  System_printf("H-End: name='%s', pEnv=0x%x, time=%d\n", name, pEnv, Timestamp_get());
  Hwi_setHookContext(hwi, pfpHwiHookSetId, (void*)0xc0de2);
#endif

  pfpHwiResume(hwi);

} /* pfpHwiEnd */

/* nothing past this point */

