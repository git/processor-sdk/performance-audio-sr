
/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// Framework -- Configuration Declarations
//

#ifndef _ASP_CONFIG_H_
#define _ASP_CONFIG_H_

#include <xdc/std.h>
//#include <ti/procsdk_audio/procsdk_audio_typ.h>
#include <procsdk_audio_typ.h>

#include "alg.h"
#include <sio.h>

#include <acp.h>

#include <paftyp.h>
#include <inpbuf.h>
#include <pafdec.h>
#include <pafenc.h>
#include <pafvol.h>
#include <outbuf.h>

#include "as1-f2.h"
#include "asp0.h"

#include "aspDecOpCircBuf_common.h"
#include "aspOutInitSync_common.h"

#ifdef HSE_MSGQ
#include <msgq.h>
#include <pool.h>
#include <staticpool.h>
#endif

//
// Audio Framework Configuration
//

typedef struct PAF_AST_InpBuf {
    SIO_Handle          hRxSio;
    PAF_InpBufConfig    inpBufConfig;
    PAF_InpBufConfig   *pInpBuf; /* Decode Processing Temporary */
    PAF_InpBufStatus    inpBufStatus;
    PAF_ASP_Chain       inpChainData;
} PAF_AST_InpBuf;

typedef struct PAF_AST_Decode {
    PAF_DecodeControl   decodeControl;
    PAF_DecodeStatus    decodeStatus;
    PAF_DecodeInStruct  decodeInStruct;
    PAF_DecodeOutStruct decodeOutStruct;
    PAF_ASP_Chain       decChainData;
    ALG_Handle          decAlg[PAF_SOURCE_N];
    PAF_AudioFrame     *pAudioFrame;
} PAF_AST_Decode;

typedef struct PAF_AST_Stream {
    PAF_AudioData      *audioFrameChannelPointers[PAF_MAXNUMCHAN_AF];
    PAF_AudioSize       audioFrameChannelSizes[PAF_MAXNUMCHAN_AF];
    PAF_AudioFrame     *pAudioFrame;
    PAF_ASP_Chain       aspChainData[GEARS];
    PAF_ASP_Chain      *aspChain[GEARS];
    PAF_AudioData      *origAudioFrameChannelPointers[PAF_MAXNUMCHAN_AF];
} PAF_AST_Stream;

typedef struct PAF_AST_OutBuf {
    SIO_Handle          hTxSio;
    PAF_OutBufConfig    outBufConfig;
    PAF_OutBufConfig   *pOutBuf; /* Decode Processing Temporary */
    PAF_OutBufStatus    outBufStatus;
    PAF_ASP_Chain       outChainData;
} PAF_AST_OutBuf;

typedef struct PAF_AST_Encode {
    PAF_EncodeControl   encodeControl;
    PAF_EncodeStatus    encodeStatus;
    PAF_VolumeStatus    volumeStatus;
    PAF_EncodeInStruct  encodeInStruct;
    PAF_EncodeOutStruct encodeOutStruct;
    PAF_ASP_Chain       encChainData;
    ALG_Handle          encAlg[PAF_SOURCE_N];
} PAF_AST_Encode;

#ifdef HSE_MSGQ
typedef struct PAF_AST_MQ {
    MSGQ_Queue rx;
    MSGQ_Queue reply;
} PAF_AST_MQ;
#endif

typedef struct PAF_AST_Config {
    Int                     as;
    PAF_AST_InpBuf          *xInp;      /* INPUT1 -INPUTN  */
    PAF_AST_Decode          *xDec;      /* DECODE1-DECODEN */
    PAF_AST_Stream          *xStr;      /* STREAM1-STREAMN */
    PAF_AST_Encode          *xEnc;      /* ENCODE1-ENCODEN */
    PAF_AST_OutBuf          *xOut;      /* OUTPUT1-OUTPUTN */
    PAF_AST_DecOpCircBuf    *xDecOpCb;  /* DECODE1-DECODEN */
    PAF_AST_OutInitSyncInfo *xOutIsi;   /* DECODE1-DECODEN */
    SmInt                   masterDec;  // these two are computed based on MASTER input
    SmInt                   masterStr;
#ifdef HSE_MSGQ
    PAF_AST_MQ              mq;
#endif
} PAF_AST_Config;


#endif  /* _ASP_CONFIG_H_ */
