
/*
Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== fwkPort.h ========
 */


#ifndef _FWK_PORT_H_
#define _FWK_PORT_H_

#include <xdc/std.h>
#include <xdc/runtime/LoggerBuf.h>
#include <ti/xdais/xdas.h>

// ------------------------------------- //
// Below is temporary for framework port //
// ------------------------------------- //
// SYS/BIOS 6.45 LOG APIs are different than BIOS 6.21
// log.h -- replacements
#define LOG_Obj LoggerBuf_Struct
#define LOG_disable(a)
#define LOG_printf(...)

// Not defined in SYS/BIOS 6.45
// sys.h -- extracted defs
#define SYS_OK              0   /* no error */ // packages/ti/bios/include/sys.h
#define SYS_EALLOC          1   /* memory allocation error */ // packages/ti/bios/include/sys.h

// 1st parameter for audioStream1Task().
// SYS/BIOS 6.45 doesn't allow more than 2 parameters for a task.
extern Int gBetaPrimeValue;

//extern LOG_Obj trace;

#endif /* _FWK_PORT_H_ */
