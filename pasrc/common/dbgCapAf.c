
/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <xdc/std.h>

#include "dbgCapAf.h"

#ifdef DBG_CAP_AF
#ifdef _TMS320C6X
#pragma DATA_SECTION(gCapAfBuf, ".gCapAfBuf");
PAF_AudioData gCapAfBuf[CAP_AF_MAX_NUM_SAMP];
#else
PAF_AudioData gCapAfBuf[CAP_AF_MAX_NUM_SAMP] __attribute__ ((section(".gCapAfBuf")));
//PAF_AudioData gCapAfBuf[CAP_AF_MAX_NUM_SAMP] __attribute__ ((section(".noinit")));
#endif
Int32 gCapAfBufIdx=0;
Int32 gCapAfBufWrapCnt=0;
#endif // DBG_CAP_AF

#ifdef DBG_CAP_AF
// Reset audio frame capture buffer
Int capAfReset(Void)
{
    gCapAfBufIdx = 0;
    gCapAfBufWrapCnt = 0;

    return 0;
}

// Write audio frame to capture buffer
Int capAfWrite(
    PAF_AudioFrame *pAf,    // audio frame
    Int8 chNum              // channel number to capture
)
{
    Int16 nSamp;
    PAF_AudioData *pSamp;
    Int16 i;
    
    if (chNum > (pAf->data.nChannels-1))
    {
        return CAP_AF_INV_CHNUM;
    }
    
    nSamp = pAf->sampleCount; //pAf->data.nSamples;
    pSamp = pAf->data.sample[chNum];

    if ((CAP_AF_MAX_NUM_SAMP - gCapAfBufIdx) < nSamp)
    {
        //return;
        gCapAfBufIdx = 0;
        gCapAfBufWrapCnt++;
    }
    
    for (i=0; i<nSamp; i++)
    {
        gCapAfBuf[gCapAfBufIdx] = *pSamp;
        pSamp++;
        gCapAfBufIdx++;
    }
    
    return CAP_AF_SOK;
}
#endif // DBG_CAP_AF
