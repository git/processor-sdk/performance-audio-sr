
/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _ASP_OUT_INIT_SYNC_H_
#define _ASP_OUT_INIT_SYNC_H_

#include <xdc/std.h>
#include <ti/ipc/GateMP.h>
#include "paftyp.h"

// error return codes
#define ASP_OUTIS_SOK                       ( 0 )                               // ok
#define ASP_OUTIS_CTL_INIT_INV_GATE         ( ASP_OUTIS_SOK-1 )                 // error: invalid gate handle
#define ASP_OUTIS_ERR_START                 ( ASP_OUTIS_CTL_INIT_INV_GATE )     // start error return ID for master & slave error return definitions

// Output Init-Sync number of decoder stages
// 2 stages: Dec Reset, Dec Info
#define ASP_OUTIS_NUM_DEC_STAGES            ( 3 )

// Output Init-Sync decoder stages indices
#define ASP_OUTIS_DEC_STAGE_RESET_IDX       ( 0 )
#define ASP_OUTIS_DEC_STAGE_INFO1_IDX       ( ASP_OUTIS_DEC_STAGE_RESET_IDX+1 )
#define ASP_OUTIS_DEC_STAGE_DECODE1_IDX     ( ASP_OUTIS_DEC_STAGE_INFO1_IDX+1 )

#define ASP_OUTIS_GATE_NAME                 ( "AspOutISGate" )  // name of GateMP used OutIS info protection
#define ASP_OUTIS_GATE_REGION_ID            ( 0 )               // IPC shared region ID used for OutIS gate allocation   

// Decoder Stage Output Processing Init-Sync Information    
typedef struct PAF_AST_DecStageOutInitSyncInfo 
{
    Int8 decFlag;           // decoder stage flag: 0:dec stage not executed; 1:dec stage executed
    PAF_AudioFrame decAf;   // decoder output audio frame
} PAF_AST_DecStageOutInitSyncInfo;

// Output Processing Init-Sync Information
typedef struct PAF_AST_OutInitSyncInfo
{
    PAF_AST_DecStageOutInitSyncInfo decStageOutInitSyncInfo[ASP_OUTIS_NUM_DEC_STAGES];
} PAF_AST_OutInitSyncInfo;

// Output Processing Init-Sync control
typedef struct PAF_AST_OutInitSyncCtl
{
    GateMP_Handle gateHandle;                   // output init-sync gate handle
    PAF_AST_OutInitSyncInfo **pXOutIsInfo;      // address of output init-sync info base pointer
} PAF_AST_OutInitSyncCtl;

// Initialize Output Processing Init-Sync control
Int outIsCtlInit(
    PAF_AST_OutInitSyncCtl *pOutIsCtl,          // output init-sync control to initialize
    PAF_AST_OutInitSyncInfo **pXOutIsInfo       // pointer to base address of output init-sync info array
);

// Copy audio frame
// Note similar functionality present in CB, check usage there.
Void outIsCpyAf(
    PAF_AudioFrame *pSrcAf,
    PAF_AudioFrame *pDstAf
);

#endif /* _ASP_OUT_INIT_SYNC_H_ */
