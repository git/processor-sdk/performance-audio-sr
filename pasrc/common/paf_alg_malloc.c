
/*
Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// Algorithm Memory Allocation
//

/*
 * File Inclusions 
 */

#include <xdc/std.h>
#include <xdc/cfg/global.h>

//#include <mem.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/IHeap.h>
#include <ti/sysbios/heaps/HeapMem.h>

//#include <stdlib.h>
//#include <string.h>

#include "alg.h"

//#include "pafhjt.h"
#include "paf_alg.h"
#include "paf_alg_priv.h"

#include "paf_ialg.h"

#include "common.h"
#include "paf_heapMgr.h"

//extern int IRAM;
//extern int SDRAM;
//extern int L3RAM;


// -----------------------------------------------------------------------------
// Debugging Trace Control, local to this file.
//

#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */

#include <logp.h>

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.
// For release, set mask to 1 to make it easier to catch any errors.
#define CURRENT_TRACE_MASK  0   // terse only

#define TRACE_MASK_TERSE    1   // only flag errors
#define TRACE_MASK_GENERAL  2   // log a half dozen lines per loop
#define TRACE_MASK_VERBOSE  4   // trace full operation

#if (CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
 #define TRACE_TERSE(a) LOG_printf a
#else
 #define TRACE_TERSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
 #define TRACE_GEN(a) LOG_printf a
#else
 #define TRACE_GEN(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
 #define TRACE_VERBOSE(a) LOG_printf a
#else
 #define TRACE_VERBOSE(a)
#endif

// Note:  memory spaces are defined in ialg.h from the xdais package.
// 17 is external.  2 and 5 are internal.

/*
 * The PAF_ALG_allocMemory is derived from _ALG_allocMemory function defined 
 * in the TI Standard XDAIS file alg_malloc.c version "XDAS 2.2.1 12-07-01"
 * It is extended to check for the Performance Audio specific "Common"
 * memory. This function assumes that memory is already
 * allocated for the common memory and shared scratch memory.
 */

Int 
PAF_ALG_allocMemory_ (
    IALG_MemRec memTab[], 
    Int n, 
    const IALG_MemRec common[],
    PAF_IALG_Config *p)
{
    char * commonBase[PAF_IALG_COMMON_MEMN+1];
    Int commonCount[PAF_IALG_COMMON_MEMN+1];
    Int i;
    Error_Block eb;

    // Initialize error block
    Error_init(&eb); 

    if(common) {
#ifdef _TMS320C6X
//#warn This pragma saves ~125 bytes! --Kurt
#pragma UNROLL(1)
#endif
        for(i=PAF_IALG_COMMON_MEM0;i<=PAF_IALG_COMMON_MEMN;++i) {
            commonBase[i]=common[i].base;
            commonCount[i]=0;
        }
    }
    
    for(i=0;i<n;++i) {
        if( common && 
           (memTab[i].attrs == PAF_IALG_COMMON_MEM0 ||
            (memTab[i].attrs >= PAF_IALG_COMMON_MEM1 &&
            memTab[i].attrs <= PAF_IALG_COMMON_MEMN))) {
            memTab[i].base = commonBase[memTab[i].attrs] + 
                             (commonCount[memTab[i].attrs] ? memTab[i].alignment : 0);
            memTab[i].base = (void *)((unsigned int )memTab[i].base & 
                             (~(memTab[i].alignment ? memTab[i].alignment - 1 : 0)));
            commonBase[memTab[i].attrs] = (char *)memTab[i].base + memTab[i].size;
            commonCount[memTab[i].attrs]++;
            TRACE_GEN((&TR_MOD, "PAF_ALG_allocMemory_.%d: memTab alloc %d bytes at 0x%x.", __LINE__, memTab[i].size, memTab[i].base));
            TRACE_GEN((&TR_MOD, "PAF_ALG_allocMemory_ (cont'd) memTab %d. i: %d", memTab[i].attrs, i));
        }
        else {
            if(memTab[i].size){
                if(p) {
                    if(p->clr){
                        TRACE_GEN((&TR_MOD, "PAF_ALG_allocMemory_.%d: calloc %d bytes from space %d", __LINE__, memTab[i].size, memTab[i].space));
                        //if(!(memTab[i].base = (void *)MEM_calloc(
                        //    PAF_ALG_memSpace(p,memTab[i].space),
                        //    memTab[i].size,memTab[i].alignment)))
                        if (!(memTab[i].base = (void *)Memory_calloc(
                            (IHeap_Handle)PAF_ALG_memSpaceToHeap(p,memTab[i].space),
                            memTab[i].size,
                            memTab[i].alignment,
                            &eb)))
                        {
                            TRACE_TERSE((&TR_MOD, "PAF_ALG_allocMemory_.%d: calloc %d bytes failed.", __LINE__, memTab[i].size));
                            SW_BREAKPOINT;
                            return PAF_ALGERR_PERSIST;
                        }
                        TRACE_GEN((&TR_MOD, "PAF_ALG_allocMemory_ (cont'd) %d at 0x%x, %d",
                                       memTab[i].size, memTab[i].base, PAF_ALG_memSpace(p,memTab[i].space)));
                    }
                    else{
                        TRACE_GEN((&TR_MOD, "PAF_ALG_allocMemory_.%d: alloc %d bytes from space %d", __LINE__, memTab[i].size, memTab[i].space));
                        //if(!(memTab[i].base = (void *)MEM_alloc(
                        //    PAF_ALG_memSpace(p,memTab[i].space),
                        //    memTab[i].size,memTab[i].alignment)))
                        if (!(memTab[i].base = (void *)Memory_alloc(
                            (IHeap_Handle)PAF_ALG_memSpaceToHeap(p, memTab[i].space),
                            memTab[i].size, 
                            memTab[i].alignment, 
                            &eb)))
                        {
                            TRACE_TERSE((&TR_MOD, "PAF_ALG_allocMemory_.%d: alloc %d bytes failed.", __LINE__, memTab[i].size));
                            SW_BREAKPOINT;
                            return PAF_ALGERR_PERSIST;
                        }
                        TRACE_GEN((&TR_MOD, "PAF_ALG_allocMemory_ (cont'd) %d at 0x%x, %d",
                                       memTab[i].size, memTab[i].base, PAF_ALG_memSpace(p,memTab[i].space)));
                    }
                }
                else {
                    PAF_IALG_Config pafConfig;
                    TRACE_GEN((&TR_MOD, "PAF_ALG_allocMemory_.%d: Setup memory regions.", __LINE__));
                    //PAF_ALG_setup(&pafConfig,IRAM,SDRAM,IRAM,0); // option: use L3RAM
                    PAF_ALG_setup(&pafConfig,
                            gPafHeapIdInt,               (HeapMem_Handle)pafHeapMgr_readHeapHandle(gPafHeapIdInt),
                            gPafHeapIdInt1,              (HeapMem_Handle)pafHeapMgr_readHeapHandle(gPafHeapIdInt1),
                            gPafHeapIdExt,               (HeapMem_Handle)pafHeapMgr_readHeapHandle(gPafHeapIdExt),
                            gPafHeapIdInt1Shm,           (HeapMem_Handle)pafHeapMgr_readHeapHandle(gPafHeapIdInt1Shm),
                            gPafHeapIdExtShm,            (HeapMem_Handle)pafHeapMgr_readHeapHandle(gPafHeapIdExtShm),
                            gPafHeapIdExtNonCachedShm,   (HeapMem_Handle)pafHeapMgr_readHeapHandle(gPafHeapIdExtNonCachedShm),
                            0);
                    TRACE_GEN((&TR_MOD, "PAF_ALG_allocMemory_.%d: alloc %d bytes from space %d", __LINE__, memTab[i].size, memTab[i].space));
                    //if(!(memTab[i].base=(void *)MEM_alloc(
                    //    PAF_ALG_memSpace(&pafConfig,memTab[i].space),
                    //    memTab[i].size,memTab[i].alignment)))
                    if (!(memTab[i].base = (void *)Memory_alloc(
                        (IHeap_Handle)PAF_ALG_memSpaceToHeap(&pafConfig, memTab[i].space),
                        memTab[i].size,
                        memTab[i].alignment, 
                        &eb)))
                    {
                        TRACE_TERSE((&TR_MOD, "PAF_ALG_allocMemory_.%d: alloc %d bytes failed.",__LINE__, memTab[i].size));
                        SW_BREAKPOINT;
                        return PAF_ALGERR_PERSIST;
                    }
                    TRACE_GEN((&TR_MOD, "PAF_ALG_allocMemory_ (cont'd) %d at 0x%x",
                                   memTab[i].size, memTab[i].base, PAF_ALG_memSpace(&pafConfig,memTab[i].space)));
                }
            }
            else
            {
                memTab[i].base=NULL;
                TRACE_TERSE((&TR_MOD, "PAF_ALG_allocMemory_.%d: alloc %d bytes failed.", __LINE__, memTab[i].size));
                SW_BREAKPOINT;
            }
        }
    }
    return 0;
}

/*
 * The PAF_ALG_mallocMemory function allocates the memory requested by the 
 * memTab's.
 */

Int
PAF_ALG_mallocMemory_ (
    IALG_MemRec common[], 
    PAF_IALG_Config *p)
{
    Int i;
    Error_Block eb;
    
    // Initialize error block
    Error_init(&eb);
    
    for(i=PAF_IALG_COMMON_MEM0;i<=PAF_IALG_COMMON_MEMN;++i)
    {
        if(0 == common[i].size)
            continue;

        if(p->clr)
        {
            TRACE_GEN((&TR_MOD, "PAF_ALG_mallocMemory_.%d: calloc %d bytes from space %d", __LINE__, common[i].size, common[i].space));
            //common[i].base = (void *)MEM_calloc( PAF_ALG_memSpace(p,common[i].space), common[i].size, common[i].alignment);
            common[i].base = (void *)Memory_calloc((IHeap_Handle)PAF_ALG_memSpaceToHeap(p,common[i].space), common[i].size, common[i].alignment, &eb);
            TRACE_GEN((&TR_MOD, "PAF_ALG_allocMemory_ (cont'd) %d at 0x%x", common[i].size, common[i].base));
        }
        else
        {
            TRACE_GEN((&TR_MOD, "PAF_ALG_mallocMemory_.%d: alloc %d bytes from space %d", __LINE__, common[i].size, common[i].space));
            //common[i].base = (void *)MEM_alloc(PAF_ALG_memSpace(p, common[i].space),common[i].size, common[i].alignment);
            common[i].base = (void *)Memory_alloc((IHeap_Handle)PAF_ALG_memSpaceToHeap(p, common[i].space), common[i].size, common[i].alignment, &eb);
            TRACE_GEN((&TR_MOD, "PAF_ALG_allocMemory_ (cont'd) %d at 0x%x", common[i].size, common[i].base));
        }

        if(!(common[i].base))
        {
            TRACE_TERSE((&TR_MOD, "PAF_ALG_mallocMemory_.%d: (c)alloc %d bytes failed", __LINE__, common[i].size));
            SW_BREAKPOINT;
            return PAF_ALGERR_COMMON;
        }
    }

    return 0;
}

/*
 * The PAF_ALG_freeMemory is derived from _ALG_freeMemory function defined 
 * in the TI Standard XDAIS file alg_malloc.c version "XDAS 2.2.1 12-07-01"
 * It is extended to check for the Performance Audio specific "Common"
 * memory and scratch memory.
 */

Void 
PAF_ALG_freeMemory(
    IALG_MemRec memTab[], 
    Int n)
{

#ifdef _TMS320C6X
//#warn Cannot free memory here because segids are not known
#endif

    // Cannot free memory here bacause there is no way to determine
    // the segid needed by MEM_free.
    // Not an issuue as of now, in PA, because, if alg creation fails
    // system does not do any thing further.
    // 
#if 0
    Int i;
    Int segid;

    for(i=0;i<n;i++){
        if(memTab[i].base != NULL && !(memTab[i].attrs == PAF_IALG_COMMON_MEM0 ||
           memTab[i].attrs >= PAF_IALG_COMMON_MEM1 && memTab[i].attrs <= PAF_IALG_COMMON_MEMN))
        {
#warn MEM_free might be 
            segid = (memTab[i].space == IALG_SARAM)?IRAM:SDRAM;
            MEM_free(segid,memTab[i].base,memTab[i].size);
        }
    }
#endif
}

/* 
 * The PAF_ALG_init function initializes the memTab's to default values.
 */

Void 
PAF_ALG_init_ (
    IALG_MemRec memTab[], 
    Int n, 
    const IALG_MemSpace memSpace[])
{
    Int i;

    for (i=0; i<n; i++) {
        memTab[i].size = 0;
        memTab[i].alignment = 0;
        memTab[i].base = NULL;
        memTab[i].space = memSpace ? memSpace[i] : PAF_IALG_NONE;
        // TRACE_GEN((&TR_MOD, "PAF_ALG_init_[%d]:space %d", i, memTab[i].space));
    }
}

/*
 *  PAF_ALG_activate is derived from standard XDAIS ALG_activate function defined in the
 *  file alg_malloc.c version "XDAS 2.2.1 12-07-01".
 */

#ifdef _TMS320C6X
/* DO NOT REMOVE THIS CODE_SECTION. --Kurt */
#pragma CODE_SECTION(PAF_ALG_activate,".text:_PAF_ALG_activate")
#endif

Void
PAF_ALG_activate (
    ALG_Handle alg)
{
#if defined(_TMS320C6X) && !defined(__TI_EABI__)
    asm (" .clink");
#endif    

    if (alg->fxns->algActivate != NULL) {
        alg->fxns->algActivate (alg);
    }
}

/*
 *  PAF_ALG_deactivate is derived from standard XDAIS ALG_deactivate function defined in the
 *  file alg_malloc.c version "XDAS 2.2.1 12-07-01".
 */

#ifdef _TMS320C6X
/* DO NOT REMOVE THIS CODE_SECTION. --Kurt */
#pragma CODE_SECTION(PAF_ALG_deactivate,".text:_PAF_ALG_deactivate")
#endif

Void 
PAF_ALG_deactivate (
    ALG_Handle alg)
{
#if defined(_TMS320C6X) && !defined(__TI_EABI__)
    asm (" .clink");
#endif

    if (alg->fxns->algDeactivate != NULL) {
        alg->fxns->algDeactivate(alg);
    }   
}

/* 
 * The PAF_ALG_memSpaceToHeapId function is derived from ALGRF_memSpace function 
 * defined in the TI released algrf.h file ver "ALGRF 0.02.06 11-21-01".
 */

Int
PAF_ALG_memSpaceToHeapId_ (
    const PAF_IALG_Config *p, 
    IALG_MemSpace space)
{
    switch(space)
    {
        case IALG_SARAM:    // PASDK: DSP L2
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeapId_.%d: IALG_SARAM (0x%x) 0x%x", __LINE__, space, p->iHeapId));
            return p->iHeapId;

        case IALG_EXTERNAL: // PASDK: DDR local memory
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeapId_.%d: IALG_EXTERNAL (0x%x) 0x%x", __LINE__, space, p->eHeapId));
            return p->eHeapId;

        case IALG_SARAM1:   // PASDK: MSMC local memory
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeapId_.%d: IALG_SARAM1 (0x%x) 0x%x", __LINE__, space, p->lHeapId));
            return p->lHeapId;

        case IALG_SARAM2:   // PASDK: MSMC shared memory
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeapId_.%d: IALG_SARAM2 (IALG_SARAM_SHM) (0x%x) 0x%x", __LINE__, space, p->lHeapIdShm));
            return p->lHeapIdShm;

        case IALG_DARAM0:   // PASDK: not used
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeapId_.%d: IALG_DARAM0 (0x%x) 0x%x", __LINE__, space, p->iHeapId));
            return p->iHeapId;

        case IALG_DARAM1:   // PASDK: not used
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeapId_.%d: IALG_DARAM1 (0x%x) 0x%x", __LINE__, space, p->iHeapId));
            return p->iHeapId;

        case IALG_DARAM2:   // PASDK: not used
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeapId_.%d: IALG_DARAM2 (0x%x) 0x%x", __LINE__, space, p->iHeapId));
            return p->iHeapId;    // not normally used.

        case IALG_ESDATA:   // PASDK: DDR shared memory
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeapId_.%d: IALG_ESDATA (IALG_EXTERNAL_SHM) (0x%x) 0x%x", __LINE__, space, p->eHeapIdShm));
            return p->eHeapIdShm;

        case IALG_ENCSDATA: // PASDK: DDR shared non-cacheable memory
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeapId_.%d: IALG_ENCSDATA (PAF_HEAP_ID_EXT_NON_CACHED_SHM) (0x%x) 0x%x", __LINE__, space, p->eHeapIdNCShm));
            return p->eHeapIdNCShm;

        default:
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeapId_.%d: default (0x%x) 0x%x", __LINE__, space, p->eHeapId));
            return p->eHeapId;
    }
 
}

HeapMem_Handle
PAF_ALG_memSpaceToHeap_ (
    const PAF_IALG_Config *p, 
    IALG_MemSpace space)
{
    switch(space)
    {
        case IALG_SARAM:    // PASDK: DSP L2
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeap_.%d: IALG_SARAM (0x%x) 0x%x", __LINE__, space, p->iHeapId));
            return p->hIHeap;

        case IALG_EXTERNAL: // PASDK: DDR local memory
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeap_.%d: IALG_EXTERNAL (0x%x) 0x%x", __LINE__, space, p->eHeapId));
            return p->hEHeap;

        case IALG_SARAM1:   // PASDK: MSMC local memory
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeap_.%d: IALG_SARAM1 (0x%x) 0x%x", __LINE__, space, p->lHeapId));
            return p->hLHeap;

        case IALG_SARAM2:
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeap_.%d: IALG_SARAM2 (IALG_SARAM_SHM) (0x%x) 0x%x", __LINE__, space, p->lHeapIdShm));
            return p->hLHeapShm;

        case IALG_DARAM0:   // PASDK: not used
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeap_.%d: IALG_DARAM0 (0x%x) 0x%x", __LINE__, space, p->iHeapId));
            return p->hIHeap;

        case IALG_DARAM1:   // PASDK: not used
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeap_.%d: IALG_DARAM1 (0x%x) 0x%x", __LINE__, space, p->iHeapId));
            return p->hIHeap;

        case IALG_DARAM2:   // PASDK: not used
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeap_.%d: IALG_DARAM2 (0x%x) 0x%x", __LINE__, space, p->iHeapId));
            return p->hIHeap;    // not normally used.

        case IALG_ESDATA:   // PASDK: DDR shared memory
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeap_.%d: IALG_ESDATA (0x%x) 0x%x", __LINE__, space, p->eHeapIdShm));
            return p->hEHeapShm;

        case IALG_ENCSDATA: // PASDK: DDR shared non-cacheable memory
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeap_.%d: IALG_ENCSDATA (0x%x) 0x%x", __LINE__, space, p->eHeapIdNCShm));
            return p->hEHeapNCShm;

        default:
            TRACE_GEN((&TR_MOD, "PAF_ALG_memSpaceToHeap_.%d: default (0x%x) 0x%x", __LINE__, space, p->eHeapId));
            return p->hEHeap;
    } 
}

/* 
 * The PAF_ALG_setup function is derived from ALGRF_setup function defined in
 * the TI released algrf_setup.c file ver "ALGRF 0.02.06 11-21-01" .
 */
    
#ifdef _TMS320C6X
#pragma CODE_SECTION(PAF_ALG_setup_,".text:_PAF_ALG_setup_")
#endif
Void
PAF_ALG_setup_ (
    PAF_IALG_Config *p, 
    Int iHeapId, 
    HeapMem_Handle hIHeap,
    Int lHeapId,
    HeapMem_Handle hLHeap,
    Int eHeapId,
    HeapMem_Handle hEHeap,
    Int lHeapIdShm,
    HeapMem_Handle hLHeapShm,
    Int eHeapIdShm,
    HeapMem_Handle hEHeapShm,
    Int eHeapIdNCShm,
    HeapMem_Handle hEHeapNCShm,
    Int clr
)
{ 
#if defined(_TMS320C6X) && !defined(__TI_EABI__)
    asm (" .clink");
#endif    
    
    /* initialize heap Ids */
    p->iHeapId        = iHeapId; 
    p->lHeapId        = lHeapId;
    p->eHeapId        = eHeapId;
    p->lHeapIdShm     = lHeapIdShm;
    p->eHeapIdShm     = eHeapIdShm;
    p->eHeapIdNCShm   = eHeapIdNCShm;
    
    /* initialize heap handles */
    p->hIHeap         = hIHeap;
    p->hLHeap         = hLHeap;
    p->hEHeap         = hEHeap;
    p->hLHeapShm      = hLHeapShm;
    p->hEHeapShm      = hEHeapShm;
    p->hEHeapNCShm    = hEHeapNCShm;
    
    /* initialize clear flag */
    p->clr=clr;
    
    // TRACE_GEN((&TR_MOD, "PAF_ALG_memSpace_.%d: IPAF_ALG_setup_", __LINE__));
}
