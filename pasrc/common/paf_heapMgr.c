
/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <xdc/std.h>
#include <xdc/runtime/IHeap.h>

#include "paf_heapMgr.h"

Int gPafHeapIdInt             = PAF_HEAP_ID_INT;                // L2 SRAM
Int gPafHeapIdInt1            = PAF_HEAP_ID_INT1;               // MSMC SRAM
Int gPafHeapIdExt             = PAF_HEAP_ID_EXT;                // DDR3
Int gPafHeapIdInt1Shm         = PAF_HEAP_ID_INT1_SHM;           // MSMC SRAM, Shared
Int gPafHeapIdExtShm          = PAF_HEAP_ID_EXT_SHM;            // DDR3, Shared
Int gPafHeapIdExtNonCachedShm = PAF_HEAP_ID_EXT_NON_CACHED_SHM; // DDR3 non-cached, Shared

// heap handle array
//static IHeap_Handle gHeapIdToHandle[PAF_NUM_HEAPS] =
IHeap_Handle gHeapIdToHandle[PAF_NUM_HEAPS] =
{
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

/* Initialize PAF heap manager */
Void pafHeapMgr_init(
    IHeap_Handle hIntHeap,
    IHeap_Handle hIntHeap1,
    IHeap_Handle hExtHeap,
    IHeap_Handle hIntHeap1Shm,
    IHeap_Handle hExtHeapShm,
    IHeap_Handle hExtHeapNonCachedShm
)
{
    gHeapIdToHandle[PAF_HEAP_ID_INT]                = hIntHeap;
    gHeapIdToHandle[PAF_HEAP_ID_INT1]               = hIntHeap1;
    gHeapIdToHandle[PAF_HEAP_ID_EXT]                = hExtHeap;
    gHeapIdToHandle[PAF_HEAP_ID_INT1_SHM]           = hIntHeap1Shm;
    gHeapIdToHandle[PAF_HEAP_ID_EXT_SHM]            = hExtHeapShm;
    gHeapIdToHandle[PAF_HEAP_ID_EXT_NON_CACHED_SHM] = hExtHeapNonCachedShm;
}

/* Write heap handle to PAF heap manager for provided index */
Void pafHeapMgr_writeHeapHandle(
    Int heapId,
    IHeap_Handle hHeap
)
{
    if (heapId < PAF_NUM_HEAPS)
    {
        gHeapIdToHandle[heapId] = hHeap;
    }
}

/* Read heap handle from PAF heap manager for provided index */
IHeap_Handle pafHeapMgr_readHeapHandle(
    Int heapId
)
{
    IHeap_Handle hHeap;
    
    if (heapId < PAF_NUM_HEAPS)
    {
        hHeap = gHeapIdToHandle[heapId];
    }
    else
    {
        hHeap = NULL;
    }
    
    return hHeap;
}
