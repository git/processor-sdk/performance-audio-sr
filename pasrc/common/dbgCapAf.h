
/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _DBG_CAP_AFB_H_
#define _DBG_CAP_AFB_H_

#include <xdc/std.h>

#include "paftyp.h"
#include "dbgBenchmark.h" // PCM high-sampling rate + SRC + CAR benchmarking

#define CAP_AF_SOK         ( 0 )
#define CAP_AF_INV_CHNUM   ( -1 )

//#define DBG_CAP_AF
#ifdef DBG_CAP_AF
// buffer capture parameters
#define CAP_AF_MAX_NUM_FRAME       ( 5625 ) //( 100 )
#define CAP_AF_MAX_SAMP_PER_FRAME  ( PAF_SYS_FRAMELENGTH )
#define CAP_AF_MAX_NUM_SAMP        ( CAP_AF_MAX_NUM_FRAME * CAP_AF_MAX_SAMP_PER_FRAME )

extern PAF_AudioData gCapAfBuf[CAP_AF_MAX_NUM_SAMP];
extern Int32 gCapAfBufIdx;
#endif // DBG_CAP_AF

// Reset audio frame capture buffer
Int capAfReset(Void);

// Write audio frame to capture buffer
Int capAfWrite(
    PAF_AudioFrame *pAf,    // audio frame
    Int8 chNum              // channel number to capture
);

#endif /* _DBG_CAP_AFB_H_ */
