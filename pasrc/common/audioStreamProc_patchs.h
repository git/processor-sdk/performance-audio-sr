
/*
Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// Framework Patch Declarations
//
//

#ifndef _ASP_PATCHS_
#define _ASP_PATCHS_

#include <xdc/std.h>
#include <pafsio.h>
#include <asp0.h>

#include <as1-f2.h>

//
// IROM-Inclusion Definitions
//

/* optional */
// #define PAF_AST_PATCHS_PA17
// #define PAF_AST_PATCHS_PA17U
// #define PAF_AST_PATCHS_PA17I
// #define PAF_AST_PATCHS_PA17Y /* not working? */
// #define PAF_AST_PATCHS_PA17Z /* not working? */

#if ! defined (PAF_AST_PATCHS_PA17) \
    && ! defined (PAF_AST_PATCHS_PA17U) \
    && ! defined (PAF_AST_PATCHS_PA17I) \
    && ! defined (PAF_AST_PATCHS_PA17D) \
    && ! defined (PAF_AST_PATCHS_PA17Y) \
    && ! defined (PAF_AST_PATCHS_PA17Z)
#define PAF_AST_PATCHS_PA17
#endif /* defined (...) */

//
// SIO Parameters
//

typedef struct PAF_SIO_ParamsN {
    Int n;
    const PAF_SIO_Params *x[1];
} PAF_SIO_ParamsN;

//
// Audio Framework Patch
//

typedef struct PAF_AST_Patchs {
    const PAF_SIO_ParamsN * devinp;
    const PAF_SIO_ParamsN * devout;
    const PAF_ASP_LinkInit * const (*i_decLinkInit);
    const PAF_ASP_LinkInit * const (*i_aspLinkInit)[GEARS];
    const PAF_ASP_LinkInit * const (*i_encLinkInit);
} PAF_AST_Patchs;

#endif  /* _ASP_PATCHS_ */
