/*
 * pfp_app.h
 *
 *  PFP application definitions, including:
 *      - PFP IDs
 *      - PFP default exponential time constant
 */

#ifndef _PFP_APP_H
#define _PFP_APP_H

#include "pfp/pfp.h"

/* PFP latch macros */
#define PFP_FINISH_MEAS     ( 0 )
#define PFP_LATCH_MEAS      ( 1 )

#ifdef _TMS320C6X
#define PFP_MAX_ASP         ( 4 )

/* Define all PFP ID's */
#define PFP_ID_MAIN         0
#define PFP_ID_ASIT_0       1
#define PFP_ID_ASIT_1       2   /* ASIT, auto processing */
#define PFP_ID_ASIT_2       3   /* ASIT, decode processing */
#define PFP_ID_ASOT_0       8   
#define PFP_ID_ASOT_1       9   /* ASOT, output processing */
#define PFP_ID_ASOT_ASP_0   16  /* ASP 0 */
#define PFP_ID_ASOT_ASP_1   17  /* ASP 1 */
#define PFP_ID_ASOT_ASP_2   18  /* ASP 2 */
#define PFP_ID_ASOT_ASP_3   19  /* ASP 3 */
#define PFP_ID_LAST PFP_ID_ASOT_ASP_3

#if PFP_ID_LAST >= PFP_PPCNT_MAX
#error too many PP IDs
#endif

/* PFP default exponential averaging time constant */
#define PFP_DEF_TAU_MS      (500.0)
#define PFP_DEF_PERIOD_MS   (100.0)
#define PFP_DEF_ALPHA       (PFP_DEF_PERIOD_MS/PFP_DEF_TAU_MS)

// disable all PFP's
#define PFP_ENABLE_MASK ( 0 )

// enable all PFP's
/*
#define PFP_ENABLE_MASK ( (1<<PFP_ID_ASIT_0) | \
                          (1<<PFP_ID_ASIT_1) | \
                          (1<<PFP_ID_ASIT_2) | \
                          (1<<PFP_ID_ASOT_0) | \
                          (1<<PFP_ID_ASOT_1) | \
                          (1<<PFP_ID_ASOT_ASP_0) | \
                          (1<<PFP_ID_ASOT_ASP_1) | \
                          (1<<PFP_ID_ASOT_ASP_2) | \
                          (1<<PFP_ID_ASOT_ASP_3) )
*/

#else /* _TMS320C6X */

/* Define all PFP ID's */
#define PFP_ID_MAIN         0
#define PFP_ID_ASDT_0       1
#define PFP_ID_ASDT_1       2   /* INFO */
#define PFP_ID_ASDT_2       3   /* DECODE */
#define PFP_ID_LAST PFP_ID_ASDT_2

#if PFP_ID_LAST >= PFP_PPCNT_MAX
#error too many PP IDs
#endif

/* PFP default exponential averaging time constant */
#define PFP_DEF_TAU_MS      (500.0)
#define PFP_DEF_PERIOD_MS   (100.0)
#define PFP_DEF_ALPHA       (PFP_DEF_PERIOD_MS/PFP_DEF_TAU_MS)

//
// NOTE: currently ARM PFP's are enabled in main().
// These masks can be considered a placeholder.

//
// disable all PFP's
#define PFP_ENABLE_MASK ( 0 )

// enable INFO and DECODE PFP's
/*
#define PFP_ENABLE_MASK ( (1<<PFP_ID_ASDT_0) | \
                          (1<<PFP_ID_ASDT_1) | \
                          (1<<PFP_ID_ASDT_2) )
*/

#endif /* _TMS320C6X */

#endif /* _PFP_APP_H */
/* nothing past this point */


