/*
 * clk.h
 *
 *  Created on: Apr 2, 2017
 *      Author: a0216546
 */

#ifndef _CLK_H
#define _CLK_H

#include <stdint.h>

/* Some useful constants */
/* These constants may need calibration if platform properties change */
/* Measurement was done with optimizatoin disabled for the clk.c and main.c */
#define CLK_WORKCNT_PER_MS  3000          /* Number of iterations of clkWorkDelay() to 
                                             achieve 1ms work on 750MHz CPU Clock */
#define CLK_CLOCKS_PER_MS   750000UL      /* for 750MHz */

/* Functions in clk.c */
extern void clkWorkDelay(int loopcnt);
extern void clkDelay(uint32_t pause);
extern void clk0Fun(void);
extern void clk1Fun(void);
extern void task0Fun(void);
extern void task1Fun(void);

#endif /* _CLK_H */
/* nothing past this point */

