/*
 * clk.c
 *
 *  Created on: Apr 2, 2017
 *      Author: a0216546
 */

#define KERNEL_BUG    0     /* Put it to 1 if you want to test for kernel bug (clock module configuration) */

#include <stdint.h>

#include <xdc/runtime/System.h>       /* for System_printf() */

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>          /* mandatory - if you call APIs like BIOS_start() */
#include <ti/sysbios/knl/Semaphore.h> /* this looks obvious */
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Swi.h>
#include <ti/sysbios/hal/Hwi.h>

/* Build configurations like semaphore handles, etc. */
#include <xdc/cfg/global.h>           /* header file for statically defined objects/handles */

/* Local Header files */
#include "clk.h"
#include "../pfp/pfp.h"
#include "../pfp_app.h"

/* Macros */
#define loop  while(1)

#include <xdc/runtime/Timestamp.h>    /* for benchmarking/profiling */
#define Timestamp_get Timestamp_get32 /* use 32-bit time stamps */

volatile int GET_STATS_COUNT = 3000;  /* Get stats after 3000 iterations */

/*----------------------------------------------------------------------------------- 
  clkWorkDelay: Waste time based on actial work
 *-----------------------------------------------------------------------------------*/

#include <math.h>

void clkWorkDelay(int loopcnt)
{
  int k;
  volatile double x, y, z;

  x = 3.14/6;
  y = 3.14/3;
  for (k = 0; k < loopcnt; k++) {
      z = sin(x)+cos(y);
  }
} /* clkWorkDelay */

/*----------------------------------------------------------------------------------- 
  clkDelay: Waste time based on time stamp
 *-----------------------------------------------------------------------------------*/

void clkDelay(uint32_t pause)
{
  volatile uint32_t now,then;

  now = then = Timestamp_get();
  while( (now-then) < pause ) {
    now = Timestamp_get();
  }
} /* clkDelay */

#if 0
/*------------------------------------------------------------ 
  clk0Fun: Wakes up task0 which runs every 100ms
 *------------------------------------------------------------*/

#if KERNEL_BUG
int clk0prev = 0;
#endif

void clk0Fun(void)
{
  Semaphore_post(semTask0WakeUp);

#if KERNEL_BUG
  if (clk0prev == GET_STATS_COUNT) {
    clk0prev = -1;
  }
  else {
    clk0prev = GET_STATS_COUNT;
  }
#endif
} /* clk0Fun */

/*------------------------------------------------------------ 
  clk1Fun: Wakes up task1 which runs every 100ms
 *------------------------------------------------------------*/

#if KERNEL_BUG
int clk1prev = 0;
#endif

void clk1Fun(void)
{
  Semaphore_post(semTask1WakeUp);

#if KERNEL_BUG
  if (clk1prev == GET_STATS_COUNT) {    /* This was used for chasing SYS/BIOS Kernel bug */
    clk1prev = -1;
  }
  else {
    clk1prev = GET_STATS_COUNT;
  }
#endif

} /* clk1Fun */

/*------------------------------------------------------------- 
  task0Fun: Task0 runs every 100ms and uses 30ms of time
 *-------------------------------------------------------------*/

#include <stdio.h>

int reset_hangover = -1;

//#pragma CODE_SECTION(task0Fun,".pfpsection")

void task0Fun(void)
{
  int k;
  pfpStats_t pfp_stats;
  IArg swiKey, hwiKey;
  Task_Handle myHandle;

  /* Task initialization */
  myHandle = Task_self();

  reset_hangover = -1;          /* No need for stat reset */

  /* Main loop */
  loop {

    pfpEnd(PFP_ID_TASK0_2,0);     /* This is OK to do since the first time it would be ignored! */
    /* We placed the end of measurement here since we do not want to measure the printf's below
       which are only used at the end of the test after which we would hit a break point */

    if (GET_STATS_COUNT > 0) {
      GET_STATS_COUNT--;
    }
    else {
      hwiKey=Hwi_disable();
      swiKey=Swi_disable();

      for (k = 0; k <= PFP_ID_LAST; k++) {
        pfpGetStats(k, &pfp_stats);
        printf("ID: %02d, n=%u, C=%lld, Cmin=%u, Cmax=%u, C-avg=%g, Avg-T=%gms, Alpha=%g, C_eavg=%g, EAvg-T=%gms\n", k,
               pfp_stats.n_count,
               pfp_stats.c_total,
               pfp_stats.c_min,
               pfp_stats.c_max,
               (double)pfp_stats.c_total/pfp_stats.n_count,
               (double)pfp_stats.c_total/pfp_stats.n_count/CLK_CLOCKS_PER_MS,
               pfp_stats.alpha,
               pfp_stats.c_average,
               pfp_stats.c_average/CLK_CLOCKS_PER_MS);
      }

      GET_STATS_COUNT = 3000;
      reset_hangover = 3;     /* <----------- you may put Breakpoint here! */
      Swi_restore(swiKey);
      Hwi_restore(hwiKey);
    }

    /* This code "recovers" after the breakpoint in order to "ignore" pending interrupts */
    if (reset_hangover > 0) {
      reset_hangover--;
    }
    else if (reset_hangover == 0) {
      reset_hangover = -1;

      hwiKey=Hwi_disable();
      swiKey=Swi_disable();

      for (k = 2; k <= PFP_ID_LAST; k++) {      /* Do not reset the first two PP's */
        pfpResetStats(k);
      }

      Swi_restore(swiKey);
      Hwi_restore(hwiKey);
    }

    pfpBegin(PFP_ID_TASK0_2, myHandle);

    /*-------------------------------------------------------------------------------------*/
    Semaphore_pend(semTask0WakeUp, BIOS_WAIT_FOREVER);  /* wait for clk0Fun to wake you up */
    /*-------------------------------------------------------------------------------------*/

    /* Waste time for 20ms */
    pfpBegin(PFP_ID_TASK0_1, myHandle);
      clkWorkDelay(20*CLK_WORKCNT_PER_MS);
    pfpEnd(PFP_ID_TASK0_1,0);

    /* Spend another 10ms but outside of previous profile point */
    clkWorkDelay(10*CLK_WORKCNT_PER_MS);

  }

  /* Never come here */

} /* task0Fun */

/*------------------------------------------------------------- 
  task1Fun: Task1 runs every 100ms and uses 23ms of time
 *-------------------------------------------------------------*/

//#pragma CODE_SECTION(task1Fun,".pfpsection")

void task1Fun(void)
{
  Task_Handle myHandle;

  /* Task initialization */
  myHandle = Task_self();

  /* Main loop */
  loop {

    pfpEnd(PFP_ID_TASK1_3, 0);
    pfpBegin(PFP_ID_TASK1_3, myHandle);          /* Measure "everything else" in this task! */

    /*-------------------------------------------------------------------------------------*/
    Semaphore_pend(semTask1WakeUp, BIOS_WAIT_FOREVER);  /* wait for clk1Fun to wake you up */
    /*-------------------------------------------------------------------------------------*/

    /* Waste time for 13ms */
    pfpBegin(PFP_ID_TASK1_1, myHandle);
      clkWorkDelay(13*CLK_WORKCNT_PER_MS);
    pfpEnd(PFP_ID_TASK1_1, 0);

    pfpBegin(PFP_ID_TASK1_2, myHandle);
      clkWorkDelay(8*CLK_WORKCNT_PER_MS);
    pfpEnd(PFP_ID_TASK1_2, 1);      /* latch this measurement */

    pfpBegin(PFP_ID_TASK1_2, myHandle);
      clkWorkDelay(2*CLK_WORKCNT_PER_MS);
    pfpEnd(PFP_ID_TASK1_2, 0);      /* Complete this measurement */

  }


  /* Never come here */

} /* task1Fun */

#endif

/* nothing past this point */

