
/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <math.h> // sin
#include <xdc/std.h>

#include "inpbuf.h"

//#define DBG_CAP_IP_PCM
#ifdef DBG_CAP_IP_PCM
// IB capture (PCM) buffer parameters
#define CAP_IB_PCM_MAX_NUM_CH           ( 8 )
#define CAP_IB_PCM_MAX_NUM_FRAME        ( 100 )
#define CAP_IB_PCM_MAX_SAMP_PER_FRAME   ( 256 )
#define CAP_IB_PCM_MAX_NUM_SAMP         ( CAP_IB_PCM_MAX_NUM_FRAME * CAP_IB_PCM_MAX_SAMP_PER_FRAME )

// IB capture (PCM) buffer
extern Int32 gCapIpPcmBuf[CAP_IB_PCM_MAX_NUM_CH][CAP_IB_PCM_MAX_NUM_SAMP];
extern Int32 gCapIbPcmBufIdx;
extern Int32 gCapIbPcmBufWrapCnt;
#endif // DBG_CAP_IP_PCM

//#define DBG_CAP_IP
#ifdef DBG_CAP_IP
// IB capture buffer parameters
//#define CAP_IB_MAX_NUM_FRAME        ( 938 )
#define CAP_IB_MAX_NUM_FRAME        ( 2000 )
//#define CAP_IB_MAX_BYTES_PER_FRAME  ( 24576 ) // for DDP
#define CAP_IB_MAX_BYTES_PER_FRAME  ( 2*30720 ) // for MAT-THD
#define CAP_IB_BUF_SZ               ( CAP_IB_MAX_NUM_FRAME * CAP_IB_MAX_BYTES_PER_FRAME )

// IB capture buffer
extern Int8 gCapIpBuf[2][CAP_IB_BUF_SZ];
extern Int32 gCapIbBufIdx[2];
extern Int32 gCapIbBufWrapCnt[2];
extern Int8 gCapIbBufPingPongSel;
#endif // DBG_CAP_IP

// Generate sinusoids in IB buffer
Void genSinIb(
    PAF_InpBufConfig *pInpBufConfig
);

// Capture data in IB buffer (PCM) to memory
Void capIbPcm(
    PAF_InpBufConfig *pInpBufConfig
);

// Reset IB capture buffer
Int capIbReset(Void);

// Capture data in IB buffer to memory
Void capIb(
    PAF_InpBufConfig *pInpBufConfig
);
