
/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <xdc/std.h>
#include <ti/ipc/GateMP.h>

#include "aspOutInitSync_common.h"

// Initialize Output Processing Init-Sync control
Int outIsCtlInit(
    PAF_AST_OutInitSyncCtl *pOutIsCtl,          // output init-sync control to initialize
    PAF_AST_OutInitSyncInfo **pXOutIsInfo       // pointer to base address of output init-sync info array
)
{
#ifdef _TMS320C6X
    GateMP_Params gateParams;
    GateMP_Handle gateHandle;
    
    GateMP_Params_init(&gateParams);
    gateParams.localProtect = GateMP_LocalProtect_THREAD;
    gateParams.remoteProtect = GateMP_RemoteProtect_SYSTEM;
    gateParams.name = ASP_OUTIS_GATE_NAME;
    gateParams.regionId = ASP_OUTIS_GATE_REGION_ID;
    gateHandle = GateMP_create(&gateParams);
    if (gateHandle != NULL)
    {
        pOutIsCtl->gateHandle = gateHandle;
    }
    else
    {
        pOutIsCtl->gateHandle = NULL;
        return ASP_OUTIS_CTL_INIT_INV_GATE;
    }
    
    pOutIsCtl->pXOutIsInfo = pXOutIsInfo;
    
    return ASP_OUTIS_SOK;
    
#elif defined(ARMCOMPILE)
    GateMP_Handle gateHandle;
    Int status;
    
    do {
        status = GateMP_open(ASP_OUTIS_GATE_NAME, &gateHandle);
    } while (status == GateMP_E_NOTFOUND);
    if (status == GateMP_S_SUCCESS)
    {
        pOutIsCtl->gateHandle = gateHandle;
    }
    else
    {
        pOutIsCtl->gateHandle = NULL;
        return ASP_OUTIS_CTL_INIT_INV_GATE;
    }
    
    pOutIsCtl->pXOutIsInfo = pXOutIsInfo;
    
    return ASP_OUTIS_SOK;

#else
    #error "Unsupported platform"

#endif
}

// Copy audio frame
// See CPL_setAudioFrame_
Void outIsCpyAf(
    PAF_AudioFrame *pSrcAf,
    PAF_AudioFrame *pDstAf
)
{
    // Write AF members written by CPL_setAudioFrame_();
    pDstAf->sampleDecode                       = pSrcAf->sampleDecode;
    PAF_PROCESS_COPY(pDstAf->sampleProcess, pSrcAf->sampleProcess);
    pDstAf->sampleRate                         = pSrcAf->sampleRate;
    pDstAf->sampleCount                        = pSrcAf->sampleCount;
    pDstAf->channelConfigurationRequest        = pSrcAf->channelConfigurationRequest;
    pDstAf->channelConfigurationStream         = pSrcAf->channelConfigurationStream;
}
