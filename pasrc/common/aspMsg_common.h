
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _ASP_MSG_COMMON_H_
#define _ASP_MSG_COMMON_H_

#include <xdc/std.h>
#include <ti/ipc/MessageQ.h>

// round up the value 'size' to the next 'align' boundary
#define ROUNDUP(size, align) \
    (UInt32)(((UInt32)(size) + ((UInt32)(align) - 1)) & ~((UInt32)(align) - 1))

#define AspMsg_MasterMsgQueName ( "MASTER:MsgQ:01" )    // master MessageQ name
#define AspMsg_SlaveMsgQueName  ( "%s:MsgQ:01"   )      // slave MessageQ name, %s is each slave's Proc Name

#define ASP_MSG_BUF_LEN         ( 32 )  // message buffer length in bytes

// ASP (Master-To-)Slave Commands
typedef enum ASP_Slave_Cmd {
    ASP_SLAVE_NULL,                         // 0
    ASP_SLAVE_START,                        // 1
    ASP_SLAVE_EXIT,                         // 2
    ASP_SLAVE_DEC_SOURCE_SELECT,            // 3
    ASP_SLAVE_DEC_EXIT,                     // 4
    ASP_SLAVE_DEC_CONTROL,                  // 5
    ASP_SLAVE_DEC_ACTIVATE,                 // 6
    ASP_SLAVE_DEC_RESET,                    // 7
    ASP_SLAVE_DEC_INFO,                     // 8
    ASP_SLAVE_DEC_DECODE,                   // 9
    ASP_SLAVE_DEC_DEACTIVATE,               // 10
    ASP_SLAVE_NCOMMANDS                     // 11
} ASP_Slave_Cmd;

// ASP (Slave-To-)Master Command Acknowledgments
typedef enum ASP_Master_Cmd {
    ASP_MASTER_NULL = ASP_SLAVE_NCOMMANDS,  // 11
    ASP_MASTER_START_DONE,                  // 12
    ASP_MASTER_EXIT_DONE,                   // 13
    ASP_MASTER_DEC_SOURCE_SELECT_DONE,      // 14
    ASP_MASTER_DEC_EXIT_DONE,               // 15
    ASP_MASTER_DEC_CONTROL_DONE,            // 16
    ASP_MASTER_DEC_ACTIVATE_DONE,           // 17
    ASP_MASTER_DEC_RESET_DONE,              // 18
    ASP_MASTER_DEC_INFO_DONE,               // 19
    ASP_MASTER_DEC_DECODE_DONE,             // 20
    ASP_MASTER_DEC_DEACTIVATE_DONE          // 21
} ASP_Master_Cmd;

// ASP message
typedef struct AspMsg
{
    MessageQ_MsgHeader reserved;
    
    UInt16  procId;                 // processor Id of sender
    UInt32  messageId;              // MB bit: ack bit, LS 31 bits: message Id
    UInt32  cmd;                    // command
    Char    buf[ASP_MSG_BUF_LEN];   // buffer for message parameters
} AspMsg;


#endif /* _ASP_MSG_COMMON_H_ */
