/*
Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== ipc.cfg.xs ========
 */

/* root of the configuration object model */
var Program = xdc.useModule('xdc.cfg.Program');
var cfgArgs = Program.build.cfgArgs;

/* configure processor names */
var procNameAry = ["HOST", "CORE0"];
var MultiProc = xdc.useModule('ti.sdo.utils.MultiProc');
MultiProc.setConfig(xdc.global.procName, procNameAry);

/* ipc configuration */
var Ipc = xdc.useModule('ti.sdo.ipc.Ipc');
Ipc.procSync = Ipc.ProcSync_PAIR;
Ipc.sr0MemorySetup = true;

/* shared region configuration */
var SharedRegion = xdc.useModule('ti.sdo.ipc.SharedRegion');

SharedRegion.numEntries = 4;

/* configure SharedRegion #0 (IPC) */
var Sr0Mem = Program.cpu.memoryMap["SR_0"];

SharedRegion.setEntryMeta(0,
    new SharedRegion.Entry({
        name:           "SR_0",
        base:           Sr0Mem.base,
        len:            Sr0Mem.len,
        ownerProcId:    1,
        isValid:        true,
        cacheEnable:    xdc.global.SR0_cacheEnable
    })
);

/* configure SharedRegion #1 (MSMC) */
var SrMsmcMem = Program.cpu.memoryMap["SR_MSMC"];

SharedRegion.setEntryMeta(1,
    new SharedRegion.Entry({
        name:           "SR_MSMC",
        base:           SrMsmcMem.base,
        len:            SrMsmcMem.len,
        ownerProcId:    1,
        isValid:        true,
        cacheEnable:    xdc.global.SrMsmcMem_cacheEnable,
        createHeap:     true
    })
);

/* configure SharedRegion #2 (DDR3) */
var SrDDr3Mem = Program.cpu.memoryMap["SR_DDR3"];

SharedRegion.setEntryMeta(2,
    new SharedRegion.Entry({
        name:           "SR_DDR3",
        base:           SrDDr3Mem.base,
        len:            SrDDr3Mem.len,
        ownerProcId:    1,
        isValid:        true,
        cacheEnable:    xdc.global.SrDDr3Mem_cacheEnable,
        createHeap:     true
    })
);

/* configure SharedRegion #3 (DDR3) */
var SrDDr3_2Mem = Program.cpu.memoryMap["COMMON2_DDR3"];

SharedRegion.setEntryMeta(3,
    new SharedRegion.Entry({
        name:           "COMMON2_DDR3",
        base:           SrDDr3_2Mem.base,
        len:            SrDDr3_2Mem.len,
        ownerProcId:    1,
        isValid:        true,
        cacheEnable:    xdc.global.SrDDr3_2Mem_cacheEnable,
        createHeap:     true
    })
);
