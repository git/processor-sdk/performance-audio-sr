/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/* ARM linker command file for DH */

/* SEARCH_DIR ( C:\ti\processor_sdk_audio_1_01_00_01\pasdk\paf\pa\build\a15\release ) */
/* SEARCH_DIR ( C:\ti\processor_sdk_audio_1_01_00_01\pasdk\paf\pa\util\da10x_misc\a15\release ) */
/* SEARCH_DIR ( C:\ti\processor_sdk_audio_1_01_00_01\dolby_ip\intrinsics\Dolby_Intrinsics_Imp\lib_float_A15 ) */
/* SEARCH_DIR ( C:\ti\processor_sdk_audio_1_01_00_01\dolby_ip\ddp\Dolby_Digital_Plus_Decoder_Imp\Source_Code\make\ddp_udc_lib\a15 )     */
/* SEARCH_DIR ( C:\ti\processor_sdk_audio_1_01_00_01\dolby_ip\ddp\Dolby_Digital_Plus_Decoder_Imp\Source_Code\make\ddp_udc_wrapper\a15 ) */
/* SEARCH_DIR ( C:\ti\processor_sdk_audio_1_01_00_01\dolby_ip\mat-thd\Dolby_MAT_Decoder_Imp\Source_Code\mat_dec\make\dthd_dec_lib\a15 ) */
/* SEARCH_DIR ( C:\ti\processor_sdk_audio_1_01_00_01\dolby_ip\mat-thd\Dolby_MAT_Decoder_Imp\Source_Code\mat_dec\make\mat_dec_lib\a15 )  */
/* SEARCH_DIR ( C:\ti\processor_sdk_audio_1_01_00_01\dolby_ip\mat-thd\Dolby_MAT_Decoder_Imp\Source_Code\mat_dec\make\thd_alg_lib\a15 )  */
/* SEARCH_DIR ( C:\ti\processor_sdk_audio_1_01_00_01\dolby_ip\dh-ip\build\a15\release ) */

INPUT ( c67x_cintrins_elf.lib simulate_dma_elf.lib )
INPUT ( acp_elf.lib asp_std_elf.lib com_asp_elf.lib com_dec_elf.lib pcm1_elf.lib statusop_common_elf.lib )
INPUT ( misc_elf.lib )
INPUT ( dlb_intrinsics_generic_float32_release.a )
INPUT ( ddp_dec_lib_generic_wrapper_release.a ddp_dec_lib_generic_float32_release.a )
INPUT ( mat_dec_lib_generic_float32_release.lib thd_alg_lib_generic_float32_release.lib dthd_dec_lib_generic_float32_release.lib )

SECTIONS
{
    .globalSection :
    {
        *(.commonSectionPafAsdtConfig)
    } > HOST_DDR3
    
    .commonSection :
    {
        *(.commonSectionPafAstConfig)
        *(.commonSectionAcpStdBetaTable)
        *(.commonSectionAcpCusBetaTable)
        *(.commonSectionAcpCusPhiTable)
        *(.commonSectionAcpCusSigmaTable)
    } > COMMON_DDR3
    
    .capBufSection (NOLOAD):
    {
        *(.gCapIbBufPcm)
        *(.gCapIbBuf)
        *(.gCapAfBuf)
    } > DDR3
    
    /*
       Remove section to exclude heapMem2Params.size 0's for heap initialization from ARM image.
       Must have corresponding "Program.sectionsExclude" in SYSBIOS configuration file.
    */
    .msmcSramHeap (NOLOAD):
    {
        *(.msmcSramHeap)  
    } > HOST_MSMC
    
    .ddr3Heap (NOLOAD):
    {
        *(.ddr3Heap)  
    } > HOST_DDR3
}
