/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/* ARM linker command file for No IP */

INPUT ( c67x_cintrins_elf.lib simulate_dma_elf.lib )
INPUT ( acp_elf.lib asp_std_elf.lib com_asp_elf.lib com_dec_elf.lib pcm1_elf.lib statusop_common_elf.lib )

SECTIONS
{
    .globalSection :
    {
        *(.commonSectionPafAsdtConfig)
    } > HOST_DDR3
    
    .commonSection :
    {
        *(.commonSectionPafAstConfig)
        *(.commonSectionAcpStdBetaTable)
        *(.commonSectionAcpCusBetaTable)
        *(.commonSectionAcpCusPhiTable)
        *(.commonSectionAcpCusSigmaTable)
    } > COMMON_DDR3
    
    .capBufSection (NOLOAD):
    {
        *(.gCapIbBufPcm)
        *(.gCapIbBuf)
        *(.gCapAfBuf)
    } > DDR3
    
    /*
       Remove section to exclude heapMem2Params.size 0's for heap initialization from ARM image.
       Must have corresponding "Program.sectionsExclude" in SYSBIOS configuration file.
    */
    .msmcSramHeap (NOLOAD):
    {
        *(.msmcSramHeap)  
    } > HOST_MSMC
    
    .ddr3Heap (NOLOAD):
    {
        *(.ddr3Heap)  
    } > HOST_DDR3    
}
