
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== main.c ========
 */

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <ti/ipc/Ipc.h>

#include "components/clk.h" /* PFP testing */
#include "pfp/pfp.h"
#include "pfp_app.h"        /* contains all PFP ID's */

//
// FL: Temporary code for ARM image size reduction
//
#include <string.h>
#include <ti/sysbios/hal/Cache.h>
#include <ti/sysbios/heaps/HeapMem.h>
// Number of control bytes used in heap buffer after heap initialized by SYSBIOS before main()
#define HEAP_CONTROL_SZ ( 8 )

extern void BOARD_initPerfCounters();
extern uint32_t readTime32(void);

/*
 *  ======== main ========
 */
Int main()
{ 
    Int status;
    Int k;
    //UInt32 tsStart, tsEnd, delta; // debug: check ARM SYS/BIOS timestamp provider
    HeapMem_ExtendedStats heapmem_stats;
    Ptr buf;
    SizeT size;
    
    Log_info0("Enter main()");

    // FL: Temporary code for ARM image size reduction
    //  Clear ARM dedicated heap buffers
    //  If heaps aren't cleared:
    //      1) DSP:ASIT: IPC state is corrupted because of incorrect memory segments for PCM2, DDP2 & THD2 beta table entries.
    //      2) DSP:ASIT: DSP hangs in GateMP_open() inside first call to statusOp_Init().
    HeapMem_getExtendedStats(heapMemMsmcSram, &heapmem_stats);
    buf = (Ptr)heapmem_stats.buf+HEAP_CONTROL_SZ;
    size = (SizeT)heapmem_stats.size-HEAP_CONTROL_SZ;
    memset(buf, 0, size);
    Cache_wb(buf, size, Cache_Type_ALLD, 0);
    Cache_wait();
    HeapMem_getExtendedStats(heapMemDdr3, &heapmem_stats);
    buf = (Ptr)heapmem_stats.buf+HEAP_CONTROL_SZ;
    size = (SizeT)heapmem_stats.size-HEAP_CONTROL_SZ;
    memset(buf, 0, size);
    Cache_wb(buf, size, Cache_Type_ALLD, 0);
    Cache_wait();

#if 0 // debug: check ARM SYS/BIOS timestamp provider
    BOARD_initPerfCounters();
#endif
    
    /* Setup Profile Points (PFP) */
    Log_info0("enter PFP Setup");
    pfpCreate();
    pfpCalibrate(1000, 1);
    for (k = 0; k <= PFP_ID_LAST; k++) 
    {
        pfpEnable(k);   /* Enable profile point #k */
    }
    for (k = 1; k <= PFP_ID_LAST; k++)
    {
        pfpSetAlpha(k, PFP_DEF_ALPHA);  /* Set default exp. avg. time const. */
    }
#if 0 // debug: dummy load
    for (k = 0; k < 1000; k++) 
    {
        tsStart=readTime32();
        pfpBegin(PFP_ID_MAIN,0);
        clkWorkDelay(CLK_WORKCNT_PER_MS);     /* This should take about 750,000 cycles to execute, or 750e3/600e6=1.25 msec. */
        pfpEnd(PFP_ID_MAIN,0);
        tsEnd=readTime32();
    }
    delta = tsEnd-tsStart;
#endif

    // Initialize IPC
    status = Ipc_start();
    if (status < 0) 
    {
        System_abort("Ipc_start failed\n");
    }

    BIOS_start();    /* does not return */
    return(0);
}
