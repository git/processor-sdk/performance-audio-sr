
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== aspOutInitSync_slave.c ========
 */


#include <xdc/std.h>

#include "aspOutInitSync_common.h"
#include "aspOutInitSync_slave.h"

// Reset dec stage flags
Int outIsResetDecStageFlags(
    PAF_AST_OutInitSyncCtl *pOutIsCtl,  // Output Init-Sync Control
    Int8 outIsiIdx                      // Output Init-Sync Info index
)
{
    IArg key;
    GateMP_Handle gateHandle;
    PAF_AST_OutInitSyncInfo *pOutIsi;
    Int8 i;
    
    // Get gate handle
    gateHandle = pOutIsCtl->gateHandle;
    // Enter gate
    key = GateMP_enter(gateHandle);

    //
    // Reset decoder stage flags
    //
    
    // Get address of Output Init-Sync Info
    pOutIsi = &((*pOutIsCtl->pXOutIsInfo)[outIsiIdx]);

    // Reset flags
    for (i = 0; i < ASP_OUTIS_NUM_DEC_STAGES; i++)
    {
        pOutIsi->decStageOutInitSyncInfo[i].decFlag = 0;
    }
    
    // Leave the gate
    GateMP_leave(gateHandle, key);
    
    return ASP_OUTIS_SOK;
}

// Write dec stage flag
Int outIsWriteDecStageFlag(
    PAF_AST_OutInitSyncCtl *pOutIsCtl, // Output Init-Sync Control
    Int8 outIsiIdx,                     // Output Init-Sync Info index
    Int8 decStageIdx,                   // dec stage index
    Int8 decFlag                        // flag value to write (0/1)
)
{
    IArg key;
    GateMP_Handle gateHandle;
    PAF_AST_OutInitSyncInfo *pOutIsi;
    PAF_AST_DecStageOutInitSyncInfo *pDecStageOutIsi;
    
    // Get gate handle
    gateHandle = pOutIsCtl->gateHandle;
    // Enter gate
    key = GateMP_enter(gateHandle);

    //
    // Write decoder stage flag
    //

    // Get address of Output Init-Sync Info
    pOutIsi = &((*pOutIsCtl->pXOutIsInfo)[outIsiIdx]);
    // Get address of Decoder Output Init-Sync Info
    pDecStageOutIsi = &pOutIsi->decStageOutInitSyncInfo[decStageIdx];

    // Write flag value
    pDecStageOutIsi->decFlag = decFlag;
    
    // Leave the gate
    GateMP_leave(gateHandle, key);
    
    return ASP_OUTIS_SOK;
}

// Write dec stage flag and AF.
// Flag must be non-zero for write of AF.
Int outIsWriteDecStageFlagAndAf(
    PAF_AST_OutInitSyncCtl *pOutIsCtl, // Output Init-Sync Control
    Int8 outIsiIdx,                     // Output Init-Sync Info index
    Int8 decStageIdx,                   // dec stage index
    Int8 decFlag,                       // flag value to write (0/1)
    PAF_AudioFrame *pDecAfWrt           // audio frame to write
)
{
    IArg key;
    GateMP_Handle gateHandle;
    PAF_AST_OutInitSyncInfo *pOutIsi;
    PAF_AST_DecStageOutInitSyncInfo *pDecStageOutIsi;
    
    // Get gate handle
    gateHandle = pOutIsCtl->gateHandle;
    // Enter gate
    key = GateMP_enter(gateHandle);

    //
    // Write decoder stage audio frame
    // Write decoder stage flag
    //
    
    // Get address of Output Init-Sync Info
    pOutIsi = &((*pOutIsCtl->pXOutIsInfo)[outIsiIdx]);
    // Get address of Decoder Output Init-Sync Info
    pDecStageOutIsi = &pOutIsi->decStageOutInitSyncInfo[decStageIdx];

    if (decFlag != 0)
    {
        // Write audio frame to Out IS
        outIsCpyAf(pDecAfWrt, &pDecStageOutIsi->decAf);
    }

    // Write flag value
    pDecStageOutIsi->decFlag = decFlag;

    // Leave the gate
    GateMP_leave(gateHandle, key);
    
    return ASP_OUTIS_SOK;
}
