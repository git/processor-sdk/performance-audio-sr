
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//
//
// Framework Declarations
//

#include <audioStreamProc_patchs.h>
#include <asp0.h>
#include <asp1.h>
#include "audioStreamDecodeProc.h"

//
// Decoder Definitions
//

//#include <dwr_inp.h>
#include <pcm.h>
#include <pcm_mds.h>


//#define _DSD_

#ifdef _DSD_
#include <dsd_tih.h>
#include <dsd2pcm.h>
#endif

#ifdef DH_BUILD
//
// DH build
//
#define _DDPAT_
#define _THD_
#else    
#undef  _DDPAT_
#undef  _THD_
#endif // DH_BUILD

#ifdef DTS_BUILD
//
// DTS build
//
#define _DTS_
#else
#undef  _DTS_
#endif // DTS_BUILD


//#define _SRC4_


#ifdef _DDPAT_
#include <ddpat.h>
#include <ddpat_tid.h>
#endif

#ifdef _THD_
#include <thd.h>
#include <thd_tid.h>
#endif

#ifdef _DTS_
#include <dtsuhda.h>
#include <dtsuhda_tid.h>
#endif

#ifdef _SRC4_
#include <src.h>
#include <src_tih.h>
#define SUC_TIH_init SRC_TIH_init
#define SUC_TIH_ISUC SRC_TIH_ISRC
#endif

const PAF_ASP_LinkInit decLinkInitI13[] =
{
    //PAF_ASP_LINKINITPARAMS (STD, DWRPCM, TII, &IDWRPCM_PARAMS),
    PAF_ASP_LINKINIT(STD, PCM, MDS),

#ifdef _DDPAT_
    PAF_ASP_LINKINIT(STD, DDP, TID),
#endif
#ifdef _THD_
    PAF_ASP_LINKINIT(STD, THD, TID),
#endif
    
#ifdef _DTS_
    PAF_ASP_LINKINIT(STD, DTSUHDA, TID),
#endif

#ifdef _DSD_
    PAF_ASP_LINKINIT(STD, DSD, TIH),
#endif

    PAF_ASP_LINKNONE,
};

const PAF_ASP_LinkInit *const patchs_decLinkInit[] =
{
    decLinkInitI13,
};


//
// Audio Stream Patch Definition
//

const PAF_ASDT_Patchs asdp_patchs_PAi =
{
    patchs_decLinkInit,
};

// EOF
