
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// PAF_DEVICE_VERSION Symbol Definitions

#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)

#include <xdc/cfg/global.h>
//#include <ti/procsdk_audio/procsdk_audio_typ.h>
#include <procsdk_audio_typ.h>

#include "paftyp.h"
#include <stdasp.h>
#include <pafsio_ialg.h>

#include "paf_heapMgr.h"

#include "asp1.h"
#include "audioStreamProc_params.h"
#include "audioStreamDecodeProc.h"

#include "dbgBenchmark.h" // PCM high-sampling rate + SRC + CAR benchmarking

//
// Framework Declarations
//

//#define PAF_AST_params_fxnsPA PAF_AST_params_fxnsPA17

//
// Audio Data Representation Definitions
//
//   External declarations to patched IROM provide standard functionality.
//

/* audio frame "width" in channels */
const SmInt PAF_AST_params_numchan[1] =
{
    32,
};

///
// Audio Stream Processing Function Table Definition
//

const PAF_AudioFunctions PAF_ASDT_params_audioFrameFunctions =
{
    &PAF_ASP_dB2ToLinear,
    &PAF_ASP_channelMask,
    &PAF_ASP_programFormat,
    &PAF_ASP_sampleRateHz,
    &PAF_ASP_delay,
};

//
// Source Select Array Declarations -- algorithm keys & sio map
//
//   External declarations to patched IROM provide standard functionality.
//

const PAF_ASP_AlgKey PAF_ASP_params_decAlgKey =
{
    PAF_SOURCE_N,                                           // length
    /* Relies on the fact that ACP_SERIES_* != 0 here */
    0,                                                      // PAF_SOURCE_UNKNOWN
    0,                                                      // PAF_SOURCE_NONE
    0,                                                      // PAF_SOURCE_PASS
    PAF_ASP_ALPHACODE (STD, SNG),                           // PAF_SOURCE_SNG
    0,                                                      // PAF_SOURCE_AUTO
    0,                                                      // PAF_SOURCE_BITSTREAM
    PAF_ASP_ALPHACODE (STD, DTSUHDA),                         // PAF_SOURCE_DTSALL
    PAF_ASP_ALPHACODE (STD, PCM),                           // PAF_SOURCE_PCMAUTO
    PAF_ASP_ALPHACODE (STD, PCM),                           // PAF_SOURCE_PCM
    PAF_ASP_ALPHACODE (STD, PCN),                           // PAF_SOURCE_PC8 /* unused */
    PAF_ASP_ALPHACODE (STD, DDP),                           // PAF_SOURCE_AC3
    PAF_ASP_ALPHACODE (STD, DTSUHDA),                         // PAF_SOURCE_DTS
    PAF_ASP_ALPHACODE (STD, AAC),                           // PAF_SOURCE_AAC
    PAF_ASP_ALPHACODE (STD, MPG),                           // PAF_SOURCE_MPEG /* unused */
    PAF_ASP_ALPHACODE (STD, DTSUHDA),                         // PAF_SOURCE_DTS12
    PAF_ASP_ALPHACODE (STD, DTSUHDA),                         // PAF_SOURCE_DTS13
    PAF_ASP_ALPHACODE (STD, DTSUHDA),                         // PAF_SOURCE_DTS14
    PAF_ASP_ALPHACODE (STD, DTSUHDA),                         // PAF_SOURCE_DTS16
    0,                                                      // PAF_SOURCE_WMA9PRO
    0,                                                      // PAF_SOURCE_MP3
    PAF_ASP_ALPHACODE (STD, DSD),                           // PAF_SOURCE_DSD1
    PAF_ASP_ALPHACODE (STD, DSD),                           // PAF_SOURCE_DSD2
    PAF_ASP_ALPHACODE (STD, DSD),                           // PAF_SOURCE_DSD3
    PAF_ASP_ALPHACODE (STD, DDP),                           // PAF_SOURCE_DDP
    PAF_ASP_ALPHACODE (STD, DTSUHDA),                       // PAF_SOURCE_DTSHD
    PAF_ASP_ALPHACODE (STD, THD),                           // PAF_SOURCE_THD
    PAF_ASP_ALPHACODE (STD, DXP),                           // PAF_SOURCE_DXP
};

//
// Mapping Declarations -- from *coders to *puts
//
//   External declarations to patched IROM provide standard functionality.
//
const SmInt PAF_AST_streamsFromDecodes_std[DECODEN_MAX] =
{
    0, 1, 2,
};

const SmInt PAF_AST_inputsFromDecodes_std[DECODEN_MAX] =
{
    0, 1, 2,
};

//
// Audio Framework Initialization Function Table Declarations
//
//   External declarations to patched IROM provide standard functionality.
//

extern const PAF_ASDT_Fxns PAF_ASDT_params_fxns;

const PAF_DecodeStatus PAF_AST_params_decodeStatus_primary =
{
    sizeof (PAF_DecodeStatus),                              // size
    1,                                                      // mode
    0,                                                      // unused1
    0,                                                      // command.action
    0,                                                      // command.result
    PAF_SAMPLERATE_UNKNOWN,                                 // sampleRate
    PAF_SOURCE_NONE,                                        // sourceSelect
    PAF_SOURCE_UNKNOWN,                                     // sourceProgram
    PAF_SOURCE_UNKNOWN,                                     // sourceDecode
    PAF_SOURCE_DUAL_STEREO,                                 // sourceDual
    4,                                                      // sourceKaraoke: both
    0,                                                      // aspGearControl: unused
    0,                                                      // aspGearStatus: unused
    0, 0, 0, 0,                                             // Unused
    0, 0, 0, 0,                                             // Unused
    0, 0, 0, 0,                                             // Unused
    0, 0, 0, 0,                                             // Unused
    0, 0, 0, 0,                                             // Unused
    0,                                                      // frameCount
    0x40,                                                   // karaoka: Vocal 1 Level
    0x40,                                                   // karaoka: Vocal 1 Pan
    0x40,                                                   // karaoka: Vocal 2 Level
    0xc0,                                                   // karaoka: Vocal 2 Pan
    0x40,                                                   // karaoka: Melody Level
    0x00,                                                   // karaoka: Melody Pan
    0,                                                      // decBypass
    0,                                                      // unused
    0,                                                      // frameLength: reset later
    1,                                                      // bufferRatio: unity
    PAF_IEC_PREEMPHASIS_UNKNOWN,                            // emphasis
    0,                                                      // bufferDrift
    0, 0,
    PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, PAF_CC_AUX_SURROUND4_UNKNOWN, 0, 0, 0, 0, 0,    
    // channelConfigurationRequest.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,    
    // channelConfigurationProgram.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,    
    // channelConfigurationDecode.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,    
    // channelConfigurationDownmix.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,    
    // channelConfigurationOverride.full 
    -3,                                                     // channelMap.from[0]
    -3,                                                     // channelMap.from[1]
    -3,                                                     // channelMap.from[2]
    -3,                                                     // channelMap.from[3]
    -3,                                                     // channelMap.from[4]
    -3,                                                     // channelMap.from[5]
    -3,                                                     // channelMap.from[6]
    -3,                                                     // channelMap.from[7]
    -3,                                                     // channelMap.from[8]
    -3,                                                     // channelMap.from[9]
    -3,                                                     // channelMap.from[10]
    -3,                                                     // channelMap.from[11]
    -3,                                                     // channelMap.from[12]
    -3,                                                     // channelMap.from[13]
    -3,                                                     // channelMap.from[14]
    -3,                                                     // channelMap.from[15]
    -3,                                                     // channelMap.from[16]
    -3,                                                     // channelMap.from[17]
    -3,                                                     // channelMap.from[18]
    -3,                                                     // channelMap.from[19]
    -3,                                                     // channelMap.from[20]
    -3,                                                     // channelMap.from[21]
    -3,                                                     // channelMap.from[22]
    -3,                                                     // channelMap.from[23]
    -3,                                                     // channelMap.from[24]
    -3,                                                     // channelMap.from[25]
    -3,                                                     // channelMap.from[26]
    -3,                                                     // channelMap.from[27]
    -3,                                                     // channelMap.from[28]
    -3,                                                     // channelMap.from[29]
    -3,                                                     // channelMap.from[30]
    -3,                                                     // channelMap.from[31]
    -3,                                                     // channelMap.to[0]
    -3,                                                     // channelMap.to[1]
    -3,                                                     // channelMap.to[2]
    -3,                                                     // channelMap.to[3]
    -3,                                                     // channelMap.to[4]
    -3,                                                     // channelMap.to[5]
    -3,                                                     // channelMap.to[6]
    -3,                                                     // channelMap.to[7]
    -3,                                                     // channelMap.to[8]
    -3,                                                     // channelMap.to[9]
    -3,                                                     // channelMap.to[10]
    -3,                                                     // channelMap.to[11]
    -3,                                                     // channelMap.to[12]
    -3,                                                     // channelMap.to[13]
    -3,                                                     // channelMap.to[14]
    -3,                                                     // channelMap.to[15]
    -3,                                                     // channelMap.to[16]
    -3,                                                     // channelMap.to[17]
    -3,                                                     // channelMap.to[18]
    -3,                                                     // channelMap.to[19]
    -3,                                                     // channelMap.to[20]
    -3,                                                     // channelMap.to[21]
    -3,                                                     // channelMap.to[22]
    -3,                                                     // channelMap.to[23]
    -3,                                                     // channelMap.to[24]
    -3,                                                     // channelMap.to[25]
    -3,                                                     // channelMap.to[26]
    -3,                                                     // channelMap.to[27]
    -3,                                                     // channelMap.to[28]
    -3,                                                     // channelMap.to[29]
    -3,                                                     // channelMap.to[30]
    -3,                                                     // channelMap.to[31]
    0,                                                      // programFormat.mask
    0,                                                      // programFormat.form
   
};

const PAF_DecodeStatus *const PAF_ASDT_params_decodeStatus[] =
{
    &PAF_AST_params_decodeStatus_primary,
};

//
// Common Space Parameter Declarations and Definitions
//
//   Local definitions in RAM provide non-standard functionality.
//   The NULL pointer provides standard functionality.
//

/* baseline definition - NULL equivalent */
/* May be used for overrides of IALG_MemSpace */

static const IALG_MemSpace params_memspace_PAi_Slave[] = {
    PAF_IALG_NONE,                                          // Scratch
    PAF_IALG_NONE,                                          // Persistant
    PAF_IALG_NONE,                                          // Write once
    PAF_IALG_NONE,                                          // Common 1
    PAF_IALG_NONE,                                          // Common 2
    PAF_IALG_NONE,                                          // Common 3
    PAF_IALG_NONE,                                          // Common 4
    PAF_IALG_NONE,                                          // Common 5
    PAF_IALG_NONE,                                          // Common 6
    PAF_IALG_NONE,                                          // Common 7
    IALG_EXTERNAL,                                          // Common 8
    PAF_IALG_NONE,                                          // Common 9
    PAF_IALG_NONE,                                          // Common 10
    PAF_IALG_NONE,                                          // Common 11
    PAF_IALG_NONE,                                          // Common 12
    PAF_IALG_NONE,                                          // Common 13
    PAF_IALG_NONE,                                          // Common 14
    PAF_IALG_NONE,                                          // Common 15
};

//
// Heap Declarations
//

//#include <pafhjt.h>

// .............................................................................
const PAF_MetadataBufStatus PAF_ASDT_params_MetadataBufStatus[] =
{
    PAF_MAX_PRIVATE_MD_SZ,  // bufSize
    PAF_MAX_NUM_PRIVATE_MD, // NumBuf
    128,                    // alignment
    &gPafHeapIdExt          // pHeapIdMdbuf
};

// .............................................................................
//
// Audio Stream Parameter Definitions
//
//   Global definition in RAM provides standard & non-standard functionality.
//

const PAF_ASDT_Params asdp_params_PAi =
{
    &PAF_ASDT_params_fxns,                                  // fxns
    {                                                       // zone
        0,                                                  // zone.master
        1,                                                  // zone.decodes
        0,                                                  // zone.decode1
        1,                                                  // zone.decodeN
        1,                                                  // zone.streams
    },
    PAF_AST_inputsFromDecodes_std,
    {                                                       // heap
        &gPafHeapIdInt1,                                    // heap.pHeapIdIntern
        &gPafHeapIdExt,                                     // heap.pHeapIdExtern
        &gPafHeapIdExt,                                     // heap.pHeapIdFrmbuf
        &gPafHeapIdInt1,                                    // heap.pHeapIdIntern1
        &gPafHeapIdInt1Shm,                                 // heap.pHeapIdInt1Shm
        &gPafHeapIdExtShm,                                  // heap.pHeapIdExtShm
        &gPafHeapIdExtNonCachedShm,                         // heap.pHeapIdExtNonCachedShm
        1,                                                  // heap.clear
    },
    {                                                       // common
        params_memspace_PAi_Slave,                          // common.space
    },
    PAF_AST_params_numchan,                                 // z_numchan
    PAF_SYS_FRAMELENGTH,                                    // framelength
    &PAF_ASDT_params_audioFrameFunctions,                   // pAudioFrameFunctions
    &PAF_ASP_chainFxns,                                     // pChainFxns
    PAF_ASDT_params_decodeStatus,                           // z_pDecodeStatus
    &PAF_ASP_params_decAlgKey,                              // pDecAlgKey
    PAF_AST_streamsFromDecodes_std,                         // streamsFromDecodes
    14592,                                                  // maxFramelength set to match maximum value possible for a stream,
                                                            // max for 192k THD:
                                                            // 14592 = ((nearest 256 mutiple of (90AUs/frame * 160sample/AU))
                                                            // + 256 samples remaining in wrapper from previous deccode)
    PAF_ASDT_params_MetadataBufStatus,                      //metadata buffer status
    NULL //PAF_AST_params_AudioFrameBufStatus                      //Audio frame buffer status
};

// EOF
