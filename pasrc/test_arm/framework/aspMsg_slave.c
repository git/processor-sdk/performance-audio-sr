
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <stdio.h>
#include <xdc/std.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/System.h>

#include "aspMsg_common.h"
#include "aspMsg_slave.h"

#define AspMsg_SlaveToMasterMsgHeapId   ( 1 )

AspMsgSlave_Module gAspMsgSlave;
AspMsgSlave_Handle hAspMsgSlave=&gAspMsgSlave;

/* Initialize ASP slave messaging */
Int AspMsgSlave_init(
    AspMsgSlave_Handle hAspMsgSlave, 
    UInt16 remoteProcId
)
{
    Int                 status = 0;
    MessageQ_Params     msgqParams;
    char                msgqName[32];

    Log_print0(Diags_ENTRY, "AspMsgSlave_init: -->");

    // initialize module object state
    // set processor Ids
    hAspMsgSlave->masterProcId = remoteProcId;
    hAspMsgSlave->slaveProcId = MultiProc_self();
    
    // create local message queue (inbound messages)
    MessageQ_Params_init(&msgqParams);
    System_sprintf(msgqName, AspMsg_SlaveMsgQueName, MultiProc_getName(MultiProc_self()));
    hAspMsgSlave->slaveQue = MessageQ_create(msgqName, &msgqParams);

    if (hAspMsgSlave->slaveQue == NULL)
    {
        status = -1;
        Log_print1(Diags_EXIT, "<-- AspMsgSlave_init(): %d", (IArg)status);
        return status;
    }

    Log_print0(Diags_INFO,"AspMsgSlave_init(): ASP Slave messaging ready");
    
    Log_print1(Diags_EXIT, "<-- AspMsgSlave_init(): %d", (IArg)status);
    return status;
}
