
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== systemInit.c ========
 */

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/System.h>
#include <ti/ipc/SharedRegion.h>
#include <ti/ipc/Ipc.h>

#include "params.h"
#include "pafhjt.h"
#include "paf_heapMgr.h"
#include "ipc_common.h"
#include "aspMsg_common.h"
#include "aspMsg_slave.h"
#include "aspDecOpCircBuf_slave.h"
#include "audioStreamProc_common.h"
#include "audioStreamDecodeProc.h"

const PAFHJT_t *pafhjt;

#define RAM_REPORT
#ifdef RAM_REPORT
#include <audioStreamProc_params.h>
#include <paf_alg_print.h>
extern PAF_ASDT_Fxns PAF_ASDT_params_fxns;
#endif

#define TASK_ASDP_PRI    ( 3 )

#define __TASK_NAME__  "TaskSysInit"

/*
 *  ======== taskSysInitFxn ========
 *  System Initializations task function
 */
Void taskSysInitFxn(Void)
{
    UInt16 remoteProcId;    
    Int status;
    UInt16  regionId;
    IHeap_Handle srHeapNonCacheDdr3;

    
    Log_info0("Enter taskSysInitFxn()");

    /* Attach IPC */
    Log_info0("taskSysInitFxn:Ipc_attach()");    
    remoteProcId = MultiProc_getId(IPC_MASTER_NAME);
    if (remoteProcId == MultiProc_INVALIDID) {
        System_abort("Improper MultiProc ID\n");
    }
    do {
        status = Ipc_attach(remoteProcId);
    //} while ((status < 0) && (status == Ipc_E_NOTREADY));
    } while (status != Ipc_S_SUCCESS);

    /* Initialize (IPC) ASP slave messaging */
    Log_info0("taskSysInitFxn:AspMsgSlave_init()");
    status = AspMsgSlave_init(
        hAspMsgSlave, 
        remoteProcId);
    if (status < 0)
    {
        Log_info1("%s: Unable to initialize ASP messaging. Exiting.", (IArg)__TASK_NAME__);
        return;
    }

    /* Initialize decoder output circular buffer control */
    Log_info0("taskSysInitFxn:cbCtlInit()");    
    status = cbCtlInit(&gPAF_ASDT_config.decOpCircBufCtl, asdp_params.zone.decodeN, &gPAF_AST_config.xDecOpCb);
    if (status < 0)
    {
        Log_info1("%s: unable to initialize Decoder Output Circular Buffer Control. Exiting.", (IArg)__TASK_NAME__);
        return;
    }
    /* Initialize Output Processing Init-Sync control */
    Log_info0("taskSysInitFxn:outIsCtlInit()");
    status = outIsCtlInit(&gPAF_ASDT_config.outIsCtl, &gPAF_AST_config.xOutIsi);
    if (status < 0)
    {
        Log_info1("%s: unable to initialize Output Init-Sync Control. Exiting.", (IArg)__TASK_NAME__);
        return;
    }

    regionId = SharedRegion_getIdByName("COMMON2_DDR3");
    srHeapNonCacheDdr3 = (IHeap_Handle)SharedRegion_getHeap(regionId);
    /* Initialize PAF heap manager */
    Log_info0("taskSysInitFxn:pafHeapMgr_init()");
    pafHeapMgr_init((IHeap_Handle)heapMemL2Sram,
        (IHeap_Handle)heapMemMsmcSram,
        (IHeap_Handle)heapMemDdr3,
        NULL,
        NULL,
        srHeapNonCacheDdr3);
    
    /* Initialize HJT */
    pafhjt = &PAFHJT_RAM;
    
#ifdef RAM_REPORT
    PAF_ASDT_params_fxns.headerPrint = PAF_ALG_headerPrint;
    PAF_ASDT_params_fxns.allocPrint = PAF_ALG_allocPrint;
    PAF_ASDT_params_fxns.commonPrint = PAF_ALG_commonPrint;
    PAF_ASDT_params_fxns.bufMemPrint = PAF_ALG_bufMemPrint;
    PAF_ASDT_params_fxns.memStatusPrint = PAF_ALG_memStatusPrint;
#endif

    Task_setPri(TaskAsdp, TASK_ASDP_PRI);

    Log_info0("Exit taskSysInitFxn()");
}
