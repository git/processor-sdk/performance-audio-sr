
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== aspOutInitSync_slave.h ========
 */

#ifndef _ASP_OUTIS_SLAVE_H_
#define _ASP_OUTIS_SLAVE_H_

#include <xdc/std.h>

#include "aspOutInitSync_common.h"

// Reset dec stage flags
Int outIsResetDecStageFlags(
    PAF_AST_OutInitSyncCtl *pOutIsCtl, // Output Init-Sync Control
    Int8 outIsiIdx                      // Output Init-Sync Info index
);

// Write dec stage flag
Int outIsWriteDecStageFlag(
    PAF_AST_OutInitSyncCtl *pOutIsCtl, // Output Init-Sync Control
    Int8 outIsiIdx,                     // Output Init-Sync Info index
    Int8 decStageIdx,                   // dec stage index
    Int8 decFlag                        // flag value to write (0/1)
);

// Write dec stage AF and flag
// Flag must be non-zero for write of AF.
Int outIsWriteDecStageFlagAndAf(
    PAF_AST_OutInitSyncCtl *pOutIsCtl, // Output Init-Sync Control
    Int8 outIsiIdx,                     // Output Init-Sync Info index
    Int8 decStageIdx,                   // dec stage index
    Int8 decFlag,                       // flag value to write (0/1)
    PAF_AudioFrame *pDecAfWrt           // audio frame to write
);


#endif /* ASP_OUTIS_SLAVE_H_ */
