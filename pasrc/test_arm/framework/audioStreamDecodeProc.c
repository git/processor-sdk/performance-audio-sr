
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamDecodeProc.c ========
 */

#include <xdc/cfg/global.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Memory.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/hal/Cache.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>

#include <acp_mds.h>
#include <pcm.h>
#include "audioStreamProc_params.h"
#include "audioStreamProc_patchs.h"
#include "audioStreamProc_config.h"

#include "common.h"
#include "aspMsg_common.h"
#include "aspMsg_slave.h"
#include "aspDecOpCircBuf_slave.h"
#include "aspOutInitSync_slave.h"
#include "audioStreamProc_common.h"
#include "audioStreamDecodeProc.h"
#include "statusOp_common.h"

#include "pfp/pfp.h"
#include "pfp_app.h"        /* contains all PFP ID's */

// debug
#include "dbgCapAf.h"
#include "dbgDib.h"
#include "evmc66x_gpio_dbg.h"

//
// Decoder Definitions
//
#define decLinkInit pQ->i_decLinkInit

#define __TASK_NAME__  "TaskAsdp"

// Check decoder sync using information in INFO audio frame 
static Int8 checkDecSync(
    PAF_AudioFrame *pAf
);

extern struct {
    Int size;
    IALG_Status *pStatus[512];
} IACP_STD_BETA_TABLE;

extern const char AFChanPtrMap[PAF_MAXNUMCHAN+1][PAF_MAXNUMCHAN];
extern PAF_ChannelConfigurationMaskTable PAF_ASP_stdCCMT;

LINNO_DEFN(TaskAsdp); /* Line number macros */
ERRNO_DEFN(TaskAsdp); /* Error number macros */

// ASDT configuration
PAF_ASDT_Config gPAF_ASDT_config 
__attribute__ ((section(".globalSectionPafAsdtConfig"))) = {
    NULL,               // taskHandle
    NULL,               // acp
    {NULL, 0, NULL},    // decOpCircBufCtl
    {NULL, NULL},       // outIsCtl
    &gPAF_AST_config    // ASIT/ASOT/ASDT shared configuration
};

PAF_AudioFrame *gpDecAudioFrame=NULL;
PAF_AudioData  *gDecAudioFrameChannelPointers[PAF_MAXNUMCHAN_AF];
PAF_AudioSize  gDecAudioFrameChannelSizes[PAF_MAXNUMCHAN_AF];
PAF_AudioData  *gDecOrigAudioFrameChannelPointers[PAF_MAXNUMCHAN_AF];

// Overflow threshold before circular buffer reset and return error to Top-Level FSM
#define DEC_OP_CB_WRTAF_OVR_THR  ( 20 ) // arbitrary setting
UInt32 gCbWrtAfErrCnt           =0; // decoder output circular buffer write error count, not including overflows
UInt32 gDecOpCbWrtAfOvr         =0; // decoder output circular buffer overflow count
UInt32 gMaxDecOpCbWrtAfOvr      =0; // max (consecutive) decoder output circular buffer overflow count
UInt32 gSlaveCbResetCnt         =0; // slave circular buffer reset count

// Global debug counters */
UInt32 gSlaveStartErrCnt        =0;
UInt32 gSlaveStartCnt           =0;
UInt32 gSlaveSourceSelectCnt    =0;
UInt32 gSlaveExitCnt            =0;
UInt32 gSlaveDecExitCnt         =0;
UInt32 gSlaveDecControlCnt      =0;
UInt32 gSlaveDecActivateCnt     =0;
UInt32 gSlaveDecResetCnt        =0;
UInt32 gSlaveDecInfoCnt         =0;
UInt32 gSlaveDecDecodeCnt       =0;
UInt32 gSlaveDecDeactivateCnt   =0;


//#define CAPTURE_DECODER_OUTSAMPLES_PP
#ifdef CAPTURE_DECODER_OUTSAMPLES_PP

#define CAP_FRAME_MAX            10000
Uint32 tempCap_frameCnt = 0;
int tempCap_decSampleOut[CAP_FRAME_MAX] = {0};
#endif


/*
 *  ======== taskAsdpFxn ========
 *  Audio Stream Decode Processing task function
 */
Void taskAsdpFxn(
    const PAF_ASDT_Params *pP,
    const PAF_ASDT_Patchs *pQ
)
{
    PAF_ASDT_Config *pAsdtCfg;          // ASDT configuration pointer
    PAF_AST_Config *pAstCfg;            // Common (shared) configuration pointer
    Int as;                             // Audio Stream Number (1, 2, etc.)
    Int z;                              // input/encode/stream/decode/output counter
    Int i;                              // phase
    Int zMD, zMS;
    Bool done;
    Bool decDone;
    ALG_Handle alg[DECODEN_MAX];
    ASP_Slave_Cmd slaveCmd;
    Int sourceSelect;
    DEC_Handle dec;
    IALG_Cmd decCtrlCmd;                // decoder control command
    Int decCtrlRet;                     // decoder control return
    Int errno;                          // error number
    Int size;
    Int argIdx;
    // Decoder output circular buffer
    PAF_AST_DecOpCircBufCtl *pCbCtl;    // Decoder output Circular Buffer control
    PAF_AudioFrame *pAfWrt;             // pointer to audio frame written to CB
    Int cbErrno;                        // CB error number
    // Output Init-Sync
    PAF_AudioFrame *pDecCtrlAf;         // pointer to Dec Control audio frame
    PAF_AST_OutInitSyncCtl *pOutIsCtl;  // OutIS control
    Int8 outIsDecInfo1Flag;             // indicates whether Dec Info Init-Sync has executed
    Int8 outIsDecDecode1Flag;           // indicates whether Dec Decode1 Init-Sync has executed
    Int outIsErrno;                     // OutIS error number
    // Messaging
    PAF_InpBufConfig *pIpBufConfig;     // IB buffer configuration
    AspMsg *pAspMsg;                    // Rx/Tx MessageQ message
    MessageQ_QueueId queId;             // MessageQ ID
    Int status;
    Int zI;
    Int bufEnd, wrapSize, currentBufSize, chunkSize;
    Int8 temp8;

    Log_info0("Enter taskAsdpFxn()");
    
    
    //
    // Audio Framework Parameters & Patch (*pP, *pQ):
    //
    if (!pP) 
    {
        TRACE_TERSE0("TaskAsdp: No Parameters defined. Exiting.");
        LINNO_RPRT(TaskAsdp, -1);
        return;
    }

    if (!pQ) 
    {
        TRACE_TERSE0("TaskAsdp: No Patchs defined. Exiting.");
        LINNO_RPRT(TaskAsdp, -1);
        return;
    }    

    //
    // Audio Stream Decode Task Configuration (*pAsdtCfg):
    //
    pAsdtCfg = &gPAF_ASDT_config;           // initialize pointer to task configuration
    pAsdtCfg->taskHandle = Task_self();     // set task handle
    pAstCfg = pAsdtCfg->pAstCfg;            // get pointer to AST common (shared) configuration 
    pCbCtl = &pAsdtCfg->decOpCircBufCtl;    // get pointer to circular buffer control
    pOutIsCtl = &pAsdtCfg->outIsCtl;        // get pointer to output init-sync control
    
    // wait for initialization message from master
    do {
        status = MessageQ_get(hAspMsgSlave->slaveQue, (MessageQ_Msg *)&pAspMsg, MessageQ_FOREVER);
        //TRACE_TERSE1("Rx ASP message: status=%d", status);
    //} while ((status != MessageQ_S_SUCCESS || (pAspMsg->cmd != ASP_SLAVE_START));
    } while (status != MessageQ_S_SUCCESS);
    if ((pAspMsg->procId != hAspMsgSlave->masterProcId) ||
        (pAspMsg->cmd != ASP_SLAVE_START))
    {
        TRACE_TERSE3("ERROR: Rx ASP message: procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);
        SW_BREAKPOINT;
    }
    hAspMsgSlave->masterMessageId = pAspMsg->messageId; 
    gSlaveStartCnt++;
    TRACE_MSG3("Rx ASP message, procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);

    // invalidate AST shared configuration
    Cache_inv(pAstCfg, sizeof(PAF_AST_Config), Cache_Type_ALLD, 0);
    Cache_wait();
    
    // invalidate Dec configuration for all Decoder zones
    Cache_inv(&pAstCfg->xDec[0], DECODEN*sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
    Cache_wait();

    // invalidate Beta Table status pointers
    Cache_inv((Ptr)(&IACP_STD_BETA_TABLE.pStatus[0]), 512*sizeof(IALG_Status *), Cache_Type_ALLD, 0); // invalidate entire beta table
    Cache_wait();

    /* Obtain Audio Stream Number (1, 2, etc.) */
    as = pAstCfg->as;
    TRACE_TERSE1("TaskAsdp: Started with AS%d.", as);
    
    //
    // Initialize message log trace and line number reporting
    //
    for (z=STREAM1; z < STREAMN; z++)
    {
        TRACE_TERSE1("TaskAsdp: AS%d: initiated", as+z);
    }
    LINNO_RPRT(TaskAsdp, -1);

    // Get decoder and stream index associated with the master input
    zMD = pAstCfg->masterDec;
    zMS = pAstCfg->masterStr;

    // 
    // Initialize per parameterized phases.
    //   - Malloc: Memory Allocation
    //   - Config: Configuration Initialization
    //   - AcpAlg: ACP Algorithm Initialization and Local Attachment
    //   - Common: Common Algorithm Initialization
    //   - AlgKey: Dec/Enc chain to Array Initialization
    //   - Unused: (available)
    //   - Unused: (available)
    //   - Unused: (available)
    //
    LINNO_RPRT(TaskAsdp, -2);
    for (i=0; i < lengthof(pP->fxns->initPhase); i++)
    {
        Int linno;
        if (pP->fxns->initPhase[i])
        {
            if ((linno = pP->fxns->initPhase[i](pP, pQ, pAsdtCfg)))
            {
                LINNO_RPRT(TaskAsdp, linno);
                return;
            }
        }
        else 
        {
            TRACE_TERSE1("TaskAsdp: AS%d: initialization phase - null", as+zMS);
        }
        TRACE_TERSE2("TaskAsdp: AS%d: initialization phase - %d completed", as+zMS, i);
        LINNO_RPRT(TaskAsdp, -i-3);
    }
    
#ifdef NON_CACHE_STATUS
    //
    // init Status structure Gate
    //
    if (statusOp_Init(GATEMP_INDEX_DEC) == STATUSOP_INIT_FAIL)
    {
        TRACE_TERSE1("TaskAsdp: Gate Index %d:initialization status GateMP Fail.", GATEMP_INDEX_DEC);
    }
    if (statusOp_Init(GATEMP_INDEX_DDP) == STATUSOP_INIT_FAIL)
    {
        TRACE_TERSE1("TaskAsdp: Gate Index %d:initialization status GateMP Fail.", GATEMP_INDEX_DDP);
    }
    if (statusOp_Init(GATEMP_INDEX_PCM) == STATUSOP_INIT_FAIL)
    {
        TRACE_TERSE1("TaskAsdp: Gate Index %d:initialization status GateMP Fail.", GATEMP_INDEX_PCM);
    }
    if (statusOp_Init(GATEMP_INDEX_THD) == STATUSOP_INIT_FAIL)
    {
        TRACE_TERSE1("TaskAsdp: Gate Index %d:initialization status GateMP Fail.", GATEMP_INDEX_THD);
    }
    if (statusOp_Init(GATEMP_INDEX_DTS) == STATUSOP_INIT_FAIL)
    {
        TRACE_TERSE1("TaskAsdp: Gate Index %d:initialization status GateMP Fail.", GATEMP_INDEX_DTS);
    }
    if (statusOp_Init(GATEMP_INDEX_DSD) == STATUSOP_INIT_FAIL)
    {
        TRACE_TERSE1("TaskAsdp: Gate Index %d:initialization status GateMP Fail.", GATEMP_INDEX_DSD);
    }
    if (statusOp_Init(GATEMP_INDEX_AAC) == STATUSOP_INIT_FAIL)
    {
        TRACE_TERSE1("TaskAsdp: Gate Index %d:initialization status GateMP Fail.", GATEMP_INDEX_AAC);
    }
#endif
    //
    // End of Initialization -- final memory usage report.
    //
    if (pP->fxns->memStatusPrint)
    {
        pP->fxns->memStatusPrint("ASDT MEMSTAT REPORT",
            HEAP_INTERNAL, HEAP_INTERNAL1, HEAP_EXTERNAL, 
            HEAP_INTERNAL1_SHM, HEAP_EXTERNAL_SHM, HEAP_EXTERNAL_NONCACHED_SHM);
    }
    
    // write back Status structure addresses for Beta Units initialized on Slave
    Cache_wb((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_DECODE]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_wb((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_PCM]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_wb((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_PCM2]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_wb((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_DDP]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_wb((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_DDP2]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_wb((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_THD]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_wb((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_THD2]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_wb((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_DTSUHDA]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_wb((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_AAC]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_wb((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_AAC2]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_wb((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_DSD]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_wb((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_DSD2]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_wait();
    
    // Send initialization complete message to master
    queId = MessageQ_getReplyQueue(pAspMsg);
    pAspMsg->procId = hAspMsgSlave->slaveProcId;
    pAspMsg->cmd = ASP_MASTER_START_DONE;
    pAspMsg->messageId = hAspMsgSlave->masterMessageId | ((UInt32)1<<31);
    TRACE_MSG3("Tx ASP message, procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);
    status = MessageQ_put(queId, (MessageQ_Msg)pAspMsg); /* send message back */
    if (status != MessageQ_S_SUCCESS)
    {
        SW_BREAKPOINT;
    }

    done = FALSE;
    while (done==FALSE)
    {
        // wait for source select message from master
        do {
            status = MessageQ_get(hAspMsgSlave->slaveQue, (MessageQ_Msg *)&pAspMsg, MessageQ_FOREVER);
        } while ((status < 0) || (pAspMsg->cmd != ASP_SLAVE_DEC_SOURCE_SELECT));
        if ((pAspMsg->procId != hAspMsgSlave->masterProcId) ||
            (pAspMsg->cmd != ASP_SLAVE_DEC_SOURCE_SELECT))
        {
            TRACE_TERSE3("ERROR: Rx ASP message: procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);
            SW_BREAKPOINT;
        }            
        hAspMsgSlave->masterMessageId = pAspMsg->messageId; 
        sourceSelect = *(Int32 *)&pAspMsg->buf[0];
        TRACE_MSG3("Rx ASP message, procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);
        TRACE_MSG1("sourceSelect=%d.", sourceSelect);
        // send source select complete message to master
        queId = MessageQ_getReplyQueue(pAspMsg);
        pAspMsg->procId = hAspMsgSlave->slaveProcId;
        pAspMsg->cmd = ASP_MASTER_DEC_SOURCE_SELECT_DONE;
        pAspMsg->messageId = hAspMsgSlave->masterMessageId | ((UInt32)1<<31);
        gSlaveSourceSelectCnt++;
        TRACE_MSG3("Tx ASP message, procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);
        MessageQ_put(queId, (MessageQ_Msg)pAspMsg); /* send message back */
        
        for (z=DECODE1; z < DECODEN; z++)
        {
            alg[z] = pAstCfg->xDec[z].decAlg[PAF_SOURCE_PCM];
        }
        alg[zMD] = pAstCfg->xDec[zMD].decAlg[sourceSelect];

#if 0 // debug, reset IB capture buffer
        capIbReset();
        Log_info0("capIbReset()");
#endif
        
#if 0 // debug, reset audio frame capture buffer
        capAfReset();
        Log_info0("capAfReset()");
#endif
        
        decDone = FALSE;
        outIsDecInfo1Flag = 0;
        outIsDecDecode1Flag = 0;
        while (decDone==FALSE)
        {
            // wait for received message from master
            do {
                status = MessageQ_get(hAspMsgSlave->slaveQue, (MessageQ_Msg *)&pAspMsg, MessageQ_FOREVER);
            } while (status < 0);
            if (pAspMsg->procId != hAspMsgSlave->masterProcId)
            {
                TRACE_TERSE3("ERROR: Rx ASP message: procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);
                SW_BREAKPOINT;
            }
            hAspMsgSlave->masterMessageId = pAspMsg->messageId; 
            slaveCmd = pAspMsg->cmd;
            TRACE_MSG3("Rx ASP message, procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);

            switch (slaveCmd)
            {
                case ASP_SLAVE_NULL:
                case ASP_SLAVE_START:
                    gSlaveStartErrCnt++;
                    TRACE_TERSE1("ERROR: unexpected slaveCmd=%d", slaveCmd);
                    
                    break;
                    
                case ASP_SLAVE_EXIT:
                    gSlaveExitCnt++;
                    TRACE_TERSE1("slaveCmd=%d", slaveCmd);
                    
                    decDone = TRUE;
                    done = TRUE;
                    break;
                    
                case ASP_SLAVE_DEC_EXIT:
                    gSlaveDecExitCnt++;
                    TRACE_TERSE1("slaveCmd=%d", slaveCmd);
                    
                    // send dec exit complete message to master
                    queId = MessageQ_getReplyQueue(pAspMsg);
                    pAspMsg->procId = hAspMsgSlave->slaveProcId;
                    pAspMsg->cmd = ASP_MASTER_DEC_EXIT_DONE;
                    pAspMsg->messageId = hAspMsgSlave->masterMessageId | ((UInt32)1<<31);
                    TRACE_MSG3("Tx ASP message, procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);
                    status = MessageQ_put(queId, (MessageQ_Msg)pAspMsg); /* send message back */
                    if (status != MessageQ_S_SUCCESS)
                    {
                        SW_BREAKPOINT;
                    }
                    
                    decDone=TRUE;
                    break;
                    
                case ASP_SLAVE_DEC_CONTROL:
                    gSlaveDecControlCnt++;
                    
                    argIdx = 0; // get decIdx
                    z = *(Int32 *)&pAspMsg->buf[argIdx];
                    argIdx += sizeof(Int32);
                    decCtrlCmd = *(IALG_Cmd *)&pAspMsg->buf[argIdx]; // get decCtrlCmd
                    TRACE_MSG3("slaveCmd=%d, decIdx=%d, decCtrlCmd=%d", slaveCmd, z, decCtrlCmd);
                    
                    decCtrlRet = alg[z]->fxns->algControl(alg[z], decCtrlCmd, NULL);

                    // send dec control complete message to master
                    queId = MessageQ_getReplyQueue(pAspMsg);
                    pAspMsg->procId = hAspMsgSlave->slaveProcId;
                    pAspMsg->cmd = ASP_MASTER_DEC_CONTROL_DONE;
                    pAspMsg->messageId = hAspMsgSlave->masterMessageId | ((UInt32)1<<31);
                    argIdx = 0; // set decCtrlRet
                    *(Int32 *)&pAspMsg->buf[argIdx] = decCtrlRet;
                    TRACE_MSG3("Tx ASP message, procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);
                    status = MessageQ_put(queId, (MessageQ_Msg)pAspMsg); /* send message back */
                    if (status != MessageQ_S_SUCCESS)
                    {
                        SW_BREAKPOINT;
                    }

                    break;
                    
                case ASP_SLAVE_DEC_ACTIVATE:
                    gSlaveDecActivateCnt++;
                    
                    argIdx = 0; // get decIdx
                    z = *(Int32 *)&pAspMsg->buf[argIdx];
                    TRACE_MSG2("slaveCmd=%d, decIdx=%d", slaveCmd, z);
                    
                    // invalidate Dec configuration
                    Cache_inv(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
                    Cache_wait();
                    
                    if (alg[z]->fxns->algActivate)
                    {
                        alg[z]->fxns->algActivate(alg[z]);
                    }

                    // Start writes to circular buffer
                    cbErrno = cbWriteStart(pCbCtl, z);
                    if (cbErrno < 0)
                    {
                        SW_BREAKPOINT;
                    }
                    gCbWrtAfErrCnt=0;       // reset write circular buffer error count
                    gDecOpCbWrtAfOvr=0;     // reset decoder output circular buffer overflow count
                    gMaxDecOpCbWrtAfOvr=0;  // reset max decoder output circular buffer overflow count
                    gSlaveCbResetCnt=0;     // reset slave circular buffer reset count
                    // Log circular buffer control variables (debug)
                    cbLog(pCbCtl, z, 1, "cbWriteStart");
                    
                    // Reset audio frame
                    resetAf(pP, z, sourceSelect);
                    
                    // Initialize OutIS Dec flags
                    outIsDecInfo1Flag = 0;      // OutIS Dec Info1 hasn't executed
                    outIsDecDecode1Flag = 0;    // OutIS Dec Decode1 hasn't executed
                    
                    // send dec activate complete message to master
                    queId = MessageQ_getReplyQueue(pAspMsg);
                    pAspMsg->procId = hAspMsgSlave->slaveProcId;
                    pAspMsg->cmd = ASP_MASTER_DEC_ACTIVATE_DONE;
                    pAspMsg->messageId = hAspMsgSlave->masterMessageId | ((UInt32)1<<31);
                    TRACE_MSG3("Tx ASP message, procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);
                    status = MessageQ_put(queId, (MessageQ_Msg)pAspMsg); /* send message back */
                    if (status != MessageQ_S_SUCCESS)
                    {
                        SW_BREAKPOINT;
                    }
                   
                    break;
                    
                case ASP_SLAVE_DEC_RESET:
                    gSlaveDecResetCnt++;
                    
                    argIdx = 0; // get decIdx
                    z = *(Int32 *)&pAspMsg->buf[argIdx];
                    TRACE_TERSE2("slaveCmd=%d,decIdx=%d", slaveCmd, z);
                    
                    dec = (DEC_Handle)alg[z];
                    errno = 0;
                    if (dec->fxns->reset)
                    {
                        //
                        // Execute decode reset
                        //
                        errno = dec->fxns->reset(dec, NULL, &pAstCfg->xDec[z].decodeControl, &pAstCfg->xDec[z].decodeStatus);

                        // Get pointer to Dec Reset output audio frame
                        pDecCtrlAf = pAstCfg->xDec[z].decodeControl.pAudioFrame;

                        // Perform Dec Reset Init-Sync
                        //  - Write Dec Reset output audio frame
                        //  - Set Dec Reset decoder stage flag
                        outIsErrno = outIsWriteDecStageFlagAndAf(pOutIsCtl, z, 
                            ASP_OUTIS_DEC_STAGE_RESET_IDX, 1, pDecCtrlAf);
                        if (outIsErrno < 0)
                        {
                            SW_BREAKPOINT; // debug
                        }
                    }

                    // write back Dec configuration
                    Cache_wb(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
                    Cache_wait();            

                    // send dec reset complete message to master
                    queId = MessageQ_getReplyQueue(pAspMsg);
                    pAspMsg->procId = hAspMsgSlave->slaveProcId;
                    pAspMsg->cmd = ASP_MASTER_DEC_RESET_DONE;
                    pAspMsg->messageId = hAspMsgSlave->masterMessageId | ((UInt32)1<<31);
                    argIdx = 0; // set decErrno
                    *(Int32 *)&pAspMsg->buf[argIdx] = errno;
                    TRACE_MSG3("Tx ASP message, procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);
                    status = MessageQ_put(queId, (MessageQ_Msg)pAspMsg); /* send message back */
                    if (status != MessageQ_S_SUCCESS)
                    {
                        SW_BREAKPOINT;
                    }

                    break;
                    
                case ASP_SLAVE_DEC_INFO:
                    gSlaveDecInfoCnt++;
                    
                    argIdx = 0; // get decIdx
                    z = *(Int32 *)&pAspMsg->buf[argIdx];
                    TRACE_TERSE2("slaveCmd=%d,decIdx=%d", slaveCmd, z);
                    // Get input associated w/ decoder
                    zI = pP->inputsFromDecodes[z];

                    // invalidate Inp configuration
                    Cache_inv(&pAstCfg->xInp[zI], sizeof(PAF_AST_InpBuf), Cache_Type_ALLD, 0);
                    Cache_wait();
                    // invalidate input data
                    pIpBufConfig = &pAstCfg->xInp[zI].inpBufConfig;
                    size = pIpBufConfig->frameLength * pIpBufConfig->sizeofElement;
                    if (sourceSelect == PAF_SOURCE_PCM ||
                        sourceSelect == PAF_SOURCE_DSD1 ||
                        sourceSelect == PAF_SOURCE_DSD2 ||
                        sourceSelect == PAF_SOURCE_DSD3)
                    {
                        size *= pIpBufConfig->stride;
                    }

                    bufEnd = (Int) pIpBufConfig->base.pVoid + pIpBufConfig->sizeofBuffer;
                    currentBufSize = (bufEnd - (Int)pIpBufConfig->pntr.pSmInt);
                    if (currentBufSize >= size)
                    {
                        chunkSize = size;
                    }
                    else
                    {
                        chunkSize = currentBufSize;
                    }
                    wrapSize = size - chunkSize;
                    // invalidate input data
                    Cache_inv((Ptr)pIpBufConfig->pntr.pSmInt, chunkSize, Cache_Type_ALLD, 0);
                    // invalidate Dec configuration
                    TRACE_MSG2("IBUF : pIpBufConfig->pntr.pSmInt: 0x%x and chunkSize: %d", (IArg)pIpBufConfig->pntr.pSmInt, chunkSize);
                    Cache_inv(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
                    // status for selected decoder should be invalidated
                    Cache_wait();

                    /* Circular buffer wrap condition*/
                    //if(((Int) pIpBufConfig->head.pVoid + size)  > bufEnd)
                    if(wrapSize > 0)
                    {
                        // invalidate input data
                        Cache_inv((Ptr)pIpBufConfig->base.pSmInt, wrapSize, Cache_Type_ALLD, 0);
                        TRACE_MSG2("IBUF : pIpBufConfig->base.pSmInt: 0x%x and wrapSize: %d", (IArg)pIpBufConfig->base.pSmInt, wrapSize);
                        // status for selected decoder should be invalidated
                        Cache_wait();
                    }
                    /* Circular buffer wrap condition */

                    dec = (DEC_Handle)alg[z];
                    errno = 0;
                    if (dec->fxns->info)
                    {
                        //
                        // Execute decode info
                        //
                        pfpBegin(PFP_ID_ASDT_1, pAsdtCfg->taskHandle);
                        errno = dec->fxns->info(dec, NULL, 
                            &pAstCfg->xDec[z].decodeControl, 
                            &pAstCfg->xDec[z].decodeStatus);
                        pfpEnd(PFP_ID_ASDT_1, PFP_FINISH_MEAS);

                        //
                        // Execute Dec Info1 Init-Sync if required
                        //
                        if (outIsDecInfo1Flag == 0)
                        {
                            // Get pointer to Dec Info1 output audio frame
                            pDecCtrlAf = pAstCfg->xDec[z].decodeControl.pAudioFrame;

                            if (checkDecSync(pDecCtrlAf) == 1)
                            {
                                // Perform Dec Info1 Init-Sync
                                //  - Write Dec Info1 output audio frame
                                //  - Set Dec Info1 decoder stage flag
                                outIsErrno = outIsWriteDecStageFlagAndAf(pOutIsCtl, z, 
                                    ASP_OUTIS_DEC_STAGE_INFO1_IDX, 1, pDecCtrlAf);
                                if (outIsErrno < 0)
                                {
                                    SW_BREAKPOINT; // debug
                                }

                                outIsDecInfo1Flag = 1;  // OutIS Dec Info1 has executed
                            }
                        }
                    }
                    
                    // write back Dec configuration
                    Cache_wb(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
                    Cache_wait();            
                    
#ifdef NON_CACHE_STATUS
                    statusOp_read(&(temp8),
                                  &(pAstCfg->xDec[z].decodeStatus.mode),
                                  sizeof(Int8),
                                  GATEMP_INDEX_DEC);
                    // Re-initialize audio frame if decoder is disabled or
                    // doesn't have a valid input
                    if (!temp8 || !pAstCfg->xInp[zI].hRxSio)
                    {
                        pP->fxns->initFrame1(pP, pQ, pAsdtCfg, z, 0);
                    }
#else
                    // Re-initialize audio frame if decoder is disabled or
                    // doesn't have a valid input
                    if (!pAstCfg->xDec[z].decodeStatus.mode || !pAstCfg->xInp[zI].hRxSio)
                    {
                        pP->fxns->initFrame1(pP, pQ, pAsdtCfg, z, 0);
                    }
#endif

                    // send dec info complete message to master
                    queId = MessageQ_getReplyQueue(pAspMsg);
                    pAspMsg->procId = hAspMsgSlave->slaveProcId;
                    pAspMsg->cmd = ASP_MASTER_DEC_INFO_DONE;
                    pAspMsg->messageId = hAspMsgSlave->masterMessageId | ((UInt32)1<<31);
                    argIdx = 0; // set decErrno
                    *(Int32 *)&pAspMsg->buf[argIdx] = errno;
                    TRACE_MSG3("Tx ASP message, procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);
                    status = MessageQ_put(queId, (MessageQ_Msg)pAspMsg); /* send message back */
                    if (status != MessageQ_S_SUCCESS)
                    {
                        SW_BREAKPOINT;
                    }
                    
                    break;
                    
                case ASP_SLAVE_DEC_DECODE:
                    gSlaveDecDecodeCnt++;
                    
                    argIdx = 0; // get decIdx
                    z = *(Int32 *)&pAspMsg->buf[argIdx];
                    TRACE_TERSE2("slaveCmd=%d, decIdx=%d", slaveCmd, z);
                    // Get input associated w/ decoder
                    zI = pP->inputsFromDecodes[z];
                    
                    // Reset AF samsiz
                    resetAfSamsiz(z);
                    
                    // Invalidate Dec configuration
                    Cache_inv(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
                    Cache_wait();
                    //TRACE_TERSE0("Dec:cache wb done");
            
                    dec = (DEC_Handle)alg[z];
                    //TRACE_TERSE1("Dec:dec handle=0x%04x", (IArg)dec);

                    errno = 0;
                    cbErrno = 0;
                    if (dec->fxns->decode)
                    {
#if 0 // debug, capture input buffer
                        capIb(pAstCfg->xInp[z].pInpBuf);
#endif
                        
                        //
                        // Execute decode
                        //
                        pfpBegin(PFP_ID_ASDT_2, pAsdtCfg->taskHandle);
                        errno = dec->fxns->decode(dec, NULL, &pAstCfg->xDec[z].decodeInStruct, &pAstCfg->xDec[z].decodeOutStruct);
                        //decode is done
                        pfpEnd(PFP_ID_ASDT_2, PFP_FINISH_MEAS);
                        if (errno < 0)
                        {
                            //SW_BREAKPOINT;
                        }
                        TRACE_TERSE0("Dec:decode done");

                        // Get pointer to Dec Decode output audio frame
                        pAfWrt = pAstCfg->xDec[z].decodeOutStruct.pAudioFrame;                           
                        TRACE_TERSE2("Dec:pAfWrt=0x%04x, nSamples=%d", (IArg)pAfWrt, pAfWrt->data.nSamples);
                        
#if 0 // debug, capture audio frame
                        if (capAfWrite(pAfWrt, PAF_LEFT) != CAP_AF_SOK)
                        {
                            Log_info0("capAfWrite() error");
                        }
#endif

                        #ifdef CAPTURE_DECODER_OUTSAMPLES_PP
                        if (tempCap_frameCnt < CAP_FRAME_MAX)
                        {
                            tempCap_decSampleOut[tempCap_frameCnt] = pAfWrt->sampleCount;
                            // alternative toggle
                            {
                                static Uint8 toggleState = 0;
                                if (toggleState == 0)
                                    GPIOSetOutput(GPIO_PORT_1, GPIO_PIN_25);
                                else
                                    GPIOClearOutput(GPIO_PORT_1, GPIO_PIN_25);
                            	toggleState = ~(toggleState);
                            }
                            tempCap_frameCnt++;
                        }
                        #endif

                        //
                        // Execute Dec Decode1 Init-Sync if required
                        //
                        if (outIsDecDecode1Flag == 0)
                        {
                            // Initialize decoder output circular buffer
                            errno = cbInitDecWrite(pCbCtl, z, sourceSelect,
                                pAstCfg->xDec[z].decodeControl.frameLength, 
                                0, pAfWrt);
                            if (errno)
                            {
                                SW_BREAKPOINT; // debug
                            }
                            // debug
                            cbLog(pCbCtl, z, 1, "cbInitSourceSel");
                            
                            // Perform Dec Decode1 Init-Sync
                            //  - Write Dec Decode1 output audio frame
                            //  - Set Dec Decode1 decoder stage flag
                            outIsErrno = outIsWriteDecStageFlagAndAf(pOutIsCtl, z, 
                                ASP_OUTIS_DEC_STAGE_DECODE1_IDX, 1, pAfWrt);
                            if (outIsErrno < 0)
                            {
                                SW_BREAKPOINT; // debug
                            }

                            outIsDecDecode1Flag = 1;    // OutIS Dec Decode1 has executed
                        }

                        //
                        // Write decoder output audio frame to circular buffer
                        //

                        // debug
                        //pCb = &pAstCfg->xDecOpCb[z];
                        //TRACE_TERSE1("Dec:pCb=0x%04x", (IArg)pCb);                                                

                        //GPIOSetOutput(GPIO_PORT_0, GPIO_PIN_99);      // debug
                        cbErrno = cbWriteAf(pCbCtl, z, pAfWrt);
                        //GPIOClearOutput(GPIO_PORT_0, GPIO_PIN_99);    // debug
                        if ((cbErrno < 0) && 
                            (cbErrno != ASP_DECOP_CB_AF_WRITE_OVERFLOW) && 
                            (cbErrno != ASP_DECOP_CB_PCM_WRITE_OVERFLOW))
                        {
                            gCbWrtAfErrCnt++;
                            //SW_BREAKPOINT; // debug
                        }
                        
                        // Handle circular buffer overflows
                        if ((cbErrno == ASP_DECOP_CB_AF_WRITE_OVERFLOW) ||
                            (cbErrno == ASP_DECOP_CB_PCM_WRITE_OVERFLOW))
                        {
                            gDecOpCbWrtAfOvr++; // increment circular buffer overflow count
                            if (gDecOpCbWrtAfOvr >= DEC_OP_CB_WRTAF_OVR_THR)
                            {
                                gMaxDecOpCbWrtAfOvr = DEC_OP_CB_WRTAF_OVR_THR; // update max overflow count
                                gDecOpCbWrtAfOvr = 0; // reset overflow count
                                
                                // Reset circular buffer
                                cbReset(pCbCtl, z);
                                gSlaveCbResetCnt++; // increment slave circular buffer reset count
                                Log_info0("ASDT:cbReset");
                            }
                        }
                        else if ((cbErrno == ASP_DECOP_CB_SOK) && (gDecOpCbWrtAfOvr > 0))
                        {
                            // No overflow detected.
                            // update max overflow count,
                            // reset overflow count
                            
                            if (gDecOpCbWrtAfOvr > gMaxDecOpCbWrtAfOvr)
                            {
                                gMaxDecOpCbWrtAfOvr = gDecOpCbWrtAfOvr;
                            }
                            gDecOpCbWrtAfOvr = 0; // reset circular buffer overflow count
                            
                            cbErrno = 0; // don't return error condition
                        }
                        //GPIOClearOutput(GPIO_PORT_0, GPIO_PIN_107);   // debug
                        TRACE_TERSE0("Dec:cbWriteAf() complete");
                        
#if 0 // debug
                        // Shows timing of CB write
                        // ADC B9
                        {
                            static Uint8 toggleState = 0;
                            if (toggleState == 0)
                                GPIOSetOutput(GPIO_PORT_0, GPIO_PIN_107);
                            else
                                GPIOClearOutput(GPIO_PORT_0, GPIO_PIN_107);
                            toggleState = ~(toggleState);
                        }
#endif        
                        
                        // Log circular buffer control variables (debug)
                        cbLog(pCbCtl, z, 1, "cbWriteAf");
                    }
                    
                    // write back Dec configuration
                    Cache_wb(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
                    Cache_wait();
                    
#ifdef NON_CACHE_STATUS
                    statusOp_read(&(temp8),
                                  &(pAstCfg->xDec[z].decodeStatus.mode),
                                  sizeof(Int8),
                                  GATEMP_INDEX_DEC);
                    // Re-initialize audio frame if decoder is disabled or 
                    // doesn't have a valid input 
                    if (!temp8 || !pAstCfg->xInp[zI].hRxSio)
                    {
                        pP->fxns->initFrame1(pP, pQ, pAsdtCfg, z, 0);
                    }
#else
                    // Re-initialize audio frame if decoder is disabled or
                    // doesn't have a valid input
                    if (!pAstCfg->xDec[z].decodeStatus.mode || !pAstCfg->xInp[zI].hRxSio)
                    {
                        pP->fxns->initFrame1(pP, pQ, pAsdtCfg, z, 0);
                    }
#endif
                    // send dec info complete message to master
                    queId = MessageQ_getReplyQueue(pAspMsg);
                    pAspMsg->procId = hAspMsgSlave->slaveProcId;
                    pAspMsg->cmd = ASP_MASTER_DEC_DECODE_DONE;
                    pAspMsg->messageId = hAspMsgSlave->masterMessageId | ((UInt32)1<<31);
                    argIdx = 0; // set decErrno
                    *(Int32 *)&pAspMsg->buf[argIdx] = errno;
                    argIdx += sizeof(Int32); // set cbErrno
                    *(Int32 *)&pAspMsg->buf[argIdx] = cbErrno;                    
                    TRACE_MSG3("Tx ASP message, procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);
                    status = MessageQ_put(queId, (MessageQ_Msg)pAspMsg); /* send message back */
                    if (status != MessageQ_S_SUCCESS)
                    {
                        SW_BREAKPOINT;
                    }
                    
                    break;
                    
                case ASP_SLAVE_DEC_DEACTIVATE:
                    gSlaveDecDeactivateCnt++;
                    
                    argIdx = 0; // get decIdx
                    z = *(Int32 *)&pAspMsg->buf[argIdx];
                    TRACE_TERSE2("slaveCmd=%d, decIdx=%d", slaveCmd, z);
                    
                    if (alg[z]->fxns->algDeactivate)
                    {
                        alg[z]->fxns->algDeactivate(alg[z]);
                    }
                    
                    // Stop writes to circular buffer
                    cbErrno = cbWriteStop(pCbCtl, z);
                    if (cbErrno < 0)
                    {
                        SW_BREAKPOINT; // debug
                    }
                    // Log circular buffer control variables (debug)
                    cbLog(pCbCtl, z, 1, "cbWriteStop");
                    
                    // Reset Output Init-Sync decoder stage flags
                    outIsErrno = outIsResetDecStageFlags(pOutIsCtl, z);
                    if (outIsErrno < 0)
                    {
                        SW_BREAKPOINT; // debug
                    }
                    
                    // send dec deactivate complete message to master
                    queId = MessageQ_getReplyQueue(pAspMsg);
                    pAspMsg->procId = hAspMsgSlave->slaveProcId;
                    pAspMsg->cmd = ASP_MASTER_DEC_DEACTIVATE_DONE;
                    pAspMsg->messageId = hAspMsgSlave->masterMessageId | ((UInt32)1<<31);
                    TRACE_MSG3("Tx ASP message, procId=%d, cmd=%d, messageId=0x%04x", pAspMsg->procId, pAspMsg->cmd, pAspMsg->messageId);
                    status = MessageQ_put(queId, (MessageQ_Msg)pAspMsg); /* send message back */
                    if (status != MessageQ_S_SUCCESS)
                    {
                        SW_BREAKPOINT;
                    }
                    
                    break;
                    
                default:
                    TRACE_TERSE1("ERROR: invalid slaveCmd=%d", slaveCmd);
                    break;
            }
        }
    }
    
    Log_info0("exit taskAsdpFxn()");   
}


// -----------------------------------------------------------------------------
// AST Initialization Function - Memory Allocation
//
//   Name:      PAF_AST_initPhaseMalloc
//   Purpose:   Audio Stream Task Function for initialization of data pointers
//              by allocation of memory.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on MEM_calloc failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//
Int
PAF_ASDT_initPhaseMalloc(
    const PAF_ASDT_Params *pP, 
    const PAF_ASDT_Patchs *pQ, 
    PAF_ASDT_Config *pAsdtCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int zMS;
    Error_Block eb;

    pAstCfg = pAsdtCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    zMS = pAstCfg->masterStr;
    
    TRACE_TERSE1("PAF_ASDT_initPhaseMalloc: AS%d: initialization phase - memory allocation", as+zMS);

    // Initialize error block
    Error_init(&eb); 

    if (!(gpDecAudioFrame = (PAF_AudioFrame *)Memory_calloc((IHeap_Handle)HEAP_EXTERNAL,
        DECODEN * sizeof (PAF_AudioFrame), 4, &eb)))
    {
        TRACE_TERSE1("PAF_ASDT_initPhaseMalloc: AS%d: Memory_calloc failed", as+zMS);
        SW_BREAKPOINT;
        return __LINE__;
    }
    TRACE_TERSE3("PAF_ASDT_initPhaseMalloc. (gpAudioFrameSlave) %d bytes from space %d at 0x%x.",
            DECODEN * sizeof (PAF_AudioFrame),
            HEAP_ID_INTERNAL1, (IArg)gpDecAudioFrame);

    TRACE_TERSE1("PAF_ASDT_initPhaseMalloc: AS%d: initialization phase - memory allocation complete.", as+zMS);
    TRACE_TERSE1("PAF_ASDT_initPhaseMalloc: AS%d: initialization phase - memory allocation complete.", as+zMS);
    return 0;
} //PAF_ASDT_initPhaseMalloc

// -----------------------------------------------------------------------------
// AST Initialization Function - Memory Initialization from Configuration
//
//   Name:      PAF_AST_initPhaseConfig
//   Purpose:   Audio Stream Task Function for initialization of data values
//              from parameters.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Other as per initFrame0 and initFrame1.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_ASDT_initPhaseConfig(
    const PAF_ASDT_Params *pP, 
    const PAF_ASDT_Patchs *pQ, 
    PAF_ASDT_Config *pAsdtCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                    /* Audio Stream Number (1, 2, etc.) */
    Int zMS;
    Int z;

    pAstCfg = pAsdtCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    zMS = pAstCfg->masterStr;
    
    TRACE_TERSE1("PAF_ASDT_initPhaseConfig: AS%d: initialization phase - configuration", as+zMS);

    // overwrite pointer to audio frame in framework decode control
    for (z=DECODE1; z < DECODEN; z++) 
    {
        pAstCfg->xDec[z].decodeControl.pAudioFrame = &gpDecAudioFrame[z];
        pAstCfg->xDec[z].decodeInStruct.pAudioFrame = &gpDecAudioFrame[z];
        pAstCfg->xDec[z].decodeStatus = *pP->z_pDecodeStatus[z];
        pP->fxns->initFrame0(pP, pQ, pAsdtCfg, z);
    }

    return 0;
}  //PAF_ASDT_initPhaseConfig

// -----------------------------------------------------------------------------
// AST Initialization Function - ACP Algorithm Instantiation
//
//   Name:      PAF_AST_initPhaseAcpAlg
//   Purpose:   Audio Stream Task Function for initialization of ACP by
//              instantiation of the algorithm.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on ACP Algorithm creation failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//
Int
PAF_ASDT_initPhaseAcpAlg(
    const PAF_ASDT_Params *pP, 
    const PAF_ASDT_Patchs *pQ, 
    PAF_ASDT_Config *pAsdtCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                     /* input/encode/stream/decode/output counter */
    Int betaPrimeOffset;
    ACP_Handle acp;
    Int zMS;
    Int zS; //, zX;

    pAstCfg = pAsdtCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    zMS = pAstCfg->masterStr;    

    TRACE_TERSE1("PAF_ASDT_initPhaseAcpAlg: AS%d: initialization phase - ACP Algorithm", as+zMS);

    ACP_MDS_init();

    if (!(acp = (ACP_Handle)ACP_MDS_create(NULL))) 
    {
        TRACE_TERSE1("PAF_ASDT_initPhaseAcpAlg: AS%d: ACP algorithm instance creation  failed", as+zMS);
        return __LINE__;
    }
    pAsdtCfg->acp = acp;

    ((ALG_Handle)acp)->fxns->algControl((ALG_Handle)acp,
        ACP_GETBETAPRIMEOFFSET, (IALG_Status *)&betaPrimeOffset);

    for (z=DECODE1; z < DECODEN; z++) 
    {
        zS = pP->streamsFromDecodes[z];
        acp->fxns->attach(acp, ACP_SERIES_STD,
            STD_BETA_DECODE + betaPrimeOffset * (as-1+zS),
            (IALG_Status *)&pAstCfg->xDec[z].decodeStatus);
        /* Ignore errors, not reported. */
    }

    TRACE_TERSE1("PAF_ASDT_initPhaseAcpAlg: AS%d: initialization phase - ACP Algorithm complete.", as+zMS);

    return 0;
} //PAF_AST_initPhaseAcpAlg

// -----------------------------------------------------------------------------
// AST Initialization Function - Common Memory and Algorithms
//
//   Name:      PAF_AST_initPhaseCommon
//   Purpose:   Audio Stream Task Function for initialization of data pointers
//              by allocation for common memory and by instantiation for
//              algorithms.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on PAF_ALG_alloc failure.
//              Source code line number on PAF_ALG_mallocMemory failure.
//              Source code line number on Decode Chain initialization failure.
//              Source code line number on ASP Chain initialization failure.
//              Source code line number on Encode Chain initialization failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//

#include <pafsio_ialg.h>

Int
PAF_ASDT_initPhaseCommon(
    const PAF_ASDT_Params *pP, 
    const PAF_ASDT_Patchs *pQ, 
    PAF_ASDT_Config *pAsdtCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                         /* Audio Stream Number (1, 2, etc.) */
    Int z;                          /* stream counter */
    //Int g;                        /* gear */
    ACP_Handle acp;
    PAF_IALG_Config pafAlgConfig;
    IALG_MemRec common[3][PAF_IALG_COMMON_MEMN+1];
   
    acp = pAsdtCfg->acp; // get acp handle
    pAstCfg = pAsdtCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    
    TRACE_TERSE0("PAF_ASDT_initPhaseCommon: initialization phase - Common Memory");

    //
    // Determine memory needs and instantiate algorithms across audio streams
    //

    TRACE_TERSE0("PAF_ASDT_initPhaseCommon: calling PAF_ALG_setup.");
    PAF_ALG_setup(&pafAlgConfig, 
        HEAP_ID_INTERNAL,               HEAP_INTERNAL, 
        HEAP_ID_INTERNAL1,              HEAP_INTERNAL1, 
        HEAP_ID_EXTERNAL,               HEAP_EXTERNAL, 
        HEAP_ID_INTERNAL1_SHM,          HEAP_INTERNAL1_SHM, 
        HEAP_ID_EXTERNAL_SHM,           HEAP_EXTERNAL_SHM, 
        HEAP_ID_EXTERNAL_NONCACHED_SHM, HEAP_EXTERNAL_NONCACHED_SHM,
        HEAP_CLEAR);

    if (pP->fxns->headerPrint)
        pP->fxns->headerPrint();

    for (z=STREAM1; z < STREAMN; z++) 
    {
        //Int zD, zE, zX;
        Int zD, zX;

        TRACE_TERSE1("PAF_ASDT_initPhaseCommon: AS%d: initialization phase - Common Algorithms", as+z);

        //
        // Determine common memory needs of Decode Algorithms
        //
        PAF_ALG_init (common[z], lengthof (common[z]), COMMONSPACE);

        zD = -1;
        for (zX = DECODE1; zX < DECODEN; zX++) 
        {
            if (pP->streamsFromDecodes[zX] == z) 
            {
                zD = zX;
                break;
            }
        }

        if (zD >= 0) 
        {
            TRACE_TERSE1("PAF_ASDT_initPhaseCommon: calling PAF_ALG_ALLOC for decoder common[%d].", z);
            if (PAF_ALG_ALLOC (decLinkInit[zD-DECODE1], common[z])) 
            {
                TRACE_TERSE3("AS%d: %s.%d: PAF_ALG_alloc failed", as+z, (IArg)__FUNCTION__, __LINE__);
                TRACE_TERSE2("Failed to alloc %d bytes from space %d", common[z]->size, common[z]->space);

                SW_BREAKPOINT;
                return __LINE__;
            }
            TRACE_TERSE3("alloced %d bytes from space %d at 0x%x", common[z]->size, common[z]->space, (IArg)common[z]->base);
            if(pP->fxns->allocPrint)
                pP->fxns->allocPrint ((const PAF_ALG_AllocInit *)(decLinkInit[z-DECODE1]),sizeof (*(decLinkInit[z-DECODE1])), &pafAlgConfig);
        }
    }
    {
        // Changes made to share scratch between zones
        // Assume maximum 3 zones and scratch common memory is at offset 0;
        int max=0;
        for (z=STREAM1; z < STREAMN; z++)
        {
            if (max<common[z][0].size)
                max=common[z][0].size;
        }
        common[STREAM1][0].size=max;
        for (z=STREAM1+1; z < STREAMN; z++)
            common[z][0].size=0;
    }
    
    //
    // Provide common memory needs of Decode Algorithms
    //
    for (z=STREAM1; z < STREAMN; z++) 
    {
        //Int zD, zE, zX;
        Int zD, zX;

        zD = -1;        
        for (zX = DECODE1; zX < DECODEN; zX++) 
        {
           if (pP->streamsFromDecodes[zX] == z) 
           {
               zD = zX;
               break;
           }
        }

        TRACE_TERSE0("PAF_ASDT_initPhaseCommon: calling PAF_ALG_mallocMemory for common space.");
        if (PAF_ALG_mallocMemory (common[z], &pafAlgConfig)) 
        {
            TRACE_TERSE3("AS%d: %s.%d: PAF_ALG_mallocMemory failed", as+z, (IArg)__FUNCTION__, __LINE__);
            TRACE_TERSE3("AS%d: z: %d.  Size 0x%x", as+z, z, common[z][0].size);
            SW_BREAKPOINT;
            return __LINE__;
        }
        TRACE_TERSE3("alloced %d bytes from space %d at 0x%x", common[z]->size, common[z]->space, (IArg)common[z]->base);
        // share zone0 scratch with all zones 
        common[z][0].base=common[0][0].base;
        if (pP->fxns->commonPrint)
                pP->fxns->commonPrint (common[z], &pafAlgConfig);

        //
        // Instantiate Decode Algorithms
        //
        if (zD >= 0) 
        {
            PAF_ASP_Chain *chain;
            TRACE_TERSE0("PAF_ASDT_initPhaseCommon: calling PAF_ASP_chainInit for decode.");
            chain =
                PAF_ASP_chainInit (&pAstCfg->xDec[zD].decChainData, pP->pChainFxns,
                                   HEAP_INTERNAL, as+z, acp, &trace,
                                   decLinkInit[zD-DECODE1], NULL, common[z], &pafAlgConfig);
            if (!chain) 
            {
                TRACE_TERSE1("PAF_ASDT_initPhaseCommon: AS%d: Decode chain initialization failed", as+z);
                return __LINE__;
            }
        }
    }
    TRACE_TERSE1("PAF_ASDT_initPhaseCommon: AS%d: Returning complete.", as+z);

    return 0;
} //PAF_ASDT_initPhaseCommon

// -----------------------------------------------------------------------------
// AST Initialization Function - Algorithm Keys
//
//   Name:      PAF_AST_initPhaseAlgKey
//   Purpose:   Audio Stream Task Function for initialization of data values
//              from parameters for Algorithm Keys.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
// .............................................................................
Int
PAF_ASDT_initPhaseAlgKey(
    const PAF_ASDT_Params *pP, 
    const PAF_ASDT_Patchs *pQ, 
    PAF_ASDT_Config *pAsdtCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* decode/encode counter */
    Int s;                      /* key number */
    PAF_ASP_Link *that;

    (void)as;  // clear warning.

    pAstCfg = pAsdtCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    
    TRACE_VERBOSE1("PAF_ASDT_initPhaseAlgKey: AS%d: initialization phase - Algorithm Keys", as);

    for (z=DECODE1; z < DECODEN; z++) 
    {
        for (s=0; s < pP->pDecAlgKey->length; s++) 
        {
            if ((pP->pDecAlgKey->code[s].full != 0)
                && (that = PAF_ASP_chainFind (&pAstCfg->xDec[z].decChainData, pP->pDecAlgKey->code[s])))
            {
                pAstCfg->xDec[z].decAlg[s] = (ALG_Handle )that->alg;
                    /* Cast in interface, for now --Kurt */
            }
            else
                pAstCfg->xDec[z].decAlg[s] = NULL;
        }
    }

    return 0;
} //PAF_AST_initPhaseAlgKey

//   Purpose:   Audio Stream Decode Task Function for initialization of the Audio
//              Frame(s) by memory allocation and loading of data pointers
//              and values.
Int
PAF_ASDT_initFrame0(
    const PAF_ASDT_Params *pP, 
    const PAF_ASDT_Patchs *pQ, 
    PAF_ASDT_Config *pAsdtCfg, 
    Int z
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                    /* Audio Stream Number (1, 2, etc.) */
    Int aLen;
    Int aSize = sizeof(PAF_AudioData);
    Int aAlign = aSize < sizeof (int) ? sizeof (int) : aSize;
    Int maxFrameLength = pP->maxFramelength;
    PAF_AudioData *aBuf=NULL;
    XDAS_UInt8 *metadataBuf;
    Error_Block    eb;
    PAF_AudioFrame *pAudioFrame;
    Int i;

    pAstCfg = pAsdtCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;

    pAudioFrame = &gpDecAudioFrame[z];
    if (pAudioFrame == NULL)
    {
        SW_BREAKPOINT;
    }
    
    // Initialize error block
    Error_init(&eb); 
    
    //maxFrameLength += PA_MODULO - maxFrameLength % PA_MODULO; // compute maximum framelength (needed for ARC support)
    //aLen = numchan[z] * maxFrameLength;
    aLen = numchan[z] * maxFrameLength + (maxFrameLength - FRAMELENGTH); /* Qin - need additional memory for starting offset
                                                                                  See line 1301 */

    //
    // Initialize audio frame elements directly
    //
    pAudioFrame->fxns = pP->pAudioFrameFunctions;
    pAudioFrame->data.nChannels = PAF_MAXNUMCHAN;
    pAudioFrame->data.nSamples = FRAMELENGTH;
    pAudioFrame->data.sample = gDecAudioFrameChannelPointers;
    pAudioFrame->data.samsiz = gDecAudioFrameChannelSizes;
    pAudioFrame->pChannelConfigurationMaskTable = &PAF_ASP_stdCCMT;

    //
    // Allocate memory for and initialize pointers to audio data buffers
    //
    //   The NUMCHANMASK is used to identify the channels for which data
    //   buffers can be allocated. Using this mask and switch statement
    //   rather than some other construct allows efficient code generation,
    //   providing just the code necessary (with significant savings).
    //
    if (pP->fxns->bufMemPrint)
    {
        pP->fxns->bufMemPrint(z, aLen*aSize, HEAP_ID_FRMBUF, 2);
    }

    TRACE_TERSE1("PAF_ASDT_initFrame0: AS%d: Memory_calloc for audio buffers", as+z);
    
    if (aLen != 0)
    {
        if (!(aBuf = (PAF_AudioData *)Memory_calloc((IHeap_Handle)HEAP_FRMBUF, aLen*aSize, aAlign, &eb)))
        {
            TRACE_TERSE1("PAF_ASDT_initFrame0: AS%d: Memory_calloc failed", as+z);
            TRACE_TERSE2("  maxFrameLength: %d.  aLen*aSize: %d", maxFrameLength, aLen*aSize);
            SW_BREAKPOINT;
            return __LINE__;
        }
    }

    TRACE_TERSE3("  maxFrameLength: %d.  aLen*aSize: %d.  aBuf: 0x%x", maxFrameLength, aLen*aSize, (IArg)aBuf);

    TRACE_TERSE1("PAF_ASDT_initFrame0: AS%d: Memory_calloc for metadata buffers", as+z);
    if (!(metadataBuf = (XDAS_UInt8 *)Memory_calloc((IHeap_Handle)HEAP_MDBUF, pP->pMetadataBufStatus->bufSize*pP->pMetadataBufStatus->NumBuf, pP->pMetadataBufStatus->alignment, &eb)))
    {
        TRACE_TERSE1("PAF_ASDT_initFrame0: AS%d: Memory_calloc failed", as+z);
        TRACE_TERSE1("  bufSize*NumBuf: %d", pP->pMetadataBufStatus->bufSize*pP->pMetadataBufStatus->NumBuf);
        SW_BREAKPOINT;
        return __LINE__;
    }

    for (i=0; i < PAF_MAXNUMCHAN_AF; i++)
    {
        gDecAudioFrameChannelPointers[i] = NULL;
    }

    if ((numchan[z] > PAF_MAXNUMCHAN) || (numchan[z] < 1)) 
    {
        TRACE_TERSE1("PAF_ASDT_initFrame0: AS%d: unsupported option", as+z);
        return __LINE__;
    }
    else 
    {
        Int j = 0;
        TRACE_TERSE1("PAF_ASDT_initFrame0: AFChanPtrMap[%d][i]", numchan[z]);
        for (i=0; i<numchan[z]; i++)
        {
            Int8 chan = AFChanPtrMap[numchan[z]][i];
            if (chan != -1)
            {
                gDecAudioFrameChannelPointers[chan] = aBuf + maxFrameLength*(j+1) - FRAMELENGTH;
                j++;
                TRACE_TERSE3("PAF_ASDT_initFrame0: chan = %d = AFChanPtrMap[%d][%d].", chan, numchan[z], i);
                TRACE_TERSE2("PAF_ASDT_initFrame0: audioFrameChannelPointers[%d]: 0x%x", chan, (IArg)gDecAudioFrameChannelPointers[chan]);
            }
        }
    }

    // Initialize original audio frame channel pointers
    for (i=PAF_LEFT; i < PAF_MAXNUMCHAN_AF; i++) 
    {
        if (gDecAudioFrameChannelPointers[i])
        {
            gDecOrigAudioFrameChannelPointers[i] = gDecAudioFrameChannelPointers[i];
        }
    }

    //
    // Initialize meta data elements
    //
    pAudioFrame->pafBsMetadataUpdate = XDAS_FALSE;
    pAudioFrame->numPrivateMetadata = 0;
    pAudioFrame->bsMetadata_offset = 0;
    pAudioFrame->bsMetadata_type = PAF_bsMetadata_channelData;
    pAudioFrame->privateMetadataBufSize = pP->pMetadataBufStatus->bufSize;
    for (i=0; i<pP->pMetadataBufStatus->NumBuf; i++)
    {
        pAudioFrame->pafPrivateMetadata[i].offset = 0;
        pAudioFrame->pafPrivateMetadata[i].size = 0;
        pAudioFrame->pafPrivateMetadata[i].pMdBuf = metadataBuf + pP->pMetadataBufStatus->bufSize*i;
    }

    return 0;
} //PAF_ASDT_initFrame0

// -----------------------------------------------------------------------------
// ASOT Initialization Function Helper - Reinitialization of Audio Frame
// AST Decoding Function              - Reinitialization of Audio Frame
//
//   Name:      PAF_ASOT_initFrame1
//   Purpose:   Audio Stream Task Function for initialization or reinitiali-
//              zation of the Audio Frame(s) by loading of data values of a
//              time-varying nature.
//   From:      audioStream1Task or equivalent
//              AST Parameter Function -> decodeInfo
//              AST Parameter Function -> decodeDecode
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     None.
//
Int
PAF_ASDT_initFrame1(
    const PAF_ASDT_Params *pP, 
    const PAF_ASDT_Patchs *pQ, 
    PAF_ASDT_Config *pAsdtCfg, 
    Int z, 
    Int apply
)
{
    PAF_AudioFrame *pAudioFrame;

    //
    // Reinitialize audio frame elements:
    //
    //   Channel Configurations during sys init                 = Unknown
    //      "          "        during info or decode           = None
    //
    //   Sample Rate / Count    during sys init, info or decode = Unknown / 0
    //

    pAudioFrame = &gpDecAudioFrame[z];

    if (apply < 0) 
    {
        pAudioFrame->channelConfigurationRequest.legacy = PAF_CC_UNKNOWN;
        pAudioFrame->channelConfigurationStream.legacy = PAF_CC_UNKNOWN;
    }
    else 
    {
        pAudioFrame->channelConfigurationRequest.legacy = PAF_CC_NONE;
        pAudioFrame->channelConfigurationStream.legacy = PAF_CC_NONE;
    }

    if (apply < 1) 
    {
        pAudioFrame->sampleRate = PAF_SAMPLERATE_UNKNOWN;
        pAudioFrame->sampleCount = 0;
    }

    return 0;
} //PAF_ASDT_initFrame1

// Reset AF, invoked during decode ACTIVATE (during state=INIT on Master)
Int resetAf(
    const PAF_ASDT_Params *pP, 
    Int z, 
    Int sourceSelect
)
{
    PAF_AudioFrame *pAudioFrame;
    Int ch;
    Int i;

    // Get audio frame
    pAudioFrame = &gpDecAudioFrame[z];
    if (pAudioFrame == NULL)
    {
        SW_BREAKPOINT;
    }

    // Reinitialize audio frame elements
    pAudioFrame->sampleDecode = PAF_SOURCE_UNKNOWN;
    pAudioFrame->sampleRate = PAF_SAMPLERATE_UNKNOWN;
    pAudioFrame->sampleCount = 0;
    pAudioFrame->channelConfigurationRequest.full = PAF_CC_UNKNOWN;
    pAudioFrame->channelConfigurationStream.full = PAF_CC_UNKNOWN;
    
    pAudioFrame->data.nChannels = PAF_MAXNUMCHAN;
    switch (sourceSelect)
    {
        case PAF_SOURCE_PCM:
            pAudioFrame->data.nSamples = FRAMELENGTH;
            break;
        case PAF_SOURCE_AAC:
            pAudioFrame->data.nSamples =  1024;
            break;
        case PAF_SOURCE_AC3:
        case PAF_SOURCE_DDP:
            pAudioFrame->data.nSamples = 1536;
            break;
        case PAF_SOURCE_THD:
            pAudioFrame->data.nSamples = 5120; //QIN FIX ME
            break;
        case PAF_SOURCE_DTS:
        case PAF_SOURCE_DTSHD:
        case PAF_SOURCE_DTS12:
        case PAF_SOURCE_DTS13:
        case PAF_SOURCE_DTS14:
        case PAF_SOURCE_DTS16:
        case PAF_SOURCE_DTSALL:
            pAudioFrame->data.nSamples = 8192; // TODO: Need to set correct value, max Sample output
            break;
        case PAF_SOURCE_DSD1:
        case PAF_SOURCE_DSD2:
        case PAF_SOURCE_DSD3:
            pAudioFrame->data.nSamples = 1024;
            break;
        default:  
            pAudioFrame->data.nSamples = FRAMELENGTH;
            break;
    }

    // Reset audio frame channel pointers
    for (ch=PAF_LEFT; ch < PAF_MAXNUMCHAN_AF; ch++) 
    {
        if (gDecAudioFrameChannelPointers[ch])
        {
            gDecAudioFrameChannelPointers[ch] = gDecOrigAudioFrameChannelPointers[ch];
        }
    }
    
    // Reset audio frame meta data elements
    pAudioFrame->pafBsMetadataUpdate = XDAS_FALSE;
    pAudioFrame->numPrivateMetadata = 0;
    pAudioFrame->bsMetadata_offset = 0;
    pAudioFrame->bsMetadata_type = PAF_bsMetadata_none;
    for (i=0; i<pP->pMetadataBufStatus->NumBuf; i++)
    {
        pAudioFrame->pafPrivateMetadata[i].offset = 0;
        pAudioFrame->pafPrivateMetadata[i].size = 0;
    }
    
    return 0;
}

// Reset AF samsiz, invoked during DECODE
Int resetAfSamsiz(
    Int z
)
{
    PAF_AudioFrame *pAudioFrame;
    Int ch;
    
    // Get audio frame
    pAudioFrame = &gpDecAudioFrame[z];
    if (pAudioFrame == NULL)
    {
        SW_BREAKPOINT;
    }
    
    // Clear samsiz for all channels - MID 208.
    for (ch=0; ch < PAF_MAXNUMCHAN_AF; ch++) 
    {
        pAudioFrame->data.samsiz[ch] = 0;
    }

    return 0;
}

// FL: Need to check correct behavior for other decoders, esp DTSHD & DXP
// Check decoder sync using information in INFO audio frame
static Int8 checkDecSync(
    PAF_AudioFrame *pAf
)
{
    if ((pAf->sampleDecode == PAF_SOURCE_THD) &&
        (pAf->sampleRate == PAF_SAMPLERATE_UNKNOWN))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
