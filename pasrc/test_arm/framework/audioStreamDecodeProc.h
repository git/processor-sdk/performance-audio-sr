
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamDecodeProc.h ========
 */

#ifndef _ASDP_H_
#define _ASDP_H_
 
#include <xdc/std.h>
#include <ti/sysbios/knl/Task.h>

#include "audioStreamProc_params.h"
#include "audioStreamProc_patchs.h"
#include "audioStreamProc_config.h"
#include "aspDecOpCircBuf_common.h"

struct PAF_ASDT_Params;
struct PAF_ASDT_Patchs;
struct PAF_ASDT_Config;

// Audio Stream Decode Task (ASDT) parameters, functions
typedef struct PAF_ASDT_Fxns {
    Int (*initPhase[8]) (const struct PAF_ASDT_Params *, const struct PAF_ASDT_Patchs *, struct PAF_ASDT_Config *);
    Int (*initFrame0) (const struct PAF_ASDT_Params *, const struct PAF_ASDT_Patchs *, struct PAF_ASDT_Config *, Int);
    Int (*initFrame1) (const struct PAF_ASDT_Params *, const struct PAF_ASDT_Patchs *, struct PAF_ASDT_Config *, Int, Int);

    // For RAM_report
    Void (*headerPrint)();
    Int  (*allocPrint)(const PAF_ALG_AllocInit *pInit, Int sizeofInit, PAF_IALG_Config *p);
    Void (*commonPrint)(IALG_MemRec common[], PAF_IALG_Config *p);
    Void (*bufMemPrint)(Int z, Int size, Int heapId, Int bufType);
    Void (*memStatusPrint)(
        CString header, 
        HeapMem_Handle hInternalHeap, 
        HeapMem_Handle hInternal1Heap, 
        HeapMem_Handle hExternalHeap, 
        HeapMem_Handle hInternal1HeapShm,
        HeapMem_Handle hExternalHeapShm,
        HeapMem_Handle hExternalNonCachedHeapShm);
} PAF_ASDT_Fxns;

// Audio Stream Decode Task (ASDT) parameters
typedef struct PAF_ASDT_Params {
    const PAF_ASDT_Fxns *fxns;
    struct {
        SmInt master;
        SmInt decodes;
        SmInt decode1;
        SmInt decodeN;
        SmInt streams;
    } zone;
    const SmInt *inputsFromDecodes;
    struct {
        int *pHeapIdIntern;             // INT memory heap ID
        int *pHeapIdExtern;             // EXT memory heap ID
        int *pHeapIdFrmbuf;             // Frame buffer heap ID
        int *pHeapIdIntern1;            // INT1 memory heap ID
        int *pHeapIdInt1Shm;            // INT1 SHared Memory heap ID
        int *pHeapIdExtShm;             // EXT SHared Memory heap ID
        int *pHeapIdExtNonCachedShm;    // EXT Non-Cached SHared Memory heap ID
        int clear; 
    } heap;
    struct {
        const IALG_MemSpace *space;
    } common;
    const SmInt *z_numchan;
    MdInt framelength;
    const PAF_AudioFunctions *pAudioFrameFunctions;
    const struct PAF_ASP_ChainFxns *pChainFxns;
    const PAF_DecodeStatus * const *z_pDecodeStatus;
    const PAF_ASP_AlgKey *pDecAlgKey;
    const SmInt *streamsFromDecodes;
    const MdInt maxFramelength;
    const PAF_MetadataBufStatus *pMetadataBufStatus;
    const PAF_AudioFrameBufStatus *pAudioFrameBufStatus;
} PAF_ASDT_Params;

// Audio Stream Decode Task (ASDT) patchs
typedef struct PAF_ASDT_Patchs {
    const PAF_ASP_LinkInit * const (*i_decLinkInit);
    //const PAF_ASP_LinkInit * const (*i_aspLinkInit)[GEARS];
} PAF_ASDT_Patchs;

// Audio Stream Decode Task (ASDT) configuration
typedef struct PAF_ASDT_Config 
{
    Task_Handle taskHandle;                     // ASDT handle
    ACP_Handle acp;                             // ASDT local ACP handle
    PAF_AST_DecOpCircBufCtl decOpCircBufCtl;    // decoder output circular buffer control
    PAF_AST_OutInitSyncCtl outIsCtl;            // output init-sync control
    PAF_AST_Config *pAstCfg;                    // ASIT/ASOT/ASDT shared configuration
} PAF_ASDT_Config;


//   Purpose:   Audio Stream Decode Task Function for initialization of data pointers
//              by allocation of memory.
Int
PAF_ASDT_initPhaseMalloc(
    const PAF_ASDT_Params *pP, 
    const PAF_ASDT_Patchs *pQ, 
    PAF_ASDT_Config *pAsdtCfg
);

//   Purpose:   Audio Stream Decode Task Function for initialization of data values
//              from parameters.
Int
PAF_ASDT_initPhaseConfig(
    const PAF_ASDT_Params *pP, 
    const PAF_ASDT_Patchs *pQ, 
    PAF_ASDT_Config *pAsdtCfg
);

//   Purpose:   Audio Stream Decode Task Function for initialization of ACP by
//              instantiation of the algorithm.
Int
PAF_ASDT_initPhaseAcpAlg(
    const PAF_ASDT_Params *pP, 
    const PAF_ASDT_Patchs *pQ, 
    PAF_ASDT_Config *pAsdtCfg
);

//   Purpose:   Audio Stream Decode Task Function for initialization of data pointers
//              by allocation for common memory and by instantiation for
//              algorithms.
Int
PAF_ASDT_initPhaseCommon(
    const PAF_ASDT_Params *pP, 
    const PAF_ASDT_Patchs *pQ, 
    PAF_ASDT_Config *pAsdtCfg
);

//   Purpose:   Audio Stream Decode Task Function for initialization of data values
//              from parameters for Algorithm Keys.
Int 
PAF_ASDT_initPhaseAlgKey(
    const PAF_ASDT_Params *pP, 
    const PAF_ASDT_Patchs *pQ, 
    PAF_ASDT_Config *pAsdtCfg
);

//   Purpose:   Audio Stream Decode Task Function for initialization of the Audio
//              Frame(s) by memory allocation and loading of data pointers
//              and values.
Int
PAF_ASDT_initFrame0(
    const PAF_ASDT_Params *pP, 
    const PAF_ASDT_Patchs *pQ, 
    PAF_ASDT_Config *pAsdtCfg, 
    Int z
);

//   Purpose:   Audio Stream Task Function for initialization or reinitialization
//              of the Audio Frame(s) by loading of data values of a
//              time-varying nature.
Int
PAF_ASDT_initFrame1(
    const PAF_ASDT_Params *pP, 
    const PAF_ASDT_Patchs *pQ, 
    PAF_ASDT_Config *pAsdtCfg, 
    Int z, 
    Int sourceSelect
);

// Reset AF, invoked during activate (INIT stage from Master)
Int resetAf(
    const PAF_ASDT_Params *pP, 
    Int z, 
    Int sourceSelect
);

// Reset AF samsiz, invoked during DECODE
Int resetAfSamsiz(
    Int z
);

extern PAF_ASDT_Config gPAF_ASDT_config;

#endif /* _ASDP_H_ */
