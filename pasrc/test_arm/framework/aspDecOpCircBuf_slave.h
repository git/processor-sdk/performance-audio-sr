
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _ASP_DECOP_CB_SLAVE_H_
#define _ASP_DECOP_CB_SLAVE_H_

#include <xdc/std.h>
#include "paftyp.h"
#include "aspDecOpCircBuf_common.h"

#include "dbgBenchmark.h" // PCM high-sampling rate + SRC + CAR benchmarking

#define ASP_DECOP_CB_INIT_INV_SOURCE_SEL    ( ASP_DECOP_CB_ERR_START-1 )    // error: invalid source selection on init
#define ASP_DECOP_CB_AF_WRITE_OVERFLOW      ( ASP_DECOP_CB_ERR_START-2 )    // error: write audio frame overflow
#define ASP_DECOP_CB_PCM_WRITE_OVERFLOW     ( ASP_DECOP_CB_ERR_START-3 )    // error: write PCM overflow

#define DEF_DEC_OP_FRAME_LEN                ( PAF_SYS_FRAMELENGTH )         // default decoder output frame length

// Initialize circular buffer for Decoder writes
Int cbInitDecWrite(
    PAF_AST_DecOpCircBufCtl *pCbCtl,    // decoder output circular buffer control
    Int8 cbIdx,                         // decoder output circular buffer index
    Int8 sourceSelect,                  // source select (PCM, DDP, etc.)
    Int16 decOpFrameLen,                // decoder output frame length (PCM samples)
    Int8 resetRwFlags,                  // whether to reset reader, writer, and drain flags
    PAF_AudioFrame *pDecInitAf          // pointer to Dec output audio frame used for CB initialization
);

// Start writes to circular buffer
Int cbWriteStart(
    PAF_AST_DecOpCircBufCtl *pCbCtl,    // decoder output circular buffer control
    Int8 cbIdx                          // decoder output circular buffer index
);

// Stop writes to circular buffer
Int cbWriteStop(
    PAF_AST_DecOpCircBufCtl *pCbCtl,    // decoder output circular buffer control
    Int8 cbIdx                          // decoder output circular buffer index
);

// Write audio frame to circular buffer
Int cbWriteAf(
    PAF_AST_DecOpCircBufCtl *pCbCtl,    // decoder output circular buffer control
    Int8 cbIdx,                         // decoder output circular buffer index
    PAF_AudioFrame *pAfWrt              // audio frame from which to write
);

#if 0
// Get next audio frame to write in circular buffer
Int cbGetNextWriteAf(
    PAF_AST_DecOpCircBufCtl *pCbCtl,    // decoder output circular buffer control
    Int8 cbIdx,                         // decoder output circular buffer index
    PAF_AudioFrame **ppAfWrt            // audio frame next to be written
);
#endif

#endif /* _ASP_DECOP_CB_SLAVE_H_ */
