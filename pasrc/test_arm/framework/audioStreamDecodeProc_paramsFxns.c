
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamDecodeProc_paramsFxns.c ========
 */

#include <xdc/std.h>
 
#include "audioStreamDecodeProc.h"
 
// .............................................................................
// Audio Stream Decode Task Parameter Functions
//
//   Name:      PAF_ASDT_params_fxns
//   Purpose:   ASDT jump table.
//   From:      PAF_AST_Params
//   Uses:      See contents.
//   States:    N.A.
//   Return:    N.A.
//   Trace:     None.
//
const PAF_ASDT_Fxns PAF_ASDT_params_fxns =
{
    {   // initPhase[8]
        PAF_ASDT_initPhaseMalloc,           // initPhaseMalloc
        PAF_ASDT_initPhaseConfig,           // initPhaseConfig
        PAF_ASDT_initPhaseAcpAlg,           // initPhaseAcpAlg
        PAF_ASDT_initPhaseCommon,           // initPhaseCommon
        PAF_ASDT_initPhaseAlgKey,           // initPhaseAlgKey
        NULL,
        NULL,
        NULL
    },
    PAF_ASDT_initFrame0,                    // initFrame0
    PAF_ASDT_initFrame1,                    // initFrame1
    NULL,                                   // headerPrint
    NULL,                                   // allocPrint
    NULL,                                   // commonPrint
    NULL,                                   // bufMemPrint
    NULL                                    // memStatusPrint
};
