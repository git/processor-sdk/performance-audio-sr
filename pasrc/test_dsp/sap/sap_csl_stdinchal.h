
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef SAP_CSL_STDINCHAL_H_
#define SAP_CSL_STDINCHAL_H_


/******************************************************************************\
* HAL macro definitions
\******************************************************************************/

  #define _VALUEOF(x) ((Uint32)(x))

  /*******************************************************/
  /* generic macros for field manipulation               */
  /*******************************************************/

  #define _PER_FSHIFT(PER,REG,FIELD) \
    _##PER##_##REG##_##FIELD##_SHIFT

  #define _PER_FMASK(PER,REG,FIELD) \
    _##PER##_##REG##_##FIELD##_MASK

  #define _PER_FSYM(PER,REG,FIELD,SYM)\
    PER##_##REG##_##FIELD##_##SYM

  #define _PER_FDEFAULT(PER,REG,FIELD) \
    ((##PER##_##REG##_##FIELD##_DEFAULT << _##PER##_##REG##_##FIELD##_SHIFT) \
    & _##PER##_##REG##_##FIELD##_MASK)

  #define _PER_FMK(PER,REG,FIELD,x) (\
    (((Uint32)(x))<<_PER_FSHIFT(##PER,##REG,##FIELD))\
    &_PER_FMASK(##PER,##REG,##FIELD)\
  )

  #define _PER_FMKS(PER,REG,FIELD,SYM) (\
    (_PER_FSYM(##PER,##REG,##FIELD,##SYM)<<_PER_FSHIFT(##PER,##REG,##FIELD))\
    &_PER_FMASK(##PER,##REG,##FIELD)\
  )

  #define _PER_FEXTRACT(PER,REG,FIELD,reg) (Uint32)(\
    (((Uint32)(reg)&_PER_FMASK(##PER,##REG,##FIELD))\
    >>_PER_FSHIFT(##PER,##REG,##FIELD))\
  )

  #define _PER_FINSERT(PER,REG,FIELD,reg,field) (Uint32)(\
    (((Uint32)(reg)&~_PER_FMASK(##PER,##REG,##FIELD))|\
    (((Uint32)(field)<<_PER_FSHIFT(##PER,##REG,##FIELD))\
    &_PER_FMASK(##PER,##REG,##FIELD)))\
  )

  /*******************************************************/
  /* macros for memmory mapped registers                 */
  /*******************************************************/

  #define _PER_RAOI(addr,PER,REG,and,or,inv)\
    (*(volatile Uint32*)(addr))=(\
      ((((*(volatile Uint32*)(addr))\
      &((Uint32)(and)))\
      |((Uint32)(or)))\
      ^((Uint32)(inv)))\
    )

  #define _PER_RGET(addr,PER,REG) \
    (*(volatile Uint32*)(addr))

  #define _PER_RSET(addr,PER,REG,x) \
    (*(volatile Uint32*)(addr))=((Uint32)(x))

  #define _PER_FGET(addr,PER,REG,FIELD) \
    _PER_FEXTRACT(##PER,##REG,##FIELD,_PER_RGET(addr,##PER,##REG))

  #define _PER_FSET(addr,PER,REG,FIELD,field)\
    _PER_RSET(addr,##PER,##REG,\
    _PER_FINSERT(##PER,##REG,##FIELD,_PER_RGET(addr,##PER,##REG),field))

  #define _PER_FSETS(addr,PER,REG,FIELD,SYM)\
    _PER_RSET(addr,##PER,##REG,_PER_FINSERT(##PER,##REG,##FIELD,_PER_RGET(addr,##PER,##REG),\
    _PER_FSYM(##PER,##REG,##FIELD,##SYM)))

  /*******************************************************/
  /* macros for CPU control registers                    */
  /*******************************************************/

  #define _PER_CRGET(PER,REG) \
    REG

  #define _PER_CRSET(PER,REG,reg) \
    REG=((Uint32)(reg))

  #define _PER_CFGET(PER,REG,FIELD) \
    _PER_FEXTRACT(##PER,##REG,##FIELD,_PER_CRGET(##PER,##REG))

  #define _PER_CFSET(PER,REG,FIELD,field)\
    _PER_CRSET(##PER,##REG,\
    _PER_FINSERT(##PER,##REG,##FIELD,_PER_CRGET(##PER,##REG),field))

  #define _PER_CFSETS(PER,REG,FIELD,SYM)\
    _PER_CRSET(##PER,##REG,\
    _PER_FINSERT(##PER,##REG,FIELD,_PER_CRGET(##PER,##REG),\
    _PER_FSYM(##PER,##REG,##FIELD,##SYM)))

/*----------------------------------------------------------------------------*/

#endif // SAP_CSL_STDINCHAL_H_

