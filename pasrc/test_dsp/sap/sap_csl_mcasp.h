
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef SAP_CSL_MCASP_H_
#define SAP_CSL_MCASP_H_

#include "sap_csl_mcasphal.h"

/******************************************************************************\
 * global macro declarations
\******************************************************************************/

/* MCASP_open() flags */
#define MCASP_OPEN_RESET     (0x00000001)
#define MCASP_OPEN     (0x00000000)

/* device identifiers for MCASP_open() */
#define MCASP_DEV0         (0)
#define MCASP_DEV1         (1)
#define MCASP_DEV2         (2)


/* device identifiers for MCASP_open() */
#define MCASP_PORT0    MCASP_DEV0
#define MCASP_PORT1    MCASP_DEV1
#define MCASP_PORT2    MCASP_DEV2

/* select DIT vs. TDM mode */
#define  MCASP_XMT_DIT     1
#define  MCASP_XMT_TDM     0

/* direction = (transmitter only) MCASP_XMT or (receiver only) MCASP_RCV  */
/*             or (both) MCASP_RCVXMT / MCASP_XMTRCV */
#define MCASP_RCV       1
#define MCASP_XMT    	2
#define MCASP_RCVXMT	3
#define MCASP_XMTRCV	3

/* Mode for clk */
#define MCASP_CLK_ASYNC       1
#define MCASP_CLK_SYNC        0

/* define Mode BURST or TDM for format*/
#define MCASP_MODE_BURST    0
#define MCASP_MODE_TDM      1

/* define Mode MSB/LSB first */
#define MCASP_FORMAT_LSB    0
#define MCASP_FORMAT_MSB    1

/* define Align  */
#define MCASP_FORMAT_LEFT   0
#define MCASP_FORMAT_RIGHT  1
      
/* Address Incrementation */
#define MCASP_XBUF_BUFSIZE 4
#define MCASP_RBUF_BUFSIZE 4
#define MCASP_DITCSR_NUMCHANNELS 6
#define MCASP_DITCSR_BUFSIZE 4
#define MCASP_DITCSR_RIGHT_OFFSET (MCASP_DITCSR_BUFSIZE*MCASP_DITCSR_NUMCHANNELS)

#define MCASP_DITUDR_NUMCHANNELS 6
#define MCASP_DITUDR_BUFSIZE 4
#define MCASP_DITUDR_RIGHT_OFFSET (MCASP_DITUDR_BUFSIZE*MCASP_DITUDR_NUMCHANNELS)

/* Status clear */
#define MCASP_RSTAT_ROVRN       0
#define MCASP_RSTAT_RSYNCERR    1
#define MCASP_RSTAT_RCKFAIL     2
#define MCASP_RSTAT_REVENSLOT   3 
#define MCASP_RSTAT_RLAST       4
#define MCASP_RSTAT_RDATA       5
#define MCASP_RSTAT_RSTAFRM     6
#define MCASP_RSTAT_RDMAERR     7
#define MCASP_RSTAT_RERR        8

#define MCASP_XSTAT_XUNDRN      0
#define MCASP_XSTAT_XSYNCERR    1
#define MCASP_XSTAT_XCKFAIL     2
#define MCASP_XSTAT_XEVENSLOT   3 
#define MCASP_XSTAT_XLAST       4
#define MCASP_XSTAT_XDATA       5
#define MCASP_XSTAT_XSTAFRM     6
#define MCASP_XSTAT_XDMAERR     7
#define MCASP_XSTAT_XERR        8


#define MCASP_XBUF0              0
#define MCASP_XBUF1              1
#define MCASP_XBUF2              2 
#define MCASP_XBUF3              3
#define MCASP_XBUF4              4
#define MCASP_XBUF5              5
#define MCASP_XBUF6              6
#define MCASP_XBUF7              7
#define MCASP_XBUF8              8
#define MCASP_XBUF9              9
#define MCASP_XBUF10             10
#define MCASP_XBUF11             11 
#define MCASP_XBUF12             12
#define MCASP_XBUF13             13
#define MCASP_XBUF14             14
#define MCASP_XBUF15             15

#define MCASP_RBUF0              0
#define MCASP_RBUF1              1
#define MCASP_RBUF2              2 
#define MCASP_RBUF3              3
#define MCASP_RBUF4              4
#define MCASP_RBUF5              5
#define MCASP_RBUF6              6
#define MCASP_RBUF7              7
#define MCASP_RBUF8              8
#define MCASP_RBUF9              9
#define MCASP_RBUF10             10
#define MCASP_RBUF11             11 
#define MCASP_RBUF12             12
#define MCASP_RBUF13             13
#define MCASP_RBUF14             14
#define MCASP_RBUF15             15

/******************************************************************************\
 * global typedef declarations
\******************************************************************************/

/* device handle object */
typedef struct {
    Uint32 allocated;
    volatile Uint32 *baseAddr;
    Uint32 xbufAddr;
    Uint32 xbufAddrCfg;
    Uint32 rbufAddr;
    Uint32 rbufAddrCfg;
    Uint32 ditcsrAddr;
    Uint32 ditudrAddr;
} MCASP_Obj, *MCASP_Handle;

/* device configuration structure */

typedef struct {
	Uint32 rmask;
	Uint32 rfmt; 
	Uint32 afsrctl; 
	Uint32 aclkrctl; 
	Uint32 ahclkrctl; 
	Uint32 rtdm; 
	Uint32 rintctl; 
	Uint32 rclkchk;
} MCASP_ConfigRcv;

typedef struct {
	Uint32 xmask; 
	Uint32 xfmt; 
	Uint32 afsxctl; 
	Uint32 aclkxctl;
	Uint32 ahclkxctl; 
	Uint32 xtdm;
	Uint32 xintctl; 
	Uint32 xclkchk;
} MCASP_ConfigXmt;


/******************************************************************************\
 * global function declarations
\******************************************************************************/
void MCASP_reset(MCASP_Handle hMcasp);
void MCASP_resetAll();
MCASP_Handle MCASP_open(int devNum, Uint32 flags);

void MCASP_configRcv(MCASP_Handle hMcasp, MCASP_ConfigRcv *config);
void MCASP_configXmt(MCASP_Handle hMcasp, MCASP_ConfigXmt *config);

#endif //SAP_CSL_MCASP_H_

