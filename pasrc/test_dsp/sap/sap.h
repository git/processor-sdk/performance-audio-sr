
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Header file for sap SIO driver.
//
//
//
#define SAP_DMA_EDMA
#define SAP_PORT_MCASP

#ifndef SAP_H
#define SAP_H

#include <ti/xdais/xdas.h>
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/IHeap.h>
#include <ti/sysbios/heaps/HeapMem.h>
#include <ti/sysbios/hal/Cache.h>

#include <xdc/runtime/Log.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Queue.h>
#include <ti/sysbios/hal/Hwi.h>
#include <xdc/runtime/Error.h>

#include <dev2.h>

// include sap_mcasp here although not needed locally, but allows user code
// (i.e. sap_xx.c) to include just sap.h and not require other includes; like csl.
#ifdef SAP_PORT_MCASP
#include "sap_csl_mcasp.h"
#endif

#include <pafsio.h>

#define SAP_NAME "/DAP"
//#define SAP_NAME "/SAP"

#define MAX_SAP_DEVICES     32  // Must be >= total number input and output devices
                                // For example, from project *io_a.h file:
                                //     total devices = DEVINP_N + DEVOUT_N

// ..........................................................................
// Global context for the SAP layer

typedef struct SAP_DriverObject
{
    DEV2_Handle    device[MAX_SAP_DEVICES];

    Uint16         numDevices;
    Uint16         unused[3];

} SAP_DriverObject;

extern SAP_DriverObject sapDrv;

// ..........................................................................
// device configuration structure -- used as argument to SIO_ctrl

struct SAP_Params_
{
    XDAS_Int32  pinMask;
};

typedef struct SAP_Params
{
    Int                 size;   // Type-specific size
    struct DXX_Params_  sio;    // Common parameters
    struct SAP_Params_  sap;    // Driver parameters
} SAP_Params;

// .............................................................................
// DMA Function table defs

typedef Int  (*SAP_DMA_TsetupParam)(DEV2_Handle, XDAS_UInt32, XDAS_UInt32, unsigned int, unsigned int);
typedef Int  (*SAP_DMA_TsetupXfer)(DEV2_Handle, XDAS_UInt32, XDAS_UInt32, XDAS_UInt32, DEV2_Frame *);

typedef struct SAP_DMA_Fxns {
    SAP_DMA_TsetupParam     setupParam;
    SAP_DMA_TsetupXfer      setupXfer;
} SAP_DMA_Fxns;

#define SAP_DMA_FTABLE_setupParam(_a,_b,_c,_d,_e)      (*pDevExt->pFxns->pDmaFxns->setupParam)(_a,_b,_c,_d,_e)
#define SAP_DMA_FTABLE_setupXfer(_a,_b,_c,_d,_e)        (*pDevExt->pFxns->pDmaFxns->setupXfer(_a,_b,_c,_d,_e)

// .............................................................................
// PORT Function table defs

typedef Int	 (*SAP_PORT_Talloc)(DEV2_Handle);
typedef Int	 (*SAP_PORT_Tclose)(DEV2_Handle);
typedef Int	 (*SAP_PORT_Tenable)(DEV2_Handle);
typedef Void     (*SAP_PORT_Tinit)(Void);
typedef Int	 (*SAP_PORT_Topen)(DEV2_Handle);
typedef Int	 (*SAP_PORT_Treset)(DEV2_Handle);
typedef Int	 (*SAP_PORT_TwatchDog)(DEV2_Handle);

typedef struct SAP_PORT_Fxns {    
    SAP_PORT_Talloc      alloc;
    SAP_PORT_Tclose      close;
    SAP_PORT_Tenable     enable;
    SAP_PORT_Tinit       init;
    SAP_PORT_Topen       open;
    SAP_PORT_Treset      reset;
    SAP_PORT_TwatchDog   watchDog;
} SAP_PORT_Fxns;

#define SAP_PORT_FTABLE_alloc(_a)        (*pDevExt->pFxns->pPortFxns->alloc)(_a)
#define SAP_PORT_FTABLE_close(_a)        (*pDevExt->pFxns->pPortFxns->close)(_a)
#define SAP_PORT_FTABLE_enable(_a)       (*pDevExt->pFxns->pPortFxns->enable)(_a)
#define SAP_PORT_FTABLE_init()           (*pFxns->pPortFxns->init)()
#define SAP_PORT_FTABLE_open(_a)         (*pDevExt->pFxns->pPortFxns->open)(_a)
#define SAP_PORT_FTABLE_reset(_a)        (*pDevExt->pFxns->pPortFxns->reset)(_a)
#define SAP_PORT_FTABLE_watchDog(_a)     (*pDevExt->pFxns->pPortFxns->watchDog)(_a)

// .............................................................................
// SAP Function table defs

typedef Int	(*SAP_Tshutdown)(DEV2_Handle);
typedef Int	(*SAP_Tstart)(DEV2_Handle);
typedef Int	(*SAP_Tconfig)(DEV2_Handle, const SAP_Params *);

typedef struct SAP_Fxns {
    //common (must be same as DEV_Fxns)
    DEV2_Tclose		close;
    DEV2_Tctrl		ctrl;
    DEV2_Tidle		idle;
    DEV2_Tissue		issue;
    DEV2_Topen		open;
    DEV2_Tready		ready;
    DEV2_Treclaim	reclaim; //optional

    //SAP specific
    SAP_Tshutdown       shutdown;
    SAP_Tstart          start;
    SAP_Tconfig         config;

    SAP_PORT_Fxns       *pPortFxns;
    SAP_DMA_Fxns        *pDmaFxns;

} SAP_Fxns;

extern SAP_Fxns SAP_FXNS;
extern Void SAP_init (void);     
extern Void SAP_watchDog (void); 

// macros assume pDevExt is available and pDevExt->pFxns is valid
#define SAP_FTABLE_shutdown(_a)          (*pDevExt->pFxns->shutdown)(_a)
#define SAP_FTABLE_start(_a)             (*pDevExt->pFxns->start)(_a)
#define SAP_FTABLE_config(_a,_b)         (*pDevExt->pFxns->config)(_a,_b)
#define SAP_FTABLE_ctrl(_a,_b,_c)        (*pDevExt->pFxns->ctrl)(_a,_b,_c)

// .............................................................................

typedef struct SAP_EDMA_Param
{
	Queue_Elem         	link;	 // queue link
    XDAS_UInt32      	hEdmaParam;  // parameter table handle
} SAP_EDMA_Param;

// .............................................................................

#define MAX_EDMA_PARAM_ENTRIES 16 /* minimum is 2 */

// device extension (device specific context).

typedef struct SAP_DeviceExtension
{
    DEV2_Handle   device;

    XDAS_Int8    numQueued;
    XDAS_Int8    errorState;
    XDAS_Int8    runState;
    XDAS_Int8    deviceNum;

    Semaphore_Handle   sync;
    Queue_Struct      xferQue;

    SAP_Fxns     *pFxns;
    const SAP_Params   *pParams;

    XDAS_UInt32 firstTCC;

    Queue_Struct      paramQue;
    Semaphore_Handle  ready;		//unused - remove me
    XDAS_Int8   numEdmaParams;

    XDAS_Int32        	edmaWordSize;
    XDAS_UInt32        	shortTCC;	//unused - remove me
    XDAS_UInt32       	shortEdma;	//unused - remove me
    XDAS_Int8			shutDown;

    XDAS_UInt32       	activeEdma;
    XDAS_UInt32       	errorEdma;

    Uint16  numSers;
    Uint16  numSlots;

    Void  *pPortExt; //unused - remove me
    Void  *pDmaExt; //unused - remove me

    SAP_EDMA_Param    edmaParams[MAX_EDMA_PARAM_ENTRIES];

    Int    optLevel;	//unused - remove me
    Int    numParamSetup;	//unused - remove me

} SAP_DeviceExtension;

// .............................................................................

#endif // SAP_H
