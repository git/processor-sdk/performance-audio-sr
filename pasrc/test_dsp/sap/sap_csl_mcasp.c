
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <xdc/std.h>
#include <ti/sysbios/hal/Hwi.h>

#include "sap_csl_mcasp.h"

#ifndef INV
#define INV    ((void*)(-1))
#endif

#define DEVICE_ENTRY(devNum) {                              \
        FALSE,                                              \
            (volatile Uint32*)_MCASP_BASE_PORT##devNum##,   \
            _MCASP_XBUF##devNum##_ADDR,                     \
            _MCASP_XBUF0##devNum##_ADDR,                    \
            _MCASP_RBUF##devNum##_ADDR,                     \
            _MCASP_RBUF0##devNum##_ADDR,                    \
            _MCASP_DITCSR##devNum##_ADDR,                   \
            _MCASP_DITUDR##devNum##_ADDR                    \
            }

/******************************************************************************\
 * static variable definitions
\******************************************************************************/
static MCASP_Obj _MCASP_deviceTable[_MCASP_PORT_CNT] =
{
    DEVICE_ENTRY(0)
    ,DEVICE_ENTRY(1)
    ,DEVICE_ENTRY(2)
};

// -----------------------------------------------------------------------------

void MCASP_reset (MCASP_Handle hMcasp)
{
    int oldMask;


    oldMask = Hwi_disable ();

    if (hMcasp == INV) {
        MCASP_reset ((MCASP_Handle) (&(_MCASP_deviceTable[0])));
        MCASP_reset ((MCASP_Handle) (&(_MCASP_deviceTable[1])));
        MCASP_reset ((MCASP_Handle) (&(_MCASP_deviceTable[2])));
    }
    else {
        MCASP_RSETH (hMcasp,PFUNC,       MCASP_PFUNC_DEFAULT);
        MCASP_RSETH (hMcasp,PDIR,        MCASP_PDIR_DEFAULT);
        MCASP_RSETH (hMcasp,AMUTE,       MCASP_AMUTE_DEFAULT);
        MCASP_RSETH (hMcasp,DLBCTL,      MCASP_DLBCTL_DEFAULT);
        MCASP_RSETH (hMcasp,DITCTL,          MCASP_DITCTL_DEFAULT);
        MCASP_RSETH (hMcasp,RMASK,       MCASP_RMASK_DEFAULT);
        MCASP_RSETH (hMcasp,RFMT, MCASP_RFMT_DEFAULT);
        MCASP_RSETH (hMcasp,AFSRCTL, MCASP_AFSRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,ACLKRCTL,MCASP_ACLKRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,AHCLKRCTL,MCASP_AHCLKRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,RTDM,MCASP_RTDM_DEFAULT);
        MCASP_RSETH (hMcasp,RINTCTL,MCASP_RINTCTL_DEFAULT);
        MCASP_RSETH (hMcasp,RCLKCHK,MCASP_RCLKCHK_DEFAULT);
        MCASP_RSETH (hMcasp,XMASK, MCASP_XMASK_DEFAULT);
        MCASP_RSETH (hMcasp,XFMT,MCASP_XFMT_DEFAULT);
        MCASP_RSETH (hMcasp,AFSXCTL,MCASP_AFSXCTL_DEFAULT);
        MCASP_RSETH (hMcasp,ACLKXCTL,MCASP_ACLKXCTL_DEFAULT);
        MCASP_RSETH (hMcasp,AHCLKXCTL,MCASP_AHCLKXCTL_DEFAULT);
        MCASP_RSETH (hMcasp,XTDM,MCASP_XTDM_DEFAULT);
        MCASP_RSETH (hMcasp,XINTCTL,MCASP_XINTCTL_DEFAULT);
        MCASP_RSETH (hMcasp,XCLKCHK,MCASP_XCLKCHK_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL0,MCASP_SRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL1,MCASP_SRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL2,MCASP_SRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL3,MCASP_SRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL4,MCASP_SRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL5,MCASP_SRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL6,MCASP_SRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL7,MCASP_SRCTL_DEFAULT);

        MCASP_RSETH (hMcasp,SRCTL8,MCASP_SRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL9,MCASP_SRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL10,MCASP_SRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL11,MCASP_SRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL12,MCASP_SRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL13,MCASP_SRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL14,MCASP_SRCTL_DEFAULT);
        MCASP_RSETH (hMcasp,SRCTL15,MCASP_SRCTL_DEFAULT);

        MCASP_RSETH (hMcasp,RSTAT,0x01F7);
        MCASP_RSETH (hMcasp,XSTAT,0x01F7);
        MCASP_RSETH (hMcasp,GBLCTL,MCASP_GBLCTL_DEFAULT);
    }

    Hwi_restore (oldMask);
    return;
} //MCASP_reset

// -----------------------------------------------------------------------------

MCASP_Handle MCASP_open (int devNum, Uint32 flags)
{
    MCASP_Handle hMcasp = INV;
    int oldMask;


    oldMask = Hwi_disable ();
        if (!_MCASP_deviceTable[devNum].allocated) {
            hMcasp = &(_MCASP_deviceTable[devNum]);
            _MCASP_deviceTable[devNum].allocated = TRUE;

            if (hMcasp != INV)
                if (flags & MCASP_OPEN_RESET)
                    MCASP_reset(hMcasp);
        }
    Hwi_restore (oldMask);

    return hMcasp;
} //MCASP_open

// -----------------------------------------------------------------------------

void MCASP_configRcv (MCASP_Handle hMcasp, MCASP_ConfigRcv *config)
{
    volatile Uint32 *base = (volatile Uint32 *)(hMcasp->baseAddr);
    register int x5,x6,x7,x8,x9,x10,x11,x12;
    int oldMask;


    oldMask = Hwi_disable ();

    /* the compiler generates more efficient code if the loads */
    /* and stores are grouped together rather than intermixed  */
    x5 = config->rmask;
    x6 = config->rfmt;
    x7 = config->afsrctl;
    x8 = config->aclkrctl;
    x9 = config->ahclkrctl;
    x10 = config->rtdm;
    x11 = config->rintctl;
    x12 = config->rclkchk;

    base[_MCASP_RMASK_OFFSET] = 	x5;
    base[_MCASP_RFMT_OFFSET] = 	x6;
    base[_MCASP_AFSRCTL_OFFSET] = x7;
    base[_MCASP_ACLKRCTL_OFFSET]= x8;
    base[_MCASP_AHCLKRCTL_OFFSET]= x9;
    base[_MCASP_RTDM_OFFSET] = 	x10;
    base[_MCASP_RINTCTL_OFFSET] = x11;
    base[_MCASP_RCLKCHK_OFFSET] = x12;

    Hwi_restore (oldMask);

} //MCASP_configRcv

// -----------------------------------------------------------------------------

void MCASP_configXmt (MCASP_Handle hMcasp, MCASP_ConfigXmt *config)
{

    volatile Uint32 *base = (volatile Uint32 *)(hMcasp->baseAddr);
    register int x13,x14,x15,x16,x17,x18,x19,x20;
    int oldMask;


    oldMask = Hwi_disable ();

    /* the compiler generates more efficient code if the loads */
    /* and stores are grouped together rather than intermixed  */
    x13 = config->xmask;
    x14 = config->xfmt;
    x15 = config->afsxctl;
    x16 = config->aclkxctl;
    x17 = config->ahclkxctl;
    x18 = config->xtdm;
    x19 = config->xintctl;
    x20 = config->xclkchk;

    base[_MCASP_XMASK_OFFSET] = 	x13;
    base[_MCASP_XFMT_OFFSET] = 	x14;
    base[_MCASP_AFSXCTL_OFFSET] = x15;
    base[_MCASP_ACLKXCTL_OFFSET]= x16;
    base[_MCASP_AHCLKXCTL_OFFSET]= x17;
    base[_MCASP_XTDM_OFFSET] = 	x18;
    base[_MCASP_XINTCTL_OFFSET] = x19;
    base[_MCASP_XCLKCHK_OFFSET] = x20;

    Hwi_restore (oldMask);
} //MCASP_configXmt

// -----------------------------------------------------------------------------
