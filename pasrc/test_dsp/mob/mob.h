
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Stacking Input Buffer Driver declarations
//
//
// Derived from /c/ti/c6000/include/dgs.h

#ifndef DOB_H
#define DOB_H

#include <xdc/std.h>
#include <dev2.h>
#include <sio2.h>
#include <ti/xdais/xdas.h>

#include <outbuf.h>
#include <pafenc.h>
// .............................................................................
//Function table defs

typedef Int	(*DOB_Tshutdown)(DEV2_Handle);
typedef Int	(*DOB_TstartClocks)(DEV2_Handle);
typedef Int	(*DOB_TissueIEC)(DEV2_Handle, PAF_OutBufConfig *);
typedef Int	(*DOB_TreallocFrames)(DEV2_Handle);

typedef struct DOB_Fxns {
    //common (must be same as DEV2_Fxns)
    DEV2_Tclose		close;
    DEV2_Tctrl		ctrl;
    DEV2_Tidle		idle;
    DEV2_Tissue		issue;
    DEV2_Topen		open;
    DEV2_Tready		ready;
    DEV2_Treclaim	reclaim;

    //DOB specific
    DOB_Tshutdown      shutdown;
    DOB_TstartClocks   startClocks;
    DOB_TissueIEC      issueIEC;
    DOB_TreallocFrames reallocFrames;
} DOB_Fxns;

extern DOB_Fxns DOB_FXNS;
extern Void DOB_init( Void );

// .............................................................................

typedef struct DOB_DeviceExtension {
    SIO2_Obj     child;  

    DOB_Fxns   *pFxns;

    XDAS_Int8   state;
    XDAS_UInt8  maxNumBuf;
    XDAS_UInt16 lengthofFrame;

    float       rateX;

    PAF_OutBufStatus *pBufStatus;
    PAF_EncodeStatus *pEncStatus;

} DOB_DeviceExtension;

// .............................................................................

#endif /* DOB_H */





