
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Stacking Input Buffer Driver declarations
//
// Derived from /c/ti/c6000/include/dgs.h

#ifndef DIB_H
#define DIB_H

#include <xdc/std.h>

#include <dev2.h>
#include <sio2.h>
#include <ti/xdais/xdas.h>

#include <inpbuf.h>
#include <pafdec.h>
#include <pafsio.h>
#include <pafsio_ialg.h>
#include <paftyp.h>


// .............................................................................
//Function table defs

typedef Int	(*DIB_TgetSync)(DEV2_Handle, PAF_InpBufConfig *);
typedef Int	(*DIB_TinitFrame)(DEV2_Handle, PAF_InpBufConfig *);
typedef Int	(*DIB_TrequestFrame)(DEV2_Handle, PAF_InpBufConfig *);
typedef Int	(*DIB_TissueChild)(DEV2_Handle, PAF_InpBufConfig *, Int, Int);
typedef Int	(*DIB_TreclaimChild)(DEV2_Handle, PAF_InpBufConfig *);
typedef Int	(*DIB_Treset)(DEV2_Handle, PAF_InpBufConfig *);
typedef Int	(*DIB_TsyncScan)(DEV2_Handle, PAF_InpBufConfig *, XDAS_UInt32 *);
typedef Int	(*DIB_TwaitForData)(DEV2_Handle, PAF_InpBufConfig *, XDAS_UInt32);
typedef Int	(*DIB_TsyncScanDTS)(DEV2_Handle, PAF_InpBufConfig *, XDAS_UInt32 *, XDAS_UInt16 *);

typedef struct DIB_Fxns {
    //common (must be same as DEV2_Fxns)
    DEV2_Tclose		close;
    DEV2_Tctrl		ctrl;
    DEV2_Tidle		idle;
    DEV2_Tissue		issue;
    DEV2_Topen		open;
    DEV2_Tready		ready;
    DEV2_Treclaim	reclaim;

    //DIB specific
    DIB_TgetSync            getSync;
    DIB_TinitFrame          initFrame;
    DIB_TrequestFrame       requestFrame;
    DIB_TissueChild         issueChild;
    DIB_TreclaimChild       reclaimChild;
    DIB_Treset              reset;
    DIB_TsyncScan           syncScan;
    DIB_TwaitForData        waitForData;
    DIB_TsyncScanDTS        syncScanDTS;
} DIB_Fxns;

extern DIB_Fxns DIB_FXNS;
extern Void DIB_init( Void );

// .............................................................................

typedef struct DIB_DeviceExtension {
    SIO2_Obj              child;  
               
    DIB_Fxns            *pFxns;
    XDAS_UInt16         *pIECFrameLength;

    XDAS_UInt32          zeroCount;
    XDAS_Int8            sourceSelect;
    XDAS_Int8            sourceProgram;
    XDAS_Int16           numSamplesExtra;   // no. samples issued but not yet needed

    XDAS_Int16           headerSize;  // in bytes (contains sync so >= sizeofSync)
    XDAS_Int16           autoRequestSize;
                       
    XDAS_Int8            syncState;   
    XDAS_Int8            scanState;
    XDAS_Int8            running;
    XDAS_Int8            unused;
                       
    // second buffer info
    XDAS_Int32           frameLength;
    XDAS_Int32           lengthofData;
    PAF_UnionPointer     pSync;

    PAF_UnionPointer     lastDTSCDSyncDuringPCM;

    XDAS_Int32           pcmFrameLength;
    XDAS_UInt32          pcmTimeout;
    XDAS_UInt32          numSamplesSinceDTS;  
    
    //hack
    PAF_InpBufStatus     *pInpBufStatus;
    PAF_DecodeStatus     *pDecodeStatus;

    PAF_SIO_IALG_Obj     *pSioIalg;
    XDAS_Int32           deferredError;

    PAF_InpBufConfig     bufConfig;

} DIB_DeviceExtension;

// .............................................................................

#endif /* DIB_H */
