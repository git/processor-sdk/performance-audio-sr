/******************************************************************************
 * Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/
#ifndef _LIBARCH_MEMMGT_H
#define _LIBARCH_MEMMGT_H

#include <stdlib.h>

/** @defgroup libarch_memmgt Memory Management API
 *  @{
 */
/** @} */

/** @addtogroup libarch_memmgt
 *  @brief Error return codes of memory management functions.
 *  @{
 */
/*@{*/
#define LIB_MEMMGT_SUCCESS          (0)   /**< Success. No error.            */
#define LIB_MEMMGT_ERROR            (-1)  /**< Failure.                      */ 
/*@}*/
/** @} */

/** 
  * @ingroup libarch_memmgt
  * @brief Memory types in terms of speed and volatility
  */
enum {
    LIB_SMEM_VFAST = 0, /**< Scratch memory, very fast       */
    LIB_SMEM_FAST,      /**< Scratch memory, fast            */
    LIB_SMEM_MED,       /**< Scratch memory, medium speed    */
    LIB_SMEM_SLOW,      /**< Scratch memory, slow            */
    LIB_PMEM_MED,       /**< Permanent memory, medium speed  */
    LIB_PMEM_SLOW,      /**< Permanent memory, slow          */
    LIB_MEMTYPE_N       /**< Total number of types           */
};

typedef unsigned int lib_mem_type_t;

typedef struct {
  size_t         size;      /**< heap size in number of bytes */
  lib_mem_type_t type;
  unsigned int   alignment;
  void           *base;     /**< base address of the heap     */   
} lib_mem_rec_t;

#define libChkAlign(base, l2a)    \
       (((uint_least32_t)(base)&((~0UL<<(l2a))))==0UL)


#endif  /* _LIBARCH_MEMMGT_H */

/* nothing past this point */
