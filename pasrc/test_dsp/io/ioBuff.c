
#include "ioBuff.h"
#include "ioBuff_loc.h"
#include "libarch.h"
#include <stdint.h>
#include <string.h>

/******************************************************************************
* Implementation of I/O Buffer Management Component
******************************************************************************/

#define IOBUF_NUM_MEM_ALLOCS 1
#define IOBUFF_INST_ALIGN   3 

/*******************************************************************************
* API function: ioBuffNumAlloc
*    Returns the maximum number of memory allocation requests that ioBuffAlloc() 
*    requires.
*******************************************************************************/
int ioBuffNumAlloc(void)
{
  return (IOBUF_NUM_MEM_ALLOCS);
}

/*******************************************************************************
* API function: ioBuffAlloc
*    Returns a table of memory records that describe the size, alignment, type 
*    and memory space of all buffers required by this component. 
*******************************************************************************/
int ioBuffAlloc(lib_mem_rec_t *mem_tbl)
{
  mem_tbl[0].size = sizeof(ioBuffInst_t);
  mem_tbl[0].alignment = IOBUFF_INST_ALIGN;
  mem_tbl[0].type = LIB_PMEM_SLOW;
  mem_tbl[0].base = NULL;

  return (IOBUFF_NOERR);
}

/*******************************************************************************
* API function: ioBuffCreate
*    Create an I/O buffer management instance. 
*******************************************************************************/
int ioBuffCreate(ioBuffHandle_t *handle, const lib_mem_rec_t *mem_tbl) 
{
  ioBuffInst_t *inst;

  if(  (mem_tbl[0].size < sizeof(ioBuffInst_t))
     ||libChkAlign(mem_tbl[0].base, IOBUFF_INST_ALIGN) ) {
    // to add: check memory type
    return (IOBUFF_ERR_MEMORY);
  }

  inst = (ioBuffInst_t *)mem_tbl[0].base;
  
  *handle = (ioBuffHandle_t)inst;

  return (IOBUFF_NOERR);  
}

/*******************************************************************************
* API function: ioBuffInit
*    Initializes the I/O buffer management instance. 
*******************************************************************************/
int ioBuffInit (ioBuffHandle_t handle, const ioBuffParams_t *params)
{
  ioBuffInst_t *inst = (ioBuffInst_t *)handle;

  if(params == NULL) {
    return (IOBUFF_ERR_BAD_PARAMS);
  }

  if((params->base == NULL) || (params->nominalDelay >= params->size)) {
    return (IOBUFF_ERR_BAD_PARAMS);
  }

  /* initialize the instance */
  inst->base  = params->base;  // I/O memory pool base address 
  inst->size  = params->size;  // I/O memory pool size
  inst->end   = (void *)((uint_least32_t)inst->base + inst->size);
  inst->sync  = params->sync;           // synchronous read or synchronous write
  inst->nom_delay = params->nominalDelay;   // nominal delay for synchronous read/write

  //inst->rw_wrap_around = 0;
  inst->status = BUFF_STATUS_NORMAL;
  inst->read_ptr  = inst->read_complete_ptr  = inst->base;
  inst->write_ptr = inst->write_complete_ptr = (void *)((uint_least32_t)inst->base + inst->nom_delay);

//  memset(inst->base, 0, inst->nom_delay);

  return (IOBUFF_NOERR);  
} /* ioBuffInit */

/*******************************************************************************
* API function: Return read pointer(s)
*******************************************************************************/
int ioBuffGetReadPtrs(ioBuffHandle_t handle, size_t mem_size, 
                      void **ptr1, size_t *size1, void **ptr2, size_t *size2)
{
  uint_least32_t address_end_of_read;
  void * new_read_ptr;
  int ret_val;
  ioBuffInst_t *inst = (ioBuffInst_t *)handle;

  if(inst->status != BUFF_STATUS_OVERFLOW) {
    /* check if buffer underflows */
    if(  (  ((uint_least32_t)inst->write_complete_ptr >= (uint_least32_t)inst->read_ptr)
          &&(((uint_least32_t)inst->write_complete_ptr - 
              (uint_least32_t)inst->read_ptr) < mem_size)
         )
       ||(  ((uint_least32_t)inst->write_complete_ptr < (uint_least32_t)inst->read_ptr)
          &&(((uint_least32_t)inst->write_complete_ptr + inst->size - 
              (uint_least32_t)inst->read_ptr) < mem_size)
         )
      ) {
      /* Buffer underflows */ 
      inst->status = BUFF_STATUS_UNDERFLOW;
  
      if(inst->sync == IOBUff_READ_SYNC) {
        /*If it's read-synchronized, don't return from here, but still return
        read pointer(s) calculated after this.  */
#if 0        
        /* reset the delay between read and write_complete pointers */
        uint_least32_t delay_adjust = inst->nom_delay;
        if(delay_adjust < mem_size) {
          delay_adjust = mem_size;
        }
        inst->read_ptr = (void *)((uint_least32_t)inst->write_complete_ptr - 
                                                  delay_adjust);
        if(inst->read_ptr < inst->base) {
          inst->read_ptr = (void *)((uint_least32_t)inst->read_ptr + inst->size);
          //inst->rw_wrap_around = 1;   // set wrap around flag
        }
         
#endif
        /* Make a dummy frame and fill it with 0's */
        new_read_ptr = (void *)((uint_least32_t)inst->write_complete_ptr - mem_size);
        if(new_read_ptr < inst->base) {
          new_read_ptr = (void *)((uint_least32_t)new_read_ptr + inst->size);
          
          memset(new_read_ptr, 0, mem_size - ((uint_least32_t)inst->write_complete_ptr - (uint_least32_t)inst->base));
          memset(inst->base, 0, (uint_least32_t)inst->write_complete_ptr - (uint_least32_t)inst->base);
        }
        else {
          memset(new_read_ptr, 0, mem_size);
        }
        
        /* Set read_ptr for calculating returning read pointer(s) below */
        inst->read_ptr = new_read_ptr;
         
        ret_val = IOBUFF_ERR_UNDERFLOW;
      }
      else {
        /* Return here if it underflows but is not read-synchronized. */
        *ptr1 = NULL; *size1 = 0;
        *ptr2 = NULL; *size2 = 0;
        return(IOBUFF_ERR_UNDERFLOW);
      }
    }
    else {
      inst->status = BUFF_STATUS_NORMAL;
      ret_val = IOBUFF_NOERR;
    }
  } /* if(inst->status != BUFF_STATUS_OVERFLOW) */
  else {
    /* already in overflow and remain in overflow untill there is some distance 
       between read pointer and write pointer */
    ret_val = IOBUFF_ERR_OVERFLOW;    
  } 

  /* return 1 or 2 pointers depending buffer wraps around or not */
  *ptr1 = inst->read_ptr;  /* starting address to read */

  /* check if read pointer is going to wrap around during this read */
  address_end_of_read = (uint_least32_t)inst->read_ptr + mem_size;
  if(address_end_of_read <= (uint_least32_t)inst->end) {  
    /* read pointer not going to wrap aound - return only 1 pointer */
    *size1 = mem_size;
    *ptr2  = NULL; *size2 = 0;
    new_read_ptr = (void *)address_end_of_read;
    if(address_end_of_read == (uint_least32_t)inst->end) {
      new_read_ptr = inst->base;
    }
  }
  else {  
    /* read pointer going to wrap around - return 2 pointers */
    *size1 = (size_t)inst->end - (size_t)inst->read_ptr;
    *ptr2  = inst->base;
    *size2 = mem_size - *size1;
    new_read_ptr = (void *)((uint_least32_t)inst->base + *size2);   
  }

  /* update read pointer */
  inst->read_ptr = new_read_ptr; 

  return (ret_val);
} /* ioBuffGetReadPtrs */


/*******************************************************************************
* API function: Rewind read pointer by a given amount.
*******************************************************************************/
int ioBuffRewindReadPtr(ioBuffHandle_t handle, size_t rewind_size)
{
  uint_least32_t address_rewind_read;
  ioBuffInst_t *inst = (ioBuffInst_t *)handle;

  address_rewind_read = (uint_least32_t)inst->read_ptr - rewind_size;
  if(address_rewind_read < (uint_least32_t)inst->base) {
	address_rewind_read = (uint_least32_t)inst->end - ((uint_least32_t)inst->base-address_rewind_read );
  }
  inst->read_ptr = (void *)address_rewind_read;

  return IOBUFF_NOERR;
} /* ioBuffRewindReadPtr */

/*******************************************************************************
* API function: Return write pointer(s)
*******************************************************************************/
int ioBuffGetWritePtrs(ioBuffHandle_t handle, size_t mem_size, 
                       void **ptr1, size_t *size1, void **ptr2, size_t *size2)
{
  int ret_val;
  uint_least32_t address_end_of_write;
  void * new_write_ptr;
  ioBuffInst_t *inst = (ioBuffInst_t *)handle;

  /* check if buffer overflows or not */
  if(inst->status != BUFF_STATUS_UNDERFLOW) {   // this condition may need to be removed!!!
    if(  (  ((uint_least32_t)inst->read_complete_ptr < (uint_least32_t)inst->write_ptr)
          &&(((uint_least32_t)inst->read_complete_ptr + inst->size - 
              (uint_least32_t)inst->write_ptr) < mem_size)
         )
       ||(  ((uint_least32_t)inst->read_complete_ptr >= (uint_least32_t)inst->write_ptr)
          &&(((uint_least32_t)inst->read_complete_ptr - 
              (uint_least32_t)inst->write_ptr) < mem_size)
         )
      ) {
      /* Buffer overflows */
      inst->status = BUFF_STATUS_OVERFLOW;
  
      if(inst->sync == IOBUFF_WRITE_SYNC){
        /* If it's write-synchronized, don't return from here. Still return the 
           write pointer(s) calculated below. */
        ret_val = IOBUFF_ERR_OVERFLOW;
      }
      else {
        /* Return here if it overflows but is not write-synchronized. */
        *ptr1 = NULL; *size1 = 0;
        *ptr2 = NULL; *size2 = 0;
        return(IOBUFF_ERR_OVERFLOW);
      }
    } 
    else {
      inst->status = BUFF_STATUS_NORMAL;
      ret_val = IOBUFF_NOERR;
    }
  } 
  else {
    /* already in underflow and remain in underflow untill write pointer is 
       ahead of read pointer */
    //ret_val = IOBUFF_ERR_UNDERFLOW;
    ret_val = IOBUFF_NOERR;  // underflow is not an error when getting write pointers
  }

  /* return 1 or 2 pointers depending buffer wraps around or not */
  *ptr1 = inst->write_ptr;  /* starting address to write */

  /* check if write pointer is going to wrap around during this write */
  address_end_of_write = (uint_least32_t)inst->write_ptr + mem_size;
  if(address_end_of_write <= (uint_least32_t)inst->end) {  
    /* write pointer not going to wrap aound, return only 1 pointer */
    *size1 = mem_size;
    *ptr2  = NULL; *size2 = 0;

    new_write_ptr = (void *)address_end_of_write;
    if(address_end_of_write == (uint_least32_t)inst->end) {
      new_write_ptr = inst->base;
    }
  }
  else {  
    /* write pointer going to wrap around, return 2 pointers */
    *size1 = (size_t)inst->end - (size_t)inst->write_ptr;
    *ptr2  = inst->base;
    *size2 = mem_size - *size1;
    new_write_ptr = (void *)((uint_least32_t)inst->base + *size2);
  }

  /* update write pointer */
  inst->write_ptr = new_write_ptr;

  return (ret_val);
} /* ioBuffGetWritePtrs */

/*******************************************************************************
* API function: Mark read completion
*******************************************************************************/
int ioBuffReadComplete(ioBuffHandle_t handle, void *read_begin, size_t read_size)
{
  ioBuffInst_t *inst = (ioBuffInst_t *)handle;

  inst->read_complete_ptr = (void *)((uint_least32_t)read_begin+read_size);
  if(inst->read_complete_ptr >= inst->end) {
    inst->read_complete_ptr = (void *) ((uint_least32_t)inst->read_complete_ptr 
                                        - inst->size);
    //inst->rw_wrap_around ^= 1;   // toggle wrap around flag
  }

  return (IOBUFF_NOERR);
}  /* ioBuffReadComplete */

/*******************************************************************************
* API function: Mark write completion
*******************************************************************************/
int ioBuffWriteComplete(ioBuffHandle_t handle, void *write_begin, size_t write_size)
{
  ioBuffInst_t *inst = (ioBuffInst_t *)handle;

  inst->write_complete_ptr = (void *)((uint_least32_t)write_begin+write_size);
  if(inst->write_complete_ptr >= inst->end) {
    inst->write_complete_ptr = (void *)((uint_least32_t)inst->write_complete_ptr 
                                        - inst->size);
    //inst->rw_wrap_around ^= 1;   // toggle wrap around flag
  }

  return (IOBUFF_NOERR);
}  /* ioBuffWriteComplete */


int ioBuffGetInfo(ioBuffHandle_t handle, ioBuffInfo_t *info)
{
  ioBuffInst_t *inst = (ioBuffInst_t *)handle;

  info->base  = inst->base;
  info->read  = inst->read_ptr;
  info->write = inst->write_ptr;
  info->size  = inst->size;

  return (IOBUFF_NOERR);
} /* ioBuffGetInfo */


void * ioBuffWrapPointer(ioBuffHandle_t handle, void *pPtr)
{
  uint_least32_t ptr_temp;
  ioBuffInst_t *inst = (ioBuffInst_t *)handle;

  ptr_temp = (uint_least32_t)pPtr;
  if(ptr_temp >= (uint_least32_t)inst->end) {
    ptr_temp = (uint_least32_t)inst->base + ptr_temp - (uint_least32_t)inst->end;
  }

  return ((void *)ptr_temp);
} /* ioBuffWrapPointer */

#if 0
int ioBuffWrapPointer(ioBuffHandle_t handle, void **pPtr)
{
  uint_least32_t ptr_temp;
  ioBuffInst_t *inst = (ioBuffInst_t *)handle;

  ptr_temp = (uint_least32_t)*pPtr;
  if(ptr_temp >= (uint_least32_t)inst->end) {
    ptr_temp = (uint_least32_t)inst->base + ptr_temp - (uint_least32_t)inst->end;
  }

  *pPtr = (void *)ptr_temp;

  return (IOBUFF_NOERR);
} /* ioBuffWrapPointer */
#endif

int ioBuffAdjustDelay(ioBuffHandle_t handle, size_t delay)
{
    size_t temp;
    ioBuffInst_t *inst = (ioBuffInst_t *)handle;

    if( (size_t)inst->write_complete_ptr >= ((size_t)inst->base+delay) ) {
        inst->read_ptr = inst->read_complete_ptr = (void *)((size_t)inst->write_complete_ptr - delay);
        //inst->rw_wrap_around = 0;
    }
    else {
        temp = delay - ((size_t)inst->write_complete_ptr - (size_t)inst->base);
        inst->read_ptr = inst->read_complete_ptr = (void *)((size_t)inst->end - temp);
        //inst->rw_wrap_around = 1;
    }

    inst->nom_delay = delay;

    return (IOBUFF_NOERR);
} /* ioBuffAdjustDelay */

/* Nothing past this point */
