
#ifndef FRAME_WORK_H
#define FRAME_WORK_H

#include "ioPhy.h"
#include "ioBuff.h"
#include "ioData.h"

#include "mcasp_cfg.h"
#include "audioStreamInpProc.h"
#include "audioStreamOutProc.h"

#define USE_IO_COMPONENTS           // define this to use I/O components: I/O BUFF and I/O PHY
                                    // undefine for loopback test without using I/O components

/** Num Bufs to be issued and reclaimed */
#define NUM_BUFS                (8)    // Used for validation testing (not used for PASDK)

#define BUFALIGN                (128) /* Alignment of buffer for use of L2 cache */

/* Number of I/O buffers - used for validation testing (not used for PASDK). */
//#define NUM_INPUT_BUFS             (30*3)  // large enough to hold 3 THD frames
#define NUM_INPUT_BUFS             (30*4)  // large enough to hold 3 THD frames
#define NUM_OUTPUT_BUFS            (12)

/* Number of buffers constituting the nominal delay for I/O buffer management. */
#define NUM_DELAY_BUFS_INPUT     1
#define NUM_DELAY_BUFS_OUTPUT    3

/* Size of total I/O buffer - used for validation testing (not used for PASDK). */
#define INPUT_BUFF_SIZE         (1024*2)*NUM_INPUT_BUFS     // large enough to hold 3 THD frames
#define OUTPUT_BUFF_SIZE        (OUTPUT_FRAME_SIZE * NUM_OUTPUT_BUFS)

/* Operation status of I/O buffer management. */
enum {
  FW_IOBUF_NORMAL = 0,
  FW_IOBUF_UNDERFLOW,
  FW_IOBUF_OVERFLOW
};


typedef struct fwInst_s {
    PAF_AST_Config astCfg;
    PAF_AST_IoInp  ioInp;
    PAF_AST_IoOut  ioOut;
    void *       iomem_pool_input_base;
    size_t       iomem_pool_input_size;
    void *       iomem_pool_output_base;
    size_t       iomem_pool_output_size;

    void *       hMcaspRxChan;
    uint_fast16_t  buf_status_in;
    uint_fast16_t  buf_status_out;

    //uint_least16_t syncState;
    //uint_least16_t syncStatePrev;

}fwInst_t;

#endif
