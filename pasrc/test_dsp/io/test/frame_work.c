
#include <stdio.h>
#include <xdc/runtime/Error.h>
#include "../io/ioPhy.h"
#include "../io/ioBuff.h"
#include "frame_work.h"
#include <ti/sysbios/io/IOM.h>  // for definition of Ptr 
#include "audioStreamProc_common.h"
#include "mcasp_cfg.h"
#include "ioConfig.h"

#ifdef IO_LOOPBACK_TEST

fwInst_t fwInst;

/* Handle to heap object */
//extern HeapMem_Handle heapHandle;
//extern HeapMem_Handle HEAP_EXTERNAL;
extern HeapMem_Handle heapMemDdr3;
extern HeapMem_Handle heapMemL2Sram;

/* McASP channel handles */
extern Ptr hMcaspTxChan;
extern Ptr hMcaspRxChan;

Ptr databuf[NUM_BUFS];

extern Int PAF_ASIT_ioCompCreate(PAF_AST_IoInp *pIoInp, int numInp, IHeap_Handle iHeapHandle);
extern Int PAF_ASOT_ioCompCreate(PAF_AST_IoOut *pIoOut, int numOut, IHeap_Handle iHeapHandle);

void ioCompsCreate()
{
    int errLineNum;
    IHeap_Handle iheap;

    iheap = HeapMem_Handle_to_xdc_runtime_IHeap(heapMemDdr3);

    /* I/O components memory for input - only 1 input for standalone test */
    errLineNum = PAF_ASIT_ioCompCreate(&fwInst.ioInp, 1, iheap);
    if(errLineNum)
    {
        printf("Memory allocation fails for input I/O components!\n");
        exit(0);
    }

    /* I/O components memory for output  - only 1 output for standalone test  */
    errLineNum = PAF_ASOT_ioCompCreate(&fwInst.ioOut, 1, iheap);
    if(errLineNum)
    {
        printf("Memory allocation fails for output I/O components!\n");
        exit(0);
    }
}

extern Ptr txBuf[NUM_BUFS];
extern Ptr rxBuf[NUM_BUFS];
extern Ptr procBuf;

void ioBuffersCreate(void)
{
    Error_Block  eb;
    uint32_t     count = 0;
    IHeap_Handle iheap;
    void *io_mempool_in;
    void *io_mempool_out;

    iheap = HeapMem_Handle_to_xdc_runtime_IHeap(heapMemDdr3);
    Error_init(&eb);

#ifdef USE_IO_COMPONENTS
    /* Allocate memory for audio input memory pool                          */
    io_mempool_in = Memory_calloc(iheap, INPUT_BUFF_SIZE, BUFALIGN, &eb);
    if(NULL == io_mempool_in){
      System_printf("\r\nMEM_calloc for io_mempool_in failed.\n");
      exit(0);
    }
    fwInst.iomem_pool_input_base = io_mempool_in;
    fwInst.iomem_pool_input_size = INPUT_BUFF_SIZE;

    /* Allocate buffers for audio data processing                           */
    for(count = 0; count < (NUM_BUFS); count ++)
    {
        databuf[count] = Memory_calloc(iheap, INPUT_FRAME_SIZE_PCM * 4,
                                       BUFALIGN, &eb);
        if(NULL == databuf[count]) {
            System_printf("\r\nMEM_calloc failed.\n");
            exit(0);
        }
    }

    /* Allocate memory for audio output memory pool                          */
    io_mempool_out = Memory_calloc(iheap, OUTPUT_BUFF_SIZE, BUFALIGN, &eb);
    if(NULL == io_mempool_out){
      System_printf("\r\nMEM_calloc for io_mempool_out failed.\n");
      exit(0);
    }
    fwInst.iomem_pool_output_base = io_mempool_out;
    fwInst.iomem_pool_output_size = OUTPUT_BUFF_SIZE;

#else
    /* Allocate buffers for the McASP data exchanges */
    for(count = 0; count < NUM_BUFS; count++)
    {
        rxBuf[count] = Memory_calloc(iheap, INPUT_FRAME_SIZE_PCM,
                                     BUFALIGN, &eb);
        if(NULL == rxBuf[count])
        {
            IFPRINT(platform_write("\r\nMEM_calloc failed for Rx\n"));
        }
    }

    /* Allocate buffers for the McASP data exchanges */
    for(count = 0; count < NUM_BUFS; count++)
    {
        txBuf[count] = Memory_calloc(iheap, INPUT_FRAME_SIZE_PCM,
                                     BUFALIGN, &eb);
        if(NULL == txBuf[count])
        {
            IFPRINT(platform_write("\r\nMEM_calloc failed for Tx\n"));
        }
    }

    procBuf = Memory_calloc(iheap, INPUT_FRAME_SIZE_PCM, BUFALIGN, &eb);
#endif

} /* ioBuffersCreate */

void audioIoCreate(void)
{
#ifdef USE_IO_COMPONENTS
    ioCompsCreate();
#endif

    ioBuffersCreate();
}

extern const MdUns iecFrameLength[23];

void ioCompsInitInput()
{
    ioDataParam_t  ioDataCfg;
    ioBuffParams_t ioBuffParams;
    ioPhyParams_t  ioPhyParams;

    ioBuffParams.base         = fwInst.iomem_pool_input_base;
    ioBuffParams.size         = fwInst.iomem_pool_input_size;
    ioBuffParams.sync         = IOBUFF_WRITE_SYNC;
    ioBuffParams.nominalDelay = INPUT_FRAME_SIZE_DEF*NUM_DELAY_BUFS_INPUT;
    if(ioBuffInit(fwInst.ioInp.hIoBuff, &ioBuffParams) != IOBUFF_NOERR) {
      printf("Input ioBuffInit error!\n");
      exit(0);
    }

    ioPhyParams.ioBuffHandle    = fwInst.ioInp.hIoBuff;
#ifdef INPUT_PCM_ONLY
    ioPhyParams.xferFrameSize   = INPUT_FRAME_SIZE_PCM;
#else
    ioPhyParams.xferFrameSize   = INPUT_FRAME_SIZE_DEF;
#endif
    ioPhyParams.mcaspChanHandle = hMcaspRxChan;
    ioPhyParams.ioBuffOp        = IOPHY_IOBUFFOP_WRITE;
    if(ioPhyInit(fwInst.ioInp.hIoPhy, &ioPhyParams) != IOPHY_NOERR) {
      printf("Input ioPhyInit error!\n");
      exit(0);
    }

    ioDataCfg.ioBuffHandle         = fwInst.ioInp.hIoBuff;
    ioDataCfg.unknownSourceTimeOut = 30720; //30720;
    ioDataCfg.frameLengthsIEC      = (uint_least16_t *)&iecFrameLength[0];
    ioDataCfg.frameLengthPCM       = INPUT_FRAME_SIZE_PCM / WORD_SIZE_PCM;
    ioDataCfg.frameLengthDef       = INPUT_FRAME_SIZE_DEF / WORD_SIZE_BITSTREAM;
    ioDataCfg.ibMode               = 0;
    ioDataCfg.zeroRunRestart       = 300;
    ioDataCfg.zeroRunTrigger       = 20;

    if(ioDataInit(fwInst.ioInp.hIoData, &ioDataCfg) != IODATA_NO_ERR) {
        printf("Input ioDataInit error!\n");
        exit(0);
    }

} /* ioCompsInitInput */

void ioCompsInitOutput()
{
    ioBuffParams_t ioBuffParams;
    ioPhyParams_t  ioPhyParams;

    ioBuffParams.base         = fwInst.iomem_pool_output_base;
    ioBuffParams.size         = fwInst.iomem_pool_output_size;
    ioBuffParams.sync         = IOBUff_READ_SYNC;
    ioBuffParams.nominalDelay = OUTPUT_FRAME_SIZE*NUM_DELAY_BUFS_OUTPUT;
    if(ioBuffInit(fwInst.ioOut.hIoBuff, &ioBuffParams) != IOBUFF_NOERR) {
      printf("Output ioBuffInit error!\n");
      exit(0);
    }

    ioPhyParams.ioBuffHandle    = fwInst.ioOut.hIoBuff;
    ioPhyParams.xferFrameSize   = OUTPUT_FRAME_SIZE;
    ioPhyParams.mcaspChanHandle = hMcaspTxChan;
    ioPhyParams.ioBuffOp        = IOPHY_IOBUFFOP_READ;
    if(ioPhyInit(fwInst.ioOut.hIoPhy, &ioPhyParams) != IOPHY_NOERR) {
      printf("Output ioPhyInit error!\n");
      exit(0);
    }
} /* ioCompsInitOutput */


#endif //#ifdef IO_LOOPBACK_TEST

