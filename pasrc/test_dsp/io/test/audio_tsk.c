
//#include "analog_test.h"
#include "frame_work.h"
#include "ioBuff.h"
#include "ioData.h"
#include "ioPhy.h"
#include <ti/drv/mcasp/mcasp_drv.h>
#include "mcasp_cfg.h"
#include "ioConfig.h"

#ifdef IO_LOOPBACK_TEST

//#define TEST_OVERFLOW_UNDERFLOW
//#define TEST_OVERFLOW_OVERFLOW
#define NUM_BUFS_USED  8

#define INPUT_SWITCH_HANGOVER 8

/* Frame index for Rx and Tx buffers */
uint8_t rxFrameIndex = 0;
uint8_t txFrameIndex = 0;
uint32_t rxBufIndex = 0;
uint32_t txBufIndex = 0;

/* McASP Tx and Rx frame buffers */
MCASP_Packet rxFrame[NUM_BUFS];
MCASP_Packet txFrame[NUM_BUFS];

/* McASP Tx and Rx frame buffer pointers */
Ptr txBuf[NUM_BUFS];
Ptr rxBuf[NUM_BUFS];
Ptr procBuf;

/* Flags for counting Rx and Tx interrupts */
volatile uint32_t rxFlag = 0;
volatile uint32_t txFlag = 0;

int dataFrameIndex=0;
int RxTSKcnt = 0, TxTSKcnt = 0;
volatile int RxFlag=0, RxFlag1=0, RxFlag2=0, TxFlag=0, TxFlag1=0, TxFlag2=0;

/* Semaphore handle for Tx and Rx */
Semaphore_Handle semR;
Semaphore_Handle semT;
Semaphore_Params SemParams;

uint32_t mcaspRxChanArg = 1;
uint32_t mcaspTxChanArg = 2;
int num_gbl_err = 0;
int txCount = 0;
int rxCount = 0;
int resetCount = 0;

extern Mcasp_ChanParams  mcaspRxChanParam;

/* McASP channel handles */
extern Ptr hMcaspTxChan;
extern Ptr hMcaspRxChan;

/* McASP device handles */
extern Ptr hMcaspDevTx;
extern Ptr hMcaspDevRx;

extern uint32_t gblErrFlag;


fwInst_t fwInst;
extern Ptr databuf[NUM_BUFS];

extern signed char*  getGlobalAddr(signed char* addr);

void inputXferStart();
void outputXferStart();

int check_mcasp_rx_overrun(void);
void mcasp_rx_restart(void);
int check_mcasp_tx_underrun(void);
void mcasp_tx_restart(void);

void mcaspRecfgFor16BitPacked(Ptr hMcaspChan, Mcasp_ChanParams *chanParams);
void mcaspRecfgFor32Bit(Ptr hMcaspChan, Mcasp_ChanParams *chanParams);

extern void ioCompsInitInput();
extern void ioCompsInitOutput();



#ifndef USE_IO_COMPONENTS
/**
 *  \brief   McASP callback function called up on the data transfer completion
 *
 *  \param  arg   [IN]  - Application specific callback argument
 *  \param  ioBuf [IN]  - McASP IO buffer
 *
 *  \return    None
 */
void mcaspAppCallbackRx(void *arg, MCASP_Packet *ioBuf)
{
	/* Callback is triggered by Rx completion */

		rxFlag++;

		/* Post semaphore */
		Semaphore_post(semR);
}

void mcaspAppCallbackTx(void *arg, MCASP_Packet *ioBuf)
{
	/* Callback is triggered by Tx completion */

	    txFlag++;

		/* Post semaphore */
		Semaphore_post(semT);
}

#else

void *mcaspRxBuf1;
void *mcaspRxBuf2;
uint32_t mcaspRxSize1;
uint32_t mcaspRxSize2;
int mcaspRxtwoXfers = 0;

void mcaspAppCallbackRx(void* arg, MCASP_Packet *mcasp_packet)
{
    RxFlag++;

    /* post semaphore */
	if(mcasp_packet->arg == IOPHY_XFER_FINAL) {
        RxFlag1++;

        Semaphore_post(semR);
    } else {
        RxFlag2++;
    }
}

void mcaspAppCallbackTx(void* arg, MCASP_Packet *mcasp_packet)
{
	TxFlag++;

    /* post semaphore */
	if(mcasp_packet->arg == IOPHY_XFER_FINAL) {
        TxFlag1++;

        Semaphore_post(semT);
    } else {
        TxFlag2++;
    }
}
#endif


#ifndef USE_IO_COMPONENTS

/**
 *  \brief   Task to echo the input data to output
 *
 *  Waits for the McASP data transfer completion and copies the
 *  Rx data to Tx buffers
 *
 *  \return    Aud_EOK on Success or error code
 */
Void taskAsipFxn(
    const PAF_ASIT_Params *pP,  //JX: asip_params_PAi defined in itopo/params.c
    const PAF_ASIT_Patchs *pQ   //JX: asop_patchs_PAi defined in itopo/patchs.c
)
{
    int32_t count;
    Int     status;

	inputXferStart();

    /* Forever loop to continuously receive and transmit audio data */
	count = 0;
    while (1)
    {
    	if(gblErrFlag)
    	{
    		break;
		}

        //Log_info0("Pending Rx semaphore");

    	/* Reclaim full buffer from the input stream */
    	Semaphore_pend(semR, BIOS_WAIT_FOREVER);

        /* Copy the receive information to the transmit buffer */
        Cache_inv(rxBuf[rxFrameIndex], INPUT_FRAME_SIZE, Cache_Type_ALL, TRUE);
        memcpy(procBuf, rxBuf[rxFrameIndex], INPUT_FRAME_SIZE);

        /* Issue an empty buffer to the input stream                          */
		rxFrame[rxFrameIndex].cmd    = MCASP_READ;
		rxFrame[rxFrameIndex].addr   = (void*)(getGlobalAddr(rxBuf[rxFrameIndex]));
		rxFrame[rxFrameIndex].size   = INPUT_FRAME_SIZE;
		rxFrame[rxFrameIndex].arg    = (uint32_t) mcaspRxChanArg;
		rxFrame[rxFrameIndex].status = 0;
		rxFrame[rxFrameIndex].misc   = 1;   /* reserved - used in callback to indicate asynch packet */

        status = mcaspSubmitChan(hMcaspRxChan, &rxFrame[rxFrameIndex]);
		if((status != MCASP_COMPLETED) && (status != MCASP_PENDING))
		{
			Log_info0("mcaspSubmitChan for Rx Failed\n");
		}

        rxFrameIndex++;
        if(rxFrameIndex==NUM_BUFS_USED) {
        	rxFrameIndex = 0;
        }

        // test McASP LLD reset
        count += 1;
        if(count == 1000) {
            mcaspRxReset();
            mcaspRxChanParam.wordWidth = Mcasp_WordLength_32;
            mcaspRxCreate();
            inputXferStart();

            count = 0;
            resetCount += 1;
        }
	}
} /* taskAsipFxn */

Void taskAsopFxn(
    const PAF_ASOT_Params *pP,
    const PAF_ASOT_Patchs *pQ
)
{
    Int     status;

	outputXferStart();
    //Semaphore_Params_init(&params);

    /* Forever loop to continuously receive and transmit audio data */
    while (1)
    {
    	if(gblErrFlag)
    	{
    		break;
		}

        //Log_info0("Pending Tx semaphore");

        /* Reclaim full buffer from the input stream */
    	Semaphore_pend(semT, BIOS_WAIT_FOREVER);

        memcpy(txBuf[txFrameIndex], procBuf, OUTPUT_FRAME_SIZE);
	    Cache_wbInv(txBuf[txFrameIndex], OUTPUT_FRAME_SIZE, Cache_Type_ALL,TRUE);

        /* Issue full buffer to the output stream                             */
        /* TX frame processing */
		txFrame[txFrameIndex].cmd    = MCASP_WRITE;
		txFrame[txFrameIndex].addr   = (void*)(getGlobalAddr(txBuf[txFrameIndex]));
		txFrame[txFrameIndex].size   = OUTPUT_FRAME_SIZE;
		txFrame[txFrameIndex].arg    = (uint32_t) mcaspTxChanArg;
		txFrame[txFrameIndex].status = 0;
		txFrame[txFrameIndex].misc   = 1;   /* reserved - used in callback to indicate asynch packet */

		status = mcaspSubmitChan(hMcaspTxChan, &txFrame[txFrameIndex]);
		if((status != MCASP_COMPLETED) && (status != MCASP_PENDING))
		{
			Log_info0("mcaspSubmitChan for Tx Failed\n");
		}
		else {
			txCount++;
		}

        txFrameIndex++;
        if(txFrameIndex==NUM_BUFS_USED) {
        	txFrameIndex = 0;
        }

	}

    //testRet(0);
}


/* put input and output in the same task */
Void taskAsipFxn_1tsk(
    const PAF_ASIT_Params *pP,  //JX: asip_params_PAi defined in itopo/params.c
    const PAF_ASIT_Patchs *pQ   //JX: asop_patchs_PAi defined in itopo/patchs.c
)
{
    Int     status;

//    mcaspRxCfgPCM();

	inputXferStart();
	outputXferStart();

    /* Forever loop to continuously receive and transmit audio data */
    while (1)
    {
    	if(gblErrFlag)
    	{
    		break;
		}

    	/* pending on both Rx and Tx semaphore */
    	Semaphore_pend(semR, BIOS_WAIT_FOREVER);
    	Semaphore_pend(semT, BIOS_WAIT_FOREVER);

        /* Copy the receive information to the transmit buffer */
        Cache_inv(rxBuf[rxFrameIndex], INPUT_FRAME_SIZE, Cache_Type_ALL, TRUE);
        memcpy(procBuf, rxBuf[rxFrameIndex], INPUT_FRAME_SIZE);

        memcpy(txBuf[txFrameIndex], procBuf, INPUT_FRAME_SIZE);
	    Cache_wbInv(txBuf[txFrameIndex], OUTPUT_FRAME_SIZE, Cache_Type_ALL,TRUE);

        /* Issue an empty buffer to the input stream                          */
		rxFrame[rxFrameIndex].cmd    = MCASP_READ;
		rxFrame[rxFrameIndex].addr   = (void*)(getGlobalAddr(rxBuf[rxFrameIndex]));
		rxFrame[rxFrameIndex].size   = INPUT_FRAME_SIZE;
		rxFrame[rxFrameIndex].arg    = (uint32_t) mcaspRxChanArg;
		rxFrame[rxFrameIndex].status = 0;
		rxFrame[rxFrameIndex].misc   = 1;   /* reserved - used in callback to indicate asynch packet */

        status = mcaspSubmitChan(hMcaspRxChan, &rxFrame[rxFrameIndex]);
		if((status != MCASP_COMPLETED) && (status != MCASP_PENDING))
		{
			Log_info0("mcaspSubmitChan for Rx Failed\n");
		}

        rxFrameIndex++;
        if(rxFrameIndex==NUM_BUFS_USED) {
        	rxFrameIndex = 0;
        }

        /* Issue full buffer to the output stream                             */
        /* TX frame processing */
		txFrame[txFrameIndex].cmd    = MCASP_WRITE;
		txFrame[txFrameIndex].addr   = (void*)(getGlobalAddr(txBuf[txFrameIndex]));
		txFrame[txFrameIndex].size   = OUTPUT_FRAME_SIZE;
		txFrame[txFrameIndex].arg    = (uint32_t) mcaspTxChanArg;
		txFrame[txFrameIndex].status = 0;
		txFrame[txFrameIndex].misc   = 1;   /* reserved - used in callback to indicate asynch packet */

		status = mcaspSubmitChan(hMcaspTxChan, &txFrame[txFrameIndex]);
		if((status != MCASP_COMPLETED) && (status != MCASP_PENDING))
		{
			Log_info0("mcaspSubmitChan for Tx Failed\n");
		}
		else {
			txCount++;
		}

        txFrameIndex++;
        if(txFrameIndex==NUM_BUFS_USED) {
        	txFrameIndex = 0;
        }
/*
        if(txCount == 3000) {
        	mcaspRxReset();
        	mcaspRxCreate();
        	inputXferStart();

        	txCount = 0;
        	resetCount += 1;
        }
*/
	}

    //testRet(0);
}

Void taskAsopFxn_1tsk(
    const PAF_ASOT_Params *pP,
    const PAF_ASOT_Patchs *pQ
)
{

}

#else    // else of #ifndef USE_IO_COMPONENTS

enum
{
    ASIP_SOURCE_DETECTION,
    ASIP_DECODE_BITSTREAM,
    ASIP_SWITCH_TO_PCM,
    ASIP_DECODE_PCM
};

int numFrameReceived, numPcmFrameReceived, numSyncLost, numUnderflow;
extern const MdUns iecFrameLength[23];

#define SYNC_PC_MASK    0x001F

void audio_rx_proc()
{
    void *buff1, *buff2;
    size_t size1, size2;

    /* Get read pointers of input memory pool */
    if (ioBuffGetReadPtrs(fwInst.ioInp.hIoBuff, INPUT_FRAME_SIZE_PCM,
                          &buff1, &size1, &buff2, &size2)
        == IOBUFF_ERR_UNDERFLOW) {
        fwInst.buf_status_in = FW_IOBUF_UNDERFLOW;
        //System_printf ("Debug: input buff underflows!\n");

        /* skip processing since there is no enough data to process */
        return;
    } else {
        fwInst.buf_status_in = FW_IOBUF_NORMAL;
    }

    /* copy the input data to data buffer                */
    Cache_inv(buff1, size1, Cache_Type_ALL, TRUE);
    memcpy(databuf[0],buff1,size1);
    ioBuffReadComplete(fwInst.ioInp.hIoBuff, buff1, size1);

    if(buff2 != NULL) {
      Cache_inv(buff2, size2, Cache_Type_ALL, TRUE);
      memcpy((void *)((size_t)databuf[0]+size1),buff2,size2);
      ioBuffReadComplete(fwInst.ioInp.hIoBuff, buff2, size2);
    }

    //Cache_wbInv(databuf[dataFrameIndex], INPUT_FRAME_SIZE, Cache_Type_ALL,TRUE);
} /* audio_rx_proc */

void audio_tx_proc()
{
    void *buff1, *buff2;
    size_t size1, size2;
    int status;
/*
    dataFrameIndex++;
    if(dataFrameIndex==NUM_BUFS) {
        dataFrameIndex = 0;
    }
*/
    // Get write pointers of output memory pool
    status = ioBuffGetWritePtrs(fwInst.ioOut.hIoBuff, OUTPUT_FRAME_SIZE,
                                &buff1, &size1, &buff2, &size2);
    if (status == IOBUFF_ERR_OVERFLOW) {
        fwInst.buf_status_out = FW_IOBUF_OVERFLOW;
        //System_printf ("Debug: output buff overflows in Tx proc!\n");

        /* skip processing since output buffer overflows */
        return;
    } 
    else if (status == IOBUFF_ERR_UNDERFLOW) {
        /* already underflows and remain in underflow */
        fwInst.buf_status_out = FW_IOBUF_UNDERFLOW;
    } 
    else {
        fwInst.buf_status_out = FW_IOBUF_NORMAL;
    }

    /* copy the output data from data buffer to output memory pool            */
    memcpy(buff1, databuf[0], size1);
    Cache_wbInv(buff1, size1, Cache_Type_ALL,TRUE);

    ioBuffWriteComplete(fwInst.ioOut.hIoBuff, buff1, size1);

    if(buff2 != NULL) {
      memcpy(buff2,(void *)((size_t)databuf[0]+size1),size2);
      Cache_wbInv(buff2, size2, Cache_Type_ALL,TRUE);

      ioBuffWriteComplete(fwInst.ioOut.hIoBuff, buff2, size2);
    }
}  /* audio_tx_proc */

Int rxDecodePcm(PAF_AST_IoInp  *pInp)
{
    ioDataCtl_t ioDataCtl;
    void *buffBase;
    void *dataStartAddress;
    size_t buffSize, frameSize, size1, size2;

    /* Get information for reading input data */
    ioDataCtl.code = IODATA_CTL_GET_INPBUFFINFO;
    ioDataControl(fwInst.ioInp.hIoData, &ioDataCtl);

    buffBase = ioDataCtl.param.dataReadInfo.buffBase;
    buffSize = ioDataCtl.param.dataReadInfo.buffSize;
    dataStartAddress = ioDataCtl.param.dataReadInfo.startAddress;
    frameSize        = ioDataCtl.param.dataReadInfo.frameSize;

    /* Depending on source information, either decode bit stream or simply
     * copy PCM data to output buffer.
     */
    // Copy PCM data to output buffer
    if(((size_t)dataStartAddress+frameSize) <= ((size_t)buffBase+buffSize)) {
        // Input buffer doesn't wrap around
        Cache_inv(dataStartAddress, frameSize, Cache_Type_ALL, TRUE);
        memcpy(databuf[0], dataStartAddress, frameSize);
    }
    else {
        // Input buffer wraps around
        size1 = (size_t)buffBase + buffSize - (size_t)dataStartAddress;
        size2 = frameSize - size1;
        Cache_inv(dataStartAddress, size1, Cache_Type_ALL, TRUE);
        memcpy(databuf[0], dataStartAddress, size1);

        Cache_inv(buffBase, size2, Cache_Type_ALL, TRUE);
        memcpy((void *)((size_t)databuf[0]+size1), buffBase, size2);
    }

    return ASIP_NO_ERR;

} /* rxDecodeProcessing */

Int rxDecodeBitStream(PAF_AST_IoInp  *pInp)
{
    return ASIP_NO_ERR;
}

Int rxDecodePlayZero(PAF_AST_IoInp  *pInp)
{
    memset(databuf[0], 0, OUTPUT_FRAME_SIZE);
}

extern void asipProcInit(PAF_AST_IoInp  *pInp);
extern Int asipProcessing(PAF_AST_Config *pAstCfg, PAF_AST_IoInp  *pInp);

Void taskAsipFxn(
    const PAF_ASIT_Params *pP,  //JX: asip_params_PAi defined in itopo/params.c
    const PAF_ASIT_Patchs *pQ   //JX: asop_patchs_PAi defined in itopo/patchs.c
)
{
    Int asipStatus;

    /* Initialize I/O components for input */
    ioCompsInitInput();

    asipProcInit(&fwInst.ioInp);

	/* Start McASP LLD  for input transfer */
	inputXferStart();

	numFrameReceived = 0;
    numPcmFrameReceived = 0;
	numSyncLost = 0;

    /* Forever loop to continously receviec and transmit audio data           */
    while (1)
    {
        if(gblErrFlag) {
            num_gbl_err++;
            break;
        }

        Semaphore_pend(semR, BIOS_WAIT_FOREVER);

        RxTSKcnt++;

        ioPhyXferComplete(fwInst.ioInp.hIoPhy, fwInst.ioInp.swapData);

#ifndef INPUT_PCM_ONLY
//        rxAutoProcessing();

//        rxDecodeProcessing();

        // Mark the input as read complete
//        if(fwInst.ioInp.autoProcState != ASIP_SWITCH_TO_PCM) {
//            ioDataReadComplete(fwInst.ioInp.hIoData);
//        }

        asipStatus = asipProcessing(&fwInst.astCfg, &fwInst.ioInp);

        if(asipStatus != ASIP_NO_ERR) {
            printf("ASIP processing error! Error code is %d\n", asipStatus);
            exit(0);
        }

#else
        // Input is configured to PCM data
        audio_rx_proc();

        rxCount++;
        if(rxCount == 2000) {
            mcaspRecfgFor32Bit();
        }

        if(rxCount == 4000) {
            mcaspRecfgFor16BitPacked();
            rxCount = 0;
        }

#if 0
        // test McASP LLD reset
        count += 1;
        if(count == 1000) {
            mcaspRxReset();
            mcaspRxChanParam.wordWidth = Mcasp_WordLength_32;
            mcaspRxCreate();
            status = ioPhyXferErrRecover(fwInst.ioInp.hIoPhy);
            if(status != IOPHY_NOERR) {
                printf("ioPhyXferErrRecover fails with error code %d!\n.", status);
                exit(0);
            }

            count = 0;
            resetCount += 1;
        }
#endif
#endif

        if(check_mcasp_rx_overrun()) {
            mcasp_rx_restart();
            ioPhyXferErrRecover(fwInst.ioInp.hIoPhy);
        }
        else {
            if(ioPhyXferSubmit(fwInst.ioInp.hIoPhy) == IOPHY_ERR_BUFF_OVERFLOW) {
              fwInst.buf_status_in = FW_IOBUF_OVERFLOW;
              printf("Input buffer overflows in Rx task!\n");
              exit(0);
            } else {
              fwInst.buf_status_in = FW_IOBUF_NORMAL;
            }
        }
    } /* while (1) */

} /* taskAsipFxn */


Void taskAsopFxn(const PAF_ASOT_Params *pP, const PAF_ASOT_Patchs *pQ)
{
    /* Initialize I/O components for output */
    ioCompsInitOutput();

    /* Start McASP LLD for output transfer */
	outputXferStart();

	/* Forever loop to continously receviec and transmit audio data           */
    //for (i32Count = 0; i32Count >= 0; i32Count++)
    while (1)
    {
        if(gblErrFlag) {
            num_gbl_err++;
            break;
        }

        Semaphore_pend(semT, BIOS_WAIT_FOREVER);

        TxTSKcnt++;

        ioPhyXferComplete(fwInst.ioOut.hIoPhy, FALSE);

#ifdef TEST_OVERFLOW_UNDERFLOW
        if(TxTSKcnt == 200) {
          while(fwInst.buf_status_out != FW_IOBUF_OVERFLOW) {
            audio_tx_proc();  // create output buffer overflow situation
          }
        }

        if(   (TxTSKcnt < 250) || (TxTSKcnt > 260) 
           || (fwInst.buf_status_out == FW_IOBUF_UNDERFLOW)) {
            audio_tx_proc();  // create output buffer underflow situation
                              // between 250 and 260
        }

        if(fwInst.buf_status_out == FW_IOBUF_UNDERFLOW) {
            audio_tx_proc();  // process one more frame during underflow
        }

        if(TxTSKcnt == 500) {
            TxTSKcnt = 0;
            TxFlag   = 0;
            TxFlag1  = 0;
            TxFlag2  = 0;
        }
#else
        audio_tx_proc();  // process one more frame during underflow
#endif

        if(check_mcasp_tx_underrun()) {
        	mcasp_tx_restart();
        }
        else {
            if(ioPhyXferSubmit(fwInst.ioOut.hIoPhy)==IOPHY_ERR_BUFF_UNDERFLOW) {
              fwInst.buf_status_out = FW_IOBUF_UNDERFLOW;
              //System_printf("Debug: output buffer underflows in Tx task!\n");
            }
            else {
              fwInst.buf_status_out = FW_IOBUF_NORMAL;
            }
        }
    } /* while (1) */
} /* taskAsopFxn */

#endif  // end of #ifndef USE_IO_COMPONENTS



extern const uint_least16_t IECframeLength[23];

void inputXferStart(void)
{
    Int32        count;

#ifdef USE_IO_COMPONENTS
    for(count = 0; count < NUM_PRIME_XFERS; count++)
    {
        ioPhyXferSubmit(fwInst.ioInp.hIoPhy);
    }
#else
    int        status;

    for(count = 0; count < NUM_PRIME_XFERS; count++)
    {
        /* Issue the first & second empty buffers to the input stream */
    	memset((uint8_t *)rxBuf[count], 0x0, INPUT_FRAME_SIZE);

		/* RX frame processing */
		rxFrame[count].cmd    = MCASP_READ;
		rxFrame[count].addr   = (void*)(getGlobalAddr(rxBuf[count]));
		rxFrame[count].size   = INPUT_FRAME_SIZE;
		rxFrame[count].arg    = (uint32_t) mcaspRxChanArg;
		rxFrame[count].status = 0;
		rxFrame[count].misc   = 1;   /* reserved - used in callback to indicate asynch packet */

		/* Submit McASP packet for Rx */
		status = mcaspSubmitChan(hMcaspRxChan, &rxFrame[count]);
		if((status != MCASP_COMPLETED) && (status != MCASP_PENDING))
		{
			IFPRINT(platform_write("mcaspSubmitChan for Rx Failed\n"));
			exit(0);
		}
    }

//    rxFrameIndex = count;
#endif
} /* inputXferStart */

void outputXferStart(void)
{
    Int32        count;

	/* Create semaphores to wait for buffer reclaiming */
//    semT = Semaphore_create(0, &SemParams, &eb);
//    semT = Semaphore_create(0, NULL, NULL);

#ifdef USE_IO_COMPONENTS
    for(count = 0; count < NUM_PRIME_XFERS; count++)
    {
        if(ioPhyXferSubmit(fwInst.ioOut.hIoPhy)==IOPHY_ERR_BUFF_UNDERFLOW) {
          fwInst.buf_status_out = FW_IOBUF_UNDERFLOW;
          //System_printf("Debug, output buffer underflows in prime!\n");
        } else {
          fwInst.buf_status_out = FW_IOBUF_NORMAL;
        }
    }
#else
    int status;

    for(count = 0; count < NUM_PRIME_XFERS; count++)
    {
    	memset((uint8_t *)txBuf[count], 0x0, OUTPUT_FRAME_SIZE);

		/* TX frame processing */
		txFrame[count].cmd    = MCASP_WRITE;
		txFrame[count].addr   = (void*)(getGlobalAddr(txBuf[count]));
		txFrame[count].size   = OUTPUT_FRAME_SIZE;
		txFrame[count].arg    = (uint32_t) mcaspTxChanArg;
		txFrame[count].status = 0;
		txFrame[count].misc   = 1;   /* reserved - used in callback to indicate asynch packet */

		/* Submit McASP packet for Tx */
		status = mcaspSubmitChan(hMcaspTxChan, &txFrame[count]);
		if((status != MCASP_COMPLETED) && (status != MCASP_PENDING))
		{
			IFPRINT(platform_write("mcaspSubmitChan for Tx Failed\n"));
			exit(0);
		}
    }

//	txFrameIndex = count;
#endif
} /* outputXferStart */

int check_mcasp_rx_overrun(void)
{
    Mcasp_errCbStatus mcaspErrStat;

    mcaspControlChan(hMcaspRxChan, Mcasp_IOCTL_CHAN_QUERY_ERROR_STATS, &mcaspErrStat);

    return (mcaspErrStat.isRcvOvrRunOrTxUndRunErr);
}

void mcasp_rx_restart(void)
{
   	mcaspRxReset();
    mcaspRxCreate();
}

int check_mcasp_tx_underrun(void)
{
    Mcasp_errCbStatus mcaspErrStat;

    mcaspControlChan(hMcaspTxChan, Mcasp_IOCTL_CHAN_QUERY_ERROR_STATS, &mcaspErrStat);

    return (mcaspErrStat.isRcvOvrRunOrTxUndRunErr);
}

void mcasp_tx_restart(void)
{
   	mcaspTxReset();
    mcaspTxCreate();
   	ioPhyXferErrRecover(fwInst.ioOut.hIoPhy);
}

void ioSemaphoreCreate(void)
{
    Semaphore_Params_init(&SemParams);

    semR = Semaphore_create(0, &SemParams, NULL);
    semT = Semaphore_create(0, &SemParams, NULL);
}

#endif  // end of #ifdef IO_LOOPBACK_TEST

