
#ifndef _IOBUFF_H
#define _IOBUFF_H

#include <stdint.h>
#include "libarch.h"
/*
 *  I/O buffer management API
*/

enum {
  IOBUFF_NOERR         = 0,
  IOBUFF_ERR_HANDLE,
  IOBUFF_ERR_MEMORY,
  IOBUFF_ERR_BAD_PARAMS,
  IOBUFF_ERR_UNDERFLOW,
  IOBUFF_ERR_OVERFLOW
}; 

enum {
  IOBUff_NO_SYNC      = 0,
  IOBUff_READ_SYNC,
  IOBUFF_WRITE_SYNC,
  IOBUFF_RW_SYNC
};

// I/O memory pool configuration parameters
typedef struct ioBuffParams {
  void          *base;        /* base address of I/O memory provided by the framework */
  uint_fast32_t size;         /* size in MAU's (bytes) of I/O memory */
  int_fast16_t  sync;         /* sync type: IOBUff_READ_SYNC, WRITE_SYNC, NO_SYNC, RW_SYNC */
  uint_fast32_t nominalDelay; /* nominal delay as # of MAU's between write and read */
} ioBuffParams_t; 

typedef void * ioBuffHandle_t;

typedef struct ioBuffInfo_s {
  void *base;
  void *read;
  void *write;
  uint_fast32_t size;
} ioBuffInfo_t;

/**
 *  @brief ioBuffNumAlloc
 *    Returns the maximum number of memory allocation requests that ioBuffAlloc() 
 *    requires.
 */
int ioBuffNumAlloc(void);

/**
 *  @brief ioBuffAlloc
 *    Returns a table of memory records that describe the size, alignment, type 
 *    and memory space of all buffers required by this component. 
 */
int ioBuffAlloc(lib_mem_rec_t *mem_tbl);

/**
 *  @brief ioBuffCreate
 *    Based on the provided memory blocks, this function creates an I/O BUFF
 *    instance and returns a handle. This handle points to the memory block
 *    that contains the instance.
 */
int ioBuffCreate(ioBuffHandle_t *handle, const lib_mem_rec_t *mem_tbl);

/**
 *  @brief ioBuffInit
 *    Initializes the I/O buffer management instance. 
 */
int ioBuffInit (ioBuffHandle_t handle, const ioBuffParams_t *params);

/**
 *  @brief ioBuffGetReadPtrs
 *    Provides pointers to read a given size of data from the I/O buffer. 
 *      - If I/O buffer wrapps around, two pointers will be provided, each with 
 *        a corresponding read size. 
 *      - If I/O buffer does not wrapp around, one pointer and the corresponding 
 *        size will be provided. The size will be equal to the input mem_size. 
 *        The second pointer will be NULL and the corresponding size will be 
 *        zero. 
 *     
 *  @param[in]   handle
 *  @param[in]   mem_size       size of data to be read
 *  @param[out]  rdptr1         read pointer 1
 *  @param[out]  size1          read size corresponding to pointer 1
 *  @param[out]  rdptr2         read pointer 2 
 *  @param[out]  size2          read size corresponding to pointer 2
 *
 */
int ioBuffGetReadPtrs(ioBuffHandle_t handle, size_t mem_size, 
                    void **rdptr1, size_t *size1, 
                    void **rdptr2, size_t *size2);

/**
 *  @brief ioBuffGetWritePtrs
 *    Provides pointers to write a given size of data to the I/O buffer. 
 *      - If I/O buffer wrapps around, two pointers will be provided, each with 
 *        a corresponding write size. 
 *      - If I/O buffer does not wrapp around, one pointer and the corresponding 
 *        size will be provided. The size will be equal to the input mem_size. 
 *        The second pointer will be NULL and the corresponding size will be 
 *        zero. 
 *     
 *  @param[in]   handle
 *  @param[in]   mem_size       size of data to be written to the I/O buffer
 *  @param[out]  wrptr1         write pointer 1
 *  @param[out]  size1          write size corresponding to pointer 1
 *  @param[out]  wrptr2         write pointer 2 
 *  @param[out]  size2          write size corresponding to pointer 2
 *
 */
int ioBuffGetWritePtrs(ioBuffHandle_t handle, size_t mem_size, 
                    void **wrptr1, size_t *size1, 
                    void **wrptr2, size_t *size2);

/**
 *  @brief ioBuffReadComplete
 *    Marks a portion of the I/O buffer as read complete so that the data in it
 *    can be overwritten now.
 *     
 *  @param[in]  handle
 *  @param[in]  start          start address of the read-completed portion 
 *  @param[in]  size           size of the read-completed portion 
 *
 *  @remark   If two buffers, then size should be sum of two sizes.....
 */
int ioBuffReadComplete(ioBuffHandle_t handle, void *start, size_t size);

/**
 *  @brief ioBuffWriteComplete
 *    Marks a portion of the I/O buffer as write complete so that the data in it
 *    can be read now.
 *     
 *  @param[in]  handle
 *  @param[in]  start          start address of the write-completed portion 
 *  @param[in]  size           size of the write-completed portion 
 *
 */
int ioBuffWriteComplete(ioBuffHandle_t handle, void *start, size_t size);

int ioBuffGetInfo(ioBuffHandle_t handle, ioBuffInfo_t *info);

void * ioBuffWrapPointer(ioBuffHandle_t handle, void * pPtr);

int ioBuffAdjustDelay(ioBuffHandle_t handle, size_t delay);

#endif
