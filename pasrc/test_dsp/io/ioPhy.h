

#ifndef _IOPHY_H
#define _IOPHY_H

#include <stdint.h>
#include <ti/drv/mcasp/mcasp_drv.h>
#include "libarch.h"
#include "ioBuff.h"

enum {
  IOPHY_NOERR         = 0,
  IOPHY_ERR_HANDLE,
  IOPHY_ERR_MEMORY,
  IOPHY_ERR_BAD_PARAMS,
  IOPHY_ERR_MCASP_FAIL,
  IOPHY_ERR_BUFF_UNDERFLOW,
  IOPHY_ERR_BUFF_OVERFLOW,
  IOPHY_ERR_BUFF_BADPTRS,
  IOPHY_ERR_BATCH_XFER_FAIL
}; 

enum {
  IOPHY_IOBUFFOP_READ   = 0,
  IOPHY_IOBUFFOP_WRITE  = 1
};

typedef uint_fast16_t ioPhyBuffOp_t;

enum {
  IOPHY_XFER_INTER  = 0,
  IOPHY_XFER_FINAL,
  IOPHY_XFER_ERROR
};

typedef void * ioPhyHandle_t;

typedef struct ioPhyParams {
  void *         mcaspChanHandle;
  uint_least32_t xferFrameSize;  // generate INT when this amount of data has been transfered
                                 // can be changed at run time through control function
  ioBuffHandle_t ioBuffHandle;
  ioPhyBuffOp_t  ioBuffOp;       // READ or WRITE
} ioPhyParams_t; 

enum {
  IOPHY_CTL_FRAME_SIZE    = 0,
  IOPHY_CTL_INPUT_CHANGE,
  IOPHY_CTL_INIT_DRIVER
};

typedef struct ioPhyCtl_s {
  uint_fast16_t code;
  union {
    uint_least32_t xferFrameSize;
    void *         mcaspChanHandle;
  } params;
} ioPhyCtl_t;

int ioPhyNumAlloc(void);
int ioPhyAlloc(lib_mem_rec_t *memTab);
int ioPhyCreate(ioPhyHandle_t *handle, const lib_mem_rec_t *mem_tbl);
int ioPhyInit(ioPhyHandle_t handle, const ioPhyParams_t *params);

/**
 *  @brief ioPhyXferCfg
 *    Configures next transfer. 
 *    Reconfigure the EDMA driver link channel (PaRAM set) that has just finished 
 *    transfer and generated TCINT.
 *
 *    This function will call I/O buff API to get the write pointers.
 *
 *  @param[in]   handle
 *
 */
int ioPhyXferSubmit(ioPhyHandle_t handle);

/**
 *  @brief ioPhyXferComplete
 *    Marks complete the transfer which has just finished. This function will
 *    call I/O buff API to mark certain portion of the I/O buffer as
 *    write-completed. 
 *
 *  @param[in]   handle
 *
 */
int ioPhyXferComplete(ioPhyHandle_t handle, int dataSwap);

/**
 *  @brief ioPhyControl
 *    Changes I/O PHY parameters at run time.
 *
 *  @param[in]   handle
 *  @param[in]   ctl
 *
 *  Configurable parameters:
 *  - ctl->xfer_frame_size: transfer frame size in bytes. Transfer completion 
 *                          INT is generated after this amount of data is 
 *                          transfered.  
 */
int ioPhyControl(ioPhyHandle_t handle, ioPhyCtl_t *ctl);

int ioPhyXferErrRecover(ioPhyHandle_t handle);

int ioPhyCheckXferStatus(ioPhyHandle_t handle, MCASP_Packet *mcasp_packet);

#endif
