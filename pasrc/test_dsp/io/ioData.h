
#ifndef IODATA_H
#define IODATA_H

#include <stdint.h>
#include "ioBuff.h"

// I/O DATA error code
enum {
    IODATA_NO_ERR,
    IODATA_ERR_MEMORY,
    IODATA_ERR_BAD_PARAMS,
	IODATA_ERR_IOBUF_UNDERFLOW,
	IODATA_ERR_IOBUF_OVERFLOW
};

// define a type for I/O DATA handle
typedef void * ioDataHandle_t;

// I/O DATA configuration parameters
typedef struct ioDataParam_s{
  ioBuffHandle_t    ioBuffHandle;           // handle to the associated I/O BUFF
  uint_least16_t   *frameLengthsIEC;        // pointer to array of supported IEC frame lengths
  uint_least16_t    frameLengthPCM;         // frame length for PCM input
  uint_least16_t    frameLengthDef;         // default frame length for SYNC search
  int_least32_t     unknownSourceTimeOut;   // time out value to declare unknown source
  uint_least32_t    zeroRunTrigger;         // threshold to declare zero run
  uint_least32_t    zeroRunRestart;         // threshold to restart auto-detection under zero run
  int8_t            ibMode;                 // IB Mode
} ioDataParam_t;

// Information needed for decoders to read a bitstream frame
typedef struct ioDataReadInfo_s {
  void *buffBase;            // base address of the buffer holding the bitstream data
  uint_least32_t buffSize;   // size of the buffer holding the bitstream data
  void *startAddress;        // start address of next complete frame to read
  uint_least32_t frameSize;  // size of the frame in bytes
} ioDataReadInfo_t;

// Control commands for ioDataControl
enum {
	//IODATA_CTL_SET_ELEMENT_SIZE,
	IODATA_CTL_SET_PCM_FRAME_LENGTH,
	IODATA_CTL_GET_INPBUFFINFO,
    IODATA_CTL_GET_AUTODET_STATUS,
};

// SYNC search/detection status
enum
{
    IODATA_SYNC_NONE,
    IODATA_SYNC_ONE,
//	IODATA_SYNC_CHECK,
    IODATA_SYNC_BITSTREAM,
    IODATA_SYNC_PCM,
    IODATA_SYNC_PCM_FORCED,
    IODATA_SYNC_PCM_ZERORUN,
    IODATA_SYNC_AUTO
};

// Frame information
typedef struct ioDataFrameInfo_s {
  uint_fast32_t frameLength;       // length in units of elements
  int8_t        elementSize;       // size of elements in bytes
} ioDataFrameInfo_t;

typedef struct streamInfo_s {
    uint_least16_t IEC_PC;
    uint_least32_t frameLength;
    uint_least32_t dataLength;
} streamInfo_t;

// Auto-detection status
typedef struct ioDataAutoDetStat_s {
  uint_least16_t   syncState;      // SYNC state
  streamInfo_t     streamInfo;  // bit stream information - value of IEC_PC
  uint8_t          deliverZeros;   // flag to indicate whether zeros should be sent to output
  uint8_t          syncTimeOut;
} ioDataAutoDetStat_t;

// I/O DATA control structure
typedef struct ioDataCtl_s {
  uint_fast16_t code;
  union {
	ioDataReadInfo_t    dataReadInfo;   // IODATA_CTL_GET_INPBUFFINFO
	uint_fast32_t       frameLengthPcm; // IODATA_CTL_SET_PCM_FRAME_LENGTH
	ioDataAutoDetStat_t autoDetStats;   // IODATA_CTL_GET_AUTODET_STATUS
  } param;      
} ioDataCtl_t;

/**
 *  @brief ioDataNumAlloc
 *    Returns the maximum number of memory allocation requests that ioDataAlloc()
 *    requires.
 */
int ioDataNumAlloc(void);

/**
 *  @brief ioDataAlloc
 *    Returns a table of memory records that describe the size, alignment, type
 *    and memory space of all buffers required by I/O DATA component.
 */
int ioDataAlloc(lib_mem_rec_t *mem_tbl);

/**
 *  @brief ioDataCreate
 *    Based on the provided memory blocks, this function creates an I/O DATA
 *    instance and returns a handle. This handle points to the memory block
 *    that contains the instance.
 */
int ioDataCreate(ioDataHandle_t *handle, const lib_mem_rec_t *mem_tbl);

/**
 *  @brief ioDATAInit
 *    Initializes the I/O DATA instance.
 */
int ioDataInit(ioDataHandle_t handle, ioDataParam_t *cfgParams);

/**
 *  @brief ioDataProcess
 *    The main function of I/O DATA processing, i.e. auto-detection.
 */
int ioDataProcess(ioDataHandle_t handle);

/**
 *  @brief ioDataReadComplete
 *    This function marks as read complete the data in the input buffer that has
 *    been processed by ioDataProcess and the decoder. It should be called after
 *    ioDataProcess() is called and after the decoder finishes reading current
 *    frame in the input buffer.
 */
int ioDataReadComplete(ioDataHandle_t handle);


/**
 *  @brief ioDataControl
 *    This function allows run-time control of I/O DATA components through control
 *    command IODATA_CTL_XXX and control structure ioDataCtl_t.
 */
int ioDataControl(ioDataHandle_t handle, ioDataCtl_t *ctlParams);


#endif
