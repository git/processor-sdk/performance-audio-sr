
#include <stdint.h>

#define BUFF_STATUS_NORMAL     0 
#define BUFF_STATUS_UNDERFLOW  1
#define BUFF_STATUS_OVERFLOW   2

typedef struct ioBuffInst_s {

  void *base;
  void *end;
  void *read_ptr;
  void *read_complete_ptr;
  void *write_ptr;
  void *write_complete_ptr;

  uint_least32_t size;
  int_fast16_t   sync;   /* sync type: ioBUff_READ_SYNC, WRITE_SYNC, NO_SYNC, RW_SYNC */
  uint_fast32_t  nom_delay;  /* nominal delay as # of MAU's between write and read */

  uint_fast16_t  status;  /* overflow, underflow, normal */
  uint_fast16_t  rw_wrap_around;  /* flag to indicate write pointer wraps around 
                                     but read pointer has not yet */
} ioBuffInst_t;
