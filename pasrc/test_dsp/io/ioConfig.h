
#ifndef IO_CONFIG_H
#define IO_CONFIG_H

//#define PASDK_SIO_DEV         // define this to use the legacy SIO/DEV based I/O driver

//#define IO_LOOPBACK_TEST        // define this to run a DSP-only standalone loopback test without PASDK framework
//#define PCM_LOOPBACK_TEST       // define this to run DSP-only PCM loopback test in PASDK framework

//#define INPUT_HDMI_4xI2S        // HDMI, 4 Rx serializers
//#define INPUT_HDMI_STEREO       // HDMI, 1 Rx serializer
//#define INPUT_SPDIF             // S/PDIF, 1 Rx serializer

//#define INPUT_PCM_ONLY        // debugging, input fixed to PCM

#define TX_MCASP_USE_MULT_SER   // This is the normal operation. Undefine this for debugging and loopback

#ifdef INPUT_PCM_ONLY
#define SWAP_INPUT_DATA        FALSE
#else
#define SWAP_INPUT_DATA        TRUE
#endif

#ifdef INPUT_HDMI_4xI2S
#define RX_MCASP_USE_MULT_SER
#define IO_HW_INTERFACE          2
//#define ASIP_ELEMENT_SIZE      2
//#define ASIP_STRIDE            8    // 4 lane, stereo
#endif

#ifdef INPUT_HDMI_STEREO
//#define ASIP_ELEMENT_SIZE      4
//#define ASIP_STRIDE            2    // 1 lane, stereo
#define IO_HW_INTERFACE          1
#endif

#ifdef INPUT_SPDIF
//#define ASIP_ELEMENT_SIZE      4
//#define ASIP_STRIDE            2    // 1 lane, stereo
#define IO_HW_INTERFACE          3
#endif

// two options regarding I/O PHY transfer size and I/O DATA read size:
//    1. fixed to 1024 words: simple for implementation and debugging, shorter delay,
//            but more interrupts and more McASP LLD calls.
//    2. adjusted to frame size after auto-detection: less interrupts and McASP LLD calls,
//            but more complicated implementation and longer delay.
#define ADJUST_XFER_SIZE_AFTER_AUTODET


// Number of serializers configured for Rx McASP
#ifdef RX_MCASP_USE_MULT_SER
#define NUM_RX_SERIALIZER       4
#else
#define NUM_RX_SERIALIZER       1
#endif

// Number of serializers configured for Tx McASP
#ifdef TX_MCASP_USE_MULT_SER
#define NUM_TX_SERIALIZER       4
#else
#define NUM_TX_SERIALIZER       1
#endif

/* Number of submitted transfers in McASP LLD priming operation.
 * Due to McASP LLD issue, there must be 3 or more transfer submits in priming.  */
#define NUM_PRIME_XFERS_INPUT   4   // 4 and above is good for input
#define NUM_PRIME_XFERS_OUTPUT  2   // 2 is good for output

// Define McASP transfer element size: number of bytes in one word
#define WORD_SIZE_BITSTREAM     2
#define WORD_SIZE_PCM           4
#define NUM_CYCLE_PER_FRAME_DEF        1024
#define NUM_CYCLE_PER_OUTPUT_FRAME_DEF 256

// Input frame length: number of words to be transfered in 1 transfer of McASP LLD Rx
// These numbers are according to existing SIO/DEV based I/O.
#define INPUT_STRIDE            (2*NUM_RX_SERIALIZER)
#ifdef INPUT_HDMI_4xI2S
#define INPUT_FRAME_LENGTH      (NUM_CYCLE_PER_FRAME_DEF*INPUT_STRIDE)      // 128 stereo/lane
#else
//#define INPUT_FRAME_LENGTH      (64*INPUT_STRIDE)    // 64 stereo/lane
#define INPUT_FRAME_LENGTH      (NUM_CYCLE_PER_FRAME_DEF*INPUT_STRIDE)      // 128 stereo/lane
#endif

// Input frame size: number of bytes to be transfered in 1 transfer of McASP LLD Rx
#define INPUT_FRAME_SIZE_DEF    (INPUT_FRAME_LENGTH * WORD_SIZE_BITSTREAM)
#define INPUT_FRAME_SIZE_PCM    (INPUT_FRAME_LENGTH * WORD_SIZE_PCM)

// Output frame length: number of words to be transfered in 1 transfer of McASP LLD Tx
#define OUTPUT_STRIDE           (2*NUM_TX_SERIALIZER)
#define OUTPUT_FRAME_LENGTH     (NUM_CYCLE_PER_FRAME_DEF*OUTPUT_STRIDE)    // 128 stereo/lane

// Output frame size: number of bytes to be transfered in 1 transfer of McASP LLD Tx
#define OUTPUT_FRAME_SIZE       (OUTPUT_FRAME_LENGTH * WORD_SIZE_PCM)



#endif
