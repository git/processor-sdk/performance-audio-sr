

#include <stdint.h>
#include <limits.h> //INT_MAX

#include "ioData.h"
#include "ioBuff.h"

//#include "ioDataLoc.h"

#include <ti/sysbios/hal/Cache.h>

#define IODATA_NUM_MEM_ALLOCS   1
#define IODATA_INST_ALIGN       3

#define IODATA_NUM_BYTES_BITSTREAM  2
#define IODATA_NUM_BYTES_PCMSAMPLE  4

#define IODATA_ELEMENT_SIZE_DEF  IODATA_NUM_BYTES_BITSTREAM

#define TRUE                  1
#define FALSE                 0

#define IODATA_SYNC_PC_MASK         0x1F
#define IODATA_SYNC_SUBTYPE_MASK    0x700
#define IODATA_SYNC_SUBTYPE_SHIFT   8
#define IODATA_SYNC_SUBTYPE_DTSHD   0x11
#define IODATA_SYNC_DDP             0x15
#define IODATA_SYNC_THD             0x16

#define IODATA_MIN_NUM_ZEROS_BEFORE_PA   2
#define IODATA_PREAMBLE_MAX_LEN          16 // TBD
#define IODATA_HEADER_LENGTH_IEC         4
#define IODATA_HEADER_CHECK_LENGTH_IEC   2  // For IEC, only look at 2 words to check SYNC.

#define IEC_PA       ((uint_least16_t) 0xF872)
#define IEC_PB       ((uint_least16_t) 0x4E1F)
#define DTS16_SYNC_A ((uint_least16_t) 0x7FFE)
#define DTS16_SYNC_B ((uint_least16_t) 0x8001)
#define DTS16_SYNC_C ((uint_least16_t) 0xFC00)
#define DTS14_SYNC_A ((uint_least16_t) 0x1FFF)
#define DTS14_SYNC_B ((uint_least16_t) 0xE800)
#define DTS14_SYNC_C ((uint_least16_t) 0x07F0)

#define  DTS_BURST_TYPE_I  0x008B
#define  DTS_BURST_TYPE_II 0x018C
#define  DTS_BURST_TYPE_III 0x028D
#define  DTS_BURST_TYPE_IV  0x0491

#define  DTS_BURST_TYPE_IV_CBR  0x02
#define  DTS_BURST_TYPE_IV_LBR  0x03
#define  DTS_BURST_TYPE_IV_HBR  0x04

#define min(a, b)  (((a) < (b)) ? (a) : (b))
#define max(a, b)  (((a) > (b)) ? (a) : (b))

//IBMODE
enum
{
    IBMODE_DEFAULT = 0,
    IBMODE_NO_ZERORUNRESTART = 1,
    IBMODE_NO_ZERORUN = 2
};

enum {
	AUTODET_RESET = 0,
	AUTODET_SYNC_SEARCH_INITIAL,
	AUTODET_SYNC_SEARCH_PCM,
	AUTODET_SYNC_CHECK_ONGOING
};

// scanState
enum
{
    SCANNED_NONE,
    SCANNED_IEC_PA,
    SCANNED_IEC_PB,
    SCANNED_IEC_PC,
    SCANNED_IEC_PD,
    SCANNED_DTS14_SYNC_A,
    SCANNED_DTS14_SYNC_B,
    SCANNED_DTS14_SYNC_C,
    SCANNED_DTS14_SYNC_D,
    SCANNED_DTS16_SYNC_A,
    SCANNED_DTS16_SYNC_B,
    SCANNED_DTS16_SYNC_C
};

typedef struct autoDet_s {
  int_least32_t    frameLength;
  int_least32_t    timeOutCntr;
  uint_least32_t   zeroCount;
  uint_least32_t   numElementsRcvd;
  int_least32_t   distToFrameEnd;

  uint_least16_t   preambleRef[IODATA_PREAMBLE_MAX_LEN];
  uint_least16_t   preambleBuf[IODATA_PREAMBLE_MAX_LEN];
  int_fast32_t     preambleInd;
  uint8_t          headerLength;
  uint8_t          headerCheckLen;

  uint_least16_t    scanState;
  uint_least16_t    checkState;

  int8_t           ibMode;
  int8_t           timeOutStarted;
  int8_t           preambleStartsPrevBuf;
  int8_t           syncDetected;
  int8_t           syncLost;
  int8_t           completeFrameRcvd;
  uint_least16_t   syncState;
  uint_least16_t   bitStreamInfo;
  uint_least16_t   *bitStreamSyncPtr;
  uint_least16_t   *frameDataStartTemp;
  uint_least16_t   *frameDataStartConfirm;
} autoDet_t;

typedef struct ioDataDebug_s {
  uint_least32_t   numFramesRcvd;
  uint_least32_t   numBuffUnderflow;
  uint_least32_t   numSyncLost;

} ioDataDebug_t;

typedef struct ioDataInst_s {
  autoDet_t        autoDet;
  ioDataDebug_t    dbgStats;
  uint_least16_t   autoDetState;
  uint_least16_t   *IECframeLengths;
  uint_least16_t   frameLengthPCM;
  uint_least16_t   frameLengthDef;
  //uint_least32_t   frameLength;        // frame length: number of elements in a frame
  uint_least32_t   numElementsRead;    // number of elements to read from input buffer
  uint_least32_t   distToFrameEnd;
  int8_t           elementSize;
  ioBuffHandle_t   ioBuffHandle;
  uint_least32_t   zeroRunTrigger;
  uint_least32_t   zeroRunRestart;
  int_least32_t    timeOutCntr;
  uint_least32_t   unknownSourceTimeOut;

  uint_least16_t   *frameDataStart;
  size_t            frameDataSize;
  uint8_t   deliverZeros;
  uint8_t   ioBuffUnderFlow;
  uint8_t   syncTimeOut;
  void *buff1;
  void *buff2;
  size_t size1;
  size_t size2;

} ioDataInst_t;


// TODO: put this data structure somewhere, or use ioDataNumAlloc & ioDataAlloc to
// dynamically allocate memory.
//ioDataInst_t ioDataInst;

void autoDetection(ioDataInst_t * pIoDataInst);
void autoDetReset(ioDataInst_t * pIoDataInst);



/*******************************************************************************
* API function: ioDataNumAlloc
*    Returns the maximum number of memory allocation requests that ioDataAlloc()
*    requires.
*******************************************************************************/
int ioDataNumAlloc(void)
{
  return (IODATA_NUM_MEM_ALLOCS);
} /* ioDataNumAlloc */

/*******************************************************************************
* API function: ioDataAlloc
*    Returns a table of memory records that describe the size, alignment, type
*    and memory space of all buffers required by I/O DATA component.
*******************************************************************************/
int ioDataAlloc(lib_mem_rec_t *mem_tbl)
{
  mem_tbl[0].size = sizeof(ioDataInst_t);
  mem_tbl[0].alignment = IODATA_INST_ALIGN;
  mem_tbl[0].type = LIB_PMEM_SLOW;
  mem_tbl[0].base = NULL;

  return (IODATA_NO_ERR);
} /* ioDataAlloc */

/*******************************************************************************
* API function: ioDataCreate
*    Create an I/O DATA instance.
*******************************************************************************/
int ioDataCreate(ioDataHandle_t *handle, const lib_mem_rec_t *mem_tbl)
{
  ioDataInst_t *inst;

  if(  (mem_tbl[0].size < sizeof(ioDataInst_t))
     ||libChkAlign(mem_tbl[0].base, IODATA_INST_ALIGN) ) {
    // to add: check memory type
    return (IODATA_ERR_MEMORY);
  }

  inst = (ioDataInst_t *)mem_tbl[0].base;

  *handle = (ioDataHandle_t)inst;

  return (IODATA_NO_ERR);
} /* ioDataCreate */

/*=============================================================================
 * API function: this function initializes I/O data processing.
 =============================================================================*/
int ioDataInit(ioDataHandle_t handle, ioDataParam_t *cfgParams)
{
  ioDataInst_t *pIoDataInst = (ioDataInst_t *)handle;

  if(cfgParams == NULL) {
    return (IODATA_ERR_BAD_PARAMS);
  }

  pIoDataInst->ioBuffHandle    = cfgParams->ioBuffHandle;
  pIoDataInst->IECframeLengths = cfgParams->frameLengthsIEC;
  pIoDataInst->frameLengthPCM  = cfgParams->frameLengthPCM;
  pIoDataInst->frameLengthDef  = cfgParams->frameLengthDef;
  pIoDataInst->autoDet.ibMode  = cfgParams->ibMode;
  pIoDataInst->zeroRunRestart  = cfgParams->zeroRunRestart;
  pIoDataInst->zeroRunTrigger  = cfgParams->zeroRunTrigger;
  pIoDataInst->unknownSourceTimeOut = cfgParams->unknownSourceTimeOut*2;
  pIoDataInst->autoDetState         = AUTODET_SYNC_SEARCH_INITIAL;
  pIoDataInst->dbgStats.numSyncLost = 0;
  pIoDataInst->ioBuffUnderFlow      = FALSE;
  pIoDataInst->syncTimeOut          = FALSE;

  autoDetReset(pIoDataInst);
  
  return IODATA_NO_ERR;
} /* ioDataInit */


/*=============================================================================
 * API function: main function of I/O DATA component processing.
 =============================================================================*/
int ioDataProcess(ioDataHandle_t ioDataHandle)
{
  void *buff1, *buff2;
  size_t size1, size2;
  int status;
  ioDataInst_t *pIoDataInst = (ioDataInst_t *)ioDataHandle;

  // Get read pointers (or sub-buffers) of the input buffer
  status = ioBuffGetReadPtrs(pIoDataInst->ioBuffHandle,
                             pIoDataInst->numElementsRead*pIoDataInst->elementSize,
                             &buff1, &size1, &buff2, &size2);
  if(status == IOBUFF_ERR_UNDERFLOW) {
	  pIoDataInst->ioBuffUnderFlow = TRUE;
	  pIoDataInst->dbgStats.numBuffUnderflow += 1;

      /* skip processing since there is no enough data to process */
      return IODATA_ERR_IOBUF_UNDERFLOW;
  }

  // Store read pointers and sizes to instance for auto-detection to read
  pIoDataInst->ioBuffUnderFlow = FALSE;
  pIoDataInst->buff1 = buff1;
  pIoDataInst->buff2 = buff2;
  pIoDataInst->size1 = size1;
  pIoDataInst->size2 = size2;

  autoDetection(pIoDataInst);

  return IODATA_NO_ERR;
} /* ioDataProcess */


/*=============================================================================
 * API function - this function marks the sub-buffers provided by I/O BUFF as
 * read complete. It makes API call to the associated I/O BUFF component to mark
 * the data as reading completed.
 =============================================================================*/
int ioDataReadComplete(ioDataHandle_t ioDataHandle)
{
  ioDataInst_t *pIoDataInst = (ioDataInst_t *)ioDataHandle;

  // If the associated I/O buffer underflows, i.e. no enough data to read, do not
  // call I/O BUFF API to mark read complete.
  if(pIoDataInst->ioBuffUnderFlow) {
	  return IODATA_ERR_IOBUF_UNDERFLOW;
  }

  ioBuffReadComplete(pIoDataInst->ioBuffHandle, pIoDataInst->buff1, pIoDataInst->size1);

  if(pIoDataInst->buff2 != NULL) {
    ioBuffReadComplete(pIoDataInst->ioBuffHandle, pIoDataInst->buff2, pIoDataInst->size2);
  }

  return IODATA_NO_ERR;
} /* ioDataReadComplete */


/*=============================================================================
 * API function - this function allows framework to dynamically control I/O data
 *                component or get information from it.
 =============================================================================*/
int ioDataControl(ioDataHandle_t ioDataHandle, ioDataCtl_t *ioDataCtl)
{
  ioBuffInfo_t ioBuffInfo;

  ioDataInst_t *pIoDataInst = (ioDataInst_t *)ioDataHandle;

  switch (ioDataCtl->code)
  {

//    case IODATA_CTL_SET_ELEMENT_SIZE:
//    	pIoDataInst->elementSize = ioDataCtl->param.frameInfo.elementSize;
//      break;

    case IODATA_CTL_SET_PCM_FRAME_LENGTH:
        pIoDataInst->numElementsRead = pIoDataInst->frameLengthPCM = ioDataCtl->param.frameLengthPcm;
      break;

    case IODATA_CTL_GET_INPBUFFINFO:
    	ioBuffGetInfo(pIoDataInst->ioBuffHandle, &ioBuffInfo);
    	ioDataCtl->param.dataReadInfo.buffBase     = ioBuffInfo.base;
    	ioDataCtl->param.dataReadInfo.buffSize     = ioBuffInfo.size;
    	ioDataCtl->param.dataReadInfo.startAddress = pIoDataInst->frameDataStart;
    	ioDataCtl->param.dataReadInfo.frameSize    = pIoDataInst->frameDataSize;
    	break;

    case IODATA_CTL_GET_AUTODET_STATUS:
      ioDataCtl->param.autoDetStats.syncState     = pIoDataInst->autoDet.syncState;
      ioDataCtl->param.autoDetStats.deliverZeros  = pIoDataInst->deliverZeros;
      ioDataCtl->param.autoDetStats.streamInfo.IEC_PC = pIoDataInst->autoDet.bitStreamInfo & IODATA_SYNC_PC_MASK;
      ioDataCtl->param.autoDetStats.streamInfo.frameLength = pIoDataInst->autoDet.frameLength;
      ioDataCtl->param.autoDetStats.streamInfo.dataLength  =  pIoDataInst->autoDet.frameLength
                                                            - pIoDataInst->autoDet.headerLength;
      ioDataCtl->param.autoDetStats.syncTimeOut   = pIoDataInst->syncTimeOut;
      break;
/*
    case IODATA_CTL_GET_STEAM_INFO:
      break;
*/
    default:
      break;
  }

  return IODATA_NO_ERR;
} /* ioDataControl */

//========== internal functions - to be moved to a different file ==========
void autoDetSyncScan(ioDataInst_t * pIoDataInst);
void autoDetSyncCheck(ioDataInst_t * pIoDataInst);
void syncScan(void *buff, size_t size, int8_t elementSize, autoDet_t *pDet);
void syncCheck(void *buff, size_t size, int8_t elementSize, autoDet_t *pDet);
void syncCheckInit(ioDataInst_t * pIoDataInst);
void syncScanPcmInit(ioDataInst_t * pIoDataInst);

/*=============================================================================
 * Main function of auto-detection of the I/O DATA component.
 =============================================================================*/
void autoDetection(ioDataInst_t * pIoDataInst)
{
  switch(pIoDataInst->autoDetState)
  {    
    case AUTODET_SYNC_SEARCH_INITIAL:
      /* Search for SYNC the first time */
      autoDetSyncScan(pIoDataInst);

      pIoDataInst->syncTimeOut = FALSE;

      if(pIoDataInst->autoDet.syncDetected) {
        syncCheckInit(pIoDataInst);  // prepare for SYNC checking in subsequent frames

        pIoDataInst->autoDet.frameDataStartConfirm = pIoDataInst->autoDet.frameDataStartTemp;
        pIoDataInst->autoDet.syncState = IODATA_SYNC_ONE;      // found SYNC once and to be verified
        pIoDataInst->autoDetState = AUTODET_SYNC_CHECK_ONGOING;
      }      
      /* Check time out counter if SYNC is not found:
           - if autoDet.timeOutStarted == FALSE (no non-zero data has been received), look at pIoDataInst->timeOutCntr
           - if autoDet.timeOutStarted == TRUE,  look at pIoDataInst->autoDet.timeOutCntr
      */
      else if(   ((!pIoDataInst->autoDet.timeOutStarted) && (pIoDataInst->timeOutCntr <= 0))
    		  || (pIoDataInst->autoDet.timeOutCntr <= 0) ) {
        /* Time out, decide whether to reset or go to PCM */
   	    if (  (pIoDataInst->autoDet.ibMode == IBMODE_DEFAULT) 
            &&(pIoDataInst->autoDet.zeroCount >= pIoDataInst->zeroRunRestart) ) {
   	      /* Reset if zero-run is long enough */
          autoDetReset(pIoDataInst);
   	      pIoDataInst->autoDetState = AUTODET_SYNC_SEARCH_INITIAL;
   	      pIoDataInst->syncTimeOut  = TRUE;
   	    }
   	    else {
   	      // inform decoder where to read PCM data
  	      pIoDataInst->frameDataStart = pIoDataInst->buff1;
  	      pIoDataInst->frameDataSize  = pIoDataInst->size1;
  	      if(pIoDataInst->buff2 != NULL) {
  	        pIoDataInst->frameDataSize  += pIoDataInst->size2;
  	      }

 	      // Go to PCM scanning - to play out PCM and keep scanning for SYNC 
  	      syncScanPcmInit(pIoDataInst);
          pIoDataInst->autoDet.syncState = IODATA_SYNC_PCM;
   	      pIoDataInst->autoDetState = AUTODET_SYNC_SEARCH_PCM;
   	    }
      }      
      break;
    
    case AUTODET_SYNC_SEARCH_PCM:
      /* Search for SYNC while PCM is played out */
      autoDetSyncScan(pIoDataInst);
      
      /* Check if SYNC is detected or there is zero-run */
      if(pIoDataInst->autoDet.syncDetected) {
        autoDetReset(pIoDataInst);
        pIoDataInst->autoDetState = AUTODET_SYNC_SEARCH_INITIAL;
      }
      else {
        // inform decoder where to read PCM data 
        pIoDataInst->frameDataStart = pIoDataInst->buff1;    
        pIoDataInst->frameDataSize  = pIoDataInst->frameLengthPCM*pIoDataInst->elementSize;

        pIoDataInst->deliverZeros = FALSE;
        
        if(pIoDataInst->autoDet.ibMode == IBMODE_DEFAULT) {
          if(pIoDataInst->autoDet.zeroCount >= pIoDataInst->zeroRunTrigger) {
            pIoDataInst->deliverZeros = TRUE;
          }

          if(pIoDataInst->autoDet.zeroCount >= pIoDataInst->zeroRunRestart) {
   	        /* Reset if zero-run is long enough */
            autoDetReset(pIoDataInst);
   	        pIoDataInst->autoDetState = AUTODET_SYNC_SEARCH_INITIAL;              
          }
        }
        else if(pIoDataInst->autoDet.ibMode == IBMODE_NO_ZERORUNRESTART) {
          if(pIoDataInst->autoDet.zeroCount >= pIoDataInst->zeroRunTrigger) {
 	        pIoDataInst->deliverZeros = TRUE;              
          }
        }
        else {
          // ibMode = IBMODE_NO_ZERORUN. Stay in this state and do nothing
        }          
      }
      
      break;

    case AUTODET_SYNC_CHECK_ONGOING:
      autoDetSyncCheck(pIoDataInst);

   	  /* Check if a full frame has been received and if so, check if SYNC is maintained */
      if(pIoDataInst->autoDet.completeFrameRcvd) {
  		if(pIoDataInst->autoDet.syncDetected) {
          pIoDataInst->dbgStats.numFramesRcvd += 1;
  		  pIoDataInst->autoDet.syncState = IODATA_SYNC_BITSTREAM;
    	  pIoDataInst->autoDet.completeFrameRcvd = FALSE;

    	  // Inform decoder to read data from the beginning of current frame (after the preamble)
    	  // Note: this address could be 1-word pass the end of input buffer and therefore needs
    	  //       to be wrapped around by ioBuffWrapPointer().
    	  pIoDataInst->frameDataStart = ioBuffWrapPointer(pIoDataInst->ioBuffHandle,
    			                                          pIoDataInst->autoDet.frameDataStartConfirm);
          pIoDataInst->frameDataSize  = pIoDataInst->autoDet.frameLength*pIoDataInst->elementSize;

    	  // Store the beginning address of data in current frame (not including the preamble)
      	  pIoDataInst->autoDet.frameDataStartConfirm = pIoDataInst->autoDet.frameDataStartTemp;

      	  // re-initialize for SYNC check
       	  syncCheckInit(pIoDataInst);
  		}
  		else {
       	  pIoDataInst->dbgStats.numSyncLost += 1;            // what else needs to be done???
   	      autoDetReset(pIoDataInst);
   	      pIoDataInst->autoDetState = AUTODET_SYNC_SEARCH_INITIAL;
  		  pIoDataInst->autoDet.syncState = IODATA_SYNC_NONE;
  		}
  	  }
      else {
 		; //pIoDataInst->autoDet.syncState = IODATA_SYNC_CHECK;
      }
    break;
    
    default: break;
  }

} /* autoDetection */

/*=============================================================================
 * This function resets auto-detection.
 =============================================================================*/
void autoDetReset(ioDataInst_t * pIoDataInst)
{
	pIoDataInst->deliverZeros           = FALSE;
	pIoDataInst->autoDet.zeroCount      = 0;
	pIoDataInst->autoDet.syncState      = IODATA_SYNC_NONE;
	pIoDataInst->autoDet.scanState      = SCANNED_NONE;
	pIoDataInst->autoDet.timeOutStarted = FALSE;
	pIoDataInst->autoDet.syncDetected   = FALSE;
	pIoDataInst->autoDet.timeOutCntr    = pIoDataInst->unknownSourceTimeOut;
	//pIoDataInst->autoDet.timeOutCntr    = pIoDataInst->unknownSourceTimeOut*20;
	pIoDataInst->numElementsRead        = pIoDataInst->frameLengthDef;
	pIoDataInst->elementSize            = IODATA_ELEMENT_SIZE_DEF;

	pIoDataInst->timeOutCntr            = pIoDataInst->unknownSourceTimeOut;
	//pIoDataInst->timeOutCntr            = pIoDataInst->unknownSourceTimeOut*20;
	pIoDataInst->dbgStats.numFramesRcvd = 0;
	pIoDataInst->dbgStats.numBuffUnderflow = 0;
} /* autoDetReset */

/*=============================================================================
 * This function re-initializes auto-detection for scanning SYNC in PCM data
 =============================================================================*/
void syncScanPcmInit(ioDataInst_t * pIoDataInst)
{
	//pIoDataInst->deliverZeros           = FALSE;
	//pIoDataInst->autoDet.zeroCount      = 0;
	//pIoDataInst->autoDet.scanState      = SCANNED_NONE;
	//pIoDataInst->autoDet.timeOutStarted = FALSE;
	//pIoDataInst->autoDet.syncDetected   = FALSE;
	//pIoDataInst->autoDet.timeOutCntr    = pIoDataInst->unknownSourceTimeOut*2;
	//pIoDataInst->autoDet.timeOutCntr    = pIoDataInst->unknownSourceTimeOut*20;
	pIoDataInst->numElementsRead        = pIoDataInst->frameLengthPCM;
	//pIoDataInst->timeOutCntr            = pIoDataInst->unknownSourceTimeOut*2;
	//pIoDataInst->timeOutCntr            = pIoDataInst->unknownSourceTimeOut*20;
	pIoDataInst->elementSize            = IODATA_NUM_BYTES_PCMSAMPLE;
} /* syncScanPcmInit */

/*=============================================================================
 * Initialization for checking sync in subsequent bitstream frames.
 =============================================================================*/
void syncCheckInit(ioDataInst_t * pIoDataInst)
{
	int i;
	uint_least16_t pc = pIoDataInst->autoDet.bitStreamInfo & IODATA_SYNC_PC_MASK;

	pIoDataInst->autoDet.frameLength    = pIoDataInst->IECframeLengths[pc];

    if (pc == 1) {
        pIoDataInst->autoDet.frameLength = 4288;
    }
    else if (pc == IODATA_SYNC_SUBTYPE_DTSHD) {
        int DTSHDSubType = (pIoDataInst->autoDet.bitStreamInfo & IODATA_SYNC_SUBTYPE_MASK)
                           >> IODATA_SYNC_SUBTYPE_SHIFT;
        pIoDataInst->autoDet.frameLength = pIoDataInst->IECframeLengths[pc] << DTSHDSubType;
    }

	pIoDataInst->autoDet.distToFrameEnd = pIoDataInst->autoDet.frameLength
			                              - pIoDataInst->autoDet.numElementsRcvd;
	
    if(pIoDataInst->autoDet.distToFrameEnd < 0)
	{
        if((pIoDataInst->buff1 != NULL) && (pIoDataInst->size1 != 0))
        {
            if((pIoDataInst->buff2 != NULL) && (pIoDataInst->size2 != 0))
            {
                pIoDataInst->autoDet.distToFrameEnd += ((pIoDataInst->size1 + pIoDataInst->size2)/pIoDataInst->elementSize);
            }
            else
            {
                pIoDataInst->autoDet.distToFrameEnd += (pIoDataInst->size1/pIoDataInst->elementSize);
            }
        }
    }
//#ifdef ADJUST_XFER_SIZE_AFTER_AUTODET
#if 1
	// Adjust the read size such that next read will contain the preamble of next
	// frame at the end of the read. For first frame after SYNC is detected, the
	// read size will be smaller than frame size. For subsequent frames, the read
	// size will be equal to the frame size.
    pIoDataInst->numElementsRead = pIoDataInst->autoDet.distToFrameEnd
    		                       + pIoDataInst->autoDet.headerLength;
#endif

	pIoDataInst->autoDet.preambleStartsPrevBuf = FALSE;
	pIoDataInst->autoDet.completeFrameRcvd = FALSE;
	pIoDataInst->autoDet.syncDetected = FALSE;
	pIoDataInst->autoDet.preambleInd = 0;

	for(i=0; i<pIoDataInst->autoDet.headerLength; i++)
	{
		pIoDataInst->autoDet.preambleBuf[i] = 0x0;
	}
} /* syncCheckInit */


/*=============================================================================
 * This function scans for sync word (preamble) in the input buffer while
 * checking for time out. It calls subroutine syncScan to scan the data.
 *
 * The read pointer(s) to the input buffer and the read sizes are provided by
 * the I/O BUFF component and stored in auto-detection instance. The read
 * pointers are also refered as sub-buffers.
 *
 * NOte: Since the input buffer may wrapp around, there may be two sub-buffers,
 *       one at the end of the input buffer and the other at the beginning,
 *       provided to auto-detection. In this case, syncScan subroutine will be
 *       called twice.
 =============================================================================*/
void autoDetSyncScan(ioDataInst_t * pIoDataInst)
{
	/* scan first sub-buffer for sync */
	if((pIoDataInst->buff1 != NULL) && (pIoDataInst->size1 != 0)) {
		syncScan(pIoDataInst->buff1, pIoDataInst->size1, pIoDataInst->elementSize,
				&pIoDataInst->autoDet);
		pIoDataInst->timeOutCntr -= pIoDataInst->size1/pIoDataInst->elementSize;
	}

	/* check if sync word is found in the first sub-buffer */
	if(pIoDataInst->autoDet.syncDetected) {
		/* skip scanning second sub-buffer if sync is detected */
		if((pIoDataInst->buff2 != NULL) && (pIoDataInst->size2 != 0)) {
			/* update the number of received elements in current frame */
		    pIoDataInst->autoDet.numElementsRcvd += pIoDataInst->size2;
		}
	}
	else if((pIoDataInst->buff2 != NULL) && (pIoDataInst->size2 != 0)) {
		/* scan second sub-buffer if there are two sub-buffers */
		syncScan(pIoDataInst->buff2, pIoDataInst->size2, pIoDataInst->elementSize,
				&pIoDataInst->autoDet);
		pIoDataInst->timeOutCntr -= pIoDataInst->size2/pIoDataInst->elementSize;
	}
} /* ioDataAutoDetSyncScan */

/*=============================================================================
 * This function scans for sync word (preamble) in the provided sub-buffer.
 =============================================================================*/
void syncScan(void *buff, size_t size, int8_t elementSize, autoDet_t *pDet)
{
	uint_least16_t  *pbuf;
    int i, stride, scanCount, zeroCount;
    uint_least16_t scanState;
    uint_least16_t *syncPtr;

	pbuf = (uint_least16_t *)buff;

	/* check element size */
    if (elementSize == IODATA_NUM_BYTES_PCMSAMPLE) {
    	/* skip every other 16-bit number and only look at MSB of PCM samples */
        stride = IODATA_NUM_BYTES_PCMSAMPLE / sizeof(uint_least16_t);
        pbuf += 1;
    }
    else {
        stride = 1;
    }

    /* get ready for scanning */
    scanCount = 0;
    zeroCount = pDet->zeroCount;
    scanState = pDet->scanState;

    /* check if part of the sync words (preamble) are found in previous call */
    if(scanState != SCANNED_NONE) {
    	/* count this sub-buffer in the number of received elements in current frame */
    	pDet->numElementsRcvd += size/sizeof(uint_least16_t);
    }

    /* scan until out of available data or sync is found */
    for (i=0; i < size/elementSize; i+=stride) {
    	uint_least16_t tail = pbuf[i];

        if (scanState == SCANNED_NONE) {
            if (tail == IEC_PA) {
                /* make sure there are some zeros before PA. */
                if (zeroCount >= IODATA_MIN_NUM_ZEROS_BEFORE_PA) {
                    scanState = SCANNED_IEC_PA;
                    syncPtr = &pbuf[i];  /* mark the position of PA */
                }
            }

            if(scanState != SCANNED_NONE) {
            	/* Found first sync word. Save its address */
            	pDet->bitStreamSyncPtr = syncPtr;

            	/* number of elements in current frame that have been received
            	 * in this sub-buffer, including the preamble (sync words). */
                pDet->numElementsRcvd = size/sizeof(uint_least16_t) - i;
                pDet->preambleRef[0]  = tail; /* save this word for sync check */
            }

            /* update zero count and prevent wrap around */
            zeroCount = min (zeroCount+1,INT_MAX - 1);
            if (tail != 0x0000) {
                zeroCount = 0;
            }

            // don't start counting until we get the first non-zero
            // sample while UNKNOWN. Note we don't have to worry
            // about the other scanCount increments since these
            // only occur after the first non-zero sample.
            //
            // so don't count unless one of the following is true:
            //    . the last sample was non-zero
            //    . we are already started counting (in this call)
            //    . we started counting in an earlier scanForSync (timeout has changed)
            //if (scanCount || (tail != 0x0000) || pDet->timeOutStarted) {
            //    scanCount += 1;
            //}

            if (tail != 0x0000) {
                scanCount += 1;
                pDet->timeOutStarted = TRUE;
            }
            else if (scanCount || pDet->timeOutStarted) {
                scanCount += 1;
            }
            else {
            	// don't increment scanCount
            }

            continue;
        }

        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

        switch (scanState) {

            case SCANNED_IEC_PA:
                if (tail == IEC_PB) {
                    scanState = SCANNED_IEC_PB;
                    scanCount += 1;
                    pDet->preambleRef[1]  = tail; /* save this word for sync check */
                }
                else {
                    scanState = SCANNED_NONE;
                }
                break;

            case SCANNED_IEC_PB:
                // Update scanCount here since, at this point, we are confident that
                // this is a proper IEC stream. Regardless if we ignore it our not.
                // Therefore we want to properly signal that this data has been scanned.
                scanCount += 1;

                // check for IEC pause packets at this time and if required ignore them.
                // By construction we are guaranteed to have tail=PC at this time.
                if ((pDet->ibMode == IBMODE_NO_ZERORUNRESTART) ||
                    (pDet->ibMode == IBMODE_NO_ZERORUN)) {
                    if ( ((tail&0x1F) == 0) || ((tail&0x1F) == 3)) {
                        scanState = SCANNED_NONE;
                        break;
                    }
                }

                // fall through to SCANNED_IEC_PC:
                pDet->preambleRef[2] = tail;  /* save this word for sync check */
            	pDet->bitStreamInfo  = tail;
                scanState = SCANNED_IEC_PC;
                break;

            case SCANNED_IEC_PC:
                //pDevExt->headerSize = IEC_HEADER_SIZE;
                pDet->preambleRef[3]  = tail; /* save this word for sync check */
            	pDet->headerLength    = IODATA_HEADER_LENGTH_IEC;
                if( (pDet->bitStreamInfo & 0x1F) == 0x11) { // DTSHD subtype
                    pDet->headerLength += 6;
                }
            	pDet->headerCheckLen  = IODATA_HEADER_CHECK_LENGTH_IEC;
                scanState = SCANNED_IEC_PD;

                break;

            case SCANNED_IEC_PD:
                // wait untill full header is received
            	pDet->syncDetected = TRUE;
            	pDet->frameDataStartTemp = &pbuf[i];

                break;

        } /* switch */

        if(pDet->syncDetected) {
        	break; // break from for loop
        }
    } /* for */

    pDet->zeroCount = zeroCount;
    pDet->scanState = scanState;
    if (pDet->timeOutCntr > scanCount) {
    	pDet->timeOutCntr -= scanCount;
    }
    else {
    	pDet->timeOutCntr = 0;
    }

} /* syncScan */


/*=============================================================================
 * This function checks for sync word (preamble) at the expected location in the
 * input buffer. Similar to ioDataAutoDetSyncScan, it looks into two sub-buffers
 * when the input buffer wraps around.
 *
 =============================================================================*/
void autoDetSyncCheck(ioDataInst_t * pIoDataInst)
{
	// look into first sub-buffer for preamble
	if((pIoDataInst->buff1 != NULL) && (pIoDataInst->size1 != 0)) {
		syncCheck(pIoDataInst->buff1, pIoDataInst->size1, pIoDataInst->elementSize,
				  &pIoDataInst->autoDet);
	}

	// check if first sub-buffer contains the expected preamble
	if(pIoDataInst->autoDet.syncDetected) {
		// skip second sub-buffer if first sub-buffer contains the expected preamble */
		if((pIoDataInst->buff2 != NULL) && (pIoDataInst->size2 != 0)) {
			// update the number of received elements in current frame
		    pIoDataInst->autoDet.numElementsRcvd += pIoDataInst->size2;
		}
	}
	else if((pIoDataInst->buff2 != NULL) && (pIoDataInst->size2 != 0)) {
		// look into second sub-buffer if there are two sub-buffers
		syncCheck(pIoDataInst->buff2, pIoDataInst->size2, pIoDataInst->elementSize,
				  &pIoDataInst->autoDet);
	}
} /* autoDetSyncCheck */


/*=============================================================================
 * This function scans checks for sync word (preamble) in the provided sub-buffer.
 =============================================================================*/
void syncCheck(void *buff, size_t size, int8_t elementSize, autoDet_t *pDet)
{
	uint_least16_t  *pbuf;
    int i, diff, restOfPreamble;
	uint_least32_t  numElements;

	pbuf = (uint_least16_t  *)buff;
	numElements = size / elementSize; // number of elements in provided sub-buffer

	// Check if the preamble already starts from the sub-buffer in previous call
	if(pDet->preambleStartsPrevBuf) {
		// Calculate number of preamble words in this sub-buffer
		restOfPreamble = pDet->headerLength - pDet->preambleInd;

		// Save the preamble words for sync check
		for(i=0; i<restOfPreamble; i++)
		{
			pDet->preambleBuf[pDet->preambleInd++] = pbuf[i];
		}

		// Starts counting elements for next sync check
    	pDet->frameDataStartTemp  = &pbuf[i]; // starting address of data in next frame
		pDet->numElementsRcvd += numElements; // number of received elements
		pDet->preambleStartsPrevBuf = FALSE;  // reset flag
	}
	else {
		// Preamble hasn't started from the sub-buffer in previous call.
		// Check if this frame ends in this sub-buffer:
		//   distToFrameEnd: distance from beginning of this sub-buffer to frame end
		//   numElements:    number of elements in this sub-buffer
		if(pDet->distToFrameEnd < numElements) {
			// Preamble of next frame is in this sub-buffer, either full or partial.
			// First distToFrameEnd elements in the sub-buffer are the end of current
			// frame. Preamble of next frame starts from pbuf[pDet->distToFrameEnd].
			if(pDet->distToFrameEnd <= (numElements-pDet->headerLength)) {
				// All the preamble words are in this sub-buffer
				for(i=pDet->distToFrameEnd; i<pDet->distToFrameEnd+pDet->headerLength; i++)
				{
					// Save preamble words for sync check
					pDet->preambleBuf[pDet->preambleInd++] = pbuf[i];
				}

				// Store the starting address of data in next frame.
				// Note: &pbuf[i] could be 1 word pass the input buffer end if
				//       the last word of preamble happens to be at the end of
				//       the input buffer. In this case, this address needs to
				//       be wrapped around to the beginning of input buffer.
	        	pDet->frameDataStartTemp  = &pbuf[i];
			}
			else {
				// Only partial preamble is in this sub-buffer.
				// The preamble spans across this sub-buffer and next.
				for(i=pDet->distToFrameEnd; i<numElements; i++)
				{
					pDet->preambleBuf[pDet->preambleInd++] = pbuf[i];
				}
				pDet->preambleStartsPrevBuf = TRUE;
			}

			// Starts counting elements for next sync check
			pDet->numElementsRcvd = numElements - pDet->distToFrameEnd;
		}
		else {
			// Current frame doesn't end here.
			// Preamble of next frame is not in this sub-buffer
			pDet->distToFrameEnd -= numElements;
		}
	}

	// Check if full preamble of next frame has been received
	if(pDet->preambleInd == pDet->headerLength) {
		pDet->completeFrameRcvd = TRUE;

		// Check if the preamble matches the reference.
		diff = 0;
		for(i=0; i<pDet->headerCheckLen; i++)
		{
			diff += pDet->preambleBuf[i] - pDet->preambleRef[i];
		}

		// SYNC is maintained if preamble matches reference
		if(diff == 0) {
			pDet->syncDetected = TRUE;
		}
		else {
			pDet->syncDetected = FALSE;
		}

		// Reset index for next preamble check
		//pDet->preambleInd = 0;
	}
} /* syncCheck */


/* nothing past this point */
