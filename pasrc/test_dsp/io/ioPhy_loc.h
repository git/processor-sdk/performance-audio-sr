
#ifndef IOPHY_LOC_H
#define IOPHY_LOC_H

#include <stdint.h>
#include "ioBuff.h"
#include <ti/drv/mcasp/mcasp_drv.h>

#define IOPHY_NUM_XFER_RECORD 8

#define IOPHY_XFER_PACKET_INTER 0
#define IOPHY_XFER_PACKET_FINAL 1


typedef struct ioPhyXferRec_s {
  void *base;
  uint_least32_t size;
  uint_fast16_t  packet_status;
} ioPhyXferRec_t;

typedef struct ioPhyInst_s {
  ioPhyXferRec_t xfer_rec[IOPHY_NUM_XFER_RECORD];
  MCASP_Packet   xfered_packets[IOPHY_NUM_XFER_RECORD];
  ioBuffHandle_t io_buff_handle;
  uint_least32_t xfer_frame_size;
  Mcasp_IOcmd_e mcasp_cmd;

  int_fast16_t  sync_cntr;
  uint_fast16_t submit_ind;
  uint_fast16_t complete_ind;

  void *mcasp_chan_handle; 

  ioPhyBuffOp_t  ioBuffOp;       // READ or WRITE
  int (*ioBuffMarkComplete)(ioBuffHandle_t handle, void *start, size_t size);
  int (*ioBuffGetPtrs)(ioBuffHandle_t handle, size_t mem_size, 
                                void **rdptr1, size_t *size1, 
                                void **rdptr2, size_t *size2);

} ioPhyInst_t;


#endif
