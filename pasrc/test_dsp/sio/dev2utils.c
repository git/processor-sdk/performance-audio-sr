
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include "fwkPort.h"

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Memory.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Semaphore.h>

#include <math.h>

#include "sio.h"

//#include "inpbuf.h"
//#include "outbuf.h"
//#include "pafsio.h"
//#include "psdkaf_typ.h"
//#include "psdkaf_dec.h"

//Add because of table
#include <sap.h>
#include <mib.h>
#include <mob.h>

//Define Module table
#if 1
DEV2_TableElem DEV2_table[] = {
    {
        {NULL, NULL},
        {"DIB", &DIB_FXNS, (Int)DIB_init, NULL, 0, 0}
    },
    {
        {NULL, NULL},
        {"DOB", &DOB_FXNS, (Int)DOB_init, NULL, 0, 0}
    },
    {
        {NULL, NULL},
        {"DAP", &SAP_FXNS, (Int)SAP_init, NULL, 0, 0}
    },
    /***
    {
        {NULL, NULL},
        {"SAP", &SAP_FXNS, (Int)SAP_init, NULL, 0, 0}
    },
    ***/
    {
        {NULL, NULL},
        {NULL, NULL, 0, NULL, 0, 0} /* End the table wit a NULL entry */
    },
};
#else
DEV2_TableElem DEV2_table[4];
void setupDev2Table(void)
{
    DEV2_createDevice("DIB", &DIB_FXNS, (Int)DIB_init, NULL);
    DEV2_createDevice("DOB", &DOB_FXNS, (Int)DOB_init, NULL);
    DEV2_createDevice("DAP", &SAP_FXNS, (Int)SAP_init, NULL);
}
#endif

void initDev2(void)
{
  SAP_init();
  DIB_init();
  DOB_init();
}


// Define available heap regions
extern const ti_sysbios_heaps_HeapMem_Handle heapMemL2Sram;
extern const ti_sysbios_heaps_HeapMem_Handle heapMemDdr3;
//extern const ti_sysbios_heaps_HeapMem_Handle heapMemMsmc;

#define DEV2_L2HEAP0 ((IHeap_Handle)heapMemL2Sram)
#define DEV2_L2HEAP1 ((IHeap_Handle)heapMemL2Sram)
#define DEV2_L2HEAP2 ((IHeap_Handle)heapMemL2Sram)
#define DEV2_EHEAP ((IHeap_Handle)heapMemDdr3)
//#define DEV2_MHEAP ((IHeap_Handle)heapMemMsmc)

HeapMem_Handle DEV2_memSpaceToHeap (IALG_MemSpace space)
{
    switch(space)
    {
        case IALG_SARAM:    /* IALG_SARAM0 = IALG_SARAM :  Normally used for IRAM.  */
            Log_info2("DEV2_memSpaceToHeap: IALG_SARAM (0x%x) 0x%x", space, (IArg)DEV2_L2HEAP0);
            return DEV2_L2HEAP0;

        case IALG_EXTERNAL: // normally external SDRAM
            Log_info2("DEV2_memSpaceToHeap: IALG_EXTERNAL (0x%x) 0x%x", space, (IArg)DEV2_EHEAP);
            return DEV2_EHEAP;

        case IALG_SARAM1:
            Log_info2("DEV2_memSpaceToHeap: IALG_SARAM1 (0x%x) 0x%x", space, (IArg)DEV2_L2HEAP1);
            return DEV2_L2HEAP1;

        case IALG_SARAM2:
            Log_info2("DEV2_memSpaceToHeap: IALG_SARAM2 (0x%x) 0x%x", space, (IArg)DEV2_L2HEAP2);
            return DEV2_L2HEAP2;

        case IALG_DARAM0:
            Log_info2("DEV2_memSpaceToHeap: IALG_DARAM0 (0x%x) 0x%x", space, (IArg)DEV2_L2HEAP0);
            return DEV2_L2HEAP0;

        case IALG_DARAM1:   // not normally used.
            Log_info2("DEV2_memSpaceToHeap: IALG_DARAM1 (0x%x) 0x%x", space, (IArg)DEV2_L2HEAP1);
            return DEV2_L2HEAP1;

        case IALG_DARAM2:   // not normally used.
            Log_info2("DEV2_memSpaceToHeap: IALG_DARAM2 (0x%x) 0x%x", space, (IArg)DEV2_L2HEAP2);
            return DEV2_L2HEAP2;    // not normally used.

        case IALG_ESDATA:   // not normally used.
            Log_info2("DEV2_memSpaceToHeap: IALG_ESDATA (0x%x) 0x%x", space, (IArg)DEV2_EHEAP);
            return DEV2_EHEAP;

        default:
            Log_info2("DEV2_ALG_memSpaceToHeap: default (0x%x) 0x%x", space, (IArg)DEV2_EHEAP);
            return DEV2_EHEAP;
    }
}
