
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <string.h> // for memset
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Memory.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Semaphore.h>

#include "sio.h"

#include "inpbuf.h"
#include "outbuf.h"

#include "pafsio.h"

#include "paftyp.h"
#include "pafdec.h"

#include "paf_heapMgr.h"

#include "fwkPort.h"
#include "dbgDib.h"

// stub for SIO create()
SIO_Handle SIO_create(
    String name, 
    Int mode, 
    size_t size, 
    SIO_Attrs *attrs
)
{
    SIO_Handle  stream;

    Log_info2("SIO_create():mode=%d, size=%d", mode, size);
    SIO2_Attrs attrs2;
    attrs2.nbufs  = attrs->nbufs;
    attrs2.bufSeg = (IHeap_Handle)pafHeapMgr_readHeapHandle(attrs->segid);
    attrs2.align  = attrs->align;
    attrs2.flush  = attrs->flush;
    attrs2.model  = attrs->model;
    attrs2.timeout = attrs->timeout;
    attrs2.callback = attrs->callback;
    stream = SIO2_create(name, mode, size, &attrs2);

    return stream;
}

// stub for SIO_ctrl()
SIO_ctrl(
    SIO_Handle stream, 
    Uns cmd, 
    Arg arg
)
{
    return SIO2_ctrl(stream, cmd, arg);
}

// stub for _SIO_idle
Int _SIO_idle(
    SIO_Handle stream, 
    Bool flush
)
{
    return _SIO2_idle(stream, flush);
}

// stub for SIO_issue()
Int SIO_issue(
    SIO_Handle stream, 
    Ptr pbuf, 
    size_t nbytes, 
    Arg arg
)
{
    return SIO2_issue(stream, pbuf, nbytes, arg);
}

// stub for SIO_reclaim()
Int SIO_reclaim(
    SIO_Handle stream, 
    Ptr *ppbuf, 
    Arg *parg
)
{    
    DEV2_Handle  device;                 /* == stream */

    /* first field in SIO_Obj is a DEV_Obj */
    device = (DEV2_Handle)stream;
    Log_info4("SIO_reclaim:stream=0x%x, ppbuf=0x%x, parg=0x%x, mode=%d", (IArg)stream, (IArg)ppbuf, (IArg)parg, device->mode);

    return SIO2_reclaim(stream, ppbuf, parg);
}
