
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== main.c ========
 */

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <ti/ipc/Ipc.h>
#include <ti/sysbios/hal/Cache.h>
#include <ti/xdais/xdas.h>

#include <ti/board/board.h>
#include <ti/addon/aud/include/evmc66x_i2c.h>
#include <ti/addon/aud/include/aud.h>
#include <ti/addon/aud/include/aud_audio.h>
#include "audio_dc_cfg.h"
#include "edma_cfg.h"
#include <ti/drv/mcasp/soc/mcasp_soc.h>

#include "components/clk.h" /* PFP testing */
#include "pfp/pfp.h"
#include "pfp_app.h"        /* contains all PFP ID's */

#include "ioConfig.h"    //TODO: remove this header
extern Void initDev2(Void);
extern void evmI2CInit(uint8_t i2cportnumber); // missing in evmc66x_i2c.h

#ifdef IO_LOOPBACK_TEST
extern void McaspDevice_init();
extern void D10_init();
extern Aud_STATUS mcaspAudioConfig();
extern void audioIoCreate(void);
extern void ioSemaphoreCreate(void);
#endif

// avoid including sap_d10.h, which would cause symbol redefinition
// warning (MCASP_PFUNC_XXX)
extern XDAS_Int32 D10_init(void);

/*
 *  ======== main ========
 */
Int main()
{
    Int status;
    Aud_STATUS AudStatus;
    Int k;
    Board_initCfg cfg;

    Log_info0("Enter main()");
    
    /* Setup Profile Points (PFP) */
    Log_info0("enter PFP Setup");
    pfpCreate();
    pfpCalibrate(1000, 1);
#if 0 // enabled in System Stream
    for (k = 0; k <= PFP_ID_LAST; k++) 
    {
        pfpEnable(k);   /* Enable profile point #k */
    }
#endif
    for (k = 1; k <= PFP_ID_LAST; k++)
    {
        pfpSetAlpha(k, PFP_DEF_ALPHA);  /* Set default exp. avg. time const. */
    }
#if 0 // debug: dummy load
    pfpEnable(0);   /* Enable profile point #0 */
    for (k = 0; k < 1000; k++) 
    {
        pfpBegin(PFP_ID_MAIN,0);
        clkWorkDelay(CLK_WORKCNT_PER_MS);     /* This should take about 750,000 cycles to execute, or 750e3/600e6=1.25 msec. */
        pfpEnd(PFP_ID_MAIN,0);
    }    
    pfpDisable(0);   /* Disable profile point #0 */
#endif    

    /* initialize board */
    cfg = BOARD_INIT_PINMUX_CONFIG | BOARD_INIT_MODULE_CLOCK;
    Board_init(cfg);
    evmI2CInit(I2C_PORT_0);
    evmI2CInit(I2C_PORT_1);
    evmI2CInit(I2C_PORT_2);

    Log_info0("exit Board init");

    Log_info0("Configure EDMA");
    /* Configure eDMA module */
    AudStatus = eDmaConfig();
    if(AudStatus != Aud_EOK)
    {
        //System_printf("eDMA Configuration Failed!\n");
        Log_info0("eDMA Configuration Failed!");
        //testRet(1);
    }

#ifdef PASDK_SIO_DEV
    Log_info0("initDev2");
    initDev2();
#endif

#ifdef IO_LOOPBACK_TEST
    /* Initialize McASP HW details */
    McaspDevice_init();

    D10_init();

#ifdef INPUT_SPDIF
    // Input is DIR
    AudStatus = aud_AudioSelectClkSrc(AUD_AUDIO_CLK_SRC_DIR);
#else
    // Input is HDMI
    AudStatus = aud_AudioSelectClkSrc(AUD_AUDIO_CLK_SRC_I2S);
#endif
    if(AudStatus != Aud_EOK)
    {
        Log_info0("aud_AudioSelectClkSrc Failed!\n");
    }
    aud_delay(50000); // Without delay between these 2 calls system aborts.

    /* Initialize McASP module */
    status = mcaspAudioConfig();
    if(status != Aud_EOK)
    {
        Log_info0("McASP Configuration Failed!\n");
    }

    ioSemaphoreCreate();

    audioIoCreate();

    Task_setPri(TaskAfp, -1);
    Task_setPri(TaskAip, -1);
    Task_setPri(TaskSysInit, -1);
    Task_setPri(TaskAsip, 5);
    Task_setPri(TaskAsop, 5);
#endif

    Log_info0("McASP device init");
    /* Initialize McASP SOC level configuration */
    McaspDevice_init();

    Log_info0("D10 init");
    /* Initialize audio hardware */
    D10_init();

    Log_info0("IPC start");
    /* Initialize IPC */
    status = Ipc_start();
    if (status < 0) 
    {
        System_abort("Ipc_start failed\n");
    }
    
    BIOS_start();    /* does not return */
    return(0);
}
