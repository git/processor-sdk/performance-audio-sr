/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/* DSP linker command file for DTS:X */

/* -i PAF_LROOT\pa\build\c66x\release */
/* -i PAF_LROOT\pa\util\da10x_misc\c66x\release */

/* -i DSPLIB_LROOT\packages\ti\dsplib\lib */
/* -i MATHLIB_LROOT\packages\ti\mathlib\lib */

/*
#ifndef DTSXIP_PKGS
-i DTSXIP_LROOT\Source_Code\PARMA\dts-3d\misc\build\c66x\Release
-i DTSXIP_LROOT\Source_Code\PARMA\dts-base\misc\build\c66x\Release
-i DTSXIP_LROOT\Source_Code\PARMA\common-flib\misc\build\c66x\Release
-i DTSXIP_LROOT\Source_Code\PARMA\lbr\misc\build\c66x\lbr\Release
-i DTSXIP_LROOT\Source_Code\PARMA\lbr\misc\build\c66x\lbrdec\Release
-i DTSXIP_LROOT\Source_Code\PARMA\parma-dec\misc\build\c66x\Release
-i DTSXIP_LROOT\Source_Code\PARMA\dtshd-c-decoder\misc\build\c66x\lib\Release
-i DTSXIP_LROOT\Source_Code\PARMA\la-strm-reconstruction\misc\build\c66x\Release
-i DTSXIP_LROOT\Source_Code\PARMA\alg\misc\build\c66x\Release
#else
-i DTSXIP_LROOT\dtsx-ip\build\c66x\release
#endif
*/

-l"acp_elf.lib"
-l"asp_std_elf.lib"
-l"com_asp_elf.lib"
-l"com_dec_elf.lib"
-l"pafsio_elf.lib"
-l"pce2_elf.lib"
-l"pcm1_elf.lib"
-l"dcs7_elf.lib"
-l"statusop_common_elf.lib"
-l"misc_elf.lib"
/* -l"src4_elf.lib" */
-l"fil_elf.lib"

-l"ae0_elf.lib"
-l"ml_elf.lib"

-l"dts-3d.lib" 
-l"dts-base.lib" 
-l"dts-flib.lib" 
-l"dts-lbr.lib" 
-l"dts-lbr-dec.lib" 
-l"dts-parma-dec.lib" 
-l"dtsx-c-decoder.lib" 
-l"la-strm-reconstruction.lib" 
-l"dts-alg.lib"

-l"dsplib.lib"
-l"mathlib.ae66"

SECTIONS
{
    .globalSectionPafAsitConfig :> CORE0_DDR3
    .globalSectionPafAsotConfig :> CORE0_DDR3
    .globalSectionPafAspmConfig :> CORE0_DDR3
    
    GROUP: {
        .commonSectionPafAstConfig
        .commonSectionAcpStdBetaTable
        .commonSectionAcpCusBetaTable
        .commonSectionAcpCusPhiTable
        .commonSectionAcpCusSigmaTable
    } > COMMON_DDR3
    
    .gCapIbPcmBuf   :> CORE0_DDR3
    .gCapIbBuf      :> DDR3
    .gCapObBuf      :> CORE0_DDR3
    .gCapAfBuf      :> CORE0_DDR3
    .sap_UNDER		:> L2SRAM
}
