
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// ACP Algorithm interface implementation augmentation for custom components
//
//
//

#include <xdc/std.h> //<std.h>
#include <ti/xdais/ialg.h> //<ialg.h>
#include <string.h>

#include <acptype.h>
#include <iacp.h>
#include <paftyp.h>
#include <pafstd_a.h>

#ifdef THX
extern Void ACP_main_thx ();
#endif /* THX */

Void ACP_main_cus ()
{
#if 0    
    // Patch for STD alpha codes. (Here only to reduce code size).
    {
        extern const ACP_Unit default_s[];
        extern const ACP_Unit lm_unknown_s[];
        extern const ACP_Unit lm_standard_s[];
        extern const ACP_Unit lm_pure_s[];

        extern struct
        {
            Int size;
            const ACP_Unit *pSequence[64];
        } IACP_STD_SIGMA_TABLE;

        const ACP_Unit **shortcut = IACP_STD_SIGMA_TABLE.pSequence;

        // Standard default alpha code
        shortcut[0x01] = (Ptr) default_s;
        shortcut[0x10] = (Ptr) lm_unknown_s;
        shortcut[execSTDListeningModeStandard & 0xff] = (Ptr) lm_standard_s;
        shortcut[execSTDListeningModePure & 0xff] = (Ptr) lm_pure_s;
    }
#endif
    
    // Patch for the CUStom alpha codes.
    {
        extern struct
        {
            Int size;
            const ACP_Unit *pSequence[64];
        } IACP_CUS_SIGMA_TABLE;

        //extern const ACP_Unit cus_default_s[];
        extern const ACP_Unit cus_atboot_s_patch[];

        extern const ACP_Unit cus_sigma32_s[];
        extern const ACP_Unit cus_sigma33_s[];
		extern const ACP_Unit cus_sigma34_s[];
		extern const ACP_Unit cus_sigma35_s[];
		extern const ACP_Unit cus_sigma36_s[];

        //extern const ACP_Unit cus_sigma37_s[];
        //extern const ACP_Unit cus_sigma38_s[];
        //extern const ACP_Unit cus_sigma39_s[];
        //extern const ACP_Unit cus_sigma40_s[];
        //extern const ACP_Unit cus_sigma41_s[];
        //extern const ACP_Unit cus_sigma42_s[];
        //extern const ACP_Unit cus_sigma43_s[];
        //extern const ACP_Unit cus_sigma44_s[];
        //extern const ACP_Unit cus_sigma45_s[];
        //extern const ACP_Unit cus_sigma46_s[];
        //extern const ACP_Unit cus_sigma47_s[];

        extern const ACP_Unit cus_sigma48_s[];
        extern const ACP_Unit cus_sigma49_s[];
		extern const ACP_Unit cus_sigma50_s[];
        extern const ACP_Unit cus_sigma51_s[];
        extern const ACP_Unit cus_sigma52_s[];
        //extern const ACP_Unit cus_sigma53_s[];
        //extern const ACP_Unit cus_sigma54_s[];
        //extern const ACP_Unit cus_sigma55_s[];
        //extern const ACP_Unit cus_sigma56_s[];
        //extern const ACP_Unit cus_sigma57_s[];
        //extern const ACP_Unit cus_sigma58_s[];
        //extern const ACP_Unit cus_sigma59_s[];
        //extern const ACP_Unit cus_sigma60_s[];
        //extern const ACP_Unit cus_sigma61_s[];
        //extern const ACP_Unit cus_sigma62_s[];
        //extern const ACP_Unit cus_sigma63_s[];

        const ACP_Unit **shortcut = IACP_CUS_SIGMA_TABLE.pSequence;

        // CUStom at-boot alpha code
        //shortcut[1] = cus_default_s;
        shortcut[2] = cus_atboot_s_patch;

        // CUStom Input switching alpha codes
        shortcut[32] = cus_sigma32_s;
        shortcut[33] = cus_sigma33_s;
        shortcut[34] = cus_sigma34_s;
		shortcut[35] = cus_sigma35_s;
		shortcut[36] = cus_sigma36_s;

        //shortcut[37] = cus_sigma37_s;
        //shortcut[38] = cus_sigma38_s;
        //shortcut[39] = cus_sigma39_s;
        //shortcut[40] = cus_sigma40_s;
        //shortcut[41] = cus_sigma41_s;
        //shortcut[42] = cus_sigma42_s;
        //shortcut[43] = cus_sigma43_s;
        //shortcut[44] = cus_sigma44_s;
        //shortcut[45] = cus_sigma45_s;
        //shortcut[46] = cus_sigma46_s;
        //shortcut[47] = cus_sigma47_s;

        // CUStom Output switching alpha codes
        shortcut[48] = cus_sigma48_s;
        shortcut[49] = cus_sigma49_s;
        shortcut[50] = cus_sigma50_s;
        shortcut[51] = cus_sigma51_s;
        shortcut[52] = cus_sigma52_s;
        //shortcut[53] = cus_sigma53_s;
        //shortcut[54] = cus_sigma54_s;
        //shortcut[55] = cus_sigma55_s;
        //shortcut[56] = cus_sigma56_s;
        //shortcut[57] = cus_sigma57_s;
        //shortcut[58] = cus_sigma58_s;
        //shortcut[59] = cus_sigma59_s;
        //shortcut[60] = cus_sigma60_s;
        //shortcut[61] = cus_sigma61_s;
        //shortcut[62] = cus_sigma62_s;
        //shortcut[63] = cus_sigma63_s;
    }

    // Enable this when custom phi is to be added
    // (example code below installs custom phi function 0;
    // up to 8 functions may be installed).

#if 0
    // Patch for the CUStom phi functions.
    {
        extern struct
        {
            Int size;
            IACP_PhiFunction *pFunction[8];
        } IACP_CUS_PHI_TABLE;

        IACP_PhiFunction **pFunc = IACP_CUS_PHI_TABLE.pFunction;

        extern IACP_PhiFunction cust_phifun0;

        pFunc[0] = cust_phifun0;
    }
#endif

#ifdef THX
    ACP_main_thx ();
#endif /* THX */
}
