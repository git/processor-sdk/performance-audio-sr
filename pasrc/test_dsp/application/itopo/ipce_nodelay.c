/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  IPCE default instance creation parameters
 */
#include <xdc/std.h> //<std.h>
#include <ipce.h>

/*
 *  ======== IPCE_PARAMS_NODELAY ========
 *  This static initialization defines the default parameters used to
 *  create instances of PCE objects.
 */

#if IPCE_PHASES != 6
#error internal error
#endif

// Start of customizable code -------------------------------------------------

#define IPCE_OUTPUT_NUMCHAN 32                               // Number of output channels
//#define IPCE_OUTPUT_ESIZE   3                               // Element size for output: 24 or 32 bits
#define IPCE_OUTPUT_ESIZE 4                               // Element size for output: 24 or 32 bits

#define IPCE_FSIZE          256                             // Size of Audio Frame

#define IPCE_OUTPUT_NUMCHAN_SECONDARY 2                     // Number of output channels in secondary

// End of customizable code ---------------------------------------------------

#define IPCE_DELAY_ESIZE    4                               // Element size for delay: packed 24 bits

/*
 *  ======== IPCE_PARAMS_STATUS_DELAY_NODELAY ========
 */

const IPCE_Status IPCE_PARAMS_STATUS_NODELAY = {
    sizeof (IPCE_Status),
    1,                                                      /* mode: enabled */
    0,                                                      /* type: unused */
    1, 0,                                                   /* phase 0 mode,type: enabled,unused */
    0, 0,                                                   /* phase 1 mode,type: enabled,unused */
    1, 0,                                                   /* phase 2 mode,type: enabled,unused */
    1, 0,                                                   /* phase 3 mode,type: enabled,unused */
    1, 0,                                                   /* phase 4 mode,type: enabled,unused */
    1, 0,                                                   /* phase 5 mode,type: enabled,unused */
    /* Delay Status */
    0,                                                      /* unused */
    1,                                                      /* unit */
    PAF_MAXNUMCHAN,                                         /* numc */
    0,                                                      /* nums */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,   // delay
    0,                                  // masterDelay
    0,                                  // pceExceptionDetect
    0,                                  // pceExceptionFlag
    0,                                  // pceExceptionMute
    0,                                  // pceClipDetect
    0,                                  // bsMetadata_type
    0,                                  // mdInsert
    32                                  // maxNumChMd
};

/*
 *  ======== IPCE_PARAMS_CONFIG_DELAY_NODELAY ========
 *  This static initialization defines the parameters used to create
 *  instances of DEL objects
 */

const IPCE_ConfigPhaseVolume IPCE_PARAMS_CONFIG_PHASE_VOLUME_NODELAY = {
    sizeof (IPCE_ConfigPhaseVolume),
    -2 * 40,                                                /* volumeRamp */
};

const IPCE_ConfigPhaseDelay IPCE_PARAMS_CONFIG_PHASE_DELAY_NODELAY = {
    sizeof (IPCE_ConfigPhaseDelay),
    0,
    IPCE_DELAY_ESIZE,
    NULL,
};

const IPCE_Config IPCE_PARAMS_CONFIG_NODELAY = {
    sizeof (IPCE_Config),
    0,                                                      /* frameLength */
    0,                                                      /* unused */
    (IPCE_ConfigPhase *) & IPCE_PARAMS_CONFIG_PHASE_VOLUME_NODELAY, /* phase 0 config: volume */
    (IPCE_ConfigPhase *) & IPCE_PARAMS_CONFIG_PHASE_DELAY_NODELAY,  /* phase 1 config: delay */
    0,                                                      /* phase 2 config: output */
    0,                                                      /* phase 3 config: unused */
    0,                                                      /* phase 4 config: unused */
    0,                                                      /* phase 5 config: unused */
    0,                                                      /* phase 0 common: volume */
    0,  /* phase 1 common: delay */
    0,                                                      /* phase 2 common: output */
    0,                                                      /* phase 3 common: unused */
    0,                                                      /* phase 4 common: unused */
    0,                                                      /* phase 5 common: unused */
    /* scale -- uninitialized */
};

/*
 *  ======== IPCE_PARAMS_ACTIVE ========
 */

const PAF_ActivePhaseVolume IPCE_PARAMS_ACTIVE_PHASE_VOLUME_NODELAY = {
    sizeof (PAF_ActivePhaseVolume),
    /* pVolumeStatus: uninitialized */
};

const PAF_ActivePhaseOutput IPCE_PARAMS_ACTIVE_PHASE_OUTPUT_NODELAY = {
    sizeof (PAF_ActivePhaseOutput),
    /* pEncodeStatus: uninitialized */
    /* pOutBufConfig: uninitialized */
};

const IPCE_Active IPCE_PARAMS_ACTIVE_NODELAY = {
    0,
    0,
    (PAF_ActivePhase *) & IPCE_PARAMS_ACTIVE_PHASE_VOLUME_NODELAY,  /* phase 0 active: volume */
    0,                                                      /* phase 1 active: delay */
    (PAF_ActivePhase *) & IPCE_PARAMS_ACTIVE_PHASE_OUTPUT_NODELAY,  /* phase 2 active: output */
    0,                                                      /* phase 3 active: unused */
    0,                                                      /* phase 4 active: unused */
    0,                                                      /* phase 5 active: unused */
    /* bitstreamMask uninitialized */
};

/*
 *  ======== IPCE_PARAMS_SCRACH ========
 */
const PAF_ScrachPhaseOutput IPCE_PARAMS_SCRACH_PHASE_OUTPUT_NODELAY = {
    sizeof (PAF_ScrachPhaseDelay) + IPCE_OUTPUT_NUMCHAN * IPCE_OUTPUT_ESIZE * IPCE_FSIZE,
};

const IPCE_Scrach IPCE_PARAMS_SCRACH_NODELAY = {
    sizeof (IPCE_Scrach),
    0,                                                      /* phase 0 scrach: volume */
    0,  /* phase 1 scrach: delay */
    0, //(PAF_ScrachPhase *) & IPCE_PARAMS_SCRACH_PHASE_OUTPUT_NODELAY,  /* phase 2 scrach: output */
    0,                                                      /* phase 3 scrach: unused */
    0,                                                      /* phase 4 scrach: unused */
    0,                                                      /* phase 5 scrach: unused */
};

const IPCE_Params IPCE_PARAMS_NODELAY = {
    sizeof (IPCE_Params),
    &IPCE_PARAMS_STATUS_NODELAY,
    &IPCE_PARAMS_CONFIG_NODELAY,
    &IPCE_PARAMS_ACTIVE_NODELAY,
    &IPCE_PARAMS_SCRACH_NODELAY,
};

const PAF_ScrachPhaseDelay IPCE_PARAMS_SCRACH_PHASE_OUTPUT_CUS_SEC = {
    sizeof(PAF_ScrachPhaseDelay)+IPCE_OUTPUT_NUMCHAN_SECONDARY*IPCE_OUTPUT_ESIZE*IPCE_FSIZE, // 2*256*4 = no of output channels for rec out * block size * output sample size
};

const IPCE_Scrach IPCE_PARAMS_SCRACH_CUS_SEC = {
    sizeof(IPCE_Scrach),
    0,                                                          /* phase 0 scrach: volume */
    0,                                                          /* phase 1 scrach: delay  */
    0, //(PAF_ScrachPhase *)&IPCE_PARAMS_SCRACH_PHASE_OUTPUT_CUS_SEC,    /* phase 2 scrach: output */
    0,                                                          /* phase 3 scrach: unused */
    0,                                                          /* phase 4 scrach: unused */
    0,                                                          /* phase 5 scrach: unused */
};

const IPCE_Params IPCE_PARAMS_CUS_SEC = {
    sizeof(IPCE_Params),
    &IPCE_PARAMS_STATUS_NODELAY,
    &IPCE_PARAMS_CONFIG_NODELAY,
    &IPCE_PARAMS_ACTIVE_NODELAY,
    &IPCE_PARAMS_SCRACH_CUS_SEC,
};

#ifndef __TI_EABI__
asm (" .global _IPCE_PARAMS");
asm ("_IPCE_PARAMS .set _IPCE_PARAMS_NODELAY");
#else
asm (" .global IPCE_PARAMS");
asm ("IPCE_PARAMS .set IPCE_PARAMS_NODELAY");    
#endif
