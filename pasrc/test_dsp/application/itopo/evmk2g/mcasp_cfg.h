/*
 * Copyright (c) 2018, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      mcasp_config.h
 *
 * \brief     McASP configuration header file
 *
 */

#ifndef _MCASP_CONFIG_H_
#define _MCASP_CONFIG_H_

#include <xdc/std.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/io/GIO.h>
#include <ti/sysbios/io/IOM.h>
#include <ti/sysbios/heaps/HeapMem.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sdo/edma3/drv/edma3_drv.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/hal/Cache.h>
#include <ti/sysbios/family/c64p/Hwi.h>

#include <stdio.h>

#include <ti/drv/mcasp/mcasp_drv.h>

#include "audio_dc_cfg.h"
#include "edma_cfg.h"

#define NUM_MCASP_PORTS 3

/**
 * \brief McASP configurations for Rx - DIR
 */
/** Receive format unit bit mask register defult value */
#define MCASP_DIR_RMASK      (0xFFFFFFFF)

/** Receive bit stream format register defult value */
#define MCASP_DIR_RFMT       (  ( 1 << 16)   /* RDATDLY: Transmit with 1 bit delay */ \
                              | ( 1 << 15)   /* RXRVRS  : MSB first */                \
                              | ( 0 << 13)   /* RPAD   : Pad extra bits with 0 */     \
		                      | (15 << 4)    /* RSSZ   : 32-bit slot size */          \
                              | ( 0 << 3)    /* RBUSSEL: Reads from edma port */      \
                              | ( 0 << 0))    /* RROT   : No rotation */

/** Receive frame sync control register defult value */
#define MCASP_DIR_AFSRCTL    (  (2 << 7)    /* RMOD : 2-slot TDM */                         \
                              | (1 << 4)    /* FRWID: Single word frame sync */             \
                              | (0 << 1)    /* FSRM : Externally-generated rx frame sync */ \
                              | (1 << 0))   /* FSRP : Start on falling edge */

/** Receive TDM time slot 0-31 register defult value */
#define MCASP_DIR_RTDM       (0x00000003)

/** Receive interrupt control register defult value */
#define MCASP_DIR_RINTCTL    (0x00000000)

/** Receive status register defult value */
#define MCASP_DIR_RSTAT      (0x000001FF)

/** Receive DMA event control register defult value */
#define MCASP_DIR_REVTCTL    (0x00000000)

/** Receive clock control register defult value */
#define MCASP_DIR_ACLKRCTL   (  (1 << 7)    /* CLKRP  : RX on raising edge */         \
                              | (0 << 5)    /* CLKRM  : Externally generated ACLK */  \
                              | (0 << 0))   /* CLKRDIV: NA */

/** Receive high-frequency clock control register defult value */
#define MCASP_DIR_AHCLKRCTL   (  (0 << 15)    /* HCLKRM  : Clock from AHCLKR pin */  \
                               | (0 << 14)    /* HCLKRP  : Falling edge */           \
                               | (0x400 << 0))    /* HCLKRDIV: AHCLKR = AUXCLK / 1 */

/** Receive clock check control register defult value */
#define MCASP_DIR_RCLKCHK     (0x00000000)

/** Data structure McASP LLD configuration parameters */
typedef struct {
     Mcasp_HwSetupData * mcaspSetupData;    // McASP setup for Tx or Rx
     Mcasp_ChanParams  * mcaspChanParams;   // LLD channel params
     uint32_t clkSetupClkRx;                // set ACLKRCTL during mcaspBindDev
     uint32_t clkSetupClkTx;                // set ACLKXCTL during mcaspBindDev
     uint32_t pdirAmute;                    // set the AMUTE bit in Pin Direction Register (PDIR)
     uint32_t amute;                        // set the Audio Mute Control Register (AMUTE)
     Int mcaspPort;                         // McASP port number
     Mcasp_chanMode_e chanMode;             // MCASP_INPUT or MCASP_OUTPUT
     MCASP_TiomCallback cbFxn;              // call back function for transfer completion
     Ptr hMcaspDev;                         // McASP device handle
     Ptr hMcaspChan;                        // McASP channel handle
} mcaspLLDconfig;

/** McASP LLD configuration parameters for all input and output interfaces */
extern mcaspLLDconfig LLDconfigRxDIR;
extern mcaspLLDconfig LLDconfigRxADC;
extern mcaspLLDconfig LLDconfigRxADC6ch;
extern mcaspLLDconfig LLDconfigRxADCStereo;
extern mcaspLLDconfig LLDconfigRxHDMIStereo;
extern mcaspLLDconfig LLDconfigRxHDMI;
extern mcaspLLDconfig LLDconfigTxDAC;
extern mcaspLLDconfig LLDconfigTxDACSlave;
extern mcaspLLDconfig LLDconfigTxDACStereo;
extern mcaspLLDconfig LLDconfigTxDACStereoSlave;
extern mcaspLLDconfig LLDconfigTxDAC12ch;
extern mcaspLLDconfig LLDconfigTxDAC16ch;

/**
 *  \brief   Configures McASP module and creates the channel
 *           for audio Tx and Rx
 *
 *  \return    Platform_EOK on Success or error code
 */
Aud_STATUS mcaspAudioConfig(void);

Aud_STATUS mcaspTxCreate(void);
Aud_STATUS mcaspTxReset(void);
Aud_STATUS mcaspRxCreate(void);
Aud_STATUS mcaspRxReset(void);
Aud_STATUS mcaspRecfgWordWidth(Ptr hMcaspChan, uint16_t wordWidth);
int mcaspCheckOverUnderRun(Ptr mcaspChanHandle);
Aud_STATUS mcasplldChanCreate(mcaspLLDconfig *lldCfg, Ptr *pChanHandle);

#endif /* _MCASP_CONFIG_H_ */
