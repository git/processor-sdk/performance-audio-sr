
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// Input / output device configuration definitions
//

//#include <ti/procsdk_audio/procsdk_audio_typ.h>
#include <procsdk_audio_typ.h>

#include <acptype.h>
#include <acpbeta.h>
#include <pafstd_a.h>
#include <pafdec_a.h>
#include <pafenc_a.h>
#include <pafsio.h>

#include <alpha/pcm_a.h>

#include <inpbuf_a.h>
#include <outbuf_a.h>

#include <pa_i13_evmk2g_io_a.h> //<pa_i13_evmda830_io_a.h>
#include <sap_d10.h>

#define  rb32DECSourceDecode 0xc024,0x0b81
#define  ob32DECSourceDecodeNone 0x0001,0x0000

#define  rb32IBSioSelect 0xc022,0x0581
#define  ob32IBSioSelect(X) (X)|0x0080,0x0000

#define  ob32OBSioSelect(X) (X)|0x0080,0x0000
#define  rb32DECSourceSelect_3 0xc024,0x09b1
#define  wb32DECSourceSelect_3 0xc024,0x09f1

#define writePA3Await(RB32,WB32) 0xcd0b,5+2,0x0204,200,10,WB32,RB32

// -----------------------------------------------------------------------------
//
// Input device configurations & shortcut definitions
//

const struct
{
    Int n;
    const PAF_SIO_Params *x[DEVINP_N];
} patchs_devinp[1] =
{
    DEVINP_N,
        // These values reflect the definitions DEVINP_* in pa*io_a.h:
        NULL,                                               // InNone
        (const PAF_SIO_Params *) &SAP_D10_RX_HDMI_STEREO,   // InHDMIStereo
        (const PAF_SIO_Params *) &SAP_D10_RX_HDMI,          // InHDMI
        (const PAF_SIO_Params *) &SAP_D10_RX_DIR,           // InDigital
        (const PAF_SIO_Params *) &SAP_D10_RX_ADC_44100HZ,   // InAnalog
};


// .............................................................................
// execPAIInNone
#define CUS_SIGMA32_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    wroteDECSourceProgramUnknown, \
    writeIBSampleRateOverrideUnknown, \
    writeIBSioSelectN(DEVINP_NULL), \
    0xcdf0,execPAIInNone

#pragma DATA_SECTION(cus_sigma32_s0, ".none")
const ACP_Unit cus_sigma32_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA32_S,
};

const ACP_Unit cus_sigma32_s[] = {
    0xc900 + sizeof (cus_sigma32_s0) / 2 - 1,
    CUS_SIGMA32_S,
};

// execPAIInHDMIStereo
#define CUS_SIGMA33_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom2(0,1), \
    writeIBUnknownTimeoutN(8*1024), \
    writeIBScanAtHighSampleRateModeEnable, \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverrideStandard, \
    writeIBSioSelectN(DEVINP_HDMI_STEREO), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectAuto, \
    0xcdf0,execPAIInHDMIStereo

#pragma DATA_SECTION(cus_sigma33_s0, ".none")
const ACP_Unit cus_sigma33_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA33_S,
};

const ACP_Unit cus_sigma33_s[] = {
    0xc900 + sizeof (cus_sigma33_s0) / 2 - 1,
    CUS_SIGMA33_S,
};

// execPAIInHDMI
#define CUS_SIGMA34_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramSurround4_1, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom16(0,4,1,5,2,6,3,7,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBUnknownTimeoutN(15*1024), \
    writeIBScanAtHighSampleRateModeEnable, \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverrideStandard, \
    writeIBSioSelectN(DEVINP_HDMI), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectAuto, \
    0xcdf0,execPAIInHDMI

// writeDECChannelMapFrom16(0,4,1,5,2,6,3,7,-3,-3,-3,-3,-3,-3,-3,-3), \
#pragma DATA_SECTION(cus_sigma34_s0, ".none")
const ACP_Unit cus_sigma34_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA34_S,
};

const ACP_Unit cus_sigma34_s[] = {
    0xc900 + sizeof (cus_sigma34_s0) / 2 - 1,
    CUS_SIGMA34_S,
};

// execPAIInDigital
#define CUS_SIGMA35_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeIBUnknownTimeoutN(2*2048), \
    writeIBScanAtHighSampleRateModeDisable, \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideDisable, \
    writeIBPrecisionDefaultOriginal, \
    writeIBPrecisionOverrideDetect, \
    writeIBSampleRateOverrideStandard, \
    writeIBSioSelectN(DEVINP_DIR), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectAuto, \
    0xcdf0,execPAIInDigital

#pragma DATA_SECTION(cus_sigma35_s0, ".none")
const ACP_Unit cus_sigma35_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA35_S,
};

const ACP_Unit cus_sigma35_s[] = {
    0xc900 + sizeof (cus_sigma35_s0) / 2 - 1,
    CUS_SIGMA35_S,
};

// execPAIInAnalog
#define CUS_SIGMA36_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramSurround4_1, \
    writePCMScaleVolumeN(2*6), \
    writeDECChannelMapFrom16(0,4,1,5,2,6,3,7,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverride44100Hz, \
    writeIBSioSelectN(DEVINP_ADC), \
    writeDECSourceSelectPCM,        \
    0xcdf0,execPAIInAnalog

#pragma DATA_SECTION(cus_sigma36_s0, ".none")
const ACP_Unit cus_sigma36_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA36_S,
};

const ACP_Unit cus_sigma36_s[] = {
    0xc900 + sizeof (cus_sigma36_s0) / 2 - 1,
    CUS_SIGMA36_S,
};

// -----------------------------------------------------------------------------
//
// Output device configurations & shortcut definitions
//

const struct
{
    Int n;
    const PAF_SIO_Params *x[DEVOUT_N];
} patchs_devout[1] =
{
    DEVOUT_N,
        // These values reflect the definitions DEVOUT_* in pa*io_a.h:
        NULL,                                               // OutNone
        (const PAF_SIO_Params *) &SAP_D10_TX_DAC,           // OutAnalog
        (const PAF_SIO_Params *) &SAP_D10_TX_DAC_SLAVE,     // OutAnalogSlave
        (const PAF_SIO_Params *) &SAP_D10_TX_DAC_12CH,      // OutAnalog_12ch
        (const PAF_SIO_Params *) &SAP_D10_TX_DAC_16CH,      // OutAnalog_16ch
};


// .............................................................................
// execPAIOutNone
#define CUS_SIGMA48_S \
    writeOBSioSelectN(DEVOUT_NULL), \
    writePA3Await(rb32OBSioSelect,ob32OBSioSelect(DEVOUT_NULL)), \
    0xcdf0,execPAIOutNone
    
#pragma DATA_SECTION(cus_sigma48_s0, ".none")
const ACP_Unit cus_sigma48_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA48_S,
};

const ACP_Unit cus_sigma48_s[] = {
    0xc900 + sizeof (cus_sigma48_s0) / 2 - 1,
    CUS_SIGMA48_S,
};

// .............................................................................
// execPAIOutAnalog
#define CUS_SIGMA49_S \
    writeOBSioSelectN(DEVOUT_DAC), \
    writeENCChannelMapTo16(0,4,1,5,2,6,3,7,-3,-3,-3,-3,-3,-3,-3,-3), \
    writePA3Await(rb32OBSioSelect,ob32OBSioSelect(DEVOUT_DAC)), \
    0xcdf0,execPAIOutAnalog

#pragma DATA_SECTION(cus_sigma49_s0, ".none")
const ACP_Unit cus_sigma49_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA49_S,
};

const ACP_Unit cus_sigma49_s[] = {
    0xc900 + sizeof (cus_sigma49_s0) / 2 - 1,
    CUS_SIGMA49_S,
};

// .............................................................................
// execPAIOutAnalogSlave
#define CUS_SIGMA50_S \
    writeOBSioSelectN(DEVOUT_DAC_SLAVE), \
    writeENCChannelMapTo16(0,4,1,5,2,6,3,7,-3,-3,-3,-3,-3,-3,-3,-3), \
    writePA3Await(rb32OBSioSelect,ob32OBSioSelect(DEVOUT_DAC_SLAVE)), \
    0xcdf0,execPAIOutAnalogSlave

#pragma DATA_SECTION(cus_sigma50_s0, ".none")
const ACP_Unit cus_sigma50_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA50_S,
};

const ACP_Unit cus_sigma50_s[] = {
    0xc900 + sizeof (cus_sigma50_s0) / 2 - 1,
    CUS_SIGMA50_S,
};

// .............................................................................
// execPAIOutAnalog12Ch
#define CUS_SIGMA51_S \
    writeOBSioSelectN(DEVOUT_DAC_12CH), \
    writeENCChannelMapTo16(0,6,1,7,2,8,3,9,4,10,5,11,-3,-3,-3,-3), \
    writePA3Await(rb32OBSioSelect,ob32OBSioSelect(DEVOUT_DAC_12CH)), \
    0xcdf0,execPAIOutAnalog12Ch

#pragma DATA_SECTION(cus_sigma51_s0, ".none")
const ACP_Unit cus_sigma51_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA51_S,
};

const ACP_Unit cus_sigma51_s[] = {
    0xc900 + sizeof (cus_sigma51_s0) / 2 - 1,
    CUS_SIGMA51_S,
};

// .............................................................................
// execPAIOutAnalog16Ch
#define CUS_SIGMA52_S \
    writeOBSioSelectN(DEVOUT_DAC_16CH), \
    writeENCChannelMapTo16(0,8,1,9,2,10,3,11,4,12,5,13,6,14,7,15), \
    writePA3Await(rb32OBSioSelect,ob32OBSioSelect(DEVOUT_DAC_16CH)), \
    0xcdf0,execPAIOutAnalog16Ch

#pragma DATA_SECTION(cus_sigma52_s0, ".none")
const ACP_Unit cus_sigma52_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA52_S,
};

const ACP_Unit cus_sigma52_s[] = {
    0xc900 + sizeof (cus_sigma52_s0) / 2 - 1,
    CUS_SIGMA52_S,
};
// EOF
