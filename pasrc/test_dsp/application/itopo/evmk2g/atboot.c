
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Initial version obtained from ver 1.10 of i12/evmda830/atboot.c
//

//#include <ti/procsdk_audio/procsdk_audio_typ.h>
#include <procsdk_audio_typ.h>

#include <acptype.h>
#include <pafaip_a.h>

#include <pa_i13_evmk2g_io_a.h> //<pa_i13_evmda830_io_a.h>

#include "dbgBenchmark.h" // PCM high-sampling rate + SRC + CAR benchmarking

#ifdef DTS_BUILD
#define DTS_ATBOOT_CFG 1
#endif

#ifdef DTS_ATBOOT_CFG    
//DTS_ATBOOT_CFG
#define CUS_ATBOOT_S \
    writeDECChannelMapTo16(PAF_LEFT,PAF_RGHT,8,9,2,12,10,11,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeENCChannelMapFrom16(PAF_LEFT,PAF_RGHT,8,9,2,12,10,11,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeVOLControlMasterN(0), \
    writeVOLOffsetMasterN(0x7fff), \
    writeSYSRecreationModeDirect, \
    writeSYSChannelConfigurationRequestSurround4_1, \
    execPAIInDigital, \
    execPAIOutAnalog
#else 
//Not DTS_ATBOOT_CFG

#ifndef DDP_ATBOOT_CFG
//
// NOT DDP at-boot configuration
//

#ifndef _ENABLE_BENCHMARK_PCMHSR_SRC_CAR_
//
// THD at-boot configuration
//

// NOT Performing PCM high-sampling rate + SRC + CAR benchmarking
// 4XI2S HDMI input for multi-ch PCM
#define CUS_ATBOOT_S \
    writeDECChannelMapTo16(PAF_LEFT,PAF_RGHT,8,9,2,12,10,11,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeENCChannelMapFrom16(PAF_LEFT,PAF_RGHT,8,9,2,12,10,11,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeVOLControlMasterN(0), \
    writeVOLOffsetMasterN(0x7fff), \
    writeSYSRecreationModeDirect, \
    writeSYSChannelConfigurationRequestSurround4_1, \
    execPAIInDigital, \
    execPAIOutAnalog
#else
// Performing PCM high-sampling rate + SRC + CAR benchmarking
// 4XI2S HDMI input for multi-ch PCM
#define CUS_ATBOOT_S \
    writeDECChannelMapTo16(PAF_LEFT,PAF_RGHT,8,9,2,12,10,11,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeENCChannelMapFrom16(PAF_LEFT,PAF_RGHT,8,9,2,12,10,11,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeVOLControlMasterN(0), \
    writeVOLOffsetMasterN(0x7fff), \
    writeSYSRecreationModeDirect, \
    writeSYSChannelConfigurationRequestSurround4_1, \
    execPAIInHDMI, \
    execPAIOutAnalog, \
    writeSRCModeDisable, \
    writeCARModeDisable
#endif    
    
#else // DDP_ATBOOT_CFG

//
// DDP at-boot configuration
//
#define CUS_ATBOOT_S \
    writeDECChannelMapTo16(PAF_LEFT,PAF_RGHT,8,9,2,12,10,11,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeENCChannelMapFrom16(PAF_LEFT,PAF_RGHT,8,9,2,12,10,11,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeVOLControlMasterN(0), \
    writeVOLOffsetMasterN(0x7fff), \
    writeSYSRecreationModeDirect, \
    writeSYSChannelConfigurationRequestSurround4_1, \
    execPAIInHDMIStereo, \
    execPAIOutAnalog
    
#endif // DDP_ATBOOT_CFG
#endif // NOT DTS_ATBOOT_CFG

#pragma DATA_SECTION(cus_atboot_s0_patch, ".none")
const ACP_Unit cus_atboot_s0_patch[] = {
    0xc900 + 0 - 1,
    CUS_ATBOOT_S,
};

const ACP_Unit cus_atboot_s_patch[] = {
    0xc900 + sizeof(cus_atboot_s0_patch)/2 - 1,
    CUS_ATBOOT_S,
};

const ACP_Unit cus_atboot_s[] = {
    0xc900 + sizeof(cus_atboot_s0_patch)/2 - 1,
    CUS_ATBOOT_S,
};

const ACP_Unit cus_attime_s[] = {
    0xc900,
};
// EOF
