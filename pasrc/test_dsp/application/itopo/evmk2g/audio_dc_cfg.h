
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 * \file      audio_dc_cfg.h
 *
 * \brief     Audio daughter card configuration header file
 *
 */

#ifndef _AUDIO_DC_CFG_H_
#define _AUDIO_DC_CFG_H_

#include <ti/addon/aud/include/aud.h>
#include <ti/addon/aud/include/aud_audio.h>
/* TI-RTOS Header files */
#include <ti/drv/i2c/I2C.h>
#include <ti/drv/i2c/soc/I2C_soc.h>

/**
 *  \brief    ADC configuration parameter structure
 *
 */
typedef struct _AdcConfig
{
	uint8_t             gain;
	AudAdcLeftInputMux  adc1LMux;
	AudAdcLeftInputMux  adc2LMux;
	AudAdcRightInputMux adc1RMux;
	AudAdcRightInputMux adc2RMux;
	AudAdcRxWordLen     wlen;
	AudAdcDataFormat    format;
	uint8_t             intEnable;
} AdcConfig;

/**
 *  \brief    DAC configuration parameter structure
 *
 */
typedef struct _DacConfig
{
	AudDacAmuteCtrl    amuteCtrl;
	uint8_t            amuteEnable;
	AudDacSamplingMode samplingMode;
	AudDacDataFormat   dataFormat;
	uint8_t            softMuteEnable;
	AudDacAttnMode     attnMode;
	AudDacDeempCtrl    deempCtrl;
	uint8_t            volume;
} DacConfig;

/**
 *  \brief    Initializes ADC module
 *
 *  This function initializes and configures the ADC modules
 *  on audio daughter card
 *
 *  \param     devId  [IN]  ADC Device Id
 *  \param     config [IN]  ADC configuration parameters
 *
 *  \return    Platform_EOK on Success or error code
 */
Aud_STATUS audioAdcConfig(AudAdcDevId  devId, AdcConfig *config);

/**
 *  \brief    Initializes DAC module
 *
 *  This function initializes and configures the DAC modules
 *  on audio daughter card
 *
 *  \param     devId  [IN]  DAC Device Id
 *  \param     config [IN]  DAC configuration parameters
 *
 *  \return    Platform_EOK on Success or error code
 */
Aud_STATUS audioDacConfig(AudDacDevId devId, DacConfig *config);

/**
 *  \brief    Initializes DIR module
 *
 *  This function initializes and configures the DIR modules
 *  on audio daughter card
 *
 *  \return    Aud_STATUS on Success or error code
 */
Aud_STATUS audioDirConfig(void);

/**
 *  \brief    Initializes HSR41 module
 *
 *  This function initializes and configures the HDMI module
 *  with the audio daughter card
 *
 *  \return    Aud_STATUS on Success
 */
Aud_STATUS audioHDMIConfig(void);

/**
 *  \brief    Read HDMI input sampling rate
 *
 */
unsigned int read_hdmi_samprate();

/**
 *  \brief    Clear HDMI interrupt on ~HMINT line
 *
 */
void clear_hdmi_hmint();

/* ------------------------------------------------------------------------ *
 *                                                                          *
 *  Variable types                                                          *
 *                                                                          *
 * ------------------------------------------------------------------------ */

#define Uint32  unsigned int
#define Uint16  unsigned short
#define Uint8   unsigned char
#define Int32   int
#define Int16   short
#define Int8    char


#endif /* _AUDIO_DC_CFG_H_ */

/* Nothing past this point */
