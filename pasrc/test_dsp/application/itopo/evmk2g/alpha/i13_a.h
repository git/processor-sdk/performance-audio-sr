
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

////// I13 master alpha code symbol file////
#ifndef _I13_A_H_
#define _I13_A_H_


#include <paftyp_a.h>
#include <pafsys_a.h>
#include <acp_a.h>
#include <inpbuf_a.h>
#include <pafdec_a.h>
#include <pafenc_a.h>
#include <pafvol_a.h>
#include <outbuf_a.h>
#include <paferr_a.h>
#include <pafid_a.h>
#include <pafstd_a.h>
#include <pafstream_a.h>
//#include <pafnic_a.h>
#include <pcm_a.h>
#include <pce_a.h>
//#include <dm_a.h>
#include <ae_a.h>
#include <ml_a.h>
#include <src_a.h>
#include <pafdecopcb_a.h>

#ifdef DH_BUILD
#include <ddpat_a.h>
#include <thd_a.h>
#include <car_a.h>
#include <oar_a.h>
#include <bmda_a.h>
#endif // DH_BUILD

#ifdef DTS_BUILD
#include <dtsuhda_a.h>
#include <dtsuhdb_a.h>
#endif // DTS_BUILD

#endif /* _I13_A_H_ */
