
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef HSDIOALPHACMDS_H
#define HSDIOALPHACMDS_H
/*****************************************************************************
Filename:
   vprocCmds_a.h

Project:
    VPROC

//  

Note:
    HSDIO = High Speed Digital Input Output
******************************************************************************/


// ****************************************************************************
// ***** I2C Addresses
// ****************************************************************************
#define HSDIO_I2C_ADDRESS_1 0xB8
#define HSDIO_I2C_ADDRESS_2 0xBA
#define HSDIO_I2C_ADDRESS_3 0xBC
#define HSDIO_I2C_ADDRESS_4 0xBE


// ****************************************************************************
// ***** Software Version & Release Status (1 byte)
// ****************************************************************************
// was SOFTWARE_RELEASE
#define HSDIO_SOFTWARE_VERSION_STS 0xC201,0x0900
#define HSDIO_SOFTWARE_RELEASE_STS 0xC201,0x0000

// ****************************************************************************
// ***** Input Select (1 byte)
// ****************************************************************************
#define HSDIO_INPUT_SELECT(nn) 0xCA00,0x0100+(nn&0xff)
#define HSDIO_GET_INPUT_SELECT 0xC200,0x0100
#define HSDIO_INPUT_SELECT_STS 0xC201,0x0100

#define HSDIO_Input_LOW_POWER   0
#define HSDIO_Input_COMPONENT   1
#define HSDIO_Input_VGA         2
#define HSDIO_Input_COMPOSITE_1 3
#define HSDIO_Input_COMPOSITE_2 4
#define HSDIO_Input_COMPOSITE_3 5
#define HSDIO_Input_COMPOSITE_4 6
#define HSDIO_Input_SVID_1      7
#define HSDIO_Input_EXTERNAL    8
#define HSDIO_Input_HDMI_1      9
#define HSDIO_Input_HDMI_2      10
#define HSDIO_Input_HDMI_3      11
#define HSDIO_Input_HDMI_4      12
#define HSDIO_Input_HDMI_5      13
#define HSDIO_Input_HDMI_6      14
#define HSDIO_Input_HDMI_7      15
#define HSDIO_Input_HDMI_8      16
#define HSDIO_Input_HDMI_9      17
#define HSDIO_Input_HDMI_10     18
#define HSDIO_Input_HDMI_11     19
#define HSDIO_Input_HDMI_12     20
#define HSDIO_Input_HDMI_13     21
#define HSDIO_Input_HDMI_14     22
#define HSDIO_Input_HDMI_15     23
#define HSDIO_Input_HDMI_16     24
#define HSDIO_Input_UNKNOWN     0xff

// or use direct input commands...
#define HSDIO_INPUT_SELECT_LOW_POWER 0xCA00,0x0100
#define HSDIO_INPUT_SELECT_COMPONENT 0xCA00,0x0101
#define HSDIO_INPUT_SELECT_VGA 0xCA00,0x0102
#define HSDIO_INPUT_SELECT_COMPOSITE_1 0xCA00,0x0103
#define HSDIO_INPUT_SELECT_COMPOSITE_2 0xCA00,0x0104
#define HSDIO_INPUT_SELECT_COMPOSITE_3 0xCA00,0x0105
#define HSDIO_INPUT_SELECT_COMPOSITE_4 0xCA00,0x0106
#define HSDIO_INPUT_SELECT_SVID_1 0xCA00,0x0107
#define HSDIO_INPUT_SELECT_EXTERNAL 0xCA00,0x0108
#define HSDIO_INPUT_SELECT_HDMI_1 0xCA00,0x0109
#define HSDIO_INPUT_SELECT_HDMI_2 0xCA00,0x010A
#define HSDIO_INPUT_SELECT_HDMI_3 0xCA00,0x010B
#define HSDIO_INPUT_SELECT_HDMI_4 0xCA00,0x010C
#define HSDIO_INPUT_SELECT_HDMI_5 0xCA00,0x010D
#define HSDIO_INPUT_SELECT_HDMI_6 0xCA00,0x010E
#define HSDIO_INPUT_SELECT_HDMI_7 0xCA00,0x010F
#define HSDIO_INPUT_SELECT_HDMI_8 0xCA00,0x0110
#define HSDIO_INPUT_SELECT_HDMI_9 0xCA00,0x0111
#define HSDIO_INPUT_SELECT_HDMI_10 0xCA00,0x0112
#define HSDIO_INPUT_SELECT_HDMI_11 0xCA00,0x0113
#define HSDIO_INPUT_SELECT_HDMI_12 0xCA00,0x0114
#define HSDIO_INPUT_SELECT_HDMI_13 0xCA00,0x0115
#define HSDIO_INPUT_SELECT_HDMI_14 0xCA00,0x0116
#define HSDIO_INPUT_SELECT_HDMI_15 0xCA00,0x0117
#define HSDIO_INPUT_SELECT_HDMI_16 0xCA00,0x0118


// ****************************************************************************
// ***** Input Detect Status (2 bytes)
// ****************************************************************************
#define HSDIO_INPUT_DETECT_STS 0xC301,0x0002

#define HSDIO_STS_INPUT_ACTIVE     1
#define HSDIO_STS_INPUT_INACTIVE   0


#define HSDIO_HDMI_INPUT_DETECT_1_msk   0x0001
#define HSDIO_HDMI_INPUT_DETECT_2_msk   0x0002
#define HSDIO_HDMI_INPUT_DETECT_3_msk   0x0004
#define HSDIO_HDMI_INPUT_DETECT_4_msk   0x0008
#define HSDIO_HDMI_INPUT_DETECT_5_msk   0x0010
#define HSDIO_HDMI_INPUT_DETECT_6_msk   0x0020
#define HSDIO_HDMI_INPUT_DETECT_7_msk   0x0040
#define HSDIO_HDMI_INPUT_DETECT_8_msk   0x0080
#define HSDIO_HDMI_INPUT_DETECT_9_msk   0x0100
#define HSDIO_HDMI_INPUT_DETECT_10_msk  0x0200
#define HSDIO_HDMI_INPUT_DETECT_11_msk  0x0400
#define HSDIO_HDMI_INPUT_DETECT_12_msk  0x0800
#define HSDIO_HDMI_INPUT_DETECT_13_msk  0x1000
#define HSDIO_HDMI_INPUT_DETECT_14_msk  0x2000
#define HSDIO_HDMI_INPUT_DETECT_15_msk  0x4000
#define HSDIO_HDMI_INPUT_DETECT_16_msk  0x8000


// ****************************************************************************
// ***** Alert Configuration (4 bytes)
// ****************************************************************************
#define HSDIO_ALERT(nn) 0xCC00,0x0004,(nn&0xffff),(nn>>16)
#define HSDIO_GET_ALERT 0xC400,0x0004
#define HSDIO_ALERT_STS 0xC401,0x0004


#define HSDIO_ALERT_INPUT_AUDIO_MUTE_msk        0x00000001
#define HSDIO_ALERT_INPUT_AUDIO_CHANGE_msk      0x00000002
#define HSDIO_ALERT_INPUT_VIDEO_CHANGE_msk      0x00000004
#define HSDIO_ALERT_INPUT_AVMUTE_CHANGE_msk     0x00000008
#define HSDIO_ALERT_INPUT_SELECT_CHANGE_msk     0x00000010
#define HSDIO_ALERT_INPUT_DETECT_CHANGE_msk     0x00000020
#define HSDIO_ALERT_CEC_CMD_msk                 0x00000040
#define HSDIO_ALERT_INPUT_PKT_CHANGE_msk        0x00000080
#define HSDIO_ALERT_VIDEO_PROC_CTRL_REQ_msk     0x00000100
#define HSDIO_ALERT_OUTPUT_PORT_1_CHANGE_msk    0x01000000
#define HSDIO_ALERT_OUTPUT_PORT_2_CHANGE_msk    0x02000000
#define HSDIO_ALERT_OUTPUT_PORT_3_CHANGE_msk    0x04000000




// for backward compatibility with some older applications...
#define HSDIO_ALERT1(nn) 0xCA00,0x0400+(nn&0xff)
#define HSDIO_GET_ALERT1 0xC200,0x0400
#define HSDIO_ALERT1_STS 0xC201,0x0400
#define HSDIO_STS_ALERT1_AUDIO_MUTE_msk     0x01
#define HSDIO_STS_ALERT1_AUDIO_CHANGE_msk   0x02
#define HSDIO_STS_ALERT1_VIDEO_CHANGE_msk   0x04
#define HSDIO_STS_ALERT1_AVMUTE_CHANGE_msk  0x08


// ****************************************************************************
// ***** Video Processor Control Selection (1 byte)
// ****************************************************************************
#define HSDIO_VIDEO_PROC_CONTROL(nn) 0xCA00,0x0800+(nn&0xff)
#define HSDIO_GET_VIDEO_PROC_CONTROL 0xC200,0x0800
#define HSDIO_VIDEO_PROC_CONTROL_STS 0xC201,0x0800

#define HSDIO_VideoProcControl_VPROC        0
#define HSDIO_VideoProcControl_HOST         1
#define HSDIO_VideoProcControl_UNKNOWN      0xff


// ****************************************************************************
// ***** Number of HDMI Input Ports Status (1 bytes)
// ****************************************************************************
#define HSDIO_NUM_INPUT_HDMI_PORTS_STS 0xC201,0x0A00

// ****************************************************************************
// ***** Repeater Function of HRPTR Status
// ****************************************************************************
#define HSDIO_REPEATER_FUNC_STS 0xC201,0x0B00

#define HSDIO_RepeaterFunc_SINK         0
#define HSDIO_RepeaterFunc_REPEATER     1
#define HSDIO_RepeaterFunc_SOURCE       2
#define HSDIO_RepeaterFunc_UNKNOWN      0xff


// ****************************************************************************
// ***** Preferred Vertical Timing (1 byte)
// ****************************************************************************
#define HSDIO_PREFERRED_VERTICAL_TMG(nn) 0xCA00,0x0C00+(nn&0xff)
#define HSDIO_GET_PREFERRED_VERTICAL_TMG 0xC200,0x0C00

#define HSDIO_PrefVerticalTmg_60        0
#define HSDIO_PrefVerticalTmg_50        1
#define HSDIO_PrefVerticalTmg_UNKNOWN   0xff


// ****************************************************************************
// ***** Suspend Command Processing Until Input Change (1 byte)
// ****************************************************************************
#define HSDIO_SUSPEND_CMDS_UNTIL_INPUT_CHANGE 0xCA00,0x0F00



// ****************************************************************************
// ***** Audio Routing to Host (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_ROUTING(nn) 0xCA00,0x1000+(nn&0xff)
#define HSDIO_GET_AUDIO_ROUTING 0xC200,0x1000
#define HSDIO_AUDIO_ROUTING_STS 0xC201,0x1000

// external input goes to host, external input goes to HSDIO Tx, Host provides HSDIO Tx audio settings
#define HSDIO_AudioRouting_EXTIN_EXTOUT     0

// external input goes to host, host provides audio to HSDIO Tx, Host provides HSDIO Tx audio settings
#define HSDIO_AudioRouting_EXTIN_HOSTOUT    1

// external input goes to host, HSDIO Tx is muted (or disabled)
#define HSDIO_AudioRouting_EXTIN_NOOUT      2

// HSDIO Rx input goes to host, HSDIO Rx audio goes to HSDIO Tx, HSDIO Rx provides HSDIO Tx audio settings
// NOTE: Host cannot control format of audio output to Host from HSDIO Rx
// NOTE: This configuration is known as "Audio Passthru"
#define HSDIO_AudioRouting_HSDIOIN_HSDIOOUT 3

// HSDIO Rx input goes to host, host provides audio to HSDIO Tx, Host provides HSDIO Tx audio settings
#define HSDIO_AudioRouting_HSDIOIN_HOSTOUT  4

// HSDIO Rx input goes to host, HSDIO Tx is muted (or disabled)
#define HSDIO_AudioRouting_HSDIOIN_NOOUT    5


// ****************************************************************************
// ***** Audio Routing to External Output (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_ROUTING_TO_EXTOUT(nn) 0xCA00,0x1200+(nn&0xff)
#define HSDIO_GET_AUDIO_ROUTING_TO_EXTOUT 0xC200,0x1200
#define HSDIO_AUDIO_ROUTING_TO_EXTOUT_STS 0xC201,0x1200

#define HSDIO_AudioRoutingToExtOut_DISABLED 0   // audio output disabled
#define HSDIO_AudioRoutingToExtOut_ENABLED  1   // audio output enabled


// ****************************************************************************
// ***** Repeater Configuration (1 byte)
// ****************************************************************************
#define HSDIO_REPEATER_CONFIG(nn) 0xCA00,0x1300+(nn&0xff)
#define HSDIO_GET_REPEATER_CONFIG 0xC200,0x1300
#define HSDIO_REPEATER_CONFIG_STS 0xC201,0x1300

// Primary output and Secondary output audio enabled
// Audio portion of upstream EDID made from combining audio EDID
//  information from both downstream devices when
//  Audio Routing is HSDIOIN_HSDIOOUT (passthru)
#define HSDIO_RepeaterConfig_PRIM_AUDIO_SEC_AUDIO   0

// Primary output audio enabled, Secondary output audio muted
// Audio portion of upstream EDID made from Primary downstream EDID when
//  Audio Routing is HSDIOIN_HSDIOOUT (passthru)
#define HSDIO_RepeaterConfig_PRIM_AUDIO_SEC_MUTE    1

// Primary output audio muted, Secondary output audio enabled
// Audio portion of upstream EDID made from Secondary downstream EDID when
//  Audio Routing is HSDIOIN_HSDIOOUT (passthru)
#define HSDIO_RepeaterConfig_PRIM_MUTE_SEC_AUDIO    2

// Primary output audio enabled, Secondary output audio enabled
// Audio portion of upstream EDID made from Primary downstream EDID when
//  Audio Routing is HSDIOIN_HSDIOOUT (passthru)
#define HSDIO_RepeaterConfig_PRIM_AUDIO_SEC_FOLLOW  3

// Primary output audio enabled, Secondary output audio enabled
// Audio portion of upstream EDID made from Secondary downstream EDID when
//  Audio Routing is HSDIOIN_HSDIOOUT (passthru)
#define HSDIO_RepeaterConfig_PRIM_FOLLOW_SEC_AUDIO  4

#define HSDIO_RepeaterConfig_UNKNOWN                0xff



// Note: this also determines how EDID data is presented to upstream devices


// ****************************************************************************
// ***** System Configuration Change GO (implement) (1 byte)
// ****************************************************************************
#define HSDIO_SYS_CFG_GO 0xCA00,0x1700


// ****************************************************************************
// ***** Output Timing Selection (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_TIMING(nn) 0xCA00,0x2000+(nn&0xff)
#define HSDIO_GET_OUTPUT_TIMING 0xC200,0x2000
#define HSDIO_OUTPUT_TIMING_STS 0xC201,0x2000

/* Note that not all of these video timing formats are supported by HSDIO */
/* For a list of supported video formats, see the software documentation */
#define HSDIO_VideoTmg_NULL                  0      /**< Not a valid format...        */
#define HSDIO_VideoTmg_NO_CHANGE             0      /**< ...or no change required     */
#define HSDIO_VideoTmg_MIN                   1      /**< Lowest valid format          */
#define HSDIO_VideoTmg_TV_MIN                1      /**< Lowest valid TV format       */
#define HSDIO_VideoTmg_01_640x480p_60Hz      1
#define HSDIO_VideoTmg_02_720x480p_60Hz      2
#define HSDIO_VideoTmg_03_720x480p_60Hz      3
#define HSDIO_VideoTmg_04_1280x720p_60Hz     4
#define HSDIO_VideoTmg_05_1920x1080i_60Hz    5
#define HSDIO_VideoTmg_06_720x480i_60Hz      6
#define HSDIO_VideoTmg_07_720x480i_60Hz      7
#define HSDIO_VideoTmg_08_720x240p_60Hz      8
#define HSDIO_VideoTmg_09_720x240p_60Hz      9
#define HSDIO_VideoTmg_10_2880x480i_60Hz     10
#define HSDIO_VideoTmg_11_2880x480i_60Hz     11
#define HSDIO_VideoTmg_12_2880x240p_60Hz     12
#define HSDIO_VideoTmg_13_2880x240p_60Hz     13
#define HSDIO_VideoTmg_14_1440x480p_60Hz     14
#define HSDIO_VideoTmg_15_1440x480p_60Hz     15
#define HSDIO_VideoTmg_16_1920x1080p_60Hz    16
#define HSDIO_VideoTmg_17_720x576p_50Hz      17
#define HSDIO_VideoTmg_18_720x576p_50Hz      18
#define HSDIO_VideoTmg_19_1280x720p_50Hz     19
#define HSDIO_VideoTmg_20_1920x1080i_50Hz    20
#define HSDIO_VideoTmg_21_720x576i_50Hz      21
#define HSDIO_VideoTmg_22_720x576i_50Hz      22
#define HSDIO_VideoTmg_23_720x288p_50Hz      23
#define HSDIO_VideoTmg_24_720x288p_50Hz      24
#define HSDIO_VideoTmg_25_2880x576i_50Hz     25
#define HSDIO_VideoTmg_26_2880x576i_50Hz     26
#define HSDIO_VideoTmg_27_2880x288p_50Hz     27
#define HSDIO_VideoTmg_28_2880x288p_50Hz     28
#define HSDIO_VideoTmg_29_1440x576p_50Hz     29
#define HSDIO_VideoTmg_30_1440x576p_50Hz     30
#define HSDIO_VideoTmg_31_1920x1080p_50Hz    31
#define HSDIO_VideoTmg_32_1920x1080p_24Hz    32
#define HSDIO_VideoTmg_33_1920x1080p_25Hz    33
#define HSDIO_VideoTmg_34_1920x1080p_30Hz    34
#define HSDIO_VideoTmg_35_2880x480p_60Hz     35
#define HSDIO_VideoTmg_36_2880x480p_60Hz     36
#define HSDIO_VideoTmg_37_2880x576p_50Hz     37
#define HSDIO_VideoTmg_38_2880x576p_50Hz     38
#define HSDIO_VideoTmg_39_1920x1080i_50Hz    39     /* 1250 total */
#define HSDIO_VideoTmg_40_1920x1080i_100Hz   40
#define HSDIO_VideoTmg_41_1280x720p_100Hz    41
#define HSDIO_VideoTmg_42_720x576p_100Hz     42
#define HSDIO_VideoTmg_43_720x576p_100Hz     43
#define HSDIO_VideoTmg_44_720x576i_100Hz     44
#define HSDIO_VideoTmg_45_720x576i_100Hz     45
#define HSDIO_VideoTmg_46_1920x1080i_120Hz   46
#define HSDIO_VideoTmg_47_1280x720p_120Hz    47
#define HSDIO_VideoTmg_48_720x480p_120Hz     48
#define HSDIO_VideoTmg_49_720x480p_120Hz     49
#define HSDIO_VideoTmg_50_720x480i_120Hz     50
#define HSDIO_VideoTmg_51_720x480i_120Hz     51
#define HSDIO_VideoTmg_52_720x576p_200Hz     52
#define HSDIO_VideoTmg_53_720x576p_200Hz     53
#define HSDIO_VideoTmg_54_720x576i_200Hz     54
#define HSDIO_VideoTmg_55_720x576i_200Hz     55
#define HSDIO_VideoTmg_56_720x480p_240Hz     56
#define HSDIO_VideoTmg_57_720x480p_240Hz     57
#define HSDIO_VideoTmg_58_720x480i_240Hz     58
#define HSDIO_VideoTmg_59_720x480i_240Hz     59
#define HSDIO_VideoTmg_60_1280x720p_24Hz     60
#define HSDIO_VideoTmg_61_1280x720p_25Hz     61
#define HSDIO_VideoTmg_62_1280x720p_30Hz     62
#define HSDIO_VideoTmg_63_1920x1080p_120Hz   63
#define HSDIO_VideoTmg_64_1920x1080p_100Hz   64
#define HSDIO_VideoTmg_TV_MAX                64     /**< Highest valid TV format      */
#define HSDIO_VideoTmg_TV_NUM                65     /**< Number of TV formats & null  */
#define HSDIO_VideoTmg_PC_MIN                128    /**< Lowest valid PC format       */
#define HSDIO_VideoTmg_PC_640x480p_60Hz      128
#define HSDIO_VideoTmg_PC_800x600p_60Hz      129
#define HSDIO_VideoTmg_PC_1152x960p_60Hz     130
#define HSDIO_VideoTmg_PC_1024x768p_60Hz     131
#define HSDIO_VideoTmg_PC_1280x768p_60Hz     132
#define HSDIO_VideoTmg_PC_1280x1024p_60Hz    133
#define HSDIO_VideoTmg_PC_1360x768p_60Hz     134
#define HSDIO_VideoTmg_PC_1400x1050p_60Hz    135
#define HSDIO_VideoTmg_PC_1600x1200p_60Hz    136
#define HSDIO_VideoTmg_PC_1024x768p_70Hz     137
#define HSDIO_VideoTmg_PC_640x480p_72Hz      138
#define HSDIO_VideoTmg_PC_800x600p_72Hz      139
#define HSDIO_VideoTmg_PC_640x480p_75Hz      140
#define HSDIO_VideoTmg_PC_1024x768p_75Hz     141
#define HSDIO_VideoTmg_PC_800x600p_75Hz      142
#define HSDIO_VideoTmg_PC_1024x864p_75Hz     143
#define HSDIO_VideoTmg_PC_1280x1024p_75Hz    144
#define HSDIO_VideoTmg_PC_640x350p_85Hz      145
#define HSDIO_VideoTmg_PC_640x400p_85Hz      146
#define HSDIO_VideoTmg_PC_720x400p_85Hz      147
#define HSDIO_VideoTmg_PC_640x480p_85Hz      148
#define HSDIO_VideoTmg_PC_800x600p_85Hz      149
#define HSDIO_VideoTmg_PC_1024x768p_85Hz     150
#define HSDIO_VideoTmg_PC_1152x864p_85Hz     151
#define HSDIO_VideoTmg_PC_1280x960p_85Hz     152
#define HSDIO_VideoTmg_PC_1280x1024p_85Hz    153
#define HSDIO_VideoTmg_PC_1024x768i_87Hz     154
#define HSDIO_VideoTmg_PC_800x600p_56Hz      155
#define HSDIO_VideoTmg_PC_1152x864p_70Hz     156
#define HSDIO_VideoTmg_PC_1152x864p_75Hz     157
#define HSDIO_VideoTmg_PC_1280x960p_60Hz     158
#define HSDIO_VideoTmg_PC_1280x960p_75Hz     159
#define HSDIO_VideoTmg_PC_MAX                159    /**< Highest valid PC format      */
#define HSDIO_VideoTmg_PC_NUM        (1+(159-128))  /**< Number of PC formats         */
#define HSDIO_VideoTmg_FORCE_PASS_THROUGH    0xFC   /**< Pass Through the input timing to output */
#define HSDIO_VideoTmg_PASS_THROUGH          0xFD   /**< Pass Through the input timing to output if supported by output */
#define HSDIO_VideoTmg_AUTO                  0xFE   /**< Automatic selection          */
#define HSDIO_VideoTmg_UNSUPPORTED           0xFF


// ****************************************************************************
// ***** Output Resolution Status (2 bytes)
// ****************************************************************************
// horizontal resolution (size)
#define HSDIO_OUTPUT_H_RES_STS 0xC301,0x0022
// vertical resolution (size)
#define HSDIO_OUTPUT_V_RES_STS 0xC301,0x0024


// ****************************************************************************
// ***** Audio Master Clock (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_MCLK_TO_HOST(nn) 0xCA00,0x3000+(nn&0xff)
#define HSDIO_GET_AUDIO_MCLK_TO_HOST 0xC200,0x3000
#define HSDIO_AUDIO_MCLK_TO_HOST_STS 0xC201,0x3000

#define HSDIO_AudioMClk_AUTO    0
#define HSDIO_AudioMClk_64X     2
#define HSDIO_AudioMClk_128X    1
#define HSDIO_AudioMClk_256X    3
#define HSDIO_AudioMClk_384X    4
#define HSDIO_AudioMClk_512X    5
#define HSDIO_AudioMClk_UNKNOWN 0xff


// ****************************************************************************
// ***** Audio Format Output to Host Cmd/Sts (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_FORMAT_TO_HOST(nn) 0xCA00,0x3100+(nn&0xff)
#define HSDIO_GET_AUDIO_FORMAT_TO_HOST 0xC200,0x3100
#define HSDIO_AUDIO_FORMAT_TO_HOST_STS 0xC201,0x3100

#define HSDIO_AudioFmt_SPDIF    0
#define HSDIO_AudioFmt_I2S      1
#define HSDIO_AudioFmt_DSD      2       // also known as one bit audio
#define HSDIO_AudioFmt_DST      3
#define HSDIO_AudioFmt_HBR      4
#define HSDIO_AudioFmt_UNKNOWN  0xff


// ****************************************************************************
// ***** Audio Sample Size to Host Cmd/Sts (1 byte)
// ****************************************************************************
//#define HSDIO_AUDIO_SAMPLE_SIZE_TO_HOST(nn) 0xCA00,0x3200+(nn&0xff)
//#define HSDIO_GET_AUDIO_SAMPLE_SIZE_TO_HOST 0xC200,0x3200
#define HSDIO_AUDIO_SAMPLE_SIZE_TO_HOST_STS 0xC201,0x3200

#define HSDIO_AudioSampleSize_RESERVED  0
#define HSDIO_AudioSampleSize_16        1
#define HSDIO_AudioSampleSize_20        2
#define HSDIO_AudioSampleSize_24        3
#define HSDIO_AudioSampleSize_32        4
#define HSDIO_AudioSampleSize_UNKNOWN   0xff


// ****************************************************************************
// ***** Audio UnMute Delay Cmd/Sts (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_UNMUTE_DELAY_TO_HOST(nn) 0xCA00,0x3300+(nn&0xff)
#define HSDIO_GET_AUDIO_UNMUTE_DELAY_TO_HOST 0xC200,0x3300
#define HSDIO_AUDIO_UNMUTE_DELAY_TO_HOST_STS 0xC201,0x3300

#define HSDIO_AudioUnMuteDelay_NO_DELAY         0
#define HSDIO_AudioUnMuteDelay_SHORT_DELAY      1
#define HSDIO_AudioUnMuteDelay_MEDIUM_DELAY     2
#define HSDIO_AudioUnMuteDelay_LONG_DELAY       3
#define HSDIO_AudioUnMuteDelay_UNKNOWN          0xff


// ****************************************************************************
// ***** Implement audio commands (causes audio format, sample size, unmute delay, and force 128x MClk to be implemented)
// ****************************************************************************
#define HSDIO_IMPLEMENT_AUDIO_TO_HOST_CMDS 0xCA00,0x3700


// ****************************************************************************
// ***** Audio Present in Stream Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_PRESENT_STS 0xC201,0x3800

#define HSDIO_AudioPresent_HAS_NO_AUDIO   0
#define HSDIO_AudioPresent_HAS_AUDIO      1
#define HSDIO_AudioPresent_UNKNOWN        0xff


// ****************************************************************************
// ***** Audio Error Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_ERROR_STS 0xC201,0x3900

#define HSDIO_AudioErr_NO_ERROR             0
#define HSDIO_AudioErr_AUDIO_FIFO_FAILURE   1
#define HSDIO_AudioErr_AUDIO_NO_VIDEO_SYNC  2
#define HSDIO_AudioErr_AUDIO_AVMUTED        3
#define HSDIO_AudioErr_AUDIO_NO_PLL_LOCK    4
#define HSDIO_AudioErr_AUDIO_NO_AUDIO       5
#define HSDIO_AudioErr_UNKNOWN              0xff


// ****************************************************************************
// ***** Sample Frequency Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_FREQ_STS 0xC201,0x3A00

#define HSDIO_AudioFreq_RESERVED    0
#define HSDIO_AudioFreq_32K         1
#define HSDIO_AudioFreq_44_1K       2
#define HSDIO_AudioFreq_48K         3
#define HSDIO_AudioFreq_88_2K       4
#define HSDIO_AudioFreq_96K         5
#define HSDIO_AudioFreq_176_4K      6
#define HSDIO_AudioFreq_192K        7
#define HSDIO_AudioFreq_UNKNOWN     0xff


// ****************************************************************************
// ***** Layout Status  (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_LAYOUT_STS 0xC201,0x3B00

#define HSDIO_AudioLayout_2         0         /* 2 channels */
#define HSDIO_AudioLayout_MULTIPLE  1         /* up to 8 channels */
#define HSDIO_AudioLayout_UNKNOWN   0xff


// ****************************************************************************
// ***** Master Clock Frequency Status (1 byte)
// ****************************************************************************
//#define HSDIO_AUDIO_INPUT_MASTER_CLK_STS 0xC201,0x3C00

//#define HSDIO_AudioMstrClk_AUTO     0
//#define HSDIO_AudioMstrClk_64X      1
//#define HSDIO_AudioMstrClk_128X     2
//#define HSDIO_AudioMstrClk_256X     3
//#define HSDIO_AudioMstrClk_384X     4
//#define HSDIO_AudioMstrClk_512X     5
//#define HSDIO_AudioMstrClk_UNKNOWN  0xff


// ****************************************************************************
// ***** Sample Packet Type Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_SAMPLE_PACKET_STS 0xC201,0x3C00

#define HSDIO_AudioSamplePacket_PCM_OR_COMPRESSED   1
#define HSDIO_AudioSamplePacket_HIGH_BIT_RATE       2
#define HSDIO_AudioSamplePacket_DSD                 3
#define HSDIO_AudioSamplePacket_DST                 4
#define HSDIO_AudioSamplePacket_UNKNOWN             0xff


// ****************************************************************************
// ***** Internal Mute Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_INTERNAL_MUTE_STS 0xC201,0x3D00

#define HSDIO_AudioMute_OFF         0
#define HSDIO_AudioMute_ON          1
#define HSDIO_AudioMute_UNKNOWN     0xff


// ****************************************************************************
// NOTE: Aux Info is also known as Audio Infoframe data from CEA-861-D
// ****************************************************************************

// ****************************************************************************
// ***** Aux Info: Information Received Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_AUX_INFO_RECEIVED_STS 0xC201,0x4000

#define HSDIO_AudioAuxInfo_RECEIVED      1
#define HSDIO_AudioAuxInfo_NOT_RECEIVED  0


// ****************************************************************************
// ***** Aux Info: Coding Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_AUX_CODING_STS 0xC201,0x4100

/* // values specified in CEA861E  section 6.6.1, CT0-CT3 */
/* // NOTE: data is right justified within status byte from HSDIO */


// ****************************************************************************
// ***** Aux Info: Channel Count Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_AUX_CHANNEL_CNT_STS 0xC201,0x4200

/* // values specified in CEA861E, section 6.6.1, CC0-CC2 */
/* // NOTE: data is right justified within status byte from HSDIO */


// ****************************************************************************
// ***** Aux Info: Sampling Frequency Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_AUX_SAMPLING_FREQ_STS 0xC201,0x4300

/* // values specified in CEA861E, section 6.6.1, SF0-SF2 */
/* // NOTE: data is right justified within status byte from HSDIO */


// ****************************************************************************
// ***** Aux Info: Sample Size Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_AUX_SAMPLE_SIZE_STS 0xC201,0x4400

/* // values specified in CEA861E, section 6.6.1, SS0-SS1 */
/* // NOTE: data is right justified within status byte from HSDIO */


// ****************************************************************************
// ***** Aux Info: Speaker Mapping Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_AUX_SPEAKER_MAPPING_STS 0xC201,0x4500

/* // values specified in CEA861E, section 6.6.2 */


// ****************************************************************************
// ***** Aux Info: Level Shift Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_AUX_LEVEL_SHIFT_STS 0xC201,0x4600

/* // values specified in CEA861E, section 6.6.2, LSV0-LSV3 */
/* // NOTE: data is right justified within status byte from HSDIO */


// ****************************************************************************
// ***** Aux Info: Downmix Inhibit Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_AUX_DOWNMIX_INHIBIT_STS 0xC201,0x4700

/* // values specified in CEA861E, section 6.6.2, DM_INH */
/* // NOTE: data is right justified within status byte from HSDIO */


// ****************************************************************************
// ***** Aux Info: Extended Coding Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_AUX_EXT_CODING_STS 0xC201,0x4800

/* // values specified in CEA861E, section 6.6.1, CXT0-CXT4 */
/* // NOTE: data is right justified within status byte from HSDIO */


// ****************************************************************************
// ***** Aux Info: LFE Playback Level Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_AUX_LFE_PLAYBACK_LEVEL_STS 0xC201,0x4900

/* // values specified in CEA861E, section 6.6.2, LFEPBL0-LFEPBL1 */
/* // NOTE: data is right justified within status byte from HSDIO */



// ****************************************************************************
// ***** Input Channel Status Data
// ****************************************************************************

// ****************************************************************************
// ***** Channel Status: Information Received
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_CHSTS_INFO_RECEIVED_STS 0xC201,0x5000

#define HSDIO_AudioChSts_RECEIVED      1
#define HSDIO_AudioChSts_NOT_RECEIVED  0

// ****************************************************************************
// ***** Channel Status: Raw Data Status
// ****************************************************************************
#define HSDIO_AUDIO_INPUT_CHSTS_0_STS 0xC201,0x5100
#define HSDIO_AUDIO_INPUT_CHSTS_1_STS 0xC201,0x5200
#define HSDIO_AUDIO_INPUT_CHSTS_2_STS 0xC201,0x5300
#define HSDIO_AUDIO_INPUT_CHSTS_3_STS 0xC201,0x5400
#define HSDIO_AUDIO_INPUT_CHSTS_4_STS 0xC201,0x5500
#define HSDIO_AUDIO_INPUT_CHSTS_5_STS 0xC201,0x5600

#define HSDIO_AUDIO_INPUT_CHSTS_SIZE 6
#define HSDIO_AUDIO_INPUT_CHSTS_STS \
        0xC506,0x0001,0x0051,HSDIO_AUDIO_INPUT_CHSTS_SIZE







/* ************************************************************************************************ */
/* // ***** Rx Input Packets                                                                        */
/* ************************************************************************************************ */

/* Input Packet Change */
#define HSDIO_INPUT_PKT_CHANGE_STS 0xC405,0x0010
/* bit masks */
#define HSDIO_INPUT_PKT_CHANGE_AVI_msk          0x00000001
#define HSDIO_INPUT_PKT_CHANGE_AIF_msk          0x00000002
#define HSDIO_INPUT_PKT_CHANGE_ACP_msk          0x00000004
#define HSDIO_INPUT_PKT_CHANGE_ISRC1_msk        0x00000008
#define HSDIO_INPUT_PKT_CHANGE_ISRC2_msk        0x00000010
#define HSDIO_INPUT_PKT_CHANGE_SPD_msk          0x00000020
#define HSDIO_INPUT_PKT_CHANGE_GBD_msk          0x00000040



/* Read all Valid status flags in a single alpha command */
#define HSDIO_INPUT_PKT_VALID_SIZE 8
#define HSDIO_INPUT_PKT_VALID_STS \
        0xC506,0x0005,0x0000,HSDIO_INPUT_PKT_VALID_SIZE

#define HSDIO_INPUT_PKT_VALID_AVI_ndx       0
#define HSDIO_INPUT_PKT_VALID_AIF_ndx       1
#define HSDIO_INPUT_PKT_VALID_ACP_ndx       2
#define HSDIO_INPUT_PKT_VALID_ISRC1_ndx     3
#define HSDIO_INPUT_PKT_VALID_ISRC2_ndx     4
#define HSDIO_INPUT_PKT_VALID_SPD_ndx       5
#define HSDIO_INPUT_PKT_VALID_GBD_ndx       6


/* ************************************************************************************************ */
/* ***** Auxilliary Video Information (AVI) */
/* ************************************************************************************************ */
#define HSDIO_INPUT_PKT_AVI_SIZE 16

#define HSDIO_INPUT_PKT_AVI_VALID_STS 0xC205,0x0000

#define HSDIO_INPUT_PKT_AVI_STS \
        0xC506,0x0005,0x0020,HSDIO_INPUT_PKT_AVI_SIZE

/* ************************************************************************************************ */
/* ***** Audio Info Frame (AIF) */
/* ************************************************************************************************ */
#define HSDIO_INPUT_PKT_AIF_SIZE 14

#define HSDIO_INPUT_PKT_AIF_VALID_STS 0xC205,0x0100

#define HSDIO_INPUT_PKT_AIF_STS \
        0xC506,0x0005,0x0040,HSDIO_INPUT_PKT_AIF_SIZE

/* ************************************************************************************************ */
/* ***** Audio Content Protection (ACP) */
/* ************************************************************************************************ */
#define HSDIO_INPUT_PKT_ACP_SIZE 32

#define HSDIO_INPUT_PKT_ACP_VALID_STS 0xC205,0x0200

#define HSDIO_INPUT_PKT_ACP_STS \
        0xC506,0x0005,0x0060,HSDIO_INPUT_PKT_ACP_SIZE

/* ************************************************************************************************ */
/* ***** International Standard Recording Code 1 (ISRC1) */
/* ************************************************************************************************ */
#define HSDIO_INPUT_PKT_ISRC1_SIZE 32

#define HSDIO_INPUT_PKT_ISRC1_VALID_STS 0xC205,0x0300

#define HSDIO_INPUT_PKT_ISRC1_STS \
        0xC506,0x0005,0x0080,HSDIO_INPUT_PKT_ISRC1_SIZE

/* ************************************************************************************************ */
/* ***** International Standard Recording Code 2 (ISRC2) */
/* ************************************************************************************************ */
#define HSDIO_INPUT_PKT_ISRC2_SIZE 32

#define HSDIO_INPUT_PKT_ISRC2_VALID_STS 0xC205,0x0400

#define HSDIO_INPUT_PKT_ISRC2_STS \
        0xC506,0x0005,0x00A0,HSDIO_INPUT_PKT_ISRC2_SIZE

/* ************************************************************************************************ */
/* ***** Source Product Descriptor (SPD) */
/* ************************************************************************************************ */
#define HSDIO_INPUT_PKT_SPD_SIZE 28

#define HSDIO_INPUT_PKT_SPD_VALID_STS 0xC205,0x0500

#define HSDIO_INPUT_PKT_SPD_STS \
        0xC506,0x0005,0x00C0,HSDIO_INPUT_PKT_SPD_SIZE

/* ************************************************************************************************ */
/* ***** Gamut Metadata Boundary description (GBD) */
/* ************************************************************************************************ */
#define HSDIO_INPUT_PKT_GBD_SIZE 32

#define HSDIO_INPUT_PKT_GBD_VALID_STS 0xC205,0x0600

#define HSDIO_INPUT_PKT_GBD_STS \
        0xC506,0x0005,0x00E0,HSDIO_INPUT_PKT_GBD_SIZE








// ****************************************************************************
// ***** Audio Output Configuration
// ****************************************************************************

// ****************************************************************************
// ***** Audio Output Valid going to HSDIO (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_PRESENT(nn) 0xCA00,0x6000+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_PRESENT 0xC200,0x6000

// data same as definitions HSDIO_AudioPresent_xxx above


// ****************************************************************************
// ***** Audio Output Format to HSDIO
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_FORMAT(nn) 0xCA00,0x6100+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_FORMAT 0xC200,0x6100

// data same as definitions HSDIO_AudioFmt_xxx above


// ****************************************************************************
// ***** Audio Output Sample Size to HSDIO Cmd/Sts (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_SAMPLE_SIZE(nn) 0xCA00,0x6200+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_SAMPLE_SIZE 0xC200,0x6200

// data same as definitions HSDIO_AudioSampleSize_xxx above


// ****************************************************************************
// ***** Audio Output Frequency to HSDIO (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_FREQ(nn) 0xCA00,0x6300+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_FREQ 0xC200,0x6300

// data same as definitions HSDIO_AudioFreq_xxx above


// ****************************************************************************
// ***** Audio Output Layout to HSDIO (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_LAYOUT(nn) 0xCA00,0x6400+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_LAYOUT 0xC200,0x6400

// data same as definitions HSDIO_AudioLayout_xxx above


// ****************************************************************************
// ***** Audio Output Sample Packet Type to HSDIO (1 byte)
// ****************************************************************************
// NOTE: THIS COMMAND IS NO LONGER SUPPORTED AND WILL BE DELETED IN THE NEXT RELEASE
#define HSDIO_AUDIO_OUTPUT_SAMPLE_PACKET(nn) 0xCA00,0x6500+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_SAMPLE_PACKET 0xC200,0x6500

// data same as definitions HSDIO_AudioSamplePacket_xxx above


// ****************************************************************************
// ***** Audio Output Sample Packet Type to HSDIO (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_MCLK(nn) 0xCA00,0x6600+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_MCLK 0xC200,0x6600

// data same as definitions HSDIO_AudioMClk_xxx above.


// ****************************************************************************
// NOTE: Aux Info is also known as Audio Infoframe data from CEA-861-D
// ****************************************************************************

// ****************************************************************************
// ***** Aux Info: Coding Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_CODING(nn) 0xCA00,0x6800+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_AUX_CODING 0xC200,0x6800

/* // values specified in CEA861D  section 6.6.1, CT0-CT3 */
/* // NOTE: data should be right justified within command byte to HSDIO */


// ****************************************************************************
// ***** Aux Info: Channel Count (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_CHANNEL_CNT(nn) 0xCA00,0x6900+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_AUX_CHANNEL_CNT 0xC200,0x6900

/* // values specified in CEA861D, section 6.6.1, CC0-CC2 */
/* // NOTE: data should be right justified within command byte to HSDIO */


// ****************************************************************************
// ***** Aux Info: Sampling Frequency (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_SAMPLING_FREQ(nn) 0xCA00,0x6A00+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_AUX_SAMPLING_FREQ 0xC200,0x6A00

/* // values specified in CEA861D, section 6.6.1, SF0-SF2 */
/* // NOTE: data should be right justified within command byte to HSDIO */


// ****************************************************************************
// ***** Aux Info: Sample Size (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_SAMPLE_SIZE(nn) 0xCA00,0x6B00+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_AUX_SAMPLE_SIZE 0xC200,0x6B00

/* // values specified in CEA861D, section 6.6.1, SS0-SS1 */
/* // NOTE: data should be right justified within command byte to HSDIO */


// ****************************************************************************
// ***** Aux Info: Speaker Mapping (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_SPEAKER_MAPPING(nn) 0xCA00,0x6C00+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_AUX_SPEAKER_MAPPING 0xC200,0x6C00

/* // values specified in CEA861D, section 6.6.2 */


// ****************************************************************************
// ***** Aux Info: Level Shift  (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_LEVEL_SHIFT(nn) 0xCA00,0x6D00+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_AUX_LEVEL_SHIFT 0xC200,0x6D00

/* // values specified in CEA861D, section 6.6.2, LSV0-LSV3 */
/* // NOTE: data should be right justified within command byte to HSDIO */


// ****************************************************************************
// ***** Aux Info: Downmix Inhibit  (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_DOWNMIX_INHIBIT(nn) 0xCA00,0x6E00+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_AUX_DOWNMIX_INHIBIT 0xC200,0x6E00

/* // values specified in CEA861D, section 6.6.2, DM_INH */
/* // NOTE: data should be right justified within command byte to HSDIO */


// ****************************************************************************
// ***** Aux Info: Extended Coding Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_EXT_CODING(nn) 0xCA00,0x6F00+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_AUX_EXT_CODING 0xC200,0x6F00

/* // values specified in CEA861E  section 6.6.1, CXT0-CXT4 */
/* // NOTE: data should be right justified within command byte to HSDIO */


// ****************************************************************************
// ***** Aux Info: LFE Playback Level  (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_LFE_PLAYBACK_LVL(nn) 0xCA00,0x7000+(nn&0xff)
#define HSDIO_GET_AUDIO_OUTPUT_AUX_LFE_PLAYBACK_LVL 0xC200,0x7000

/* // values specified in CEA861E, section 6.6.2, LFEPBL0-LFEPBL1 */
/* // NOTE: data should be right justified within command byte to HSDIO */



// ****************************************************************************
// ***** Output Channel Status Data
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_CHSTS_PCM_PREEMPHASIS(nn) 0xCA00,0x7800+(nn&0xff)
#define HSDIO_AUDIO_OUTPUT_CHSTS_COPYRIGHT(nn) 0xCA00,0x7900+(nn&0xff)
#define HSDIO_AUDIO_OUTPUT_CHSTS_CATEGORY_CODE(nn) 0xCA00,0x7A00+(nn&0xff)
#define HSDIO_AUDIO_OUTPUT_CHSTS_WORD_LENGTH(nn) 0xCA00,0x7B00+(nn&0xff)

#define HSDIO_GET_AUDIO_OUTPUT_CHSTS_PCM_PREEMPHASIS 0xC200,0x7800
#define HSDIO_GET_AUDIO_OUTPUT_CHSTS_COPYRIGHT 0xC200,0x7900
#define HSDIO_GET_AUDIO_OUTPUT_CHSTS_CATEGORY_CODE 0xC200,0x7A00
#define HSDIO_GET_AUDIO_OUTPUT_CHSTS_WORD_LENGTH 0xC200,0x7B00


#define HSDIO_CSFormatInfo_PCM_NO_PRE   0
#define HSDIO_CSFormatInfo_PCM_PRE      1
#define HSDIO_CSFormatInfo_NOTPCM       4

#define HSDIO_CSCopyright_PROTECTED     0
#define HSDIO_CSCopyright_UNPROTECTED   1

#define HSDIO_CSWordLength_MAX_20       0x0
#define HSDIO_CSWordLength_MAX_24       0x1
#define HSDIO_CSWordLength_16           0x2
#define HSDIO_CSWordLength_17           0xC
#define HSDIO_CSWordLength_18           0x4
#define HSDIO_CSWordLength_19           0x8
#define HSDIO_CSWordLength_20           0xA
#define HSDIO_CSWordLength_21           0xD
#define HSDIO_CSWordLength_22           0x5
#define HSDIO_CSWordLength_23           0x9
#define HSDIO_CSWordLength_24           0xB


// ****************************************************************************
// ***** Audio Output Configuration GO (tells HSDIO to process all audio output parameters)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_GO 0xCA00,0x7F00



// ****************************************************************************
// ***** Input Configuration
// ****************************************************************************

// ****************************************************************************
// ***** Input Sync Status
// ****************************************************************************
#define HSDIO_INPUT_SYNC_STS 0xC201,0x9000

#define HSDIO_InSync_SYNC_LOSS        0
#define HSDIO_InSync_SYNC_DETECTED    1
#define HSDIO_InSync_UNKNOWN          0xff


// ****************************************************************************
// ***** Input Timing Status
// ****************************************************************************
#define HSDIO_INPUT_TIMING_STS 0xC201,0x9100

// data same as definitions HSDIO_VideoTmg_xxx above


// ****************************************************************************
// ***** Input Copy Protection Status
// ****************************************************************************
#define HSDIO_INPUT_COPY_PROTECTION_STS 0xC201,0x9200

#define HSDIO_CopyProtection_NONE           0
#define HSDIO_CopyProtection_HDCP           1
#define HSDIO_CopyProtection_MACROVISION    2
#define HSDIO_CopyProtection_AUTO           128
#define HSDIO_CopyProtection_ON             129
#define HSDIO_CopyProtection_UNKNOWN        0xff


// ****************************************************************************
// ***** Input Source Type Status
// ****************************************************************************
#define HSDIO_INPUT_SOURCE_TYPE_STS 0xC201,0x9300

#define HSDIO_SourceType_DVI            1
#define HSDIO_SourceType_HDMI           2
#define HSDIO_SourceType_COMPONENT      3
#define HSDIO_SourceType_VGA            4
#define HSDIO_Sourcetype_COMPOSITE      5
#define HSDIO_SourceType_SVIDEO         6
#define HSDIO_SourceType_UNKNOWN        0xff


// ****************************************************************************
// ***** Input Color Space Status
// ****************************************************************************
#define HSDIO_INPUT_COLOR_SPACE_STS 0xC201,0x9400

#define HSDIO_ColorSpace_AUTO       0
#define HSDIO_ColorSpace_RGB        1
#define HSDIO_ColorSpace_YCbCr      2
#define HSDIO_ColorSpace_XVYCC      3
#define HSDIO_ColorSpace_UNKNOWN    0xff


// ****************************************************************************
// ***** Input Color Space Detailed Status
// ****************************************************************************
#define HSDIO_INPUT_COLOR_SPACE_DETAILED_STS 0xC201,0x9500

#define HSDIO_ColorSpaceDtl_AUTO        0
#define HSDIO_ColorSpaceDtl_RGB_FULL    1
#define HSDIO_ColorSpaceDtl_RGB_LIMITED 2
#define HSDIO_ColorSpaceDtl_YCbCr_709   3
#define HSDIO_ColorSpaceDtl_YCbCr_601   4
#define HSDIO_ColorSpaceDtl_XVYCC_709   5
#define HSDIO_ColorSpaceDtl_XVYCC_601   6
#define HSDIO_ColorSpaceDtl_UNKNOWN     0xff


// ****************************************************************************
// ***** Input Color Depth Status
// ****************************************************************************
#define HSDIO_INPUT_COLOR_DEPTH_STS 0xC201,0x9600

#define HSDIO_ColorDepth_AUTO       0
#define HSDIO_ColorDepth_8_BIT      8
#define HSDIO_ColorDepth_10_BIT     10
#define HSDIO_ColorDepth_12_BIT     12
#define HSDIO_ColorDepth_16_BIT     16
#define HSDIO_ColorDepth_NA         0xfe    // not applicable
#define HSDIO_ColorDepth_UNKNOWN    0xff


// ****************************************************************************
// ***** Input Video Mode Status
// ****************************************************************************
#define HSDIO_INPUT_VIDEO_MODE_STS 0xC201,0x9700

#define HSDIO_VideoMode_AUTO        0
#define HSDIO_VideoMode_656         1
#define HSDIO_VideoMode_444         2
#define HSDIO_VideoMode_422         3
#define HSDIO_VideoMode_NA          0xfe    // not applicable
#define HSDIO_VideoMode_UNKNOWN     0xff


// ****************************************************************************
// ***** Input AV Mute Status
// ****************************************************************************
#define HSDIO_INPUT_AVMUTE_STS 0xC201,0x9800

#define HSDIO_AvMute_OFF       0
#define HSDIO_AvMute_ON        1
#define HSDIO_AvMute_UNKNOWN   0xff


// ****************************************************************************
// ***** Input EQ Setting (1 byte)
// ****************************************************************************
#define HSDIO_INPUT_EQ_SETTING(nn) 0xCA00,0x9A00+(nn&0xff)
#define HSDIO_GET_INPUT_EQ_SETTING 0xC200,0x9A00

#define HSDIO_EqSetting_AUTO        0
#define HSDIO_EqSetting_LO          1
#define HSDIO_EqSetting_MED_LO      2
#define HSDIO_EqSetting_MED         3
#define HSDIO_EqSetting_MED_HI      4
#define HSDIO_EqSetting_HI          5
#define HSDIO_EqSetting_UNKNOWN     0xff


// ****************************************************************************
// ***** Input Pixel Repetition Status
// ****************************************************************************
#define HSDIO_INPUT_PIXEL_REP_STS 0xC201,0x9B00

/* 0=no repetition, 1=2x, 2=3x, 3=4x etc. */


// ****************************************************************************
// ***** Video Processing Configuration
// ****************************************************************************

// ****************************************************************************
// ***** Display Mode (1 byte)
// ****************************************************************************
// was VIDPROC_DISPLAY_MODE
#define HSDIO_DISPLAY_MODE(nn) 0xCA00,0xA000+(nn&0xff)
#define HSDIO_GET_DISPLAY_MODE 0xC200,0xA000
#define HSDIO_DISPLAY_MODE_STS 0xC201,0xA000

#define HSDIO_ImageDisplayMode_OFF          0
#define HSDIO_ImageDisplayMode_LIVE         1
#define HSDIO_ImageDisplayMode_FREEZE_FRAME 2
#define HSDIO_ImageDisplayMode_FLAT_FRAME   3
#define HSDIO_ImageDisplayMode_UNKNOWN      0xff


// ****************************************************************************
// ***** Aspect Ratio (1 byte)
// ****************************************************************************
#define HSDIO_ASPECT_RATIO(nn) 0xCA00,0xA200+(nn&0xff)
#define HSDIO_GET_ASPECT_RATIO 0xC200,0xA200
#define HSDIO_ASPECT_RATIO_STS 0xC201,0xA200

#define HSDIO_AspectRatio_ANAMORPHIC    0   // linear horizontal stretch
#define HSDIO_AspectRatio_PANAROMIC     1   // LR sides stretched more than middle
#define HSDIO_AspectRatio_LETTERBOX     2   // keep aspect ratio with bars as necessary
#define HSDIO_AspectRatio_EXTRACT       3   // image zoomed to remove bars
#define HSDIO_AspectRatio_THROUGH       4   // image passes through without scaling


// ****************************************************************************
// ***** Aspect Ratio Pan/Tilt/Crop/Zoom settings (2 bytes)
// ****************************************************************************
#define HSDIO_CROPZOOM_HORIZONTAL_SIZE(nn) 0xCB00,0x00A3,0x0000+(nn)    // data range 1 to 100, nominal 100
#define HSDIO_GET_CROPZOOM_HORIZONTAL_SIZE 0xC300,0x00A3
#define HSDIO_CROPZOOM_HORIZONTAL_SIZE_STS 0xC301,0x00A3

#define HSDIO_CROPZOOM_VERTICAL_SIZE(nn) 0xCB00,0x00A5,0x0000+(nn)  // data range 1 to 100, nominal 100
#define HSDIO_GET_CROPZOOM_VERTICAL_SIZE 0xC300,0x00A5
#define HSDIO_CROPZOOM_VERTICAL_SIZE_STS 0xC301,0x00A5

#define HSDIO_PAN_HORIZONTAL_POS(nn) 0xCB00,0x00A7,0x0000+(nn) // data range 0 to 100, nominal 50
#define HSDIO_GET_PAN_HORIZONTAL_POS 0xC300,0x00A7
#define HSDIO_PAN_HORIZONTAL_POS_STS 0xC301,0x00A7

#define HSDIO_TILT_VERTICAL_POS(nn) 0xCB00,0x00A9,0x0000+(nn)   // data range 0 to 100, nominal 50
#define HSDIO_GET_TILT_VERTICAL_POS 0xC300,0x00A9
#define HSDIO_TILT_VERTICAL_POS_STS 0xC301,0x00A9


#define HSDIO_PTCZ_GO 0xCA00,0xAB00  // implements the image size and position settings


// ****************************************************************************
// ***** Detail Level (1 byte)
// ****************************************************************************
#define HSDIO_DETAIL_LEVEL(nn) 0xCA00,0xAC00+(nn&0xff)    // data range 0 to 100
#define HSDIO_GET_DETAIL_LEVEL 0xC200,0xAC00
#define HSDIO_DETAIL_LEVEL_STS 0xC201,0xAC00


// ****************************************************************************
// ***** Detail Threshold (1 byte)
// ****************************************************************************
#define HSDIO_DETAIL_THRESHOLD(nn) 0xCA00,0xAD00+(nn&0xff)    // data range 0 to 100
#define HSDIO_GET_DETAIL_THRESHOLD 0xC200,0xAD00
#define HSDIO_DETAIL_THRESHOLD_STS 0xC201,0xAD00


// ****************************************************************************
// ***** Noise Reduction (1 byte)
// ****************************************************************************
#define HSDIO_NOISE_REDUCTION(nn) 0xCA00,0xAE00+(nn&0xff) // data range 0 to 100
#define HSDIO_GET_NOISE_REDUCTION 0xC200,0xAE00
#define HSDIO_NOISE_REDUCTION_STS 0xC201,0xAE00


// ****************************************************************************
// ***** Image Density (1 byte)
// ****************************************************************************
#define HSDIO_IMAGE_DENSITY(nn) 0xCA00,0xAF00+(nn&0xff)
#define HSDIO_GET_IMAGE_DENSITY 0xC200,0xAF00
#define HSDIO_IMAGE_DENSITY_STS 0xC201,0xAF00

#define HSDIO_IMAGE_DENSITY_MIN 0
#define HSDIO_IMAGE_DENSITY_MAX 100
#define HSDIO_IMAGE_DENSITY_NORMAL HSDIO_IMAGE_DENSITY_MAX


// ****************************************************************************
// ***** Output Video Processing Position/Size (2 bytes)
// ****************************************************************************
// NOTE: size and position limits are dependent on output resolution

// 2 bytes
// data range 0 to output horizontal resolution
#define HSDIO_HORIZONTAL_SIZE(nn) 0xCB00,0x00B0,0x0000+(nn)
#define HSDIO_GET_HORIZONTAL_SIZE 0xC300,0x00B0
#define HSDIO_HORIZONTAL_SIZE_STS 0xC301,0x00B0

// 2 bytes
// data range 0 to output vertical resolution
#define HSDIO_VERTICAL_SIZE(nn) 0xCB00,0x00B2,0x0000+(nn)
#define HSDIO_GET_VERTICAL_SIZE 0xC300,0x00B2
#define HSDIO_VERTICAL_SIZE_STS 0xC301,0x00B2

// 2 bytes
// data range 0 to output horizontal resolution
#define HSDIO_HORIZONTAL_POS(nn) 0xCB00,0x00B4,0x0000+(nn)
#define HSDIO_GET_HORIZONTAL_POS 0xC300,0x00B4
#define HSDIO_HORIZONTAL_POS_STS 0xC301,0x00B4

// 2 bytes
// data range 0 to output vertical resolution
#define HSDIO_VERTICAL_POS(nn) 0xCB00,0x00B6,0x0000+(nn)
#define HSDIO_GET_VERTICAL_POS 0xC300,0x00B6
#define HSDIO_VERTICAL_POS_STS 0xC301,0x00B6

// 1 byte
#define HSDIO_VIDPSZ_IMG_GO 0xCA00,0xB800  // implements the image size and position settings


// ****************************************************************************
// ***** DeInterlacer InterFrame Motion Detection Threshold (1 byte)
// ****************************************************************************
#define HSDIO_DI_IFMD_THRESHOLD(nn) 0xCA00,0xBD00+(nn&0xff)  // data range 4 to 6
#define HSDIO_GET_DI_IFMD_THRESHOLD 0xC200,0xBD00
#define HSDIO_DI_IFMD_THRESHOLD_STS 0xC201,0xBD00


// ****************************************************************************
// ***** Film Mode Cadence Status (1 byte)
// ****************************************************************************
#define HSDIO_DI_FILM_MODE_CADENCE_SEQ_STS 0xC201,0xBE00

#define HSDIO_FilmModeCadence_NP        0
#define HSDIO_FilmModeCadence_3_2       1
#define HSDIO_FilmModeCadence_2_2       2
#define HSDIO_FilmModeCadence_UNKNOWN   0xff

// ****************************************************************************
// ***** Film Mode Pull Down Status (1 byte)
// ****************************************************************************
#define HSDIO_DI_FILM_MODE_PULL_DOWN_STS 0xC201,0xBF00



// ****************************************************************************
// ***** Video Processing Brightness/Contrast Settings (2 bytes each)
// ****************************************************************************
#define HSDIO_BRIGHTNESS_RED(nn) 0xCB00,0x00C0,0x0000+(nn)  // data range 0 to 2047, nominal 1024
#define HSDIO_GET_BRIGHTNESS_RED 0xC300,0x00C0
#define HSDIO_BRIGHTNESS_RED_STS 0xC301,0x00C0

#define HSDIO_BRIGHTNESS_GREEN(nn) 0xCB00,0x00C2,0x0000+(nn)    // data range 0 to 2047, nominal 1024
#define HSDIO_GET_BRIGHTNESS_GREEN 0xC300,0x00C2
#define HSDIO_BRIGHTNESS_GREEN_STS 0xC301,0x00C2

#define HSDIO_BRIGHTNESS_BLUE(nn) 0xCB00,0x00C4,0x0000+(nn) // data range 0 to 2047, nominal 1024
#define HSDIO_GET_BRIGHTNESS_BLUE 0xC300,0x00C4
#define HSDIO_BRIGHTNESS_BLUE_STS 0xC301,0x00C4

#define HSDIO_BRIGHTNESS_GO 0xCA00,0xC600    // implement brightness settings above

#define HSDIO_MIN_BRIGHTNESS              0
#define HSDIO_MAX_BRIGHTNESS              2047

#define HSDIO_CONTRAST_RED(nn) 0xCB00,0x00C7,0x0000+(nn)    // data range 0 to 2047, nominal 1024
#define HSDIO_GET_CONTRAST_RED 0xC300,0x00C7
#define HSDIO_CONTRAST_RED_STS 0xC301,0x00C7

#define HSDIO_CONTRAST_GREEN(nn) 0xCB00,0x00C9,0x0000+(nn)  // data range 0 to 2047, nominal 1024
#define HSDIO_GET_CONTRAST_GREEN 0xC300,0x00C9
#define HSDIO_CONTRAST_GREEN_STS 0xC301,0x00C9

#define HSDIO_CONTRAST_BLUE(nn) 0xCB00,0x00CB,0x0000+(nn)   // data range 0 to 2047, nominal 1024
#define HSDIO_GET_CONTRAST_BLUE 0xC300,0x00CB
#define HSDIO_CONTRAST_BLUE_STS 0xC301,0x00CB

#define HSDIO_CONTRAST_GO 0xCA00,0xCD00  // implement contrast settings above

#define HSDIO_MIN_CONTRAST                0
#define HSDIO_MAX_CONTRAST                2047



// ****************************************************************************
// ***** Manufacturer Splash Screen OSD
// ****************************************************************************
#define HSDIO_OSD_MFR_SPLASH_SCREEN_OFF 0xCA00,0xE000

#define HSDIO_OSD_MFR_SPLASH_SCREEN_ON 0xCA00,0xE001  // OSD will timeout and clear from screen automatically

// ****************************************************************************
// ***** Timing Info OSD
// ****************************************************************************
#define HSDIO_OSD_TMG_INFO_OFF 0xCA00,0xE100

#define HSDIO_OSD_TMG_INFO_ON 0xCA00,0xE101   // OSD will timeout and clear from screen automatically





// ****************************************************************************
// ***** CEC Enable Control (1 byte)
// ****************************************************************************
#define HSDIO_CEC_ENABLE(nn) 0xCA00,0xE400+(nn&0xff)
#define HSDIO_GET_CEC_ENABLE 0xC200,0xE400
#define HSDIO_CEC_ENABLE_STS 0xC201,0xE400

#define HSDIO_CEC_ENABLE_SWITCH_msk 0x01
#define HSDIO_CEC_ENABLE_AMP_msk    0x02

// ****************************************************************************
// ***** CEC Amp Volume Setting (1 byte)
// *****    Note: A value of 0 indicates No Sound
// ****************************************************************************
#define HSDIO_CEC_AMP_VOLUME(nn) 0xCA00,0xE500+(nn&0xff)
#define HSDIO_GET_CEC_AMP_VOLUME 0xC200,0xE500

#define HSDIO_CEC_AMP_VOLUME_MIN    0
#define HSDIO_CEC_AMP_VOLUME_MAX    0x64

// ****************************************************************************
// ***** CEC Amp Mute Setting (1 byte)
// ****************************************************************************
#define HSDIO_CEC_AMP_MUTE(nn) 0xCA00,0xE600+(nn&0xff)
#define HSDIO_GET_CEC_AMP_MUTE 0xC200,0xE600

/* define elsewhere, here for convenience
#define HSDIO_AudioMute_OFF      0
#define HSDIO_AudioMute_ON       1
*/

// ****************************************************************************
// ***** CEC System Audio Mode Setting (1 byte)
// ****************************************************************************
#define HSDIO_CEC_SYS_AUDIO_MODE(nn) 0xCA00,0xE700+(nn&0xff)
#define HSDIO_GET_CEC_SYS_AUDIO_MODE 0xC200,0xE700
#define HSDIO_CEC_SYS_AUDIO_MODE_STS 0xC201,0xE700

#define HSDIO_CecSysAudioMode_OFF   0
#define HSDIO_CecSysAudioMode_ON    1

// ****************************************************************************
// ***** CEC Input Mode Setting (1 byte)
// ****************************************************************************
#define HSDIO_CEC_INPUT_MODE_STS 0xC201,0xE800

#define HSDIO_CecInputMode_HDMI     0
#define HSDIO_CecInputMode_TV       1

// ****************************************************************************
// ***** CEC System Power State (1 byte)
// ****************************************************************************
#define HSDIO_CEC_SYS_POWER_STATE(nn) 0xCA00,0xE900+(nn&0xff)
#define HSDIO_GET_CEC_SYS_POWER_STATE 0xC200,0xE900

#define HSDIO_CecPowerState_ON              0
#define HSDIO_CecPowerState_STANDBY         1
#define HSDIO_CecPowerState_STANDBY_TO_ON   2
#define HSDIO_CecPowerState_ON_TO_STANDBY   3

// ****************************************************************************
// ***** CEC Key Status (1 byte)
// ****************************************************************************
#define HSDIO_CEC_KEY_STS 0xC201,0xEA00

// these key definition values defined by HDMI Spec 1.3a
#define HSDIO_CEC_KEY_VOLUME_UP         0x41
#define HSDIO_CEC_KEY_VOLUME_DOWN       0x42
#define HSDIO_CEC_KEY_MUTE              0x43
#define HSDIO_CEC_KEY_AV_INPUT          0x69
#define HSDIO_CEC_KEY_AUDIO_INPUT       0x6A
#define HSDIO_CEC_KEY_POWER_TOGGLE      0x6B
#define HSDIO_CEC_KEY_POWER_OFF         0x6C
#define HSDIO_CEC_KEY_POWER_ON          0x6D

// ****************************************************************************
// ***** CEC Key Operand Status (1 byte)
// ****************************************************************************
#define HSDIO_CEC_KEY_OPERAND_STS 0xC201,0xEB00


// ****************************************************************************
// ***** CEC Event Status (2 byte)
// ****************************************************************************
//#define HSDIO_CEC_EVENT_STS 0xC201,0xE800
#define HSDIO_CEC_EVENT_STS 0xC301,0x00EC

#define HSDIO_CEC_ENABLE_CHANGED_msk            0x01
#define HSDIO_CEC_SYS_AUDIO_MODE_CHANGED_msk    0x02
#define HSDIO_CEC_INPUT_MODE_CHANGED_msk        0x04
#define HSDIO_CEC_EVENT_KEY_PRESS_msk           0x08
#define HSDIO_CEC_EVENT_KEY_RELEASE_msk         0x10
#define HSDIO_CEC_EVENT_STANDBY_msk             0x20






// ****************************************************************************
// ***** Output Port Commands
//    these commands require a port number parameter.
//    Port Numbers are assigned below
// ****************************************************************************
#define HSDIO_OUTPUT_PORT_HDMI_1 0x10
#define HSDIO_OUTPUT_PORT_HDMI_2 0x12
#define HSDIO_OUTPUT_PORT_HD_ANA 0x14

// ****************************************************************************
// ***** Output Sink Detect Status (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_SINK_DETECT_STS(port) 0xC200+(port+1),0x0000

#define HSDIO_SinkDetect_NOT_DETECTED       0
#define HSDIO_SinkDetect_DETECTED           1
#define HSDIO_SinkDetect_SENSED             2
#define HSDIO_SinkDetect_DETECTED_SENSED    3
#define HSDIO_SinkDetect_UNKNOWN            0xff

// ****************************************************************************
// ***** Output Sink Type (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_SINK_TYPE(port,nn) 0xCA00+(port),0x0100+(nn&0xff)
#define HSDIO_GET_OUTPUT_SINK_TYPE(port) 0xC200+(port),0x0100
#define HSDIO_OUTPUT_SINK_TYPE_STS(port) 0xC200+(port+1),0x0100

#define HSDIO_SinkType_AUTO             0
#define HSDIO_SinkType_DVI              1
#define HSDIO_SinkType_HDMI             2
#define HSDIO_SinkType_COMPONENT        3
#define HSDIO_SinkType_VGA              4
#define HSDIO_SinkType_COMPOSITE        5
#define HSDIO_SinkType_SVIDEO           6
#define HSDIO_SinkType_UNKNOWN          0xff    // detected but not known


// ****************************************************************************
// ***** Output Color Space (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_COLOR_SPACE(port,nn) 0xCA00+(port),0x0200+(nn&0xff)
#define HSDIO_GET_OUTPUT_COLOR_SPACE(port) 0xC200+(port),0x0200
#define HSDIO_OUTPUT_COLOR_SPACE_STS(port) 0xC200+(port+1),0x0200

// data same as definitions HSDIO_ColorSpace_xxx above


// ****************************************************************************
// ***** Output Color Space Detail (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_COLOR_SPACE_DTL(port,nn) 0xCA00+(port),0x0300+(nn&0xff)
#define HSDIO_GET_OUTPUT_COLOR_SPACE_DTL(port) 0xC200+(port),0x0300
#define HSDIO_OUTPUT_COLOR_SPACE_DTL_STS(port) 0xC200+(port+1),0x0300

// data same as definitions HSDIO_ColorSpaceDtl_xxx above


// ****************************************************************************
// ***** Output Color Depth (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_COLOR_DEPTH(port,nn) 0xCA00+(port),0x0400+(nn&0xff)
#define HSDIO_GET_OUTPUT_COLOR_DEPTH(port) 0xC200+(port),0x0400
#define HSDIO_OUTPUT_COLOR_DEPTH_STS(port) 0xC200+(port+1),0x0400

// data same as definitions HSDIO_ColorDepth_xxx above


// ****************************************************************************
// ***** Output Video Mode (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_VIDEO_MODE(port,nn) 0xCA00+(port),0x0500+(nn&0xff)
#define HSDIO_GET_OUTPUT_VIDEO_MODE(port) 0xC200+(port),0x0500
#define HSDIO_OUTPUT_VIDEO_MODE_STS(port) 0xC200+(port+1),0x0500

// data same as definitions HSDIO_VideoMode_xxx above


// ****************************************************************************
// ***** Output Copy Protection (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_COPY_PROTECTION(port,nn) 0xCA00+(port),0x0600+(nn&0xff)
#define HSDIO_GET_OUTPUT_COPY_PROTECTION(port) 0xC200+(port),0x0600
#define HSDIO_OUTPUT_COPY_PROTECTION_STS(port) 0xC200+(port+1),0x0600

// data same as definitions HSDIO_CopyProtection_xxx above


// ****************************************************************************
// ***** Output Timing Format Error Status (1 byte)
// ****************************************************************************
// NO LONGER SUPPORTED
//#define HSDIO_OUTPUT_TIMING_FORMAT_ERROR_STS(port) 0xC200+(port+1),0x0700

//#define HSDIO_TimingFormatError_NO_ERROR        0
//#define HSDIO_TimingFormatError_ERROR           1
//#define HSDIO_TimingFormatError_UNKNOWN         0xff

// ****************************************************************************
// ***** Output State Status (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_STATE_STS(port) 0xC200+(port+1),0x0700

#define HSDIO_OutputState_POWER_DOWN                0
#define HSDIO_OutputState_IDLE                      1
#define HSDIO_OutputState_VIDEO_NO_CP               2
#define HSDIO_OutputState_VIDEO_AVMUTE              3
#define HSDIO_OutputState_CP_AUTHENTICATING         4
#define HSDIO_OutputState_VIDEO_CP                  5
#define HSDIO_OutputState_VIDEO_CP_ERR_AV_MUTED     6
#define HSDIO_OutputState_UNKNOWN                   0xff


// ****************************************************************************
// ***** Output Port Type Status (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_PORT_TYPE_STS(port) 0xC200+(port+1),0x0800

#define HSDIO_PortType_NOT_PRESENT      0
#define HSDIO_PortType_HDMI             1
#define HSDIO_PortType_DVI              2
#define HSDIO_PortType_COMPONENT        3
#define HSDIO_PortType_VGA              4
#define HSDIO_PortType_COMPONENT_VGA    5
#define HSDIO_PortType_S_VIDEO          6
#define HSDIO_PortType_COMPOSITE        7
#define HSDIO_PortType_EXTERNAL         8
#define HSDIO_PortType_UNKNOWN          0xff


// ****************************************************************************
// ***** Output Port 5V Supply Overcurrent Status (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_5V_SUPPLY_OVC_STS(port) 0xC200+(port+1),0x0900

#define HSDIO_Boolean_FALSE     0
#define HSDIO_Boolean_TRUE      1


// ****************************************************************************
// ***** Output Port HDCP Error Status (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_HDCP_ERROR_STS(port) 0xC200+(port+1),0x0A00

#define HSDIO_HdcpError_NONE                    0
#define HSDIO_HdcpError_BAD_BKSV                1
#define HSDIO_HdcpError_RI_MISMATCH             2
#define HSDIO_HdcpError_PJ_MISMATCH             3
#define HSDIO_HdcpError_I2C_ERROR               4
#define HSDIO_HdcpError_REP_DONE_TIMEOUT        5
#define HSDIO_HdcpError_MAX_CASCADE_EXCEEDED    6
#define HSDIO_HdcpError_V_DASH_CHECK_FAILED     7
#define HSDIO_HdcpError_MAX_DEVICE_EXCEEDED     8
#define HSDIO_HdcpError_UNKNOWN                 0xff


// ****************************************************************************
// ***** Output Preferred Video Timing Status (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_PREFERRED_VIDEO_TMG_STS(port) 0xC200+(port+1),0x0B00

// status returned is a video timing format HSDIO_VideoTmg_...


// ****************************************************************************
// ***** Output Native Video Timing Status (1 byte)
// ***** 4 native settings are available
// ****************************************************************************
#define HSDIO_OUTPUT_NATIVE_VIDEO_TMG_STS(port) 0xC200+(port+1),0x0C00
#define HSDIO_OUTPUT_NATIVE_VIDEO_TMG2_STS(port) 0xC200+(port+1),0x0D00
#define HSDIO_OUTPUT_NATIVE_VIDEO_TMG3_STS(port) 0xC200+(port+1),0x0E00
#define HSDIO_OUTPUT_NATIVE_VIDEO_TMG4_STS(port) 0xC200+(port+1),0x0F00

// status returned is a video timing format HSDIO_VideoTmg_...


// ****************************************************************************
// ***** Output EDID Video Timing Status (32 bytes)
// ****************************************************************************
/*
    need explanation of how this data is formatted
*/

// read a single byte at offset nn
#define HSDIO_OUTPUT_EDID_VIDEO_TIMING_STS(port,nn) 0xC200+(port+1),0x1000+(nn<<8)

// read an array of bytes
#define HSDIO_OUTPUT_EDID_VIDEO_TIMING_BFR_SIZE 32
#define HSDIO_OUTPUT_EDID_VIDEO_TIMING_BFR_STS(port) \
        0xC506,(port+1),0x0010,HSDIO_OUTPUT_EDID_VIDEO_TIMING_BFR_SIZE



// ****************************************************************************
// ***** Output Audio Valid Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_PRESENT_STS(port) 0xC200+(port+1),0x3000

// data same as definitions HSDIO_AudioPresent_xxx above

// ****************************************************************************
// ***** Output Audio Error Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_ERROR_STS(port) 0xC200+(port+1),0x3100

// data same as definitions HSDIO_AudioErr_xxx above

// ****************************************************************************
// ***** Output Audio Format Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_FORMAT_STS(port) 0xC200+(port+1),0x3200

// data same as definitions HSDIO_AudioFmt_xxx above

// ****************************************************************************
// ***** Output Audio Sample Size Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_SAMPLE_SIZE_STS(port) 0xC200+(port+1),0x3300

// data same as definitions HSDIO_AudioSampleSize_xxx above

// ****************************************************************************
// ***** Output Audio Frequency Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_FREQ_STS(port) 0xC200+(port+1),0x3400

// data same as definitions HSDIO_AudioFreq_xxx above

// ****************************************************************************
// ***** Output Audio Layout Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_LAYOUT_STS(port) 0xC200+(port+1),0x3500

// data same as definitions HSDIO_AudioLayout_xxx above

// ****************************************************************************
// ***** Output Audio Layout Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_SAMPLE_PACKET_STS(port) 0xC200+(port+1),0x3600

// data same as definitions HSDIO_AudioSamplePacket_xxx above


// ****************************************************************************
// ***** Aux Info: Coding Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_CODING_STS(port) 0xC200+(port+1),0x3800

/* // values specified in CEA861D  section 6.6.1, Table 17, CT0-CT3 */
/* // NOTE: data is right justified within status byte from HSDIO */

// ****************************************************************************
// ***** Aux Info: Channel Count Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_CHANNEL_CNT_STS(port) 0xC200+(port+1),0x3900

/* // values specified in CEA861D, section 6.6.1, Table 17, CC0-CC2 */
/* // NOTE: data is right justified within status byte from HSDIO */

// ****************************************************************************
// ***** Aux Info: Sampling Frequency Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_SAMPLING_FREQ_STS(port) 0xC200+(port+1),0x3A00

/* // values specified in CEA861D, section 6.6.1, Table 18, SF0-SF2 */
/* // NOTE: data is right justified within status byte from HSDIO */

// ****************************************************************************
// ***** Aux Info: Sample Size Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_SAMPLE_SIZE_STS(port) 0xC200+(port+1),0x3B00

/* // values specified in CEA861D, section 6.6.1, Table 18, SS0-SS1 */
/* // NOTE: data is right justified within status byte from HSDIO */

// ****************************************************************************
// ***** Aux Info: Speaker Mapping Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_SPEAKER_MAPPING_STS(port) 0xC200+(port+1),0x3C00

/* // values specified in CEA861D, section 6.6.2, Table 20 */

// ****************************************************************************
// ***** Aux Info: Level Shift Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_LEVEL_SHIFT_STS(port) 0xC200+(port+1),0x3D00

/* // values specified in CEA861D, section 6.6.2, Table 21, LSV0-LSV3 */
/* // NOTE: data is right justified within status byte from HSDIO */

// ****************************************************************************
// ***** Aux Info: Downmix Inhibit Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_DOWNMIX_INHIBIT_STS(port) 0xC200+(port+1),0x3E00

/* // values specified in CEA861D, section 6.6.2, Table 22, DM_INH */
/* // NOTE: data is right justified within status byte from HSDIO */

// ****************************************************************************
// ***** Aux Info: Extended Coding Status (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_EXT_CODING_STS(port) 0xC200+(port+1),0x3F00

/* // values specified in CEA861E section 6.6.1, CXT0-CXT4 */
/* // NOTE: data is right justified within status byte from HSDIO */

// ****************************************************************************
// ***** Aux Info: LFE Playback Level  (1 byte)
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_AUX_LFE_PLAYBACK_LVL_STS(port) 0xC200+(port+1),0x4000

/* // values specified in CEA861E, section 6.6.2, LFEPBL0-LFEPBL1 */
/* // NOTE: data is right justified within status byte from HSDIO */



// ****************************************************************************
// ***** Channel Status
// ****************************************************************************
#define HSDIO_AUDIO_OUTPUT_CHSTS_0_STS(port) 0xC200+(port+1),0x4800
#define HSDIO_AUDIO_OUTPUT_CHSTS_1_STS(port) 0xC200+(port+1),0x4900
#define HSDIO_AUDIO_OUTPUT_CHSTS_2_STS(port) 0xC200+(port+1),0x4A00
#define HSDIO_AUDIO_OUTPUT_CHSTS_3_STS(port) 0xC200+(port+1),0x4B00
#define HSDIO_AUDIO_OUTPUT_CHSTS_4_STS(port) 0xC200+(port+1),0x4C00
#define HSDIO_AUDIO_OUTPUT_CHSTS_5_STS(port) 0xC200+(port+1),0x4D00

#define HSDIO_AUDIO_OUTPUT_CHSTS_SIZE 6
#define HSDIO_AUDIO_OUTPUT_CHSTS_STS(port) \
        0xC506,(port+1),0x0048,HSDIO_AUDIO_OUTPUT_CHSTS_SIZE


// ****************************************************************************
// ***** Output EDID Audio Descriptors
// ****************************************************************************

#define HSDIO_OUTPUT_EDID_MAX_AUDIO_DESCRIPTORS 20

/* send audio descriptor command components */
/* NOTE: the descriptor parameter is the audio descriptor entry in the table,
        value should be between 0 and HSDIO_OUTPUT_EDID_MAX_AUDIO_DESCRIPTORS-1 */

#define HSDIO_OUTPUT_EDID_AUDIO_DESC_FORMAT_STS(port,descriptor)        0xC200+(port+1),0x5000+((descriptor*4)<<8)

#define HSDIO_OUTPUT_EDID_AUDIO_DESC_NUM_CHANNELS_STS(port,descriptor)  0xC200+(port+1),0x5100+((descriptor*4)<<8)

#define HSDIO_OUTPUT_EDID_AUDIO_DESC_SAMPLE_RATES_STS(port,descriptor)  0xC200+(port+1),0x5200+((descriptor*4)<<8)

#define HSDIO_OUTPUT_EDID_AUDIO_DESC_MISC_STS(port,descriptor)          0xC200+(port+1),0x5300+((descriptor*4)<<8)

// command to read the entire array of HSDIO_OUTPUT_EDID_MAX_AUDIO_DESCRIPTORS*4 bytes
#define HSDIO_OUTPUT_EDID_AUDIO_DESC_SIZE   HSDIO_OUTPUT_EDID_MAX_AUDIO_DESCRIPTORS*4
#define HSDIO_OUTPUT_EDID_AUDIO_DESC_STS(port) \
        0xC506,(port+1),0x0050,HSDIO_OUTPUT_EDID_AUDIO_DESC_SIZE


// ****************************************************************************
// ***** Output EDID Speaker Map (3 useful bytes)
// ****************************************************************************
#define HSDIO_OUTPUT_EDID_SPEAKER_MAP_SIZE   4
#define HSDIO_OUTPUT_EDID_SPEAKER_MAP_STS(port) \
        0xC506,(port+1),0x00A0,HSDIO_OUTPUT_EDID_SPEAKER_MAP_SIZE


// ****************************************************************************
// ***** Output EDID Basic Audio (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_EDID_BASIC_AUDIO_STS(port) 0xC200+(port+1),0xA300

/* defined previously, here for reference
#define HSDIO_Boolean_FALSE     0
#define HSDIO_Boolean_TRUE      1
*/


// ****************************************************************************
// ***** Output Port Max TMDS Frequency (2 byte) for deep color output control
// ****************************************************************************
#define HSDIO_OUTPUT_MAX_TMDS_FREQ(port,nn)   0xCB00+(port),0x00A4,0x0000+(nn)
#define HSDIO_GET_OUTPUT_MAX_TMDS_FREQ(port)  0xC300+(port),0x00A4

/* the following tmds frequency values are suggested (values for nn)... */
/* limit deep color to 1080p */
#define HSDIO_MAX_TMDS_FREQ_1080P       225

/* limit deep color to 720p/1080i */
#define HSDIO_MAX_TMDS_FREQ_1080I       165

/* disable deep color support */
#define HSDIO_MAX_TMDS_FREQ_NO_DEEP_CLR 0


// ****************************************************************************
// ***** Output Timing Status (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_TMG_STS(port) 0xC200+(port+1),0xA600

/* defined previously: HSDIO_VideoTmg_...  */


// ****************************************************************************
// ***** Output Pixel Repetition Status (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_PIXEL_REP_STS(port) 0xC200+(port+1),0xA700

/* 0=no repetition, 1=2x, 2=3x, 3=4x etc. */

// ****************************************************************************
// ***** Output EDID YCbCr Support Status (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_EDID_YCBCR_STS(port) 0xC200+(port+1),0xA800

#define HSDIO_OUTPUT_EDID_YCBCR_444_msk     0x01
#define HSDIO_OUTPUT_EDID_YCBCR_422_msk     0x02


// ****************************************************************************
// ***** Output Port TMDS Drive Strength (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_TMDS_DRIVE_STR(port,nn) 0xCA00+(port),0xA900+(nn&0xff)
#define HSDIO_GET_OUTPUT_TMDS_DRIVE_STR(port) 0xC200+(port),0xA900

#define HSDIO_OUTPUT_TMDS_DRIVE_STR_MIN     0
#define HSDIO_OUTPUT_TMDS_DRIVE_STR_MAX     31



// ****************************************************************************
// ***** Output Port Change (1 byte)
// ****************************************************************************
#define HSDIO_OUTPUT_CHANGE_STS(port) 0xC200+(port+1),0xAF00

/* Output port change event masks */
#define HSDIO_ALERT_OUT_VIDEO_CHANGE_msk        0x01
#define HSDIO_ALERT_OUT_AUDIO_CHANGE_msk        0x02
#define HSDIO_ALERT_OUT_EDID_CHANGE_msk         0x04
#define HSDIO_ALERT_OUT_5VSUPPLY_OVC_msk        0x08

/* ************************************************************************************************ */
/* // ***** EDID Information from Host */
/* ************************************************************************************************ */

/* ***** Speaker Allocation Cmd */
#define HSDIO_EDID_SPEAKER_ALLOCATION_BLOCK(nn)   0xCA02,0x0000+(nn&0xff)
#define HSDIO_GET_EDID_SPEAKER_ALLOCATION_BLOCK   0xC202,0x0000

#define HSDIO_EDID_SPEAKER_ALLOCATION_BLOCK_2(nn)   0xCA02,0x0100+(nn&0xff)
#define HSDIO_GET_EDID_SPEAKER_ALLOCATION_BLOCK_2   0xC202,0x0100

#define HSDIO_EDID_SPEAKER_ALLOCATION_BLOCK_3(nn)   0xCA02,0x0200+(nn&0xff)
#define HSDIO_GET_EDID_SPEAKER_ALLOCATION_BLOCK_3   0xC202,0x0200


/* ***** Audio Descriptor Cmds */

#define HSDIO_EDID_MAX_AUDIO_DESCRIPTORS                     20
#define HSDIO_EDID_TEXT_DESCRIPTOR_LENGTH                    13
#define HSDIO_EDID_TEXT_DESCRIPTOR_LENGTH_WITH_TERMINATOR    (HSDIO_EDID_TEXT_DESCRIPTOR_LENGTH+1)
#define HSDIO_EDID_MFR_PNP_ID_LENGTH                         3
#define HSDIO_EDID_MFR_PNP_ID_LENGTH_WITH_TERMINATOR         (HSDIO_EDID_MFR_PNP_ID_LENGTH+1)

/* send command as single 32 bit word */
// not supported in this release #define HSDIO_AUDIO_DESC(descriptor,nn)  0xCC00,0x0044+(descriptor*4),(nn&0xffff),(nn>>16)

/* send audio descriptor command components */
/* NOTE: the descriptor parameter is the audio descriptor entry in the table,
        value should be between 0 and HSDIO_EDID_MAX_AUDIO_DESCRIPTORS-1 */

#define HSDIO_EDID_AUDIO_DESC_FORMAT(descriptor,nn)           0xCA02,0x0400+((descriptor*4)<<8)+(nn&0xff)
#define HSDIO_GET_EDID_AUDIO_DESC_FORMAT(descriptor)          0xC202,0x0400+((descriptor*4)<<8)

#define HSDIO_EDID_AUDIO_DESC_NUM_CHANNELS(descriptor,nn)     0xCA02,0x0500+((descriptor*4)<<8)+(nn&0xff)
#define HSDIO_GET_EDID_AUDIO_DESC_NUM_CHANNELS(descriptor)    0xC202,0x0500+((descriptor*4)<<8)

#define HSDIO_EDID_AUDIO_DESC_SAMPLE_RATES(descriptor,nn)     0xCA02,0x0600+((descriptor*4)<<8)+(nn&0xff)
#define HSDIO_GET_EDID_AUDIO_DESC_SAMPLE_RATES(descriptor)    0xC202,0x0600+((descriptor*4)<<8)

#define HSDIO_EDID_AUDIO_DESC_MISC(descriptor,nn)             0xCA02,0x0700+((descriptor*4)<<8)+(nn&0xff)
#define HSDIO_GET_EDID_AUDIO_DESC_MISC(descriptor)            0xC202,0x0700+((descriptor*4)<<8)

// command to load the entire audio descriptor array
// NOTE: not working yet
//#define HSDIO_EDID_AUDIO_DESC_SIZE   HSDIO_EDID_MAX_AUDIO_DESCRIPTORS*4
//#define HSDIO_EDID_AUDIO_DESC_STS(pData) \
//        0xC506,0x0002,0x0004,HSDIO_EDID_AUDIO_DESC_SIZE,pData



/* ***** Manufacture Plug N Play ID (PNPID) */
#define HSDIO_EDID_MFR_PNPID(str) 0xCD06,0x0002,0x0060,strlen(str)+1,str
#define HSDIO_GET_EDID_MFR_PNPID 0xC506,0x0002,0x0060,HSDIO_EDID_MFR_PNP_ID_LENGTH_WITH_TERMINATOR

// the following version of this command is for use in CALFA as it will add the NULL (0) at the end
#define HSDIO_EDID_MFR_PNPID_CALFA(str) 0xcd06,0x0002,0x0060,strlen(str)+1,str"\0"


/* ***** Product Code */
#define HSDIO_EDID_PRODUCT_CODE(nn)   0xCB02,0x0064,0x0000+(nn)
#define HSDIO_GET_EDID_PRODUCT_CODE   0xC302,0x0064


/* ***** Short Serial Number */
#define HSDIO_EDID_SHORT_SERIAL_NUMBER(nn)    0xCC02,0x0066,(nn&0xffff),(nn>>16)
#define HSDIO_GET_EDID_SHORT_SERIAL_NUMBER    0xC402,0x0066


/* **** Manufacture Week and Year */
#define HSDIO_EDID_WEEK_OF_MFR(nn)    0xCA02,0x6A00+(nn&0xff)
#define HSDIO_GET_EDID_WEEK_OF_MFR    0xC202,0x6A00

#define HSDIO_EDID_YEAR_OF_MFR(nn)    0xCA02,0x6B00+(nn&0xff)
#define HSDIO_GET_EDID_YEAR_OF_MFR    0xC202,0x6B00


/* ***** Preferred Timing */
#define HSDIO_EDID_PREFERRED_TIMING(nn)   0xCA02,0x6C00+(nn&0xff)
#define HSDIO_GET_EDID_PREFERRED_TIMING   0xC202,0x6C00

/* the following formats are supported for preferred timing
(taken from definitions above, provided here for convenience)
#define HSDIO_VideoTmg_04_1280x720p_60Hz     4
#define HSDIO_VideoTmg_05_1920x1080i_60Hz    5
#define HSDIO_VideoTmg_16_1920x1080p_60Hz    16
#define HSDIO_VideoTmg_19_1280x720p_50Hz     19
#define HSDIO_VideoTmg_20_1920x1080i_50Hz    20
#define HSDIO_VideoTmg_31_1920x1080p_50Hz    31
#define HSDIO_VideoTmg_32_1920x1080p_24Hz    32
#define HSDIO_VideoTmg_33_1920x1080p_25Hz    33
*/


/* ***** Audio Latency */
#define HSDIO_EDID_AUDIO_LATENCY_PROGRESSIVE(nn)  0xCB02,0x0070,nn
#define HSDIO_GET_EDID_AUDIO_LATENCY_PROGRESSIVE  0xC302,0x0070

#define HSDIO_EDID_AUDIO_LATENCY_INTERLACED(nn)   0xCB02,0x0072,nn
#define HSDIO_GET_EDID_AUDIO_LATENCY_INTERLACED   0xC302,0x0072



/* ***** Maximum TMDS Frequency for Deep Color Support */
#define HSDIO_EDID_MAX_TMDS_FREQ(nn)              0xCB02,0x0074,nn
#define HSDIO_GET_EDID_MAX_TMDS_FREQ              0xC302,0x0074

/* the following tmds frequency values are suggested (values for nn)... */
/* limit deep color to 1080p */
/* previously defined...
#define HSDIO_MAX_TMDS_FREQ_1080P       225
*/

/* limit deep color to 720p/1080i */
/* previously defined...
#define HSDIO_MAX_TMDS_FREQ_1080I       165
*/

/* disable deep color support */
/* previously defined...
#define HSDIO_MAX_TMDS_FREQ_NO_DEEP_CLR 0
*/



/* ***** Product Name */
#define HSDIO_EDID_PRODUCT_NAME(str) \
        0xCD06,0x0002,0x0080,strlen(str)+1,str
#define HSDIO_GET_EDID_PRODUCT_NAME \
        0xC506,0x0002,0x0080,HSDIO_EDID_TEXT_DESCRIPTOR_LENGTH_WITH_TERMINATOR

// the following version of this command is for use in CALFA as it will add the NULL (0) at the end
#define HSDIO_EDID_PRODUCT_NAME_CALFA(str) \
        0xcd06,0x0002,0x0080,strlen(str)+1,str"\0"


/* Manufacture's Name */
#define HSDIO_EDID_MFR_NAME(str) \
        0xCD06,0x0002,0x0090,strlen(str)+1,str
#define HSDIO_GET_EDID_MFR_NAME \
        0xC506,0x0002,0x0090,HSDIO_EDID_TEXT_DESCRIPTOR_LENGTH_WITH_TERMINATOR

// the following version of this command is for use in CALFA as it will add the NULL (0) at the end
#define HSDIO_EDID_MFR_NAME_CALFA(str) \
        0xcd06,0x0002,0x0090,strlen(str)+1,str"\0"


/* ***** Process EDID Values */
#define HSDIO_EDID_GO    0xCA02,0xFF00



/* ************************************************************************************************ */
/*                                                                                                  */
/* ***** FLASHWRITER Commands                                                                       */
/*                                                                                                  */
/* ************************************************************************************************ */

// ****************************************************************************
// ***** FlashWriter Status (1 byte)
// ****************************************************************************
#define FLSHWR_STS 0xC201,0x8000
/* in example code, the following command definition is used:
#define FLASH_WRITER_STS                0xC201,0x8000
*/

#define FLSHWR_STS_DOWNLOAD_BUSY                0
#define FLSHWR_STS_DOWNLOAD_READY               1
#define FLSHWR_STS_DOWNLOAD_ACK                 2
#define FLSHWR_STS_DOWNLOAD_INVALID_CHECKSUM    3
#define FLSHWR_STS_DOWNLOAD_ERROR               4

/*
in example code, the following enumerated data type is used instead:
typedef enum
{
    STS_DOWNLOAD_BUSY,
    STS_DOWNLOAD_READY,
    STS_DOWNLOAD_ACK,
    STS_DOWNLOAD_INVALID_CHECKSUM,
    STS_DOWNLOAD_ERROR,
} flashStatus_t;
*/

// ****************************************************************************
// ***** FlashWriter Attention (1 byte)
// ****************************************************************************
#define FLSHWR_ATTENTION 0xCA00,0x8800
/* in example code, the following command definition is used:
#define FLASH_ATTENTION                 0xCA00,0x8800
*/


// ****************************************************************************
// ***** FlashWriter Download Complete (1 byte)
// ****************************************************************************
#define FLSHWR_DOWNLOAD_COMPLETE 0xCA00,0x8A00
/* in example code, the following command definition is used:
#define FLASH_DOWNLOAD_COMPLETE         0xCA00,0x8A00
*/


// ****************************************************************************
// ***** FlashWriter Start Application (1 byte)
// ****************************************************************************
#define FLSHWR_START_APP 0xCA00,0x8B00
/* in example code, the following command definition is used:
#define FLASH_START_APP                 0xCA00,0x8B00
*/


// ****************************************************************************
// ***** FlashWriter Download Binary data buffer (alpha type 7)
// ****************************************************************************
#define FLSHWR_DOWNLOAD_BINARY(cnt,addr,pBuf) 0xCF01,cnt,addr,pBuf
/* in example code, the following command definition is used:
#define TYPE7_WRITE_MESSAGE_CODE        0xCF01
*/

/* maximum number of bytes that can be downloaded in one command,
    checksum byte is included at end of buffer and is part of this count */
#define FLSHWR_DOWNLOAD_BINARY_BUFFER_SIZE     1024


/* ************************************************************************************************ */
/*                                                                                                  */
/* ***** Diagnostics    (Do not use unless you know what you are doing)                             */
/*                                                                                                  */
/* ************************************************************************************************ */


/* ***** Diagnostic Debug Mask (32 bits) */
#define HSDIO_DIAG_SET_DEBUG_MASK(nn)  0xCC00,0x00F4,(nn&0xffff),(nn>>16)
// where nn can be a logical OR of any or all of the following masks...

// copy protection debug mask
#define HSDIO_DBG_CP_DETAILED_msk         0x00000001
#define HSDIO_DBG_CP_MINIMAL_msk          0x00000002
#define HSDIO_DBG_CP_RECURRING_msk        0x00000004

// operations debug mask
#define HSDIO_DBG_OP_DETAILED_msk         0x00000010
#define HSDIO_DBG_OP_MINIMAL_msk          0x00000020
#define HSDIO_DBG_OP_RECURRING_msk        0x00000040

// video related debug mask
#define HSDIO_DBG_V_DETAILED_msk          0x00000100
#define HSDIO_DBG_V_MINIMAL_msk           0x00000200
#define HSDIO_DBG_V_RECURRING_msk         0x00000400

// audio related debug mask
#define HSDIO_DBG_A_DETAILED_msk          0x00001000
#define HSDIO_DBG_A_MINIMAL_msk           0x00002000
#define HSDIO_DBG_A_RECURRING_msk         0x00004000

// command related debug mask
#define HSDIO_DBG_CMD_DETAILED_msk        0x00010000
#define HSDIO_DBG_CMD_MINIMAL_msk         0x00020000

// data packet debug mask (Infoframes and HDMI Packets)
#define HSDIO_DBG_PKT_DETAILED_msk        0x01000000
#define HSDIO_DBG_PKT_MINIMAL_msk         0x02000000

// CEC debug mask
#define HSDIO_DBG_CEC_DETAILED_msk        0x04000000
#define HSDIO_DBG_CEC_MINIMAL_msk         0x08000000

// error debug mask
#define HSDIO_DBG_COMM_ERROR_msk          0x00800000
#define HSDIO_DBG_FATAL_ERROR_msk         0x00200000
#define HSDIO_DBG_WARNING_msk             0x00100000


#define HSDIO_DIAG_SET_DEBUG_MASK_TYPE(nn) 0xCA00,0xF800+(nn&0xff)
// where nn can be one of the following...
#define HSDIO_DiagDebugMaskType_ALL_ON          0
#define HSDIO_DiagDebugMaskType_ALL_OFF         1
#define HSDIO_DiagDebugMaskType_ALL             2
#define HSDIO_DiagDebugMaskType_INPUT_DEV       3   // current input device
#define HSDIO_DiagDebugMaskType_INPUT_DEV_ALL   4   // all input devices
#define HSDIO_DiagDebugMaskType_OUTPUT_DEV_ALL  5
#define HSDIO_DiagDebugMaskType_OUTPUT_DEV_1    6
#define HSDIO_DiagDebugMaskType_OUTPUT_DEV_2    7
#define HSDIO_DiagDebugMaskType_OUTPUT_DEV_3    8
#define HSDIO_DiagDebugMaskType_HIB             9
#define HSDIO_DiagDebugMaskType_HOB             10
#define HSDIO_DiagDebugMaskType_EDID            11  // EDID building and parsing
#define HSDIO_DiagDebugMaskType_MAINAPP         12  // Main Application



/* ***** Diagnostic Commands */

// Set Hot Plug Detect (HPD) on selected input
#define HSDIO_DIAG_SET_HPD(input,state) 0xCA00,0xF900+(input&0x7F)+(state<<7)
// input can be: HSDIO_Input_HDMI_1 through HSDIO_Input_HDMI_16
// state can be: HSDIO_Boolean_TRUE (HPD is HI) or HSDIO_Boolean_FALSE (HPD is LO)


// Execute ADV7510 Diagnostic Command
#define HSDIO_DIAG_ADV7510_1_EXECUTE 0xCA00,0xFA02
#define HSDIO_DIAG_ADV7510_2_EXECUTE 0xCA00,0xFA03

// HDMI Rx (ADV7604) Input High Frequency EQ Setting
#define HSDIO_DIAG_ADV7604_HIFREQ_EQ(lf,hf) 0xCC00,0x00F0,((lf&0xff)<<8)+1,(hf&0xff)

// Execute Current Input Device Diagnostic Command
#define HSDIO_DIAG_INPUT_DEV_EXECUTE 0xCA00,0xFA01


#endif

