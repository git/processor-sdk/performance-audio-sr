
/******************************************************************************
* Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// DA10x SPI, I2C and UART slave control status (DCS7) implementation
//
//
//

/* I2C LLD Header files */
#include <ti/drv/i2c/I2C.h>
/* SPI LLD Header files */
#include <ti/drv/spi/SPI.h>
/* UART LLD Header files */
#include <ti/drv/uart/UART.h>

#include "dcs7.h"
#include "dcs7_params.h"
#include "dcs7_priv.h"
#include "dcs7_medialayer.h"


/* SPI parameters structure Slave mode*/
SPI_Params spiParams =
{
#ifdef SPICALLBACK
    SPI_MODE_CALLBACK,                 /* transferMode */
#else                                  
    SPI_MODE_BLOCKING,                 /* transferMode */
#endif
    Semaphore_PendState_WAIT_FOREVER,  /* transferTimeout */
#ifdef SPICALLBACK
    (SPI_CallbackFxn)&SPI_transferCallback,
#else
    NULL,                              /* transferCallbackFxn */
#endif
    SPI_SLAVE,                         /* mode */
    1000000,                           /* bitRate */
    DCS7_PARAMS_DEV_SPI_CLEN_16,       /* dataSize */
    SPI_POL0_PHA0,                     /* frameFormat */
    NULL                               /* custom */
};

const DCS7_Params_Dev_Spi DCS7_PARAMS_DEV_SPI =
{
    sizeof(DCS7_Params_Dev_Spi),         /* size of the structure */
    DCS7_PARAMS_DEV_SPI_NP_4_SCS,        /* number of pins */
    DCS7_PARAMS_DEV_SPI_ENAHIZ_DIS,      /* SPIENA pin mode */
    0,0,                                 
    0,                                   /* error mask */
    NULL,                                /* handle to SPI lld */
    &spiParams,                          /* SPI Initialization param */
    {0, NULL, NULL, NULL}                /* SPI transfer info */
};

#if 0 // FL: temporary remove this since "I2C_SLAVE" is a PDK 1.0.1.2_eng customization not present in PDK 1.0.6
const DCS7_Params_Dev_I2c DCS7_PARAMS_DEV_I2C =
{
    sizeof(DCS7_Params_Dev_I2c),                             /* size of the structure */  
    NULL,                                                    /* handle to I2C lld, only dynamic init parameter */
    {I2C_MODE_BLOCKING, NULL, I2C_100kHz, NULL, I2C_SLAVE},  /* I2C Initialization parameters */
    {NULL, 0, NULL, 0, 0, NULL, NULL},                       /* I2C transfer info */
    0x11,                                                    /* Own slave address of K2G */
    0,0,0,                                                   /* reserved */
    0                                                        /* Error mask */ 
};
#endif

const DCS7_Params_Dev_Uart DCS7_PARAMS_DEV_UART =
{
    sizeof(DCS7_Params_Dev_Uart),  // size of the structure
    0,                             // emask
    NULL,                          // hUart
#ifdef UARTCALLBACK
    &UART_transferCallback,        // transferCallbackFxn
    NULL,                          // hSwi
    NULL,                          // hCallback
#endif
    0,                             // isEMDAConfigured
    SRECORD_RES_MAX_S1_CNT * ASCII_SRECORD_S1_MAX_SIZE, // scratchBufSize
    NULL,                          // scratchBuf
    {0},                           // uartParams
    19200                          // Baudrate
};

const DCS7_Params_Dev DCS7_PARAMS_DEV =
{
    /* Select the appropriate device from the following choices:
           DCS7_PARAMS_DEV_SPI - Select SPI device
           DCS7_PARAMS_DEV_I2C - Select I2C device
           DCS7_PARAMS_DEV_UART - Select UART device */
//    (Uint32)&DCS7_PARAMS_DEV_SPI,
//    (Uint32)&DCS7_PARAMS_DEV_I2C,
    (Uint32)&DCS7_PARAMS_DEV_UART,
};

Uint32 DCS7Trace = Diags_USER2;

const DCS7_Params DCS7_PARAMS =
{
    sizeof(DCS7_Params),
    /* DCS7 supports following values for device (dev):
       DCS7_PARAMS_DEV_SPI0 - SPI0 device
       DCS7_PARAMS_DEV_I2C0 - I2C0 device
       DCS7_PARAMS_DEV_UART0 - UART0 device */
    DCS7_PARAMS_DEV_UART0,
    /* DCS7 supports following values for error detection and report (edr):
       DCS7_PARAMS_EDR_DIS - disable error detection and report
       DCS7_PARAMS_EDR_ENA - enable error detection and report */
    DCS7_PARAMS_EDR_ENA,
    /* Reserved fields */
    0,0,
    /* The receive buffer size, watermarks values and the transmit buffer size
       values should be chosen depending upon the data rate, latency 
       requirements, application memory requirements, etc. */
    512,
    256,
    512,
    512,
    /* DCS7 device parameters */
    (DCS7_Params_Dev*)&DCS7_PARAMS_DEV,
};

void DCS7_errorInit()
{
    /* Initialize the physical interface that will be used for reporting 
       error occurence to the master */
}

void DCS7_errorAssert()
{
    /* Configure the physical interface to indicate occurence of an error
       to the master */ 
}
void DCS7_errorDeassert()
{
    /* Configure the physical interface to indicate error recovery is 
       completed and the slave is ready to accept new data */
}
