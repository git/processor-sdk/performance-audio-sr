/*
 * Copyright (c) 2018, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      mcasp_config.c
 *
 * \brief     Configures McASP module
 *
 */

#include "mcasp_cfg.h"
#include "ioConfig.h"

#define AUDIO_DAC0_TEST

/* McASP device handles - one for each McASP port. */
Ptr mcaspDevHandles[NUM_MCASP_PORTS] = {NULL, NULL, NULL};

/* McASP parameters needed by mcaspBindDev */
Mcasp_Params mcaspParams;

/* Error flag */
uint32_t gblErrFlag = 0;
Error_Block eb;

void GblErr(Mcasp_errCbStatus arg);

/* Handle to eDMA */
extern EDMA3_DRV_Handle hEdma0;
extern EDMA3_DRV_Handle hEdma1;

/* External function declarations */
extern void McaspDevice_init(void);
extern signed char*  getGlobalAddr(signed char* addr);

#ifdef IO_LOOPBACK_TEST
extern void mcaspAppCallbackRx(void* arg, MCASP_Packet *mcasp_packet);
extern void mcaspAppCallbackTx(void* arg, MCASP_Packet *mcasp_packet);
#else
extern void asipMcaspCallback(void* arg, MCASP_Packet *mcasp_packet);
extern void asopMcaspCallback(void* arg, MCASP_Packet *mcasp_packet);
#endif


/* McASP HW setup for receive (ADC) */
Mcasp_HwSetupData mcaspRcvSetupADC = {
        /* .rmask    = */ 0xFFFFFFFF, /* 16 bits are to be used     */
        /* .rfmt     = */ 0x0001C0F0, /*
                                       * 0 bit delay from framesync
                                       * MSB first
                                       * No extra bit padding
                                       * Padding bit (ignore)
                                       * slot Size is 32
                                       * Reads from DMA port
                                       * NO rotation
                                       */
        /* .afsrctl  = */ 0X00000112, /* I2S mode - 2 slot TDM
                                       * Frame sync is one word
                                       * Internally generated frame sync
                                       * Rising edge is start of frame
                                       */
        /* .rtdm     = */ 0x00000003, /* slot 1 and 2 are active (I2S)        */
        /* .rintctl  = */ 0x00000000, /* sync error and overrun error         */
        /* .rstat    = */ 0x000001FF, /* reset any existing status bits       */
        /* .revtctl  = */ 0x00000000, /* DMA request is enabled               */
        {
             /* .aclkrctl  = */ 0x000000A7, // Receiver samples data on the rising edge of the serial clock
                                            // Internal receive clock source from output of programmable bit clock divider
                                            // Receive bit clock divide ratio = 8
             /* .ahclkrctl = */ 0x00008000, // Internal receive high-frequency clock source from output of programmable high clock divider.
                                            // Falling edge. AHCLKR is inverted before programmable bit clock divider.
             /* .rclkchk   = */ 0x00000000
        }
};

/* McASP HW setup for receive (S/PDIF or HDMI)*/
Mcasp_HwSetupData mcaspRcvSetupDIR = {
	MCASP_DIR_RMASK,   /* .rmask:   0xFFFFFFFF    */
	MCASP_DIR_RFMT,    /* .rfmt:    0x000180F0    */
	MCASP_DIR_AFSRCTL, /* .afsrctl: 0x00000111    */
	MCASP_DIR_RTDM,    /* .rtdm:    0x00000003    */
	MCASP_DIR_RINTCTL, /* .rintctl: 0x00000000    */
	MCASP_DIR_RSTAT,   /* .rstat:   0x000001FF    */
	MCASP_DIR_REVTCTL, /* .revtctl  */
        {
			MCASP_DIR_ACLKRCTL,  /* .aclkrctl:  0x00000080  */  // Receiver samples data on the rising edge of the serial clock
			                                                    // External receive clock source from ACLKR pin.
			                                                    // Receive bit clock divide ratio = 1
			MCASP_DIR_AHCLKRCTL, /* .ahclkrctl: 0x00000000  */
			MCASP_DIR_RCLKCHK    /* .rclkchk:   0x00000000  */
        }
};

/* McASP HW setup for transmit (DAC) */
Mcasp_HwSetupData mcaspXmtSetupDAC = {
        /* .xmask    = */ 0xFFFFFFFF, /* 16 bits are to be used     */
        /* .xfmt     = */ 0x000180F0, /*
                                       * 1 bit delay from framesync
                                       * MSB first
                                       * No extra bit padding
                                       * Padding bit (ignore)
                                       * slot Size is 32
                                       * Reads from DMA port
                                       * NO rotation
                                       */
//        /* .afsxctl  = */ 0x00000112, /* I2S mode - 2 slot TDM
//                                       * Frame sync is one word
//                                       * Rising edge is start of frame
//                                       * Internally generated frame sync
//                                       */
        /* .afsxctl  = */ 0x00000113, /* I2S mode - 2 slot TDM
                                       * Frame sync is one word
                                       * Falling edge is start of frame
                                       * Internally generated frame sync
                                       */
        /* .xtdm     = */ 0x00000003, /* slot 1 and 2 are active (I2S)               */
        /* .xintctl  = */ 0x00000000, /* sync error,overrun error,clK error   */
        /* .xstat    = */ 0x000001FF, /* reset any existing status bits       */
        /* .xevtctl  = */ 0x00000000, /* DMA request is enabled or disabled   */
        {
             /* .aclkxctl  = */ 0X000000E1,  // Transmit bit clock divide ratio = 2 --> works for 48khz PCM but not for DDP
//             /* .aclkxctl  = */ 0X000000E7,  // Transmit bit clock divide ratio = 8 --> working for Dolby/DTS 48khz but not for PCM
//             /* .aclkxctl  = */ 0X000000E3,  // Transmit bit clock divide ratio = 4 --> Dolby/DTS 96khz
//             /* .aclkxctl  = */ 0X000000E1,  // Transmit bit clock divide ratio = 2 --> Dolby/DTS 192khz
             /* .ahclkxctl = */ 0x00004000,
             /* .xclkchk   = */ 0x00000000
        },
};

/* McASP HW setup for transmit (DAC slave) */
Mcasp_HwSetupData mcaspXmtSetupDACSlave = {
        /* .xmask    = */ 0xFFFFFFFF, /* 16 bits are to be used     */
        /* .xfmt     = */ 0x000180F0, /*
                                       * 0 bit delay from framesync
                                       * MSB first
                                       * No extra bit padding
                                       * Padding bit (ignore)
                                       * slot Size is 32
                                       * Reads from DMA port
                                       * NO rotation
                                       */
        /* .afsxctl  = */ 0x00000113, /* I2S mode - 2 slot TDM
                                       * Frame sync is one word
                                       * Rising edge is start of frame
                                       * Internally generated frame sync
                                       */
        /* .xtdm     = */ 0x00000003, /* slot 1 and 2 are active (I2S)        */
        /* .xintctl  = */ 0x00000000, /* sync error,overrun error,clK error   */
        /* .xstat    = */ 0x000001FF, /* reset any existing status bits       */
        /* .xevtctl  = */ 0x00000000, /* DMA request is enabled or disabled   */
        {
             /* .aclkxctl  = */ 0X000000A7,
             /* .ahclkxctl = */ 0x0000C000,
             /* .xclkchk   = */ 0x00000000
        },
};

/* McASP channel parameters for ADC input */
Mcasp_ChanParams  mcaspRxChanParamADC =
{
    0x0004,                   /* number of serializers      */
    {Mcasp_SerializerNum_0,
     Mcasp_SerializerNum_1,
     Mcasp_SerializerNum_2,
     Mcasp_SerializerNum_3 }, /* serializer index           */
    &mcaspRcvSetupADC,
    TRUE,                     /* isDmaDriven                */
    Mcasp_OpMode_TDM,         /* Mode (TDM/DIT)             */
    Mcasp_WordLength_32,      /* wordWidth                  */
    NULL,                     /* void * userLoopJobBuffer   */
    0,                        /* userLoopJobLength          */
    NULL,                     /* edmaHandle                 */
    GblErr,
    2,                        /* number of TDM channels      */
    Mcasp_BufferFormat_MULTISER_MULTISLOT_SEMI_INTERLEAVED_1,
    TRUE,                     /* enableHwFifo */
    1,                        /* hwFifoEventDMARatio */
    TRUE,                     /* isDataPacked */
    Mcasp_WordBitsSelect_LSB  /* wordBitsSelect */
};

/* McASP channel parameters for ADC 6 channel input */
Mcasp_ChanParams  mcaspRxChanParamADC6ch =
{
    0x0003,                   /* number of serializers      */
    {Mcasp_SerializerNum_0,
     Mcasp_SerializerNum_1,
     Mcasp_SerializerNum_2},  /* serializer index           */
    &mcaspRcvSetupADC,
    TRUE,
    Mcasp_OpMode_TDM,         /* Mode (TDM/DIT)             */
    Mcasp_WordLength_32,
    NULL,
    0,
    NULL,
    GblErr,
    2,                        /* number of TDM channels      */
    Mcasp_BufferFormat_MULTISER_MULTISLOT_SEMI_INTERLEAVED_1,
    TRUE,                     /* enableHwFifo */
    1,                        /* hwFifoEventDMARatio */
    TRUE,                     /* isDataPacked */
    Mcasp_WordBitsSelect_LSB  /* wordBitsSelect */
};

/* McASP channel parameters for ADC stereo input */
Mcasp_ChanParams  mcaspRxChanParamADCStereo =
{
    0x0001,                   /* number of serializers      */
    {Mcasp_SerializerNum_0},  /* serializer index           */
    &mcaspRcvSetupADC,
    TRUE,
    Mcasp_OpMode_TDM,         /* Mode (TDM/DIT)             */
    Mcasp_WordLength_32,
    NULL,
    0,
    NULL,
    GblErr,
    2,                        /* number of TDM channels      */
    Mcasp_BufferFormat_MULTISER_MULTISLOT_SEMI_INTERLEAVED_1,
    TRUE,                     /* enableHwFifo */
    1,                        /* hwFifoEventDMARatio */
    TRUE,                     /* isDataPacked */
    Mcasp_WordBitsSelect_LSB  /* wordBitsSelect */
};

/* McAsp channel parameters for DIR input                 */
Mcasp_ChanParams  mcaspRxChanParamDIR =
{
	0x0001,                    /* Number of serializers      */
	{Mcasp_SerializerNum_5},   /* Serializer index           */
	&mcaspRcvSetupDIR,
	TRUE,
	Mcasp_OpMode_TDM,          /* Mode (TDM/DIT)             */
	Mcasp_WordLength_16,       /* 16-bit by default          */
	NULL,
	0,
	NULL,
	GblErr,
	2,                        /* number of TDM channels      */
	Mcasp_BufferFormat_1SER_MULTISLOT_INTERLEAVED,
	TRUE,
	1,                        /* hwFifoEventDMARatio */
	TRUE,                     /* isDataPacked */
	Mcasp_WordBitsSelect_MSB  /* wordBitsSelect */
};

/* McASP LLD channel parameters for HDMI input with 4XI2S:
 *     When slot size of McASP is configured to 32-bit, HDMI data always come to 16 MSBs of the slot
 *     and the 16 LSBs are filled with 0's. This is the nature of HDMI and I2S.
 *     For PCM data, we want all 32 bits in the slot to be transferred to the input buffer:
 *        - wordWidth = Mcasp_WordLength_32
 *        - isDataPacked = 1,
 *        - wordBitsSelect having no effect since wordWidth = slot size
 *     For bit stream, we want only 16 MSBs in the slot to be transferred to the input buffer:
 *        - wordWidth = Mcasp_WordLength_16
 *        - isDataPacked = 1,
 *        - wordBitsSelect = Mcasp_WordBitsSelect_MSB
 */
Mcasp_ChanParams  mcaspRxChanParamHDMI =
{
	0x0004,                    /* number of serializers      */
	{Mcasp_SerializerNum_12,
	 Mcasp_SerializerNum_13,
	 Mcasp_SerializerNum_14,
	 Mcasp_SerializerNum_15 }, /* serializer index           */
	&mcaspRcvSetupDIR,
	TRUE,
	Mcasp_OpMode_TDM,          /* Mode (TDM/DIT)             */
    Mcasp_WordLength_16,       /* 16-bit word length, MSB or LSB of slot to be transfered, depending on wordBitsSelect */
	NULL,
	0,
	NULL,
	GblErr,
	2,                        /* number of TDM channels      */
	Mcasp_BufferFormat_MULTISER_MULTISLOT_SEMI_INTERLEAVED_1,
	TRUE,                     /* enableHwFifo */
	1,                        /* hwFifoEventDMARatio */
	TRUE,                     /* isDataPacked, only transfer the selected bits of slot, based on wordWidth and wordBitsSelect */
	Mcasp_WordBitsSelect_MSB  /* wordBitsSelect, only matters if wordWidth < slot size */
};

/* McAsp channel parameters for HDMI stereo input with 1XI2S  */
Mcasp_ChanParams  mcaspRxChanParamHDMIStereo =
{
	0x0001,                    /* Number of serializers      */
	{Mcasp_SerializerNum_12},   /* Serializer index          */
	&mcaspRcvSetupDIR,
	TRUE,
	Mcasp_OpMode_TDM,          /* Mode (TDM/DIT)             */
    Mcasp_WordLength_16,       /* 16-bit by default          */
	NULL,
	0,
	NULL,
	GblErr,
	2,                        /* number of TDM channels      */
	Mcasp_BufferFormat_1SER_MULTISLOT_INTERLEAVED,
	TRUE,                     /* enableHwFifo */
	1,                        /* hwFifoEventDMARatio */
	TRUE,                     /* isDataPacked */
	Mcasp_WordBitsSelect_MSB  /* wordBitsSelect */
};

/* McAsp channel parameters for DAC output - DAC0            */
Mcasp_ChanParams  mcaspTx0ChanParamDAC =
{
	0x0004,                   /* number of serializers       */
	{Mcasp_SerializerNum_0,
	 Mcasp_SerializerNum_1,
	 Mcasp_SerializerNum_2,
	 Mcasp_SerializerNum_3 }, /* serializer index for DAC0   */
	&mcaspXmtSetupDAC,
	TRUE,
	Mcasp_OpMode_TDM,
	Mcasp_WordLength_32,      /* word width                  */
	NULL,
	0,
	NULL,
	GblErr,
	2,                        /* number of TDM channels      */
	Mcasp_BufferFormat_MULTISER_MULTISLOT_SEMI_INTERLEAVED_1,
	TRUE,
	1,                        /* hwFifoEventDMARatio */
	TRUE,                     /* isDataPacked */
	Mcasp_WordBitsSelect_LSB  /* wordBitsSelect */
};

/* McAsp channel parameters for DAC stereo output - DAC0     */
Mcasp_ChanParams  mcaspTx0ChanParamDACStereo =
{
    0x0001,                   /* number of serializers       */
    {Mcasp_SerializerNum_0},  /* serializer index for DAC0   */
    &mcaspXmtSetupDAC,
    TRUE,
    Mcasp_OpMode_TDM,
    Mcasp_WordLength_32,      /* word width                  */
    NULL,
    0,
    NULL,
    GblErr,
    2,                        /* number of TDM channels      */
    Mcasp_BufferFormat_MULTISER_MULTISLOT_SEMI_INTERLEAVED_1,
    TRUE,
    1,                        /* hwFifoEventDMARatio */
    TRUE,                     /* isDataPacked */
    Mcasp_WordBitsSelect_LSB  /* wordBitsSelect */
};

/* McAsp channel parameters for DAC 12 channel output        */
Mcasp_ChanParams  mcaspTx0ChanParamDAC12ch =
{
    0x0006,                   /* number of serializers       */
    {Mcasp_SerializerNum_0,
     Mcasp_SerializerNum_1,
     Mcasp_SerializerNum_2,
     Mcasp_SerializerNum_3,
     Mcasp_SerializerNum_4,
     Mcasp_SerializerNum_5 }, /* serializer index for DAC0    */
    &mcaspXmtSetupDAC,
    TRUE,
    Mcasp_OpMode_TDM,
    Mcasp_WordLength_32,      /* word width                  */
    NULL,
    0,
    NULL,
    GblErr,
    2,                        /* number of TDM channels      */
    Mcasp_BufferFormat_MULTISER_MULTISLOT_SEMI_INTERLEAVED_1,
    TRUE,
    1,                        /* hwFifoEventDMARatio */
    TRUE,                     /* isDataPacked */
    Mcasp_WordBitsSelect_LSB  /* wordBitsSelect */
};

/* McAsp channel parameters for DAC 16 channel output        */
Mcasp_ChanParams  mcaspTx0ChanParamDAC16ch =
{
    0x0008,                   /* number of serializers       */
    {Mcasp_SerializerNum_0,
     Mcasp_SerializerNum_1,
     Mcasp_SerializerNum_2,
     Mcasp_SerializerNum_3,
     Mcasp_SerializerNum_4,
     Mcasp_SerializerNum_5,
     Mcasp_SerializerNum_6,
     Mcasp_SerializerNum_7 }, /* serializer index for DAC0    */
    &mcaspXmtSetupDAC,
    TRUE,
    Mcasp_OpMode_TDM,
    Mcasp_WordLength_32,      /* word width                  */
    NULL,
    0,
    NULL,
    GblErr,
    2,                        /* number of TDM channels      */
    Mcasp_BufferFormat_MULTISER_MULTISLOT_SEMI_INTERLEAVED_1,
    TRUE,
    1,                        /* hwFifoEventDMARatio */
    TRUE,                     /* isDataPacked */
    Mcasp_WordBitsSelect_LSB  /* wordBitsSelect */
};

/* McAsp channel parameters for transmit - DAC1              */
Mcasp_ChanParams  mcaspTx1ChanParam =
{
	0x0001,                   /* number of serializers       */
	{Mcasp_SerializerNum_4},  /* serializer index for DAC0   */
	&mcaspXmtSetupDAC,
	TRUE,
	Mcasp_OpMode_TDM,
	Mcasp_WordLength_32,      /* word width                  */
	NULL,
	0,
	NULL,
	GblErr,
	2,                        /* number of TDM channels      */
	Mcasp_BufferFormat_1SER_MULTISLOT_INTERLEAVED,
	TRUE,
	1,                        /* hwFifoEventDMARatio */
	TRUE,                     /* isDataPacked */
	Mcasp_WordBitsSelect_LSB  /* wordBitsSelect */
};


/**
 *  \brief    Function called by McASP driver in case of error
 *
 *  \return    None
 */
void GblErr(Mcasp_errCbStatus arg)
{
	gblErrFlag = 1;
}


/* DAC default configuration parameters */
DacConfig  DAC_Cfg =
{
	AUD_DAC_AMUTE_CTRL_DAC_DISABLE_CMD,   /* Amute event */
	0,                                    /* Amute control */
	AUD_DAC_SAMPLING_MODE_AUTO,           /* Sampling mode */
	AUD_DAC_DATA_FORMAT_I2S,              /* Data format */
	0,                                    /* Soft mute control */
	AUD_DAC_ATTENUATION_WIDE_RANGE,       /* Attenuation mode */
	AUD_DAC_DEEMP_DISABLE,                /* De-emph control */
	100                                   /* Volume */
};

/**
 *  \brief    Configures audio DAC module
 *
 *  \return    none
 */
void configAudioDAC(void)
{
	Aud_STATUS status;

	aud_delay(10000);

	/* Initialize Audio DAC module */
	status = audioDacConfig(AUD_DAC_DEVICE_ALL, &DAC_Cfg);
	if(status != Aud_EOK)
	{
		//platform_write("Audio DAC Configuration Failed!\n");
		//testRet(1);
	}
}
#if 0
/**
 *  \brief   Configures McASP module and creates the channel
 *           for audio Tx and Rx
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS mcaspAudioConfig(void)
{
    int32_t status;

	hMcaspDevTx  = NULL;
	hMcaspDevRx  = NULL;
	hMcaspTxChan = NULL;
	hMcaspRxChan = NULL;

	/* Initialize McASP Tx and Rx parameters */
	mcaspTxParams = Mcasp_PARAMS;
	mcaspRxParams = Mcasp_PARAMS;

	mcaspTxParams.mcaspHwSetup.tx.clk.clkSetupClk = 0x23;  // not used
	mcaspTxParams.mcaspHwSetup.rx.clk.clkSetupClk = 0x23;  // not used
	mcaspRxParams.mcaspHwSetup.rx.clk.clkSetupClk = 0x23;
	mcaspRxParams.mcaspHwSetup.tx.clk.clkSetupClk = 0x63; // Asynchronous. Separate clock and frame sync used by transmit and receive sections.

#ifndef INPUT_SPDIF
	mcaspRxParams.mcaspHwSetup.glb.pdir  |= 0x2000000;   //Special case, since for HDMI input - mcasp0 is both Rx & Tx
	mcaspRxParams.mcaspHwSetup.glb.amute = 0x2;		     // this to ensure one doesn't overwrite the other (rx/tx)
	mcaspTxParams.mcaspHwSetup.glb.pdir  |= 0x2000000;   //Set Amute pin as output for Tx channel
	mcaspTxParams.mcaspHwSetup.glb.amute = 0x2;
#else
	mcaspTxParams.mcaspHwSetup.glb.pdir  |= 0x2000000;   //Set Amute pin as output for Tx channel
	mcaspTxParams.mcaspHwSetup.glb.amute = 0x2;
#endif

	/* Set the HW interrupt number */
	//mcaspTxParams.hwiNumber = 8;
	//mcaspRxParams.hwiNumber = 8;

	/* Initialize eDMA handle */
#ifdef INPUT_SPDIF
	mcaspRxChanParam.edmaHandle  = hEdma1;
#else
	mcaspRxChanParam.edmaHandle  = hEdma0;
#endif

	mcaspTx0ChanParam.edmaHandle = hEdma0;
	mcaspTx1ChanParam.edmaHandle = hEdma0;
    
#ifdef INPUT_SPDIF
	/* Bind McASP0 for Tx */
	status = mcaspBindDev(&hMcaspDevTx, CSL_MCASP_0, &mcaspTxParams);
	if((status != MCASP_COMPLETED) || (hMcaspDevTx == NULL))
	{
		//IFPRINT(platform_write("mcaspBindDev for Tx Failed\n"));
		return (Aud_EFAIL);
	}

	/* Bind McASP2 for Rx */
	status = mcaspBindDev(&hMcaspDevRx, CSL_MCASP_2, &mcaspRxParams);
	if((status != MCASP_COMPLETED) || (hMcaspDevRx == NULL))
	{
		//IFPRINT(platform_write("mcaspBindDev for Rx Failed\n"));
		return (Aud_EFAIL);
	}

#else /* HDMI or HDMI_STEREO */
	/* Bind McASP0 for Rx and Tx */
	status = mcaspBindDev(&hMcaspDevRx, CSL_MCASP_0, &mcaspRxParams);
	if((status != MCASP_COMPLETED) || (hMcaspDevRx == NULL))
	{
		//IFPRINT(platform_write("mcaspBindDev for Rx Failed\n"));
		return (Aud_EFAIL);
	}

	hMcaspDevTx = hMcaspDevRx;
#endif

	/* Create McASP channel for Tx */
	status = mcaspCreateChan(&hMcaspTxChan, hMcaspDevTx,
	                         MCASP_OUTPUT,
#ifdef AUDIO_DAC0_TEST
	                         &mcaspTx0ChanParam,
#else
	                         &mcaspTx1ChanParam,
#endif
#ifdef IO_LOOPBACK_TEST
	                         mcaspAppCallbackTx, NULL);
#else
	                         asopMcaspCallback, NULL);
#endif

	if((status != MCASP_COMPLETED) || (hMcaspTxChan == NULL))
	{
		//IFPRINT(platform_write("mcaspCreateChan for Tx Failed\n"));
		return (Aud_EFAIL);
	}

	/* Create McASP channel for Rx */
	status = mcaspCreateChan(&hMcaspRxChan, hMcaspDevRx,
	                         MCASP_INPUT,
	                         &mcaspRxChanParam,
#ifdef IO_LOOPBACK_TEST
	                         mcaspAppCallbackRx, NULL);
#else
							 asipMcaspCallback, NULL);
#endif

	if((status != MCASP_COMPLETED) || (hMcaspRxChan == NULL))
	{
		//IFPRINT(platform_write("mcaspCreateChan for Rx Failed\n"));
		return (Aud_EFAIL);
	}

	return (Aud_EOK);
} /* mcaspAudioConfig */


Aud_STATUS mcaspRx(void)
{

}

Aud_STATUS mcaspRxDeInit(void)
{
	mcaspDeleteChan(hMcaspRxChan);
	hMcaspRxChan = NULL;

	mcaspUnBindDev(hMcaspDevRx);
	hMcaspDevRx = NULL;

    return (Aud_EOK);
}

Aud_STATUS mcaspChanReset(Ptr hMcaspDev, Ptr hMcaspChan)
{
    if(hMcaspChan != NULL) {
        mcaspDeleteChan(hMcaspChan);
    }
}

Aud_STATUS mcaspRxReset(void)
{
    if(hMcaspRxChan != NULL) {
		mcaspDeleteChan(hMcaspRxChan);
		hMcaspRxChan = NULL;
    }

	return (Aud_EOK);
}

Aud_STATUS mcaspRxCreate(void)
{
    int32_t status;

	/* Create McASP channel for Rx */
	status = mcaspCreateChan(&hMcaspRxChan, hMcaspDevRx,
	                         MCASP_INPUT,
	                         &mcaspRxChanParam,
#ifdef IO_LOOPBACK_TEST
	                         mcaspAppCallbackRx, NULL);
#else
							 asipMcaspCallback, NULL);
#endif

	if((status != MCASP_COMPLETED) || (hMcaspRxChan == NULL))
	{
		//IFPRINT(platform_write("mcaspCreateChan for Rx Failed\n"));
		return (Aud_EFAIL);
	}

	return (Aud_EOK);
}

Aud_STATUS mcaspTxReset(void)
{
    if(hMcaspTxChan != NULL) {
		mcaspDeleteChan(hMcaspTxChan);
		hMcaspTxChan = NULL;
    }

	return (Aud_EOK);
}

Aud_STATUS mcaspTxCreate(void)
{
    int32_t status;

	/* Create McASP channel for Tx */
	status = mcaspCreateChan(&hMcaspTxChan, hMcaspDevTx,
	                         MCASP_OUTPUT,
	                         &mcaspTx0ChanParam,
#ifdef IO_LOOPBACK_TEST
	                         mcaspAppCallbackTx, NULL);
#else
                        	 asopMcaspCallback, NULL);
#endif
	if((status != MCASP_COMPLETED) || (hMcaspTxChan == NULL))
	{
		//IFPRINT(platform_write("mcaspCreateChan for Tx Failed\n"));
		return (Aud_EFAIL);
	}

	return (Aud_EOK);
}
#endif


Aud_STATUS mcaspRecfgWordWidth(Ptr hMcaspChan, uint16_t wordWidth)
{
    Mcasp_ChanParams chanParams;
    int32_t status;

    chanParams.wordWidth = wordWidth;  //to do: change mcaspControlChan to have wordWidth as the parameter instead of chanParams!!

    status = mcaspControlChan(hMcaspChan, Mcasp_IOCTL_CHAN_PARAMS_WORD_WIDTH, &chanParams);

    if((status != MCASP_COMPLETED)) {
        return (Aud_EFAIL);
    }
    else {
        return (Aud_EOK);
    }
} /* mcaspRecfgWordWidth */

/*======================================================================================
 *  This function checks if McASP Rx overruns or Tx underruns
 *====================================================================================*/
int mcaspCheckOverUnderRun(Ptr mcaspChanHandle)
{
    Mcasp_errCbStatus mcaspErrStat;

    mcaspControlChan(mcaspChanHandle, Mcasp_IOCTL_CHAN_QUERY_ERROR_STATS, &mcaspErrStat);

    return (mcaspErrStat.isRcvOvrRunOrTxUndRunErr);
}


/** McASP LLD configuration parameters for all input and output interfaces */
mcaspLLDconfig LLDconfigRxDIR =     // for SAP_D10_RX_DIR
{
    &mcaspRcvSetupDIR, 
    &mcaspRxChanParamDIR,
    0x23,
    0x63,                           // Asynchronous. Separate clock and frame sync used by transmit and receive sections.
    0x0,
    0x2,
    CSL_MCASP_2,
    MCASP_INPUT,
    asipMcaspCallback,
    NULL,
    NULL
};

mcaspLLDconfig LLDconfigRxADC =     // for SAP_D10_RX_ADC_44100HZ, SAP_D10_RX_ADC_88200HZ
{
    &mcaspRcvSetupADC, 
    &mcaspRxChanParamADC,
    0x23,
    0x63,
    0x0,
    0x2,
    CSL_MCASP_1,
    MCASP_INPUT,
    asipMcaspCallback,
    NULL,
    NULL
};

mcaspLLDconfig LLDconfigRxADC6ch =     // for SAP_D10_RX_ADC_6CH_44100HZ, SAP_D10_RX_ADC_6CH_88200HZ
{
    &mcaspRcvSetupADC, 
    &mcaspRxChanParamADC6ch,
    0x23,
    0x63,
    0x0,
    0x2,
    CSL_MCASP_1,
    MCASP_INPUT,
    asipMcaspCallback,
    NULL,
    NULL,
};

mcaspLLDconfig LLDconfigRxADCStereo =     // for SAP_D10_RX_ADC_STEREO_44100HZ, SAP_D10_RX_ADC_STEREO_88200HZ
{
    &mcaspRcvSetupADC, 
    &mcaspRxChanParamADCStereo,
    0x23,
    0x63,
    0x0,
    0x2,
    CSL_MCASP_1,
    MCASP_INPUT,
    asipMcaspCallback,
    NULL,
    NULL
};

mcaspLLDconfig LLDconfigRxHDMIStereo =   // for SAP_D10_RX_HDMI_STEREO
{
    &mcaspRcvSetupDIR, 
    &mcaspRxChanParamHDMIStereo,
    0x23,
    0x63,
    0x02000000,                     // Set Amute pin as output since mcasp0 is both Rx & Tx for HDMI
    0x2,
    CSL_MCASP_0,
    MCASP_INPUT,
    asipMcaspCallback,
    NULL,
    NULL
};

mcaspLLDconfig LLDconfigRxHDMI =    // for SAP_D10_RX_HDMI
{
    &mcaspRcvSetupDIR, 
    &mcaspRxChanParamHDMI,
    0x23,
    0x63,
    0x02000000,                    // Set Amute pin as output since mcasp0 is both Rx & Tx for HDMI
    0x2,
    CSL_MCASP_0,
    MCASP_INPUT,
    asipMcaspCallback,
    NULL,
    NULL
};

/*
mcaspLLDconfig LLDconfigTxDIT =    // for SAP_D10_TX_DIT
{
    &mcaspXmtSetupDIT, 
    &mcaspTx0ChanParamDIT,
    NULL,
    NULL,
    CSL_MCASP_2
};
*/

mcaspLLDconfig LLDconfigTxDAC =    // for SAP_D10_TX_DAC
{
    &mcaspXmtSetupDAC,
    &mcaspTx0ChanParamDAC,
    0x23,
    0x63,
    0x02000000,                       // Set Amute pin as output for Tx channel
    0x2,
    CSL_MCASP_0,
    MCASP_OUTPUT,
    asopMcaspCallback,
    NULL,
    NULL
};

mcaspLLDconfig LLDconfigTxDACSlave =    // for SAP_D10_TX_DAC_SLAVE
{
    &mcaspXmtSetupDACSlave, 
    &mcaspTx0ChanParamDAC,
    0x23,
    0x63,
    0x02000000,
    0x2,
    CSL_MCASP_0,
    MCASP_OUTPUT,
    asopMcaspCallback,
    NULL,
    NULL
};

mcaspLLDconfig LLDconfigTxDACStereo =    // for SAP_D10_TX_STEREO_DAC
{
    &mcaspXmtSetupDAC, 
    &mcaspTx0ChanParamDACStereo,
    0x23,
    0x63,
    0x02000000,
    0x2,
    CSL_MCASP_0,
    MCASP_OUTPUT,
    asopMcaspCallback,
    NULL,
    NULL
};

mcaspLLDconfig LLDconfigTxDACStereoSlave =    // for SAP_D10_TX_STEREO_DAC_SLAVE
{
    &mcaspXmtSetupDACSlave, 
    &mcaspTx0ChanParamDACStereo,
    0x23,
    0x63,
    0x02000000,
    0x2,
    CSL_MCASP_0,
    MCASP_OUTPUT,
    asopMcaspCallback,
    NULL,
    NULL
};

mcaspLLDconfig LLDconfigTxDAC12ch =    // for SAP_D10_TX_DAC_12CH
{
    &mcaspXmtSetupDAC, 
    &mcaspTx0ChanParamDAC12ch,
    0x23,
    0x63,
    0x02000000,
    0x2,
    CSL_MCASP_0,
    MCASP_OUTPUT,
    asopMcaspCallback,
    NULL,
    NULL
};

mcaspLLDconfig LLDconfigTxDAC16ch =    // for SAP_D10_TX_DAC_16CH
{
    &mcaspXmtSetupDAC, 
    &mcaspTx0ChanParamDAC16ch,
    0x23,
    0x63,
    0x02000000,
    0x2,
    CSL_MCASP_0,
    MCASP_OUTPUT,
    asopMcaspCallback,
    NULL,
    NULL
};


/**
 *  \brief Create a channel of McASP LLD and return the handle.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS mcasplldChanCreate(mcaspLLDconfig *lldCfg, Ptr *pChanHandle)
{
    int32_t status;

    if(mcaspDevHandles[lldCfg->mcaspPort] == NULL) {
        /* Initialize McASP parameters */
        mcaspParams = Mcasp_PARAMS;    // Mcasp_PARAMS defined in McASP LLD

        mcaspParams.mcaspHwSetup.rx.clk.clkSetupClk = lldCfg->clkSetupClkRx;
        mcaspParams.mcaspHwSetup.tx.clk.clkSetupClk = lldCfg->clkSetupClkTx;
        mcaspParams.mcaspHwSetup.glb.pdir  |= lldCfg->pdirAmute;
        mcaspParams.mcaspHwSetup.glb.amute  = lldCfg->amute;

        status = mcaspBindDev(&mcaspDevHandles[lldCfg->mcaspPort], lldCfg->mcaspPort, &mcaspParams);
        if((status != MCASP_COMPLETED) || (mcaspDevHandles[lldCfg->mcaspPort] == NULL)) {
            return (Aud_EFAIL);
        }
    }

    lldCfg->hMcaspDev = mcaspDevHandles[lldCfg->mcaspPort];

    lldCfg->mcaspChanParams->mcaspSetup = lldCfg->mcaspSetupData;
    if(lldCfg->mcaspPort == CSL_MCASP_0) {
        lldCfg->mcaspChanParams->edmaHandle = hEdma0;
    }
    else {
        lldCfg->mcaspChanParams->edmaHandle = hEdma1;
    }

    /* Create McASP channel */
    *pChanHandle = NULL;
    status = mcaspCreateChan(pChanHandle, lldCfg->hMcaspDev,
                             lldCfg->chanMode, lldCfg->mcaspChanParams,
                             lldCfg->cbFxn, NULL);

    if((status != MCASP_COMPLETED) || (*pChanHandle == NULL))
    {
        //IFPRINT(platform_write("mcaspCreateChan for Tx Failed\n"));
        return (Aud_EFAIL);
    }

    return (Aud_EOK);
}  /* mcasplldChanCreate */


/* Nothing past this point */
