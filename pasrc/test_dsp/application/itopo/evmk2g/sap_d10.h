
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// I/O device configuration data structure declarations for D10 board.
//


#ifndef SAP_D10_H
#define SAP_D10_H

#include <xdc/std.h>
#include <ti/xdais/xdas.h>
#include <paftyp_a.h>
#include <sap.h>
#include <pafsio.h>

// mode is a bitfield
//      bit 0:  SYNC 0 = synchronous, 1 = asynchronous (Tx)
//      bit 1:  Reserved
//      bit 2-5 RATE 1 = 32, 2 = 44.1, 3 = 48
//      bit 6-7:  MCLK 0 = DIR, 1 = OSC, 2 = AUX
//      bit 8-9:  MODE 0 = STD  1 = 1394 (Rx and Tx Async) 2 = HDMI 3 = Lynx

#define D10_SYNC_MASK   0x01
#define D10_SYNC_SHIFT  0x00
#define D10_SYNC_SYNC   0
#define D10_SYNC_ASYNC  1

#define D10_RATE_MASK    0x3C
#define D10_RATE_SHIFT   0x02
#define D10_RATE_32KHZ   0x1
#define D10_RATE_44_1KHZ   0x2
#define D10_RATE_48KHZ   0x3
#define D10_RATE_88_2KHZ   0x4
#define D10_RATE_96KHZ   0x5
#define D10_RATE_176_4KHZ   0x6
#define D10_RATE_192KHZ  0x7

#define D10_MCLK_MASK   0xC0
#define D10_MCLK_SHIFT  0x06
#define D10_MCLK_DIR    0
#define D10_MCLK_OSC    1
#define D10_MCLK_HDMI   2

#define D10_MODE_MASK   0x300
#define D10_MODE_SHIFT  0x08
#define D10_MODE_STD    0
#define D10_MODE_HDMI   1

#define AUDK2G_AUDIO_CLK_SRC_OSC (AUDK2G_AUDIO_CLK_SRC_I2S+1)  //temporary, to add AUDK2G_AUDIO_CLK_SRC_OSC to audk2g_audio.h

//
// Device parameter data types, recieve
//

struct SAP_D10_Rx_Params_
{
    XDAS_UInt16 mode;
    XDAS_UInt8 unused[2];
};
typedef struct SAP_D10_Rx_Params
{
    Int size;                           // Type-specific size
    struct DXX_Params_ sio;             // Common parameters
    struct SAP_Params_ sap;             // Device parameters
    struct SAP_D10_Rx_Params_ d10rx;      // Board Receive parameters
} SAP_D10_Rx_Params;

extern const SAP_D10_Rx_Params SAP_D10_RX_DIR;
extern const SAP_D10_Rx_Params SAP_D10_RX_ADC_44100HZ;
extern const SAP_D10_Rx_Params SAP_D10_RX_HDMI_STEREO;
extern const SAP_D10_Rx_Params SAP_D10_RX_HDMI;


//
// Device parameter data types, transmit
//

struct SAP_D10_Tx_Params_
{
    XDAS_UInt8 mode;
    XDAS_UInt8 unused[3];
};

typedef struct SAP_D10_Tx_Params
{
    Int size;                           // Type-specific size
    struct DXX_Params_ sio;             // Common parameters
    struct SAP_Params_ sap;             // Device parameters
    struct SAP_D10_Tx_Params_ d10tx;      // Board Transmit parameters
} SAP_D10_Tx_Params;

extern const SAP_D10_Tx_Params SAP_D10_TX_DAC;
extern const SAP_D10_Tx_Params SAP_D10_TX_STEREO_DAC;
extern const SAP_D10_Tx_Params SAP_D10_TX_DAC_SLAVE;
extern const SAP_D10_Tx_Params SAP_D10_TX_DAC_12CH;
extern const SAP_D10_Tx_Params SAP_D10_TX_DAC_16CH;

#endif // SAP_D10_H

