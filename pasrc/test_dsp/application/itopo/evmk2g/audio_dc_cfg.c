
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 * \file      audio_dc_config.c
 *
 * \brief     Configures Audio daughter card HW modules
 *
 */

#include "audio_dc_cfg.h"
#include "vproccmds_a.h"
#include <stdarg.h>
#include "string.h"
#include <xdc/runtime/Log.h>
#include <ti/addon/aud/include/aud_audio.h>

#define HSR4_I2C_ADDR 0x5D
#define	HSR4_I2C_PORT_NUM CSL_I2C_1
#define I2C_TRANSACTION_TIMEOUT         (2000U)

I2C_Params i2cParams;
I2C_Handle i2cHandle = NULL;
I2C_Transaction i2cTransaction;
uint32_t gI2cDelay=I2C_TRANSACTION_TIMEOUT;

Aud_STATUS audioHDMIConfig(void);
int alpha_i2c_write(unsigned short, ...);
void set_audio_desc(unsigned char ,unsigned char ,unsigned char ,unsigned char ,unsigned char );
void hrptredid();
void  hdmi128();

int gret_val=0;

/* ------------------------------------------------------------------------ *
 *  Prototypes                                                              *
 * ------------------------------------------------------------------------ */

/**
 *  \brief    Initializes ADC module
 *
 *  This function initializes and configures the ADC modules
 *  on audio daughter card
 *
 *  \param     devId  [IN]  ADC Device Id
 *  \param     config [IN]  ADC configuration parameters
 *
 *  \return    Platform_EOK on Success or error code
 */
Aud_STATUS audioAdcConfig(AudAdcDevId  devId, AdcConfig *config)
{
	Aud_STATUS status;

	if(config == NULL)
	{
		IFPRINT(aud_write("Invalid Inputs\n"));
		return (Aud_EINVALID);
	}

	/* Initialize all the HW instances of ADC */
	status = aud_AudioAdcInit(devId);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : aud_AudioAdcInit Failed\n"));
		return (status);
	}

	/* Set ADC channel gain */
	status = aud_AudioAdcSetGain(devId, AUD_ADC_CH_ALL, config->gain);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : aud_AudioAdcSetGain Failed\n"));
		return (status);
	}

	/* Configure Left input mux for ADC1L */
	status = aud_AudioAdcSetLeftInputMux(devId, AUD_ADC_CH1_LEFT,
	                                         config->adc1LMux);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : aud_AudioAdcSetLeftInputMux Failed\n"));
		return (status);
	}

	/* Configure Left input mux for ADC2L*/
	status = aud_AudioAdcSetLeftInputMux(devId, AUD_ADC_CH2_LEFT,
	                                         config->adc2LMux);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : aud_AudioAdcSetLeftInputMux Failed\n"));
		return (status);
	}

	/* Configure Right input mux for ADC1R */
	status = aud_AudioAdcSetRightInputMux(devId, AUD_ADC_CH1_RIGHT,
	                                          config->adc1RMux);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : aud_AudioAdcSetRightInputMux Failed\n"));
		return (status);
	}

	/* Configure Right input mux for ADC2R */
	status = aud_AudioAdcSetRightInputMux(devId, AUD_ADC_CH2_RIGHT,
	                                          config->adc2RMux);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : aud_AudioAdcSetRightInputMux Failed\n"));
		return (status);
	}

	/* Configure audio data format */
	status = aud_AudioAdcDataConfig(devId, config->wlen, config->format);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : aud_AudioAdcDataConfig Failed\n"));
		return (status);
	}

	/* Configure all the interrupts */
	status = aud_AudioAdcConfigIntr(devId, AUD_ADC_INTR_ALL,
	                                    config->intEnable);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : aud_AudioAdcConfigIntr Failed\n"));
		return (status);
	}

	return (status);
}

/**
 *  \brief    Initializes DAC module
 *
 *  This function initializes and configures the DAC modules
 *  on audio daughter card
 *
 *  \param     devId  [IN]  DAC Device Id
 *  \param     config [IN]  DAC configuration parameters
 *
 *  \return    Platform_EOK on Success or error code
 */
Aud_STATUS audioDacConfig(AudDacDevId devId, DacConfig *config)
{
	Aud_STATUS status;

	if(config == NULL)
	{
		IFPRINT(aud_write("Invalid Inputs\n"));
		return (Aud_EINVALID);
	}

	/* Initialize Audio DAC */
	status = aud_AudioDacInit(devId);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : aud_AudioDacInit Failed\n"));
		return (status);
	}

	/* Configure AMUTE control event */
	status = aud_AudioDacAmuteCtrl(devId, config->amuteCtrl,
	                                   config->amuteEnable);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : aud_AudioDacAmuteCtrl Failed\n"));
		return (status);
	}

	/* Set sampling mode */
	status = aud_AudioDacSetSamplingMode(devId, config->samplingMode);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : aud_AudioDacSetSamplingMode Failed\n"));
		return (status);
	}

	/* Set data format */
	status = aud_AudioDacSetDataFormat(devId, config->dataFormat);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : aud_AudioDacSetDataFormat Failed\n"));
		return (status);
	}

	/* Enable soft mute control */
	status = aud_AudioDacSoftMuteCtrl(devId, AUD_DAC_CHAN_ALL,
	                                      config->softMuteEnable);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : aud_AudioDacSoftMuteCtrl Failed\n"));
		return (status);
	}

	/* Set attenuation mode */
	status = aud_AudioDacSetAttnMode(devId, config->attnMode);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : aud_AudioDacSetAttnMode Failed\n"));
		return (status);
	}

	/* Set De-emphasis control */
	status = aud_AudioDacDeempCtrl(devId, config->deempCtrl);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : aud_AudioDacDeempCtrl Failed\n"));
		return (status);
	}

	/* Set DAC volume */
	status = aud_AudioDacSetVolume(devId, AUD_DAC_CHAN_ALL, config->volume);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : aud_AudioDacSetVolume Failed\n"));
		return (status);
	}

	return (status);
}

/**
 *  \brief    Initializes DIR module
 *
 *  This function initializes and configures the DIR modules
 *  on audio daughter card
 *
 *  \return    Platform_EOK on Success or error code
 */
Aud_STATUS audioDirConfig(void)
{
	Aud_STATUS status;
	int8_t          fsout;
#ifdef CHECK_ERROR_STATUS
	uint32_t        timeout;
#endif

	status = aud_AudioDirInit();
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDirConfig : Audio DIR Configuration Failed!\n"));
		return (status);
	}

#ifdef CHECK_ERROR_STATUS
	/* DIR should be in PLL mode.
	   Wait for ERROR signal to be low as DIR is configured for
	   AUTO mode */
	timeout = ERROR_STATUS_WAIT_TIMEOUT;
	while (timeout)
	{
		if(!aud_AudioDirGetErrStatus())
		{
			IFPRINT(aud_write("audioDirConfig : DIR in PLL Mode\n"));
			break;
		}

		IFPRINT(aud_write("audioDirConfig : Waiting for DIR to Enter PLL Mode...\n"));
		aud_delay(10);
		timeout--;
	}

	if(!timeout)
	{
		IFPRINT(aud_write("audioDirConfig : DIR is not in PLL Mode!!\n"));
		return (Platform_EFAIL);
	}
#endif

	fsout = aud_AudioDirGetFsOut();
	if(fsout == 2)
	{
		IFPRINT(aud_write("audioDirConfig : Out of Range Sampling Frequency\n"));
	}
	else if(fsout == 0)
	{
		IFPRINT(aud_write("audioDirConfig : Calculated Sampling Frequency Output is 43 kHz�45.2 kHz\n"));
	}
	else if(fsout == 1)
	{
		IFPRINT(aud_write("audioDirConfig : Calculated Sampling Frequency Output is 46.8 kHz�49.2 kHz\n"));
	}
	else if(fsout == 3)
	{
		IFPRINT(aud_write("audioDirConfig : Calculated Sampling Frequency Output is 31.2 kHz�32.8 kHz\n"));
	}
	else
	{
		IFPRINT(aud_write("audioDirConfig : Error in Reading FSOUT status \n"));
		status = Aud_EFAIL;
	}

 	return (status);
}

/**
 *  \brief    Prepare Alpha commands for serial (I2C) communication
 *
 *  This function parses the presented HSR41 alpha commands
 *  and prepares CPM-formatted messages to be sent over the serial interface.
 *
 *  \return    '1' on completion
 */

int alpha_i2c_write(unsigned short var1, ...)
{
	unsigned short alpha_type,length,temp_var;
	int i,offset,ret_val;
	unsigned char cmd[50];
	char *s;
	va_list argp;
	va_start(argp, var1);

	alpha_type = var1>> 8;
	switch(alpha_type)
	{
		case 0xca:
		case 0xc2:
		case 0xc3:
		case 0xc4:
			length = 4;
			break;
		case 0xcb:
			length = 6;
			break;
		case 0xcc:
			length = 8;
			break;
		case 0xcd:
		case 0xc5:
			length= 8; // temporary - data starts after 8 bytes
			break;
	}

	cmd[0]=length;
	temp_var=var1;
	for(i=0;i<length-2;i+=2) // convert to bytes as per protocol
	{
		cmd[i+1]= temp_var & 0xff;
		cmd[i+2]= temp_var >> 8;
		temp_var=va_arg(argp, short);
	}
	cmd[i+1]= temp_var & 0xff;
	cmd[i+2]= temp_var >> 8;


	if(alpha_type == 0xcd) // special processing for variable length
	{
		offset=9;
		s = va_arg(argp, char *); // remaining data is in form of string
		length = temp_var; // last short indicates data length
		cmd[0]+=length;
		for(i=offset;i<offset+length;i++)
			cmd[i]=s[i-offset];
	}
va_end(argp);

	do
	{
		I2C_transactionInit(&i2cTransaction);
		i2cTransaction.slaveAddress = HSR4_I2C_ADDR;
		i2cTransaction.writeBuf = cmd;
		i2cTransaction.writeCount = cmd[0]+1;
		i2cTransaction.timeout   = I2C_TRANSACTION_TIMEOUT;
		ret_val = I2C_transfer(i2cHandle, &i2cTransaction);

		if(ret_val !=I2C_STS_SUCCESS)
		{
			gret_val++;
			I2C_control(i2cHandle, I2C_CMD_RECOVER_BUS, &gI2cDelay);
		}
	}while(ret_val !=I2C_STS_SUCCESS);

return ret_val;
}

/**
 *  \brief    Prepares audio format descriptors
 *
 *  This function prepares the configuration data to be programmed in HSR41's
 *  audio format descriptor registers.
 *
 *
 *  \return    none
 */
void set_audio_desc(unsigned char var1,unsigned char var2,unsigned char var3,unsigned char var4,unsigned char var5)
{
	int ret_val=I2C_STS_SUCCESS;

	do{ret_val=alpha_i2c_write(HSDIO_EDID_AUDIO_DESC_FORMAT(var1, var2));}while (ret_val !=I2C_STS_SUCCESS);
	do{ret_val=alpha_i2c_write(HSDIO_EDID_AUDIO_DESC_NUM_CHANNELS(var1, var3));}while (ret_val !=I2C_STS_SUCCESS);
	do{ret_val=alpha_i2c_write(HSDIO_EDID_AUDIO_DESC_SAMPLE_RATES(var1, var4));}while (ret_val !=I2C_STS_SUCCESS);
	do{ret_val=alpha_i2c_write(HSDIO_EDID_AUDIO_DESC_MISC(var1, var5));}while (ret_val !=I2C_STS_SUCCESS);
}

/**
 *  \brief    Program HDMI CEA / EDID values, as necessary.
 *
 *  This function configures the HSR41 HDMI Repeater
 *  with the necessary CEA/EDID information, enabling support for
 *  the required audio formats.
 *
 *  \return    none
 */

void hrptredid()
{
	int ret_val=I2C_STS_SUCCESS;

	do{ret_val=alpha_i2c_write(HSDIO_EDID_SPEAKER_ALLOCATION_BLOCK(0xFF));}while (ret_val !=I2C_STS_SUCCESS);
	aud_delay(10);
	do{ret_val=alpha_i2c_write(HSDIO_EDID_SPEAKER_ALLOCATION_BLOCK_2(0x7));}while (ret_val !=I2C_STS_SUCCESS);

	set_audio_desc(0,1,2,0x7f,7);	// PCM 2 channel, 32kHz, 44.1kHz, 48kHz, 88.2kHz, 96kHz, 176.4 KHz, 192 KHz, 16bit, 20bit, 24bit
	set_audio_desc(1,1,8,0x7f,7);	// PCM 8 channel, 32kHz, 44.1kHz, 48kHz, 88.2kHz, 96kHz, 176.4 KHz, 192 KHz, 16bit, 20bit, 24bit
	set_audio_desc(2,2,6,0x7,80);	// AC3 6 channel, 32kHz, 44.1kHz, 48kHz, 640kHz max bit rate
	set_audio_desc(3,10,8,0x07,1);	// Dolby Digital Plus, 8 channel, 32kHz, 44.1kHz, 48kHz, codec specific:1
	set_audio_desc(4,12,8,0x7F,1);	// MAT(MPL)(Dolby TrueHD), 8 channel, 32kHz, 44.1kHz, 48kHz, 88.2kHz, 96kHz,
									// 176.4kHz, 192kHz,  codec specific:1
	set_audio_desc(5,7,6,0x1E,192);	// DTS 6 channel, 44.1kHz, 48kHz, 88.2kHz, 96kHz, 1,536kHz max bit rate
	set_audio_desc(6,7,8,0x6,192);  // DTS 8 channel, 44.1kHz, 48kHz, 1,536kHz max bit rate
	set_audio_desc(7,11,8,0x7F,3);  // DTS-HD, 8 channel, 32kHz, 44.1kHz, 48kHz, 88.2kHz, 96kHz, 176.4kHz, 192kHz,
									// last byte is 3 for DTS:X, 1 otherwise.
	set_audio_desc(8,11,8,0x7F,1);	// DTS-HD, 8 channel - same as above, but last byte = 1
	set_audio_desc(9,6,6,0x1f,192); //AAC LC (9,6,6,0x1f,192);
	set_audio_desc(10,0,0,0,0);
	set_audio_desc(11,0,0,0,0);
	set_audio_desc(12,0,0,0,0);
	set_audio_desc(13,0,0,0,0);
	set_audio_desc(14,0,0,0,0);
	set_audio_desc(15,0,0,0,0);
	set_audio_desc(16,0,0,0,0);
	set_audio_desc(17,0,0,0,0);
	set_audio_desc(18,0,0,0,0);
	set_audio_desc(19,0,0,0,0);
	do{ret_val=alpha_i2c_write(HSDIO_EDID_GO);}while (ret_val !=I2C_STS_SUCCESS);
}

/**
 *  \brief    Configures HSR41's TO_HOST interface
 *
 *  This function configures the HDMI repeater for EXTRACT mode of operation
 *  with the necessary HOST interface programming.
 *
 *  \return    None
 */
// Note: HDMI Tx/Out Port's state is not cared for, at all.

void  hdmi128()
{
	int ret_val=I2C_STS_SUCCESS;

	do{ret_val=alpha_i2c_write(HSDIO_ALERT(HSDIO_ALERT_INPUT_AUDIO_CHANGE_msk));}while (ret_val !=I2C_STS_SUCCESS);
	do{ret_val=alpha_i2c_write(HSDIO_AUDIO_MCLK_TO_HOST(HSDIO_AudioMClk_128X));}while (ret_val !=I2C_STS_SUCCESS);
	do{ret_val=alpha_i2c_write(HSDIO_AUDIO_UNMUTE_DELAY_TO_HOST(HSDIO_AudioUnMuteDelay_NO_DELAY));}while (ret_val !=I2C_STS_SUCCESS);
	do{ret_val=alpha_i2c_write(HSDIO_AUDIO_FORMAT_TO_HOST(HSDIO_AudioFmt_I2S));}while (ret_val !=I2C_STS_SUCCESS);
	do{ret_val=alpha_i2c_write(HSDIO_IMPLEMENT_AUDIO_TO_HOST_CMDS);}while (ret_val !=I2C_STS_SUCCESS);
	do{ret_val=alpha_i2c_write(HSDIO_AUDIO_ROUTING(HSDIO_AudioRouting_HSDIOIN_NOOUT));}while (ret_val !=I2C_STS_SUCCESS);
	do{ret_val=alpha_i2c_write(HSDIO_SYS_CFG_GO);}while (ret_val !=I2C_STS_SUCCESS);
}

/**
 *  \brief    Fetch Video Sync Status from HSR41
 *
 *  This function retrieves the Video Sync status from HSR41
 *  as part of the 'D10 initialization' sequence, prior to
 *	any subsequent interaction.
 *
 *  \return    1 on Success
 */

unsigned int read_hdmi_videosyncstatus()
{
	unsigned char data[50];
	Uint8 length=0;
	int ret_val=0;

	aud_delay(10);
	ret_val=alpha_i2c_write(HSDIO_INPUT_SYNC_STS);
	Log_info0("read_hdmi_videosyncstatus: After writing HSDIO_INPUT_SYNC_STS.");

	if(ret_val ==I2C_STS_SUCCESS)
	{
		I2C_transactionInit(&i2cTransaction);
		i2cTransaction.slaveAddress = HSR4_I2C_ADDR;
		i2cTransaction.readBuf = &length;
		i2cTransaction.readCount = 1;
		i2cTransaction.timeout   = I2C_TRANSACTION_TIMEOUT;
		ret_val = I2C_transfer(i2cHandle, &i2cTransaction);
		if(ret_val ==I2C_STS_SUCCESS)
		{
			Log_info1("read_hdmi_videosyncstatus: The read count is %d.",length);
			I2C_transactionInit(&i2cTransaction);
			i2cTransaction.slaveAddress = HSR4_I2C_ADDR;
			i2cTransaction.readBuf = &data[0];
			i2cTransaction.readCount = length;
			i2cTransaction.timeout   = I2C_TRANSACTION_TIMEOUT;
			ret_val = I2C_transfer(i2cHandle, &i2cTransaction);
			Log_info1("read_hdmi_videosyncstatus: returned status = 0x%x", data[2]);
		}

	}
	else
	{
		gret_val++;
		I2C_control(i2cHandle, I2C_CMD_RECOVER_BUS, &gI2cDelay);
	}

	if(ret_val == I2C_STS_SUCCESS) ret_val= data[2]; // 1-byte / indicates error status
	else
	{
		gret_val++;
		I2C_control(i2cHandle, I2C_CMD_RECOVER_BUS, &gI2cDelay);
	}

	return ret_val;
}

/**
 *  \brief    Clear the HMINT status
 *
 *  This function clears the status of HMINT signal on HSR41.
 *
 *  \return    none
 */
void clear_hdmi_hmint()
{
	int ret_val=0;

	aud_delay(10);
	ret_val=alpha_i2c_write(HSDIO_ALERT_STS); //clear the interrupt on ~HMINT by reading the Alert Status register

	if(ret_val !=I2C_STS_SUCCESS)
		while(1);		// Control shouldn't be here

	return;
}

/**
 *  \brief    Retrieve the HDMI input interface rate
 *
 *  This function retrieves the input sample rate (or interface rate for bitstreams)
 *  from the HSR41 card.
 *
 *  \return    HSDIO Sample Rate on Success
 */
Uint32 read_hdmi_samprate()
{
	unsigned char data[50];
	Uint8 length;
	int ret_val=7;

	Log_info0("Audio DC CFG: Entered read_hdmi_samprate");
	aud_delay(10);
	ret_val=alpha_i2c_write(HSDIO_AUDIO_INPUT_FREQ_STS);\
	if(ret_val==I2C_STS_SUCCESS)
	{
		I2C_transactionInit(&i2cTransaction);
		i2cTransaction.slaveAddress = HSR4_I2C_ADDR;
		i2cTransaction.readBuf = &length;
		i2cTransaction.readCount = 1;
		i2cTransaction.timeout   = I2C_TRANSACTION_TIMEOUT;
		ret_val = I2C_transfer(i2cHandle, &i2cTransaction);
		if(ret_val==I2C_STS_SUCCESS)
		{
			I2C_transactionInit(&i2cTransaction);
			i2cTransaction.slaveAddress = HSR4_I2C_ADDR;
			i2cTransaction.readBuf = &data[0];
			i2cTransaction.readCount = length;
			i2cTransaction.timeout   = I2C_TRANSACTION_TIMEOUT;
			ret_val = I2C_transfer(i2cHandle, &i2cTransaction);
		}

	}
	else
		while(1);

	Log_info0("Audio DC CFG: Leaving read_hdmi_samprate");

	if(ret_val==I2C_STS_SUCCESS) ret_val= data[2]; // indicates sample rate
	else
	while(1);

	return ret_val;
}

/**
 *  \brief    Initializes the HSR41 HDMI repeater module
 *
 *  This function initializes and configures the HSR41 module
 *  via the serial (I2C1) command interface for the necessary
 *  audio operations.
 *
 *  \return    Aud_EOK on Success
 */
Aud_STATUS audioHDMIConfig(void)
{
	Aud_STATUS status = Aud_EOK;

	I2C_HwAttrs   i2c_cfg;

	/* Get the default I2C init configurations */
	I2C_socGetInitCfg(HSR4_I2C_PORT_NUM, &i2c_cfg);

	i2c_cfg.enableIntr = 0;	// disable interrupt

	/* Set the default I2C init configurations */
	I2C_socSetInitCfg(HSR4_I2C_PORT_NUM, &i2c_cfg);

    I2C_init();

    I2C_Params_init(&i2cParams);
    /* BitRate : 400 Kbps */
    i2cParams.bitRate = I2C_400kHz;

    i2cHandle = I2C_open(HSR4_I2C_PORT_NUM, &i2cParams);

    Log_info0("Before EDID config.");
    hrptredid();

    Log_info0("Before HSR4 Clock config.");
	hdmi128();

	Log_info0("Before HSR4 Video Sync Detection.");

	while((HSDIO_InSync_SYNC_DETECTED != read_hdmi_videosyncstatus()));

	Log_info0("After HSR4 Video Sync Detection.");

	return (status);
}


/* Nothing past this point */
