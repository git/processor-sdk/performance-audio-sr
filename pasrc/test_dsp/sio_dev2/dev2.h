
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== dev2.h ========
 */

#ifndef DEV2_
#define DEV2_
#define DEV2_H_

#define xdc_runtime_Error__nolocalnames
//#define ti_sdo_ipc_DriverTypes__nolocalnames

#include <ti/sysbios/knl/Queue.h>
#include <ti/sysbios/knl/Semaphore.h>

#include <xdc/runtime/Error.h>
//#include <ti/sdo/io/DriverTypes.h>

#ifdef __cplusplus
extern "C" {
#endif

#define DEV2_OK      (0)   /* To replace SYS_OK */
#define DEV2_ENOMEM  (-1)  /* Replace SYS_EALLOC */
#define DEV2_EINVAL  (-2)  /* Replace SYS_EINVAL */
#define DEV2_ENODEV  (-3)  /* Replace SYS_ENODEV */
#define DEV2_EBADIO  (-4)  /* Replace SYS_EBADIO */

#define DEV2_INPUT   (0)
#define DEV2_OUTPUT  (1)
#define DEV2_MODES   (2)

/*
 *  These types are used to identify the type of the DEV2 driver
 *  in DEV2_D_devtab[].
 *  WARNING -- These ids must match those defined .h62, etc. files!
 */
#define DEV2_SIOTYPE             0
#define DEV2_IOMTYPE             1
#define DEV2_OTHERTYPE           2

typedef struct DEV2_Obj *DEV2_Handle;

//typedef xdc_Bits32 ti_sdo_io_DriverTypes_PacketCmd;
typedef xdc_Bits32 DEV2_PacketCmd;

/*
 * DEV2_Frame represents hangers which hold the stream buffers
 * All the buffer exchange between SIO/DEV2 happens using these frames.
 *
 * IMPORTANT NOTE: The DEV2_Frame struct has to match DriverTypes_Packet
 * The size of type of each field within this struct should match.
 */
typedef struct DEV2_Frame {      /* frame object */
    Queue_Elem                          link;   /* queue link */
    Ptr                                 addr;   /*! buffer address */
    SizeT                               origSize; /*! size requested */
    SizeT                               size;   /*! processed size */
    Arg                                 arg;    /*! arg to be used by end app */
    DEV2_PacketCmd                      cmd;    /*! command for mini-driver */
    xdc_runtime_Error_Id                error;  /*! error id */
    Arg                                 misc;   /*! reserved */
    Int                                 status; /*! reserved for IOM support */
    UArg                                drvArg; /*! reserved for driver */
} DEV2_Frame;

typedef Int     (*DEV2_Tclose)(DEV2_Handle);
typedef Int     (*DEV2_Tctrl)(DEV2_Handle, Uns, Arg);
typedef Int     (*DEV2_Tidle)(DEV2_Handle, Bool);
typedef Void    (*DEV2_Tinit)(Void);
typedef Int     (*DEV2_Tissue)(DEV2_Handle);
typedef Int     (*DEV2_Topen)(DEV2_Handle, String);
typedef Bool    (*DEV2_Tready)(DEV2_Handle, Semaphore_Handle);
typedef Int     (*DEV2_Treclaim)(DEV2_Handle);

/*
 * DEV2_Fxns represent driver functions template in SIO/DEV2 model
 */
typedef struct DEV2_Fxns {
    DEV2_Tclose          close;
    DEV2_Tctrl           ctrl;
    DEV2_Tidle           idle;
    DEV2_Tissue          issue;
    DEV2_Topen           open;
    DEV2_Tready          ready;
    DEV2_Treclaim        reclaim;
} DEV2_Fxns;

typedef struct DEV2_Callback {   /* DEV2 callback structure */
    Fxn        fxn;            /* function */
    Arg        arg0;           /* argument 0 */
    Arg        arg1;           /* argument 1 */
} DEV2_Callback;

/*
 * DEV2_Obj provides interface to DEV2 layer and its part of SIO object.
 */
typedef struct DEV2_Obj {    /* must be first field in device object */
    Queue_Handle  todevice;   /* downstream frames go here */
    Queue_Handle  fromdevice; /* upstream frames go here */
    size_t      bufsize;    /* buffer size */
    Uns         nbufs;      /* number of buffers */
    xdc_runtime_IHeap_Handle bufSeg;      /* buffer segment */
    Int         mode;       /* DEV2_INPUT/DEV2_OUTPUT */
    Int         devid;      /* device id */
    Ptr         params;     /* device parameters */
    Ptr         object;     /* pointer to device specific object */
    DEV2_Fxns   fxns;       /* driver function table */
    Uns         timeout;    /* timeout for DEV2_reclaim() */
    Uns         align;      /* buffer alignment */
    DEV2_Callback *callback; /* pointer to callback */
} DEV2_Obj;

/*
 * DEV2_Device holds the device attributes as specified by the user
 * in the GCONF DEV2 template or by the DEV2_createDevice API.
 */
typedef struct DEV2_Device {     /* device driver specifier */
    String      name;           /* device name */
    Void        *fxns;          /* device function table */
    Int         devid;          /* device id */
    Ptr         params;         /* device parameters */
    Uns         type;           /* type of the device */
    Ptr         devp;           /* pointer to device global data */
} DEV2_Device;

typedef struct DEV2_TableElem {
    Queue_Elem    qElem;
    DEV2_Device   device;
} DEV2_TableElem;

/*
 * DEV2_Attrs is used while creating the device dynamically
 */
typedef struct DEV2_Attrs{
    Int         devid;          /* device id */
    Ptr         params;         /* device parameters */
    Uns         type;           /* type of the device */
    Ptr         devp;           /* pointer to device global data */
}DEV2_Attrs;

/*
 * DEV2 function macros make SIO code more readable.
 */
#define DEV2_close(_dev)                 (*((_dev)->fxns.close))(_dev)
#define DEV2_ctrl(_dev,_cmd,_arg)        (*((_dev)->fxns.ctrl))(_dev,_cmd,_arg)
#define DEV2_idle(_dev,_flush)           (*((_dev)->fxns.idle))(_dev,_flush)
#define DEV2_issue(_dev)                 (*((_dev)->fxns.issue))(_dev)
#define DEV2_open(_dev,_name)            (*((_dev)->fxns.open))(_dev,_name)
#define DEV2_ready(_dev,_sem)            (*((_dev)->fxns.ready))(_dev,_sem)
#define DEV2_reclaim(_dev)               (*((_dev)->fxns.reclaim))(_dev)

/*
 *  ======== default fxns ========
 */
#define DEV2_CLOSE       ((DEV2_Tclose)DEV2_zero)
#define DEV2_CTRL        ((DEV2_Tctrl)DEV2_ebadio)
#define DEV2_EXCHANGE    ((DEV2_Tinput)DEV2_ebadio)
#define DEV2_IDLE        ((DEV2_Tidle)DEV2_zero)
#define DEV2_ISSUE       ((DEV2_Tissue)DEV2_ebadio)
#define DEV2_OPEN        ((DEV2_Topen)DEV2_zero)
#define DEV2_READY       ((DEV2_Tready)DEV2_one)
#define DEV2_RECLAIM     ((DEV2_Treclaim)DEV2_ebadio)

extern Int DEV2_one(void);
extern Int DEV2_zero(void);

/* for backwards compatibility ... */
extern Void             DEV2_init(Void);

extern Int              DEV2_ebadio(DEV2_Handle);

extern String           DEV2_match(String name, DEV2_Device **driver);
extern Void             DEV2_find(String name, DEV2_Device **driver);
extern DEV2_Frame      *DEV2_mkframe(xdc_runtime_IHeap_Handle seg,
                                Uns size, Uns align);
extern Void             DEV2_rmframe(DEV2_Frame *frame,
                                xdc_runtime_IHeap_Handle seg, Uns size);
extern Int DEV2_createDevice(String name, Void *fxns, Fxn initFxn,
                                DEV2_Attrs *attrs);
extern Int DEV2_deleteDevice(String name);

extern __FAR__ DEV2_Attrs        DEV2_ATTRS;

extern DEV2_TableElem DEV2_table[];

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif /* DEV2_*/
