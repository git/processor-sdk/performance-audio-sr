
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== dgn2.h ========
 *  Generator Device Module Interface
 */

#ifndef DGN2_
#define DGN2_

//#include <ti/bios/include/std.h>
#include <stddef.h>
#include "dev2.h"

#ifdef __cplusplus
extern "C" {
#endif

extern __FAR__ DEV2_Fxns DGN2_FXNS;

/*
 *  ======== DGN2_Params ========
 */
typedef struct {                /* device parameters */
    struct {
        Int     value;          /* constant value */
    } constant;
    struct {
        UInt32  seed;           /* seed value */
        Int     lower;          /* lower limit of generator */
        Int     upper;          /* upper limit of generator */
    } rand;
    struct {
        Int     gain;           /* gain */
        Int     freq;           /* frequency (cycles / sec) */
        Int     phase;          /* phase (in radians) */
        Int     rate;           /* sample rate (samples / sec) */
    } sine;
    struct {
        Fxn     fxn;            /* user function */
        Arg     arg;            /* and argument */
    } user;
} DGN2_Params;

typedef DGN2_Params DGN2_IParams;

extern __FAR__ DGN2_Params DGN2_PARAMS;       /* default parameter values */

typedef struct DGN2_GenObj {     
    Fxn         fxn;
    Arg         arg;
    UInt32      seed;           /* current seed value */
    Int         index;          /* current index into sine value table */
    Int         step;           /* the step for indexing the sine table */
    Int         shift;          /* sine shift value to achieve gain */
    DGN2_Params  *dparams;       /* parameters */
} DGN2_GenObj;

extern Void DGN2_iconst(DGN2_GenObj *gen, Int16 *addr, size_t size);
extern Void DGN2_irand(DGN2_GenObj *gen, Int16 *addr,  size_t size);
extern Void DGN2_isine(DGN2_GenObj *gen, Int16 *addr, size_t size);
extern Void DGN2_printHex(Arg arg, Ptr addr, size_t size);
extern Void DGN2_user(DGN2_GenObj *gen, Ptr addr, size_t size);

#define DGN2_CONST       (Int32)DGN2_iconst     /* constant series device id */
#define DGN2_RAND        (Int32)DGN2_irand      /* random series device id */
#define DGN2_SINE        (Int32)DGN2_isine      /* sine wave device id */
#define DGN2_USER        (Int32)DGN2_user       /* user function */

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif /* DGN2_ */
