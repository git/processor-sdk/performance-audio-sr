
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== sio2.h ========
 *  Stream I/O Manager
 */

#ifndef SIO2_
#define SIO2_

#include <stddef.h>

#include <xdc/runtime/IHeap.h>

#include <ti/sysbios/knl/Queue.h>

#include "dev2.h"

#ifdef __cplusplus
extern "C" {
#endif

#define SIO2_OK          0      /* To replace SYS_OK */
#define SIO2_ENOMEM     (-1)    /* To replace SYS_EALLOC */
#define SIO2_EINVAL     (-2)    /* To replace SYS_EINVAL */
#define SIO2_ENODEV     (-3)    /* To replace SYS_ENODEV */
#define SIO2_EBADIO  	(-4)  /* Replace SYS_EBADIO */
#define SIO2_ETIMEOUT  	(-5)  /* Replace SYS_ETIMEOUT */
#define SIO2_EALLOC  	(-6)  /* Replace SYS_EALLOC */
#define SIO2_EBUSY  	(-7)  /* Replace SYS_EBUSY */
#define SIO2_EMODE  	(-8)  /* Replace SYS_EMODE */

#define SIO2_INPUT      DEV2_INPUT              /* input stream */
#define SIO2_OUTPUT     DEV2_OUTPUT             /* output stream */

#define SIO2_STANDARD           0
#define SIO2_ISSUERECLAIM       1

typedef DEV2_Callback SIO2_Callback;    /* for callback mode */

typedef struct SIO2_Attrs {     /* stream attributes */
    Int         nbufs;          /* number of buffers */
    xdc_runtime_IHeap_Handle bufSeg;   /* buffer segment */
    size_t      align;          /* buffer alignment */
    Bool        flush;          /* TRUE -> don't block in DEV2_idle() */
    Uns         model;          /* SIO_STANDARD, SIO_ISSUERECLAIM */
    Uns         timeout;        /* passed to all calls to DEV2_reclaim() */
    SIO2_Callback *callback;    /* initializes callback in DEV2_Obj */
} SIO2_Attrs;

typedef struct SIO2_Obj {       /* stream object */
    DEV2_Obj     dobj;          /* device object */
    Bool         flush;         /* flush */
    Queue_Struct framelist;     /* start of frame list */
    DEV2_Frame  *standardFrame; /* frame for use in SIO_STANDARD model only */
    Uns         model;          /* SIO_STANDARD or SIO_ISSUERECLAIM */
    DEV2_Fxns   *pfxns;         /* pointer to DEV2_Fxns for static stream init */
} SIO2_Obj, *SIO2_Handle;

extern __FAR__ SIO2_Attrs SIO2_ATTRS;   /* default attributes */

extern Int _SIO2_idle(SIO2_Handle stream, Bool flush);
#define SIO2_idle(stream)               _SIO2_idle(stream, 0)
#define SIO2_flush(stream)              _SIO2_idle(stream, 1)

#define SIO2_ctrl(stream, cmd, arg)     DEV2_ctrl((DEV2_Handle)stream, cmd, arg)
#define SIO2_ready(stream)              DEV2_ready((DEV2_Handle)stream, NULL)

#define SIO2_segid(stream)              ((stream)->dobj.segid)
#define SIO2_bufsize(stream)            ((stream)->dobj.bufsize)
#define SIO2_nbufs(stream)              ((stream)->dobj.nbufs)

extern SIO2_Handle SIO2_create(String name, Int mode, size_t size, SIO2_Attrs *attrs);
extern Int SIO2_delete(SIO2_Handle stream);
extern Int SIO2_get(SIO2_Handle stream, Ptr *buf);
extern Int SIO2_staticbuf(SIO2_Handle stream, Ptr *buf);
extern Int SIO2_put(SIO2_Handle stream, Ptr *buf, size_t nbytes);
extern Uns SIO2_select(SIO2_Handle streamtab[], Int n, Uns timeout);
extern Int SIO2_issue(SIO2_Handle stream, Ptr pbuf, size_t nbytes, Arg arg);
extern Int SIO2_reclaim(SIO2_Handle stream, Ptr *ppbuf, Arg *parg);
extern Int SIO2_reclaimx(SIO2_Handle stream, Ptr *ppbuf, Arg *parg, Int *pfstatus);

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif /* SIO2_ */
