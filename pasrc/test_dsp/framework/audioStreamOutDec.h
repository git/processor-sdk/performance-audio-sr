
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamOutDec.h ========
 */

#ifndef _ASOP_DEC_H_
#define _ASOP_DEC_H_

#include <xdc/std.h>

#include "audioStreamOutProc.h"

// status codes
#define ASOP_DOP_SOK                        (   0 ) // ok
#define ASOP_DOP_ERR_RESET_ENCRESET         (  -1 ) // error - reset, enc reset
#define ASOP_DOP_ERR_DEC1_ASPCHAINRESET     (  -2 ) // error - dec1, ASP chain reset
#define ASOP_DOP_ERR_DEC1_ENCINFO           (  -3 ) // error - dec1, enc info
#define ASOP_DOP_ERR_DEC1_SETRATEX          (  -4 ) // error - dec1, set rate X
#define ASOP_DOP_ERR_DEC1_STARTOUTPUT       (  -5 ) // error - dec1, start output
#define ASOP_DOP_ERR_INFO2_SETRATEX         (  -6 ) // error - info2, set rate X
#define ASOP_DOP_ERR_INIT_CBINITREAD        (  -7 ) // error - init, CB init read
#define ASOP_DOP_ERR_INIT_CBREADSTART       (  -8 ) // error - init, CB read start
#define ASOP_DOP_ERR_STREAM_CBREAD          (  -9 ) // error - stream, CB read
#define ASOP_DOP_ERR_STREAM_CBREADUNDTHR    ( -10 ) // error - stream, CB read underflow threshold reached
#define ASOP_DOP_ERR_STREAM_ASPCHAINAPPLY   ( -11 ) // error - stream, ASP chain apply
#define ASOP_DOP_ERR_ENCODE_ENCSELECT       ( -12 ) // error - encode, encoder (re-)select
#define ASOP_DOP_ERR_ENCODE_ENC             ( -13 ) // error - encode, encoder 
#define ASOP_DOP_ERR_ENCODE_WRTOPBUFS       ( -14 ) // error - encode, write output buffers 
#define ASOP_DOP_ERR_FINALTEST_CBCHKDRAIN   ( -15 ) // error - final test, CB check drained
#define ASOP_DOP_ERR_FINALTEST_CBDRAINED    ( -16 ) // error - final test, CB drained
#define ASOP_DOP_ERR_COMPLETE_CBREADSTOP    ( -17 ) // error - complete, CB read stop
#define ASOP_DOP_ERR_COMPLETE_STOPOUTPUT    ( -18 ) // error - complete, stop output


//   Purpose:   ASOT Function for Output reset
Int asopDecOutProcReset(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg,
    Int frame
);

//   Purpose:   Dec Info1 stub function
Int asopDecOutProcInfo1(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int frame 
);

//   Purpose:   Reset ASP chain, execute ENC info, and initiate Output
Int asopDecOutProcDec1(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int frame 
);

//   Purpose:   Re-initiate Output
Int asopDecOutProcInfo2(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int frame 
);

// Initialize Decoder output processing
Int asopDecOutProcInit(
    const PAF_ASOT_Params *pP,
    const PAF_ASOT_Patchs *pQ,
    PAF_ASOT_Config *pAsotCfg, 
    Int frame 
);

// Process Decoder output audio data using ASP chain
Int asopDecOutProcStream(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int frame 
);

// Process ASP chain output audio data using Encoder
Int asopDecOutProcEncode(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int frame 
);

// Check if processing of current stream is complete
Int asopDecOutProcFinalTest(
    const struct PAF_ASOT_Params *pP, 
    const struct PAF_ASOT_Patchs *pQ, 
    struct PAF_ASOT_Config *pAsotCfg, 
    Int frame
);

// Terminate Decoder output processing
Int asopDecOutProcComplete(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int frame
);

//   Purpose:   Decoding output processing function for processing Encode Commands.
Int asopDecOutProcEncodeCommand(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
);

#endif /* _ASOP_DEC_H_ */
