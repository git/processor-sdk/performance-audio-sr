
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== alphaIntervalProc.c ========
 */

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/hal/Timer.h>

#include <acp.h>
#include <acp_mds.h>

//#include <acptype.h>
#include <acpbeta.h>
//#include <pafstd_a.h>

#include "logp.h"
#include "pafaip.h"

#include "fwkPort.h"

//#define MEMSTAT_REPORT
#ifdef MEMSTAT_REPORT
#include "paf_heapMgr.h"
#include "paf_alg_print.h"
#endif /* MEMSTAT_REPORT */

//
// Alpha code shortcut arrays
//

#ifndef execSTDAtBoot
#define execSTDAtBoot 0xc102
#endif

#ifndef execALTAtBoot
#define execALTAtBoot ((execSTDAtBoot) + (ACP_SERIES_ALT-ACP_SERIES_STD << 12))
#endif

#ifndef execOEMAtBoot
#define execOEMAtBoot ((execSTDAtBoot) + (ACP_SERIES_OEM-ACP_SERIES_STD << 12))
#endif

#ifndef execCUSAtBoot
#define execCUSAtBoot ((execSTDAtBoot) + (ACP_SERIES_CUS-ACP_SERIES_STD << 12))
#endif

const ACP_Unit atboot_sA[][2] = 
{
    { 0xc901, execSTDAtBoot, },
    { 0xc901, execALTAtBoot, },
    { 0xc901, execOEMAtBoot, },
    { 0xc901, execCUSAtBoot, },
};

#ifndef execSTDAtTime
#define execSTDAtTime 0xc104
#endif

#ifndef execALTAtTime
#define execALTAtTime ((execSTDAtTime) + (ACP_SERIES_ALT-ACP_SERIES_STD << 12))
#endif

#ifndef execOEMAtTime
#define execOEMAtTime ((execSTDAtTime) + (ACP_SERIES_OEM-ACP_SERIES_STD << 12))
#endif

#ifndef execCUSAtTime
#define execCUSAtTime ((execSTDAtTime) + (ACP_SERIES_CUS-ACP_SERIES_STD << 12))
#endif

const ACP_Unit attime_sA[][2] = 
{
    { 0xc901, execSTDAtTime, },
    { 0xc901, execALTAtTime, },
    { 0xc901, execOEMAtTime, },
    { 0xc901, execCUSAtTime, },
};

const PAF_AlphaIntervalProcessingStatus aipStatusInit = 
{
    sizeof (PAF_AlphaIntervalProcessingStatus),
    3,      // mode
    0x99,   // mask
    100,    // time (msec.)
};

// Global debug counters */
Uint32 gTaskAipCnt=0;

LINNO_DEFN (alphaIntervalProcessingTask); /* Line number macros */
ERRNO_DEFN (alphaIntervalProcessingTask); /* Error number macros */

/*
 *  ======== taskAipFxn ========
 *  Alpha Interval Processing task function
 */
Void taskAipFxn(
    Int betaPrimeValue
)
{
    Int mode;
    ALG_Handle acpAlg;
    ACP_Handle acp;
    Int betaPrime;
    Int betaPrimeOffset;
    PAF_AlphaIntervalProcessingStatus aipStatus;
    Int errno;
    // debug
    //UInt32 curTime;
    
    //System_printf("Enter taskAipFxn()\n");
    Log_info0("Enter taskAipFxn()");

    //
    // Initialize
    //
    aipStatus = aipStatusInit;   

    LINNO_RPRT(alphaIntervalProcessingTask, -1);

    // Create an ACP algorithm instance with trace enabled
    ACP_MDS_init ();

    if (!(acpAlg = (ALG_Handle )ACP_MDS_create (NULL)))
    {
        //LOG_printf(&trace, "AIP: ACP algorithm instance creation failed");
        Log_info0("AIP: ACP algorithm instance creation failed");
        LINNO_RPRT(alphaIntervalProcessingTask, __LINE__);
        return;
    }
    
    acpAlg->fxns->algControl (acpAlg, ACP_GETBETAPRIMEOFFSET, 
        (IALG_Status *) &betaPrimeOffset);
    betaPrime = betaPrimeOffset * betaPrimeValue;

    acp = (ACP_Handle )acpAlg;
    acp->fxns->attach(acp, ACP_SERIES_STD, STD_BETA_SYSINT+betaPrime, 
        (IALG_Status *)&aipStatus);
    
    //LOG_printf(&trace, "AIP: ACP processing initialized");
    Log_info0("AIP: ACP processing initialized");

#ifdef MEMSTAT_REPORT
    // Output current heap memory usage
    PAF_ALG_memStatusPrint("AIP RAM REPORT", 
        PAF_HEAP_INT, PAF_HEAP_INT1, PAF_HEAP_EXT, 
        PAF_HEAP_INT1_SHM, PAF_HEAP_EXT_SHM, PAF_HEAP_EXT_NONCACHED_SHM);
#endif  
    
    // Set default stream
    if (betaPrimeValue != 0)
    {
        ACP_Unit stream_s[2] = { 0xcd09, 0x0400, };

        stream_s[1] += betaPrimeValue;
        if (errno = acp->fxns->apply (acp, stream_s, NULL)) 
        {
            //LOG_printf(&trace, "AIP: at boot stream error 0x%04x sending 0x%x, 0x%x",
            //        errno, stream_s[0], stream_s[1]);
            Log_info3("AIP: at boot stream error 0x%04x sending 0x%x, 0x%x",
                errno, stream_s[0], stream_s[1]);
            LINNO_RPRT(alphaIntervalProcessingTask, __LINE__);
            return;
        }
    }

    // Process at boot interval if mode is non-zero
    if (mode = aipStatus.mode) 
    {
        Int i;
        Int mask = aipStatus.mask;
        for (i=0; i < 4; i++)
        {
            if (mask & (0x01 << i)) 
            {
                //LOG_printf(&trace, "AIP: at boot %d: %x, %x", i, atboot_sA[i][0], atboot_sA[i][1]);
                Log_info3("AIP: at boot %d: %x, %x", i, atboot_sA[i][0], atboot_sA[i][1]);
                if (errno = acp->fxns->sequence(acp, atboot_sA[i], NULL)) 
                {
                    //LOG_printf(&trace, "AIP: at boot series %d error (0x%04x) <ignored>", i, errno);
                    Log_info2("AIP: at boot series %d error (0x%04x) <ignored>", i, errno);
                    ERRNO_RPRT (alphaIntervalProcessingTask, errno);
                }
                else
                {
                    //LOG_printf(&trace, "AIP: at boot series %d process", i);                    
                    Log_info1("AIP: at boot series %d process", i);                    
                }
            }
        }
    }
    
    LINNO_RPRT(alphaIntervalProcessingTask, -2);

    //
    // Run until task is destroyed by the system 
    //    
    
    while (mode = aipStatus.mode)
    {
        Uns time, interval, elapsed, now, then;

        while (((mode = aipStatus.mode) > 1) && ((time = aipStatus.time) > 0))
        {
            // Compute interval in system clock ticks,
            // interval time expressed in msec.
            // Note: time can be updated via alpha commands
            interval = time * USEC_PER_MSEC / Clock_tickPeriod; //interval = (CLK_countspms() * time) / CLK_getprd();            
            
            // Compute elapsed time
            now = Clock_getTicks(); //now = TSK_time ();            
            elapsed = now - then;
            then = now;
            
            if (interval > elapsed)
            {
                Task_sleep(interval - elapsed); //TSK_sleep (interval - elapsed);
            }

            if (mode > 2)
            {
                Int i;
                Int mask = aipStatus.mask;
                for (i=0; i < 4; i++) 
                {
                    if (mask & (0x10 << i))
                    {
                        if (errno = acp->fxns->sequence(acp, attime_sA[i], NULL))
                        {
                            // LOG_printf(&trace, "AIP: at time series %d error (0x%04x) <ignored>", i, errno);
                            // Log_info2("AIP: at time series %d error (0x%04x) <ignored>", i, errno);
                            ERRNO_RPRT (alphaIntervalProcessingTask, errno);
                        }
                        else
                        {
                            // LOG_printf(&trace, "AIP: at time series %d process", i);
                            // Log_info1("AIP: at time series %d process", i);
                        }
                    }
                }
            }
            
            gTaskAipCnt++;
            
            //curTime = Clock_getTicks();
            //System_printf("System time in taskAipFxn = %lu\n", (ULong)curTime);
            //Log_info1("System time in taskAipFxn = %u", curTime);
        }
    }

    // Block until re-prioritized when mode or time is zero
    LINNO_RPRT(alphaIntervalProcessingTask, __LINE__);
    Task_setPri(Task_self(), -1);   //TSK_setpri( TSK_self(), -1);
    Task_yield();                   //TSK_yield();
    
    //System_printf("Exit taskAipFxn()\n");
    Log_info0("Exit taskAipFxn()");
}
