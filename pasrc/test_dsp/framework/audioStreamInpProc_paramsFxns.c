
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamInpProc_paramsFxns.c ========
 */

#include <xdc/std.h>
 
#include "as0.h"
#include "audioStreamInpProc.h"
 
// .............................................................................
// Audio Stream Input Task Parameter Functions
//
//   Name:      PAF_ASIT_params_fxns
//   Purpose:   ASIT jump table.
//   From:      PAF_AST_Params
//   Uses:      See contents.
//   States:    N.A.
//   Return:    N.A.
//   Trace:     None.
//
const PAF_ASIT_Fxns PAF_ASIT_params_fxns =
{
    {   // initPhase[8]
        PAF_ASIT_initPhaseMalloc,           // initPhaseMalloc,
        PAF_ASIT_initPhaseConfig,           // initPhaseConfig,
        PAF_ASIT_initPhaseAcpAlg,           // initPhaseAcpAlg,
        PAF_ASIT_initPhaseCommon,           // initPhaseCommon,
        PAF_ASIT_initPhaseAlgKey,           // initPhaseAlgKey,
        PAF_ASIT_initPhaseDevice,           // initPhaseDevice,
        PAF_ASIT_initPhaseDecOpCircBuf,     // initPhaseDecOpCircBuf,
        PAF_ASIT_initPhaseOutIS,            // initPhaseOutIS
    },
    NULL,                                   // passProcessing
    PAF_ASIT_autoProcessing,                // autoProcessing
    PAF_ASIT_decodeProcessing,              // decodeProcessing
    PAF_ASIT_decodeCommand,                 // decodeCommand
    PAF_ASIT_decodeInit,                    // decodeInit
    PAF_ASIT_decodeInfo,                    // decodeInfo
    PAF_ASIT_decodeInfo1,                   // decodeInfo1
    PAF_ASIT_decodeInfo2,                   // decodeInfo2
    PAF_ASIT_decodeDecode,                  // decodeDecode
    PAF_ASIT_decodeFinalTest,               // decodeFinalTest
    PAF_ASIT_decodeComplete,                // decodeComplete
    PAF_ASIT_selectDevices,                 // selectDevices
    PAF_ASIT_sourceDecode,                  // sourceDecode
    PAF_DEC_deviceAllocate,                 // deviceAllocate
    PAF_DEC_deviceSelect,                   // deviceSelect
    NULL, //PAF_DEC_computeFrameLength,             // computeFrameLength
    PAF_DEC_updateInputStatus,              // updateInputStatus
    NULL, /*headerPrint*/                   // headerPrint
    NULL, /*allocPrint*/                    // allocPrint
    NULL, /*commonPrint*/                   // commonPrint
    NULL, /*bufMemPrint*/                   // bufMemPrint
    NULL /*memStatusPrint*/                 // memStatusPrint
};
