
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== systemInit.c ========
 */

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/System.h>
#include <ti/ipc/SharedRegion.h>
#include <ti/ipc/Ipc.h>
#include <ti/sysbios/family/c66/Cache.h>
#include <ti/sysbios/knl/Swi.h>
#include <xdc/runtime/Error.h>

#include "params.h"
#include "pafhjt.h"
#include "paf_heapMgr.h"
#include "ipc_common.h"
//#include "aspMsg_common.h"
//#include "aspMsg_master.h"
#include "aspDecOpCircBuf_master.h"
#include "audioStreamProc_master.h" // ASIT/ASOT config
#include "audioStreamProc_common.h" // ASIT/ASOT/ASDT config
#include "audioStreamInpProc.h"
#include "audioStreamOutProc.h"
#include "systemInit.h"

// debug
#include <ti/addon/aud/include/evmc66x_pinmux.h>
#include "evmc66x_gpio_dbg.h"


#define __TASK_NAME__  "TaskSysInit"

#define TASK_AFP_PRI        ( 2 )
#define TASK_AIP_PRI        ( 3 )
#define TASK_SSP_PRI        ( 1 )
#define TASK_ASIP_PRI       ( 4 ) //( 4 )
#define TASK_ASOP_PRI       ( 4 )

#define SYS_INIT_SLEEP      ( 100 )

// For patching alpha codes
extern Void ACP_main_cus(Void);

//#define MEMSTAT_REPORT
#ifdef MEMSTAT_REPORT
#include "paf_alg_print.h"
#endif

#define RAM_REPORT
#ifdef RAM_REPORT
#include "audioStreamProc_params.h"
#include "paf_alg_print.h"
extern PAF_ASIT_Fxns PAF_ASIT_params_fxns;
extern PAF_ASOT_Fxns PAF_ASOT_params_fxns;
#endif

// Task initialization flag array.
// Each flag indicates whether Task has been initialized.
//   0: uninitialized
//   1: initialized
Int gTaskInitFlag[NUM_SYS_TASKS];

Swi_Handle gAsopSwiHandle;
extern void asopSwiFunc();

// PAF Hidden Jump Table (HJT) pointer
const PAFHJT_t *pafhjt;

/* Semaphore handles for Tx and Rx - to put in structures */
Semaphore_Handle asipSemRx;
Semaphore_Handle asopSemTx;
void sysCreateSemaphores(void);

/*
 *  ======== taskSysInitFxn ========
 *  System Initializations task function
 */
Void taskSysInitFxn(Void)
{
    UInt16  regionId;
    IHeap_Handle srHeapMsmc, srHeapDdr3, srHeapNonCacheDdr3;
    UInt16 remoteProcId;    
    Int status;


    Log_info0("Enter taskSysInitFxn()");

    // Create Shared DDR region as non-Cacheable, Before IPC attach
    // TODO: remove hardcoded address and get correct values from .bld
    {   // base: 0x81000000, len: 0x01000000,
        //UInt Common2_DDR3_MAR_base = 0x01848204;
        UInt Common2_DDR3_base = 0x81000000;
        UInt Common2_DDR3_len = 0x01000000;
        // disable cache for common2 DDR3
        Cache_setMar((xdc_Ptr)Common2_DDR3_base, Common2_DDR3_len, Cache_Mar_DISABLE);
    }

    //taskPriAfp = Task_setPri(TaskAfp, -1);
    //taskPriAip = Task_setPri(TaskAip, -1);
    //taskPriAsip = Task_setPri(TaskAsip, -1);

    // Attach IPC
    Log_info0("taskSysInitFxn:Ipc_attach()");
    remoteProcId = MultiProc_getId(IPC_SLAVE_NAME);
    if (remoteProcId == MultiProc_INVALIDID) {
        System_abort("taskSysInitFxn(): improper MultiProc ID\n");
    }
    do {
        status = Ipc_attach(remoteProcId);
    //} while ((status < 0) && (status == Ipc_E_NOTREADY));
    } while (status != Ipc_S_SUCCESS);

    // Create ASIT event
    status = astCreateEvent(&gAsitEvtHandle);
    if (status < 0)
    {
        System_abort("taskSysInitFxn(): unable to initialize Event. Exiting.");
    }
    
    // Create ASIT sync event (for master IPC MessageQ)
    status = astCreateSyncEvent(gAsitEvtHandle, ASIT_EVTMSK_RXACK, &gAsitSyncEvtHandle);
    if (status < 0)
    {
        System_abort("taskSysInitFxn(): unable to initialize SyncEvent. Exiting.");
    }
    
    // Initialize (IPC) ASP master messaging
    // Note: MessageQ_open() called in this function.
    //          MessageQ_open() blocks. 
    //          However, no other Tasks are allowed to execute at this point.
    Log_info0("taskSysInitFxn(): AspMsgMaster_init()");
    status = AspMsgMaster_init(gPAF_ASIT_config.hAspMsgMaster,
        remoteProcId,
        ASIT_ASP_MSG_HEAP_ID, 
        ASIT_ASP_MSG_MASTER_NUMSGS, 
        gAsitSyncEvtHandle,
        gAsitEvtHandle,
        ASIT_EVTMSK_RXACK);
    if (status < 0)
    {
        System_abort("taskSysInitFxn(): unable to initialize ASIT ASP messaging. Exiting.");
    }
    
    // Initialize decoder output circular buffer control
    Log_info0("taskSysInitFxn(): cbCtlInit()");
    status = cbCtlInit(&gPAF_ASPM_config.decOpCircBufCtl, asip_params.zone.decodeN, &gPAF_AST_config.xDecOpCb);
    if (status < 0)
    {
        System_abort("taskSysInitFxn(): unable to initialize Decoder Output Circular Buffer Control. Exiting.");
    }
    
    // Initialize Output Processing Init-Sync control
    Log_info0("taskSysInitFxn:outIsCtlInit()");
    status = outIsCtlInit(&gPAF_ASPM_config.outIsCtl, &gPAF_AST_config.xOutIsi);
    if (status < 0)
    {
        System_abort("taskSysInitFxn(): unable to initialize Output Init-Sync Control. Exiting.");
    }

    // Initialize PAF heap manager
    Log_info0("taskSysInitFxn:pafHeapMgr_init()");
    regionId = SharedRegion_getIdByName("SR_MSMC");
    srHeapMsmc = (IHeap_Handle)SharedRegion_getHeap(regionId);
    regionId = SharedRegion_getIdByName("SR_DDR3");
    srHeapDdr3 = (IHeap_Handle)SharedRegion_getHeap(regionId);
    regionId = SharedRegion_getIdByName("COMMON2_DDR3");
    srHeapNonCacheDdr3 = (IHeap_Handle)SharedRegion_getHeap(regionId);
    pafHeapMgr_init((IHeap_Handle)heapMemL2Sram, 
        (IHeap_Handle)heapMemMsmcSram, 
        (IHeap_Handle)heapMemDdr3, 
        srHeapMsmc, 
        srHeapDdr3,
        srHeapNonCacheDdr3);

#ifdef MEMSTAT_REPORT
    // Output current heap memory usage
    PAF_ALG_memStatusPrint("System Initialization, Initial MEMSTAT REPORT",
        PAF_HEAP_INT, PAF_HEAP_INT1, PAF_HEAP_EXT, 
        PAF_HEAP_INT1_SHM, PAF_HEAP_EXT_SHM, PAF_HEAP_EXT_NONCACHED_SHM);
#endif        
    
    // Initialize HJT
    pafhjt = &PAFHJT_RAM;
    
    // Patch sigma tables
    Log_info0("taskSysInitFxn(): ACP_main_cus()");
    ACP_main_cus();

#ifdef RAM_REPORT // patch RAM report functions
    PAF_ASIT_params_fxns.headerPrint = PAF_ALG_headerPrint;
    PAF_ASIT_params_fxns.allocPrint = PAF_ALG_allocPrint;
    PAF_ASIT_params_fxns.commonPrint = PAF_ALG_commonPrint;
    PAF_ASIT_params_fxns.bufMemPrint = PAF_ALG_bufMemPrint;
    PAF_ASIT_params_fxns.memStatusPrint = PAF_ALG_memStatusPrint;
    PAF_ASOT_params_fxns.headerPrint = PAF_ALG_headerPrint;
    PAF_ASOT_params_fxns.allocPrint = PAF_ALG_allocPrint;
    PAF_ASOT_params_fxns.commonPrint = PAF_ALG_commonPrint;
    PAF_ASOT_params_fxns.bufMemPrint = PAF_ALG_bufMemPrint;
    PAF_ASOT_params_fxns.memStatusPrint = PAF_ALG_memStatusPrint;
#endif

#if 1 // debug, GPIO ports
    // Any of these can be used on ARM, but DSP must configure the GPIO since pinMuxSetMode() is only available on DSP
    aud_pinMuxSetMode(101,AUD_PADCONFIG_MUX_MODE_QUATERNARY); // PAD101 to QUATERNARY, GPIO (B5 on AudioDaughterCard J12)
    GPIOSetDirection(GPIO_PORT_0, GPIO_PIN_99, GPIO_OUT); // GPIO0,pin99 output
    GPIOSetOutput(GPIO_PORT_0, GPIO_PIN_99);
    GPIOClearOutput(GPIO_PORT_0, GPIO_PIN_99);
    aud_pinMuxSetMode(117,AUD_PADCONFIG_MUX_MODE_QUATERNARY); // PAD117 to QUATERNARY, GPIO (B8 on AudioDaughterCard J12)
    GPIOSetDirection(GPIO_PORT_0, GPIO_PIN_106, GPIO_OUT); // GPIO0,pin106 output
    GPIOSetOutput(GPIO_PORT_0, GPIO_PIN_106);
    GPIOClearOutput(GPIO_PORT_0, GPIO_PIN_106);
    aud_pinMuxSetMode(118,AUD_PADCONFIG_MUX_MODE_QUATERNARY); // PAD118 to QUATERNARY, GPIO (B9 on AudioDaughterCard J12)
    GPIOSetDirection(GPIO_PORT_0, GPIO_PIN_107, GPIO_OUT); // GPIO0,pin107 output
    GPIOSetOutput(GPIO_PORT_0, GPIO_PIN_107);
    GPIOClearOutput(GPIO_PORT_0, GPIO_PIN_107);
#endif
    
    // This is required to use SYS alpha codes in atboot?
    Log_info0("taskSysInitFxn:TaskSystemStream resume");
    gTaskInitFlag[TASK_SSP_INIT_FLAG_IDX] = 0;
    Task_setPri(TaskSystemStream, TASK_SSP_PRI);
    // Wait until Execution of TaskSystemStream is complete once.
    while (gTaskInitFlag[TASK_SSP_INIT_FLAG_IDX] == 0)  // System Stream initialization flag is set to 1 in TaskSystemStream
    {
        Task_sleep(SYS_INIT_SLEEP);
    }

    Swi_Params swiParams;
    Error_Block eb;
    Error_init(&eb);
    Swi_Params_init(&swiParams);
	swiParams.priority = 5;
    gAsopSwiHandle = Swi_create(asopSwiFunc, &swiParams, &eb);
    if (gAsopSwiHandle == NULL) {
     System_abort("Swi create failed");
    }

    Task_setPri(TaskAfp, TASK_AFP_PRI);
    Task_setPri(TaskAip, TASK_AIP_PRI);
    Task_setPri(TaskAsip, TASK_ASIP_PRI);
    Task_setPri(TaskAsop, TASK_ASOP_PRI);

    // Create ASIT/ASOT semaphores
    //sysCreateSemaphores();

#ifdef MEMSTAT_REPORT
    // Output current heap memory usage
    PAF_ALG_memStatusPrint("System Initialization, Final MEMSTAT REPORT",
        PAF_HEAP_INT, PAF_HEAP_INT1, PAF_HEAP_EXT, 
        PAF_HEAP_INT1_SHM, PAF_HEAP_EXT_SHM, PAF_HEAP_EXT_NONCACHED_SHM);
#endif           
    
    Log_info0("Exit taskSysInitFxn()");
}

// Create ASIT/ASOT events
void sysCreateSemaphores(void)
{
    Semaphore_Params SemParams;

    Semaphore_Params_init(&SemParams);

    asipSemRx = Semaphore_create(0, &SemParams, NULL);
    asopSemTx = Semaphore_create(0, &SemParams, NULL);
}
