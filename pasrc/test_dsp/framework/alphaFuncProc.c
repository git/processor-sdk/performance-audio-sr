
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== AudioFuncProc.c ========
 */

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <stdlib.h>

#include "paf_heapMgr.h"
#include "alphaFuncProc.h"

//#define MEMSTAT_REPORT
#ifdef MEMSTAT_REPORT
#include "paf_alg_print.h"
#endif /* MEMSTAT_REPORT */

// debug
//#include "dbgIdle.h" // initiate IDLE memstat

#define ENABLE_AE_TRACE
#ifdef ENABLE_AE_TRACE
    //#define AE_TRACE0(a) LOG_printf a
    #define AE_TRACE0(a)            Log_info0(a)
    #define AE_TRACE1(a,b)          Log_info1(a,b)
    #define AE_TRACE2(a,b,c)        Log_info2(a,b,c)
    #define AE_TRACE3(a,b,c,d)      Log_info3(a,b,c,d)
    #define AE_TRACE4(a,b,c,d,e)    Log_info4(a,b,c,d,e)    
#else
    #define AE_TRACE0(a)
    #define AE_TRACE1(a,b)
    #define AE_TRACE2(a,b,c)
    #define AE_TRACE3(a,b,c,d)
    #define AE_TRACE4(a,b,c,d,e)
#endif


// Global debug counters */
Uint32 gTaskAfpCnt=0;

AFP_Obj AFP_OBJ;

Void *AFP_memAlloc(
    AFP_Handle handle,
    Int32 *pHeapId,
    UInt32 size,
    UInt32 align
)
{
    IHeap_Handle hHeap;
    
    hHeap = pafHeapMgr_readHeapHandle(*pHeapId);
    return (void *)Memory_alloc((IHeap_Handle)hHeap, size, align, NULL);
}

Void AFP_process(
    AFP_Handle handle
)
{
    Int rxIdx, txIdx;
    Int encap, len, res_len, error, cnt, txStrip, i, rxOffset, txOffset;

    /* Process Alpha commands */
    for(;;)
    {
        /* Set read and write buffer indices */
        rxIdx = 2;
        txIdx = 3;
        txOffset = 0;
        encap = 0;

        /* Read the payload */
        if (handle->dcs7->fxns->read(handle->dcs7, 
            handle->rxBuf+rxIdx,
            handle->params->rxBufSize-(rxIdx<<1)))
        {
            //handle->fxns->log(handle,handle->logObj,
            //    "AFP6: Error in reading message");
            
            /* Write the response */
            handle->txBuf[txIdx++] = 0xdead;
            AE_TRACE0("AFP_process:  0xDEAD: Error in reading alpha message.");
            handle->txBuf[3-txOffset-1] = txIdx-3+txOffset;
            handle->dcs7->fxns->write(handle->dcs7,
                &handle->txBuf[3-txOffset-1],
                handle->params->txBufSize-(3-txOffset-1<<1));
                
            continue;
        }
        
        /* Get CPM count */
        cnt = handle->rxBuf[rxIdx++];
        /* Process the C-record */
        while (cnt)
        {
            /* Get the length of alpha code */
            len = handle->acp->fxns->length(handle->acp, &handle->rxBuf[rxIdx]);
            /* Get the response length */
            res_len = handle->acp->fxns->htgnel(handle->acp, &handle->rxBuf[rxIdx]);
            if (res_len > (handle->params->txBufSize-2>>1)-txIdx)
            {
                handle->txBuf[txIdx++] = 0xdead;
                //handle->fxns->log(handle,handle->logObj,
                //    "AFP6: Response too big");
                AE_TRACE0("AFP_process:  0xDEAD: Response too big.");
                break;
            }

            /* Add encapsulation to alpha command -- ACP limitation */
            if (((handle->rxBuf[rxIdx] & 0xff00) == 0xc900) || 
                (handle->rxBuf[rxIdx] == 0xcd01))
            {
                Int lene, nele;
                Int lenp, nelp;
                Int k;
                if ((handle->rxBuf[rxIdx]&0xff00) == 0xc900)
                {
                    k=1;
                }
                else
                {
                    k=2;
                }
                lene = len-k;
                nele = 0;
                while (lene)
                {
                    lenp = handle->acp->fxns->length(handle->acp, &handle->rxBuf[rxIdx+k]);
                    nelp = handle->acp->fxns->htgnel(handle->acp, &handle->rxBuf[rxIdx+k]);
                    k += lenp;
                    lene -= lenp;
                    nele += nelp;
                }
                if (nele < 256)
                {
                    res_len = nele+1;
                }
                else
                {
                    res_len = nele+2;
                }
                rxOffset = 0;
                if (res_len > (handle->params->txBufSize-4>>1)-txIdx)
                {
                    handle->txBuf[txIdx++] = 0xc901;
                    handle->txBuf[txIdx++] = 0xdead;
                    AE_TRACE0("AFP_process:  0xDEAD: Response too big.");
                    //handle->fxns->log(handle,handle->logObj,
                    //    "AFP6: Response too big");
                    break;
                }
            }
            else if (len < 256)
            {
                handle->rxBuf[--rxIdx] = 0xc900+len;
                rxOffset = 1;
            }
            else
            {
                handle->rxBuf[--rxIdx] = len;
                handle->rxBuf[--rxIdx] = 0xcd01;
                rxOffset = 2;
            }

            
            /* Process alpha command */
            error = handle->acp->fxns->sequence(handle->acp,
                &handle->rxBuf[rxIdx],
                &handle->txBuf[txIdx]);

            
            if (error)
            {
                //handle->fxns->log(handle,handle->logObj,
                //    "AFP6: Sequence error");
                AE_TRACE0("AFP_process:  0xDEAD: Sequence error.");
                if (res_len || encap)
                {
                    handle->txBuf[txIdx++] = 0xdead;
                }
                cnt -= len;
                rxIdx += len+rxOffset;
                continue;
            }
            else if (rxOffset)
            {
                /* Delete encapsulation -- ACP limitation */
                if ((handle->txBuf[txIdx] & 0xff00) == 0xc900)
                {
                    txStrip = 1;
                }
                else if (handle->txBuf[txIdx] == 0xcd01)
                {
                    txStrip=2;
                }
                for (i=0; i<res_len; ++i)
                {
                    handle->txBuf[txIdx+i] = handle->txBuf[txIdx+i+txStrip];
                }
            }
            
            /* Update the count, rxIdx and txIdx */
            cnt -= len;
            rxIdx += len+rxOffset;
            txIdx += res_len;
        }
        
        /* Write the response */
        handle->txBuf[3-txOffset-1] = txIdx-3+txOffset;
        handle->dcs7->fxns->write(handle->dcs7,
            &handle->txBuf[3-txOffset-1],
            handle->params->txBufSize-(3-txOffset-1<<1));
    }
}

/*
 *  ======== taskAfpFxn ========
 *  Audio Function Processing task function
 */
Void taskAfpFxn(AFP_Handle handle)
{
    IALG_Status *pStatus;
    IHeap_Handle hHeap;
    void *memSeg;
    
    Log_info0("TaskAfp: Enter");

    if (handle == NULL)
    {
        handle = &AFP_OBJ;
    }

    /* Create an instance of ACP Algorithm */
    handle->fxns->acpInit();
    handle->acp = ACP_create(&ACP_MDS_IACP,handle->acpParams);
    if (handle->acp == NULL)
    {
        Log_info0("TaskAfp: ACP instance creation failed");
    }
    else
    {
        // Get pointer to ACP Status structure
        ((ALG_Handle)handle->acp)->fxns->algControl((IALG_Handle)(handle->acp),
            ACP_GETSTATUSADDRESS1, 
            (IALG_Status *)&pStatus);
        // Attach ACP Status structure to Beta Unit Id==STD_BETA_UART
        handle->acp->fxns->attach(handle->acp, 
            ACP_SERIES_STD, 
            STD_BETA_UART, 
            pStatus);

        // Attach ACP Status structure to Beta Unit Id==STD_BETA_UART
        handle->acp->fxns->attach(handle->acp, 
            ACP_SERIES_STD, 
            STD_BETA_BETATABLE, 
            (IALG_Status *)((ACP_MDS_Obj *)handle->acp)->config.betaTable[ACP_SERIES_STD]);
        // Attach STD Beta Table to Beta Unit Id==STD_BETA_BETATABLE
        handle->acp->fxns->attach(handle->acp, 
            ACP_SERIES_STD, 
            STD_BETA_PHITABLE, 
            (IALG_Status *)((ACP_MDS_Obj *)handle->acp)->config.phiTable[ACP_SERIES_STD]);
        // Attach STD Sigma Table to Beta Unit Id==STD_BETA_SIGMATABLE
        handle->acp->fxns->attach(handle->acp, 
            ACP_SERIES_STD, 
            STD_BETA_SIGMATABLE, 
            (IALG_Status *)((ACP_MDS_Obj *)handle->acp)->config.sigmaTable[ACP_SERIES_STD]);
            
        Log_info0("TaskAfp: ACP initialized");
        
        // Initialize DCS
        handle->dcs7 = handle->dcs7Fxns->open(handle->params->dcs7Params,
            handle->dcs7Config, 
            (DCS7_Fxns_Ptr)handle->dcs7Fxns,
            handle->acp);
        if (handle == NULL)
        {
            Log_info0("TaskAfp: DCS7 creation failed");
        }
        else
        {
            Log_info0("TaskAfp: DCS7 initialized");
            
            hHeap = pafHeapMgr_readHeapHandle(*handle->pBufHeapId);
            memSeg = (void *)&hHeap;
            handle->rxBuf = (Uint16 *)handle->dcs7Fxns->memAlloc(handle->dcs7,
                memSeg,
                handle->params->rxBufSize,
                4);
            handle->txBuf = (Uint16 *)handle->dcs7Fxns->memAlloc(handle->dcs7,
                memSeg,
                handle->params->txBufSize,
                4);
            if ((handle->rxBuf == NULL) || (handle->txBuf == NULL))
            {
                Log_info0("TaskAfp: Buffer allocation failed");
            }
            else
            {
                Log_info0("TaskAfp: Initialized");

#ifdef MEMSTAT_REPORT
                // Output current heap memory usage
                PAF_ALG_memStatusPrint("AFP MEMSTAT REPORT", 
                    PAF_HEAP_INT, PAF_HEAP_INT1, PAF_HEAP_EXT, 
                        PAF_HEAP_INT1_SHM, PAF_HEAP_EXT_SHM, PAF_HEAP_EXT_NONCACHED_SHM);
#endif /* MEMSTAT_REPORT */
                //gRunIdleMemStatRpt=1; // debug, allow Idle mem stat to run
               
                // Process alpha commands
                handle->fxns->process(handle);
            }
        }
    }

    /* Terminate AFP */
    handle->fxns->acpExit();
    Log_info0("TaskAfp: Exit");
    Task_setPri(TaskAfp, -1);
    Task_yield();
}

const AFP_Fxns AFP_DEFAULT_FXNS = {
//    AFP_log,
    AFP_memAlloc,
    AFP_process,
    ACP_MDS_init,
    ACP_MDS_exit
};

AFP_Obj AFP_OBJ = {
    sizeof(AFP_Obj),
    &AFP_DEFAULT_FXNS,
    &AFP_PARAMS,
    NULL,
    &DCS7_FXNS,
    NULL,
    NULL,
    (ACP_Params*)&ACP_PARAMS,
    NULL,
    NULL,
    (Int *)&gPafHeapIdExt,
    (Int *)&gPafHeapIdExt,
//    NULL,
};
