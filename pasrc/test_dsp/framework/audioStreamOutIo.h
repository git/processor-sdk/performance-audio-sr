
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamOutIo.h ========
 */

#ifndef _ASOP_IO_H_
#define _ASOP_IO_H_

#include <xdc/std.h>

#include "audioStreamOutProc.h"

// status codes
#define ASOP_IO_SOK                 (  0 )  // ok
#define ASOP_IO_ERR_INV_PARAMS      ( -1 )  // error, invalid parameters
#define ASOP_IO_ERR_IO_UNINIT       ( -2 )  // error, IO uninitialized
#define ASOP_IO_ERR_INV_STATE       ( -3 )  // error, IO invalid state
#define ASOP_IO_ERR_MCASP_CFG       ( -4 )  // error, McASP configuration
#define ASOP_IO_ERR_IOBUFF_INIT     ( -5 )  // error, IO Buff initialization
#define ASOP_IO_ERR_IOPHY_INIT      ( -6 )  // error, IO Phy initialization
#define ASOP_IO_ERR_OUTBUF_OVERFLOW ( -7 )  // error, Output buffer overflow
#define ASOP_IO_ERR_RATE_CHANGE     ( -8 )  // error, rateX change


// Select Output devices
Int asopSelectDevices(
    const PAF_SIO_Params *pOutCfg, 
    PAF_AST_IoOut *pOut
);

// Check if Output device SIO selection changed
Int checkOutDevSioSelUpdate(
    const PAF_ASOT_Params *pP, 
    PAF_ASOT_Config *pAsotCfg,
    Int z, 
    Bool *pOutDevSelUpdate
);

// Check if any Output device SIO selection changed
Int checkAnyOutDevSioSelUpdate(
    const PAF_ASOT_Params *pP, 
    PAF_ASOT_Config *pAsotCfg,
    Bool *pOutDevSelUpdate
);

// Re-initiate Output
Int asopSetCheckRateX(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int check);

// Initiate Output
Int asopStartOutput(
    const PAF_ASOT_Params *pP,
    const PAF_ASOT_Patchs *pQ,
    PAF_ASOT_Config *pAsotCfg
);

// Terminate Output
Int asopStopOutput(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
);

// Initialize I/O components for output processing
Int asopIoCompsInit(
    Int16 strAfSampleCount,     // stream audio frame sample count
    PAF_AST_OutBuf *pOutBuf,    // pointer to Output Buffer
    PAF_AST_IoOut *pOutIo       // pointer to Output IO
);

// Check I/O physical layer initialized
Bool asopIoPhyCheckInit(Void);

// I/O physical layer prime operation required by McASP LLD
Void asopIoPhyPrime(
    PAF_AST_IoOut *pOut
);

// Initialize Output buffer configuration
Int asopInitOutBufConfig(
    PAF_AST_OutBuf *pOutBuf, 
    PAF_AST_IoOut *pOutIo
);

// Get output buffer write pointers for encoder to write
Int asopGetOutBufPtrs(
    PAF_AST_IoOut *pOutIo,
    size_t writeSize
);

// Update Output buffer configuration
Int asopUpdateOutBufConfig(
    PAF_AST_OutBuf *pOutBuf, 
    PAF_AST_IoOut *pOutIo
);

// Mark Output buffers write complete
Int asopMarkOutBuffsWriteComplete(
    PAF_AST_OutBuf *pOutBuf, 
    PAF_AST_IoOut *pOutIo
);

// This function starts an I/O PHY transfer for output 
Void asopPhyTransferStart(
    PAF_AST_IoOut *pOut
);


#endif /* _ASOP_IO_H_ */
