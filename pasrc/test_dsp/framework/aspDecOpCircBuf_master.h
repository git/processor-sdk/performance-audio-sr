
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _ASP_DECOP_CB_MASTER_H_
#define _ASP_DECOP_CB_MASTER_H_

#include <xdc/std.h>
#include "paftyp.h"
#include "aspDecOpCircBuf_common.h"

#define ASP_DECOP_CB_AF_READ_UNDERFLOW      ( ASP_DECOP_CB_ERR_START-1 )    // error: read AF underflow
#define ASP_DECOP_CB_PCM_READ_UNDERFLOW     ( ASP_DECOP_CB_ERR_START-2 )    // error: read PCM underflow
#define ASP_DECOP_CB_READ_INVSTATE          ( ASP_DECOP_CB_ERR_START-3 )    // error: circular buffer invalid state on read

#define ASP_DECOP_CHECK_DRAINSTATE_ALL      ( -1 )                          // check circular buffer combined drain state

#define DEF_STR_FRAME_LEN                   ( PAF_SYS_FRAMELENGTH )         // default stream frame length

// Initialize circular buffer
Int cbInit(
    PAF_AST_DecOpCircBuf *pCb
);

#if 0 // FL: moved to ARM
// Initialize circular buffer for selected source
Int cbInitSourceSel(
    PAF_AST_DecOpCircBufCtl *pCbCtl,    // decoder output circular buffer control
    Int8 cbIdx,                         // decoder output circular buffer index
    Int8 sourceSelect,                  // source select (PCM, DDP, etc.)
    Int16 decOpFrameLen,                // decoder output frame length (PCM samples)
    Int16 strFrameLen,                  // stream frame length (PCM samples)
    Int8 resetRwFlags                   // whether to reset reader, writer, and drain flags
);
#endif

// Initialize circular buffer for Stream reads
Int cbInitStreamRead(
    PAF_AST_DecOpCircBufCtl *pCbCtl,    // decoder output circular buffer control
    Int8 cbIdx                          // decoder output circular buffer index
);

// Start reads from circular buffer
Int cbReadStart(
    PAF_AST_DecOpCircBufCtl *pCbCtl,    // decoder output circular buffer control
    Int8 cbIdx                          // decoder output circular buffer index
);

// Stop reads from circular buffer
Int cbReadStop(
    PAF_AST_DecOpCircBufCtl *pCbCtl,    // decoder output circular buffer control
    Int8 cbIdx                          // decoder output circular buffer index
);

// Read audio frame from circular buffer
Int cbReadAf(
    PAF_AST_DecOpCircBufCtl *pCbCtl,    // decoder output circular buffer control
    Int8 cbIdx,                         // decoder output circular buffer index
    PAF_AudioFrame *pAfRd               // audio frame into which to read
);

// Read CB stream frame length
Int cbReadStrFrameLen(
    PAF_AST_DecOpCircBufCtl *pCbCtl,    // decoder output circular buffer control
    Int8 cbIdx,                         // decoder output circular buffer index, or indicator combined drain state desired
    Int16 *pStrFrameLen                 // stream frame length
);

// Check circular buffer drain state
Int cbCheckDrainState(
    PAF_AST_DecOpCircBufCtl *pCbCtl,    // decoder output circular buffer control
    Int8 cbIdx,                         // decoder output circular buffer index, or indicator combined drain state desired
    Int8 *pDrainedFlag                  // output drain state indicator (combined or for selected circular buffer)
);


#endif /* _ASP_DECOP_CB_MASTER_H_ */
