
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <string.h>
#include <xdc/std.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/heaps/HeapBuf.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/SharedRegion.h>

#include "aspMsg_common.h"
#include "aspMsg_master.h"
#include "audioStreamProc_common.h"
#include "common.h"

// Gets ack command corresponding to send command
static UInt32 
getAckCmd(
    UInt32 sndCmd
);

/* Initialize ASP master messaging */
Int AspMsgMaster_init(
    AspMsgMaster_Handle hAspMsgMaster,  // message master handle
    UInt16 remoteProcId,                // remote processor Id
    UInt16 heapId,                      // heap Id for message pool
    UInt16 numMsgs,                     // number of messages in message pool
    SyncEvent_Handle syncEventHandle,   // sync event handle
    Event_Handle eventHandle,           // event handle
    UInt eventId                        // event Id
)
{
    Int             status = ASP_MSG_MASTER_SOK;
    Int             align;
    Error_Block     eb;
    IHeap_Handle    srHeap;
    HeapBuf_Params  heapParams;
    MessageQ_Params msgqParams;
    char            msgqName[32];
    UInt16          regionId;
    UInt8           i;

    Log_print0(Diags_ENTRY, "AspMsgMaster_init(): -->");
    Log_info0("AspMsgMaster_init(): -->");
    
    // set default values
    hAspMsgMaster->masterProcId = 0;
    hAspMsgMaster->slaveProcId = 0;
    hAspMsgMaster->masterQue = NULL;
    hAspMsgMaster->slaveQueId = MessageQ_INVALIDMESSAGEQ;
    hAspMsgMaster->heapId = heapId;
    hAspMsgMaster->numMsgs = numMsgs;
    hAspMsgMaster->heap = NULL;
    hAspMsgMaster->msgSize = 0;
    hAspMsgMaster->poolSize = 0;
    hAspMsgMaster->store = NULL;            // placed in Shared Region 0
    hAspMsgMaster->messageId = 0;
    hAspMsgMaster->saveMsgInfo = NULL;      // placed in Shared Region 0
    hAspMsgMaster->saveMsgWrtIdx = 0;
    hAspMsgMaster->saveMsgRdIdx = 0;
    hAspMsgMaster->numSaveMsgInfo = 0;
    hAspMsgMaster->syncEventHandle = NULL;
    hAspMsgMaster->eventHandle =  NULL;
    hAspMsgMaster->eventId = Event_Id_NONE;
    
    // set processor Ids
    hAspMsgMaster->masterProcId = MultiProc_self();
    hAspMsgMaster->slaveProcId = remoteProcId;
    
    // get IPC shared region Id
    regionId = SharedRegion_getIdByName("SR_0"); // message pool memory hard-coded in Shared Region 0

    // compute message size to fill entire cache lines
    align = SharedRegion_getCacheLineSize(regionId);
    hAspMsgMaster->msgSize = ROUNDUP(sizeof(AspMsg), align);
    
    // compute message pool size
    hAspMsgMaster->poolSize = hAspMsgMaster->msgSize * numMsgs;
    
    // acquire message pool memory
    srHeap = (IHeap_Handle)SharedRegion_getHeap(regionId);
    hAspMsgMaster->store = Memory_alloc(srHeap, hAspMsgMaster->poolSize, align, NULL);
    if (hAspMsgMaster->store == NULL)
    {
        Log_error0("AspMsgMaster_init(): failed allocating message store");
        status = ASP_MSG_MASTER_MSGPOOL_MALLOC_FAIL;
        Log_print1(Diags_INFO, "AspMsgMaster_init(): <-- status=%d", (IArg)status);
        return status;
    }

    // create a heap in shared memory for message pool
    HeapBuf_Params_init(&heapParams);
    heapParams.blockSize = hAspMsgMaster->msgSize;
    heapParams.numBlocks = numMsgs;
    heapParams.bufSize = hAspMsgMaster->poolSize;
    heapParams.align = align;
    heapParams.buf = hAspMsgMaster->store;
    Error_init(&eb);

    hAspMsgMaster->heap = HeapBuf_create(&heapParams, &eb);
    if (hAspMsgMaster->heap == NULL) 
    {
        Log_error0("AspMsgMaster_init(): failed creating heap for message pool");
        status = ASP_MSG_MASTER_MSGPOOL_HEAPBUF_CREATE_FAIL;
        Log_print1(Diags_INFO, "AspMsgMaster_init(): <-- status=%d", (IArg)status);
        return status;
    }

    // bind message pool to heapId
    status = MessageQ_registerHeap((Ptr)(hAspMsgMaster->heap), hAspMsgMaster->heapId);
    if (status != MessageQ_S_SUCCESS)
    {
        Log_error0("AspMsgMaster_init(): failed registering heap");
        status = ASP_MSG_MASTER_MSGQ_REGHEAP_FAIL;
        Log_print1(Diags_INFO, "AspMsgMaster_init(): <-- status=%d", (IArg)status);
        return status;        
    }

    // create local message queue (inbound messages)
    MessageQ_Params_init(&msgqParams);
    if (syncEventHandle != NULL)
    {
        // Store info related to SyncEvent (non-default) synchronizer.
        // It is assumed the SyncEvent was created using this Event and Event Id.
        hAspMsgMaster->syncEventHandle = syncEventHandle;
        hAspMsgMaster->eventHandle = eventHandle;
        hAspMsgMaster->eventId = eventId;
        
        // Initialize SyncEvent synchronizer for Master MessageQ creation.
        msgqParams.synchronizer = syncEventHandle;
    }

    hAspMsgMaster->masterQue = MessageQ_create(NULL, &msgqParams);
    if (hAspMsgMaster->masterQue == NULL) 
    {
        Log_error0("AspMsgMaster_init(): failed creating MessageQ");
        status = ASP_MSG_MASTER_MASTER_MSGQ_CREATE_FAIL;
        Log_print1(Diags_INFO, "AspMsgMaster_init(): <-- status=%d", (IArg)status);
        return status;
    }

    // open the remote message queue
    System_sprintf(msgqName, AspMsg_SlaveMsgQueName, MultiProc_getName(remoteProcId));
    Log_info0("AspMsgMaster_init(): MessageQ_open() start");
    do {
        status = MessageQ_open(msgqName, &hAspMsgMaster->slaveQueId);
        Log_info1("MessageQ_open() status=%d", status);
    } while (status == MessageQ_E_NOTFOUND);
    if (status < 0) 
    {
        Log_error0("AspMsgMaster_init(): failed opening MessageQ");
        status = ASP_MSG_MASTER_SLAVE_MSGQ_OPEN_FAIL;
        Log_print1(Diags_INFO, "AspMsgMaster_init(): <-- status=%d", (IArg)status);
        return status;
    }
    Log_info0("AspMsgMaster_init(): MessageQ_open() finish");

    // allocate and initialize send message Id array
    hAspMsgMaster->saveMsgInfo = Memory_alloc(srHeap, numMsgs * sizeof(AspMsgMaster_SaveMsgInfo), align, NULL); // send message Id array hard-coded in Shared Region 0
    if (hAspMsgMaster->saveMsgInfo != NULL)
    {
        for (i = 0; i < hAspMsgMaster->numMsgs; i++)
        {
            // mark as unused
            hAspMsgMaster->saveMsgInfo[i].sndCmd = ASP_SLAVE_NULL;
            hAspMsgMaster->saveMsgInfo[i].msgId = (UInt32)(1<<31);
        }
    }
    else
    {
        Log_error0("AspMsgMaster_init(): failed creating send message Id array");
        status = ASP_MSG_MASTER_SAVEMSGID_MALLOC_FAIL;
        Log_print1(Diags_INFO, "AspMsgMaster_init(): <-- status=%d", (IArg)status);
        return status;
    }

    Log_print0(Diags_INFO, "AspMsgMaster_init(): ASP Master messaging ready");
    Log_info0("AspMsgMaster_init(): ASP Master messaging ready");
    
    Log_print1(Diags_EXIT, "<-- AspMsgMaster_init(): %d", (IArg)status);    
    Log_info1("<-- AspMsgMaster_init(): %d", (IArg)status);
    
    return ASP_MSG_MASTER_SOK;
}

// ASP message send function
Int AspMsgSnd(
    AspMsgMaster_Handle hAspMsgMaster,  // message master handle
    UInt32 sndCmd,                      // command sent from master to slave
    char *sndMsgBuf                     // message buffer for message sent from master to slave
)
{
    AspMsg *pAspMsg;
    Int status;

    // allocate message
    pAspMsg = (AspMsg *)MessageQ_alloc(hAspMsgMaster->heapId, hAspMsgMaster->msgSize);
    if (pAspMsg == NULL) 
    {
        Log_info0("AspMsgSnd(): MessageQ_alloc() failure.");
        return ASP_MSG_ERR_QUEUE_ALLOC;
    }

    // set return queue in the message header and fill in message payload
    MessageQ_setReplyQueue(hAspMsgMaster->masterQue, (MessageQ_Msg)pAspMsg);
    pAspMsg->procId = hAspMsgMaster->masterProcId;
    pAspMsg->messageId = hAspMsgMaster->messageId;
    pAspMsg->cmd = sndCmd;

    // copy the message provided by caller
    if (sndMsgBuf != NULL) 
    {
        memcpy(pAspMsg->buf, sndMsgBuf, ASP_MSG_BUF_LEN*sizeof(Char));
    }

    // send the message
    Log_info3("AspMsgSnd(): Tx message: procId=%d, messageId=0x%04x, cmd=%d", pAspMsg->procId, pAspMsg->messageId, pAspMsg->cmd);
    status = MessageQ_put(hAspMsgMaster->slaveQueId, (MessageQ_Msg)pAspMsg);
    if (status != MessageQ_S_SUCCESS) 
    {
        Log_info0("AspMsgSnd(): MessageQ_put() failure.");
        MessageQ_free((MessageQ_Msg)pAspMsg);
        return ASP_MSG_ERR_QUEUE_PUT;
    }

    // Save message info for ack message validation.
    //  Expect ack messages in same order as sent messages.
    //  Multiple messages can be sent between response messages.
    hAspMsgMaster->saveMsgInfo[hAspMsgMaster->saveMsgWrtIdx].sndCmd = sndCmd;
    hAspMsgMaster->saveMsgInfo[hAspMsgMaster->saveMsgWrtIdx].msgId = hAspMsgMaster->messageId;
    hAspMsgMaster->saveMsgWrtIdx++;
    if (hAspMsgMaster->saveMsgWrtIdx >= hAspMsgMaster->numMsgs)
    {
        hAspMsgMaster->saveMsgWrtIdx = 0;
    }
    hAspMsgMaster->numSaveMsgInfo++;
    if (hAspMsgMaster->numSaveMsgInfo > hAspMsgMaster->numMsgs)
    {
        Log_info0("AspMsgSnd(): Tx message overflow.");
        MessageQ_free((MessageQ_Msg)pAspMsg);
        return ASP_MSG_ERR_SEND_OVR;
    }

    // increment message Id for next message to send
    hAspMsgMaster->messageId = (hAspMsgMaster->messageId + 1) & ~(1<<31); // mask MSB, message Id is LS 31 bits
    
    return ASP_MSG_NO_ERR;
} /* AspMsgSnd */

// Message receive acknowledge function
//  Expected acknowledge is an input parameter.
//  Expected acknowledge is compared against acknowledge for least recently sent command.
Int                                     // returned status
AspMsgRcvAck(
    AspMsgMaster_Handle hAspMsgMaster,  // message master handle
    UInt32 ackCmd,                      // expected acknowledgment sent from slave to master
    char *ackMsgBuf,                    // message buffer for acknowledgment message sent from slave to master
    Bool pendOnEvent                    // whether to pend on Event synchronizer
)
{
    AspMsg *pAspMsg;
    Bool validMsg;
    Int status;
    UInt32 saveAckCmd;

    if (hAspMsgMaster->syncEventHandle == NULL)
    {
        // wait for complete message from slave
        // pend on default Semaphore synchronizer
        // read message upon sync
        status = MessageQ_get(hAspMsgMaster->masterQue, (MessageQ_Msg *)&pAspMsg, MessageQ_FOREVER);    
        if (status != MessageQ_S_SUCCESS)
        {
            Log_info0("AspMsgRcvAck(): MessageQ_get() failure.");
            if (pAspMsg != NULL)
            {
                MessageQ_free((MessageQ_Msg)pAspMsg);
            }
            return ASP_MSG_ERR_QUEUE_GET;
        }
    }
    else
    {
        if (pendOnEvent == TRUE)
        {
            // wait for complete message from slave
            // pend on non-default SyncEvent synchronizer
            Event_pend(hAspMsgMaster->eventHandle, Event_Id_NONE, hAspMsgMaster->eventId, BIOS_WAIT_FOREVER);
        }
        else
        {
            // Sync already performed outside this function or polling
            ;
        }
        
        // Read message from MessageQ
        status = MessageQ_get(hAspMsgMaster->masterQue, (MessageQ_Msg *)&pAspMsg, 0);
        if (status != MessageQ_S_SUCCESS)
        {
            if (status == MessageQ_E_TIMEOUT)
            {
                Log_info0("AspMsgRcvAck(): MessageQ_get() timeout.");
                return ASP_MSG_ERR_QUEUE_TIMEOUT;
            }
            else
            {
                Log_info0("AspMsgRcvAck(): MessageQ_get() failure.");
                if (pAspMsg != NULL)
                {
                    MessageQ_free((MessageQ_Msg)pAspMsg);
                }
                return ASP_MSG_ERR_QUEUE_GET;
            }
        }
    }
    
    // check if returned message is valid
    validMsg = ((pAspMsg->procId == hAspMsgMaster->slaveProcId) &&  // check ack message from slave
                (pAspMsg->messageId & (UInt32)(1<<31)));            // check message ack bit set by slave
    if (validMsg == FALSE)
    {
        Log_info3("AspMsgRcvAck(): Rx Message ACK ERROR: procId=%d, messageId=0x%04x, cmd=%d", pAspMsg->procId, pAspMsg->messageId, pAspMsg->cmd);
        MessageQ_free((MessageQ_Msg)pAspMsg);
        return ASP_MSG_ERR_ACKNOWLEDGE;
    }

    // Determine ack command for least recently sent message
    saveAckCmd = getAckCmd(hAspMsgMaster->saveMsgInfo[hAspMsgMaster->saveMsgRdIdx].sndCmd);
    
    // Compare expected ack command against ack command of least recently sent message
    validMsg = (ackCmd == saveAckCmd);
    if (validMsg == FALSE)
    {
        TRACE_TERSE2("AspMsgRcvAck(): Rx Message ACK ERROR: expect ackCmd=%d, ack for oldest message=%d", ackCmd, saveAckCmd);
        MessageQ_free((MessageQ_Msg)pAspMsg);
        return ASP_MSG_ERR_ACKNOWLEDGE;
    }
    
    // Compare returned message ack command & Id against ack command & Id of least recently sent message
    validMsg = ((pAspMsg->cmd == saveAckCmd) && 
                ((pAspMsg->messageId & (UInt32)~(1<<31)) == hAspMsgMaster->saveMsgInfo[hAspMsgMaster->saveMsgRdIdx].msgId));
    if (validMsg == TRUE)
    {
        // mark location unused
        hAspMsgMaster->saveMsgInfo[hAspMsgMaster->saveMsgRdIdx].sndCmd = ASP_SLAVE_NULL;
        hAspMsgMaster->saveMsgInfo[hAspMsgMaster->saveMsgRdIdx].msgId = (UInt32)(1<<31);
        
        hAspMsgMaster->saveMsgRdIdx++;
        if (hAspMsgMaster->saveMsgRdIdx >= hAspMsgMaster->numMsgs)
        {
            hAspMsgMaster->saveMsgRdIdx = 0;
        }
        hAspMsgMaster->numSaveMsgInfo--;
        if (hAspMsgMaster->numSaveMsgInfo < 0)
        {
            Log_info0("AspMsgRcvAck(): Rx message underflow.");
            MessageQ_free((MessageQ_Msg)pAspMsg);
            return ASP_MSG_ERR_RECEIVE_UND;
        }
    }
    else
    {
        TRACE_TERSE3("AspMsgRcvAck(): Rx Message ACK ERROR: procId=%d, messageId=0x%04x, cmd=%d", pAspMsg->procId, pAspMsg->messageId, pAspMsg->cmd);
        MessageQ_free((MessageQ_Msg)pAspMsg);
        return ASP_MSG_ERR_ACKNOWLEDGE;
    }

    // get the returned message
    if (ackMsgBuf != NULL) 
    {
        memcpy(ackMsgBuf, pAspMsg->buf, ASP_MSG_BUF_LEN*sizeof(char));
    }

    // free the message 
    status = MessageQ_free((MessageQ_Msg)pAspMsg);
    if (status != MessageQ_S_SUCCESS) 
    {
        Log_info0("AspMsgRcvAck(): MessageQ_free() failure.");
        return ASP_MSG_ERR_QUEUE_FREE;
    }

    // No error in messaging operation, even though there
    // may be error in returned (acknowledgment) message.
    return ASP_MSG_NO_ERR;
} /* AspMsgRcvAck */

// Get number of outstanding messages function
Int                                     // returned status
AspMsgGetNumSndMsgUnack(
    AspMsgMaster_Handle hAspMsgMaster,  // message master handle
    UInt16 *pNumSndMsgUnack             // number of unacknowledged messages sent to the slave
)
{
    *pNumSndMsgUnack = hAspMsgMaster->numSaveMsgInfo;
    
    return ASP_MSG_NO_ERR;
}

// Message receive acknowledge function
//  Received acknowledge is an output parameter.
Int                                     // returned status
AspMsgRcvAckUnk(
    AspMsgMaster_Handle hAspMsgMaster,  // message master handle
    UInt32 *pAckCmd,                    // acknowledgment sent from slave to master
    char *ackMsgBuf,                    // message buffer for acknowledgment message sent from slave to master
    Bool pendOnEvent                    // whether to pend on Event synchronizer
)
{
    AspMsg *pAspMsg;
    Bool validMsg;
    Int status;
    UInt32 saveAckCmd;

    if (hAspMsgMaster->syncEventHandle == NULL)
    {
        // wait for complete message from slave
        // pend on default Semaphore synchronizer
        // read message upon sync
        status = MessageQ_get(hAspMsgMaster->masterQue, (MessageQ_Msg *)&pAspMsg, MessageQ_FOREVER);    
        if (status != MessageQ_S_SUCCESS)
        {
            Log_info0("AspMsgRcvAck(): MessageQ_get() failure.");
            if (pAspMsg != NULL)
            {
                MessageQ_free((MessageQ_Msg)pAspMsg);
            }
            return ASP_MSG_ERR_QUEUE_GET;
        }
    }
    else
    {
        if (pendOnEvent == TRUE)
        {
            // wait for complete message from slave
            // pend on non-default SyncEvent synchronizer
            Event_pend(hAspMsgMaster->eventHandle, Event_Id_NONE, hAspMsgMaster->eventId, BIOS_WAIT_FOREVER);
        }
        else
        {
            // Sync already performed outside this function or polling
            ;
        }
        
        // Read message from MessageQ
        status = MessageQ_get(hAspMsgMaster->masterQue, (MessageQ_Msg *)&pAspMsg, 0);
        if (status != MessageQ_S_SUCCESS)
        {
            if (status == MessageQ_E_TIMEOUT)
            {
                Log_info0("AspMsgRcvAck(): MessageQ_get() timeout.");
                return ASP_MSG_ERR_QUEUE_TIMEOUT;
            }
            else
            {
                Log_info0("AspMsgRcvAck(): MessageQ_get() failure.");
                if (pAspMsg != NULL)
                {
                    MessageQ_free((MessageQ_Msg)pAspMsg);
                }
                return ASP_MSG_ERR_QUEUE_GET;
            }
        }
    }
    
    // check if returned message is valid
    validMsg = ((pAspMsg->procId == hAspMsgMaster->slaveProcId) &&  // check ack message from slave
                (pAspMsg->messageId & (UInt32)(1<<31)));            // check message ack bit set by slave
    if (validMsg == FALSE)
    {
        Log_info3("AspMsgRcvAck(): Rx Message ACK ERROR: procId=%d, messageId=0x%04x, cmd=%d", pAspMsg->procId, pAspMsg->messageId, pAspMsg->cmd);
        MessageQ_free((MessageQ_Msg)pAspMsg);
        return ASP_MSG_ERR_ACKNOWLEDGE;
    }

    // Determine ack command for least recently sent message
    saveAckCmd = getAckCmd(hAspMsgMaster->saveMsgInfo[hAspMsgMaster->saveMsgRdIdx].sndCmd);
    
#if 0
    // Compare expected ack command against ack command of least recently sent message
    validMsg = (ackCmd == saveAckCmd);
    if (validMsg == FALSE)
    {
        TRACE_TERSE2("AspMsgRcvAck(): Rx Message ACK ERROR: expect ackCmd=%d, ack for oldest message=%d", ackCmd, saveAckCmd);
        MessageQ_free((MessageQ_Msg)pAspMsg);
        return ASP_MSG_ERR_ACKNOWLEDGE;
    }
#endif
    
    // Compare returned message ack command & Id against ack command & Id of least recently sent message
    validMsg = ((pAspMsg->cmd == saveAckCmd) && 
                ((pAspMsg->messageId & (UInt32)~(1<<31)) == hAspMsgMaster->saveMsgInfo[hAspMsgMaster->saveMsgRdIdx].msgId));
    if (validMsg == TRUE)
    {
        // mark location unused
        hAspMsgMaster->saveMsgInfo[hAspMsgMaster->saveMsgRdIdx].sndCmd = ASP_SLAVE_NULL;
        hAspMsgMaster->saveMsgInfo[hAspMsgMaster->saveMsgRdIdx].msgId = (UInt32)(1<<31);
        
        hAspMsgMaster->saveMsgRdIdx++;
        if (hAspMsgMaster->saveMsgRdIdx >= hAspMsgMaster->numMsgs)
        {
            hAspMsgMaster->saveMsgRdIdx = 0;
        }
        hAspMsgMaster->numSaveMsgInfo--;
        if (hAspMsgMaster->numSaveMsgInfo < 0)
        {
            Log_info0("AspMsgRcvAck(): Rx message underflow.");
            MessageQ_free((MessageQ_Msg)pAspMsg);
            return ASP_MSG_ERR_RECEIVE_UND;
        }
    }
    else
    {
        TRACE_TERSE3("AspMsgRcvAck(): Rx Message ACK ERROR: procId=%d, messageId=0x%04x, cmd=%d", pAspMsg->procId, pAspMsg->messageId, pAspMsg->cmd);
        MessageQ_free((MessageQ_Msg)pAspMsg);
        return ASP_MSG_ERR_ACKNOWLEDGE;
    }

    // get the returned message
    if (pAckCmd != NULL)
    {
        *pAckCmd = pAspMsg->cmd;
    }
    
    if (ackMsgBuf != NULL) 
    {
        memcpy(ackMsgBuf, pAspMsg->buf, ASP_MSG_BUF_LEN*sizeof(char));
    }

    // free the message 
    status = MessageQ_free((MessageQ_Msg)pAspMsg);
    if (status != MessageQ_S_SUCCESS) 
    {
        Log_info0("AspMsgRcvAck(): MessageQ_free() failure.");
        return ASP_MSG_ERR_QUEUE_FREE;
    }

    // No error in messaging operation, even though there
    // may be error in returned (acknowledgment) message.
    return ASP_MSG_NO_ERR;
} /* AspMsgRcvAckUnk */

// Gets ack command corresponding to send command
static UInt32 
getAckCmd(
    UInt32 sndCmd
)
{
    return sndCmd+ASP_SLAVE_NCOMMANDS;
}
