
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== systemStream.h ========
 */

#ifndef _SYSTEM_STREAM_H_
#define _SYSTEM_STREAM_H_

#include <xdc/std.h>
#include <acp.h>
#include "pafsys.h"

#define PAF_SYSTEMSTREAMFXNS_XN 4

struct PAF_SST_Params;
struct PAF_SST_Config;

typedef void PAF_SST_FxnsMain 
    (const struct PAF_SST_Params *, struct PAF_SST_Config *);
typedef Int PAF_SST_FxnsCompute 
    (const struct PAF_SST_Params *, struct PAF_SST_Config *, Int64 *);
typedef Int PAF_SST_FxnsTransmit 
    (const struct PAF_SST_Params *, struct PAF_SST_Config *, Int64 *);

// System Stream parameters, functions
typedef struct PAF_SST_Fxns {
    PAF_SST_FxnsMain *main;
    Int32 count;
    struct {
        PAF_SST_FxnsCompute  *compute;
        PAF_SST_FxnsTransmit *transmit;
    } sub[1];
} PAF_SST_Fxns;

// System Stream parameters
typedef struct PAF_SST_Params {
    Int8 streams;
    Int8 stream1; /* unused */
    Int8 streamN; /* unused */
    Int8 ss;
    const PAF_SST_Fxns *fxns;
} PAF_SST_Params;

// System Stream configuration
typedef struct PAF_SST_Config {
    UInt8 firstTimeInit;
    ACP_Handle acp;
    PAF_SystemStatus *pStatus;
} PAF_SST_Config;

typedef struct PAF_ERR_Config {
    ACP_Handle acp;
    PAF_ErrorStatus *pStatus;
} PAF_ERR_Config;

/* Audio Stream functions */
extern PAF_SST_FxnsMain     systemStreamMain;
extern PAF_SST_FxnsCompute  systemStream1Compute;
extern PAF_SST_FxnsTransmit systemStream1Transmit;
extern PAF_SST_FxnsCompute  systemStream2Compute;
extern PAF_SST_FxnsTransmit systemStream2Transmit;
extern PAF_SST_FxnsCompute  systemStream3Compute;
extern PAF_SST_FxnsTransmit systemStream3Transmit;
#ifdef THX
extern PAF_SST_FxnsCompute  systemStream4Compute;
extern PAF_SST_FxnsTransmit systemStream4Transmit;
#endif
extern PAF_SST_FxnsCompute  systemStream5Compute;
extern PAF_SST_FxnsTransmit systemStream5Transmit;

#endif /* _SYSTEM_STREAM_H_ */
