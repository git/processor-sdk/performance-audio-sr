
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamInpProc.h ========
 */

#ifndef _ASIP_H_
#define _ASIP_H_
 
#include <xdc/std.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/knl/Task.h>

#include "aspMsg_master.h"
#include "audioStreamProc_params.h"
#include "audioStreamProc_patchs.h"
#include "audioStreamProc_config.h"
#include "audioStreamProc_master.h"
#include "statusOp_common.h"
#include "ioPhy.h"
#include "ioBuff.h"
#include "ioData.h"

struct PAF_ASIT_Params;
struct PAF_ASIT_Patchs;
struct PAF_ASIT_Config;

// Audio Stream Input Task (ASIT) parameters, functions
typedef struct PAF_ASIT_Fxns {
    Int (*initPhase[8]) (const struct PAF_ASIT_Params *, const struct PAF_ASIT_Patchs *, struct PAF_ASIT_Config *);
    //Int (*initFrame0) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, Int);
    //Int (*initFrame1) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, Int, Int);
    Int (*passProcessing) (const struct PAF_ASIT_Params *, const struct PAF_ASIT_Patchs *, struct PAF_ASIT_Config *, Int);
    //Int (*passProcessingCopy) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *);
    Int (*autoProcessing) (const struct PAF_ASIT_Params *, const struct PAF_ASIT_Patchs *, struct PAF_ASIT_Config *, Int, ALG_Handle);
    Int (*decodeProcessing) (const struct PAF_ASIT_Params *, const struct PAF_ASIT_Patchs *, struct PAF_ASIT_Config *, Int);    
    Int (*decodeCommand) (const struct PAF_ASIT_Params *, const struct PAF_ASIT_Patchs *, struct PAF_ASIT_Config *);
    Int (*decodeInit) (const struct PAF_ASIT_Params *, const struct PAF_ASIT_Patchs *, struct PAF_ASIT_Config *, Int);
    Int (*decodeInfo) (const struct PAF_ASIT_Params *, const struct PAF_ASIT_Patchs *, struct PAF_ASIT_Config *, Int, Int);
    Int (*decodeInfo1) (const struct PAF_ASIT_Params *, const struct PAF_ASIT_Patchs *, struct PAF_ASIT_Config *, Int, Int);
    Int (*decodeInfo2) (const struct PAF_ASIT_Params *, const struct PAF_ASIT_Patchs *, struct PAF_ASIT_Config *, Int, Int);
    //Int (*decodeCont) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, ALG_Handle *, Int, Int);
    Int (*decodeDecode) (const struct PAF_ASIT_Params *, const struct PAF_ASIT_Patchs *, struct PAF_ASIT_Config *, Int, Int, Int);
    Int (*decodeFinalTest) (const struct PAF_ASIT_Params *, const struct PAF_ASIT_Patchs *, struct PAF_ASIT_Config *, Int, Int);    
    Int (*decodeComplete) (const struct PAF_ASIT_Params *, const struct PAF_ASIT_Patchs *, struct PAF_ASIT_Config *, ALG_Handle *, Int, Int);
    Int (*selectDevices) (const struct PAF_ASIT_Params *, const struct PAF_ASIT_Patchs *, struct PAF_ASIT_Config *);
    Int (*sourceDecode) (const struct PAF_ASIT_Params *, const struct PAF_ASIT_Patchs *, struct PAF_ASIT_Config *, Int);    
    Int (*deviceAllocate) (SIO_Handle *, int, int, int, Ptr);
    Int (*deviceSelect) (SIO_Handle *, int, int, Ptr);
    Int (*computeFrameLength) (Int, Int, Int);
    Int (*updateInputStatus) (SIO_Handle, PAF_InpBufStatus *, PAF_InpBufConfig *);
    //Int (*copy) (Uns, PAF_InpBufConfig *, Uns, PAF_OutBufConfig *);

    // For RAM_report
    Void (*headerPrint)();
    Int  (*allocPrint)(const PAF_ALG_AllocInit *pInit, Int sizeofInit, PAF_IALG_Config *p);
    Void (*commonPrint)(IALG_MemRec common[], PAF_IALG_Config *p);
    Void (*bufMemPrint)(Int z, Int size, Int heapId, Int bufType);
    Void (*memStatusPrint)(
        CString header, 
        HeapMem_Handle hInternalHeap, 
        HeapMem_Handle hInternal1Heap, 
        HeapMem_Handle hExternalHeap, 
        HeapMem_Handle hInternal1HeapShm, 
        HeapMem_Handle hExternalHeapShm, 
        HeapMem_Handle hExternalNoncachedHeapShm);
} PAF_ASIT_Fxns;

// Audio Stream Input Task (ASIT) parameters
typedef struct PAF_ASIT_Params {
    const PAF_ASIT_Fxns *fxns;
    struct {
        SmInt master;
        SmInt inputs;
        SmInt input1;
        SmInt inputN;
        SmInt decodes;
        SmInt decode1;
        SmInt decodeN;
        SmInt streams;
        SmInt stream1; /* unused */
        SmInt streamN; /* unused */
        SmInt encodes;
        SmInt encode1;
        SmInt encodeN;
        SmInt outputs;
        SmInt output1;
        SmInt outputN;
    } zone;
    const SmInt *inputsFromDecodes;
    const SmInt *outputsFromEncodes;
    struct {
        int *pHeapIdIntern;             // INT memory heap ID
        int *pHeapIdExtern;             // EXT memory heap ID
        int *pHeapIdInpbuf;             // IB buffer heap ID
        int *pHeapIdOutbuf;             // OB buffer heap ID
        int *pHeapIdFrmbuf;             // Frame buffer heap ID
        int *pHeapIdIntern1;            // INT1 memory heap ID
        int *pHeapIdInt1Shm;            // INT1 SHared Memory heap ID
        int *pHeapIdExtShm;             // EXT SHared Memory heap ID
        int *pHeapIdExtNonCachedShm;    // EXT Non-Cached SHared Memory heap ID
        int clear; 
    } heap;
    struct {
        const IALG_MemSpace *space;
    } common;
    const LgInt *z_rx_bufsiz;
    const LgInt *z_tx_bufsiz;
    const SmInt *z_numchan;
    MdInt framelength;
    const PAF_AudioFunctions *pAudioFrameFunctions;
    const struct PAF_ASP_ChainFxns *pChainFxns;
    const PAF_InpBufStatus *pInpBufStatus;
    const PAF_DecodeStatus * const *z_pDecodeStatus;
    const PAF_OutBufStatus *pOutBufStatus;
    const PAF_EncodeStatus * const *z_pEncodeStatus;
    const PAF_VolumeStatus *pVolumeStatus;
    const PAF_ASP_AlgKey *pDecAlgKey;
    const PAF_ASP_AlgKey *pEncAlgKey;
    const PAF_ASP_SioMap *pDecSioMap;
    const SmInt *streamsFromDecodes;
    const SmInt *streamsFromEncodes;
    const MdInt maxFramelength;
    const SmInt *streamOrder;
    const PAF_ASP_LinkInit * const (*i_inpLinkInit);
    const PAF_ASP_LinkInit * const (*i_outLinkInit);
    const PAF_ASP_outNumBufMap *  const (*poutNumBufMap);
    const PAF_MetadataBufStatus *pMetadataBufStatus;
    const PAF_AudioFrameBufStatus *pAudioFrameBufStatus;
    const PAF_AST_DecOpCircBufStatus * const *z_pDecOpCircBufStatus;    
} PAF_ASIT_Params;

// Audio Stream Input Task (ASIT) patchs
typedef struct PAF_ASIT_Patchs {
    const PAF_SIO_ParamsN * devinp;
    //const PAF_SIO_ParamsN * devout;
    //const PAF_ASP_LinkInit * const (*i_decLinkInit);
    //const PAF_ASP_LinkInit * const (*i_aspLinkInit)[GEARS];
    //const PAF_ASP_LinkInit * const (*i_encLinkInit);
} PAF_ASIT_Patchs;

enum {
    ASIP_NO_ERR,
    ASIP_ERR_AUTO_DETECION,
    ASIP_ERR_NO_MATCHING_SOURCE,
    ASIP_ERR_SWITCH_TO_PCM,
    ASIP_ERR_D10_CFG,
    ASIP_ERR_MCASP_CFG,
    ASIP_ERR_INPUT_CFG,
    ASIP_ERR_DECODE_INIT,
    ASIP_ERR_DECODE_COMMAND,
    ASIP_ERR_DECODE_INFO1,
    ASIP_ERR_DECODE_INFO2,
    ASIP_ERR_DECODE_DATA,
    ASIP_ERR_DECODE_FINAL,
    ASIP_ERR_DECODE_COMPLETE,
    ASIP_ERR_DECODE_MSG,
    ASIP_ERR_DECODE_QUIT,
    ASIP_ERR_ABORT
};


enum {
    ASIT_RESET,
    ASIT_SOURCE_DETECTION,
    ASIT_PCM_TRANSITION,
    ASIT_DECODE_PROCESSING
};

enum {
    ASIT_NO_ERR,
    ASIT_AUTODET_TIME_OUT,
    ASIT_ERR_IOBUFF_INIT,
    ASIT_ERR_IODATA_INIT,
    ASIT_ERR_IOPHY_INIT,
    ASIT_ERR_INPDATA_PROC,
    ASIT_ERR_INPBUF_UNDERFLOW,
    ASIT_ERR_AUTO_DETECION,
    ASIT_ERR_NO_MATCHING_SOURCE,
    ASIT_ERR_SWITCH_TO_PCM,
    ASIT_ERR_D10_CFG,
    ASIT_ERR_MCASP_CFG,
    ASIT_ERR_INPUT_CFG,
    ASIT_ERR_DECODE_INIT,
    ASIT_ERR_DECODE,
    ASIT_ERR_DECODE_COMMAND,
    ASIT_ERR_DECODE_INFO1,
    ASIT_ERR_DECODE_INFO2,
    ASIT_ERR_DECODE_DATA,
    ASIT_ERR_DECODE_FINAL,
    ASIT_ERR_DECODE_COMPLETE,
    ASIT_ERR_DECODE_MSG,
    ASIT_ERR_DECODE_QUIT,
    ASIP_ERR_DECODE_ABORT,
    ASIT_ERR_EVENTS,
    ASIT_ERR_ABORT
};

enum {
    DEC_NO_ERR,
    DEC_ERR_SOURCE_NONE,
    DEC_ERR_ASPERR_ABORT,
    DEC_ERR_ASPERR_QUIT,
    DEC_ERR_WRONG_MSG,
    DEC_ERR_INFO_SNDMSG,
    DEC_ERR_INFO_ACKMSG,
    DEC_ERR_DECODE_SNDMSG,
    DEC_ERR_DECODE_ACKMSG,
    DEC_ERR_DECODE_FINAL,
    DEC_ERR_COMPLETE_MSG,
    DEC_ERR_COMPLETE_ACKMSG, 
    DEC_ERR_COMPLETE_INFO_ACKMSG,
    DEC_ERR_COMPLETE_DECODE_ACKMSG,
	DEC_ERR_COMPLETE_RESET_ACKMSG
};


#define DEC_MSGMSK_INPDATA   0x1 // Input data message
#define DEC_MSGMSK_RXACK     0x2 // Receive acknowledge message
#define DEC_MSGMSK_INFOACK   0x4 // temp, will be removed
#define DEC_MSGMSK_DECACK    0x8 // temp, will be removed


// Input I/O structure
typedef struct PAF_AST_InpIO {
    ioPhyHandle_t        hIoPhy;     /* handle to I/O physical layer */
    ioBuffHandle_t       hIoBuff;    /* handle to I/O buffer management */
    ioDataHandle_t       hIoData;    /* handle to I/O data processing */
    Ptr                  hMcaspChan; /* handle to McASP LLD channel */
    const void           *pRxParams; /* pointer to D10 Rx Params */

    Int sourceSelect;
    Int sourceProgram;

    Int stride;
    Int       preSyncState;
    Int       numPrimeXfers;
    Int       mcaspXferErr;
/*
    void     *mcaspRxBuf1;
    void     *mcaspRxBuf2;
    uint32_t  mcaspRxSize1;
    uint32_t  mcaspRxSize2;
    Int       mcaspRxtwoXfers;
*/
    // debugging counters
    //uint32_t numXferStart;
    //uint32_t numXferFinish;
    //uint32_t numXferInterm;
    uint32_t numInputOverrun;
    uint32_t numUnderflow;
    uint32_t numAsitRestart;
    uint32_t numAsitDecodeQuit;
    uint32_t numFrameReceived;
    uint32_t numPcmFrameReceived;

    size_t         phyXferSize;
    int_fast32_t   pcmSwitchHangOver;
    uint_least16_t asipState;
    //uint_least16_t asipProcState;
    bool           buffReadComplete;
    bool           swapData;
    bool           firstTimeInit;
} PAF_AST_IoInp;

#define DEC_INPBUF_CONFIG_QUEUE_SIZE 8
// Decoder structure
typedef struct asipDecProc_s {
    Int state;
    Int frame;
    Int block;

    Int majorAuFound;
    Int initDone;
    Int decodeAckDelayed;

    PAF_InpBufConfig inpBufConfigQueue[DEC_INPBUF_CONFIG_QUEUE_SIZE];
    Int rdidx;
    Int wridx;
} asipDecProc_t;

// Audio Stream Input Task (ASIT) configuration
typedef struct PAF_ASIT_Config {
    Task_Handle taskHandle;             // ASIT handle
    ACP_Handle acp;                     // ASIT local ACP handle
    AspMsgMaster_Handle hAspMsgMaster;  // ASIT message master handle
    PAF_ASPM_Config *pAspmCfg;          // ASIT/ASOT shared configuration
    PAF_AST_Config *pAstCfg;            // ASIT/ASOT/ASDT shared configuration
    PAF_AST_IoInp *pIoInp;              // ASIT IO configuration
    asipDecProc_t inpDec;
} PAF_ASIT_Config;


// ASIT event IDs
#define ASIT_EVTMSK_NONE        0x0
#define ASIT_EVTMSK_INPDATA     0x1 // Input data (Rx McASP EDMA) event
#define ASIT_EVTMSK_RXACK       0x2 // Receive acknowledge (IPC MessageQ) message event
#define ASIT_EVTMSK_INFOACK     0x4 // temp, will be removed
#define ASIT_EVTMSK_DECACK      0x8 // temp, will be removed

// ASIT event handle
extern Event_Handle gAsitEvtHandle;

// ASIT Sync event handle
extern SyncEvent_Handle gAsitSyncEvtHandle;

// ASIT ASP messaging
#define ASIT_ASP_MSG_HEAP_ID        ( 0 )                           // ASIT message master heap Id
#define ASIT_ASP_MSG_MASTER_NUMSGS  ( ASP_MSG_MASTER_DEF_NUMMSGS )  // ASIT message master number of messages

// ASIT configuration
extern PAF_ASIT_Config gPAF_ASIT_config;


//   Purpose:   Audio Stream Input Task Function for initialization of data pointers
//              by allocation of memory.
Int 
PAF_ASIT_initPhaseMalloc(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
);

//   Purpose:   Audio Stream Input Task Function for initialization of data values
//              from parameters.
Int
PAF_ASIT_initPhaseConfig(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
);

//   Purpose:   Audio Stream Input Task Function for initialization of ACP by
//              instantiation of the algorithm.
Int
PAF_ASIT_initPhaseAcpAlg(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
);

//   Purpose:   Audio Stream Input Task Function for allocation of common memory.
Int
PAF_ASIT_initPhaseCommon(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
);

//   Purpose:   Audio Stream Input Task Function for initialization of ASP algorithms.
Int
PAF_ASIT_initPhaseAspAlg(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
);

//   Purpose:   Audio Stream Input Task Function for initialization of data values
//              from parameters for Algorithm Keys.
Int
PAF_ASIT_initPhaseAlgKey(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
);

//   Purpose:   Audio Stream Input Task Function for initialization of Decoder Output Circular Buffer.
Int
PAF_ASIT_initPhaseDecOpCircBuf(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
);

//   Purpose:   Audio Stream Input Task Function for initialization of Input Devices.
Int
PAF_ASIT_initPhaseDevice(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
);

//   Purpose:   Audio Stream Input Task Function for initialization of Output Init-Sync.
Int
PAF_ASIT_initPhaseOutIS(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsdtCfg
);


#if 0
//   Purpose:   Audio Stream Task Function for initialization or reinitiali-
//              zation of the Audio Frame(s) by loading of data values of a
//              time-varying nature.
Int
PAF_AST_initFrame1(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_AST_Config *pAsitCfg, 
    Int z, 
    Int apply
);
#endif

//   Purpose:   Audio Stream Task Function for processing audio data to
//              determine the input type without output.
Int
PAF_ASIT_autoProcessing(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int inputTypeSelect, 
    ALG_Handle pcmAlgMaster);

//   Purpose:   Audio Stream Input Task Function for processing audio data.
//
Int
PAF_ASIT_decodeProcessing(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int sourceSelect
);

//   Purpose:   Decoding Function for processing Decode Commands.
Int
PAF_ASIT_decodeCommand(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
);

//   Purpose:   Decoding Function for reinitializing the decoding process.
Int
PAF_ASIT_decodeInit(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int sourceSelect
);

//   Purpose:   Decoding Function for processing information in a manner that
//              is common for both initial and subsequent frames of input data.
Int
PAF_ASIT_decodeInfo(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int frame, 
    Int block
);

//   Purpose:   Decoding Function for processing information in a manner that
//              is unique to initial frames of input data.
Int
PAF_ASIT_decodeInfo1(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int frame, 
    Int block
);

//   Purpose:   Decoding Function for processing information in a manner that
//              is unique to frames of input data other than the initial one.
Int
PAF_ASIT_decodeInfo2(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int frame, 
    Int block
);

#if 0
//   Purpose:   Decoding Function for processing that occurs subsequent to
//              information processing but antecedent to timing processing
//              for frames of input data other than the initial one.
Int
PAF_AST_decodeCont(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    ALG_Handle decAlg[], 
    Int frame, 
    Int block
);
#endif

//   Purpose:   Decoding Function for processing of input data by the
//              Decode Algorithm.
Int
PAF_ASIT_decodeDecode(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int sourceSelect, 
    Int frame, 
    Int block
);

//   Purpose:   Decoding Function for determining whether processing of the
//              current frame is complete.
Int
PAF_ASIT_decodeFinalTest(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int frame, 
    Int block
);

//   Purpose:   Decoding Function for terminating the decoding process.
Int
PAF_ASIT_decodeComplete(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    ALG_Handle decAlg[], 
    Int frame, 
    Int block
);

//   Purpose:   Audio Stream Input Task Function for selecting the devices used
//              for input.
Int
PAF_ASIT_selectDevices(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
);

//   Purpose:   Audio Stream Input Task Function for selecting the sources used
//              for decoding of input to output.
Int
PAF_ASIT_sourceDecode(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int x
);

//   Purpose:   Common Function for processing algorithm chains.
Int
PAF_AST_streamChainFunction(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int iChainFrameFxns, 
    Int abortOnError, 
    Int logArg
);

//   Purpose:   writing 8-bit integer to shared memeory
static inline
sharedMemWriteInt8(
	volatile XDAS_Int8 *address, /* address to write to */
	Int8 value,                  /* value to write to the adddress */
	Int gateIdx)                 /* GateMP Index, defined in statusOp_common.h */
{
#ifdef NON_CACHE_STATUS
	statusOp_write((void *)address, (void *)&value, sizeof(Int8), gateIdx);
#else
	*address = value;
#endif
}

//   Purpose:   read 8-bit integer from shared memeory
static inline Int8               /* return the value read in the shared memory */
sharedMemReadInt8(
	volatile XDAS_Int8 *address, /* address to read from */
	Int gateIdx)                 /* GateMP Index, defined in statusOp_common.h */
{
#ifdef NON_CACHE_STATUS
	Int8 tempVar;
	statusOp_read((void *)&tempVar, (void *)address, sizeof(Int8), gateIdx);

	return tempVar;
#else
	return(*address);
#endif
}

//   Purpose:   writing 32-bit integer to shared memeory
static inline sharedMemWriteInt(volatile XDAS_Int32 *address, Int value, Int gateIdx)
{
#ifdef NON_CACHE_STATUS
	statusOp_write((void *)address, (void *)&value, sizeof(Int), gateIdx);
#else
	*address = value;
#endif
}

//   Purpose:   read 32-bit integer from shared memeory
static inline Int sharedMemReadInt(volatile XDAS_Int32 *address, Int gateIdx)
{
#ifdef NON_CACHE_STATUS
	Int tempVar;
	statusOp_read((void *)&tempVar, (void *)address, sizeof(Int), gateIdx);

	return tempVar;
#else
	return(*address);
#endif
}

Int asitUpdateInputStatus(const void *pRxParams, PAF_InpBufStatus *pStatus,
                          PAF_InpBufConfig *pInpBuf);
/*
Int asipDecodeInit(
        const PAF_ASIT_Params *pP,
        PAF_ASIT_Config *pAsitCfg,
        Int sourceSelect);
*/
/*Int asipDecodeProcessing(
        const PAF_ASIT_Params *pP,
        const PAF_ASIT_Patchs *pQ,
        PAF_ASIT_Config *pAsitCfg,
        Int sourceSelect);*/
Int asitDecodeProcessing(const PAF_ASIT_Params *pP,
                         const PAF_ASIT_Patchs *pQ,
                         PAF_ASIT_Config       *pAsitCfg,
                         UInt asitEvents);

Int decDecodeInit(
        const PAF_ASIT_Params *pP,
        PAF_ASIT_Config *pAsitCfg,
        Int sourceSelect);

Int decDecodeFsm(
        const PAF_ASIT_Params *pP,
        const PAF_ASIT_Patchs *pQ,
        PAF_ASIT_Config *pAsitCfg,
        Int sourceSelect,
        UInt decMsg);

Int getFrameLengthSourceSel(
        const PAF_ASIT_Params *pP,
        Int8 sourceSelect);

Int rxDecodePcm(PAF_AST_IoInp  *pInp);

Int rxDecodeBitStream(PAF_AST_IoInp  *pInp);

Int rxDecodePlayZero(PAF_AST_IoInp  *pInp);


#endif /* _ASIP_H_ */
