
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== audioStreamOutDec.c ========
 */

#include <string.h> // for memset
#include <xdc/runtime/Log.h>
#include <xdc/std.h>

#include "paferr.h"
#include "pafsio_ialg.h"
#include "pce.h"
#include "stdasp.h"
#include "asperr.h"

#include "aspDecOpCircBuf_master.h"
#include "audioStreamProc_common.h"
#include "audioStreamOutProc.h"
#include "audioStreamOutIo.h"
#include "audioStreamOutDec.h"
#include "ioConfig.h"    //TODO: remove this header
#include "ioPhy.h"

#define ENC_Handle PCE_Handle /* works for all: PCE */

// debug
#include "evmc66x_gpio_dbg.h"
#include "dbgCapAf.h"
PAF_AST_DecOpCircBufStats gCbStats; // circular buffer stats

// debug
// Underflow threshold before circular buffer reset and return error to Top-Level FSM
#define DEC_OP_CB_RDAF_UND_THR  ( 80 ) // arbitrary setting
UInt32 gCbReadAfErr         =0; // read circular buffer error count, not including underflows
UInt32 gDecOpCbRdAfUnd      =0; // decoder output circular buffer underflow count
UInt32 gMaxDecOpCbRdAfUnd   =0; // max (consecutive) decoder output circular buffer underflow count
UInt32 gMasterCbResetCnt    =0; // master circular buffer reset count


//   Purpose:   Common Function for processing algorithm chains
static Int streamChainFunction(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int iChainFrameFxns, 
    Int abortOnError, 
    Int logArg
);

//   Purpose:   ASOT Function for Output reset
Int asopDecOutProcReset(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int frame
)
{
    PAF_AST_Config *pAstCfg;    // ASIT/ASOT/ASDT shared configuration
    PAF_AST_IoOut *pOut;        // ASOT IO configuration
    Int as;                     // Audio Stream Number (1, 2, etc.) */
    Int zO, zS;
    Int z;                      // encode counter
    Int errno;                  // error number
    Int status;                 // status code
    
    status = ASOP_DOP_SOK;
    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    pOut = pAsotCfg->pIoOut; // get pointer to ASOT IO configuration
    as = pAstCfg->as;

    for (z=ENCODE1; z < ENCODEN; z++) 
    {
        zO = pP->outputsFromEncodes[z];
        zS = pP->streamsFromEncodes[z];
        if (pOut[zO].hIoPhy && pAstCfg->xEnc[z].encodeStatus.mode) 
        {
            Int select = pAstCfg->xEnc[z].encodeStatus.select;
            ALG_Handle encAlg = pAstCfg->xEnc[z].encAlg[select];
            ENC_Handle enc = (ENC_Handle )encAlg;

            TRACE_VERBOSE1("AS%d: PAF_ASOT_outputReset: initializing encode", as+zS);

            if (encAlg->fxns->algActivate)
            {
                encAlg->fxns->algActivate(encAlg);
            }
            
            if (enc->fxns->reset)
            {
                errno = enc->fxns->reset(enc, NULL, 
                    &pAstCfg->xEnc[z].encodeControl, 
                    &pAstCfg->xEnc[z].encodeStatus);
                if (errno)
                {
                    status = ASOP_DOP_ERR_RESET_ENCRESET;
                    return status;
                }
            }
        }
    }    
    
    return status;
}

//   Purpose:   Dec Info1 stub function
Int asopDecOutProcInfo1(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int frame 
)
{
    return ASOP_DOP_SOK;
}

//   Purpose:   Reset ASP chain, execute ENC info, and initiate Output
Int asopDecOutProcDec1(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int frame 
)
{
    PAF_AST_Config *pAstCfg;    // ASIT/ASOT/ASDT shared configuration
    PAF_AST_IoOut *pOut;        // ASOT IO configuration
    Int zO, zS; 
    Int z;                      // decode/encode counter
    Int errno;                  // error number
    Int status;                 // status code
    
    status = ASOP_DOP_SOK;
    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    pOut = pAsotCfg->pIoOut; // get pointer to ASOT IO configuration

    //
    // Reset ASP chain
    //
    TRACE_VERBOSE0("asopDecOutProcDec1: calling streamChainFunction.");
    errno = streamChainFunction(pP, pQ, pAsotCfg, PAF_ASP_CHAINFRAMEFXNS_RESET, 1, frame);  // SRC reset is called inside;
    if (errno)
    {
        TRACE_TERSE1("asopDecOutProcDec1: streamChainFunction returns errno 0x%x ", errno);
        status = ASOP_DOP_ERR_DEC1_ASPCHAINRESET;
        return status;
    }

    //
    // Encode Info
    //
    TRACE_VERBOSE0("asopDecOutProcDec1: calling enc->info.");
    for (z=ENCODE1; z < ENCODEN; z++) 
    {
        Int zO = pP->outputsFromEncodes[z];
        if (pOut[zO].hIoPhy && pAstCfg->xEnc[z].encodeStatus.mode) 
        {
            Int select = pAstCfg->xEnc[z].encodeStatus.select;
            ALG_Handle encAlg = pAstCfg->xEnc[z].encAlg[select];
            ENC_Handle enc = (ENC_Handle )encAlg;
            
            if (enc->fxns->info)
            {
                errno = enc->fxns->info(enc, NULL,
                    &pAstCfg->xEnc[z].encodeControl, 
                    &pAstCfg->xEnc[z].encodeStatus);
                if (errno)
                {
                    TRACE_TERSE1("asopDecOutProcDec1: enc info returns errno 0x%x ", errno);
                    status = ASOP_DOP_ERR_DEC1_ENCINFO;
                    return status;
                }
            }
        }
    }

    //
    // Initialize Output components
    //

    // FL, New IO: Something needed in new IO
    // FL, New IO: API for multiple Outputs
    // FL, New IO: SIO calls need to be checked
    errno = asopSetCheckRateX(pP, pQ, pAsotCfg, 0);
    if (errno)
    {
        if (errno != ASOP_IO_ERR_RATE_CHANGE)
        {
            TRACE_TERSE1("asopDecOutProcDec1: asopSetCheckRateX returns errno 0x%x", errno);
            status = ASOP_DOP_ERR_DEC1_SETRATEX;
            return status;
        }
        else
        {
            TRACE_TERSE1("asopDecOutProcDec1: asopSetCheckRateX returns errno 0x%x, ignoring", errno);            
        }
    }
    
    // FL, New IO: API for multiple Outputs
    // FL, New IO: SIO calls need to be checked
    errno = asopStartOutput(pP, pQ, pAsotCfg);
    if (errno)
    {
        TRACE_TERSE1("asopDecOutProcDec1: asopStartOutput returns errno 0x%x ", errno);
        status = ASOP_DOP_ERR_DEC1_STARTOUTPUT;
        return status;
    }     
    
    return status;
} //asopDecOutProcDec1

//   Purpose:   Re-initiate Output
Int asopDecOutProcInfo2(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ,  
    PAF_ASOT_Config *pAsotCfg, 
    Int frame 
)
{
    PAF_AST_Config *pAstCfg;    // ASIT/ASOT/ASDT shared configuration
    PAF_AST_IoOut  *pOut;       // ASIO IO configuration
    Int zO, zS;
    Int z;                      // decode/encode counter
    Int errno;                  // error number
    Int status;                 // status code
    
    status = ASOP_DOP_SOK;
    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    pOut = pAsotCfg->pIoOut; // get pointer to ASOT IO configuration

    // FL, New IO: Something needed in new IO
    // FL, New IO: API for multiple Outputs
    // FL, New IO: SIO calls need to be checked
    errno = asopSetCheckRateX(pP, pQ, pAsotCfg, 0);
    if (errno)
    {
        //
        // Note Rate change is NOT ignored here
        //
        
        TRACE_TERSE1("asopDecOutProcInfo2: info returns errno 0x%x ", errno);
        status = ASOP_DOP_ERR_INFO2_SETRATEX;
        return status;
    }
    
    // Find first Output associated with Master Stream
    zO = OUTPUT1;
    for (z=ENCODE1; z < ENCODEN; z++)
    {
        zS = pP->streamsFromEncodes[z]; // get Stream associated with Encoder
        if (zS == pAstCfg->masterStr)
        {
            // This Encoder is associated with Master Stream.
            // Note other Encoder can also be associated with Master Stream.
            zO = pP->outputsFromEncodes[z]; // get Output associated with Encoder
        }
    }
    
    // Start output transfer
    // FL, New IO: API for single Output
    //asopPhyTransferStart(&pOut[zO]);
    
    return status;
}

// Initialize Decoder output processing
Int asopDecOutProcInit(
    const PAF_ASOT_Params *pP,
    const PAF_ASOT_Patchs *pQ,
    PAF_ASOT_Config *pAsotCfg, 
    Int frame 
)
{
    PAF_AST_DecOpCircBufCtl *pCbCtl;    // Decoder output circular buffer control
    Int z;                              // decode counter
    Int errno;                          // error number
    Int status;                         // status code

    status = ASOP_DOP_SOK;
    pCbCtl = &pAsotCfg->pAspmCfg->decOpCircBufCtl; // get pointer to circular buffer control

    for (z=DECODE1; z < DECODEN; z++)
    {       
        // Start decoder output circular buffer reads
        errno = cbReadStart(pCbCtl, z);
        if (errno)
        {
            TRACE_TERSE1("asopDecOutProcInit:cbReadStart() error=%d", errno);
            status = ASOP_DOP_ERR_INIT_CBREADSTART;
            return status;
        }
        
        gCbReadAfErr=0;         // reset read circular buffer error count
        gDecOpCbRdAfUnd=0;      // reset decoder output circular buffer underflow count
        gMaxDecOpCbRdAfUnd=0;   // reset max decoder output circular buffer underflow count
        gMasterCbResetCnt=0;    // reset master circular buffer reset count

        // debug, log circular buffer control variables
        cbLog(pCbCtl, z, 1, "asopDecOutProcInit:cbReadStart");
    }
        
    return status;
} /* asopDecOutProcInit */

// Process Decoder output audio data using ASP chain
Int asopDecOutProcStream(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int frame 
)
{
    PAF_AST_Config *pAstCfg;
    PAF_AST_DecOpCircBufCtl *pCbCtl;    // Decoder output circular buffer control
    Int z;                              // decode/stream counter
    PAF_AudioFrame *pAfRd;
    //PAF_AST_DecOpCircBufStats cbStats;  // circular buffer statistics
    Int errno;                          // error number
    Int status;                         // status code

    status = ASOP_DOP_SOK;
    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration    
    pCbCtl = &pAsotCfg->pAspmCfg->decOpCircBufCtl; // get pointer to circular buffer control
    
    for (z=DECODE1; z < DECODEN; z++) 
    {
        Int zS = pP->streamsFromDecodes[z];
        
        //
        // Read decoder output circular buffer
        //
        pAfRd = pAstCfg->xStr[zS].pAudioFrame;
        //GPIOSetOutput(GPIO_PORT_0, GPIO_PIN_106);       // debug
        errno = cbReadAf(pCbCtl, z, pAfRd);
        //GPIOClearOutput(GPIO_PORT_0, GPIO_PIN_106);     // debug
        if ((errno < 0) && 
            (errno != ASP_DECOP_CB_AF_READ_UNDERFLOW) && 
            (errno != ASP_DECOP_CB_PCM_READ_UNDERFLOW))
        {
            gCbReadAfErr++;
            TRACE_TERSE1("asopDecOutProcStream:cbReadAf() error=%d", errno);
            //SW_BREAKPOINT; // debug
            status = ASOP_DOP_ERR_STREAM_CBREAD;
            return status;
        }

        // Handle underflows
        if ((errno == ASP_DECOP_CB_AF_READ_UNDERFLOW) ||
            (errno == ASP_DECOP_CB_PCM_READ_UNDERFLOW))
        {
            // FL: Need to check behavior of cbReset() on exit/re-entry into Output processing.
            gDecOpCbRdAfUnd++; // increment circular buffer underflow count
            if (gDecOpCbRdAfUnd >= DEC_OP_CB_RDAF_UND_THR) 
            {
                // Underflow count above threshold.
                // (1) set max underflow count to threshold
                // (2) reset underflow count
                // (3) reset circular buffer
                
                gMaxDecOpCbRdAfUnd = DEC_OP_CB_RDAF_UND_THR; // update max underflow count
                gDecOpCbRdAfUnd = 0; // reset underflow count

                // Reset circular buffer
                cbReset(pCbCtl, z);
                gMasterCbResetCnt++; // increment master circular buffer reset count
                Log_info0("asopDecOutProcStream:cbReset()");
            
                status = ASOP_DOP_ERR_STREAM_CBREADUNDTHR;
                return status;
            }
        }
        else if ((errno == ASP_DECOP_CB_SOK) && (gDecOpCbRdAfUnd > 0))
        {
            // No underflow detected.
            // update max underflow count,
            // reset underflow count
            
            // update max underflow count
            if (gDecOpCbRdAfUnd > gMaxDecOpCbRdAfUnd)
            {
                gMaxDecOpCbRdAfUnd = gDecOpCbRdAfUnd;
            }
            gDecOpCbRdAfUnd = 0; // reset circular buffer underflow count
        }
        //Log_info0("asopDecOutProcStream:cbReadAf() complete.");
        //GPIOClearOutput(GPIO_PORT_0, GPIO_PIN_106);   // debug
        Log_info0("asopDecOutProcStream:cbReadAf() complete.");
        
#if 0 // debug
            // Shows timing of CB read
            // ADC B8
            {
                static Uint8 toggleState = 0;
                if (toggleState == 0)
                    GPIOSetOutput(GPIO_PORT_0, GPIO_PIN_106);
                else
                    GPIOClearOutput(GPIO_PORT_0, GPIO_PIN_106);
                toggleState = ~(toggleState);
            }
#endif

        // debug, get circular buffer statistics
        //cbGetStats(pCbCtl, z, &cbStats);
        cbGetStats(pCbCtl, z, &gCbStats);

        // debug
        cbLog(pCbCtl, z, 1, "asopDecOutProcStream:cbReadAf()");
        
#if 0 // debug, capture audio frame
        if (capAfWrite(pAfRd, PAF_LEFT) != CAP_AF_SOK)
//      if (capAfWrite(pAfRd, PAF_RIGHT) != CAP_AF_SOK)
        {
            Log_info0("asopDecOutProcStream:capAfWrite() error");
        }
#endif
    }
            
    TRACE_VERBOSE0("asopDecOutProcStream: calling streamChainFunction()");
    errno = streamChainFunction(pP, pQ, pAsotCfg, PAF_ASP_CHAINFRAMEFXNS_APPLY, 1, frame);
    if (errno)
    {
        TRACE_TERSE1("asopDecOutProcStream: streamChainFunction() returns errno 0x%x ", errno);
        status = ASOP_DOP_ERR_STREAM_ASPCHAINAPPLY;
        return status;
    }

#if 0 // debug, capture audio frame
    if (capAfWrite(pAfRd, PAF_LEFT) != CAP_AF_SOK)
    {
        Log_info0("asopDecOutProcStream:capAfWrite() error");
    }
#endif
    
    return status;
} //asopDecodeStream

// -----------------------------------------------------------------------------
// ASOT Decoding Function - Encode Processing
//
//   Name:      PAF_ASOT_decodeEncode
//   Purpose:   Decoding Function for processing of audio frame data by the
//              Encode Algorithm.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
Int asopDecOutProcEncode(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int frame 
)
{
    PAF_AST_Config *pAstCfg;    // ASIT/ASOT/ASDT shared configuration
    PAF_AST_IoOut  *pOut;       // ASIO IO configuration
    Int as;                     // Audio Stream Number (1, 2, etc.)
    Int zX, zE, zS;
    Int z;                      // encode/output counter
    Int errno;                  // error number
    Int status;                 // status code
    //ioPhyCtl_t ioPhyCtl;

    status = ASOP_DOP_SOK;
    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    pOut = pAsotCfg->pIoOut; // get pointer to ASOT IO configuration
    as = pAstCfg->as;

    // Await output buffers (but not first time)
    for (z=OUTPUT1; z < OUTPUTN; z++) 
    {
        // determine encoder associated with this output
        zE = z;
        for (zX = ENCODE1; zX < ENCODEN; zX++) 
        {
            if (pP->outputsFromEncodes[zX] == z) 
            {
                zE = zX;
                break;
            }
        }
        zS = pP->streamsFromEncodes[zE];

        if (pOut[z].hIoPhy)
        {
            //uint32_t phyXferSizeOld;

            // update length (e.g. ARC may have changed) - moved out of this function
/*            pAstCfg->xOut[z].outBufConfig.lengthofFrame =
                pAstCfg->xEnc[zE].encodeInStruct.pAudioFrame->sampleCount;
*/
            TRACE_GEN2("asopDecodeEncode: AS%d: processing frame %d -- idle", as+zS, frame);

#if 0 // debug 
            // Shows timing of Output Tx SIO reclaim
            // ADC B8
            {
                static Uint8 toggleState = 0;
                if (toggleState == 0)
                    GPIOSetOutput(GPIO_PORT_0, GPIO_PIN_106);
                else
                    GPIOClearOutput(GPIO_PORT_0, GPIO_PIN_106);
                toggleState = ~(toggleState);
            }
#endif            
        }
        else 
        {
            TRACE_VERBOSE2("AS%d: asopDecodeEncode: processing frame %d -- idle <ignored>", as+zS, frame);
        }
    }

    // Encode data
    for (z=ENCODE1; z < ENCODEN; z++) 
    {
#if 0 // debug, capture audio frame
        PAF_AudioFrame *pAfRd;
        pAfRd = pAstCfg->xEnc[z].encodeInStruct.pAudioFrame;
        if (capAfWrite(pAfRd, PAF_LEFT) != CAP_AF_SOK)
        {
            Log_info0("asopDecOutProcEncode:capAfWrite() error");
        }
#endif
        
        Int zS = pP->streamsFromEncodes[z];
        (void)zS; // clear compiler warning in case not used with tracing disabled
        if (pAstCfg->xEnc[z].encodeStatus.mode) 
        {
            Int select = pAstCfg->xEnc[z].encodeStatus.select;
            ALG_Handle encAlg = pAstCfg->xEnc[z].encAlg[select];
            ENC_Handle enc = (ENC_Handle)encAlg;
            if (select != pAstCfg->xEnc[z].encodeControl.encActive)
            {
                pAstCfg->xEnc[z].encodeControl.encActive = select;
                TRACE_TERSE0("asopDecodeEncode: AS%d: changing selected encoder, return error");
                return ASOP_DOP_ERR_ENCODE_ENCSELECT;
            }
            TRACE_GEN2("asopDecodeEncode: AS%d: processing frame %d -- encode", as+zS, frame);

            // (MID 1933) temp. workaround for PCE2
            pAstCfg->xEnc[z].encodeInStruct.pAudioFrame->data.nChannels = PAF_MAXNUMCHAN;
            //pAstCfg->xEnc[z].encodeInStruct.pAudioFrame->data.nChannels = 2;   //JXTODO: make this correct

            if (enc->fxns->encode)
            {
                pAstCfg->xEnc[z].encodeOutStruct.bypassFlag =
                        pP->z_pEncodeStatus[z]->encBypass;
                errno = enc->fxns->encode(enc, NULL, 
                    &pAstCfg->xEnc[z].encodeInStruct, 
                    &pAstCfg->xEnc[z].encodeOutStruct);
                if (errno)
                {
                    if (errno != PCEERR_OUTPUT_POINTERNULL)    // errno = PCEERR_OUTPUT_RESULTRANGE
                    {
                        TRACE_TERSE1("asopDecodeEncode: encode returns error %d", errno);
                        status = ASOP_DOP_ERR_ENCODE_ENC;
                        return status;
                    }
                }
            }
        }
        else 
        {
            TRACE_VERBOSE2("asopDecodeEncode: AS%d: processing frame %d -- encode <ignored>",
                as+pP->streamsFromEncodes[z], frame);
        }
    }

    // add debug code to dump output samples to memory

    // Transmit data
    for (z=OUTPUT1; z < OUTPUTN; z++) 
    {
        // determine stream associated with this output
        zE = z;
        for (zX = ENCODE1; zX < ENCODEN; zX++) 
        {
            if (pP->outputsFromEncodes[zX] == z) 
            {
                zE = zX;
                break;
            }
        }
        zS = pP->streamsFromEncodes[zE];
    }

    return status;
} /* asopDecodeEncode */

//   Purpose: Check if output processing of current stream is complete
Int asopDecOutProcFinalTest(
    const struct PAF_ASOT_Params *pP, 
    const struct PAF_ASOT_Patchs *pQ, 
    struct PAF_ASOT_Config *pAsotCfg, 
    Int frame
)
{
    PAF_AST_DecOpCircBufCtl *pCbCtl;    // decoder output circular buffer control
    Int8 drainedFlag;                   // CB drained indicator flag
    Int zMD;                            // master Dec index
    Int errno;                          // error number
    Int status;                         // status code
    
    status = ASOP_DOP_SOK;
    pCbCtl = &pAsotCfg->pAspmCfg->decOpCircBufCtl; // get pointer to circular buffer control
    zMD = pAsotCfg->pAstCfg->masterDec; // get master Dec index

    // Check circular buffer drain state
    errno = cbCheckDrainState(pCbCtl, zMD, &drainedFlag);
    if (errno)
    {
        status = ASOP_DOP_ERR_FINALTEST_CBCHKDRAIN;
        return status;
    }
    else if (drainedFlag == 1) // errno == 0
    {
        status = ASOP_DOP_ERR_FINALTEST_CBDRAINED;
        return status;
    }
    
    return status;
}    

// -----------------------------------------------------------------------------
// ASOT Decoding Function - Stream-Final Processing
//
//   Name:      PAF_ASOT_decodeComplete
//   Purpose:   Decoding Function for terminating the decoding process.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
Int asopDecOutProcComplete(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int frame
)
{
    PAF_AST_Config *pAstCfg;            // ASIT/ASOT/ASDT shared configuration
    PAF_AST_IoOut  *pOut;               // ASIO IO configuration
    PAF_AST_DecOpCircBufCtl *pCbCtl;    // Decoder output circular buffer control
    Int as;                             // Audio Stream Number (1, 2, etc.)
    Int z;                              // decode/encode counter
    Int errno;                          // error number
    Int status;                         // status code
    
    status = ASOP_DOP_SOK;
    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    (void)as;  // clear compiler warning in case not used with tracing disabled

    pCbCtl = &pAsotCfg->pAspmCfg->decOpCircBufCtl; // get pointer to circular buffer control
    
    for (z=DECODE1; z < DECODEN; z++)
    {
        // Stop decoder output circular buffer reads
        errno = cbReadStop(pCbCtl, z);
        if (errno)
        {
            TRACE_TERSE1("asopDecOutProcComplete:cbReadStop() error=%d", errno);
            //SW_BREAKPOINT; // debug
            status = ASOP_DOP_ERR_COMPLETE_CBREADSTOP;
            return status;
        }
        // debug
        cbLog(pCbCtl, z, 1, "asopDecOutProcComplete:cbReadStop");
    }
    
    streamChainFunction(pP, pQ, pAsotCfg, PAF_ASP_CHAINFRAMEFXNS_FINAL, 0, frame);

    for (z=ENCODE1; z < ENCODEN; z++) 
    {
        Int zO = pP->outputsFromEncodes[z];
        if (pOut[zO].hIoPhy && pAstCfg->xEnc[z].encodeStatus.mode) 
        {
            Int select = pAstCfg->xEnc[z].encodeStatus.select;
            ALG_Handle encAlg = pAstCfg->xEnc[z].encAlg[select];
#ifdef PAF_ASP_FINAL
            ENC_Handle enc = (ENC_Handle)encAlg;
#endif /* PAF_ASP_FINAL */
            TRACE_VERBOSE1("asopDecOutProcComplete: AS%d: finalizing encode", as+z);
#ifdef PAF_ASP_FINAL
            if (enc->fxns->final)
            {
                enc->fxns->final(enc, NULL, &pAstCfg->xEnc[z].encodeControl,
                    &pAstCfg->xEnc[z].encodeStatus);
            }
#endif /* PAF_ASP_FINAL */
            if (encAlg->fxns->algDeactivate)
            {
                encAlg->fxns->algDeactivate(encAlg);
            }
        }
        else 
        {
            TRACE_VERBOSE1("asopDecOutProcComplete: AS%d: finalizing encode <ignored>", as+z);
        }
    }

    // wait for remaining data to be output
    errno = asopStopOutput(pP, pQ, pAsotCfg);
    if (errno)
    {
        status = ASOP_DOP_ERR_COMPLETE_STOPOUTPUT;
        return status;
    }

    return status;
} //asopDecOutProcComplete

// -----------------------------------------------------------------------------
// ASOT Decoding Function - Encode Command Processing
//
//   Name:      PAF_ASOT_encodeCommand
//   Purpose:   Decoding Function for processing Encode Commands.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * Command execution.
//              * SIO control errors.
//              * Error number macros.
//
Int asopDecOutProcEncodeCommand(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                     // Audio Stream Number (1, 2, etc.)
    Int zO, zS;
    Int z;                      // encode counter
    Int errno = 0;              // error number
    Int status;                 // status code

    status = ASOP_DOP_SOK;
    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;

    for (z=ENCODE1; z < ENCODEN; z++) 
    {
        zO = pP->outputsFromEncodes[z];
        zS = pP->streamsFromEncodes[z];
        if (!(pAstCfg->xEnc[z].encodeStatus.command2 & 0x80)) 
        {
            switch (pAstCfg->xEnc[z].encodeStatus.command2) 
            {
                case 0: // command none - process
                    pAstCfg->xEnc[z].encodeStatus.command2 |= 0x80;
                    break;
                case 1: // mute command
                    TRACE_VERBOSE2("asopDecOutProcEncodeCommand: AS%d: encode command mute (0x%02x)", as+zS, 1);
                    pAstCfg->xEnc[z].encodeStatus.command2 |= 0x80;
                    break;
                case 2: // unmute command
                    TRACE_VERBOSE2("asopDecOutProcEncodeCommand: AS%d: encode command unmute (0x%02x)", as+zS, 2);
                    pAstCfg->xEnc[z].encodeStatus.command2 |= 0x80;
                    break;
                default: // command unknown - ignore
                    break;
            }
        }
    }

    //ERRNO_RPRT (TaskAsop, errno);

    return status;
} //asopDecOutProcEncodeCommand

// -----------------------------------------------------------------------------
// ASOT Decoding Function Helper - Chain Processing
//
//   Name:      PAF_ASOT_streamChainFunction
//   Purpose:   Common Function for processing algorithm chains.
//   From:      AST Parameter Function -> decodeInfo1
//              AST Parameter Function -> decodeStream
//              AST Parameter Function -> decodeComplete
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
static Int streamChainFunction(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int iChainFrameFxns, 
    Int abortOnError, 
    Int logArg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                     // Audio Stream Number (1, 2, etc.)
    Int z;                      // stream counter
    Int dFlag, eFlag, gear;
    Int zX;
    Int zS;
    Int errno;                  // error number

    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    (void)as; // clear compiler warning in case not used with tracing disabled

    for (zS = STREAM1; zS < STREAMN; zS++)
    {
        z = pP->streamOrder[zS];  // Select stream order from streamOrder parameter - MID 788

        // apply stream
        //      unless the stream is associated with a decoder and it is not running
        // or
        //      unless the stream is associated with an encoder and it is not running
        // Also gear control only works for streams with an associated decoder
        // if no such association exists then gear 0 (All) is used
        dFlag = 1;
        gear = 0;
        for (zX = DECODE1; zX < DECODEN; zX++) 
        {
            if (pP->streamsFromDecodes[zX] == z) 
            {
                dFlag = pAstCfg->xDec[zX].decodeStatus.mode;
                gear = pAstCfg->xDec[zX].decodeStatus.aspGearStatus;
                break;
            }
        }
        eFlag = 1;
        for (zX = ENCODE1; zX < ENCODEN; zX++) 
        {
            if (pP->streamsFromEncodes[zX] == z) 
            {
                eFlag = pAstCfg->xEnc[zX].encodeStatus.mode;
                break;
            }
        }

        if (dFlag && eFlag) 
        {
            PAF_ASP_Chain *chain = pAstCfg->xStr[z].aspChain[gear];
            PAF_AudioFrame *frame = pAstCfg->xStr[z].pAudioFrame;
            Int (*func) (PAF_ASP_Chain *, PAF_AudioFrame *) =
                chain->fxns->chainFrameFunction[iChainFrameFxns];

            TRACE_GEN2(iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_RESET
                       ? "asopStreamChainFunction: AS%d: processing frame %d -- audio stream (reset)"
                       : iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_APPLY
                       ? "asopStreamChainFunction: AS%d: processing frame %d -- audio stream (apply)"
                       : iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_FINAL
                       ? "asopStreamChainFunction: AS%d: processing frame %d -- audio stream (final)"
                       : "asopStreamChainFunction: AS%d: processing frame %d -- audio stream (?????)",
                       as+z, logArg);
            errno = (*func) (chain, frame);   // ASP chain reset function: SRC is the 1st in the chain and reset
            TRACE_VERBOSE2("asopStreamChainFunction: AS%d: errno 0x%x.", as+z, errno);

            if (errno && abortOnError)
            {
                return errno;
            }
        }
        else 
        {
            TRACE_GEN2(iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_RESET
                       ? "asopStreamChainFunction: AS%d: processing frame %d -- audio stream (reset) <ignored>"
                       : iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_APPLY
                       ? "asopStreamChainFunction: AS%d: processing frame %d -- audio stream (apply) <ignored>"
                       : iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_FINAL
                       ? "asopStreamChainFunction: AS%d: processing frame %d -- audio stream (final) <ignored>"
                       : "asopStreamChainFunction: AS%d: processing frame %d -- audio stream (?????) <ignored>",
                       as+z, logArg);
        }
    }

    return 0;
} //asopStreamChainFunction


/* nothing past this point */
