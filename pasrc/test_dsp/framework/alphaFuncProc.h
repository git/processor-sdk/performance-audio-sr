
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== alphaFuncProc.h ========
 */

#ifndef _AFP_H_
#define _AFP_H_
 
#include <xdc/std.h>

#include <acp.h>
//#include <acp_mds.h>
//#include <acp_mds_priv.h>
#include "dcs7.h"
#include "alphaFuncProcParams.h"

// Global debug counters */
extern Uint32 gTaskAfpCnt; // debug counter for AFP task

/* AFP Handle */
typedef struct AFP_Obj *AFP_Handle;

/* AFP Function Table */
typedef struct AFP_Fxns {
    //void   (*log)(AFP_Handle, void *, char *); // FL: leave dedicated log out for now
    void * (*memAlloc)(AFP_Handle, Int32 *, UInt32, UInt32);
    void   (*process)(AFP_Handle);    
    void   (*acpInit)(void);
    void   (*acpExit)(void);
} AFP_Fxns;

/* AFP Object */
typedef struct AFP_Obj {
    Uint32 size;
    const AFP_Fxns *fxns;
    const AFP_Params *params;
    DCS7_Handle dcs7;
    const DCS7_Fxns *dcs7Fxns;
    const DCS7_Config *dcs7Config;
    ACP_Handle acp;
    ACP_Params *acpParams;
    Uint16 *rxBuf;
    Uint16 *txBuf;
    Int *pBufHeapId;
    Int *pObjHeapId;
    //void *logObj;
} AFP_Obj;

#endif /* _AFP_H_ */
