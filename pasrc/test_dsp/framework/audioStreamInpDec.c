/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamInpDec.c ========
 */
#include <ti/sysbios/hal/Cache.h>
#include <xdc/runtime/Log.h>

#include "procsdk_audio_typ.h"
#include "audioStreamInpProc.h"
#include "audioStreamProc_common.h"
#include "aspMsg_common.h"
#include "aspMsg_master.h"
#include "asperr.h"
#include "common.h"
#include "as1-f2.h"

#include "ioConfig.h"    //TODO: remove this header
#include "ioBuff.h"
#include "ioPhy.h"
#include "ioData.h"


extern void asitPostInfoEvent();    // TODO: remove
extern void asitPostDecEvent();     // TODO: remove

enum {
    DEC_STATE_INIT_ACK,
    DEC_STATE_INFO_SND,
    DEC_STATE_INFO_ACK_DECODE_SND,
    DEC_STATE_DECODE_ACK,
    DEC_STATE_INFO,
    DEC_STATE_DECODE,
    DEC_STATE_QUIT
};

enum {
    DEC_MSG_INFO,
    DEC_MSG_DECODE
};

static Int decodeInit(const PAF_ASIT_Params *pP, PAF_ASIT_Config *pAsitCfg,
                      Int sourceSelect);
static Int decodeInitSnd(
        const PAF_ASIT_Params *pP,
        PAF_ASIT_Config *pAsitCfg,
        Int sourceSelect);
static Int decodeInitAck(
        const PAF_ASIT_Params *pP,
        PAF_ASIT_Config *pAsitCfg,
        Int sourceSelect);

static Int decDecodeComplete(const PAF_ASIT_Params *pP,
                             PAF_ASIT_Config *pAsitCfg);

static Int decErrorCheck(const PAF_ASIT_Params *pP, const PAF_ASIT_Patchs *pQ,
                         PAF_ASIT_Config *pAsitCfg, Int sourceSelect);
static Int decInfoSnd(const PAF_ASIT_Params *pP, const PAF_ASIT_Patchs *pQ,
                         PAF_ASIT_Config *pAsitCfg);
static Int decInfoAck(const PAF_ASIT_Params *pP, const PAF_ASIT_Patchs *pQ,
                         PAF_ASIT_Config *pAsitCfg);
static Int decDecodeAck(const PAF_ASIT_Params *pP, const PAF_ASIT_Patchs *pQ,
                           PAF_ASIT_Config *pAsitCfg);
static Int decDecodeSnd(const PAF_ASIT_Params *pP, const PAF_ASIT_Patchs *pQ,
                           PAF_ASIT_Config *pAsitCfg);
static Int decDecodeFinalTest(const PAF_ASIT_Params *pP, const PAF_ASIT_Patchs *pQ,
                              PAF_ASIT_Config *pAsitCfg);
static void decGetStreamInfo(PAF_AST_IoInp *pInp, asipDecProc_t *pDec);
static void decUpdateInpBufConfig(PAF_AST_Config *pAstCfg, asipDecProc_t *pDec);
static Int decIbConfigQueNotEmpty(asipDecProc_t *pDec);

Int decCheckMajorAu(PAF_AST_Config *pAstCfg);

// Processes unacknowledged Ack messages in decDecodeComplete()
static Int decDecodeCompleteProcAck(
    const PAF_ASIT_Params *pP, 
    PAF_ASIT_Config *pAsitCfg,
    Int z,
    UInt32 ackCmd, 
    char *decMsgBuf
);

// Processes unacknowledged Info Ack messages in decDecodeComplete()
static Int decDecodeCompleteProcDecInfoAck(
    const PAF_ASIT_Params *pP, 
    PAF_ASIT_Config *pAsitCfg,
    Int z,
    char *decMsgBuf
);

// Processes unacknowledged Decode Ack messages in decDecodeComplete()
static Int decDecodeCompleteProcDecDecodeAck(
    const PAF_ASIT_Params *pP, 
    PAF_ASIT_Config *pAsitCfg,
    Int z,
    char *decMsgBuf
);

// Processes unacknowledged Reset Ack messages in decDecodeComplete()
static Int decDecodeCompleteProcDecResetAck(
    const PAF_ASIT_Params *pP, 
    PAF_ASIT_Config *pAsitCfg,
    Int z,
    char *decMsgBuf
);

extern UInt32 gCbWrtAfErrCnt;


Int decDecodeInit(
        const PAF_ASIT_Params *pP,
        PAF_ASIT_Config *pAsitCfg,
        Int sourceSelect)
{
    Int errno;

//    errno = decodeInit(pP, pAsitCfg, sourceSelect);
    errno = decodeInitSnd(pP, pAsitCfg, sourceSelect);

    if(errno) {
        decDecodeComplete(pP, pAsitCfg);

        return ASIP_ERR_DECODE_INIT;
    }

    pAsitCfg->inpDec.majorAuFound = FALSE;
    
    return ASIP_NO_ERR;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
Int decDecodeFsm(
        const PAF_ASIT_Params *pP,
        const PAF_ASIT_Patchs *pQ,
        PAF_ASIT_Config *pAsitCfg,
        Int sourceSelect,
        UInt decMsg)
{
    asipDecProc_t  *pDec;
    Int decErr, decDone;

    pDec = &pAsitCfg->inpDec;

    TRACE_VERBOSE1("Entering decDecodeFsm with decMsg %d.", decMsg);

    decErr = decErrorCheck(pP, pQ, pAsitCfg, sourceSelect);
    if(decErr == DEC_ERR_ASPERR_ABORT) {
        return decErr;
    }
    else if(decErr != DEC_NO_ERR) {
        pDec->state = DEC_STATE_QUIT;
    }
    else {
        ; // No error
    }

    //if ( (decMsg & DEC_MSGMSK_INPDATA) && (pDec->state != DEC_STATE_INIT_ACK)) {
    if ( (decMsg & DEC_MSGMSK_INPDATA) ) {
        // put IB read information to the queue
        decGetStreamInfo(&pAsitCfg->pIoInp[pAsitCfg->pAstCfg->masterDec], pDec);
        TRACE_VERBOSE0("decDecodeFsm: INPUT DATA event. Put IB info into queue.");
    }

    // It is possible to go through the FSM multiple times:
    //    - Multiple messages may be received, INPDATA and DECACK. In this case,
    //      the FSM will process DECACK in state DEC_STATE_DECODE_ACK and then
    //      INPDATA in state DEC_STATE_INFO_SND.
    //    - DECACK is received after INPDATA. In this case, after DECACK is
    //      processed, we will spoof decMsg with INPDATA so that FSM will run
    //      again and process INPDATA in DEC_STATE_INFO_SND.
    do {
        switch(pDec->state)
        {
        case DEC_STATE_INIT_ACK:
            // Ignore any input data event in this state
            if(decMsg & DEC_MSGMSK_INPDATA) {
                decMsg &= ~DEC_MSGMSK_INPDATA;   // clear bit mask
            }

            if (decMsg & DEC_MSGMSK_RXACK) {
                int errno;
                errno = decodeInitAck(pP, pAsitCfg, sourceSelect);

                if(errno) {
                    decDecodeComplete(pP, pAsitCfg);

                    return ASIP_ERR_DECODE_INIT;
                }

                pDec->state = DEC_STATE_INFO_SND;
                TRACE_VERBOSE0("decDecodeFsm: DEC_STATE_INIT_ACK done.");

                decMsg &= ~DEC_MSGMSK_RXACK;  // clear the bit mask
            }
            break;

        case DEC_STATE_INFO_SND:
            if(decMsg != DEC_MSGMSK_INPDATA) {
                // Only DEC_MSGMSK_INPDATA is expected in this state
                decErr = DEC_ERR_WRONG_MSG;
            }
            else {
                decMsg &= ~DEC_MSGMSK_INPDATA;   // clear bit mask

                // Prepare and send INFO to decoder
                decUpdateInpBufConfig(pAsitCfg->pAstCfg, pDec);

                decErr = decInfoSnd(pP, pQ, pAsitCfg);
                if(decErr == DEC_NO_ERR) {
                    pDec->state = DEC_STATE_INFO_ACK_DECODE_SND;
                    TRACE_VERBOSE0("decDecodeFsm: DEC_STATE_INFO_SND done.");
                }
            }
        break;

        case DEC_STATE_INFO_ACK_DECODE_SND:
            //if(decMsg != DEC_MSGMSK_INFOACK) {
                // only DEC_MSGMSK_INFOACK is expected in this state
            //if((decMsg&DEC_MSGMSK_INFOACK) != DEC_MSGMSK_INFOACK) {
            //    // During debugging, it is possible that INPDATA and INFOACK
            //    // come at the same time (e.g. due to breaking point).
            //    decErr = DEC_ERR_WRONG_MSG;
            //}
            //else {

            if (decMsg & DEC_MSGMSK_RXACK) {
                decMsg &= ~DEC_MSGMSK_RXACK;  // clear the bit mask

                // Process the INFO acknowledgment from decoder
                decErr = decInfoAck(pP, pQ, pAsitCfg);
                if(decErr == DEC_NO_ERR) {
                    // Don't start decode until major access unit is found.
                    if(!pDec->majorAuFound) {
                        // Do we want to still check major AU after it is found?
                        // In old code, it was not checked again after it was found.
                        pDec->majorAuFound = decCheckMajorAu(pAsitCfg->pAstCfg);
                    }

                    if(pDec->majorAuFound) {
                        // Major access unit is found. Send message to decoder to decode now.
                        decErr = decDecodeSnd(pP, pQ, pAsitCfg);
                        if(decErr == DEC_NO_ERR) {
                            pDec->state = DEC_STATE_DECODE_ACK;

                            TRACE_VERBOSE0("decDecodeFsm: DEC_STATE_INFO_ACK_DECODE_SND done and major AU found. Going to DEC_STATE_DECODE_ACK.");
                        }
                    }
                    else {
                        // No major access unit - go back to INFO.
                        pDec->frame++;
                        pDec->state = DEC_STATE_INFO_SND;
                        TRACE_VERBOSE0("decDecodeFsm: DEC_STATE_INFO_ACK_DECODE_SND done and no major AU. Going to DEC_STATE_INFO_SND.");
                    }
                }
            }
            else if (decMsg & DEC_MSGMSK_INPDATA) {
                // Ignore INPDATA message since IB info is already queued
                decMsg &= ~DEC_MSGMSK_INPDATA;
            }
            else {
                decErr = DEC_ERR_WRONG_MSG;
            }
        break;

        case DEC_STATE_DECODE_ACK:
            // Two different messages may be received: DECACK (decode acknowledgment)
            // or INPDATA (input data ready).
            if (decMsg & DEC_MSGMSK_RXACK)
            {
                decMsg &= ~DEC_MSGMSK_RXACK;  // clear the bit mask

                // Process DECODE ACK from decoder
                decErr = decDecodeAck(pP, pQ, pAsitCfg);
                if(decErr == DEC_NO_ERR) {
                    // Decode finishes. Conduct final test.
                    decErr = decDecodeFinalTest(pP, pQ, pAsitCfg);
                    if(decErr == DEC_NO_ERR) {
                        pDec->frame++;
                        pDec->state = DEC_STATE_INFO_SND;
                        TRACE_VERBOSE0("decDecodeFsm: DEC_STATE_DECODE_ACK done and going to DEC_STATE_INFO_SND.");

                        // Check if IB config queue is empty or not
                        if(decIbConfigQueNotEmpty(pDec)) {
                            // Need to prepare and send INFO to decoder immediately.
                            // Because INPUT_DATA message is already received,
                            // we're just spoofing the message with INPDATA to
                            // run the FSM one more time.
                            decMsg |= DEC_MSGMSK_INPDATA;

                            TRACE_VERBOSE0("decDecodeFsm: DECODE_ACK was late and INFO_SND was delayed.");
                        }
                    }
                }
            }
            else if (decMsg & DEC_MSGMSK_INPDATA) {
                // Ignore INPDATA message since IB info is already queued
                decMsg &= ~DEC_MSGMSK_INPDATA;
            }
            else {
                decErr = DEC_ERR_WRONG_MSG;
            }
        break;

        case DEC_STATE_QUIT:
            //gAsipQuitCnt++;
            TRACE_VERBOSE0("decDecodeFsm: state: DEC_STATE_QUIT");
        break;

        default:
        break;
        } /* switch */

        // Loop through the FSM one more time if:
        //    - there are more real or spoofed messages to process,
        //    - and there is no error.
        if((decMsg==0) || (decErr!=DEC_NO_ERR)) {
            decDone = TRUE;
        }
        else {
            decDone = FALSE;
        }
    } while(!decDone);

    // Error handling - complete decoding
    if(decErr != DEC_NO_ERR) {
        TRACE_VERBOSE1("decDecodeFsm: decErr %d, calling decDecodeComplete.", decErr);
        decDecodeComplete(pP, pAsitCfg);
        TRACE_VERBOSE0("decDecodeFsm: decDecodeComplete done.");
    }

    return decErr;
} /* decDecodeFsm */

void decGetStreamInfo(PAF_AST_IoInp *pInp, asipDecProc_t *pDec)
{
    PAF_InpBufConfig *pBufConfig;
    ioDataCtl_t ioDataCtl;

    /* Get information for reading input data */
    ioDataCtl.code = IODATA_CTL_GET_INPBUFFINFO;
    ioDataControl(pInp->hIoData, &ioDataCtl);

    if(ioDataCtl.param.dataReadInfo.frameSize != pInp->phyXferSize) {
        // Fatal error!
        TRACE_VERBOSE0("TaskAsip: error in updating I/O");
        SW_BREAKPOINT;
    }

    pBufConfig = &(pDec->inpBufConfigQueue[pDec->wridx]);

    pBufConfig->base.pVoid   = ioDataCtl.param.dataReadInfo.buffBase;
    pBufConfig->sizeofBuffer = ioDataCtl.param.dataReadInfo.buffSize;
    pBufConfig->pntr.pSmInt  = ioDataCtl.param.dataReadInfo.startAddress;

    TRACE_TERSE2("Frame start address: 0x%x., preamble: 0x%x",
                 (UInt)ioDataCtl.param.dataReadInfo.startAddress,
                 *(UInt *)ioDataCtl.param.dataReadInfo.startAddress);

    pDec->wridx++;
    if(pDec->wridx == DEC_INPBUF_CONFIG_QUEUE_SIZE) {
        pDec->wridx = 0;
    }
}

void decUpdateInpBufConfig(PAF_AST_Config *pAstCfg, asipDecProc_t *pDec)
{
    PAF_InpBufConfig *pBufConfig = &(pAstCfg->xInp[pAstCfg->masterDec].inpBufConfig);

    pBufConfig->base.pVoid   = pDec->inpBufConfigQueue[pDec->rdidx].base.pVoid;
    pBufConfig->sizeofBuffer = pDec->inpBufConfigQueue[pDec->rdidx].sizeofBuffer;
    pBufConfig->pntr.pSmInt  = pDec->inpBufConfigQueue[pDec->rdidx].pntr.pSmInt;

    pDec->rdidx++;
    if(pDec->rdidx == DEC_INPBUF_CONFIG_QUEUE_SIZE) {
        pDec->rdidx = 0;
    }
}

Int decIbConfigQueNotEmpty(asipDecProc_t *pDec)
{
    if(pDec->rdidx != pDec->wridx) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}

Int decErrorCheck(const PAF_ASIT_Params *pP,
                  const PAF_ASIT_Patchs *pQ,
                  PAF_ASIT_Config *pAsitCfg,
                  Int sourceSelect
)
{
    Int retVal, getVal, zMD;
    Int8 sourceConfig;

    retVal = DEC_NO_ERR;

    // Check if source has configured to NONE
    zMD = pAsitCfg->pAstCfg->masterDec;
    sourceConfig = sharedMemReadInt8(&(pAsitCfg->pAstCfg->xDec[zMD].decodeStatus.sourceSelect),
                                     GATEMP_INDEX_DEC);
    if (sourceConfig == PAF_SOURCE_NONE || sourceSelect == PAF_SOURCE_NONE) {
        TRACE_VERBOSE0("decDecodeFsm: sourceSelect == PAF_SOURCE_NONE");
        retVal = DEC_ERR_SOURCE_NONE;
    }

    // Process commands (decode)
    getVal = pP->fxns->decodeCommand(pP, pQ, pAsitCfg);
    if (getVal == ASPERR_QUIT) {
        TRACE_VERBOSE0("decDecodeFsm. %d: state = QUIT");
        retVal = DEC_ERR_ASPERR_QUIT;
    }
    else if (getVal == ASPERR_ABORT) {
        TRACE_VERBOSE0("decDecodeFsm. %d: return getVal");

        // Return here if ASPERR_ABORT
        retVal = DEC_ERR_ASPERR_ABORT;
    }
    else {
        ;  // No error
    }

    return retVal;
}  /* decErrorCheck */


Int decCheckMajorAu(PAF_AST_Config *pAstCfg)
{
    Int8 sourceDecode, sampleRate;
    Int zMD;

    zMD = pAstCfg->masterDec;

    sourceDecode = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceDecode),
                                     GATEMP_INDEX_DEC);
    sampleRate   = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sampleRate),
                                     GATEMP_INDEX_DEC);
    if ( ( (sourceDecode == PAF_SOURCE_THD)     ||
           (sourceDecode == PAF_SOURCE_DXP)     ||
           (sourceDecode == PAF_SOURCE_DTSHD)
         )
       &&( sampleRate == PAF_SAMPLERATE_UNKNOWN)
       ) {
        return FALSE;
    }
    else {
        return TRUE;
    }
} /* decCheckMajorAu*/


// -----------------------------------------------------------------------------
// ASIT Decoding Function - Reinitialization of Decode
//
//   Name:      decodeInit
//   Purpose:   Decoding Function for reinitializing the decoding process.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
// -----------------------------------------------------------------------------
static Int decodeInit(
        const PAF_ASIT_Params *pP,
        PAF_ASIT_Config *pAsitCfg,
        Int sourceSelect)
{
    PAF_AST_Config *pAstCfg;
    PAF_AST_IoInp  *pInp;
    AspMsgMaster_Handle hAspMsgMaster;  // ASIT message master handle
    //PAF_AST_DecOpCircBufCtl *pCbCtl;    /* Decoder output circular buffer control */
    Int as;                             /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode/encode counter */
    Int errno = 0;                      /* error number */
    Int zI, zS;
    Int argIdx;
    Int8 tempVar8;
    char decMsgBuf[ASP_MSG_BUF_LEN];
    Int status;

    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to AST common (shared) configuration
    pInp    = pAsitCfg->pIoInp;                 // get pointer to IO configuration
    hAspMsgMaster = pAsitCfg->hAspMsgMaster;    // get message master handle

    as = pAstCfg->as;
    (void)as;  // clear compiler warning in case not used with tracing disabled

    //pCbCtl = &pAsitCfg->pAspmCfg->decOpCircBufCtl; // get pointer to circular buffer control

    // reset frameCount
    for (z=DECODE1; z < DECODEN; z++)
    {
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode),
                                     GATEMP_INDEX_DEC);
        if (tempVar8)
        {
            sharedMemWriteInt(&(pAstCfg->xDec[z].decodeStatus.frameCount),
                              (Int)0, GATEMP_INDEX_DEC);
        }
    }

    // loop through all supported inputs
    for (z=DECODE1; z < DECODEN; z++)
    {
        zI = pP->inputsFromDecodes[z];
        zS = pP->streamsFromDecodes[z];
        (void)zS; // clear compiler warning in case not used with tracing disabled
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode),
                                     GATEMP_INDEX_DEC);
        if (pInp[zI].hIoPhy && tempVar8)
        {
            Uns gear;
            TRACE_VERBOSE1("AS%d: PAF_ASIT_decodeInit: initializing decode", as+zS);

            // write back Dec configuration
            Cache_wb(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
            Cache_wait();

            // send dec activate message to slave
            TRACE_VERBOSE1("AS%d: PAF_ASIT_decodeInit: sending dec activate message to slave", as+zS);
            argIdx = 0; // set decIdx (zone index)
            *(Int32 *)&decMsgBuf[argIdx] = z;
            status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_DEC_ACTIVATE, decMsgBuf);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInit: error in sending DEC_ACTIVATE message ");
                SW_BREAKPOINT; // temporary
                return ASIP_ERR_DECODE_MSG; // temporary
            }

            TRACE_VERBOSE1("AS%d: PAF_ASIT_decodeInit: waiting slave to finish dec activate", as+zS);
            status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_DEC_ACTIVATE_DONE, NULL, TRUE);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInit: error in receiving DEC_ACTIVATE_DONE ack message ");
                SW_BREAKPOINT; // temporary
                return ASIP_ERR_DECODE_MSG; // temporary
            }
            TRACE_VERBOSE1("AS%d: PAF_ASIT_decodeInit: slave finished dec activate", as+zS);

            // send dec reset message to slave
            argIdx = 0; // set decIdx
            *(Int32 *)&decMsgBuf[argIdx] = z;
            status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_DEC_RESET, decMsgBuf);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInit: error in sending DEC_RESET message ");
                SW_BREAKPOINT; // temporary
                return ASIP_ERR_DECODE_MSG; // temporary
            }

            TRACE_VERBOSE1("AS%d: PAF_ASIT_decodeInit: waiting slave to finish dec reset", as+zS);
            status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_DEC_RESET_DONE, decMsgBuf, TRUE);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInit: error in sending DEC_RESET message ");
                SW_BREAKPOINT; // temporary
                return ASIP_ERR_DECODE_MSG; // temporary
            }
            else
            {
                argIdx = 0; // get decErrno
                errno = *(Int32 *)&decMsgBuf[argIdx];
            }
            TRACE_VERBOSE1("AS%d: PAF_ASIT_decodeInit: slave finished dec reset", as+zS);

            // invalidate Dec configuration
            Cache_inv(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
            Cache_wait();

            if (errno != 0) {
                return ASIP_ERR_DECODE_MSG;
            }

            gear = (Uns)sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.aspGearControl),
                                          GATEMP_INDEX_DEC);
            tempVar8 = gear < GEARS ? gear : 0;
            sharedMemWriteInt8(&(pAstCfg->xDec[z].decodeStatus.aspGearStatus),
                               tempVar8, GATEMP_INDEX_DEC);

            // Compute decoder frame length based on source selection
            // JXU: what does this function do? why does it only return PCM frame length?
            /*frameLength = getFrameLengthSourceSel(pP, sourceSelect);

            pAstCfg->xDec[z].decodeControl.frameLength = frameLength;
            pAstCfg->xDec[z].decodeInStruct.sampleCount = frameLength;
            pAstCfg->xDec[z].decodeControl.sampleRate = PAF_SAMPLERATE_UNKNOWN;*/

/*
            if (z != zMD) {    // JXTODO: implement similar thing with new I/O
                if (errno = SIO_idle(pAstCfg->xInp[zI].hRxSio)) {
                    return errno;
                }
            }
*/
/*//JXTODO: find out if frameLength needs to be passed to I/O DATA or I/O PHY.
            ioDataCtl.code = IODATA_CTL_SET_PCM_FRAME_LENGTH;
            ioDataCtl.param.frameLengthPcm = frameLength;
            ioDataControl(pInp[zI].hIoData, &ioDataCtl);
*/
            //JXTODO: do we need to update input status here again?
            if (errno = asitUpdateInputStatus(pInp[zI].pRxParams,
                                              &pAstCfg->xInp[zI].inpBufStatus,
                                              &pAstCfg->xInp[zI].inpBufConfig)) {
                return ASIP_ERR_INPUT_CFG;
            }
        } /* end of if(hIoPhy && decodeStatus.mode) */
    } /* end of for (z=DECODE1; z < DECODEN; z++) */

    pAsitCfg->inpDec.frame = 0;
    pAsitCfg->inpDec.block = 0;
    pAsitCfg->inpDec.state = DEC_STATE_INFO_SND;
    //pAsitCfg->inpDec.state = DEC_STATE_INFO;
    pAsitCfg->inpDec.rdidx = 0;
    pAsitCfg->inpDec.wridx = 0;

    return ASIP_NO_ERR;
}  /* decodeInit */


// -----------------------------------------------------------------------------
// ASIT Decoding Function - Reinitialization of Decode
//
//   Name:      decDecodeInit
//   Purpose:   Decoding Function for reinitializing the decoding process.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
// -----------------------------------------------------------------------------
static Int decodeInitSnd(
        const PAF_ASIT_Params *pP,
        PAF_ASIT_Config *pAsitCfg,
        Int sourceSelect)
{
    PAF_AST_Config *pAstCfg;
    PAF_AST_IoInp  *pInp;
    AspMsgMaster_Handle hAspMsgMaster;  // ASIT message master handle
    //PAF_AST_DecOpCircBufCtl *pCbCtl;    /* Decoder output circular buffer control */
    Int as;                             /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode/encode counter */
    Int errno;                          /* error number */
    Int zI, zS;
    Int argIdx;
    Int8 tempVar8;
    char decMsgBuf[ASP_MSG_BUF_LEN];
    Int status;

    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to AST common (shared) configuration
    pInp    = pAsitCfg->pIoInp;                 // get pointer to IO configuration
    hAspMsgMaster = pAsitCfg->hAspMsgMaster;    // get message master handle

    as = pAstCfg->as;
    (void)as;  // clear compiler warning in case not used with tracing disabled

    //pCbCtl = &pAsitCfg->pAspmCfg->decOpCircBufCtl; // get pointer to circular buffer control

    // reset frameCount
    for (z=DECODE1; z < DECODEN; z++)
    {
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode),
                                     GATEMP_INDEX_DEC);
        if (tempVar8)
        {
            sharedMemWriteInt(&(pAstCfg->xDec[z].decodeStatus.frameCount),
                              (Int)0, GATEMP_INDEX_DEC);
        }
    }

    // loop through all supported inputs
    for (z=DECODE1; z < DECODEN; z++)
    {
        zI = pP->inputsFromDecodes[z];
        zS = pP->streamsFromDecodes[z];
        (void)zS; // clear compiler warning in case not used with tracing disabled
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode),
                                     GATEMP_INDEX_DEC);
        if (pInp[zI].hIoPhy && tempVar8)
        {
            TRACE_VERBOSE1("AS%d: PAF_ASIT_decodeInit: initializing decode", as+zS);

            // write back Dec configuration
            Cache_wb(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
            Cache_wait();

            // send dec activate message to slave
            TRACE_VERBOSE1("AS%d: PAF_ASIT_decodeInit: sending dec activate message to slave", as+zS);
            argIdx = 0; // set decIdx (zone index)
            *(Int32 *)&decMsgBuf[argIdx] = z;
            status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_DEC_ACTIVATE, decMsgBuf);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInit: error in sending DEC_ACTIVATE message ");
                SW_BREAKPOINT; // temporary
                return ASIP_ERR_DECODE_MSG; // temporary
            }

            TRACE_VERBOSE1("AS%d: PAF_ASIT_decodeInit: waiting slave to finish dec activate", as+zS);
            status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_DEC_ACTIVATE_DONE, NULL, TRUE);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInit: error in receiving DEC_ACTIVATE_DONE ack message ");
                SW_BREAKPOINT; // temporary
                return ASIP_ERR_DECODE_MSG; // temporary
            }
            TRACE_VERBOSE1("AS%d: PAF_ASIT_decodeInit: slave finished dec activate", as+zS);

            // send dec reset message to slave
            argIdx = 0; // set decIdx
            *(Int32 *)&decMsgBuf[argIdx] = z;
            status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_DEC_RESET, decMsgBuf);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInit: error in sending DEC_RESET message ");
                SW_BREAKPOINT; // temporary
                return ASIP_ERR_DECODE_MSG; // temporary
            }

            TRACE_VERBOSE1("AS%d: PAF_ASIT_decodeInit: waiting slave to finish dec reset", as+zS);
        } /* end of if(hIoPhy && decodeStatus.mode) */
    } /* end of for (z=DECODE1; z < DECODEN; z++) */

    pAsitCfg->inpDec.state = DEC_STATE_INIT_ACK;

    return ASIP_NO_ERR;
}  /* decDecodeInitSnd */


static Int decodeInitAck(
        const PAF_ASIT_Params *pP,
        PAF_ASIT_Config *pAsitCfg,
        Int sourceSelect)
{
    PAF_AST_Config *pAstCfg;
    PAF_AST_IoInp  *pInp;
    AspMsgMaster_Handle hAspMsgMaster;  // ASIT message master handle
    Int as;                             /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode/encode counter */
    Int errno;                          /* error number */
    Int zI, zS;
    Int8 tempVar8;
    char decMsgBuf[ASP_MSG_BUF_LEN];

    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to AST common (shared) configuration
    pInp    = pAsitCfg->pIoInp;                 // get pointer to IO configuration

    as = pAstCfg->as;
    (void)as;  // clear compiler warning in case not used with tracing disabled


    // loop through all supported inputs
    for (z=DECODE1; z < DECODEN; z++)
    {
        Uns gear;
        int status;
        zI = pP->inputsFromDecodes[z];
        zS = pP->streamsFromDecodes[z];
        (void)zS; // clear compiler warning in case not used with tracing disabled

        hAspMsgMaster = pAsitCfg->hAspMsgMaster;    // get message master handle
        status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_DEC_RESET_DONE, decMsgBuf, FALSE);
        if (status != ASP_MSG_NO_ERR)
        {
            TRACE_TERSE0("decodeInit: error in sending DEC_RESET message ");
            SW_BREAKPOINT; // temporary
            return ASIP_ERR_DECODE_MSG; // temporary
        }
        else
        {
            errno = *(Int32 *)&decMsgBuf[0];
            if (errno != 0) {
                return ASIP_ERR_DECODE_MSG;
            }
        }
        TRACE_VERBOSE1("AS%d: PAF_ASIT_decodeInit: slave finished dec reset", as+zS);

        // invalidate Dec configuration
        Cache_inv(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
        Cache_wait();

        if (errno != 0) {
            return ASIP_ERR_DECODE_MSG;
        }

        gear = (Uns)sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.aspGearControl),
                                      GATEMP_INDEX_DEC);
        tempVar8 = gear < GEARS ? gear : 0;
        sharedMemWriteInt8(&(pAstCfg->xDec[z].decodeStatus.aspGearStatus),
                           tempVar8, GATEMP_INDEX_DEC);

        //JXTODO: do we need to update input status here again?
        if (errno = asitUpdateInputStatus(pInp[zI].pRxParams,
                                          &pAstCfg->xInp[zI].inpBufStatus,
                                          &pAstCfg->xInp[zI].inpBufConfig)) {
            return ASIP_ERR_INPUT_CFG;
        }
    } /* end of for (z=DECODE1; z < DECODEN; z++) */

    pAsitCfg->inpDec.frame = 0;
    pAsitCfg->inpDec.block = 0;
    pAsitCfg->inpDec.rdidx = 0;
    pAsitCfg->inpDec.wridx = 0;

    return ASIP_NO_ERR;
}  /* decDecodeInitAck */

Int decDecodeComplete( const PAF_ASIT_Params *pP,
                       PAF_ASIT_Config *pAsitCfg)
{
    PAF_AST_Config *pAstCfg;
    PAF_AST_IoInp  *pInp;
    AspMsgMaster_Handle hAspMsgMaster;  // ASIT message master handle
    Int as;                             /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode/encode counter */
    Int zI;
    Int argIdx;
    Int8 tempVar8;
    UInt32 ackCmd;
    char decMsgBuf[ASP_MSG_BUF_LEN];
    Int status;
    UInt16 numSndMsgUnack;      // number of unknowledged send messages

    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to AST common (shared) configuration
    pInp    = pAsitCfg->pIoInp;                 // get pointer to IO configuration
    hAspMsgMaster = pAsitCfg->hAspMsgMaster;    // get message master handle
    as = pAstCfg->as;
    (void)as;  // clear compiler warning in case not used with tracing disabled

    for (z=DECODE1; z < DECODEN; z++)
    {
        zI = pP->inputsFromDecodes[z];

        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode),
                                     GATEMP_INDEX_DEC);
        if (pInp[zI].hIoPhy && tempVar8)
        {
            TRACE_VERBOSE1("decDecodeComplete: AS%d: finalizing decode", as+z);

            // Get number of pending ack messages
            AspMsgGetNumSndMsgUnack(hAspMsgMaster, &numSndMsgUnack);
            TRACE_TERSE1("decDecodeComplete: number of pending ACK messages=%d.", numSndMsgUnack);
            
            // Process any pending ack messages as they arrive
            while (numSndMsgUnack > 0)
            {
                // Receive pending acknowledge message
                status = AspMsgRcvAckUnk(hAspMsgMaster, &ackCmd, decMsgBuf, TRUE);
                if (status != ASP_MSG_NO_ERR)
                {
                    TRACE_TERSE0("decDecodeComplete: error in receiving ACK message.");
                    SW_BREAKPOINT; // debug
                    return DEC_ERR_COMPLETE_MSG;
                }
                TRACE_TERSE1("decDecodeComplete: Rx pending ACK message=%d.", ackCmd);
                
                //
                // Process acknowledge message
                //
                status = decDecodeCompleteProcAck(pP, pAsitCfg, z, ackCmd, decMsgBuf);
                if (status != DEC_NO_ERR)
                {
                    TRACE_TERSE1("decDecodeComplete: error processing ACK message=%d.", ackCmd);
                    return status;
                }

                AspMsgGetNumSndMsgUnack(hAspMsgMaster, &numSndMsgUnack);
            }
            
            // send dec deactivate message to slave
            argIdx = 0; // set decIdx
            *(Int32 *)&decMsgBuf[argIdx] = z;
            status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_DEC_DEACTIVATE, decMsgBuf);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decDecodeComplete: error in sending DEC_DEACTIVATE message.");
                SW_BREAKPOINT; // debug
                return DEC_ERR_COMPLETE_MSG;
            }
            status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_DEC_DEACTIVATE_DONE, NULL, TRUE);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decDecodeComplete: error in receiving DEC_DEACTIVATE_DONE message.");
                SW_BREAKPOINT; // debug
                return DEC_ERR_COMPLETE_MSG;
            }
        }
        else
        {
            TRACE_VERBOSE1("decDecodeComplete: AS%d: processing decode <ignored>", as+z);
        }
    }

    return DEC_NO_ERR;
}  /* decDecodeComplete */


// -----------------------------------------------------------------------------
// Decoding Function - Frame-Final Processing
//
//   Name:      decDecodeFinalTest
//   Purpose:   Decoding Function for determining whether processing of the
//              current frame is complete.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    0 if incomplete, and 1 if complete.
//   Trace:     None.
//

Int
decDecodeFinalTest(
    const PAF_ASIT_Params *pP,
    const PAF_ASIT_Patchs *pQ,
    PAF_ASIT_Config *pAsitCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int zMD;
    Int sourceSelect;
    Int sourceProgram;
    Int8 tempVar8, temp2Var8;

    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    zMD = pAstCfg->masterDec;

    sourceSelect  = (Int)sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceSelect),
                                           GATEMP_INDEX_DEC);
    sourceProgram = (Int)sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceProgram),
                                           GATEMP_INDEX_DEC);

    if ((sourceSelect == PAF_SOURCE_NONE) || (sourceSelect == PAF_SOURCE_PASS))
    {
        return DEC_ERR_DECODE_FINAL;
    }

    // The following allows for Force modes to switch without command deferral. This might
    // be better suited for inclusion in DIB_requestFrame, but for now will reside here.
    if ((sourceSelect == PAF_SOURCE_SNG) || (sourceSelect > PAF_SOURCE_BITSTREAM))
    {
        if (sourceSelect == PAF_SOURCE_DTSALL)
        {
            if (sourceProgram != PAF_SOURCE_DTS11 &&
                sourceProgram != PAF_SOURCE_DTS12 &&
                sourceProgram != PAF_SOURCE_DTS13 &&
                sourceProgram != PAF_SOURCE_DTS14 &&
                sourceProgram != PAF_SOURCE_DTS16 &&
                sourceProgram != PAF_SOURCE_DTSHD)
            {
                return DEC_ERR_DECODE_FINAL;
            }
        }
        else if (sourceSelect == PAF_SOURCE_PCMAUTO)
        {
            if (sourceProgram != PAF_SOURCE_PCM)
            {
                return DEC_ERR_DECODE_FINAL;
            }
        }
        else
        {
            tempVar8  = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceDecode),
                                          GATEMP_INDEX_DEC);
            temp2Var8 = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceSelect),
                                          GATEMP_INDEX_DEC);
            if (temp2Var8 != tempVar8)
            {
                return DEC_ERR_DECODE_FINAL;
            }
        }
    }

    return DEC_NO_ERR;
} //decDecodeFinalTest

Int decInfoSnd(
        const PAF_ASIT_Params *pP,
        const PAF_ASIT_Patchs *pQ,
        PAF_ASIT_Config *pAsitCfg
)
{
    PAF_AST_Config *pAstCfg;
    PAF_AST_IoInp  *pInp;
    AspMsgMaster_Handle hAspMsgMaster;  // ASIT message master handle
    Int as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                     /* input/decode/stream counter */
    Int errno;                 /* error number */
    Int zD, zI, zS, zX;
    Int argIdx;
    Int8 tempVar8;
    char decMsgBuf[ASP_MSG_BUF_LEN];
    Int status;

    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to common (shared) configuration
    pInp    = pAsitCfg->pIoInp;                 // get pointer to IO configuration
    hAspMsgMaster = pAsitCfg->hAspMsgMaster;    // get message master handle
    as = pAstCfg->as;

    // Set decode control: sample rate, emphasis
    for (z=INPUT1; z < INPUTN; z++)
    {
        zD = z;
        for (zX = DECODE1; zX < DECODEN; zX++)
        {
            if (pP->inputsFromDecodes[zX] == z) {
                zD = zX;
                break;
            }
        }

        if (pInp[z].hIoPhy) {
            //determine associated decoder
            if (pAstCfg->xInp[z].inpBufStatus.sampleRateStatus !=
                pAstCfg->xDec[zD].decodeControl.sampleRate) {
                if (pAstCfg->xDec[zD].decodeControl.sampleRate == PAF_SAMPLERATE_UNKNOWN) {
                    pAstCfg->xDec[zD].decodeControl.sampleRate =
                        pAstCfg->xInp[z].inpBufStatus.sampleRateStatus;
                }
                else {
                    //TRACE_TERSE1("decDecodeInfo: AS%d: return error ASPERR_INFO_RATECHANGE", as+pAstCfg->masterStr);
                    TRACE_TERSE2("inpBufStatus.sampleRateStatus: 0x%x, decodeControl.sampleRate: 0x%x",
                        pAstCfg->xInp[z].inpBufStatus.sampleRateStatus,
                        pAstCfg->xDec[zD].decodeControl.sampleRate);
                    // return (ASPERR_INFO_RATECHANGE);
                }
            }

            tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[zD].decodeStatus.sourceDecode),
                                         GATEMP_INDEX_DEC);
            pAstCfg->xDec[zD].decodeControl.emphasis =
                tempVar8 != PAF_SOURCE_PCM
                ? PAF_IEC_PREEMPHASIS_NO // fix for Mantis ID #119
                : pAstCfg->xInp[z].inpBufStatus.emphasisStatus;
        }
        else {
            pAstCfg->xDec[zD].decodeControl.sampleRate = PAF_SAMPLERATE_UNKNOWN;
            pAstCfg->xDec[zD].decodeControl.emphasis = PAF_IEC_PREEMPHASIS_UNKNOWN;
        }
    } /* end of for (z=INPUT1; z < INPUTN; z++) */

    // Decode info
    for (z=DECODE1; z < DECODEN; z++)
    {
        zI = pP->inputsFromDecodes[z];
        zS = pP->streamsFromDecodes[z];
        (void)zS; // clear compiler warning in case not used with tracing disabled

        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode),
                                     GATEMP_INDEX_DEC);
        if (pInp[zI].hIoPhy  && tempVar8)
        {
            TRACE_GEN2("PAF_ASIT_decodeInfo: AS%d: processing frame %d -- info", as+zS, pAsitCfg->inpDec.frame);

            if (errno = asitUpdateInputStatus(pInp[zI].pRxParams,
                                              &pAstCfg->xInp[zI].inpBufStatus,
                                              &pAstCfg->xInp[zI].inpBufConfig)) {
                TRACE_TERSE1("asitUpdateInputStatus return error errno 0x%x.", errno);
                return DEC_ERR_INFO_SNDMSG;
            }

#if 0 // debug
            // capture input buffer
            capIb(pAstCfg->xInp[zI].pInpBuf);
            gCapIb_cnt++;
#endif

            // write back Inp configuration
            Cache_wb(&pAstCfg->xInp[zI], sizeof(PAF_AST_InpBuf), Cache_Type_ALLD, 0);
            // write back Dec configuration
            Cache_wb(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
            Cache_wait();

            // send info message to slave
            argIdx = 0; // set decIdx
            *(Int32 *)&decMsgBuf[argIdx] = z;
            status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_DEC_INFO, decMsgBuf);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInfo: error in sending DEC_INFO message ");
                SW_BREAKPOINT; // temporary
                return -1;     // temporary
            }
/*            else
            {
                argIdx = 0; // get decErrno
                errno = *(Int32 *)&decMsgBuf[argIdx];
            }

            if (errno) {
                TRACE_TERSE1("decInfoSndMSg return error errno 0x%x.", errno);
                return DEC_ERR_INFO_SNDMSG;
            }
*/
        }
    } // z=DECODE1 to DECODEN

//////////////////////////////////////////////////////////////////////////////
    //asitPostInfoEvent(); // simulation
//////////////////////////////////////////////////////////////////////////////

    return ASIP_NO_ERR;
}  /* decInfoSnd() */


Int decInfoAck(
        const PAF_ASIT_Params *pP,
        const PAF_ASIT_Patchs *pQ,
        PAF_ASIT_Config *pAsitCfg
)
{
    PAF_AST_Config *pAstCfg;
    PAF_AST_IoInp  *pInp;
    AspMsgMaster_Handle hAspMsgMaster;
    //Int as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                     /* input/decode/stream counter */
    Int errno;                 /* error number */
    Int zI;
    Int zMD;
    Int zMI;
    Int argIdx;
    Int8 tempVar8;
    Int tempVar;
    char decMsgBuf[ASP_MSG_BUF_LEN];
    Int status;

    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to common (shared) configuration
    pInp    = pAsitCfg->pIoInp;                 // get pointer to IO configuration
    hAspMsgMaster = pAsitCfg->hAspMsgMaster;    // get message master handle
    //as = pAstCfg->as;
    zMD = pAstCfg->masterDec;
    zMI = pP->zone.master;

    // Decode info
    for (z=DECODE1; z < DECODEN; z++)
    {
        zI = pP->inputsFromDecodes[z];
        
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode),
                                     GATEMP_INDEX_DEC);
        if (pInp[zI].hIoPhy  && tempVar8)
        {
            // acknowledge info message from slave
            errno  = 0;
            //argIdx = 0; // set decIdx
            //*(Int32 *)&decMsgBuf[argIdx] = z;
            status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_DEC_INFO_DONE, decMsgBuf, FALSE);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInfo: error in receiving DEC_INFO_DONE ack message ");
                SW_BREAKPOINT; // temporary
                errno = -1;    // temporary error return value
                return errno;     // temporary
            }
            else
            {
                argIdx = 0; // get decErrno
                errno = *(Int32 *)&decMsgBuf[argIdx];
            }

            // invalidate Dec configuration
            Cache_inv(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
            Cache_wait();

            if (errno) {
                TRACE_TERSE1("decInfoAck return error errno 0x%x.", errno);
                return DEC_ERR_INFO_ACKMSG;
            }

            // increment decoded frame count
            tempVar = sharedMemReadInt(&(pAstCfg->xDec[z].decodeStatus.frameCount),
                                       GATEMP_INDEX_DEC);
            tempVar += 1;
            sharedMemWriteInt(&(pAstCfg->xDec[z].decodeStatus.frameCount),
                              tempVar, GATEMP_INDEX_DEC);
        }
    } // z=DECODE1 to DECODEN

    // query IB for latest sourceProgram (needed if we started decoding due to a force mode)
    tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.mode),
                                 GATEMP_INDEX_DEC);
    if (tempVar8)
    {
        sharedMemWriteInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceProgram),
                           pInp[zMI].sourceProgram, GATEMP_INDEX_DEC);
    }

    // since now decoding update decode status for all enabled decoders
    for (z=DECODE1; z < DECODEN; z++)
    {
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode),
                                     GATEMP_INDEX_DEC);
        if (tempVar8)
        {
            tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.sourceProgram),
                                         GATEMP_INDEX_DEC);
            sharedMemWriteInt8(&(pAstCfg->xDec[z].decodeStatus.sourceDecode),
                               tempVar8, GATEMP_INDEX_DEC);

            tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.sourceSelect),
                                         GATEMP_INDEX_DEC);
            if (tempVar8 == PAF_SOURCE_SNG)
            {
                tempVar8 = PAF_SOURCE_SNG;
                sharedMemWriteInt8(&(pAstCfg->xDec[z].decodeStatus.sourceDecode),
                                   tempVar8, GATEMP_INDEX_DEC);
            }
        }
    }

    return ASIP_NO_ERR;
}


static Int decDecodeSnd(const PAF_ASIT_Params *pP, const PAF_ASIT_Patchs *pQ,
                           PAF_ASIT_Config *pAsitCfg)
{
    PAF_AST_Config *pAstCfg;
    PAF_AST_IoInp  *pInp;
    AspMsgMaster_Handle hAspMsgMaster;  // ASIT message master handle
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* decode/stream counter */
    Int argIdx;
    //Int cbErrno;
    Int errno;
    char decMsgBuf[ASP_MSG_BUF_LEN];
    Int status;

    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to common (shared) configuration
    pInp    = pAsitCfg->pIoInp;                 // get pointer to IO configuration
    hAspMsgMaster = pAsitCfg->hAspMsgMaster;    // get message master handle
    as = pAstCfg->as;
    (void)as; // clear compiler warning in case not used with tracing disabled

    // Decode data
    for (z=DECODE1; z < DECODEN; z++)
    {
        Int zI = pP->inputsFromDecodes[z];
        Int zS = pP->streamsFromDecodes[z];
        (void)zS; // clear compiler warning in case not used with tracing disabled
        if ( pInp[zI].hIoPhy && pAstCfg->xDec[z].decodeStatus.mode )
        {
            TRACE_GEN2("PAF_ASIT_decodeDecode: AS%d: decodeDecode: processing block %d -- decode", as+zS, pAsitCfg->inpDec.block);

            TRACE_VERBOSE3("PAF_ASIT_decodeDecode: AS%d: decodeDecode: decoding from 0x%x (base) 0x%x (ptr)",
                    as+zS,
                    (IArg)pAstCfg->xInp[z].pInpBuf->base.pVoid,
                    (IArg)pAstCfg->xInp[z].pInpBuf->head.pVoid);

#if 0 // debug
            // capture input buffer
            capIbPcm(pAstCfg->xInp[z].pInpBuf);
#endif
            
            // write back Dec configuration
            Cache_wb(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
            Cache_wait();

            // send decode message to slave
            errno  = 0;
            argIdx = 0; // set decIdx
            *(Int32 *)&decMsgBuf[argIdx] = z;
            status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_DEC_DECODE, decMsgBuf);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeDecode: error in sending DEC_DECODE message ");
                SW_BREAKPOINT; // temporary
                return -1;     // temporary
            }
/*            else
            {
                argIdx = 0; // get decErrno
                errno = *(Int32 *)&decMsgBuf[argIdx];
                argIdx += sizeof(Int32); // get cbErrno
                cbErrno = *(Int32 *)&decMsgBuf[argIdx];
                if (cbErrno != 0)
                {
                    gCbWrtAfErrCnt++;
                    TRACE_TERSE1("CB write error=%d", cbErrno);
                    //SW_BREAKPOINT; // temporary
                }

                if(errno) {
                    TRACE_TERSE1("decDecodeSnd return error errno 0x%x.", errno);
                    return DEC_ERR_DECODE_SNDMSG;
                }
            }*/

        } // hIoPhy && decodeStatus.mode
        else {
            TRACE_VERBOSE2("AS%d: PAF_ASIT_decodeDecode: processing block %d -- decode <ignored>", as+zS, pAsitCfg->inpDec.block);
        }
    } // z=DECODE1 to DECODEN

//////////////////////////////////////////////////////////////////////////////
    //asitPostDecEvent(); // simulation
//////////////////////////////////////////////////////////////////////////////

    return ASIP_NO_ERR;
} /* decDecodeSnd() */


static Int decDecodeAck(const PAF_ASIT_Params *pP, const PAF_ASIT_Patchs *pQ,
                           PAF_ASIT_Config *pAsitCfg)
{
    PAF_AST_Config *pAstCfg;
    PAF_AST_IoInp  *pInp;
    AspMsgMaster_Handle hAspMsgMaster;  // ASIT message master handle
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* decode/stream counter */
    Int errno;                  /* error number */
    Int argIdx;
    Int cbErrno;
    char decMsgBuf[ASP_MSG_BUF_LEN];
    Int status;

    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to common (shared) configuration
    pInp    = pAsitCfg->pIoInp;                 // get pointer to IO configuration
    hAspMsgMaster = pAsitCfg->hAspMsgMaster;    // get message master handle
    as = pAstCfg->as;
    (void)as; // clear compiler warning in case not used with tracing disabled

    // Decode data
    for (z=DECODE1; z < DECODEN; z++)
    {
        Int zI = pP->inputsFromDecodes[z];
        Int zS = pP->streamsFromDecodes[z];
        (void)zS; // clear compiler warning in case not used with tracing disabled
        if ( pInp[zI].hIoPhy && pAstCfg->xDec[z].decodeStatus.mode )
        {
            // receive decode ack message from slave
            errno  = 0;
            //argIdx = 0; // set decIdx
            //*(Int32 *)&decMsgBuf[argIdx] = z;
            status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_DEC_DECODE_DONE, decMsgBuf, FALSE);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeDecode: error in receiving DEC_DECODE_DONE ack message ");
                SW_BREAKPOINT; // temporary
                errno = -1; // temporary error return value
                return errno;
            }
            else
            {
                argIdx = 0; // get decErrno
                errno = *(Int32 *)&decMsgBuf[argIdx];
                argIdx += sizeof(Int32); // get cbErrno
                cbErrno = *(Int32 *)&decMsgBuf[argIdx];
                if (cbErrno != 0)
                {
                    gCbWrtAfErrCnt++;
                    TRACE_TERSE1("CB write error=%d", cbErrno);
                    //SW_BREAKPOINT; // temporary
                }
            }

            // invalidate Dec configuration
            Cache_inv(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
            Cache_wait();

            if(errno) {
                TRACE_TERSE1("decDecodeAck return error errno 0x%x.", errno);
                return DEC_ERR_DECODE_ACKMSG;
            }

#if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)
            as_traceChannels(pAsitCfg, z);
#endif
        } // hIoPhy && decodeStatus.mode
        else {
            TRACE_VERBOSE2("AS%d: PAF_ASIT_decodeDecode: processing block %d -- decode <ignored>", as+zS, pAsitCfg->inpDec.block);
        }
    } // z=DECODE1 to DECODEN

    return ASIP_NO_ERR;
} /* decDecodeAck */

// Processes unacknowledged Ack messages in decDecodeComplete()
static Int decDecodeCompleteProcAck(
    const PAF_ASIT_Params *pP, 
    PAF_ASIT_Config *pAsitCfg,
    Int z,
    UInt32 ackCmd, 
    char *decMsgBuf
)
{
    Int status;
    
    switch(ackCmd)
    {
        case ASP_MASTER_DEC_INFO_DONE:
            status = decDecodeCompleteProcDecInfoAck(pP, pAsitCfg, z, decMsgBuf);
            break;
        
        case ASP_MASTER_DEC_DECODE_DONE: 
            status = decDecodeCompleteProcDecDecodeAck(pP, pAsitCfg, z, decMsgBuf);
            break;
		
        // We need to acknowledge errors in reset state, as we do not pend for reset
		//acknowledgement in decodeInitSnd
		case ASP_MASTER_DEC_RESET_DONE:
		    status = decDecodeCompleteProcDecResetAck(pP, pAsitCfg, z, decMsgBuf);
			break;

		case ASP_MASTER_DEC_CONTROL_DONE:
		    status = DEC_NO_ERR;
			break;
            
        default:
            status = DEC_ERR_COMPLETE_ACKMSG;
            break;
    }
    
    return status;
} /* decDecodeCompleteProcAck */

// Processes unacknowledged Decode Ack messages in decDecodeComplete()
static Int decDecodeCompleteProcDecInfoAck(
    const PAF_ASIT_Params *pP, 
    PAF_ASIT_Config *pAsitCfg,
    Int z,
    char *decMsgBuf
)
{
    PAF_AST_Config *pAstCfg;    // pointer to common (shared) ASIT/ASDT/ASOT configuration
    PAF_AST_IoInp  *pInp;       // pointer to Input IO configuration
    Int zMD;
    Int zMI;
    Int argIdx;                 // message payload argument index
    Int tempVar;
    Int8 tempVar8;
    Int errno;

    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to common (shared) configuration
    pInp = pAsitCfg->pIoInp;                    // get pointer to Input IO configuration
    zMD = pAstCfg->masterDec;
    zMI = pP->zone.master;


    // invalidate Dec configuration
    Cache_inv(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
    Cache_wait();

    // extract message payload
    argIdx = 0; // get decErrno
    errno = *(Int32 *)&decMsgBuf[argIdx];
    
    if (errno) 
    {
        TRACE_TERSE1("decDecodeCompleteProcDecInfoAck() return error errno 0x%x.", errno);
        return DEC_ERR_COMPLETE_INFO_ACKMSG;
    }
    
    // increment decoded frame count
    tempVar = sharedMemReadInt(&(pAstCfg->xDec[z].decodeStatus.frameCount), 
        GATEMP_INDEX_DEC);
    tempVar += 1;
    sharedMemWriteInt(&(pAstCfg->xDec[z].decodeStatus.frameCount), 
        tempVar, GATEMP_INDEX_DEC);
    
    // query IB for latest sourceProgram (needed if we started decoding due to a force mode)
    tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.mode), 
        GATEMP_INDEX_DEC);
    if (tempVar8)
    {
        sharedMemWriteInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceProgram),
            pInp[zMI].sourceProgram, GATEMP_INDEX_DEC);
    }

    // update decode status for all enabled decoders
    tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode), 
        GATEMP_INDEX_DEC);
    if (tempVar8)
    {
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.sourceProgram), 
            GATEMP_INDEX_DEC);
        sharedMemWriteInt8(&(pAstCfg->xDec[z].decodeStatus.sourceDecode), 
            tempVar8, GATEMP_INDEX_DEC);

        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.sourceSelect),
            GATEMP_INDEX_DEC);
        if (tempVar8 == PAF_SOURCE_SNG)
        {
            sharedMemWriteInt8(&(pAstCfg->xDec[z].decodeStatus.sourceDecode),
                tempVar8, GATEMP_INDEX_DEC);
        }
    }

    return DEC_NO_ERR;
}

// Processes unacknowledged Decode Ack messages in decDecodeComplete()
static Int decDecodeCompleteProcDecDecodeAck(
    const PAF_ASIT_Params *pP, 
    PAF_ASIT_Config *pAsitCfg,
    Int z,
    char *decMsgBuf
)
{
    PAF_AST_Config *pAstCfg;    // pointer to common (shared) ASIT/ASDT/ASOT configuration
    Int argIdx;                 // message payload argument index
    Int errno;
    Int cbErrno;
    
    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to common (shared) configuration
    
    // invalidate Dec configuration
    Cache_inv(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
    Cache_wait();
    
    // extract message payload
    argIdx = 0; // get decErrno
    errno = *(Int32 *)&decMsgBuf[argIdx];
    argIdx += sizeof(Int32); // get cbErrno
    cbErrno = *(Int32 *)&decMsgBuf[argIdx];
    
    if (cbErrno != 0)
    {
        gCbWrtAfErrCnt++;
        TRACE_TERSE1("CB write error=%d", cbErrno);
        //SW_BREAKPOINT; // temporary
    }
    
    if (errno) 
    {
        TRACE_TERSE1("decDecodeCompleteProcDecInfoAck() return error errno 0x%x.", errno);
        return DEC_ERR_COMPLETE_DECODE_ACKMSG;
    }
    
    return DEC_NO_ERR;
}

// Processes unacknowledged Reset Ack messages in decDecodeComplete()
static Int decDecodeCompleteProcDecResetAck(
    const PAF_ASIT_Params *pP, 
    PAF_ASIT_Config *pAsitCfg,
    Int z,
    char *decMsgBuf
)
{
    PAF_AST_Config *pAstCfg;    // pointer to common (shared) ASIT/ASDT/ASOT configuration
    Int argIdx;                 // message payload argument index
    Int errno;
    Int cbErrno;
    
    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to common (shared) configuration
    
    // invalidate Dec configuration
    Cache_inv(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
    Cache_wait();
    
    // extract message payload
    argIdx = 0; // get decErrno
    errno = *(Int32 *)&decMsgBuf[argIdx];
    
    if (errno) 
    {
        TRACE_TERSE1("decDecodeCompleteProcDecResetAck() return error errno 0x%x.", errno);
        return DEC_ERR_COMPLETE_RESET_ACKMSG;
    }
    
    return DEC_NO_ERR;
}

/* Nothing past this line */
