
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamOutInit.c ========
 */

#include <xdc/std.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Memory.h>

#include <acp_mds.h>
#include "pafsio_ialg.h"
#include "stdasp.h"

#include "common.h"
#include "audioStreamProc_common.h"
#include "audioStreamOutProc.h"

// -----------------------------------------------------------------------------
// Debugging Trace Control, local to this file.
// 
#include "logp.h"

// Allow a developer to selectively enable tracing.
#define CURRENT_TRACE_MASK      0x07

#define TRACE_MASK_TERSE        0x01   // only flag errors and show init
#define TRACE_MASK_GENERAL      0x02   // half dozen lines per frame
#define TRACE_MASK_VERBOSE      0x04   // trace full operation

#if !(CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
    #undef  TRACE_TERSE0
    #undef  TRACE_TERSE1
    #undef  TRACE_TERSE2
    #undef  TRACE_TERSE3
    #undef  TRACE_TERSE4
    #define TRACE_TERSE0(a)
    #define TRACE_TERSE1(a,b)
    #define TRACE_TERSE2(a,b,c)
    #define TRACE_TERSE3(a,b,c,d)
    #define TRACE_TERSE4(a,b,c,d,e)
#endif
    
#if !(CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
    #undef  TRACE_GEN0
    #undef  TRACE_GEN1
    #undef  TRACE_GEN2
    #undef  TRACE_GEN3
    #undef  TRACE_GEN4
    #define TRACE_GEN0(a)
    #define TRACE_GEN1(a,b)
    #define TRACE_GEN2(a,b,c)
    #define TRACE_GEN3(a,b,c,d)
    #define TRACE_GEN4(a,b,c,d,e)
#endif

#if !(CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
    #undef  TRACE_VERBOSE0
    #undef  TRACE_VERBOSE1
    #undef  TRACE_VERBOSE2
    #undef  TRACE_VERBOSE3
    #undef  TRACE_VERBOSE4
    #define TRACE_VERBOSE0(a)
    #define TRACE_VERBOSE1(a,b)
    #define TRACE_VERBOSE2(a,b,c)
    #define TRACE_VERBOSE3(a,b,c,d)
    #define TRACE_VERBOSE4(a,b,c,d,e)
#endif

// -----------------------------------------------------------------------------
//
// Audio Stream Output Task definitions
//

//
// Audio Stream Processing Definitions
//
#define aspLinkInit pQ->i_aspLinkInit

//
// Encoder Definitions
//
#define encLinkInit pQ->i_encLinkInit

//
// Output Definitions
//
#define outLinkInit pP->i_outLinkInit

// -----------------------------------------------------------------------------

LINNO_DECL(TaskAsop); // Line number macros
ERRNO_DECL(TaskAsop); // Error number macros

// Create IO Buff & Phy framework components
static Int PAF_ASOT_ioCompCreate(
    PAF_AST_IoOut *pIoOut, 
    Int numOut, 
    IHeap_Handle iHeapHandle
);

// Audio Stream Output Processing initialization function
Void taskAsopFxnInit(
    const PAF_ASOT_Params *pP,
    const PAF_ASOT_Patchs *pQ
)
{
    PAF_ASOT_Config *pAsotCfg;      /* ASOT configuration pointer */
    PAF_AST_Config *pAstCfg;        /* Common (shared) configuration pointer */
    PAF_AST_IoOut *pOut;
    Int as;                         /* Audio Stream Number (1, 2, etc.) */
    Int z;                          /* input/encode/stream/decode/output counter */
    Int i;                          /* phase */
    Int zMS;
    Int status;

    Log_info0("Enter taskAsopFxnInit()");

    //
    // Audio Stream Output Task Parameters & Patch (*pP, *pQ)
    //
    if (!pP)
    {
        TRACE_TERSE0("TaskAsop: No Parameters defined. Exiting.");
        LINNO_RPRT(TaskAsop, -1);
        return;
    }

    if (!pQ)
    {
        TRACE_TERSE0("TaskAsop: No Patchs defined. Exiting.");
        LINNO_RPRT(TaskAsop, -1);
        return;
    }

    //
    // Audio Stream Output Task Configuration (*pAsotCfg):
    //
    pAsotCfg = &gPAF_ASOT_config;       // initialize pointer to task configuration
    pAsotCfg->taskHandle = Task_self(); // set task handle
    pAstCfg = pAsotCfg->pAstCfg;        // get pointer to AST common (shared) configuration

    // Create ASOT event
    status = astCreateEvent(&gAsotEvtHandle);
    if (status < 0)
    {
        TRACE_TERSE0("TaskAsop: unable to initialize event. Exiting.");
        return;
    }
    
    /* Obtain Audio Stream Number (1, 2, etc.) */
    as = pAstCfg->as;
    TRACE_TERSE1("TaskAsop: Started with AS%d.", as);

    //
    // Initialize message log trace and line number reporting
    //
    for (z=STREAM1; z < STREAMN; z++)
    {
        TRACE_TERSE1("TaskAsop: AS%d: initiated", as+z);
    }
    LINNO_RPRT(TaskAsop, -1);

    //
    // Determine stream index
    //
    zMS = pAstCfg->masterStr;

    // Initialize as per parametrized phases:
    //
    //   In standard form these are:
    //      - Malloc: Memory Allocation
    //      - Config: Configuration Initialization
    //      - AcpAlg: ACP Algorithm Initialization and Local Attachment
    //      - Common: Common Memory Initialization
    //      - AlgKey: Dec/Enc chain to Array Initialization
    //      - Device: I/O Device Initialization
    //      - Unused: (available)
    //      - Unused: (available)
    //
    LINNO_RPRT(TaskAsop, -2);
    for (i=0; i < lengthof(pP->fxns->initPhase); i++)
    {
        Int linno;
        if (pP->fxns->initPhase[i])
        {
            linno = pP->fxns->initPhase[i](pP, pQ, pAsotCfg);
            if (linno)
            {
                LINNO_RPRT(TaskAsop, linno);
                return;
            }
        }
        else
        {
            TRACE_TERSE1("TaskAsop: AS%d: initialization phase - null", as+zMS);
        }
        TRACE_TERSE2("TaskAsop: AS%d: initialization phase - %d completed", as+zMS, i);
        LINNO_RPRT(TaskAsop, -i-3);
    }

    //
    // End of Initialization -- display memory usage report.
    //
    if (pP->fxns->memStatusPrint)
    {
        pP->fxns->memStatusPrint("ASOT MEMSTAT REPORT",
            HEAP_INTERNAL, HEAP_INTERNAL1, HEAP_EXTERNAL,
            HEAP_INTERNAL1_SHM, HEAP_EXTERNAL_SHM, HEAP_EXTERNAL_NONCACHED_SHM);
    }

    pOut = &pAsotCfg->pIoOut[zMS];
    pOut->hMcaspChan = NULL;
} /* taskAsopFxnInit */


// Create IO Buff & Phy framework components
static Int PAF_ASOT_ioCompCreate(
    PAF_AST_IoOut *pIoOut, 
    Int numOut, 
    IHeap_Handle iHeapHandle)
{
    int i, j, num_alloc;
    lib_mem_rec_t   *mem_rec;
    ioBuffHandle_t  ioBufHandle;
    ioPhyHandle_t   ioPhyHandle;
    Error_Block     eb;

    for(i=0; i<numOut; i++)
    {
        // Create an I/O BUFF instance
        // Obtain number of memory blocks required by I/O BUFF
        num_alloc = ioBuffNumAlloc();

        // Obtain requirements of each memory block
        mem_rec   = (lib_mem_rec_t *)malloc(sizeof(lib_mem_rec_t)*num_alloc);
        if(ioBuffAlloc(mem_rec) != IOBUFF_NOERR) {
            return __LINE__;
        }

        /* Allocate memory and create I/O BUFF instance */
        for(j=0; j<num_alloc; j++) {
            mem_rec[j].base = Memory_calloc(iHeapHandle, mem_rec[j].size,
                                            (1<<mem_rec[j].alignment), &eb);
        }

        if(ioBuffCreate(&ioBufHandle, mem_rec) != IOBUFF_NOERR) {
            return __LINE__;
        }

        pIoOut[i].hIoBuff = ioBufHandle;

        free(mem_rec);

        // Create an I/O PHY instance
        // Obtain number of memory blocks required by I/O PHY
        num_alloc = ioPhyNumAlloc();

        // Obtain requirements of each memory block
        mem_rec   = (lib_mem_rec_t *)malloc(sizeof(lib_mem_rec_t)*num_alloc);
        if(ioPhyAlloc(mem_rec) != IOBUFF_NOERR) {
            return __LINE__;
        }

        /* Allocate memory and create I/O PHY instance */
        for(j=0; j<num_alloc; j++) {
            mem_rec[j].base = Memory_calloc(iHeapHandle, mem_rec[j].size,
                                            (1<<mem_rec[j].alignment), &eb);
        }

        if(ioPhyCreate(&ioPhyHandle, mem_rec) != IOBUFF_NOERR) {
            return __LINE__;
        }

        pIoOut[i].hIoPhy = ioPhyHandle;
		
#if 1
#ifdef _TMS320C6X
    GateMP_Params gateParams;
    GateMP_Handle gateHandle;
    
    GateMP_Params_init(&gateParams);
    gateParams.localProtect = GateMP_LocalProtect_TASKLET;//GateMP_LocalProtect_THREAD;
    gateParams.remoteProtect = GateMP_RemoteProtect_SYSTEM;
    gateParams.name = ASP_STREAM_OUT_GATE_NAME;
    gateParams.regionId = ASP_STREAM_OUT_GATE_REGION_ID;
    gateHandle = GateMP_create(&gateParams);
    if (gateHandle != NULL)
    {
        pIoOut[i].gateHandle = gateHandle;
    }
    else
    {
        pIoOut[i].gateHandle = NULL;
        return ASP_STREAM_OUT_INV_GATE;
    }

#elif defined(ARMCOMPILE)
    GateMP_Handle gateHandle;
    Int status;
    
    do {
        status = GateMP_open(ASP_STREAM_OUT_GATE_NAME, &gateHandle);
    } while (status == GateMP_E_NOTFOUND);
    if (status == GateMP_S_SUCCESS)
    {
        pIoOut[i].gateHandle = gateHandle;
    }
    else
    {
        pIoOut[i].gateHandle = NULL;
        return ASP_STREAM_OUT_INV_GATE;
    }

#else
    #error "Unsupported platform"

#endif  
#endif
        free(mem_rec);

    } // end of for loop

    return 0;
} // PAF_ASOT_ioCompCreate

// -----------------------------------------------------------------------------
// AST Initialization Function - Memory Allocation
//
//   Name:      PAF_ASOT_initPhaseMalloc
//   Purpose:   Audio Stream Output Task Function for initialization of data pointers
//              by allocation of memory.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on MEM_calloc failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//

Int PAF_ASOT_initPhaseMalloc(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int zMS, errLineNum;
    Error_Block    eb;
    //Int i;

    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    zMS = pAstCfg->masterStr;

    TRACE_TERSE1("PAF_ASOT_initPhaseMalloc: AS%d: initialization phase - memory allocation", as+zMS);

    // Initialize error block
    Error_init(&eb); 

    /* Stream memory */
    if (!(pAstCfg->xStr = Memory_calloc((IHeap_Handle)HEAP_INTERNAL, STREAMN * sizeof (*pAstCfg->xStr), 4, &eb)))
    {
        TRACE_TERSE1("PAF_ASOT_initPhaseMalloc: AS%d: Memory_calloc failed", as+zMS);
        SW_BREAKPOINT;
        return __LINE__;
    }
    TRACE_TERSE3("PAF_ASOT_initPhaseMalloc. (pAstCfg->xStr) %d bytes from space %d at 0x%x.",
            STREAMN * sizeof (*pAstCfg->xStr),
            HEAP_ID_INTERNAL, (IArg)pAstCfg->xStr);

    {
        Int z;                          /* stream counter */

        PAF_AudioFrame *fBuf;

        if (!(fBuf = (PAF_AudioFrame *)Memory_calloc((IHeap_Handle)HEAP_INTERNAL, STREAMS * sizeof (*fBuf), 4, &eb)))
        {
            TRACE_TERSE1("PAF_ASOT_initPhaseMalloc: AS%d: Memory_calloc failed", as+zMS);
            SW_BREAKPOINT;
            return __LINE__;
        }
        TRACE_TERSE3("PAF_ASOT_initPhaseMalloc. (fBuf) %d bytes from space %d at 0x%x.",
                STREAMS * sizeof (*fBuf),
                HEAP_ID_INTERNAL, (IArg)fBuf);

        for (z=STREAM1; z < STREAMN; z++)
        {
            pAstCfg->xStr[z].pAudioFrame = &fBuf[z-STREAM1];
            TRACE_TERSE2("pAstCfg->xStr[%d].pAudioFrame = 0x%x", z, (IArg)pAstCfg->xStr[z].pAudioFrame);
        }
    }

    /* Encode memory */
    if (!(pAstCfg->xEnc = Memory_calloc((IHeap_Handle)HEAP_INTERNAL, ENCODEN * sizeof (*pAstCfg->xEnc), 4, &eb)))
    {
        TRACE_TERSE1("PAF_ASOT_initPhaseMalloc: AS%d: Memory_calloc failed", as+zMS);
        SW_BREAKPOINT;
        return __LINE__;
    }
    TRACE_TERSE3("PAF_ASOT_initPhaseMalloc. (pAstCfg->xEnc) %d bytes from space %d at 0x%x.",
            ENCODEN * sizeof (*pAstCfg->xEnc),
            HEAP_ID_INTERNAL, (IArg)pAstCfg->xEnc);

    /* Output memory */
    if (!(pAstCfg->xOut = Memory_calloc((IHeap_Handle)HEAP_INTERNAL, OUTPUTN * sizeof (*pAstCfg->xOut), 4, &eb)))
    {
        TRACE_TERSE1("PAF_ASOT_initPhaseMalloc: AS%d: Memory_calloc failed", as+zMS);
        SW_BREAKPOINT;
        return __LINE__;
    }
    TRACE_TERSE3("PAF_ASOT_initPhaseMalloc. (pAstCfg->xOut) %d bytes from space %d at 0x%x.",
            OUTPUTN * sizeof (*pAstCfg->xOut),
            HEAP_ID_INTERNAL, (IArg)pAstCfg->xOut);

    /* Output I/O data structure memory */
    if (!(pAsotCfg->pIoOut = Memory_calloc((IHeap_Handle)HEAP_INTERNAL, OUTPUTN * sizeof (*pAsotCfg->pIoOut), 4, &eb)))
    {
        TRACE_TERSE1("PAF_ASOT_initPhaseMalloc: AS%d: Memory_calloc failed", as+zMS);
        SW_BREAKPOINT;
        return __LINE__;
    }
    TRACE_TERSE3("PAF_ASOT_initPhaseMalloc. (pAsotCfg->pIoOut) %d bytes from space %d at 0x%x.",
                 OUTPUTN * sizeof (*pAsotCfg->pIoOut),
                 HEAP_ID_INTERNAL, (IArg)pAsotCfg->pIoOut);

    /* I/O components memory for output */
    errLineNum = PAF_ASOT_ioCompCreate(pAsotCfg->pIoOut, OUTPUTN, (IHeap_Handle)HEAP_INTERNAL);
    if(errLineNum)
    {
        SW_BREAKPOINT;
        return errLineNum;
    }
    TRACE_TERSE1("PAF_ASOT_initPhaseMalloc: AS%d: initialization phase - memory allocation complete.", as+zMS);
    return 0;
} //PAF_ASOT_initPhaseMalloc

// -----------------------------------------------------------------------------
// ASOT Initialization Function - Memory Initialization from Configuration
//
//   Name:      PAF_ASOT_initPhaseConfig
//   Purpose:   Audio Stream Output Task Function for initialization of data values
//              from parameters.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Other as per initFrame0 and initFrame1.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
Int PAF_ASOT_initPhaseConfig(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* input/encode/stream/decode/output counter */
    Int zMS;

    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    zMS = pAstCfg->masterStr;

    TRACE_TERSE1("PAF_ASOT_initPhaseConfig: AS%d: initialization phase - configuration", as+zMS);

    //
    // Unspecified elements have been initialized to zero during alloc
    //

    for (z=STREAM1; z < STREAMN; z++) 
    {
        Int linno;
        if (linno = pP->fxns->initFrame0(pP, pQ, pAsotCfg, z))
        {
            return linno;           
        }
        if (linno = pP->fxns->initFrame1(pP, pQ, pAsotCfg, z, -1))
        {
            return linno;
        }
    }

    for (z=ENCODE1; z < ENCODEN; z++) 
    {
        Int zO = pP->outputsFromEncodes[z];
        Int zS = pP->streamsFromEncodes[z];
        pAstCfg->xEnc[z].encodeControl.size = sizeof(pAstCfg->xEnc[z].encodeControl);
        pAstCfg->xEnc[z].encodeControl.pAudioFrame = pAstCfg->xStr[zS].pAudioFrame;
        pAstCfg->xEnc[z].encodeControl.pVolumeStatus = &pAstCfg->xEnc[z].volumeStatus;
        pAstCfg->xEnc[z].encodeControl.pOutBufConfig = &pAstCfg->xOut[zO].outBufConfig;
        pAstCfg->xEnc[z].encodeStatus = *pP->z_pEncodeStatus[z];
        pAstCfg->xEnc[z].encodeControl.encActive = pAstCfg->xEnc[z].encodeStatus.select;
        pAstCfg->xEnc[z].volumeStatus = *pP->pVolumeStatus;
        pAstCfg->xEnc[z].encodeInStruct.pAudioFrame = pAstCfg->xStr[zS].pAudioFrame;
    }

    for (z=OUTPUT1; z < OUTPUTN; z++)
    {
        pAstCfg->xOut[z].outBufStatus = *pP->pOutBufStatus;
    }

    TRACE_TERSE1("PAF_ASOT_initPhaseConfig: AS%d: initialization phase - configuration complete.", as+zMS);
    return 0;
} //PAF_ASOT_initPhaseConfig

// -----------------------------------------------------------------------------
// ASOT Initialization Function - ACP Algorithm Instantiation
//
//   Name:      PAF_ASOT_initPhaseAcpAlg
//   Purpose:   Audio Stream Input Task Function for initialization of ACP by
//              instantiation of the algorithm.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on ACP Algorithm creation failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//
Int PAF_ASOT_initPhaseAcpAlg(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* input/encode/stream/decode/output counter */
    Int betaPrimeOffset;
    ACP_Handle acp;
    Int zMS;
    Int zS, zX;

    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    zMS = pAstCfg->masterStr;

    TRACE_TERSE1("PAF_ASOT_initPhaseAcpAlg: AS%d: initialization phase - ACP Algorithm", as+zMS);

    ACP_MDS_init();

    if (!(acp = (ACP_Handle )ACP_MDS_create(NULL))) 
    {
        TRACE_TERSE1("PAF_ASOT_initPhaseAcpAlg: AS%d: ACP algorithm instance creation  failed", as+zMS);
        return __LINE__;
    }
    pAsotCfg->acp = acp;

    ((ALG_Handle)acp)->fxns->algControl((ALG_Handle) acp,
        ACP_GETBETAPRIMEOFFSET, (IALG_Status *)&betaPrimeOffset);

    for (z=ENCODE1; z < ENCODEN; z++) 
    {
        zS = pP->streamsFromEncodes[z];
        acp->fxns->attach(acp, ACP_SERIES_STD,
            STD_BETA_ENCODE + betaPrimeOffset * (as-1+zS),
            (IALG_Status *)&pAstCfg->xEnc[z].encodeStatus);
        acp->fxns->attach(acp, ACP_SERIES_STD,
            STD_BETA_VOLUME + betaPrimeOffset * (as-1+zS),
            (IALG_Status *)&pAstCfg->xEnc[z].volumeStatus);
        /* Ignore errors, not reported. */
    }

    for (z=OUTPUT1; z < OUTPUTN; z++) 
    {
        zS = z;
        for (zX = ENCODE1; zX < ENCODEN; zX++) 
        {
            if (pP->outputsFromEncodes[zX] == z) 
            {
                zS = pP->streamsFromEncodes[zX];
                break;
            }
        }
        acp->fxns->attach(acp, ACP_SERIES_STD,
            STD_BETA_OB + betaPrimeOffset * (as-1+zS),
            (IALG_Status *)&pAstCfg->xOut[z].outBufStatus);
        /* Ignore errors, not reported. */
    }

    TRACE_TERSE1("PAF_ASOT_initPhaseAcpAlg: AS%d: initialization phase - ACP Algorithm complete.", as+zMS);

    return 0;
} //PAF_ASOT_initPhaseAcpAlg

// -----------------------------------------------------------------------------
// ASOT Initialization Function - Common Memory
//
//   Name:      PAF_ASOT_initPhaseCommon
//   Purpose:   Audio Stream Output Task Function for allocation of common memory.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on PAF_ALG_alloc failure.
//              Source code line number on PAF_ALG_mallocMemory failure.
//              Source code line number on Decode Chain initialization failure.
//              Source code line number on ASP Chain initialization failure.
//              Source code line number on Encode Chain initialization failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//
Int PAF_ASOT_initPhaseCommon(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* stream counter */
    Int g;                      /* gear */
    ACP_Handle acp;
    PAF_IALG_Config pafAlgConfig;
    IALG_MemRec common[3][PAF_IALG_COMMON_MEMN+1];
   
    acp = pAsotCfg->acp;
    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;

    TRACE_TERSE0("PAF_ASOT_initPhaseCommon: initialization phase - Common Memory");

    //
    // Determine memory needs and instantiate algorithms across audio streams
    //
    TRACE_TERSE0("PAF_ASOT_initPhaseCommon: calling PAF_ALG_setup.");
    PAF_ALG_setup(&pafAlgConfig, 
        HEAP_ID_INTERNAL,                 HEAP_INTERNAL, 
        HEAP_ID_INTERNAL1,                HEAP_INTERNAL1, 
        HEAP_ID_EXTERNAL,                 HEAP_EXTERNAL, 
        HEAP_ID_INTERNAL1_SHM,            HEAP_INTERNAL1_SHM, 
        HEAP_ID_EXTERNAL_SHM,             HEAP_EXTERNAL_SHM, 
        HEAP_ID_EXTERNAL_NONCACHED_SHM,   HEAP_EXTERNAL_NONCACHED_SHM,
        HEAP_CLEAR);

    if (pP->fxns->headerPrint)
    {
        pP->fxns->headerPrint();        
    }

    for (z = STREAM1; z < STREAMN; z++) 
    {
        //Int zD, zE, zX;
        Int zE, zX;

        TRACE_TERSE1("PAF_ASOT_initPhaseCommon: AS%d: initialization phase - Common Memory", as+z);

        //
        // Determine common memory needs for:
        //  (1) ASP Algorithms
        //  (2) Encode Algorithms
        //  (3) Logical Output drivers
        //
        PAF_ALG_init(common[z], lengthof(common[z]), COMMONSPACE);

        zE = -1;
        for (zX = ENCODE1; zX < ENCODEN; zX++) 
        {
            if (pP->streamsFromEncodes[zX] == z) 
            {
                zE = zX;
                break;
            }
        }

        TRACE_TERSE1("Calling PAF_ALG_ALLOC for stream common[%d].", z);
        if (PAF_ALG_ALLOC(aspLinkInit[z-STREAM1][0], common[z])) 
        {
            TRACE_TERSE1("PAF_ASOT_initPhaseCommon: AS%d: PAF_ALG_alloc failed", as+z);
            TRACE_TERSE2("Failed to alloc %d bytes from space %d ", common[z]->size, common[z]->space);
            SW_BREAKPOINT;
            return __LINE__;
        }
        TRACE_TERSE3("alloced %d bytes from space %d at 0x%x", common[z]->size, common[z]->space, (IArg)common[z]->base);
        if (pP->fxns->allocPrint)
        {
            pP->fxns->allocPrint((const PAF_ALG_AllocInit *)(aspLinkInit[z-STREAM1][0]), sizeof (*(aspLinkInit[z-STREAM1][0])), &pafAlgConfig);
        }

        if (zE >= 0) 
        {
            TRACE_TERSE1("PAF_ASOT_initPhaseCommon: calling PAF_ALG_ALLOC/ for encoder common[%d].", z);
            if (PAF_ALG_ALLOC(encLinkInit[zE-ENCODE1], common[z])) 
            {
                TRACE_TERSE1("PAF_ASOT_initPhaseCommon: AS%d: PAF_ALG_alloc failed", as+z);
                SW_BREAKPOINT;
                return __LINE__;
            }
            TRACE_TERSE3("alloced %d bytes from space %d at 0x%x", common[z]->size, common[z]->space, (IArg)common[z]->base);
            if (pP->fxns->allocPrint)
            {
                pP->fxns->allocPrint((const PAF_ALG_AllocInit *)(encLinkInit[z-ENCODE1]), sizeof (*(encLinkInit[z-ENCODE1])), &pafAlgConfig);
            }
        }

        //
        // Determine common memory needs of Logical IO drivers
        //

        if (OUTPUT1 <= z && z < OUTPUTN)
        {
            TRACE_TERSE1("PAF_ASOT_initPhaseCommon: calling PAF_ALG_ALLOC outLinkInit common[%d].", z);
            if (PAF_ALG_ALLOC(outLinkInit[z-OUTPUT1], common[z]))
            {
                TRACE_TERSE1("PAF_ASOT_initPhaseMalloc: AS%d: PAF_ALG_alloc failed", as+z);
                TRACE_TERSE2("Failed to alloc %d bytes from space %d", common[z]->size, (IArg)common[z]->space);
                SW_BREAKPOINT;
                return __LINE__;
            }
            TRACE_TERSE3("alloced %d bytes from space %d at 0x%x", common[z]->size, common[z]->space, (IArg)common[z]->base);
            if (pP->fxns->allocPrint)
            {
                pP->fxns->allocPrint((const PAF_ALG_AllocInit *)(outLinkInit[z-INPUT1]), sizeof (*(outLinkInit[z-INPUT1])), &pafAlgConfig);
            }
        }
    }
    {
        // Changes made to share scratch between zones
        // Assume maximum 3 zones and scratch common memory is at offset 0;
        int max=0;
        for (z=STREAM1; z<STREAMN; z++)
        {
            if (max < common[z][0].size)
            {
                max = common[z][0].size;
            }
        }
        common[STREAM1][0].size=max;
        for (z=STREAM1+1; z<STREAMN; z++)
        {
            common[z][0].size = 0;            
        }
    }
        
    //
    // Allocate common memory for:
    //  (1) ASP Algorithms
    //  (2) Encode Algorithms
    //  (3) Logical Output drivers
    //
    for (z = STREAM1; z < STREAMN; z++) 
    {
        //Int zD, zE, zX;
        Int zE, zX;

        TRACE_TERSE0("PAF_ASOT_initPhaseCommon: calling PAF_ALG_mallocMemory for common space.");
        if (PAF_ALG_mallocMemory(common[z], &pafAlgConfig)) 
        {
            TRACE_TERSE1("AS%d: PAF_ALG_mallocMemory failed", as+z);
            TRACE_TERSE3("AS%d: z: %d.  Size 0x%x", as+z, z, common[z][0].size);
            SW_BREAKPOINT;
            return __LINE__;
        }
        TRACE_TERSE3("alloced %d bytes from space %d at 0x%x", common[z]->size, common[z]->space, (IArg)common[z]->base);
        // share zone0 scratch with all zones 
        common[z][0].base = common[0][0].base;
        if (pP->fxns->commonPrint)
        {
            pP->fxns->commonPrint(common[z], &pafAlgConfig);
        }

        zE = -1;
        for (zX = ENCODE1; zX < ENCODEN; zX++) 
        {
            if (pP->streamsFromEncodes[zX] == z) 
            {
                zE = zX;
                break;
            }
        }

        pAstCfg->xStr[z].aspChain[0] = NULL;
        for (g=0; g < GEARS; g++) 
        {
            PAF_ASP_Chain *chain;
            TRACE_TERSE0("PAF_ASOT_initPhaseCommon: calling PAF_ASP_chainInit for ASPs.");
            chain = PAF_ASP_chainInit(&pAstCfg->xStr[z].aspChainData[g], pP->pChainFxns,
                HEAP_INTERNAL, as+z, acp, &trace,
                aspLinkInit[z-STREAM1][g], pAstCfg->xStr[z].aspChain[0], common[z], &pafAlgConfig);
            if (!chain) 
            {
                TRACE_TERSE2("AS%d: ASP chain %d initialization failed", as+z, g);
                return __LINE__;
            }
            else
            {
                pAstCfg->xStr[z].aspChain[g] = chain;
            }
        }

        if (zE >= 0) 
        {
            PAF_ASP_Chain *chain;
            TRACE_TERSE0("PAF_ASOT_initPhaseCommon: calling PAF_ASP_chainInit for encode.");
            chain = PAF_ASP_chainInit(&pAstCfg->xEnc[zE].encChainData, pP->pChainFxns,
                HEAP_INTERNAL, as+z, acp, &trace,
                encLinkInit[zE-ENCODE1], NULL, common[z], &pafAlgConfig);
            if (!chain) 
            {
                TRACE_TERSE1("AS%d: Encode chain initialization failed", as+z);
                return __LINE__;
            }
        }

        //
        // Allocate non-common memories for Logical IO drivers
        //    Since these structures are used at run-time we allocate from external memory
        if (OUTPUT1 <= z && z < OUTPUTN) 
        {
            PAF_ASP_Chain *chain;
            TRACE_TERSE2("PAF_ASOT_initPhaseMalloc: AS%d: non-common output chain init for %d",
                           as+z, z);
            chain = PAF_ASP_chainInit (&pAstCfg->xOut[z].outChainData, pP->pChainFxns,
                        HEAP_EXTERNAL, as+z, acp, &trace,
                        outLinkInit[z-OUTPUT1], NULL, common[z], &pafAlgConfig);
            if (!chain) 
            {
                TRACE_TERSE1("AS%d: Output chain initialization failed", as+z);
                return __LINE__;
            }
        }
    }
    TRACE_TERSE1("AS%d: PAF_ASOT_initPhaseCommon: Returning complete.", as+z);

    return 0;
} //PAF_ASOT_initPhaseCommon

// -----------------------------------------------------------------------------
// ASOT Initialization Function - Algorithm Keys
//
//   Name:      PAF_ASOT_initPhaseAlgKey
//   Purpose:   Audio Stream Output Task Function for initialization of data values
//              from parameters for Algorithm Keys.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
// .............................................................................
Int PAF_ASOT_initPhaseAlgKey(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* decode/encode counter */
    Int s;                      /* key number */
    PAF_ASP_Link *that;

    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    (void)as; // clear compiler warning in case not used with tracing disabled

    TRACE_VERBOSE1("PAF_ASOT_initPhaseAlgKey: AS%d: initialization phase - Algorithm Keys", as);

    for (z=ENCODE1; z < ENCODEN; z++) 
    {
        for (s=0; s < pP->pEncAlgKey->length; s++) 
        {
            if ((pP->pEncAlgKey->code[s].full != 0) && 
                (that = PAF_ASP_chainFind(&pAstCfg->xEnc[z].encChainData, pP->pEncAlgKey->code[s])))
            {
                pAstCfg->xEnc[z].encAlg[s] = (ALG_Handle )that->alg;
            }
            /* Cast in interface, for now --Kurt */
            else
            {
                pAstCfg->xEnc[z].encAlg[s] = NULL;                
            }
        }
    }

    return 0;
} //PAF_ASOT_initPhaseAlgKey

// -----------------------------------------------------------------------------
// ASOT Initialization Function - I/O Devices
//
//   Name:      PAF_ASOT_initPhaseDevice
//   Purpose:   Audio Stream Output Task Function for initialization of I/O Devices.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on device allocation failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//
Int PAF_ASOT_initPhaseDevice(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* input/output counter */
    PAF_SIO_IALG_Obj    *pObj;
    PAF_SIO_IALG_Config *pAlgConfig;
    PAF_IALG_Config pafAlgConfig;

    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    (void)as; // clear compiler warning in case not used with tracing disabled

    TRACE_TERSE1("PAF_ASOT_initPhaseDevice: AS%d: initialization phase - I/O Devices", as);

    if(pP->fxns->bufMemPrint)
    {
        PAF_ALG_setup (&pafAlgConfig, 
            HEAP_ID_INTERNAL,               HEAP_INTERNAL, 
            HEAP_ID_INTERNAL1,              HEAP_INTERNAL1,
            HEAP_ID_EXTERNAL,               HEAP_EXTERNAL,
            HEAP_ID_INTERNAL1_SHM,          HEAP_INTERNAL1_SHM,
            HEAP_ID_EXTERNAL_SHM,           HEAP_EXTERNAL_SHM,
            HEAP_ID_EXTERNAL_NONCACHED_SHM, HEAP_EXTERNAL_NONCACHED_SHM,
            HEAP_CLEAR);
        TRACE_TERSE2("PAF_ASOT_initPhaseDevice: AS%d: calling PAF_ALG_setup with clear at %d.", as, HEAP_CLEAR);
    }
    
    for (z=OUTPUT1; z < OUTPUTN; z++) 
    {
        PAF_OutBufConfig *pConfig = &pAstCfg->xOut[z].outBufConfig;

        pObj = (PAF_SIO_IALG_Obj *)pAstCfg->xOut[z].outChainData.head->alg;
        pAlgConfig = &pObj->config;

        pAstCfg->xOut[z].hTxSio = NULL;
        pConfig->base.pVoid     = pAlgConfig->pMemRec[0].base;
        pConfig->pntr.pVoid     = pAlgConfig->pMemRec[0].base;
        pConfig->head.pVoid     = pAlgConfig->pMemRec[0].base;
        pConfig->allocation     = pAlgConfig->pMemRec[0].size;
        pConfig->sizeofElement  = 3;
        pConfig->precision      = 24;
        if(pP->fxns->bufMemPrint)
        {
            pP->fxns->bufMemPrint(z,pAlgConfig->pMemRec[0].size,PAF_ALG_memSpaceToHeapId(&pafAlgConfig,pAlgConfig->pMemRec[0].space),1);
        }
    }
    TRACE_TERSE1("PAF_ASOT_initPhaseDevice: AS%d: initialization phase - I/O Devices complete.", as);

    return 0;
} //PAF_ASOT_initPhaseDevice

// -----------------------------------------------------------------------------
// ASOT Initialization Function Helper - Initialization of Audio Frame
//
//   Name:      PAF_ASOT_initFrame0
//   Purpose:   Audio Stream Output Task Function for initialization of the Audio
//              Frame(s) by memory allocation and loading of data pointers
//              and values.
//   From:      AST Parameter Function -> decodeInfo
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on MEM_calloc failure.
//              Source code line number on unsupported option.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * Memory allocation errors.
//              * Unsupported option errors.
//

// MID 314
extern const char AFChanPtrMap[PAF_MAXNUMCHAN+1][PAF_MAXNUMCHAN];
extern PAF_ChannelConfigurationMaskTable PAF_ASP_stdCCMT_patch;

Int PAF_ASOT_initFrame0(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int z
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int ch;
    //Int aLen;
    Int aLen_int=0,aLen_ext=0;
    Int aSize = sizeof(PAF_AudioData);
    Int aAlign = aSize < sizeof (int) ? sizeof (int) : aSize;
    Int maxFrameLength = pP->maxFramelength;
    Int zX;
    PAF_AudioData *aBuf_int=NULL;
    PAF_AudioData *aBuf_ext=NULL;
    XDAS_UInt8 *metadataBuf;
    char i;
    Error_Block    eb;

    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;

    // Initialize error block
    Error_init(&eb); 

    // Compute maximum framelength (needed for ARC support)
    maxFrameLength += PA_MODULO - maxFrameLength % PA_MODULO;
    //aLen = numchan[z] * maxFrameLength;
    for (i=0; i < numchan[z]; i++)
    {
        if (pP->pAudioFrameBufStatus->space[i] == IALG_SARAM)
        {
            aLen_int += maxFrameLength;
        }
        else
        {
            aLen_ext += maxFrameLength;
        }
    }

    //
    // Initialize audio frame elements directly
    //
    pAstCfg->xStr[z].pAudioFrame->fxns = pP->pAudioFrameFunctions;
    pAstCfg->xStr[z].pAudioFrame->data.nChannels = PAF_MAXNUMCHAN; ///
///    pAstCfg->xStr[z].pAudioFrame->data.nChannels = PAF_MAXNUMCHAN_AF;
    pAstCfg->xStr[z].pAudioFrame->data.nSamples = FRAMELENGTH;
    pAstCfg->xStr[z].pAudioFrame->data.sample = pAstCfg->xStr[z].audioFrameChannelPointers;
    pAstCfg->xStr[z].pAudioFrame->data.samsiz = pAstCfg->xStr[z].audioFrameChannelSizes;
    pAstCfg->xStr[z].pAudioFrame->pChannelConfigurationMaskTable = &PAF_ASP_stdCCMT;

    //
    // Allocate memory for and initialize pointers to audio data buffers
    //
    //   The NUMCHANMASK is used to identify the channels for which data
    //   buffers can be allocated. Using this mask and switch statement
    //   rather than some other construct allows efficient code generation,
    //   providing just the code necessary (with significant savings).
    //
    if (pP->fxns->bufMemPrint)
    {
        pP->fxns->bufMemPrint(z, aLen_int*aSize, HEAP_ID_FRMBUF, 2);
        pP->fxns->bufMemPrint(z, aLen_ext*aSize, HEAP_ID_EXTERNAL, 2);
    }

    TRACE_TERSE1("PAF_ASOT_initFrame0: AS%d: Memory_calloc for audio buffers", as+z);
    
    if (aLen_int*aSize!=0) // check size != 0, otherwise malloc throws fatal error
    {
        if (!(aBuf_int = (PAF_AudioData *)Memory_calloc((IHeap_Handle)HEAP_FRMBUF, (aLen_int+(maxFrameLength-FRAMELENGTH))*aSize, aAlign, &eb))) //Qin: Add start offset
        {
            TRACE_TERSE1("PAF_ASOT_initFrame0: AS%d: Memory_calloc failed", as+z);
            TRACE_TERSE2("  maxFrameLength: %d.  aLen_int*aSize: %d", maxFrameLength, (aLen_int+(maxFrameLength-FRAMELENGTH))*aSize);
            SW_BREAKPOINT;
            return __LINE__;
        }
    }
        
    if (aLen_ext*aSize!=0)
    {
        if (!(aBuf_ext = (PAF_AudioData *)Memory_calloc((IHeap_Handle)HEAP_EXTERNAL, (aLen_ext+(maxFrameLength-FRAMELENGTH))*aSize, aAlign, &eb)))//Qin: Add start offset
        {
            TRACE_TERSE1("PAF_ASOT_initFrame0: AS%d: Memory_calloc failed", as+z);
            TRACE_TERSE2("  maxFrameLength: %d.  aLen_ext*aSize: %d", maxFrameLength, (aLen_ext+(maxFrameLength-FRAMELENGTH))*aSize);
            SW_BREAKPOINT;
            return __LINE__;
        }
    }
    
    TRACE_TERSE3("  maxFrameLength: %d.  aLen_int*aSize: %d.  aBuf_int: 0x%x", maxFrameLength, (aLen_int+(maxFrameLength-FRAMELENGTH))*aSize, (IArg)aBuf_int);
    TRACE_TERSE3("  maxFrameLength: %d.  aLen_ext*aSize: %d.  aBuf_ext: 0x%x", maxFrameLength, (aLen_ext+(maxFrameLength-FRAMELENGTH))*aSize, (IArg)aBuf_ext);

    TRACE_TERSE1("PAF_ASOT_initFrame0: AS%d: Memory_calloc for metadata buffers", as+z);
    if (!(metadataBuf = (XDAS_UInt8 *)Memory_calloc((IHeap_Handle)HEAP_MDBUF, pP->pMetadataBufStatus->bufSize*pP->pMetadataBufStatus->NumBuf, pP->pMetadataBufStatus->alignment, &eb)))
    {
        TRACE_TERSE1("PAF_ASOT_initFrame0: AS%d: Memory_calloc failed", as+z);
        TRACE_TERSE1("  bufSize*NumBuf: %d", pP->pMetadataBufStatus->bufSize*pP->pMetadataBufStatus->NumBuf);
        SW_BREAKPOINT;
        return __LINE__;
    }

    {
        Int i;

#pragma UNROLL(1)
        for (i=0; i < PAF_MAXNUMCHAN_AF; i++)
        {
            pAstCfg->xStr[z].audioFrameChannelPointers[i] = NULL;
        }
    }

    // MID 314
    if((numchan[z] > PAF_MAXNUMCHAN) || (numchan[z] < 1)) 
    {
        TRACE_TERSE1("PAF_ASOT_initFrame0: AS%d: unsupported option", as+z);
        return __LINE__;
    }
    else 
    {
        Int j = 0;
        Int k = 0;
        TRACE_TERSE1("PAF_ASOT_initFrame0: AFChanPtrMap[%d][i]", numchan[z]);
        for(i=0;i<numchan[z];i++)
        {
            char chan = AFChanPtrMap[numchan[z]][i];
            if(chan != -1)
            {
                if(pP->pAudioFrameBufStatus->space[i] == IALG_SARAM)
                {
                    pAstCfg->xStr[z].audioFrameChannelPointers[chan] = aBuf_int + maxFrameLength*(j+1) - FRAMELENGTH;
                    j++;
                }
                else
                {        
                    pAstCfg->xStr[z].audioFrameChannelPointers[chan] = aBuf_ext + maxFrameLength*(k+1) - FRAMELENGTH;
                    k++;
                }    
                TRACE_TERSE3("PAF_ASOT_initFrame0: chan = %d = AFChanPtrMap[%d][%d].", chan, numchan[z], i);
                TRACE_TERSE2("PAF_ASOT_initFrame0: audioFrameChannelPointers[%d]: 0x%x", chan, (IArg)pAstCfg->xStr[z].audioFrameChannelPointers[chan]);
            }
        }
    }

    for (ch=PAF_LEFT; ch < PAF_MAXNUMCHAN_AF; ch++) 
    {
        if (pAstCfg->xStr[z].audioFrameChannelPointers[ch])
        {
            pAstCfg->xStr[z].origAudioFrameChannelPointers[ch] = pAstCfg->xStr[z].audioFrameChannelPointers[ch];
        }
    }

    //
    // Initialize meta data elements
    //
    pAstCfg->xStr[z].pAudioFrame->pafBsMetadataUpdate = XDAS_FALSE;
    pAstCfg->xStr[z].pAudioFrame->numPrivateMetadata = 0;
    pAstCfg->xStr[z].pAudioFrame->bsMetadata_offset = 0;
    pAstCfg->xStr[z].pAudioFrame->bsMetadata_type = PAF_bsMetadata_channelData;
    pAstCfg->xStr[z].pAudioFrame->privateMetadataBufSize = pP->pMetadataBufStatus->bufSize;
    for(i=0;i<pP->pMetadataBufStatus->NumBuf;i++)
    {
        pAstCfg->xStr[z].pAudioFrame->pafPrivateMetadata[i].offset = 0;
        pAstCfg->xStr[z].pAudioFrame->pafPrivateMetadata[i].size = 0;
        pAstCfg->xStr[z].pAudioFrame->pafPrivateMetadata[i].pMdBuf = metadataBuf + pP->pMetadataBufStatus->bufSize*i;
    }

    //
    // Initialize decoder elements directly
    //

    for (zX = DECODE1; zX < DECODEN; zX++) 
    {
        if (pP->streamsFromDecodes[zX] == z) 
        {
#ifdef NOAUDIOSHARE
            pAstCfg->xDec[zX].decodeInStruct.audioShare.nSamples = 0;
            pAstCfg->xDec[zX].decodeInStruct.audioShare.sample = NULL;
#else /* NOAUDIOSHARE */
            pAstCfg->xDec[zX].decodeInStruct.audioShare.nSamples = aLen_int;
            pAstCfg->xDec[zX].decodeInStruct.audioShare.sample = aBuf_int;
#endif /* NOAUDIOSHARE */
        }
    }

    return 0;
} //PAF_ASOT_initFrame0

// -----------------------------------------------------------------------------
// ASOT Initialization Function Helper - Reinitialization of Audio Frame
// AST Decoding Function              - Reinitialization of Audio Frame
//
//   Name:      PAF_ASOT_initFrame1
//   Purpose:   Audio Stream Task Function for initialization or reinitiali-
//              zation of the Audio Frame(s) by loading of data values of a
//              time-varying nature.
//   From:      audioStream1Task or equivalent
//              AST Parameter Function -> decodeInfo
//              AST Parameter Function -> decodeDecode
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     None.
//
Int PAF_ASOT_initFrame1(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int z, 
    Int apply
)
{
    PAF_AST_Config *pAstCfg;

    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration

    //
    // Reinitialize audio frame elements:
    //
    //   Channel Configurations during sys init                 = Unknown
    //      "          "        during info or decode           = None
    //
    //   Sample Rate / Count    during sys init, info or decode = Unknown / 0
    //

    if (apply < 0) 
    {
        pAstCfg->xStr[z].pAudioFrame->channelConfigurationRequest.legacy = PAF_CC_UNKNOWN;
        pAstCfg->xStr[z].pAudioFrame->channelConfigurationStream.legacy = PAF_CC_UNKNOWN;
    }
    else 
    {
        pAstCfg->xStr[z].pAudioFrame->channelConfigurationRequest.legacy = PAF_CC_NONE;
        pAstCfg->xStr[z].pAudioFrame->channelConfigurationStream.legacy = PAF_CC_NONE;
    }

    if (apply < 1) 
    {
        pAstCfg->xStr[z].pAudioFrame->sampleRate = PAF_SAMPLERATE_UNKNOWN;
        pAstCfg->xStr[z].pAudioFrame->sampleCount = 0;
    }

    return 0;
} //PAF_ASOT_initFrame1
