
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamProc_master.c ========
 */

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <ti/sysbios/knl/Event.h>
#include "audioStreamProc_common.h"
#include "audioStreamProc_master.h"

// ASPM configuration-- ASIT/ASOT shared
#pragma DATA_SECTION(gPAF_ASPM_config, ".globalSectionPafAspmConfig")
PAF_ASPM_Config gPAF_ASPM_config = {
    { NULL, 0, NULL }, // gateHandle, numCb, xDecOpCb
    { NULL, NULL }, // gateHandle, xOutIsi
};

// Create AST event object
Int astCreateEvent(
    Event_Handle *pEventHandle
)
{
    Event_Handle eventHandle;
    Error_Block eb;
    Int status;
    
    Error_init(&eb);
    
    // Create event,
    // using default instance configuration parameters.
    eventHandle = Event_create(NULL, &eb);
    if (eventHandle == NULL)
    {
        status = -1;
    }
    else
    {
        status = 0;
    }
    
    *pEventHandle = eventHandle;
        
    return status;
}

// Create AST sync event object
Int astCreateSyncEvent(
    Event_Handle eventHandle,
    UInt eventId, 
    SyncEvent_Handle *pSyncEventHandle
)
{
    Error_Block eb;
    SyncEvent_Params params;
    SyncEvent_Handle syncEventHandle;
    Int status;
 
    Error_init(&eb);

    // Create sync event
    SyncEvent_Params_init(&params);    
    params.event = eventHandle; // initialize synchronizer event
    params.eventId = eventId;   // initialize synchronizer event Id
    
    syncEventHandle = SyncEvent_create(&params, &eb);
    if (syncEventHandle == NULL)
    {
        status = -1;
    }
    else
    {
        status = 0;
    }
    
    *pSyncEventHandle = syncEventHandle;
    
    return status;
}
