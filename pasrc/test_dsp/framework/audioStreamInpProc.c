
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamInpProc.c ========
 */

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/hal/Cache.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>

#include <sio.h>
#include <pafsio_ialg.h>

#include "ioConfig.h"

#include "common.h"
#include "paf_heapMgr.h"
#include "aspMsg_common.h"
#include "aspMsg_master.h"
#include "aspDecOpCircBuf_master.h"
#include "aspOutInitSync_master.h"
#include "audioStreamProc_common.h"
#include "audioStreamProc_master.h"
#include "audioStreamInpProc.h"
#include "statusOp_common.h"

#include "pfp/pfp.h"
#include "pfp_app.h"        /* contains all PFP ID's */
Int32 gNumPfpAsit1=0; // debug
Int32 gNumPfpAsit2=0;

// debug
//#include "dbgCapAf.h"


// -----------------------------------------------------------------------------
// Debugging Trace Control, local to this file.
// 
#include "logp.h"

#define TRACE_ARC(a)

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.
#define CURRENT_TRACE_MASK      0x27

#define TRACE_MASK_TERSE        0x01   // only flag errors and show init
#define TRACE_MASK_GENERAL      0x02   // half dozen lines per frame
#define TRACE_MASK_VERBOSE      0x04   // trace full operation
#define TRACE_MASK_DATA         0x08   // Show data
#define TRACE_MASK_TIME         0x10   // Timing related traces
#define TRACE_MASK_MSG          0x20   // Message related traces

#if !(CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
    #undef  TRACE_TERSE0
    #undef  TRACE_TERSE1
    #undef  TRACE_TERSE2
    #undef  TRACE_TERSE3
    #undef  TRACE_TERSE4
    #define TRACE_TERSE0(a)
    #define TRACE_TERSE1(a,b)
    #define TRACE_TERSE2(a,b,c)
    #define TRACE_TERSE3(a,b,c,d)
    #define TRACE_TERSE4(a,b,c,d,e)
#endif

#if !(CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
    #undef  TRACE_GEN0
    #undef  TRACE_GEN1
    #undef  TRACE_GEN2
    #undef  TRACE_GEN3
    #undef  TRACE_GEN4
    #define TRACE_GEN0(a)
    #define TRACE_GEN1(a,b)
    #define TRACE_GEN2(a,b,c)
    #define TRACE_GEN3(a,b,c,d)
    #define TRACE_GEN4(a,b,c,d,e)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
    // consolidate list of processing strings, indexed by PAF_SOURCE
    static char *procName[] =
    {
        "", //PAF_SOURCE_UNKNOWN
        "", //PAF_SOURCE_NONE
        "AS%d: Pass processing ...",   //PAF_SOURCE_PASS
        "AS%d: SNG processing ...",    //PAF_SOURCE_SNG
        "AS%d: Auto processing ...",   //PAF_SOURCE_AUTO
        "AS%d: Auto processing ...",   //PAF_SOURCE_BITSTREAM
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTSALL
        "AS%d: PCM processing ...",    //PAF_SOURCE_PCMAUTO
        "AS%d: PCM processing ...",    //PAF_SOURCE_PCM
        "AS%d: PCN processing ...",    //PAF_SOURCE_PC8
        "AS%d: AC3 processing ...",    //PAF_SOURCE_AC3
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS
        "AS%d: AAC processing ...",    //PAF_SOURCE_AAC
        "AS%d: MPG processing ...",    //PAF_SOURCE_MPEG
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS12
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS13
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS14
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS16
        "AS%d: WMP processing ...",    //PAF_SOURCE_WMA9PRO
        "AS%d: MP3 processing ...",    //PAF_SOURCE_MP3
        "AS%d: DSD processing ...",    //PAF_SOURCE_DSD1
        "AS%d: DSD processing ...",    //PAF_SOURCE_DSD2
        "AS%d: DSD processing ...",    //PAF_SOURCE_DSD3
        "AS%d: DDP processing ...",    //PAF_SOURCE_DDP
        "AS%d: DTSHD processing ...",  //PAF_SOURCE_DTSHD
        "AS%d: THD processing ...",    //PAF_SOURCE_THD
        "AS%d: DXP processing ...",    //PAF_SOURCE_DXP
        "AS%d: WMA processing ...",    //PAF_SOURCE_WMA
    };

#else
    #undef  TRACE_VERBOSE0
    #undef  TRACE_VERBOSE1
    #undef  TRACE_VERBOSE2
    #undef  TRACE_VERBOSE3
    #undef  TRACE_VERBOSE4
    #define TRACE_VERBOSE0(a)
    #define TRACE_VERBOSE1(a,b)
    #define TRACE_VERBOSE2(a,b,c)
    #define TRACE_VERBOSE3(a,b,c,d)
    #define TRACE_VERBOSE4(a,b,c,d,e)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)
    #define TRACE_DATA(a) LOG_printf a
#else
    #define TRACE_DATA(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_TIME)
    #define TRACE_TIME(a) LOG_printf a
    #define TIME_MOD  trace // this could be different
    static Int dtime()
    {
         static Int old_time = 0;
         Int time = TSK_time();
         Int delta_time = time - old_time;
         old_time = time;
         return( delta_time);
    }

    static char *stateName[11] =
    {
         "INIT",
         "INFO1",
         "AGAIN",
         "INFO2",
         "CONT",
         "TIME",
         "DECODE",
         "STREAM",
         "ENCODE",
         "FINAL",
         "QUIT"
    };

#else
    #define TRACE_TIME(a)
#endif

#if !(CURRENT_TRACE_MASK & TRACE_MASK_MSG)
    #undef  TRACE_MSG0
    #undef  TRACE_MSG1
    #undef  TRACE_MSG2
    #undef  TRACE_MSG3
    #undef  TRACE_MSG4
    #define TRACE_MSG0(a)
    #define TRACE_MSG1(a,b)
    #define TRACE_MSG2(a,b,c)
    #define TRACE_MSG3(a,b,c,d)
    #define TRACE_MSG4(a,b,c,d,e)
#endif
    

// .............................................................................

#include <pafsio.h>
#include "paferr.h"

#include <acp_mds.h>

#include <pcm.h>

#include <pce.h>

#include <doberr.h>

#include <diberr.h>

#include <stdasp.h>

#include "as0.h"
#include "asperr.h"

#include "audioStreamProc_params.h"
#include "audioStreamProc_patchs.h"
#include "audioStreamProc_config.h"
#include "audioStreamInpProc.h"


#include "dbgBenchmark.h" // PCM high-sampling rate + SRC + CAR benchmarking
#ifdef _ENABLE_BENCHMARK_PCMHSR_SRC_CAR_
//
// Performing PCM high-sampling rate + SRC + CAR benchmarking
//

#define PROFILER
#endif

#ifdef PROFILER
extern UInt32 start_profiling;
#endif

extern UInt32 gAspProfileEnable;

//
// Audio Stream Definitions
//

// minimum audio frame "length" in samples (must be multiple of PA_MODULO)
#define MINFRAMELENGTH 24
#define PA_MODULO       8   // also defined independently in ARC2 code, and may be hard coded other places.


//
// Decoder Definitions
//
//#define decLinkInit pQ->i_decLinkInit

//
// Audio Stream Processing Definitions
//
//#define aspLinkInit pQ->i_aspLinkInit

//
// Encoder Definitions
//
#define encLinkInit pQ->i_encLinkInit

//
// Input, Output Definitions
//
#define inpLinkInit pP->i_inpLinkInit
//#define outLinkInit pP->i_outLinkInit

/* ---------------------------------------------------------------- */
/*              Parameter macro definitions end here.               */
/* ---------------------------------------------------------------- */

//
// Standardized Definitions
//

//#define DEC_Handle PCM_Handle /* works for all: SNG, PCM, AC3, DTS, AAC */

#define __TASK_NAME__  "TaskAsip"


//
// Audio Stream Input Task definitions
//

#define ASIP_FRAMELEN_SS_DEFAULT    ( 256 )
#define ASIP_FRAMELEN_SS_PCM        ( FRAMELENGTH ) // pP->frameLength
#define ASIP_FRAMELEN_SS_DDP_AC3    ( 1536 )        // hard-coded
#define ASIP_FRAMELEN_SS_THD        ( 1536 )        // hard-coded
#define ASIP_FRAMELEN_SS_DTS        ( 512 )         

// Compute decoder frame length based on selected source
Int getFrameLengthSourceSel(
    const PAF_ASIT_Params *pP, 
    Int8 sourceSelect
);

LINNO_DEFN(TaskAsip); /* Line number macros */
ERRNO_DEFN(TaskAsip); /* Error number macros */

// ASIT ASP messaging
AspMsgMaster_Module gAsitAspMsgMaster;  // ASIT message master

// ASIT configuration
#pragma DATA_SECTION(gPAF_ASIT_config, ".globalSectionPafAsitConfig")
PAF_ASIT_Config gPAF_ASIT_config = {
    NULL,               // taskHandle
    NULL,               // acp
    &gAsitAspMsgMaster, // hAspMsgMaster
    &gPAF_ASPM_config,  // pAspmCfg, shared ASIT/ASOT configuration
    &gPAF_AST_config,   // pAstCfg, shared ASIT/ASOT/ASDT configuration
    NULL,               // pIoInp, ASIT IO configuration
    0                   // inpDec, ASIT state
};

// For writeDECCommandRestart
volatile UInt32 gCommandOutputTask_SYNC   =0;
volatile UInt32 gCommandOutputTask_ACK    =0;

// Global debug counters */
UInt32 gAsipRxSioReclaimCnt     =0;
UInt32 gAsipInitCnt             =0;
UInt32 gAsipInfo1Cnt            =0;
UInt32 gAsipInfo2Cnt            =0;
UInt32 gAsipDecodeCnt           =0;
UInt32 gAsipDecodeErrCnt        =0;
UInt32 gAsipFinalCnt            =0;
UInt32 gAsipQuitCnt             =0;
UInt32 gAsipInfo1_PrimaryErrCnt =0;
UInt32 gAsipInfo1_ErrCnt        =0;
UInt32 gAsipInfo2_PrimaryErrCnt =0;
UInt32 gAsipInfo2_ErrCnt        =0;
// debug
UInt32 gCapIbReset_cnt          =0;
UInt32 gCapIb_cnt               =0;
UInt32 gCbWrtAfErrCnt           =0; // decoder output circular buffer write error count (returned from ARM to DSP)
                                   
#include "dbgDib.h" // debug

extern struct {
    Int size;
    IALG_Status *pStatus[512];
} IACP_STD_BETA_TABLE;

// debug
//#include "evmc66x_gpio_dbg.h"

/*
 *  ======== taskAsipFxnInit ========
 *  Audio Stream Input Processing initialization function
 */
Void taskAsipFxnInit(
    const PAF_ASIT_Params *pP,
    const PAF_ASIT_Patchs *pQ
)
{
    PAF_ASIT_Config *pAsitCfg;          // ASIT configuration pointer
    PAF_AST_Config *pAstCfg;            // AST Common (shared) configuration pointer
    PAF_AST_IoInp *pInp;            /* Input I/O components */
    AspMsgMaster_Handle hAspMsgMaster;  // ASIT message master handle    
    Int as;                             // Audio Stream Number (1, 2, etc.)
    Int z;                              // input/encode/stream/decode/output counter
    Int i;                              // phase
    Int zMI, zMS, zX;
    Int size;
    Int status;

    Log_info0("Enter taskAsipFxnInit()");

    //
    // Audio Stream Input Task Parameters & Patch (*pP, *pQ)
    //
    if (!pP)
    {
        TRACE_TERSE0("TaskAsip: No Parameters defined. Exiting.");
        LINNO_RPRT(TaskAsip, -1);
        return;
    }

    if (!pQ)
    {
        TRACE_TERSE0("TaskAsip: No Patchs defined. Exiting.");
        LINNO_RPRT(TaskAsip, -1);
        return;
    }

    //
    // Audio Stream Input Task Configuration (*pAsitCfg):
    //
    pAsitCfg = &gPAF_ASIT_config;               // initialize pointer to task configuration
    pAsitCfg->taskHandle = Task_self();         // set task handle
    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to AST common (shared) configuration
    hAspMsgMaster = pAsitCfg->hAspMsgMaster;    // get message master handle

    /* Set Audio Stream Number (1, 2, etc.) */
    as = gBetaPrimeValue + 1;
    pAstCfg->as = as;
    TRACE_TERSE1("TaskAsip: Started with AS%d.", as);

    //
    // Initialize message log trace and line number reporting
    //
    for (z=STREAM1; z < STREAMN; z++)
    {
        TRACE_TERSE1("TaskAsip: AS%d: initiated", as+z);
    }
    LINNO_RPRT(TaskAsip, -1);

    //
    // Determine decoder and stream indices associated with the master input
    //
    zMI = pP->zone.master;
    pAstCfg->masterDec = zMI;
    pAstCfg->masterStr = zMI;
    for (zX = DECODE1; zX < DECODEN; zX++)
    {
        if (pP->inputsFromDecodes[zX] == zMI)
        {
            pAstCfg->masterDec = zX;
            pAstCfg->masterStr = pP->streamsFromDecodes[zX];
            break;
        }
    }
    zMS = pAstCfg->masterStr;

    // Initialize as per parametrized phases:
    //
    //   In standard form these are:
    //      - Malloc: Memory Allocation
    //      - Config: Configuration Initialization
    //      - AcpAlg: ACP Algorithm Initialization and Local Attachment
    //      - Common: Common Memory Initialization
    //      - AlgKey: Dec/Enc chain to Array Initialization
    //      - Device: I/O Device Initialization
    //      - DecOpCircBuf: Decoder Output Circular Buffer
    //      - OutIS: Output Init-Sync
    //
    LINNO_RPRT(TaskAsip, -2);
    for (i=0; i < lengthof(pP->fxns->initPhase); i++)
    {
        Int linno;
        if (pP->fxns->initPhase[i])
        {
            linno = pP->fxns->initPhase[i](pP, pQ, pAsitCfg);
            if (linno)
            {
                LINNO_RPRT(TaskAsip, linno);
                return;
            }
        }
        else
        {
            TRACE_TERSE1("TaskAsip: AS%d: initialization phase - null", as+zMS);
        }
        TRACE_TERSE2("TaskAsip: AS%d: initialization phase - %d completed", as+zMS, i);
        LINNO_RPRT(TaskAsip, -i-3);
    }

    //
    // End of Initialization -- display memory usage report.
    //
    if (pP->fxns->memStatusPrint)
    {
        pP->fxns->memStatusPrint("ASIT MEMSTAT REPORT",
            HEAP_INTERNAL, HEAP_INTERNAL1, HEAP_EXTERNAL,
            HEAP_INTERNAL1_SHM, HEAP_EXTERNAL_SHM, HEAP_EXTERNAL_NONCACHED_SHM);
    }

    // write back AST shared configuration
    Cache_wb(pAstCfg, sizeof(PAF_AST_Config), Cache_Type_ALLD, 0);
    Cache_wait();

    // write back Dec configuration
    Cache_wb(&pAstCfg->xDec[0], DECODEN*sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
    Cache_wait();

    // write back entire beta table
    Cache_wb((Ptr)(&IACP_STD_BETA_TABLE.pStatus[0]), 512*sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_wait();

    // send start initialization message to slave
    status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_START, NULL);
    if (status != ASP_MSG_NO_ERR)
    {
        SW_BREAKPOINT;
    }
    
    // wait for initialization complete message from slave
    // no other thread is allowed to run until Slave finishes startup
    do {
        status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_START_DONE, NULL, FALSE);
    } while (status == ASP_MSG_ERR_QUEUE_TIMEOUT);
    if (status != ASP_MSG_NO_ERR)
    {
        SW_BREAKPOINT;
    }

    // invalidate Status structure addresses for Beta Units initialized on Slave
    Cache_inv((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_DECODE]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_inv((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_PCM]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_inv((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_PCM2]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_inv((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_DDP]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_inv((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_DDP2]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_inv((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_THD]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_inv((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_THD2]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_inv((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_DTSUHDA]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_inv((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_AAC]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_inv((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_AAC2]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_inv((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_DSD]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_inv((Ptr)(&IACP_STD_BETA_TABLE.pStatus[STD_BETA_DSD2]), sizeof(IALG_Status *), Cache_Type_ALLD, 0);
    Cache_wait();
    if (!IACP_STD_BETA_TABLE.pStatus[STD_BETA_DECODE]) Log_info0("WARNING: beta unit for Dec==NULL");
    if (!IACP_STD_BETA_TABLE.pStatus[STD_BETA_PCM]) Log_info0("WARNING: beta unit for Dec==NULL");
    if (!IACP_STD_BETA_TABLE.pStatus[STD_BETA_PCM2]) Log_info0("WARNING: beta unit for Dec==NULL");
    if (!IACP_STD_BETA_TABLE.pStatus[STD_BETA_DDP]) Log_info0("WARNING: beta unit for Dec==NULL");
    if (!IACP_STD_BETA_TABLE.pStatus[STD_BETA_DDP2]) Log_info0("WARNING: beta unit for Dec==NULL");
    if (!IACP_STD_BETA_TABLE.pStatus[STD_BETA_THD]) Log_info0("WARNING: beta unit for Dec==NULL");
    if (!IACP_STD_BETA_TABLE.pStatus[STD_BETA_THD2]) Log_info0("WARNING: beta unit for Dec==NULL");
    if (!IACP_STD_BETA_TABLE.pStatus[STD_BETA_DTSUHDA]) Log_info0("WARNING: beta unit for Dec==NULL");
    if (!IACP_STD_BETA_TABLE.pStatus[STD_BETA_AAC]) Log_info0("WARNING: beta unit for Dec==NULL");
    if (!IACP_STD_BETA_TABLE.pStatus[STD_BETA_AAC2]) Log_info0("WARNING: beta unit for Dec==NULL");
    if (!IACP_STD_BETA_TABLE.pStatus[STD_BETA_DSD]) Log_info0("WARNING: beta unit for Dec==NULL");
    if (!IACP_STD_BETA_TABLE.pStatus[STD_BETA_DSD2]) Log_info0("WARNING: beta unit for Dec==NULL");
    
    // (***) FL: revisit
    // invalidate Status structures for Beta Units initialized on Slave
    if (IACP_STD_BETA_TABLE.pStatus[STD_BETA_DECODE] != NULL)
    {
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_DECODE]), sizeof(Int), Cache_Type_ALLD, 0);
        size = IACP_STD_BETA_TABLE.pStatus[STD_BETA_DECODE]->size;
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_DECODE]), size, Cache_Type_ALLD, 0);
        Cache_wait();
    }
    if (IACP_STD_BETA_TABLE.pStatus[STD_BETA_PCM] != NULL)
    {
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_PCM]), sizeof(Int), Cache_Type_ALLD, 0);
        size = IACP_STD_BETA_TABLE.pStatus[STD_BETA_PCM]->size;
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_PCM]), size, Cache_Type_ALLD, 0);
        Cache_wait();
    }
    if (IACP_STD_BETA_TABLE.pStatus[STD_BETA_PCM2] != NULL)
    {
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_PCM2]), sizeof(Int), Cache_Type_ALLD, 0);
        size = IACP_STD_BETA_TABLE.pStatus[STD_BETA_PCM2]->size;
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_PCM2]), size, Cache_Type_ALLD, 0);
        Cache_wait();
    }
    if (IACP_STD_BETA_TABLE.pStatus[STD_BETA_DDP] != NULL)
    {
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_DDP]), sizeof(Int), Cache_Type_ALLD, 0);
        size = IACP_STD_BETA_TABLE.pStatus[STD_BETA_DDP]->size;
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_DDP]), size, Cache_Type_ALLD, 0);
        Cache_wait();
    }
    if (IACP_STD_BETA_TABLE.pStatus[STD_BETA_DDP2] != NULL)
    {
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_DDP2]), sizeof(Int), Cache_Type_ALLD, 0);
        size = IACP_STD_BETA_TABLE.pStatus[STD_BETA_DDP2]->size;
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_DDP2]), size, Cache_Type_ALLD, 0);
        Cache_wait();
    }
    if (IACP_STD_BETA_TABLE.pStatus[STD_BETA_THD] != NULL)
    {
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_THD]), sizeof(Int), Cache_Type_ALLD, 0);
        size = IACP_STD_BETA_TABLE.pStatus[STD_BETA_THD]->size;
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_THD]), size, Cache_Type_ALLD, 0);
        Cache_wait();
    }
    if (IACP_STD_BETA_TABLE.pStatus[STD_BETA_THD2] != NULL)
    {
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_THD2]), sizeof(Int), Cache_Type_ALLD, 0);
        size = IACP_STD_BETA_TABLE.pStatus[STD_BETA_THD2]->size;
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_THD2]), size, Cache_Type_ALLD, 0);
        Cache_wait();
    }
    if (IACP_STD_BETA_TABLE.pStatus[STD_BETA_DTSUHDA] != NULL)
    {
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_DTSUHDA]), sizeof(Int), Cache_Type_ALLD, 0);
        size = IACP_STD_BETA_TABLE.pStatus[STD_BETA_DTSUHDA]->size;
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_DTSUHDA]), size, Cache_Type_ALLD, 0);
        Cache_wait();
    }
    if (IACP_STD_BETA_TABLE.pStatus[STD_BETA_AAC] != NULL)
    {
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_AAC]), sizeof(Int), Cache_Type_ALLD, 0);
        size = IACP_STD_BETA_TABLE.pStatus[STD_BETA_AAC]->size;
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_AAC]), size, Cache_Type_ALLD, 0);
        Cache_wait();
    }
    if (IACP_STD_BETA_TABLE.pStatus[STD_BETA_AAC2] != NULL)
    {
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_AAC2]), sizeof(Int), Cache_Type_ALLD, 0);
        size = IACP_STD_BETA_TABLE.pStatus[STD_BETA_AAC2]->size;
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_AAC2]), size, Cache_Type_ALLD, 0);
        Cache_wait();
    }
    if (IACP_STD_BETA_TABLE.pStatus[STD_BETA_DSD] != NULL)
    {
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_DSD]), sizeof(Int), Cache_Type_ALLD, 0);
        size = IACP_STD_BETA_TABLE.pStatus[STD_BETA_DSD]->size;
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_DSD]), size, Cache_Type_ALLD, 0);
        Cache_wait();
    }
     if (IACP_STD_BETA_TABLE.pStatus[STD_BETA_DSD2] != NULL)
    {
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_DSD2]), sizeof(Int), Cache_Type_ALLD, 0);
        size = IACP_STD_BETA_TABLE.pStatus[STD_BETA_DSD2]->size;
        Cache_inv((Ptr)(IACP_STD_BETA_TABLE.pStatus[STD_BETA_DSD2]), size, Cache_Type_ALLD, 0);
    }

#ifdef NON_CACHE_STATUS
    // Get the shared status structure GateMP handles. Initialized at slave.
    if (statusOp_Init(GATEMP_INDEX_DEC) == STATUSOP_INIT_FAIL)
    {
        Log_info1("TaskAfp: Gate Index %d:Open status GateMP Fail.", GATEMP_INDEX_DEC);
    }
    if (statusOp_Init(GATEMP_INDEX_DDP) == STATUSOP_INIT_FAIL)
    {
        Log_info1("TaskAfp: Gate Index %d:Open status GateMP Fail.", GATEMP_INDEX_DDP);
    }
    if (statusOp_Init(GATEMP_INDEX_PCM) == STATUSOP_INIT_FAIL)
    {
        Log_info1("TaskAfp: Gate Index %d:Open status GateMP Fail.", GATEMP_INDEX_PCM);
    }
    if (statusOp_Init(GATEMP_INDEX_THD) == STATUSOP_INIT_FAIL)
    {
        Log_info1("TaskAfp: Gate Index %d:Open status GateMP Fail.", GATEMP_INDEX_THD);
    }
    if (statusOp_Init(GATEMP_INDEX_DTS) == STATUSOP_INIT_FAIL)
    {
        Log_info1("TaskAfp: Gate Index %d:Open status GateMP Fail.", GATEMP_INDEX_DTS);
    }
    if (statusOp_Init(GATEMP_INDEX_DSD) == STATUSOP_INIT_FAIL)
    {
        Log_info1("TaskAfp: Gate Index %d:Open status GateMP Fail.", GATEMP_INDEX_DSD);
    }
    if (statusOp_Init(GATEMP_INDEX_AAC) == STATUSOP_INIT_FAIL)
    {
        Log_info1("TaskAfp: Gate Index %d:Open status GateMP Fail.", GATEMP_INDEX_AAC);
    }
#endif

    // invalidate Dec configuration
    Cache_inv(&gPAF_AST_config.xDec[0], DECODEN*sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
    Cache_wait();

    pInp = &pAsitCfg->pIoInp[zMI];        // pointer to input I/O components
    pInp->asipState = ASIT_RESET;
    pInp->firstTimeInit = TRUE;
    pInp->numAsitRestart = 0;
    pInp->hMcaspChan = NULL;

} /* taskAsipFxnInit */

/*
 *  ======== taskAsipFxn ========
 *  Audio Stream Input Processing task function
 */
#ifdef PASDK_SIO_DEV
Void taskAsipFxn(
#else
Void taskAsipFxn_Not_Used(
#endif
    const PAF_ASIT_Params *pP,
    const PAF_ASIT_Patchs *pQ
)
{
    PAF_ASIT_Config *pAsitCfg;          // ASIT configuration pointer
    PAF_AST_Config *pAstCfg;            // AST Common (shared) configuration pointer
    AspMsgMaster_Handle hAspMsgMaster;  // ASIT message master handle
    Int as;                             // Audio Stream Number (1, 2, etc.)
    Int z;                              // input/encode/stream/decode/output counter
    Int errno;                          // error number
    Int zMD, zMI, zMS;
    Int loopCount = 0;  // used to stop trace to see startup behavior.        
    // Messaging
    Int8 tempVar8;
    char asipMsgBuf[ASP_MSG_BUF_LEN];
    Int status;

    Log_info0("Enter taskAsipFxn()");

    taskAsipFxnInit(pP, pQ);  // initialization of input task
    
    //
    // Audio Stream Input Task Configuration (*pAsitCfg):
    //
    pAsitCfg = &gPAF_ASIT_config;       // initialize pointer to task configuration
    pAstCfg = pAsitCfg->pAstCfg;        // get pointer to AST common (shared) configuration
    hAspMsgMaster = pAsitCfg->hAspMsgMaster; // get message master handle

    /* Set Audio Stream Number (1, 2, etc.) */
    as = pAstCfg->as;

    //
    // Determine decoder and stream indices associated with the master input
    //
    zMI = pP->zone.master;
    zMD = pAstCfg->masterDec;
    zMS = pAstCfg->masterStr;
    
    //
    // Main processing loop
    //   
    for (z=STREAM1; z < STREAMN; z++)
    {
        TRACE_VERBOSE1("TaskAsip: AS%d: running", as+z);
    }

    TRACE_TERSE0("TaskAsip: Entering Main Loop.");
    
    errno = 0;
    for (;;)
    {
        Int sourceSelect;
        XDAS_Int8 sourceProgram;

        loopCount++;

#if 0   // enable and tune to see startup behavior.
        // this is an alternative to fixed/circular setting in pa.cfg.
        // If you are searching for a memory allocation failure, disable on first round.
        // All allocation has already happened.
        // This is the outer loop.  This loop count goes up when the stream resets.
        // If the stream is running without problems, this does not increment.
        // If the stream is repeatedly resetting, this loop count will go up rapidly.
        if (loopCount > 10)  // see traces for a few of the passes through the main loop.
        {
             TRACE_TERSE1("TaskAsip: Trace stopped at loop %d.", loopCount);
             LOG_disable(&TR_MOD);  // stop tracing
        }
#endif

        TRACE_GEN2("TaskAsip (begin Main loop %d) (errno 0x%x)", loopCount, errno);
        TRACE_TIME((&TIME_MOD, "as1_f2... + %d = %d (begin Main loop)", dtime(), TSK_time()));
        
        // since not decoding indicate such
        pP->fxns->sourceDecode(pP, pQ, pAsitCfg, PAF_SOURCE_NONE);

        // any error forces idling of input
        if (errno) 
        {
            // If gCommandOutputTask_SYNC invoked by writeDECCommandRestart, wait for output task in sync ...
            if (gCommandOutputTask_SYNC) {
                TRACE_VERBOSE1("TaskAsip: writeDECCommandRestart issued ... Wait for ack from output task side (errno=%d)", errno);
                // Wait for ack
                while (gCommandOutputTask_ACK == 0) {
                    Task_sleep(1);
                }
                TRACE_VERBOSE0("TaskAsip: writeDECCommandRestart issued ... Sync-ed! Startover the process");
                gCommandOutputTask_ACK = 0;
                gCommandOutputTask_SYNC = 0;
            }
            
            for (z=INPUT1; z < INPUTN; z++)
            {
                if (pAstCfg->xInp[z].hRxSio)
                {
                    SIO_idle(pAstCfg->xInp[z].hRxSio);
                }
            }
        
            TRACE_TERSE1("TaskAsip: Trace stopped at loop %d.", loopCount);
            ERRNO_RPRT(TaskAsip, errno);
        }
        
        // Execute a TSK_sleep to ensure that any non-blocking code paths are broken
        // up to allow lower priority tasks to run. This may seem odd to be at the top
        // of the state machine but provides for a cleaner flow even though the very
        // first time we enter we do a sleep which is non-intuitive.
        TRACE_VERBOSE1("TaskAsip: AS%d: ... sleeping ...", as+zMS);
        TRACE_TIME((&TIME_MOD, "as1-f2... + %d = %d (begin SLEEP)", dtime(), TSK_time()));
        Task_sleep(1);

        TRACE_GEN1("TaskAsip: AS%d: Input device selection ...", as+zMS);
        errno = pP->fxns->selectDevices(pP, pQ, pAsitCfg);
        if (errno)
        {
            TRACE_TERSE2("TaskAsip: selectDevices returned errno = 0x%04x at line %d. AS%d", errno, as+zMS);
            continue;
        }

        // if no master input selected then we don't know what may be at the input
        // so set to unknown and skip any remaining processing
        if (!pAstCfg->xInp[zMI].hRxSio)
        {
            sharedMemWriteInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceProgram), 
                               (Int8)PAF_SOURCE_UNKNOWN, GATEMP_INDEX_DEC);

            TRACE_VERBOSE1("TaskAsip: AS%d: No input selected...", as+zMS);
            continue;
        }
        
        // if here then we have a valid input so query its status
        errno = pP->fxns->updateInputStatus(pAstCfg->xInp[zMI].hRxSio, &pAstCfg->xInp[zMI].inpBufStatus, &pAstCfg->xInp[zMI].inpBufConfig);
        if (errno)
        {
            TRACE_VERBOSE1("TaskAsip: continue as updateInputStatus returns 0x%x", errno);
            continue;
        }

        // If master decoder is not enabled, or the input is unlocked, then do nothing
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.mode), 
                                     GATEMP_INDEX_DEC);
        if (!tempVar8 || !pAstCfg->xInp[zMI].inpBufStatus.lock)
        {
            TRACE_VERBOSE0("TaskAsip: Not locked, continue");
            continue;
        }
        
        // If no source selected then do nothing
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceSelect), 
                                     GATEMP_INDEX_DEC);
        if (tempVar8 == PAF_SOURCE_NONE)
        {
            sharedMemWriteInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceProgram), 
                               (Int8)PAF_SOURCE_NONE, GATEMP_INDEX_DEC);
            TRACE_VERBOSE1("TaskAsip: AS%d: no source selected, continue", as+zMS);
            continue;
        }

        // If we want pass processing then proceed directly
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceSelect), 
                                     GATEMP_INDEX_DEC);
        if (tempVar8 == PAF_SOURCE_PASS)
        {
            TRACE_VERBOSE1("TaskAsip: AS%d: Pass processing ...", as+zMS);
            sharedMemWriteInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceProgram), 
                               (Int8)PAF_SOURCE_PASS, GATEMP_INDEX_DEC);

            pP->fxns->sourceDecode(pP, pQ, pAsitCfg, PAF_SOURCE_PASS);
            if (pP->fxns->passProcessing)
            {
                errno = pP->fxns->passProcessing(pP, pQ, pAsitCfg, NULL);                
            }
            else 
            {
                TRACE_TERSE2("TaskAsip: AS%d: Pass Processing not supported, errno 0x%x", as+zMS, ASPERR_PASS);
                errno = ASPERR_PASS;
            }
            TRACE_VERBOSE0("TaskAsip: continue");
            continue;
        }

        // .....................................................................
        // At this point we have an enabled input and want to decode something.
        // If no decoder selected then do nothing. Need to reset the sourceProgram, since
        // when no decoder is selected there are no calls to IB
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceSelect), 
                                     GATEMP_INDEX_DEC);
        pfpBegin(PFP_ID_ASIT_1, pAsitCfg->taskHandle);  // PFP begin
        gNumPfpAsit1++;
        errno = pP->fxns->autoProcessing(pP, pQ, pAsitCfg, tempVar8, NULL);
        pfpEnd(PFP_ID_ASIT_1, PFP_FINISH_MEAS);         // PFP end
        gNumPfpAsit1--;

        if (errno)
        {
            TRACE_VERBOSE1("TaskAsip: autoProcessing returns 0x%x, continue", errno);
            continue;
        }
        
        // query for input type
        errno = SIO_ctrl(pAstCfg->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_SOURCEPROGRAM, (Arg )&sourceProgram);
        if (errno)
        {
            TRACE_TERSE2("TaskAsip: SIO_ctrl returns 0x%x, then 0x%x, continue", errno, ASPERR_AUTO_PROGRAM);
            errno = ASPERR_AUTO_PROGRAM;
            continue;
        }

        sharedMemWriteInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceProgram), 
                           sourceProgram, GATEMP_INDEX_DEC);

        // if input is unclassifiable then do nothing
        if (sourceProgram == PAF_SOURCE_UNKNOWN)
        {
            TRACE_VERBOSE0("TaskAsip: Source program unknown. continue");
            continue;
        }

        // now that we have some input classification, and possibly an outstanding
        // input frame, we determine whether or not to call decodeProcessing and with
        // what decAlg.
        sourceSelect = PAF_SOURCE_NONE;
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceSelect), 
                                     GATEMP_INDEX_DEC);
        switch (tempVar8)
        {
            // If autodetecting, decoding everything, and input is something
            // (i.e. bitstream or PCM) then decode.
            case PAF_SOURCE_AUTO:
                if (sourceProgram >= PAF_SOURCE_PCM)
                {
                    sourceSelect = sourceProgram;                    
                }
                break;

            // If autodetecting, decoding only PCM, and input is PCM then decode.
            case PAF_SOURCE_PCMAUTO:
                if (sourceProgram == PAF_SOURCE_PCM)
                {
                    sourceSelect = sourceProgram;                    
                }
                break;

            // If autodetecting, decoding only bitstreams, and input is a bitstream then decode.
            case PAF_SOURCE_BITSTREAM:
                if (sourceProgram >= PAF_SOURCE_AC3)
                {
                    sourceSelect = sourceProgram;                    
                }
                break;

            // If autodetecting, decoding only DTS, and input is DTS then decode.
            case PAF_SOURCE_DTSALL:
                switch (sourceProgram) 
                {
                    case PAF_SOURCE_DTS11:
                    case PAF_SOURCE_DTS12:
                    case PAF_SOURCE_DTS13:
                    case PAF_SOURCE_DTS14:
                    case PAF_SOURCE_DTS16:
                    case PAF_SOURCE_DTSHD:
                        sourceSelect = sourceProgram;
                        break;
                }
                break;

            // All others, e.g., force modes, fall through to here.
            // If user made specific selection then program must match select.
            // (NB: this compare relies on ordering of PAF_SOURCE)
            default:
                sourceSelect = (Int)sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceSelect),
                		                              GATEMP_INDEX_DEC);
                if ((sourceSelect >= PAF_SOURCE_PCM) && (sourceSelect <= PAF_SOURCE_N))
                {
                    if (sourceProgram != sourceSelect)
                    {
                        sourceSelect = PAF_SOURCE_NONE;                        
                    }
                }
                break;
        }

        // if we didn't find any matches then skip
        if (sourceSelect == PAF_SOURCE_NONE)
        {
            TRACE_VERBOSE0("TaskAsip: no matching source type, continue");
            continue;
        }

#if 0 // debug
        // reset IB capture buffer
        capIbReset();
        gCapIbReset_cnt++;
        Log_info0("capIbReset()");
#endif

        // send source select message to slave
        *(Int32 *)&asipMsgBuf[0] = sourceSelect;
        status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_DEC_SOURCE_SELECT, asipMsgBuf);
        if (status != ASP_MSG_NO_ERR)
        {
            TRACE_VERBOSE0("TaskAsip: error in sending SOURCE_SELECT message");
            SW_BREAKPOINT;            
        }
        // receive source select acknowledge message from slave
        status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_DEC_SOURCE_SELECT_DONE, NULL, TRUE);
        if (status != ASP_MSG_NO_ERR)
        {
            TRACE_VERBOSE0("TaskAsip: error in receiving SOURCE_SELECT_DONE ack message");
            SW_BREAKPOINT;            
        }
        
        // set to unknown so that we can ensure, for IOS purposes, that sourceDecode = NONE
        // iff we are in this top level state machine and specifically not in decodeProcessing
        pP->fxns->sourceDecode(pP, pQ, pAsitCfg, PAF_SOURCE_UNKNOWN);
        
        TRACE_VERBOSE1(procName[sourceProgram], as+zMS);

        // Reduce priority level since autodetection complete
        Task_setPri(pAsitCfg->taskHandle, Task_getPri(pAsitCfg->taskHandle)-1);
        
        gAspProfileEnable=1; // enable ASP profiling
        
        TRACE_VERBOSE0("TaskAsip: calling decodeProcessing.");
        errno = pP->fxns->decodeProcessing(pP, pQ, pAsitCfg, sourceSelect);
        if (errno) 
        {
            TRACE_TERSE1("TaskAsip: decodeProcessing returns 0x%x, continue", errno);
        }
        else
        {
            TRACE_VERBOSE0("TaskAsip: decodeProcessing complete with no error.");
        }        

        gAspProfileEnable=0; // disable ASP profiling
        
        // Increase priority level since decoding complete
        Task_setPri(pAsitCfg->taskHandle, Task_getPri(pAsitCfg->taskHandle)+1);

        // send dec exit message to slave
        status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_DEC_EXIT, NULL);
        if (status != ASP_MSG_NO_ERR)
        {
            TRACE_VERBOSE0("TaskAsip: error in sending DEC_EXIT message");
            SW_BREAKPOINT;                
        }
        // receive dec exit acknowledge message from slave
        status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_DEC_EXIT_DONE, NULL, TRUE);
        if (status != ASP_MSG_NO_ERR)
        {
            TRACE_VERBOSE0("TaskAsip: error in receiving DEC_EXIT_DONE ack message");
            SW_BREAKPOINT;                
        }
    } // End of main processing loop for (;;)
    
    //Log_info0("Exit taskAsipFxn()");
}

Int PAF_ASIT_ioCompCreate(PAF_AST_IoInp *pIoInp, int numInp, IHeap_Handle iHeapHandle)
{
    int i, j, num_alloc;
    lib_mem_rec_t *mem_rec;
    ioBuffHandle_t  ioBufHandle;
    ioPhyHandle_t   ioPhyHandle;
    ioDataHandle_t  ioDataHandle;
    Error_Block     eb;

    for(i=0; i<numInp; i++)
    {
        // Create an I/O BUFF instance
        // Obtain number of memory blocks required by I/O BUFF
        num_alloc = ioBuffNumAlloc();

        // Obtain requirements of each memory block
        mem_rec   = (lib_mem_rec_t *)malloc(sizeof(lib_mem_rec_t)*num_alloc);
        if(ioBuffAlloc(mem_rec) != IOBUFF_NOERR) {
            return __LINE__;
        }

        /* Allocate memory and create I/O BUFF instance */
        for(j=0; j<num_alloc; j++) {
            mem_rec[j].base = Memory_calloc(iHeapHandle, mem_rec[j].size,
                                            (1<<mem_rec[j].alignment), &eb);
        }

        if(ioBuffCreate(&ioBufHandle, mem_rec) != IOBUFF_NOERR) {
            return __LINE__;
        }

        pIoInp[i].hIoBuff = ioBufHandle;

        free(mem_rec);

        // Create an I/O PHY instance
        // Obtain number of memory blocks required by I/O PHY
        num_alloc = ioPhyNumAlloc();

        // Obtain requirements of each memory block
        mem_rec   = (lib_mem_rec_t *)malloc(sizeof(lib_mem_rec_t)*num_alloc);
        if(ioPhyAlloc(mem_rec) != IOBUFF_NOERR) {
            return __LINE__;
        }

        /* Allocate memory and create I/O PHY instance */
        for(j=0; j<num_alloc; j++) {
            mem_rec[j].base = Memory_calloc(iHeapHandle, mem_rec[j].size,
                                            (1<<mem_rec[j].alignment), &eb);
        }

        if(ioPhyCreate(&ioPhyHandle, mem_rec) != IOBUFF_NOERR) {
            return __LINE__;
        }

        pIoInp[i].hIoPhy = ioPhyHandle;

        free(mem_rec);

        // Create an I/O DATA instance
        // Obtain number of memory blocks required by I/O DATA
        num_alloc = ioDataNumAlloc();

        // Obtain requirements of each memory block
        mem_rec   = (lib_mem_rec_t *)malloc(sizeof(lib_mem_rec_t)*num_alloc);
        if(ioDataAlloc(mem_rec) != IOBUFF_NOERR) {
            return __LINE__;
        }

        /* Allocate memory and create I/O DATA instance */
        for(j=0; j<num_alloc; j++) {
            mem_rec[j].base = Memory_calloc(iHeapHandle, mem_rec[j].size,
                                            (1<<mem_rec[j].alignment), &eb);
        }

        if(ioDataCreate(&ioDataHandle, mem_rec) != IOBUFF_NOERR) {
            return __LINE__;
        }

        pIoInp[i].hIoData = ioDataHandle;

        free(mem_rec);

    } // end of for loop

    return 0;
} // PAF_ASIT_ioCompCreate

// -----------------------------------------------------------------------------
// ASIT Initialization Function - Memory Allocation
//
//   Name:      PAF_ASIT_initPhaseMalloc
//   Purpose:   Audio Stream Input Task Function for initialization of data pointers
//              by allocation of memory.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on MEM_calloc failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//
Int
PAF_ASIT_initPhaseMalloc(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                    /* Audio Stream Number (1, 2, etc.) */
    Int zMS, errLineNum;
    Error_Block    eb;
	IHeap_Handle   decHeapHandle;

    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    zMS = pAstCfg->masterStr;
    
    TRACE_TERSE1("PAF_ASIT_initPhaseMalloc: AS%d: initialization phase - memory allocation", as+zMS);

    // Initialize error block
    Error_init(&eb); 

    /* Input memory */
    if (!(pAstCfg->xInp = Memory_calloc((IHeap_Handle)HEAP_INTERNAL1_SHM, 
        INPUTN * sizeof (*pAstCfg->xInp), 4, &eb)))
    {
        TRACE_TERSE1("PAF_ASIT_initPhaseMalloc: AS%d: Memory_calloc failed", as+zMS);
        SW_BREAKPOINT;
        return __LINE__;
    }
    TRACE_TERSE3("PAF_ASIT_initPhaseMalloc. (pAstCfg->xInp) %d bytes from space %d at 0x%x.",
        INPUTN * sizeof (*pAstCfg->xInp),
        HEAP_ID_INTERNAL1_SHM, (IArg)pAstCfg->xInp);

    /* Input I/O data structure memory */
    if (!(pAsitCfg->pIoInp = Memory_calloc((IHeap_Handle)HEAP_INTERNAL1_SHM,
        INPUTN * sizeof (*pAsitCfg->pIoInp), 4, &eb)))
    {
        TRACE_TERSE1("PAF_ASIT_initPhaseMalloc: AS%d: Memory_calloc failed", as+zMS);
        SW_BREAKPOINT;
        return __LINE__;
    }
    TRACE_TERSE3("PAF_ASIT_initPhaseMalloc. (pAsitCfg->pIoInp) %d bytes from space %d at 0x%x.",
        INPUTN * sizeof (*pAsitCfg->pIoInp),
        HEAP_ID_INTERNAL1_SHM, (IArg)pAsitCfg->pIoInp);

    /* I/O components memory for input */
    errLineNum = PAF_ASIT_ioCompCreate(pAsitCfg->pIoInp, INPUTN, (IHeap_Handle)HEAP_INTERNAL1_SHM);
    if(errLineNum)
    {
        SW_BREAKPOINT;
        return errLineNum;
    }

    /* Decode memory */
#ifdef NON_CACHE_STATUS
    decHeapHandle = (IHeap_Handle)HEAP_EXTERNAL_NONCACHED_SHM;
#else 
    decHeapHandle = (IHeap_Handle)HEAP_INTERNAL1_SHM;
#endif
    if (!(pAstCfg->xDec = Memory_calloc(decHeapHandle,
        DECODEN * sizeof (*pAstCfg->xDec), 4, &eb)))
    {
        TRACE_TERSE1("PAF_ASIT_initPhaseMalloc: AS%d: Memory_calloc failed", as+zMS);
        SW_BREAKPOINT;
        return __LINE__;
    }

    TRACE_TERSE3("PAF_ASIT_initPhaseMalloc. (pAstCfg->xDec) %d bytes from space %d at 0x%x.",
        DECODEN * sizeof (*pAstCfg->xDec),
        HEAP_ID_INTERNAL1_SHM, (IArg)pAstCfg->xDec);
#if 0
    if (!(pAsitCfg->pInpDec = Memory_calloc(decHeapHandle,
        DECODEN * sizeof (*pAsitCfg->pInpDec), 4, &eb)))
    {
        TRACE_TERSE1("PAF_ASIT_initPhaseMalloc: AS%d: Memory_calloc failed", as+zMS);
        SW_BREAKPOINT;
        return __LINE__;
    }
    TRACE_TERSE3("PAF_ASIT_initPhaseMalloc. (pAsitCfg->pInpDec) %d bytes from space %d at 0x%x.",
                 DECODEN * sizeof (*pAsitCfg->pInpDec),
                 HEAP_ID_INTERNAL1_SHM, (IArg)pAsitCfg->pInpDec);
#endif
    TRACE_TERSE1("PAF_ASIT_initPhaseMalloc: AS%d: initialization phase - memory allocation complete.", as+zMS);
    return 0;
} //PAF_ASIT_initPhaseMalloc

// -----------------------------------------------------------------------------
// ASIT Initialization Function - Memory Initialization from Configuration
//
//   Name:      PAF_ASIT_initPhaseConfig
//   Purpose:   Audio Stream Task Function for initialization of data values
//              from parameters.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Other as per initFrame0 and initFrame1.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
Int
PAF_ASIT_initPhaseConfig(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                     /* input/encode/stream/decode/output counter */
    Int zMS;

    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    zMS = pAstCfg->masterStr;
    
    TRACE_TERSE1("PAF_ASIT_initPhaseConfig: AS%d: initialization phase - configuration", as+zMS);

    //
    // Unspecified elements have been initialized to zero during alloc
    //
    
    for (z=INPUT1; z < INPUTN; z++) 
    {
        pAstCfg->xInp[z].inpBufStatus = *pP->pInpBufStatus;
        pAstCfg->xInp[z].inpBufConfig.pBufStatus = &pAstCfg->xInp[z].inpBufStatus;
    }

    for (z=DECODE1; z < DECODEN; z++) 
    {
        Int zI = pP->inputsFromDecodes[z];
        pAstCfg->xDec[z].decodeControl.size = sizeof(pAstCfg->xDec[z].decodeControl);
        pAstCfg->xDec[z].decodeControl.pInpBufConfig = (const PAF_InpBufConfig *)&pAstCfg->xInp[zI].inpBufConfig;
    }

    TRACE_TERSE1("PAF_ASIT_initPhaseConfig: AS%d: initialization phase - configuration complete.", as+zMS);
    return 0;
} //PAF_ASIT_initPhaseConfig

// -----------------------------------------------------------------------------
// ASIT Initialization Function - ACP Algorithm Instantiation
//
//   Name:      PAF_ASIT_initPhaseAcpAlg
//   Purpose:   Audio Stream Input Task Function for initialization of ACP by
//              instantiation of the algorithm.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on ACP Algorithm creation failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//
Int
PAF_ASIT_initPhaseAcpAlg(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                 /* Audio Stream Number (1, 2, etc.) */
    Int z;                  /* input/encode/stream/decode/output counter */
    Int betaPrimeOffset;
    ACP_Handle acp;
    Int zMS;
    Int zS, zX;

    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    zMS = pAstCfg->masterStr;
    
    TRACE_TERSE1("PAF_ASIT_initPhaseAcpAlg: AS%d: initialization phase - ACP Algorithm", as+zMS);

    ACP_MDS_init();

    if (!(acp = (ACP_Handle)ACP_MDS_create(NULL))) 
    {
        TRACE_TERSE1("PAF_ASIT_initPhaseAcpAlg: AS%d: ACP algorithm instance creation failed", as+zMS);
        return __LINE__;
    }
    pAsitCfg->acp = acp;

    ((ALG_Handle)acp)->fxns->algControl((ALG_Handle) acp,
        ACP_GETBETAPRIMEOFFSET, (IALG_Status *)&betaPrimeOffset);

    for (z=INPUT1; z < INPUTN; z++) 
    {
        zS = z;
        for (zX = DECODE1; zX < DECODEN; zX++) 
        {
            if (pP->inputsFromDecodes[zX] == z) 
            {
                zS = pP->streamsFromDecodes[zX];
                break;
            }
        }
        acp->fxns->attach(acp, ACP_SERIES_STD,
            STD_BETA_IB + betaPrimeOffset * (as-1+zS),
            (IALG_Status *)&pAstCfg->xInp[z].inpBufStatus);
        /* Ignore errors, not reported. */
    }

    TRACE_TERSE1("PAF_ASIT_initPhaseAcpAlg: AS%d: initialization phase - ACP Algorithm complete.", as+zMS);

    return 0;
} //PAF_ASIT_initPhaseAcpAlg

// -----------------------------------------------------------------------------
// ASIT Initialization Function - Common Memory
//
//   Name:      PAF_ASIT_initPhaseCommon
//   Purpose:   Audio Stream Input Task Function for allocation of common memory.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on PAF_ALG_alloc failure.
//              Source code line number on PAF_ALG_mallocMemory failure.
//              Source code line number on Decode Chain initialization failure.
//              Source code line number on ASP Chain initialization failure.
//              Source code line number on Encode Chain initialization failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//
Int
PAF_ASIT_initPhaseCommon(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* stream counter */
    ACP_Handle acp;
    PAF_IALG_Config pafAlgConfig;
    IALG_MemRec common[3][PAF_IALG_COMMON_MEMN+1];
   
    acp = pAsitCfg->acp;
    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    
    TRACE_TERSE0("PAF_ASIT_initPhaseCommon: initialization phase - Common Memory");

    //
    // Determine memory needs and instantiate algorithms across audio streams
    //
    TRACE_TERSE0("PAF_ASIT_initPhaseCommon: calling PAF_ALG_setup.");
    PAF_ALG_setup(&pafAlgConfig, 
        HEAP_ID_INTERNAL,               HEAP_INTERNAL, 
        HEAP_ID_INTERNAL1,              HEAP_INTERNAL1, 
        HEAP_ID_EXTERNAL,               HEAP_EXTERNAL, 
        HEAP_ID_INTERNAL1_SHM,          HEAP_INTERNAL1_SHM, 
        HEAP_ID_EXTERNAL_SHM,           HEAP_EXTERNAL_SHM, 
        HEAP_ID_EXTERNAL_NONCACHED_SHM, HEAP_EXTERNAL_NONCACHED_SHM,
        HEAP_CLEAR);

    if (pP->fxns->headerPrint)
    {
        pP->fxns->headerPrint();        
    }

    for (z = STREAM1; z < STREAMN; z++) 
    {
        TRACE_TERSE1("PAF_ASIT_initPhaseCommon: AS%d: initialization phase - Common Memory", as+z);

        //
        // Determine common memory for:
        //  (1) Logical Input drivers
        //
        // Decode Algorithms common memory determined in ASP Slave.
        //
        PAF_ALG_init(common[z], lengthof(common[z]), COMMONSPACE);

        //
        // Determine common memory needs of Logical Input driver
        //

        // really need to loop over all inputs for this stream using the tables
        // inputsFromDecodes and streamsFromDecodes. But these don't exist for this
        // patch, and not needed for FS11, since there is only one input.
        if (INPUT1 <= z && z < INPUTN) 
        {
            TRACE_TERSE2("PAF_ASIT_initPhaseCommon: AS%d: alloc inpLinkInit common[%d]", as+z, z);
            if (PAF_ALG_ALLOC(inpLinkInit[z-INPUT1], common[z]))
            {
                TRACE_TERSE1("PAF_ASIT_initPhaseCommon: AS%d: PAF_ALG_alloc failed", as+z);
                TRACE_TERSE2("failed to alloc %d bytes from space %d", common[z]->size, (IArg)common[z]->space);
                SW_BREAKPOINT;
                return __LINE__;
            }
            TRACE_TERSE3("alloced %d bytes from space %d at 0x%x", common[z]->size, common[z]->space, (IArg)common[z]->base);
            if (pP->fxns->allocPrint)
            {
                pP->fxns->allocPrint((const PAF_ALG_AllocInit *)(inpLinkInit[z-INPUT1]), sizeof (*(inpLinkInit[z-INPUT1])), &pafAlgConfig);
            }
        }
    }
    {
        // Changes made to share scratch between zones
        // Assume maximum 3 zones and scratch common memory is at offset 0;
        int max=0;
        for (z=STREAM1; z<STREAMN; z++)
        {
            if (max < common[z][0].size)
            {
                max = common[z][0].size;
            }
        }
        common[STREAM1][0].size=max;
        for (z=STREAM1+1; z<STREAMN; z++)
        {
            common[z][0].size = 0;            
        }
    }
        
    //
    // Allocate common memory for:
    //  (1) Logical Input drivers
    //
    for (z = STREAM1; z < STREAMN; z++) 
    {
        TRACE_TERSE0("PAF_ASIT_initPhaseCommon: calling PAF_ALG_mallocMemory for common space.");
        if (PAF_ALG_mallocMemory(common[z], &pafAlgConfig)) 
        {
            TRACE_TERSE1("PAF_ASIT_initPhaseCommon: AS%d: PAF_ALG_mallocMemory failed", as+z);
            TRACE_TERSE3("AS%d: z: %d.  Size 0x%x", as+z, z, common[z][0].size);
            SW_BREAKPOINT;
            return __LINE__;
        }
        TRACE_TERSE3("alloced %d bytes from space %d at 0x%x", common[z]->size, common[z]->space, (IArg)common[z]->base);
        // share zone0 scratch with all zones 
        common[z][0].base = common[0][0].base;
        if (pP->fxns->commonPrint)
        {
            pP->fxns->commonPrint(common[z], &pafAlgConfig);
        }

        //
        // Allocate non-common memories for Logical IO drivers
        //    Since these structures are used at run-time we allocate from external memory
        if (INPUT1 <= z && z < INPUTN) 
        {
            PAF_ASP_Chain *chain;
            TRACE_TERSE2("PAF_ASIT_initPhaseCommon: AS%d: non-common input chain init for %d",
                           as+z, z);
            chain = PAF_ASP_chainInit(&pAstCfg->xInp[z].inpChainData, pP->pChainFxns,
                        HEAP_EXTERNAL, as+z, acp, &trace,
                        inpLinkInit[z-INPUT1], NULL, common[z], &pafAlgConfig);
            if (!chain) 
            {
                TRACE_TERSE1("PAF_ASIT_initPhaseCommon: AS%d: Input chain initialization failed", as+z);
                return __LINE__;
            }
        }
    }
    TRACE_TERSE1("PAF_ASIT_initPhaseCommon: AS%d: Returning complete.", as+z);

    return 0;
} //PAF_ASIT_initPhaseCommon

// -----------------------------------------------------------------------------
// ASIT Initialization Function - Algorithm Keys
//
//   Name:      PAF_ASIT_initPhaseAlgKey
//   Purpose:   Audio Stream Input Task Function for initialization of data values
//              from parameters for Algorithm Keys.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
// .............................................................................
Int
PAF_ASIT_initPhaseAlgKey(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                    /* Audio Stream Number (1, 2, etc.) */

    
    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    (void)as;  // clear compiler warning in case not used with tracing disabled

    TRACE_VERBOSE1("PAF_ASIT_initPhaseAlgKey: AS%d: initialization phase - Algorithm Keys", as);

    return 0;
} //PAF_ASIT_initPhaseAlgKey

// -----------------------------------------------------------------------------
// ASIT Initialization Function - I/O Devices
//
//   Name:      PAF_ASIT_initPhaseDevice
//   Purpose:   Audio Stream Input Task Function for initialization of I/O Devices.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on device allocation failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//
Int
PAF_ASIT_initPhaseDevice(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                             /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* input/output counter */
    PAF_SIO_IALG_Obj    *pObj;
    PAF_SIO_IALG_Config *pAlgConfig;
    PAF_IALG_Config pafAlgConfig;


    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    (void)as; // clear compiler warning in case not used with tracing disabled

    TRACE_TERSE1("PAF_ASIT_initPhaseDevice: AS%d: initialization phase - I/O Devices", as);

    if (pP->fxns->bufMemPrint)
    {
        PAF_ALG_setup (&pafAlgConfig, 
            HEAP_ID_INTERNAL,               HEAP_INTERNAL, 
            HEAP_ID_INTERNAL1,              HEAP_INTERNAL1,
            HEAP_ID_EXTERNAL,               HEAP_EXTERNAL,
            HEAP_ID_INTERNAL1_SHM,          HEAP_INTERNAL1_SHM,
            HEAP_ID_EXTERNAL_SHM,           HEAP_EXTERNAL_SHM,
            HEAP_ID_EXTERNAL_NONCACHED_SHM, HEAP_EXTERNAL_NONCACHED_SHM,
            HEAP_CLEAR);
        TRACE_TERSE2("PAF_ASIT_initPhaseDevice: AS%d: calling PAF_ALG_setup with clear at %d.", as, HEAP_CLEAR);
    }

    for (z=INPUT1; z < INPUTN; z++) 
    {
        PAF_InpBufConfig *pConfig = &pAstCfg->xInp[z].inpBufConfig;

        pObj = (PAF_SIO_IALG_Obj *)pAstCfg->xInp[z].inpChainData.head->alg;
        pAlgConfig = &pObj->config;

        pAstCfg->xInp[z].hRxSio = NULL;

        pConfig->base.pVoid       = pAlgConfig->pMemRec[0].base;
        pConfig->pntr.pVoid       = pAlgConfig->pMemRec[0].base;
        pConfig->head.pVoid       = pAlgConfig->pMemRec[0].base;
        pConfig->futureHead.pVoid = pAlgConfig->pMemRec[0].base;
        pConfig->allocation       = pAlgConfig->pMemRec[0].size;
        pConfig->sizeofElement    = 2;
        pConfig->precision        = 16;

        if (pP->fxns->bufMemPrint)
        {
            pP->fxns->bufMemPrint(z, pAlgConfig->pMemRec[0].size, PAF_ALG_memSpaceToHeapId(&pafAlgConfig,pAlgConfig->pMemRec[0].space), 0);
        }
    }

    TRACE_TERSE1("PAF_ASIT_initPhaseDevice: AS%d: initialization phase - I/O Devices complete.", as);

    return 0;
} //PAF_ASIT_initPhaseDevice

// -----------------------------------------------------------------------------
// ASIT Initialization Function - Decoder Output Circular Buffer
//
//   Name:      PAF_ASIT_initPhaseDecOpCircBuf
//   Purpose:   Audio Stream Input Task Function for initialization of Decoder Output Circular Buffer.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on device allocation failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//
Int
PAF_ASIT_initPhaseDecOpCircBuf(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
)
{
    PAF_AST_Config *pAstCfg;
    PAF_AST_DecOpCircBuf *pCb;          /* Decoder output circular buffer */
    Int as;                             /* Audio Stream Number (1, 2, etc.) */
    Int zMS;
    Int z;                              /* decode counter */
    Int errno;                          /* error number */
    Error_Block    eb;
    Int i;
    ACP_Handle acp;
    Int betaPrimeOffset;
    Int zS;

    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    zMS = pAstCfg->masterStr;

    // Decode output circular buffer memory
    if (!(pAstCfg->xDecOpCb = Memory_calloc((IHeap_Handle)HEAP_EXTERNAL_NONCACHED_SHM,
        DECODEN * sizeof (*pAstCfg->xDecOpCb), 4, &eb)))
    {
        TRACE_TERSE1("PAF_ASIT_initPhaseDecOpCircBuf: AS%d: Memory_calloc failed", as+zMS);
        SW_BREAKPOINT;
        return __LINE__;
    }

    TRACE_TERSE3("PAF_ASIT_initPhaseDecOpCircBuf. (pAsitCfg->xDecOpCb) %d bytes from space %d at 0x%x.",
        DECODEN * sizeof (*pAstCfg->xDecOpCb),
        HEAP_ID_INTERNAL1_SHM, (IArg)pAstCfg->xDecOpCb);

    for (z=DECODE1; z < DECODEN; z++)
    {
        pCb = &pAstCfg->xDecOpCb[z];
        
        // allocate audio frame circular buffer
        if (!(pCb->afCb = Memory_calloc((IHeap_Handle)HEAP_INTERNAL1_SHM, ASP_DECOP_CB_MAX_NUM_AF * sizeof(PAF_AudioFrame), 4, &eb)))
        {
            TRACE_TERSE1("PAF_ASIT_initPhaseDecOpCircBuf: AS%d: Memory_calloc failed", as+zMS);
            SW_BREAKPOINT;
        }
        // allocate audio frame PCM sample pointer array
        for (i = 0; i<ASP_DECOP_CB_MAX_NUM_AF; i++)
        {
            if (!(pCb->afCb[i].data.sample = Memory_calloc((IHeap_Handle)HEAP_INTERNAL1_SHM, ASP_DECOP_CB_MAX_NUM_PCM_CH_MAT * sizeof(PAF_AudioData *), 4, &eb)))
            {
                TRACE_TERSE1("PAF_ASIT_initPhaseDecOpCircBuf: AS%d: Memory_calloc failed", as+zMS);
                SW_BREAKPOINT;
            }
            if (!(pCb->afCb[i].data.samsiz = Memory_calloc((IHeap_Handle)HEAP_INTERNAL1_SHM, ASP_DECOP_CB_MAX_NUM_PCM_CH_MAT * sizeof(PAF_AudioSize), 4, &eb)))
            {
                TRACE_TERSE1("PAF_ASIT_initPhaseDecOpCircBuf: AS%d: Memory_calloc failed", as+zMS);
                SW_BREAKPOINT;
            }                
        }
        // allocate PCM sample buffer
        if (!(pCb->pcmBuf = Memory_calloc((IHeap_Handle)HEAP_EXTERNAL_SHM, ASP_DECOP_CB_PCM_BUF_SZ * sizeof(PAF_AudioData), 4, &eb)))
        {
            TRACE_TERSE1("PAF_ASIT_initPhaseDecOpCircBuf: AS%d: Memory_calloc failed", as+zMS);
            SW_BREAKPOINT;
        }
        pCb->pcmBufEnd = pCb->pcmBuf + ASP_DECOP_CB_PCM_BUF_SZ;
        // allocate Metadata buffers
        if (!(pCb->metaBuf = Memory_calloc((IHeap_Handle)HEAP_EXTERNAL_SHM, ASP_DECOP_CB_MAX_NUM_AF * PAF_MAX_PRIVATE_MD_SZ * PAF_MAX_NUM_PRIVATE_MD * sizeof(Int8), 4, &eb)))
        {
            TRACE_TERSE1("PAF_ASIT_initPhaseDecOpCircBuf: AS%d: Memory_calloc failed", as+zMS);
            SW_BREAKPOINT;
        }
        pCb->metaBufEnd = pCb->metaBuf + (ASP_DECOP_CB_MAX_NUM_AF * PAF_MAX_PRIVATE_MD_SZ * PAF_MAX_NUM_PRIVATE_MD);

        #ifdef CB_RW_OP_CAP_PP // debug
        // allocate debug buffer
        if (!(pCb->cb_samples_op = Memory_calloc((IHeap_Handle)HEAP_EXTERNAL_NONCACHED_SHM, CB_OP_COUNT_MAX * sizeof(UInt32), 4, &eb)))
        {
            TRACE_TERSE1("PAF_ASIT_initPhaseDecOpCircBuf: AS%d: Memory_calloc failed", as+zMS);
            SW_BREAKPOINT;
        }

        // allocate debug buffer
        if (!(pCb->cb_op_owner = Memory_calloc((IHeap_Handle)HEAP_EXTERNAL_NONCACHED_SHM, CB_OP_COUNT_MAX * sizeof(UInt8), 4, &eb)))
        {
            TRACE_TERSE1("PAF_ASIT_initPhaseDecOpCircBuf: AS%d: Memory_calloc failed", as+zMS);
            SW_BREAKPOINT;
        }

        // allocate debug buffer
        if (!(pCb->cb_afRdIdx = Memory_calloc((IHeap_Handle)HEAP_EXTERNAL_NONCACHED_SHM, CB_OP_COUNT_MAX * sizeof(UInt8), 4, &eb)))
        {
            TRACE_TERSE1("PAF_ASIT_initPhaseDecOpCircBuf: AS%d: Memory_calloc failed", as+zMS);
            SW_BREAKPOINT;
        }

        // allocate debug buffer
        if (!(pCb->cb_afWrtIdx = Memory_calloc((IHeap_Handle)HEAP_EXTERNAL_NONCACHED_SHM, CB_OP_COUNT_MAX * sizeof(UInt8), 4, &eb)))
        {
            TRACE_TERSE1("PAF_ASIT_initPhaseDecOpCircBuf: AS%d: Memory_calloc failed", as+zMS);
            SW_BREAKPOINT;
        }

        // allocate debug buffer
        if (!(pCb->cb_numAfCb = Memory_calloc((IHeap_Handle)HEAP_EXTERNAL_NONCACHED_SHM, CB_OP_COUNT_MAX * sizeof(UInt8), 4, &eb)))
        {
            TRACE_TERSE1("PAF_ASIT_initPhaseDecOpCircBuf: AS%d: Memory_calloc failed", as+zMS);
            SW_BREAKPOINT;
        }
        #endif
    }

    for (z = DECODE1; z < DECODEN; z++)
    {
        //
        // Initialize decoder output circular
        //
        
        // Initialize status
        pAstCfg->xDecOpCb[z].cbStatus = *pP->z_pDecOpCircBufStatus[z];
        
        // Default initialization
        errno = cbInit(pCb);
        if (errno)
        {
            //SW_BREAKPOINT; // debug
            return errno;
        }
    }
    
    // Get ASIT ACP handle
    acp = pAsitCfg->acp;
    if (!acp)
    {
        TRACE_TERSE1("PAF_ASIT_initPhaseDecOpCircBuf: AS%d: ACP algorithm instance creation  failed", as+zMS);
        return __LINE__;
    }
    
    // Get Beta Prime offset
    ((ALG_Handle)acp)->fxns->algControl((ALG_Handle) acp,
        ACP_GETBETAPRIMEOFFSET, (IALG_Status *)&betaPrimeOffset);
            
    for (z = DECODE1; z < DECODEN; z++)
    {
        // ACP attach CB
        zS = pP->streamsFromDecodes[z];
        acp->fxns->attach(acp, ACP_SERIES_STD,
            STD_BETA_DECOPCB + betaPrimeOffset * (as-1+zS),
            (IALG_Status *)&pAstCfg->xDecOpCb[z].cbStatus);        
    }
            
    return 0;
} //PAF_ASIT_initPhaseDecOpCircBuf


// -----------------------------------------------------------------------------
// ASIT Initialization Function - Output Init-Sync
//
//   Name:      PAF_ASIT_initPhaseOutIS
//   Purpose:   Audio Stream Input Task Function for initialization of Output Init-Sync.
//
Int
PAF_ASIT_initPhaseOutIS(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                             /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode counter */
    PAF_AST_OutInitSyncInfo *pOutISI;
    Error_Block    eb;
    Int i;


    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    
    /* Output Init-Sync memory */
    if (!(pAstCfg->xOutIsi = Memory_calloc((IHeap_Handle)HEAP_EXTERNAL_NONCACHED_SHM, 
        DECODEN * sizeof (*pAstCfg->xOutIsi), 4, &eb)))
    {
        TRACE_TERSE1("PAF_ASIT_initPhaseOutIS: AS%d: Memory_calloc failed", as);
        SW_BREAKPOINT;
        return __LINE__;
    }

    // Reset all decoder stage flags
    for (z=DECODE1; z < DECODEN; z++)
    {
        // Get address of Output Init-Sync Info
        pOutISI = &pAstCfg->xOutIsi[z];
        for (i = 0; i < ASP_OUTIS_NUM_DEC_STAGES; i++)
        {
            // Reset flag value
            pOutISI->decStageOutInitSyncInfo[i].decFlag = 0;
        }
    }    
    
    return 0;
} // PAF_ASIT_initPhaseOutIS


#if 0
// -----------------------------------------------------------------------------
// AST Processing Function - Pass-Through Processing
//
//   Name:      PAF_AST_passProcessing
//   Purpose:   Audio Stream Task Function for processing audio data as a
//              pass-through from the input driver to the output driver
//              for development and testing.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information on initialization.
//              * State information on processing.
//

#pragma CODE_SECTION(PAF_AST_passProcessing,".text:_PAF_AST_passProcessing")
/* Pass Processing is often omitted from builds to save memory, */
/* and CODE_SECTION/clink constructs facilitate this omission.  */

Int
PAF_AST_passProcessing(const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int hack)
{
    Int z;                              /* input/output counter */
    Int errno = 0;                      /* error number */
    Int getVal;
    Int rxNumChan, txNumChan;
    Int first;
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;


#ifndef __TI_EABI__
    asm (" .clink"); /* See comment above regarding CODE_SECTION/clink. */
#endif    

    TRACE_VERBOSE0("PAF_AST_passProcessing: initializing");

    //
    // Determine that receive/transmit channels are compatible
    //

    // Can handle handle only master input
    for (z=INPUT1; z < INPUTN; z++) {
        if (z != zMI && pC->xInp[z].hRxSio)
            return (ASPERR_PASS + 0x01);
    }

    /* Number of receive/transmit channels */

    if (! pC->xInp[zMI].hRxSio)
        return (ASPERR_PASS + 0x11);
    if (SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_NUMCHANNELS, (Arg) &rxNumChan))
        return (ASPERR_PASS + 0x12);
    if (rxNumChan > NUM_TX_CHAN(zMI))
        return (ASPERR_PASS + 0x13);

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (! pC->xOut[zMI].hTxSio)
            return (ASPERR_PASS + 0x10*(z+2) + 0x01);
        if (SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_GET_NUMCHANNELS, (Arg) &txNumChan))
            return (ASPERR_PASS + 0x10*(z+2) + 0x02);
        if (txNumChan > NUM_TX_CHAN(zMI))
            return (ASPERR_PASS + 0x10*(z+2) + 0x03);
    }

    //
    // Set up receive/transmit
    //

    SIO_idle (pC->xInp[zMI].hRxSio);
    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if(SIO_idle (pC->xOut[z].hTxSio))
            return ASPERR_IDLE;
    }

    if (SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_SET_SOURCESELECT, PAF_SOURCE_PCM))
        return (ASPERR_PASS + 0x14);

    if (SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_SET_PCMFRAMELENGTH, FRAMELENGTH))
        return (ASPERR_PASS + 0x15);

    for (z=OUTPUT1; z < OUTPUTN; z++)
        pC->xOut[z].outBufConfig.lengthofFrame = FRAMELENGTH;

    if (SIO_issue (pC->xInp[zMI].hRxSio, &pC->xInp[zMI].inpBufConfig, sizeof (pC->xInp[zMI].inpBufConfig), PAF_SIO_REQUEST_SYNC))
        return ASPERR_PASS + 0x16;

    if (SIO_reclaim (pC->xInp[zMI].hRxSio, (Ptr)&pC->xInp[zMI].pInpBuf, NULL) != sizeof (PAF_InpBufConfig))
        return ASPERR_PASS + 0x17;

    //
    // Receive and transmit the data in single-frame buffers
    //

    first = 1;
    while (pC->xDec[zMD].decodeStatus.sourceSelect == PAF_SOURCE_PASS) {
        PAF_OutBufConfig *pOutBuf;
        PAF_InpBufConfig *pInpBuf;

        if (first) {
            first = 0;

            TRACE_VERBOSE0("PAF_AST_passProcessing: starting output");

            for (z=OUTPUT1; z < OUTPUTN; z++) {
                getVal = SIO_issue (pC->xOut[z].hTxSio, &pC->xOut[z].outBufConfig, sizeof(pC->xOut[z].outBufConfig), 0);
                if (getVal > 0) {
                    errno = ASPERR_ISSUE;
                    break;
                }
                else if (getVal < 0) {
                    errno = -getVal;
                    break;
                }

                if (getVal = SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_UNMUTE, 0))
                    return (getVal & 0xff) | ASPERR_MUTE;
            }
            if (errno)
                break;

        }

        getVal = SIO_issue (pC->xInp[zMI].hRxSio, &pC->xInp[zMI].inpBufConfig, sizeof (pC->xInp[zMI].inpBufConfig), PAF_SIO_REQUEST_NEWFRAME);
        if (getVal > 0) {
            errno = ASPERR_ISSUE;
            break;
        }

        TRACE_VERBOSE1("PAF_AST_passProcessing: awaiting frame -- input size %d", rxNumChan * FRAMELENGTH);

        getVal = SIO_reclaim (pC->xInp[zMI].hRxSio, (Ptr) &pInpBuf, NULL);
        if (getVal < 0) {
            errno = -getVal;
            break;
        }

        for (z=OUTPUT1; z < OUTPUTN; z++) {
            getVal = SIO_reclaim (pC->xOut[z].hTxSio, (Ptr) &pOutBuf, NULL);
            if (getVal < 0) {
                errno = -getVal;
                break;
            }
        }
        if( errno )
            break;

        TRACE_VERBOSE0("PAF_AST_passProcessing: copying frame");

        if (errno = pP->fxns->passProcessingCopy (pP, pQ, pC))
            break;

        for (z=OUTPUT1; z < OUTPUTN; z++) {
            getVal = SIO_issue (pC->xOut[z].hTxSio, &pC->xOut[z].outBufConfig, sizeof(pC->xOut[z].outBufConfig), 0);
            if (getVal > 0) {
                errno = ASPERR_ISSUE;
                break;
            }
            else if (getVal < 0) {
                errno = -getVal;
                break;
            }
        }
        if( errno )
            break;
    }

    //
    // Close down receive/transmit
    //

    TRACE_TERSE0("PAF_AST_passProcessing: finalizing"));

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (getVal = SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_MUTE, 0)) {
            if (! errno)
                errno = (getVal & 0xff) | ASPERR_MUTE;
            /* convert to sensical errno */
        }
    }

    SIO_idle (pC->xInp[zMI].hRxSio);
    for (z=OUTPUT1; z < OUTPUTN; z++)
        SIO_idle (pC->xOut[z].hTxSio);

    return errno;

} //PAF_AST_passProcessing
#endif // #if 0

#if 0
// -----------------------------------------------------------------------------
// AST Processing Function Helper - Pass-Through Processing Patch Point
//
//   Name:      PAF_AST_passProcessingCopy
//   Purpose:   Pass-Through Processing Function for copying audio data
//              from the input buffer to the output buffer.
//   From:      AST Parameter Function -> passProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              x
//

#pragma CODE_SECTION(PAF_AST_passProcessingCopy,".text:_PAF_AST_passProcessingCopy")
/* Pass Processing is often omitted from builds to save memory, */
/* and CODE_SECTION/clink constructs facilitate this omission.  */

Int
PAF_AST_passProcessingCopy (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int z;                              /* output counter */
    Int errno;                          /* error number */
    Int i;
    Int rxNumChan, txNumChan;
    Int zMI = pP->zone.master;


    asm (" .clink"); /* See comment above regarding CODE_SECTION/clink. */

    // Copy data from input channels to output channels one of two ways:

    if (SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_NUMCHANNELS, (Arg) &rxNumChan))
        return (ASPERR_PASS + 0x12);

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_GET_NUMCHANNELS, (Arg) &txNumChan))
            return (ASPERR_PASS + 0x22);

        if( txNumChan <= rxNumChan ) {

            // Copy one to one, ignoring later rx channels as needed.

            for( i=0; i < txNumChan; i++ ) {
                errno = pP->fxns->copy( i, &pC->xInp[zMI].inpBufConfig, i, &pC->xOut[z].outBufConfig );
                if( errno )
                    return errno;
            }
        }
        else {

            // Copy one to many, repeating earlier rx channels as needed.

            Int from, to;

            from = 0;
            to   = 0;
            while( to < txNumChan ) {
                errno = pP->fxns->copy( from, &pC->xInp[zMI].inpBufConfig, to, &pC->xOut[z].outBufConfig );
                if( errno )
                    return errno;

                from++;
                to++;
                if( from == rxNumChan )
                    from = 0;
            }
        }
    }

    return 0;
} //PAF_AST_passProcessingCopy
#endif // #if 0

// -----------------------------------------------------------------------------
// ASIT Processing Function - Auto Processing
//
//   Name:      PAF_ASIT_autoProcessing
//   Purpose:   Audio Stream Input Task Function for processing audio data to
//              determine the input type without output.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information on initialization.
//

#define DECSIOMAP(X)                                                \
    pP->pDecSioMap->map[(X) >= pP->pDecSioMap->length ? 0 : (X)]

Int
PAF_ASIT_autoProcessing(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int inputTypeSelect, 
    ALG_Handle pcmAlgMaster
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                 /* Audio Stream Number (1, 2, etc.) */
    Int errno = 0;          /* error number */
    Int nbytes;
    Int frameLength;
    Int zMD;
    Int zMI;
    Int zMS;
    
    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    zMD = pAstCfg->masterDec;
    zMS = pAstCfg->masterStr;    
    zMI = pP->zone.master;    
    
    TRACE_VERBOSE1("PAF_ASIT_autoProcessing: AS%d: PAF_AST_autoProcessing", as+zMS);

    if (errno = SIO_ctrl(pAstCfg->xInp[zMI].hRxSio,
                         PAF_SIO_CONTROL_SET_SOURCESELECT,
                         DECSIOMAP(pAstCfg->xDec[zMD].decodeStatus.sourceSelect)))
    {
        TRACE_VERBOSE2("PAF_ASIT_autoProcessing: AS%d: source select returns 0x%x", as+zMS, errno);
        return errno;
    }
    
    frameLength = FRAMELENGTH; // fix PCM frameLength
    if (errno = SIO_ctrl(pAstCfg->xInp[zMI].hRxSio,
                         PAF_SIO_CONTROL_SET_PCMFRAMELENGTH, frameLength))
    {
        TRACE_VERBOSE2("PAF_ASIT_autoProcessing: SET_PCMFRAMELENGTH returns 0x%x, returning ASPERR_AUTO_LENGTH, 0x%x",
            errno, ASPERR_AUTO_LENGTH);
        return ASPERR_AUTO_LENGTH;
    }

    if (errno = SIO_issue(pAstCfg->xInp[zMI].hRxSio,
                          &pAstCfg->xInp[zMI].inpBufConfig, sizeof(pAstCfg->xInp[zMI].inpBufConfig),
                          PAF_SIO_REQUEST_SYNC))
    {
        TRACE_VERBOSE2("PAF_ASIT_autoProcessing: REQUEST_SYNC returns 0x%x, returning ASPERR_ISSUE, 0x%x",
            errno, ASPERR_ISSUE);
        return ASPERR_ISSUE;
    }

    TRACE_VERBOSE1("PAF_ASIT_autoProcessing: AS%d: awaiting sync", as+zMS);

    // all of the sync scan work is done in this call. If the error returned
    // is DIBERR_SYNC then that just means we didn't find a sync, not a real I/O
    // error so we mask it off.
    nbytes = SIO_reclaim(pAstCfg->xInp[zMI].hRxSio, (Ptr)&pAstCfg->xInp[zMI].pInpBuf, NULL);
    if (nbytes == -DIBERR_SYNC)
    {
#if 0 // debug
        // Shows timing of autosync restart
        // ADC B5
        {
            static Uint8 toggleState = 0;
            if (toggleState == 0)
                GPIOSetOutput(GPIO_PORT_0, GPIO_PIN_99);
            else
                GPIOClearOutput(GPIO_PORT_0, GPIO_PIN_99);
            toggleState = ~(toggleState);
        }
#endif                
        TRACE_TERSE2("PAF_ASIT_autoProcessing: AS%d: SIO_reclaim returned 0x%x, ignoring", as+zMS, nbytes);
        return nbytes;
    }
    if (nbytes != sizeof(PAF_InpBufConfig))
    {
        TRACE_TERSE3("PAF_ASIT_autoProcessing. SIO_reclaim returned %d, not %d, returning ASPERR_RECLAIM (0x%x)",
            nbytes, sizeof(PAF_InpBufConfig), ASPERR_RECLAIM);
        return ASPERR_RECLAIM;
    }
    if (errno)
    {
        TRACE_TERSE2("PAF_ASIT_autoProcessing: AS%d: returning errno 0x%x", as+zMS, errno);
    }
    return errno;
} //PAF_ASIT_autoProcessing

UInt32 gProbe1Err=0;
UInt32 gProbe2Err=0;
UInt32 gMajorAuMissed=0;

// -----------------------------------------------------------------------------
// ASIT Processing Function - Decode Processing
//
//   Name:      PAF_ASIT_decodeProcessing
//   Purpose:   Audio Stream Input Task Function for processing audio data.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information on initialization (via children).
//              * State information on processing (via children).
//              * Decode warnings.
//
// When "writeDECModeContinuous" is used for zMI input/decode:
// PAF_AST_decodeProcessing() loop may be (is designed to be) exited:
// (a) if "writeDECCommandRestart" is used
//    (or "writeDECCommandAbort", but this performs no cleanup whatsoever, and so its use is discouraged)
// (b) if "writeDECSourceSelectNone" is used
// [ the latter events lead to QUIT state, simply for exiting (errme = errno = ASPERR_QUIT)
// (c) if an error occurs in
//     INIT
//     CONT ("subsequent block state", which "Establish[es] secondary timing")
//         -> PAF_AST_decodeCont(): "Await slave inputs"
//     STREAM (errno |= PAF_COMPONENT_ASP)
//     ENCODE (errno |= PAF_COMPONENT_ENCODE)
// [ the latter errors lead to "switch_break:"
//         -> PAF_AST_decodeComplete(), which always returns 0 (no error) ]
//
// [ Notably, in FINAL ("frame-finalization state")
//         -> PAF_AST_decodeFinalTest() is *not* called,
//   and so any other (asynchronous) changes in pC->xDec[zMD].decodeStatus.sourceSelect are ignored. ]
// [ For completeness, note also: "default" state, internal check (errme = errno = ASPERR_UNKNOWNSTATE) ]
//
// States in which error can't occur:
//     AGAIN ("subsequent initial state")
//
// States in which (some) errors must be handled:
//     INFO1 ("first frame state")
//         -> PAF_AST_decodeInfo(): pass on ASPERR_INFO_RATECHANGE, ASPERR_INFO_PROGRAM ("bad" internal error)
//            -> *DONE* must "catch" ASPERR_RECLAIM from SIO_reclaim (pC->xInp[zMI].hRxSio) -- note zMI only **
//               ?*? but what about ASPERR_RESYNC from same call ?*?
//            -> *for now, at least, pass on error from pP->fxns->updateInputStatus ()*
//            -> *DONE* must "catch" error from (zMI) dec->fxns->info() **
//         -> PAF_AST_decodeInfo1(): pass on any errors which occur here:
//            - pP->fxns->streamChainFunction (... PAF_ASP_CHAINFRAMEFXNS_RESET)
//            - enc->fxns->info()
//            - pP->fxns->setCheckRateX()
//            - pP->fxns->startOutput()
//            - "Start slave inputs if necessary"
//     INFO2 ("subsequent frame state")
//         -> PAF_AST_decodeInfo(): (see above)
//         -> PAF_AST_decodeInfo2(): pass on any errors which occur here:
//            - pP->fxns->setCheckRateX()
//     TIME ("timing state")
//         -> PAF_AST_decodeTime(): "Special timing consideations for AC-3"
//         -> performs SIO_issue (... PAF_SIO_REQUEST_FULLFRAME) & SIO_reclaim() *for zMI only*
//         -> now, DIB_issue [PAF_SIO_REQUEST_FULLFRAME] would only return SYS_EINVAL for "bad" internal error
//            (*OK* don't try to recover from this*)
//         -> much more likely would be SIO_reclaim() error (ASPERR_RECLAIM)
//         -> *DONE* must "catch" (just) ASPERR_RECLAIM error -- note zMI only,
//            possibly in PAF_AST_decodeProcessing() itself **
//     DECODE ("decode state")
//         -> PAF_AST_decodeDecode(): pass on error from
//            - PAF_SIO_CONTROL_GET_NUM_REMAINING ("bad" internal error)
//            - dec->fxns->reset()
//            - PAF_SIO_CONTROL_SET_PCMFRAMELENGTH
//         -> *DONE* must catch error from (zMI) dec->fxns->decode()
//         -> *?* must catch ASPERR_ISSUE from (zMI) SIO_issue()
Int
PAF_ASIT_decodeProcessing(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int sourceSelect
)
{
    PAF_AST_Config *pAstCfg;
    //Int as = pAsitCfg->as;              /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode counter */
    Int errno;                          /* error number */
    Int getVal;
    enum { INIT, INFO1, INFO2, DECODE, FINAL, QUIT } state;
    ALG_Handle alg[DECODEN_MAX];
    Int zMD;
    //Int zMS;
    Int frame; // decoder input frame count
    Int block; // decoder output block count / input frame
    Int8 tempVar8, temp2Var8;
    
    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    zMD = pAstCfg->masterDec;
    //zMS = pAstCfg->masterStr;
        
    for (z=DECODE1; z < DECODEN; z++)
    {
        alg[z] = pAstCfg->xDec[z].decAlg[PAF_SOURCE_PCM];        
    }
    alg[zMD] = NULL; // decAlgMaster; // alg[] init is on slave

    //
    // Receive and process the data in single-frame buffers
    //

    state = INIT;
    errno = 0; /* error number */

    tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceSelect), 
                                 GATEMP_INDEX_DEC);
    TRACE_TERSE1("PAF_ASIT_decodeProcessing: sourceSelect is %d", tempVar8);

    for (;;) 
    {
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceSelect), 
                                     GATEMP_INDEX_DEC);
        if (tempVar8 == PAF_SOURCE_NONE)
        {
            TRACE_VERBOSE0("PAF_ASIT_decodeProcessing: sourceSelect == PAF_SOURCE_NONE");
            state = QUIT;
        }

        // Process commands (decode)
        getVal = pP->fxns->decodeCommand(pP, pQ, pAsitCfg);
        if (getVal) 
        {
            if (state != INIT)   // no need to restart/abort if not yet started
            {
                if (getVal == ASPERR_QUIT)
                {
                    state = QUIT;
                    TRACE_VERBOSE0("PAF_ASIT_decodeProcessing. %d: state = QUIT");
                }
                else if (getVal == ASPERR_ABORT)
                {
                    TRACE_VERBOSE0("PAF_ASIT_decodeProcessing. %d: return getVal");
                    
                    // PFP end -- outside of PFP for errors, EOS, or Input SIO change
                    pfpEnd(PFP_ID_ASIT_2, PFP_FINISH_MEAS);
                    gNumPfpAsit2--;
                    
                    return getVal;
                }
                else
                {
                    /* ignore */;
                }
            }
            TRACE_VERBOSE0("PAF_ASIT_decodeProcessing: state == INIT");
        }

        TRACE_TIME((&TIME_MOD,         "... + %d = %d ->", dtime(), TSK_time()));
        TRACE_TIME((&TIME_MOD,         "                 state = %s", stateName[state]));

        // Process state (decode)
        switch (state) 
        {
            case INIT: // initial state
                gAsipInitCnt++;
                Log_info0("TaskAsip: state=INIT");               

                errno = pP->fxns->decodeInit(pP, pQ, pAsitCfg, sourceSelect);
                if (errno)
                {
                    TRACE_VERBOSE1("PAF_ASIT_decodeProcessing: INIT, errno 0x%x.  break after decodeInit", errno);
                    break;
                }
                               
                frame = 0;
                block = 0;

                TRACE_VERBOSE0("PAF_ASIT_decodeProcessing: state: INIT->INFO1");
                state = INFO1;
                continue;

            case INFO1: // first frame state
                gAsipInfo1Cnt++;
                Log_info0("TaskAsip: state=INFO1");
                
                // Establish primary timing
                errno = pP->fxns->decodeInfo(pP, pQ, pAsitCfg, frame, block);
                if (errno)
                {
                    gAsipInfo1_PrimaryErrCnt++;
                    TRACE_TERSE1("INFO1: errno 0x%x after decodeInfo, primary timing", errno);
                    break;
                }
                
                tempVar8  = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceDecode),
                                              GATEMP_INDEX_DEC);
                temp2Var8 = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sampleRate), 
                                              GATEMP_INDEX_DEC);
                // Don't start decode until major access unit is found.
                if (((tempVar8 == PAF_SOURCE_THD)    ||
                     (tempVar8 == PAF_SOURCE_DXP)    ||
                     (tempVar8 == PAF_SOURCE_DTSHD)) &&
                     (temp2Var8 == PAF_SAMPLERATE_UNKNOWN)) 
                {
                    Int z;
                    for (z=DECODE1; z < DECODEN; z++) 
                    {
                        Int zI = pP->inputsFromDecodes[z];
                        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode), 
                                                     GATEMP_INDEX_DEC);
                        if (pAstCfg->xInp[zI].hRxSio && tempVar8)
                        {
                            TRACE_VERBOSE0("TaskAsip: PAF_ASIT_decodeProcessing: INFO1, SIO_issue");
                            if (SIO_issue(pAstCfg->xInp[zI].hRxSio, &pAstCfg->xInp[zI].inpBufConfig,
                                sizeof(pAstCfg->xInp[zI].inpBufConfig), PAF_SIO_REQUEST_NEWFRAME))
                            {
                                gProbe2Err++;
                                TRACE_TERSE0("PAF_ASIT_decodeProcessing. %d: INFO1, return (ASPERR_ISSUE)");
                                
                                // PFP end -- outside of PFP for errors, EOS, or Input SIO change
                                pfpEnd(PFP_ID_ASIT_2, PFP_FINISH_MEAS);
                                gNumPfpAsit2--;
                                
                                return (ASPERR_ISSUE);
                            }
                        }
                    }
                    TRACE_VERBOSE1("PAF_ASIT_decodeProcessing: INFO1: frame %d, not major access unit", frame);
                    gMajorAuMissed++; // debug
                    frame++;
                    state = INFO1;
                    continue;
                }
                TRACE_VERBOSE1("PAF_ASIT_decodeProcessing: INFO1: frame %d, major access unit found", frame);

                // Establish secondary timing
                errno = pP->fxns->decodeInfo1(pP, pQ, pAsitCfg, frame, block);
                if (errno)
                {
                    TRACE_VERBOSE1("PAF_ASIT_decodeProcessing: INFO1, errno 0x%x.  break after decodeInfo1", errno);
                    gAsipInfo1_ErrCnt++;
                    break;
                }

                TRACE_VERBOSE0("PAF_ASIT_decodeProcessing: state: INFO1->DECODE");
                state = DECODE;
                continue;

            case INFO2: // subsequent frame state
                gAsipInfo2Cnt++;
                Log_info0("TaskAsip: state=INFO2");

                // Establish primary timing
                errno = pP->fxns->decodeInfo(pP, pQ, pAsitCfg, frame, block);
                if (errno)
                {
                    TRACE_TERSE1("PAF_ASIT_decodeProcessing: INFO2 break on decodeInfo. errno 0x%x", errno);
                    gAsipInfo2_PrimaryErrCnt++;
                    break;
                }
                                
                // Measure cycles in decode processing loop.
                // Only measures cycles spent in loop.
                pfpEnd(PFP_ID_ASIT_2, PFP_FINISH_MEAS);         // PFP end
                gNumPfpAsit2--;
                pfpBegin(PFP_ID_ASIT_2, pAsitCfg->taskHandle);  // PFP begin
                gNumPfpAsit2++;

                errno = pP->fxns->decodeInfo2(pP, pQ, pAsitCfg, frame, block);
                if (errno)
                {
                    TRACE_TERSE1("PAF_ASIT_decodeProcessing. %d: INFO2 break on decodeInfo2. errno 0x%x", errno);
                    gAsipInfo2_ErrCnt++;
                    break;
                }
               
                TRACE_VERBOSE0("PAF_ASIT_decodeProcessing: state: INFO2->DECODE");
                state = DECODE;
                continue;

            case DECODE: // decode state
                gAsipDecodeCnt++;
                Log_info0("TaskAsip: state=DECODE");

                errno = pP->fxns->decodeDecode(pP, pQ, pAsitCfg, sourceSelect, frame, block);
                if (errno)
                {
                    gAsipDecodeErrCnt++;
                    TRACE_TERSE1("PAF_ASIT_decodeProcessing: state: DECODE.  decodeDecode err 0x%04x", errno);
                    break;
                }
                
                TRACE_VERBOSE0("PAF_ASIT_decodeProcessing: state: DECODE->FINAL");
                state = FINAL;
                continue;

            case FINAL: // frame-finalization state
                gAsipFinalCnt++;
                Log_info0("TaskAsip: state=FINAL");

                // Check for final frame, and if indicated:
                // - Exit state machine to "complete" processing.
                if (pP->fxns->decodeFinalTest(pP, pQ, pAsitCfg, frame, block)) 
                {
                    break;
                }

                frame++;
                TRACE_VERBOSE0("PAF_ASIT_decodeProcessing: state: FINAL->AGAIN");
                state = INFO2;
                continue;

            case QUIT: // exit state
                gAsipQuitCnt++;
                Log_info0("TaskAsip: state=QUIT");

                // Quit:
                // - Set error number registers.
                // - Exit state machine to "decode complete" processing.
                TRACE_VERBOSE0("PAF_ASIT_decodeProcessing: state: QUIT");
                errno = ASPERR_QUIT;
                break;

            default: // unknown state

                // Unknown:
                // - Set error number registers.
                // - Exit state machine to "decode complete" processing.

                TRACE_TERSE1("PAF_ASIT_decodeProcessing: state: unknown, 0x%x", state);
                errno = ASPERR_UNKNOWNSTATE;
                break;

        }  // End of switch (state).

        TRACE_VERBOSE0("PAF_ASIT_decodeProcessing: Calling decode complete");
        if (pP->fxns->decodeComplete(pP, pQ, pAsitCfg, alg, frame, block))
        {
            /* ignored? */;
        }
        
        TRACE_TIME((&TIME_MOD, "as1-f2: ... + %d = ?? (final? %d)", dtime(), state == FINAL));

        // PFP end -- outside of PFP for errors, EOS, or Input SIO change
        //pfpEnd(PFP_ID_ASIT_2, PFP_FINISH_MEAS);
        //gNumPfpAsit2--;
        
        //return errno;
        break;
    }  // End of for (;;) to Receive, process, and transmit the data.
    
    // PFP end -- outside of PFP for errors, EOS, or Input SIO change
    pfpEnd(PFP_ID_ASIT_2, PFP_FINISH_MEAS);
    gNumPfpAsit2--;
    
    return errno;
} //PAF_ASIT_decodeProcessing

// -----------------------------------------------------------------------------
// ASIT Decoding Function - Decode Command Processing
//
//   Name:      PAF_ASIT_decodeCommand
//   Purpose:   Decoding Function for processing Decode Commands.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * Command execution.
//

Int
PAF_ASIT_decodeCommand(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                 /* Audio Stream Number (1, 2, etc.) */
    Int z;                  /* decode counter */
    Int zS;
    Int8 tempVar8;

    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;

    for (z=DECODE1; z < DECODEN; z++) 
    {
        zS = pP->streamsFromDecodes[z];
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.command2), 
                                     GATEMP_INDEX_DEC);
        if (!(tempVar8 & 0x80))
        {
            switch (tempVar8)
            {
                case 0: // command none - process
                    tempVar8 |= 0x80;
                    sharedMemWriteInt8(&(pAstCfg->xDec[z].decodeStatus.command2),
                                       tempVar8, GATEMP_INDEX_DEC);
                    break;
                case 1: // command abort - leave now
                    TRACE_TERSE2("AS%d: PAF_ASIT_decodeCommand: decode command abort (0x%02x)", as+zS, 1);
                    tempVar8 |= 0x80;
                    sharedMemWriteInt8(&(pAstCfg->xDec[z].decodeStatus.command2),
                                       tempVar8, GATEMP_INDEX_DEC);
                    return (ASPERR_ABORT);
                case 2: // command restart - leave later
                    TRACE_TERSE2("AS%d: PAF_ASIT_decodeCommand: decode command quit (0x%02x)", as+zS, 2);
                    tempVar8 |= 0x80;
                    sharedMemWriteInt8(&(pAstCfg->xDec[z].decodeStatus.command2),
                                       tempVar8, GATEMP_INDEX_DEC);
                                       
                    // Invoke command to output task to break the loop
                    gCommandOutputTask_SYNC = 1;
                    
                    return (ASPERR_QUIT);
                default: // command unknown - ignore
                    break;
            }
        }
    }

    return 0;
} //PAF_ASIT_decodeCommand

// -----------------------------------------------------------------------------
// ASIT Decoding Function - Reinitialization of Decode
//
//   Name:      PAF_ASIT_decodeInit
//   Purpose:   Decoding Function for reinitializing the decoding process.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
Int
PAF_ASIT_decodeInit(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int sourceSelect
)
{
    PAF_AST_Config *pAstCfg;
    //PAF_AST_DecOpCircBufCtl *pCbCtl;    /* Decoder output circular buffer control */
    AspMsgMaster_Handle hAspMsgMaster;
    Int as;                             /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode/encode counter */
    Int errno;                          /* error number */
    Int zI, zS;
    Int zMD;
    Int zMI;
    Int argIdx;
    Int8 tempVar8;
    char decMsgBuf[ASP_MSG_BUF_LEN];
    Int status;
    
    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to AST common (shared) configuration
    hAspMsgMaster = pAsitCfg->hAspMsgMaster;    // get message master handle
    as = pAstCfg->as;
    zMD = pAstCfg->masterDec;
    zMI = pP->zone.master;
    (void)as;  // clear compiler warning in case not used with tracing disabled
    
    //pCbCtl = &pAsitCfg->pAspmCfg->decOpCircBufCtl; // get pointer to circular buffer control

    // reset frameCount
    for (z=DECODE1; z < DECODEN; z++)
    {
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode),
                                     GATEMP_INDEX_DEC);
        if (tempVar8)
        {
            sharedMemWriteInt(&(pAstCfg->xDec[z].decodeStatus.frameCount),
                              (Int)0, GATEMP_INDEX_DEC);
        }
    }

    for (z=DECODE1; z < DECODEN; z++) 
    {
        zI = pP->inputsFromDecodes[z];
        zS = pP->streamsFromDecodes[z];
        (void)zS; // clear compiler warning in case not used with tracing disabled
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode), 
                                     GATEMP_INDEX_DEC);
        if (pAstCfg->xInp[zI].hRxSio && tempVar8)
        {

            Uns gear;
            Int frameLength;
            TRACE_VERBOSE1("AS%d: PAF_ASIT_decodeInit: initializing decode", as+zS);

            // write back Dec configuration
            Cache_wb(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
            Cache_wait();

            // send dec activate message to slave
            argIdx = 0; // set decIdx (zone index)
            *(Int32 *)&decMsgBuf[argIdx] = z;
            status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_DEC_ACTIVATE, decMsgBuf);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInit: error in sending DEC_ACTIVATE message");
                SW_BREAKPOINT; // temporary
                return -1; // temporary
            }           
            status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_DEC_ACTIVATE_DONE, NULL, TRUE);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInit: error in receiving DEC_ACTIVATE_DONE message");
                SW_BREAKPOINT; // temporary
                return -1; // temporary
            }           

            // send dec reset message to slave
            argIdx = 0; // set decIdx
            *(Int32 *)&decMsgBuf[argIdx] = z;
            status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_DEC_RESET, decMsgBuf);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInit: error in sending DEC_RESET message ");
                SW_BREAKPOINT; // temporary
                return -1; // temporary
            }
            status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_DEC_RESET_DONE, decMsgBuf, TRUE);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInit: error in receiving DEC_RESET_DONE ack message ");
                SW_BREAKPOINT; // temporary
                return -1; // temporary
            }
            else
            {
                argIdx = 0; // get decErrno
                errno = *(Int32 *)&decMsgBuf[argIdx];                
            }

            // invalidate Dec configuration
            Cache_inv(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
            Cache_wait();
            
            if (errno != 0)
            {
                return errno;
            }
            
            gear = (Uns)sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.aspGearControl),
            		                      GATEMP_INDEX_DEC);
            tempVar8 = gear < GEARS ? gear : 0;
            sharedMemWriteInt8(&(pAstCfg->xDec[z].decodeStatus.aspGearStatus),
            		           tempVar8, GATEMP_INDEX_DEC);
            
            // Compute decoder frame length based on source selection
            frameLength = getFrameLengthSourceSel(pP, sourceSelect);
            
            pAstCfg->xDec[z].decodeControl.frameLength = frameLength;
            pAstCfg->xDec[z].decodeInStruct.sampleCount = frameLength;
            pAstCfg->xDec[z].decodeControl.sampleRate = PAF_SAMPLERATE_UNKNOWN;
          
            if (z != zMD) 
            {
                if (errno = SIO_idle(pAstCfg->xInp[zI].hRxSio))
                {
                    return errno;
                }
            }

            tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.sourceSelect),
            		                     GATEMP_INDEX_DEC);
            if (errno = SIO_ctrl(pAstCfg->xInp[zI].hRxSio, PAF_SIO_CONTROL_SET_SOURCESELECT,
                DECSIOMAP(tempVar8)))
            {
                return errno;
            }

            if (errno = SIO_ctrl(pAstCfg->xInp[zI].hRxSio, PAF_SIO_CONTROL_SET_PCMFRAMELENGTH, 
                frameLength))
            {
                return errno;
            }
            if (errno = pP->fxns->updateInputStatus(pAstCfg->xInp[zI].hRxSio, &pAstCfg->xInp[zI].inpBufStatus, 
                &pAstCfg->xInp[zI].inpBufConfig))
            {
                return errno;
            }
        }
    }

    if (pAstCfg->xInp[zMI].hRxSio) 
    {
        errno = SIO_issue(pAstCfg->xInp[zMI].hRxSio, &pAstCfg->xInp[zMI].inpBufConfig,
            sizeof(pAstCfg->xInp[zMI].inpBufConfig), PAF_SIO_REQUEST_NEWFRAME);
        if (errno)
        {
            return errno;
        }
    }

    return 0;
} //PAF_AST_decodeInit

// -----------------------------------------------------------------------------
// ASIT Decoding Function - Info Processing, Common
//
//   Name:      PAF_ASIT_decodeInfo
//   Purpose:   Decoding Function for processing information in a manner that
//              is common for both initial and subsequent frames of input data.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
Int
PAF_ASIT_decodeInfo(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int frame, 
    Int block
)
{
    PAF_AST_Config *pAstCfg;
    AspMsgMaster_Handle hAspMsgMaster;
    Int as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                     /* input/decode/stream counter */
    Int errno;                 /* error number */
    Int sioErr;                /* error number, SIO */
    Int zD, zI, zS, zX;
    Int zMD;
    Int zMI;
    Int zMS;
    Int argIdx;
    Int8 tempVar8;
    Int tempVar;
    char decMsgBuf[ASP_MSG_BUF_LEN];
    Int status;

    
    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to AST common (shared) configuration
    hAspMsgMaster = pAsitCfg->hAspMsgMaster;    // get message master handle
    as = pAstCfg->as;
    zMD = pAstCfg->masterDec;
    zMS = pAstCfg->masterStr;
    zMI = pP->zone.master;
    (void)zMS;  (void)as;  // clear compiler warning in case not used with tracing disabled
    
    // Set decode control: sample rate, emphasis
    for (z=INPUT1; z < INPUTN; z++)
    {
        zD = z;
        for (zX = DECODE1; zX < DECODEN; zX++) 
        {
            if (pP->inputsFromDecodes[zX] == z) 
            {
                zD = zX;
                break;
            }
        }
#ifdef PROFILER
            // modify for different sampling rate
            if (pAstCfg->xInp[z].inpBufStatus.sampleRateStatus == PAF_SAMPLERATE_96000HZ)
            {
                start_profiling = 1;
            }
            else
            {
                start_profiling = 0;
            }
#endif
        if (pAstCfg->xInp[z].hRxSio) 
        {
            //determine associated decoder
            if (pAstCfg->xInp[z].inpBufStatus.sampleRateStatus != 
                pAstCfg->xDec[zD].decodeControl.sampleRate) 
            {
                if (pAstCfg->xDec[zD].decodeControl.sampleRate == PAF_SAMPLERATE_UNKNOWN) 
                {
                    pAstCfg->xDec[zD].decodeControl.sampleRate = 
                        pAstCfg->xInp[z].inpBufStatus.sampleRateStatus;
                }
                else
                {
                    //TRACE_TERSE1("PAF_ASIT_decodeInfo: AS%d: return error ASPERR_INFO_RATECHANGE", as+pAstCfg->masterStr);
                    TRACE_TERSE2("inpBufStatus.sampleRateStatus: 0x%x, decodeControl.sampleRate: 0x%x",
                        pAstCfg->xInp[z].inpBufStatus.sampleRateStatus, 
                        pAstCfg->xDec[zD].decodeControl.sampleRate);
                    // return (ASPERR_INFO_RATECHANGE);
                }
            }

            tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[zD].decodeStatus.sourceDecode),
            		                     GATEMP_INDEX_DEC);
            pAstCfg->xDec[zD].decodeControl.emphasis = 
                tempVar8 != PAF_SOURCE_PCM
                ? PAF_IEC_PREEMPHASIS_NO // fix for Mantis ID #119
                : pAstCfg->xInp[z].inpBufStatus.emphasisStatus;
        }
        else 
        {
            pAstCfg->xDec[zD].decodeControl.sampleRate = PAF_SAMPLERATE_UNKNOWN;
            pAstCfg->xDec[zD].decodeControl.emphasis = PAF_IEC_PREEMPHASIS_UNKNOWN;
        }
    }

    // Wait for info input
    TRACE_VERBOSE2("PAF_ASIT_decodeInfo: AS%d: awaiting frame %d -- sync+info+data", as+zMS, frame);
    if (pAstCfg->xInp[zMI].hRxSio) 
    {
        TRACE_VERBOSE0("PAF_ASIT_decodeInfo: call SIO_reclaim to get input buffer.");
        sioErr = SIO_reclaim(pAstCfg->xInp[zMI].hRxSio, (Ptr)&pAstCfg->xInp[zMI].pInpBuf, NULL);
        if (sioErr != sizeof(pAstCfg->xInp[zMI].inpBufConfig))
        {
            TRACE_TERSE1("SIO_reclaim on input returned error ASPERR_RECLAIM.  sioErr: 0x%x", sioErr);
            return ASPERR_RECLAIM;
        }
        
#if 0 // debug
        // capture input buffer
        capIb(pAstCfg->xInp[zMI].pInpBuf);
        gCapIb_cnt++;
#endif
        
#if 0 // debug
        // Shows timing of Input Rx SIO reclaim after decoding has started (autodet complete)
        // ADC B5
        {
            static Uint8 toggleState = 0;
            if (toggleState == 0)
                GPIOSetOutput(GPIO_PORT_0, GPIO_PIN_99);
            else
                GPIOClearOutput(GPIO_PORT_0, GPIO_PIN_99);
            toggleState = ~(toggleState);
        }
#endif

        gAsipRxSioReclaimCnt++;
    } //pAstCfg->xInp[zMI].hRxSio

    // Decode info
    for (z=DECODE1; z < DECODEN; z++) 
    {
        zI = pP->inputsFromDecodes[z];
        zS = pP->streamsFromDecodes[z];
        (void)zS; // clear compiler warning in case not used with tracing disabled

        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode),
                                     GATEMP_INDEX_DEC);
        if (pAstCfg->xInp[zI].hRxSio && tempVar8)
        {
            TRACE_GEN2("PAF_ASIT_decodeInfo: AS%d: processing frame %d -- info", as+zS, frame);

            if (errno = pP->fxns->updateInputStatus(pAstCfg->xInp[zI].hRxSio,
                &pAstCfg->xInp[zI].inpBufStatus, &pAstCfg->xInp[zI].inpBufConfig))
            {
                TRACE_TERSE1("return error errno 0x%x.", errno);
                return errno;
            }

            // write back Inp configuration
            Cache_wb(&pAstCfg->xInp[zI], sizeof(PAF_AST_InpBuf), Cache_Type_ALLD, 0);
            // write back Dec configuration
            Cache_wb(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
            Cache_wait();
            
            // send info message to slave
            argIdx = 0; // set decIdx
            *(Int32 *)&decMsgBuf[argIdx] = z;
            status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_DEC_INFO, decMsgBuf);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInfo: error in sending DEC_INFO message ");
                SW_BREAKPOINT; // temporary
                return -1;     // temporary
            }
            status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_DEC_INFO_DONE, decMsgBuf, TRUE);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeInfo: error in receiving DEC_INFO_DONE ack message");
                SW_BREAKPOINT; // temporary
                return -1;     // temporary
            }
            else
            {
                argIdx = 0; // get decErrno
                errno = *(Int32 *)&decMsgBuf[argIdx];                
            }
            
            // invalidate Dec configuration
            Cache_inv(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
            Cache_wait();                            
            
            if (errno)
            {
                TRACE_TERSE1("return error errno 0x%x.", errno);
                return errno;
            }

            // increment decoded frame count
            tempVar = sharedMemReadInt(&(pAstCfg->xDec[z].decodeStatus.frameCount),
                                       GATEMP_INDEX_DEC);
            tempVar += 1;
            sharedMemWriteInt(&(pAstCfg->xDec[z].decodeStatus.frameCount),
                              tempVar, GATEMP_INDEX_DEC);
        }
    } // z=DECODE1 to DECODEN

    // query IB for latest sourceProgram (needed if we started decoding due to a force mode)
    tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.mode),
                                 GATEMP_INDEX_DEC);
    if (tempVar8)
    {
        XDAS_Int8 sourceProgram;
        if (errno = SIO_ctrl(pAstCfg->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_SOURCEPROGRAM,
            (Arg)&sourceProgram))
        {
            TRACE_TERSE1("return error ASPERR_AUTO_PROGRAM. errno 0x%x.", errno);
            return ASPERR_AUTO_PROGRAM;
        }

        sharedMemWriteInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceProgram),
                           sourceProgram, GATEMP_INDEX_DEC);
    }

    // since now decoding update decode status for all enabled decoders
    for (z=DECODE1; z < DECODEN; z++)
    {
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode),
                                     GATEMP_INDEX_DEC);
        if (tempVar8)
        {
            tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.sourceProgram),
                                         GATEMP_INDEX_DEC);
            sharedMemWriteInt8(&(pAstCfg->xDec[z].decodeStatus.sourceDecode),
                               tempVar8, GATEMP_INDEX_DEC);

            tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.sourceSelect),
                                         GATEMP_INDEX_DEC);
            if (tempVar8 == PAF_SOURCE_SNG)
            {
                tempVar8 = PAF_SOURCE_SNG;
                sharedMemWriteInt8(&(pAstCfg->xDec[z].decodeStatus.sourceDecode),
                                   tempVar8, GATEMP_INDEX_DEC);
            }
        }
    }

    return 0;
} //PAF_ASIT_decodeInfo

// -----------------------------------------------------------------------------
// ASIT Decoding Function - Info Processing, Initial
//
//   Name:      PAF_ASIT_decodeInfo1
//   Purpose:   Decoding Function for processing information in a manner that
//              is unique to initial frames of input data.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
Int
PAF_ASIT_decodeInfo1(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int frame, 
    Int block
)
{
    return 0;
} //PAF_ASIT_decodeInfo1

// -----------------------------------------------------------------------------
// AST Decoding Function - Info Processing, Subsequent
//
//   Name:      PAF_AST_decodeInfo2
//   Purpose:   Decoding Function for processing information in a manner that
//              is unique to frames of input data other than the initial one.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     None.
//
Int
PAF_ASIT_decodeInfo2(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int frame, 
    Int block
)
{
    return 0;
} //PAF_ASIT_decodeInfo2

#if 0
// -----------------------------------------------------------------------------
// AST Decoding Function - Continuation Processing
//
//   Name:      PAF_AST_decodeCont
//   Purpose:   Decoding Function for processing that occurs subsequent to
//              information processing but antecedent to timing processing
//              for frames of input data other than the initial one.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeCont(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    ALG_Handle decAlg[], 
    Int frame, 
    Int block
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* decode counter */
    Int zI, zS;
    Int zMD;
    Int8 tempVar8;

    
    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    zMD = pAstCfg->masterDec;
    (void)as;  // clear compiler warning in case not used with tracing disabled
    
    // Await slave inputs
    for (z=DECODE1; z < DECODEN; z++)
    {
        zI = pP->inputsFromDecodes[z];
        zS = pP->streamsFromDecodes[z];
        (void)zS;
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode),
                                     GATEMP_INDEX_DEC);
        if (z == zMD
            || ! pAstCfg->xInp[zI].hRxSio
            || ! tempVar8)
            continue;
        TRACE_VERBOSE2("PAF_AST_decodeCont: AS%d: awaiting frame %d -- data", as+zS, frame);
        if (SIO_reclaim(pAstCfg->xInp[zI].hRxSio, (Ptr)&pAstCfg->xInp[zI].pInpBuf, NULL)
            != sizeof (pAstCfg->xInp[zI].inpBufConfig))
            return (ASPERR_RECLAIM);
    }

    return 0;
} //PAF_AST_decodeCont
#endif

// -----------------------------------------------------------------------------
// ASIT Decoding Function - Decode Processing
//
//   Name:      PAF_ASIT_decodeDecode
//   Purpose:   Decoding Function for processing of input data by the
//              Decode Algorithm.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
Int
PAF_ASIT_decodeDecode(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int sourceSelect, 
    Int frame, 
    Int block
)
{
    PAF_AST_Config *pAstCfg;
    AspMsgMaster_Handle hAspMsgMaster;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* decode/stream counter */
    Int errno;                  /* error number */
    //Int ch;
    Int argIdx;
    Int cbErrno;
    Int frameLength;    
    char decMsgBuf[ASP_MSG_BUF_LEN];
    Int status;

    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to AST common (shared) configuration
    hAspMsgMaster = pAsitCfg->hAspMsgMaster;    // get message master handle
    as = pAstCfg->as;
    (void)as; // clear compiler warning in case not used with tracing disabled

    // Decode data
    for (z=DECODE1; z < DECODEN; z++) 
    {
        Int zI = pP->inputsFromDecodes[z];
        Int zS = pP->streamsFromDecodes[z];
        (void)zS; // clear compiler warning in case not used with tracing disabled
        if (pAstCfg->xInp[zI].hRxSio && pAstCfg->xDec[z].decodeStatus.mode)
        {
            TRACE_GEN2("PAF_ASIT_decodeDecode: AS%d: decodeDecode: processing block %d -- decode", as+zS, block);

            TRACE_VERBOSE3("PAF_ASIT_decodeDecode: AS%d: decodeDecode: decoding from 0x%x (base) 0x%x (ptr)",
                    as+zS,
                    (IArg)pAstCfg->xInp[z].pInpBuf->base.pVoid,
                    (IArg)pAstCfg->xInp[z].pInpBuf->head.pVoid);

#if 0 // debug
            // capture input buffer
            capIbPcm(pAstCfg->xInp[z].pInpBuf);
#endif
            
            // write back Dec configuration
            Cache_wb(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
            Cache_wait();                        

            // send decode message to slave
            argIdx = 0; // set decIdx
            *(Int32 *)&decMsgBuf[argIdx] = z;
            status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_DEC_DECODE, decMsgBuf);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeDecode: error in sending DEC_DECODE message ");
                SW_BREAKPOINT; // temporary
                return -1;     // temporary
            }
            status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_DEC_DECODE_DONE, decMsgBuf, TRUE);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeDecode: error in receiving DEC_DECODE_DONE ack message ");
                SW_BREAKPOINT; // temporary
                return -1;     // temporary
            }
            else
            {
                argIdx = 0; // get decErrno
                errno = *(Int32 *)&decMsgBuf[argIdx];
                argIdx += sizeof(Int32); // get cbErrno
                cbErrno = *(Int32 *)&decMsgBuf[argIdx];
                if (cbErrno != 0)
                {
                    gCbWrtAfErrCnt++;
                    TRACE_TERSE1("CB write error=%d", cbErrno);
                    //SW_BREAKPOINT; // temporary
                }                
            }
            
            // invalidate Dec configuration
            Cache_inv(&pAstCfg->xDec[z], sizeof(PAF_AST_Decode), Cache_Type_ALLD, 0);
            Cache_wait();

#if 0 // decoder decode call, slave
            if (dec->fxns->decode
                && (errno = dec->fxns->decode (dec, NULL,
                                               &pC->xDec[z].decodeInStruct, &pC->xDec[z].decodeOutStruct))) 
#endif
            if (errno)
            {
                TRACE_VERBOSE1("PAF_ASIT_decodeDecode: fxns->decode returns 0x%x", errno);
                return errno;
            }

#if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)
            as_traceChannels(pC, z);
#endif

            // Compute decoder frame length based on source selection
            frameLength = getFrameLengthSourceSel(pP, sourceSelect);

//#ifdef PROFILER
//            // modify for different decoders
//            if (sourceSelect == PAF_SOURCE_THD)
//            {
//                start_profiling = 1;
//            }
//            else
//            {
//                start_profiling = 0;
//            }
//#endif


// ............................................................................

            pAstCfg->xDec[z].decodeControl.frameLength = frameLength;
            pAstCfg->xDec[z].decodeInStruct.sampleCount = frameLength;
            if (errno = SIO_ctrl(pAstCfg->xInp[zI].hRxSio,
                PAF_SIO_CONTROL_SET_PCMFRAMELENGTH, frameLength))
            {
                TRACE_VERBOSE1("PAF_ASIT_decodeDecode: SIO SET_PCMFRAMELENGTH returns 0x%x", errno);
                return errno;
            }
                
            TRACE_VERBOSE1("PAF_ASIT_decodeDecode: calling SIO_issue[%d]", zI);
            if (errno = SIO_issue(pAstCfg->xInp[zI].hRxSio, &pAstCfg->xInp[zI].inpBufConfig,
                sizeof(pAstCfg->xInp[zI].inpBufConfig),
                PAF_SIO_REQUEST_NEWFRAME))
            {
                TRACE_VERBOSE2("PAF_ASIT_decodeDecode: SIO_issue returns 0x%x, we return ASPERR_ISSUE (0x%x)", errno, ASPERR_ISSUE);
                return (ASPERR_ISSUE);
            }
        } // hRxSio && decodeStatus.mode
        else
        {
            TRACE_VERBOSE2("AS%d: PAF_ASIT_decodeDecode: processing block %d -- decode <ignored>", as+zS, block);
        }
    } // z=DECODE1 to DECODEN

    return 0;
} //PAF_ASIT_decodeDecode

// -----------------------------------------------------------------------------
// ASIT Decoding Function - Frame-Final Processing
//
//   Name:      PAF_ASIT_decodeFinalTest
//   Purpose:   Decoding Function for determining whether processing of the
//              current frame is complete.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    0 if incomplete, and 1 if complete.
//   Trace:     None.
//

Int
PAF_ASIT_decodeFinalTest(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int frame, 
    Int block
)
{
    PAF_AST_Config *pAstCfg;
    Int zMD;
    Int sourceSelect;
    Int sourceProgram;
    Int8 tempVar8, temp2Var8;

    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    zMD = pAstCfg->masterDec;

    sourceSelect  = (Int)sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceSelect),
                                           GATEMP_INDEX_DEC);
    sourceProgram = (Int)sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceProgram),
                                           GATEMP_INDEX_DEC);

    if ((sourceSelect == PAF_SOURCE_NONE) || (sourceSelect == PAF_SOURCE_PASS))
    {
        return 1;
    }

    // The following allows for Force modes to switch without command deferral. This might
    // be better suited for inclusion in DIB_requestFrame, but for now will reside here.
    if ((sourceSelect == PAF_SOURCE_SNG) || (sourceSelect > PAF_SOURCE_BITSTREAM)) 
    {
        if (sourceSelect == PAF_SOURCE_DTSALL)
        {
            if (sourceProgram != PAF_SOURCE_DTS11 &&
                sourceProgram != PAF_SOURCE_DTS12 &&
                sourceProgram != PAF_SOURCE_DTS13 &&
                sourceProgram != PAF_SOURCE_DTS14 &&
                sourceProgram != PAF_SOURCE_DTS16 &&
                sourceProgram != PAF_SOURCE_DTSHD)
            {
                return 1;
            }
        }
        else if (sourceSelect == PAF_SOURCE_PCMAUTO) 
        {
            if (sourceProgram != PAF_SOURCE_PCM)
            {
                return 1;
            }
        }
        else 
        {
            tempVar8  = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceDecode),
                                          GATEMP_INDEX_DEC);
            temp2Var8 = sharedMemReadInt8(&(pAstCfg->xDec[zMD].decodeStatus.sourceSelect),
                                          GATEMP_INDEX_DEC);
            if (temp2Var8 != tempVar8)
            {
                return 1;
            }
        }
    }

    return 0;
} //PAF_ASIT_decodeFinalTest

// -----------------------------------------------------------------------------
// ASIT Decoding Function - Stream-Final Processing
//
//   Name:      PAF_ASIT_decodeComplete
//   Purpose:   Decoding Function for terminating the decoding process.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
Int
PAF_ASIT_decodeComplete(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    ALG_Handle decAlg[], 
    Int frame, 
    Int block
)
{
    PAF_AST_Config *pAstCfg;
    AspMsgMaster_Handle hAspMsgMaster;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* decode/encode counter */
    Int argIdx;
    Int8 tempVar8;
    char decMsgBuf[ASP_MSG_BUF_LEN];
    Int status;
    
    pAstCfg = pAsitCfg->pAstCfg;                // get pointer to AST common (shared) configuration
    hAspMsgMaster = pAsitCfg->hAspMsgMaster;    // get message master handle
    as = pAstCfg->as;
    (void)as;  // clear compiler warning in case not used with tracing disabled

#ifdef PAF_ASP_FINAL
    /* This material is currently not utilized */
#endif /* PAF_ASP_FINAL */
    for (z=DECODE1; z < DECODEN; z++) 
    {
#ifdef PAF_ASP_FINAL
        DEC_Handle dec = (DEC_Handle )decAlg[z];
#endif /* PAF_ASP_FINAL */
        Int zI = pP->inputsFromDecodes[z];
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode),
                                     GATEMP_INDEX_DEC);
        if (pAstCfg->xInp[zI].hRxSio && tempVar8)
        {
            TRACE_VERBOSE1("PAF_ASIT_decodeComplete: AS%d: finalizing decode", as+z);
#ifdef PAF_ASP_FINAL
            if (dec->fxns->final)
                dec->fxns->final(dec, NULL, &pAstCfg->xDec[z].decodeControl,
                                 &pAstCfg->xDec[z].decodeStatus);
#endif /* PAF_ASP_FINAL */

            // send dec deactivate message to slave
            argIdx = 0; // set decIdx
            *(Int32 *)&decMsgBuf[argIdx] = z;
            status = AspMsgSnd(hAspMsgMaster, ASP_SLAVE_DEC_DEACTIVATE, decMsgBuf);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeComplete: error in sending DEC_DEACTIVATE message.");
                SW_BREAKPOINT;
                return -1;
            }
            status = AspMsgRcvAck(hAspMsgMaster, ASP_MASTER_DEC_DEACTIVATE_DONE, NULL, TRUE);
            if (status != ASP_MSG_NO_ERR)
            {
                TRACE_TERSE0("decodeComplete: error in receiving DEC_DEACTIVATE ack message.");
                SW_BREAKPOINT;
                return -1;
            }
        }
        else 
        {
            TRACE_VERBOSE1("PAF_ASIT_decodeComplete: AS%d: processing decode <ignored>", as+z);
        }
    }
    return 0;
} //PAF_ASIT_decodeComplete

// -----------------------------------------------------------------------------
// ASIT Selection Function - Input Device Selection
//
//   Name:      PAF_ASIT_selectDevices
//   Purpose:   Audio Stream Input Task Function for selecting the devices used
//              for input.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//
Int
PAF_ASIT_selectDevices(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg
)
{
    PAF_AST_Config *pAstCfg;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* input/output counter */
    Int errno = 0;              /* error number */
    Int errme;                  /* error number, local */
    Int device;
    Int zMD;

    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration
    as = pAstCfg->as;
    zMD = pAstCfg->masterDec;
    (void)as;  // clear compiler warning in case not used with tracing disabled
    
    // Select input devices
    for (z=INPUT1; z < INPUTN; z++) 
    {
        if ((device = pAstCfg->xInp[z].inpBufStatus.sioSelect) >= 0) 
        {
            TRACE_VERBOSE2("PAF_ASIT_selectDevices: AS%d: input device %d selecting ...", as+z, device);

            // check for valid index into device array
            if (device >= pQ->devinp->n)
            {
                device = 0; /* treat as device None */
            }

            errme = pP->fxns->deviceSelect(&pAstCfg->xInp[z].hRxSio, SIO_INPUT,
                HEAP_ID_INPBUF, (Ptr)pQ->devinp->x[device]);

            if (errme)
            {
                TRACE_VERBOSE2("PAF_ASIT_selectDevices: errme 0x%x, errno 0x%x", errme, errno);
                if (!errno)
                {
                    errno = ASPERR_DEVINP + errme;
                }
                pAstCfg->xInp[z].inpBufStatus.sioSelect = 0x80;
            }
            else 
            {
                pAstCfg->xInp[z].inpBufStatus.sioSelect = device | 0x80;
                // register decodeStatus pointer with input devices
                // This allows, e.g., autoProcessing to exit when sourceSelect = none
                // Use zMIs decodeStatus for all inputs
                if (pAstCfg->xInp[z].hRxSio) 
                {
                    // register PAF_SIO_IALG object address
                    SIO_ctrl(pAstCfg->xInp[z].hRxSio, PAF_SIO_CONTROL_SET_IALGADDR, 
                        (Arg)pAstCfg->xInp[z].inpChainData.head->alg);
                    SIO_ctrl(pAstCfg->xInp[z].hRxSio, PAF_SIO_CONTROL_SET_DECSTATUSADDR, 
                        (Arg)&pAstCfg->xDec[zMD].decodeStatus);
                }
            }
        }
    }

    return errno;
} //PAF_ASIT_selectDevices

// -----------------------------------------------------------------------------
// ASIT Selection Function - Source Selection
//
//   Name:      PAF_ASIT_sourceDecode
//   Purpose:   Audio Input Stream Task Function for selecting the sources used
//              for decoding of input to output.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     None.
//
Int
PAF_ASIT_sourceDecode(
    const PAF_ASIT_Params *pP, 
    const PAF_ASIT_Patchs *pQ, 
    PAF_ASIT_Config *pAsitCfg, 
    Int x
)
{
    PAF_AST_Config *pAstCfg;
    Int z;                              /* decode counter */
    Int8 tempVar8;

    pAstCfg = pAsitCfg->pAstCfg; // get pointer to AST common (shared) configuration

    for (z=DECODE1; z < DECODEN; z++)
    {
        tempVar8 = sharedMemReadInt8(&(pAstCfg->xDec[z].decodeStatus.mode),
                                     GATEMP_INDEX_DEC);
        if (tempVar8)
        {
            tempVar8 = x;
            sharedMemWriteInt8(&(pAstCfg->xDec[z].decodeStatus.sourceDecode),
                               tempVar8, GATEMP_INDEX_DEC);
        }
    }
    
    return 0;
} //PAF_ASIT_sourceDecode

// -----------------------------------------------------------------------------

#if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)

//------------------------------------------------------------------------------
static int sSkipCount = 0;
int gReportBuffers = 0;
void as_traceChannels(PAF_AST_Config *pC, int z)
{
    PAF_AudioFrame *pAudioFrame = pC->xDec[z].decodeInStruct.pAudioFrame;
    int i;
         
// #ifdef THIS_IS_DSPA
    sSkipCount++;
    if (sSkipCount<1)
        return;
    sSkipCount = 0;
// #endif

#ifdef THIS_IS_DSPB
    if (!gReportBuffers)
        return;
    gReportBuffers = 0;
#endif

    dp(NULL, "\n");
    for (i=0; i<PAF_MAXNUMCHAN; i++)
    {
        if (pAudioFrame->data.sample[i] != 0)
        {
            float *wp = (float*)pAudioFrame->data.sample[i];
               dp(NULL, "i: %d.  p: 0x%x.  %f, %f, %f, %f\n",
                       i, pAudioFrame->data.sample[i], wp[0], wp[1], wp[2], wp[3]);
        }
    }
}
#endif

// Compute decoder frame length based on selected source
Int getFrameLengthSourceSel(
    const PAF_ASIT_Params *pP, 
    Int8 sourceSelect
)
{
    Int frameLength;
    
    switch(sourceSelect)
    {
        case PAF_SOURCE_PCM:
            frameLength = FRAMELENGTH;
            break;
        case PAF_SOURCE_AAC:
            frameLength = 1024;
            break;
        case PAF_SOURCE_AC3: 
        case PAF_SOURCE_DDP: 
            frameLength = ASIP_FRAMELEN_SS_DDP_AC3;
            break;
        case PAF_SOURCE_THD:
            frameLength = ASIP_FRAMELEN_SS_THD;
            break;
        case PAF_SOURCE_DTS:
        case PAF_SOURCE_DTSHD:
        case PAF_SOURCE_DTS12:
        case PAF_SOURCE_DTS13:
        case PAF_SOURCE_DTS14:
        case PAF_SOURCE_DTS16:
        case PAF_SOURCE_DTSALL:
            frameLength = ASIP_FRAMELEN_SS_DTS;
            break;
        case PAF_SOURCE_DSD1:
        case PAF_SOURCE_DSD2:
        case PAF_SOURCE_DSD3:
            frameLength = 1024;
            break;
        default:
            frameLength = ASIP_FRAMELEN_SS_DEFAULT;
            break;
    }
    
    return frameLength;
}
