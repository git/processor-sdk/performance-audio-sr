
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamOutProc_paramsFxns.c ========
 */

#include <xdc/std.h>
 
#include "as0.h"
#include "audioStreamOutProc.h"
#include "audioStreamOutInit.h"
 
// .............................................................................
// Audio Stream Output Task Parameter Functions
//
//   Name:      PAF_ASOT_params_fxns
//   Purpose:   ASOT jump table.
//   From:      PAF_AST_Params
//   Uses:      See contents.
//   States:    N.A.
//   Return:    N.A.
//   Trace:     None.
//
const PAF_ASOT_Fxns PAF_ASOT_params_fxns =
{
    {   // initPhase[8]
        PAF_ASOT_initPhaseMalloc,
        PAF_ASOT_initPhaseConfig,
        PAF_ASOT_initPhaseAcpAlg,
        PAF_ASOT_initPhaseCommon,
        PAF_ASOT_initPhaseAlgKey,
        PAF_ASOT_initPhaseDevice,
        NULL,
        NULL
    },
    PAF_ASOT_initFrame0,                    // initFrame0
    PAF_ASOT_initFrame1,                    // initFrame1
    NULL, //PAF_ASOT_decodeProcessing,              // decodeProcessing
    NULL, //PAF_ASOT_encodeCommand,                 // encodeCommand
    NULL, //PAF_ASOT_decodeInit,                    // decodeInit
    NULL, //PAF_ASOT_decodeInfo1,                   // decodeInfo1
    NULL, //PAF_ASOT_decodeInfo2,                   // decodeInfo2
    NULL, //PAF_ASOT_decodeStream,                  // decodeStream
    NULL, //PAF_ASOT_decodeEncode,                  // decodeEncode
    NULL, //PAF_ASOT_decodeFinalTest,               // decodeFinalTest
    NULL, //PAF_ASOT_decodeComplete,                // decodeComplete
    NULL, //PAF_ASOT_selectDevices,                 // selectDevices
    NULL, //PAF_ASOT_startOutput,                   // startOutput
    NULL, //PAF_ASOT_stopOutput,                    // stopOutput
    NULL, //PAF_ASOT_setCheckRateX,                 // setCheckRateX
    NULL, //PAF_ASOT_streamChainFunction,           // streamChainFunction
    PAF_DEC_deviceAllocate,                 // deviceAllocate
    PAF_DEC_deviceSelect,                   // deviceSelect
    NULL, /*headerPrint*/                   // headerPrint
    NULL, /*allocPrint*/                    // allocPrint
    NULL, /*commonPrint*/                   // commonPrint
    NULL, /*bufMemPrint*/                   // bufMemPrint
    NULL /*memStatusPrint*/                 // memStatusPrint
};
