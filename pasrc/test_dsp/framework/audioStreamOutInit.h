
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamOutInit.h ========
 */

#ifndef _ASOP_INIT_H_
#define _ASOP_INIT_H_
 
#include <xdc/std.h>

#include "audioStreamProc_params.h"
#include "audioStreamProc_patchs.h"
#include "audioStreamProc_config.h"


//   Purpose:   Audio Stream Output Task Function for initialization of data pointers
//              by allocation of memory.
Int PAF_ASOT_initPhaseMalloc(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
);
    
//   Purpose:   Audio Stream Output Task Function for initialization of data values
//              from parameters.
Int PAF_ASOT_initPhaseConfig(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
);

//   Purpose:   Audio Stream Output Task Function for initialization of ACP by
//              instantiation of the algorithm.
Int PAF_ASOT_initPhaseAcpAlg(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
);

//   Purpose:   Audio Stream Output Task Function for allocation of common memory.
Int PAF_ASOT_initPhaseCommon(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
);

//   Purpose:   Audio Stream Output Task Function for initialization of data values
//              from parameters for Algorithm Keys.
Int PAF_ASOT_initPhaseAlgKey(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
);

//   Purpose:   Audio Stream Output Task Function for initialization of Input Devices.
Int PAF_ASOT_initPhaseDevice(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
);

//   Purpose:   Audio Stream Output Task Function for initialization of the Audio
//              Frame(s) by memory allocation and loading of data pointers
//              and values.
Int PAF_ASOT_initFrame0(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int z
);

//   Purpose:   Audio Stream Task Function for initialization or reinitiali-
//              zation of the Audio Frame(s) by loading of data values of a
//              time-varying nature.
Int PAF_ASOT_initFrame1(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int z, 
    Int apply
);

// Audio Stream Output Processing initialization function
Void taskAsopFxnInit(
    const PAF_ASOT_Params *pP,
    const PAF_ASOT_Patchs *pQ
);

#endif /* _ASOP_INIT_H_ */
