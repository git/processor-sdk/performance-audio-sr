
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _ASP_MSG_MASTER_H_
#define _ASP_MSG_MASTER_H_

#include <xdc/std.h>
#include <ti/sysbios/heaps/HeapBuf.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/syncs/SyncEvent.h>
#include <ti/ipc/MessageQ.h>

// Error return codes
#define ASP_MSG_MASTER_SOK                          (  0 )
#define ASP_MSG_MASTER_MSGPOOL_MALLOC_FAIL          ( -1 )
#define ASP_MSG_MASTER_MSGPOOL_HEAPBUF_CREATE_FAIL  ( -2 )
#define ASP_MSG_MASTER_MSGQ_REGHEAP_FAIL            ( -3 )
#define ASP_MSG_MASTER_MASTER_MSGQ_CREATE_FAIL      ( -4 )
#define ASP_MSG_MASTER_SLAVE_MSGQ_OPEN_FAIL         ( -5 )
#define ASP_MSG_MASTER_SAVEMSGID_MALLOC_FAIL        ( -6 )

// Error return codes for function AspMsgSend & AspMsgRcvAck
#define ASP_MSG_NO_ERR                              (  0 )
#define ASP_MSG_ERR_QUEUE_ALLOC                     ( -1 )
#define ASP_MSG_ERR_QUEUE_PUT                       ( -2 )
#define ASP_MSG_ERR_QUEUE_GET                       ( -3 )
#define ASP_MSG_ERR_QUEUE_TIMEOUT                   ( -4 )
#define ASP_MSG_ERR_QUEUE_FREE                      ( -5 )
#define ASP_MSG_ERR_ACKNOWLEDGE                     ( -6 )
#define ASP_MSG_ERR_SEND_OVR                        ( -7 )
#define ASP_MSG_ERR_RECEIVE_UND                     ( -8 )

#define ASP_MSG_MASTER_DEF_NUMMSGS                  (  4 )

// save message info
typedef struct AspMsgMaster_SaveMsgInfo
{
    UInt32 sndCmd;  // command sent from master to slave
    UInt32 msgId;   // Id contained in LS 31 bits, MS bit is 0
} AspMsgMaster_SaveMsgInfo;

// module structure
typedef struct AspMsgMaster_Module
{
    UInt16                      masterProcId;       // master processor id
    UInt16                      slaveProcId;        // slave processor id
    MessageQ_Handle             masterQue;          // created locally
    MessageQ_QueueId            slaveQueId;         // created remotely, opened locally
    UInt16                      heapId;             // MessageQ heapId
    UInt16                      numMsgs;            // maximum number of messages
    HeapBuf_Handle              heap;               // message heap
    Int                         msgSize;            // aligned size of message
    Int                         poolSize;           // size of message pool
    Ptr                         store;              // memory store for message pool
    UInt32                      messageId;          // Next send message Id. Format: Id contained in LS 31 bits, MS bit is 0.
    AspMsgMaster_SaveMsgInfo    *saveMsgInfo;       // Send message info array
    UInt8                       saveMsgWrtIdx;      // index of next location in saveMsgInfo[] for write (message send)
    UInt8                       saveMsgRdIdx;       // index of next location in saveMsgInfo[] for read (message receive)
    Int8                        numSaveMsgInfo;     // number of saved message info
    SyncEvent_Handle            syncEventHandle;    // sync event handle
    Event_Handle                eventHandle;        // event handle
    UInt                        eventId;            // event Id
} AspMsgMaster_Module;

// module handle
typedef AspMsgMaster_Module *   AspMsgMaster_Handle;

// Initialize ASP master messaging
Int                                     // returned status
AspMsgMaster_init(
    AspMsgMaster_Handle hAspMsgMaster,  // message master handle
    UInt16 remoteProcId,                // remote processor Id
    UInt16 heapId,                      // heap Id for message pool
    UInt16 numMsgs,                     // number of messages in message pool
    SyncEvent_Handle syncEventHandle,   // sync event handle
    Event_Handle eventHandle,           // event handle
    UInt eventId                        // event Id    
);

/* 
 * ASP message send function
 *
 * Description: 
 *      This function sends a message from the master to the slave processor.
 */
Int                                     // returned status
AspMsgSnd(
    AspMsgMaster_Handle hAspMsgMaster,  // message master handle
    UInt32 sndCmd,                      // command sent from master to slave
    char *sndMsgBuf                     // message buffer for message sent from master to slave
);

/* 
 * ASP message receive acknowledge function
 *
 * Description: 
 *      This function receives an acknowledgment message from the slave to the master processor.
 *      The expected acknowledgement is an input to the function.
 */
Int                                     // returned status
AspMsgRcvAck(
    AspMsgMaster_Handle hAspMsgMaster,  // message master handle
    UInt32 ackCmd,                      // expected acknowledgment sent from slave to master
    char *ackMsgBuf,                    // message buffer for acknowledgment message sent from slave to master
    Bool pendOnEvent                    // whether to pend on Event synchronizer
);

/* 
 * ASP message get number of outstanding messages function
 *
 * Description: 
 *      This function returns the number of messages sent to the slave which haven't yet been acknowledged.
 */
Int                                     // returned status
AspMsgGetNumSndMsgUnack(
    AspMsgMaster_Handle hAspMsgMaster,  // message master handle
    UInt16 *pNumSndMsgUnack             // number of unacknowledged messages sent to the slave
);

/* 
 * ASP message receive acknowledge function
 *
 * Description: 
 *      This function receives an acknowledgment message from the slave to the master processor.
 *      The receive acknowledgement is output by the function.
 */
Int                                     // returned status
AspMsgRcvAckUnk(
    AspMsgMaster_Handle hAspMsgMaster,  // message master handle
    UInt32 *pAckCmd,                    // acknowledgment sent from slave to master
    char *ackMsgBuf,                    // message buffer for acknowledgment message sent from slave to master
    Bool pendOnEvent                    // whether to pend on Event synchronizer
);


#endif /* _ASP_MSG_MASTER_H_ */
