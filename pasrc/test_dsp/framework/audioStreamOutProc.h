
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== audioStreamOutProc.h ========
 */

#ifndef _ASOP_H_
#define _ASOP_H_
 
#include <xdc/std.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/knl/Task.h>

#include "mcasp_cfg.h"
#include "ioPhy.h"
#include "ioBuff.h"

#include "audioStreamProc_params.h"
#include "audioStreamProc_patchs.h"
#include "audioStreamProc_config.h"
#include "audioStreamProc_master.h"

#define TRACE_TIME(a)

/* !!!!!!!!! Revisit !!!!!!!!!!!!!!
 * Purpose : Temporary ASOT frame length sizes.
 * 			 Deprecate PAF_SYS_FRAMELENGTH in phases.*/
#define PAF_ASOT_FRAMELENGTH		256
#define PAF_ASOT_MAX_FRAMELENGTH	1024

// ASOT event IDs
#define Evt_Id_AsotWakeTimer    Event_Id_00  // ASOT Wake Timer event
#define Evt_Id_AsotRxMsgAsit    Event_Id_01  // ASOT Rx ASIT message event
#define Evt_Id_AsotRxMsgAsdt    Event_Id_02  // ASOT Rx ASDT message event
#define Evt_Id_AsotTxMcaspEdma  Event_Id_03  // ASOT Tx McASP EDMA event

#if 1
#define ASP_STREAM_OUT_GATE_NAME ( "AspStreamOutGate" )  // name of GateMP used StreamOut info protection
#define ASP_STREAM_OUT_GATE_REGION_ID ( 0 )
#define ASP_STREAM_OUT_INV_GATE          ( -1 )                      // error: invalid gate handle
#endif

// ASOT event handle - to put in structure
extern Event_Handle gAsotEvtHandle;

struct PAF_ASOT_Params;
struct PAF_ASOT_Patchs;
struct PAF_ASOT_Config;

// Audio Stream Output Task (ASOT) parameters, functions
typedef struct PAF_ASOT_Fxns {
    Int (*initPhase[8]) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *);
    Int (*initFrame0) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *, Int);
    Int (*initFrame1) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *, Int, Int);    
    Int (*decodeProcessing) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *);    
    Int (*encodeCommand) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *);
    Int (*decodeInit) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *);
    Int (*decodeInfo1) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *, Int);
    Int (*decodeInfo2) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *, Int);
    Int (*decodeStream) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *, Int);
    Int (*decodeEncode) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *, Int);
    Int (*decodeFinalTest) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *, Int);    
    Int (*decodeComplete) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *, Int);
    Int (*selectDevices) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *, Bool *);
    Int (*startOutput) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *);
    Int (*stopOutput) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *);
    Int (*setCheckRateX) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *, Int);
    Int (*streamChainFunction) (const struct PAF_ASOT_Params *, const struct PAF_ASOT_Patchs *, struct PAF_ASOT_Config *, Int, Int, Int);
    Int (*deviceAllocate) (SIO_Handle *, int, int, int, Ptr);
    Int (*deviceSelect) (SIO_Handle *, int, int, Ptr);

    // For RAM_report
    Void (*headerPrint)();
    Int  (*allocPrint)(const PAF_ALG_AllocInit *pInit, Int sizeofInit, PAF_IALG_Config *p);
    Void (*commonPrint)(IALG_MemRec common[], PAF_IALG_Config *p);
    Void (*bufMemPrint)(Int z, Int size, Int heapId, Int bufType);
    Void (*memStatusPrint)(
        CString header, 
        HeapMem_Handle hInternalHeap, 
        HeapMem_Handle hInternal1Heap, 
        HeapMem_Handle hExternalHeap, 
        HeapMem_Handle hInternal1HeapShm, 
        HeapMem_Handle hExternalHeapShm, 
        HeapMem_Handle hExternalNoncachedHeapShm);
} PAF_ASOT_Fxns;

// Audio Stream Output Task (ASOT) parameters
typedef struct PAF_ASOT_Params {
    const PAF_ASOT_Fxns *fxns;
    struct {
        SmInt master;
        SmInt inputs;
        SmInt input1;
        SmInt inputN;
        SmInt decodes;
        SmInt decode1;
        SmInt decodeN;
        SmInt streams;
        SmInt stream1; /* unused */
        SmInt streamN; /* unused */
        SmInt encodes;
        SmInt encode1;
        SmInt encodeN;
        SmInt outputs;
        SmInt output1;
        SmInt outputN;
    } zone;
    const SmInt *inputsFromDecodes;
    const SmInt *outputsFromEncodes;
    struct {
        int *pHeapIdIntern;             // INT memory heap ID
        int *pHeapIdExtern;             // EXT memory heap ID
        int *pHeapIdInpbuf;             // IB buffer heap ID
        int *pHeapIdOutbuf;             // OB buffer heap ID
        int *pHeapIdFrmbuf;             // Frame buffer heap ID
        int *pHeapIdIntern1;            // INT1 memory heap ID
        int *pHeapIdInt1Shm;            // INT1 SHared Memory heap ID
        int *pHeapIdExtShm;             // EXT SHared Memory heap ID
        int *pHeapIdExtNonCachedShm;    // EXT Non-Cached SHared Memory heap ID
        int clear; 
    } heap;
    struct {
        const IALG_MemSpace *space;
    } common;
    const LgInt *z_rx_bufsiz;
    const LgInt *z_tx_bufsiz;
    const SmInt *z_numchan;
    MdInt framelength;
    const PAF_AudioFunctions *pAudioFrameFunctions;
    const struct PAF_ASP_ChainFxns *pChainFxns;
    const PAF_InpBufStatus *pInpBufStatus;
    const PAF_DecodeStatus * const *z_pDecodeStatus;
    const PAF_OutBufStatus *pOutBufStatus;
    const PAF_EncodeStatus * const *z_pEncodeStatus;
    const PAF_VolumeStatus *pVolumeStatus;
    const PAF_ASP_AlgKey *pDecAlgKey;
    const PAF_ASP_AlgKey *pEncAlgKey;
    const PAF_ASP_SioMap *pDecSioMap;
    const SmInt *streamsFromDecodes;
    const SmInt *streamsFromEncodes;
    const MdInt maxFramelength;
    const SmInt *streamOrder;
    const PAF_ASP_LinkInit * const (*i_inpLinkInit);
    const PAF_ASP_LinkInit * const (*i_outLinkInit);
    const PAF_ASP_outNumBufMap *  const (*poutNumBufMap);
    const PAF_MetadataBufStatus *pMetadataBufStatus;
    const PAF_AudioFrameBufStatus *pAudioFrameBufStatus;
} PAF_ASOT_Params;

// Audio Stream Output Task (ASOT) patchs
typedef struct PAF_ASOT_Patchs {
    const PAF_SIO_ParamsN * devout;
    const PAF_ASP_LinkInit * const (*i_aspLinkInit)[GEARS];
    const PAF_ASP_LinkInit * const (*i_encLinkInit);
} PAF_ASOT_Patchs;

typedef struct PAF_AST_OutIO {
    ioPhyHandle_t        hIoPhy;        // handle to I/O physical layer
    ioBuffHandle_t       hIoBuff;       // handle to I/O buffer management
    Ptr                  hMcaspChan;    // handle to McASP LLD channel for output
    mcaspLLDconfig       *pLldCfg;      // pointer to McASP LLD configuration
    
    uint32_t             phyXferSize;
    uint32_t             stride;

    void     *mcaspTxBuf1;
    void     *mcaspTxBuf2;
    uint32_t  mcaspTxSize1;
    uint32_t  mcaspTxSize2;
    int       mcaspTxtwoXfers;

    int       numPrimeXfers;

    uint32_t  mcaspXferErr;

    void  *buff1;                       // pointer to 1st buffer in output memory pool
    void  *buff2;                       // pointer to 2nd buffer in output memory pool in case of buffer wrap
    size_t size1;                       // size of 1st buffer in output memory pool
    size_t size2;                       // size of 2nd buffer in output memory pool
    
    uint32_t ioBuffBuf2AllocCnt;        // Output buffer2 allocation (split buffer on buffer wrap) count
    uint32_t errIoBuffOvrCnt;           // Output IO overflow count
    uint32_t errIoBuffUndCnt;           // Output IO underflow count
    float    rateX;                     // Input/Output clock ratio
    
    // debugging counters
    uint32_t num_xfers;
    GateMP_Handle gateHandle;

} PAF_AST_IoOut;

// Audio Stream Input Task (ASOT) configuration
typedef struct PAF_ASOT_Config {
    Task_Handle     taskHandle;                 // ASOT task handle
    uint_least16_t  state;                      // ASOT state
    ACP_Handle      acp;                        // ASOT ACP handle
    Int8            cbDrainedFlag[DECODEN_MAX]; // CB drained flags
    PAF_ASPM_Config *pAspmCfg;                  // ASIT/ASOT shared configuration
    PAF_AST_Config  *pAstCfg;                   // ASIT/ASOT/ASDT shared configuration
    PAF_AST_IoOut   *pIoOut;                    // ASOT IO configuration
    uint32_t        asopPhyTransferStartflag;       // Flag to indicate transfer start in SWI(asopSwiFunc)
} PAF_ASOT_Config;

// ASOT configuration
extern PAF_ASOT_Config gPAF_ASOT_config;

#endif /* _ASOP_H_ */
