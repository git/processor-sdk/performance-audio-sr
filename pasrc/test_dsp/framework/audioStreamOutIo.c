
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== audioStreamOutIo.c ========
 */

#include <string.h> // for memset
#include <xdc/runtime/Log.h>
#include <xdc/runtime/System.h>
#include <xdc/std.h>

#include "mcasp_cfg.h"
#include "ioConfig.h"    //TODO: remove this header
#include "ioBuff.h"
#include "ioPhy.h"
#include "ioData.h"

#include "pafsio_ialg.h"
#include "stdasp.h"
#include "asperr.h"

#include "audioStreamProc_common.h"
#include "audioStreamOutProc.h"
#include "audioStreamOutIo.h"

#define DEC_OUTNUMBUF_MAP(X) \
      pP->poutNumBufMap[z]->map[(X) >= pP->poutNumBufMap[z]->length ? 0 : (X)]

//#define STRIDE_WORST_CASE 32  // 4-byte (32-bit) word, 2 slots, 4 serializers

extern Ptr hMcaspTxChan;
extern Int d10Initialized;


// Select Output devices
Int asopSelectDevices(
    const PAF_SIO_Params *pOutCfg, 
    PAF_AST_IoOut *pOut
)
{
    mcaspLLDconfig *pReqLldCfg;
    Ptr mcaspChanHandle;
    Int32 status;
    Aud_STATUS audStatus;
    UInt postedEvents;    

    if ((pOut->hIoBuff == NULL) || (pOut->hIoPhy == NULL) || (!d10Initialized)) 
    {
        return ASOP_IO_ERR_IO_UNINIT;
    }

    // Deactivate currently active Output device
    if (pOut->hMcaspChan != NULL)
    {
        // Reset channel
        status = mcaspControlChan(pOut->hMcaspChan, MCASP_CHAN_RESET, NULL);
        if (status != MCASP_COMPLETED)
        {
            Log_info0("asopSelectDevices(): McASP channel reset failed!\n");
            return ASOP_IO_ERR_MCASP_CFG;            
        }
        
        // Delete McASP LLD channel
        status = mcaspDeleteChan(pOut->hMcaspChan);
        if (status != MCASP_COMPLETED)
        {
            Log_info0("asopSelectDevices(): McASP channel deletion failed!\n");
            return ASOP_IO_ERR_MCASP_CFG;
        }
        
        // Clear (drop) already posted Output data events
        postedEvents = Event_getPostedEvents(gAsotEvtHandle);
        while ((postedEvents & Evt_Id_AsotTxMcaspEdma) != 0)
        {
            Event_pend(gAsotEvtHandle, Event_Id_NONE, Evt_Id_AsotTxMcaspEdma, 0);
            postedEvents = Event_getPostedEvents(gAsotEvtHandle);
        }        
        
        pOut->hMcaspChan = NULL;            // reset active McASP LLD handle
        pOut->pLldCfg->hMcaspChan = NULL;   // reset McASP LLD handle for active McASP LLD configuration
        pOut->pLldCfg = NULL;               // reset pointer to active McASP LLD configuration
    }

    // Activate requested device
    if (pOutCfg != NULL)
    {
        //
        // Device other than OutNone selected
        //
        
        pReqLldCfg = (mcaspLLDconfig *)pOutCfg->sio.pConfig;
        if (pReqLldCfg->hMcaspChan == NULL) 
        {
            // Create McASP LLD channel
            mcaspChanHandle = NULL;
            audStatus = mcasplldChanCreate(pReqLldCfg, &mcaspChanHandle);
            if (audStatus != Aud_EOK) 
            {
                Log_info0("asopSelectDevices(): McASP channel creation failed!\n");
                return ASOP_IO_ERR_MCASP_CFG;
            }

            pReqLldCfg->hMcaspChan = mcaspChanHandle;   // set McASP LLD handle for requested McASP LLD configuration
            pOut->pLldCfg = pReqLldCfg;                 // set pointer to active McASP LLD configuration
            pOut->hMcaspChan = pReqLldCfg->hMcaspChan;  // set active McASP LLD handle
            
            // configure stride according to selected McASP LLD configuration
            pOut->stride = pReqLldCfg->mcaspChanParams->noOfSerRequested * 
                pReqLldCfg->mcaspChanParams->noOfChannels;
                
            // initialize rateX
            pOut->rateX = 1.; // rateX==1.0 for CLKXDIV==1
        }
        else
        {
            return ASOP_IO_ERR_INV_STATE;
        }
    }
    else
    {
        //
        // OutNone device selected
        //
        
        pOut->hMcaspChan = NULL;    // reset active McASP LLD handle
        pOut->pLldCfg = NULL;       // reset pointer to active McASP LLD configuration
    }

    return ASOP_IO_SOK;
}

// Check if Output device SIO selection changed
Int checkOutDevSioSelUpdate(
    const PAF_ASOT_Params *pP, 
    PAF_ASOT_Config *pAsotCfg,
    Int z, 
    Bool *pOutDevSelUpdate
)
{
    PAF_AST_Config *pAstCfg;
    
    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration

    if ((z < OUTPUT1) || (z >= OUTPUTN))
    {
        *pOutDevSelUpdate = FALSE;
        return ASOP_IO_ERR_INV_PARAMS;
    }
    
    *pOutDevSelUpdate = (Bool)(pAstCfg->xOut[z].outBufStatus.sioSelect >= 0);

    return ASOP_IO_SOK;
}

// Check if any Output device SIO selection changed
Int checkAnyOutDevSioSelUpdate(
    const PAF_ASOT_Params *pP, 
    PAF_ASOT_Config *pAsotCfg,
    Bool *pOutDevSelUpdate
)
{
    PAF_AST_Config *pAstCfg;
    Int outDevSelUpdate;
    Int z;
    
    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration

    outDevSelUpdate = FALSE;
    for (z=OUTPUT1; z < OUTPUTN; z++) 
    {
        if (pAstCfg->xOut[z].outBufStatus.sioSelect >= 0)
        {
            outDevSelUpdate = TRUE;
            break;
        }
    }
    
    *pOutDevSelUpdate = outDevSelUpdate;

    return ASOP_IO_SOK;
}

// -----------------------------------------------------------------------------
// ASOT Decoding Function Helper - SIO Driver Change
//
//   Name:      PAF_ASOT_setCheckRateX
//   Purpose:   Decoding Function for reinitiating output.
//   From:      AST Parameter Function -> decodeInfo1
//              AST Parameter Function -> decodeInfo2
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     None.
//

/* 0: set, 1: check, unused for now. --Kurt */
Int asopSetCheckRateX(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg, 
    Int check
)
{
    PAF_AST_Config *pAstCfg;
    PAF_AST_IoOut  *pOut;
    float rateX;
    PAF_SampleRateHz rateO /* std */, rateI /* inv */;
    Int z;                              /* output counter */
    Int zx;                             /* output re-counter */
    Int getVal;
    int inputRate, inputCount, outputRate, outputCount;
    Int zMD;
    Int zMI;
    Int zMS;
    Int zE, zX;
    // "proof of concept" for McASP LLD API
    Uint32 divider, clkXDiv;
    Mcasp_HwSetupData mcaspSetup;
    Int32 status;

    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    pOut = pAsotCfg->pIoOut; // get pointer to ASOT IO configuration
    
    zMD = pAstCfg->masterDec;
    zMS = pAstCfg->masterStr;
    zMI = pP->zone.master;

    inputRate = pAstCfg->xInp[zMI].inpBufStatus.sampleRateStatus;
    inputCount = pAstCfg->xDec[zMD].decodeStatus.frameLength;
    rateI = pAstCfg->xStr[zMS].pAudioFrame->fxns->sampleRateHz(pAstCfg->xStr[zMS].pAudioFrame, 
        inputRate, PAF_SAMPLERATEHZ_INV);

    for (z=OUTPUT1; z < OUTPUTN; z++) 
    {
        if (pOut[z].hIoPhy && (pAstCfg->xOut[z].outBufStatus.clock & 0x01)) 
        {
            // determine associated encoder
            zE = z;
            for (zX = ENCODE1; zX < ENCODEN; zX++) 
            {
                if (pP->outputsFromEncodes[zX] == z) 
                {
                    zE = zX;
                    break;
                }
            }

            outputRate = pAstCfg->xEnc[zE].encodeStatus.sampleRate;
            outputCount = pAstCfg->xEnc[zE].encodeStatus.frameLength;
            rateO = pAstCfg->xStr[zMS].pAudioFrame->fxns->sampleRateHz(pAstCfg->xStr[zMS].pAudioFrame, 
                outputRate, PAF_SAMPLERATEHZ_STD);
            if ((rateI > 0) && (rateO > 0))
            {
                rateX = rateO /* std */ * rateI /* inv */;
            }
            else if (inputCount != 0)
            {
                rateX = (float)outputCount / inputCount;
            }
            else
            {
                return ASPERR_INFO_RATERATIO;
            }

            //
            // "Proof on concept" code for McASP LLD API to change bit clock divider.
            //
            if (pOut->rateX != rateX)
            {
                // Initialize divider value.
                // This works for AHCLKX input from HDMI & sample rate = 44.1,48,88.2,96,192 kHz.
                divider = 2;
                
                // Update divider based on calculated rateX
                divider /= rateX;
                
#if 0 // debug
                {
                    UInt32 regVal;

                    // Experimental code: directly write CLKXDIV -- produces correct output                
                    regVal = *(volatile UInt32 *)0x23400B0; // read MCASP_ACLKXCTL
                    regVal &= ~0x1F; // mask off CLKXDIV bits
                    //regVal |= 7; // set CLKXDIV for 48 kHz
                    //regVal |= 3; // set CLKXDIV for 96 kHz
                    //regVal |= 1; // set CLKXDIV for 192 kHz
                    regVal |= (divider-1); // set CLKXDIV
                    *(volatile UInt32 *)0x23400B0 = regVal; // write MCASP_ACLKXCTL                    
                }
#endif                  

                // Calculate CLKXDIV bit field value
                clkXDiv = (divider-1) & CSL_MCASP_ACLKXCTL_CLKXDIV_MASK;

                // update CLKXDIV setup
                status = mcaspControlChan(pOut->hMcaspChan, Mcasp_IOCTL_CNTRL_SET_FORMAT_CHAN_CLKXDIV, &clkXDiv);
                if (status != MCASP_COMPLETED)
                {
                    Log_info0("asopSetCheckRateX(): McASP set channel format CLKXDIV failed!\n");
                    return ASOP_IO_ERR_MCASP_CFG;
                }
                
                pOut->rateX = rateX; // update saved rateX
                
                return ASOP_IO_ERR_RATE_CHANGE;
            }
        }
    }

    return ASOP_IO_SOK;
} //asopSetCheckRateX

// -----------------------------------------------------------------------------
// ASOT Decoding Function Helper - SIO Driver Start
//
//   Name:      PAF_ASOT_startOutput
//   Purpose:   Decoding Function for initiating output.
//   From:      AST Parameter Function -> decodeInfo1
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * SIO control errors.
//
Int asopStartOutput(
    const PAF_ASOT_Params *pP,
    const PAF_ASOT_Patchs *pQ,
    PAF_ASOT_Config *pAsotCfg
)
{
    PAF_AST_Config *pAstCfg;
    PAF_AST_IoOut  *pOut;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* output counter */
    Int nbufs;
    Int zE, zS, zX;
    Int zMD;
    PAF_SIO_IALG_Obj    *pObj;
    PAF_SIO_IALG_Config *pAlgConfig;
    ioPhyCtl_t ioPhyCtl;


    pAstCfg = pAsotCfg->pAstCfg; // get pointer to common (shared) configuration
    pOut = pAsotCfg->pIoOut; // get pointer to ASOT IO configuration
    as = pAstCfg->as;
    zMD = pAstCfg->masterDec;

    for (z=OUTPUT1; z < OUTPUTN; z++)
    {
        if (pOut[z].hIoPhy)
        {
            // determine associated encoder and stream
            zE = z;
            zS = z;
            for (zX = ENCODE1; zX < ENCODEN; zX++)
            {
                if (pP->outputsFromEncodes[zX] == z)
                {
                    zE = zX;
                    zS = pP->streamsFromEncodes[zE];
                    break;
                }
            }

            // Set sample count so that DOB knows how much data to send
            pAstCfg->xOut[z].outBufConfig.lengthofFrame =
                pAstCfg->xEnc[zE].encodeInStruct.pAudioFrame->sampleCount;

            // Update framework Phy transfer size
            pOut[z].phyXferSize = pAstCfg->xOut[z].outBufConfig.lengthofFrame * pOut[z].stride * WORD_SIZE_PCM;
            // Update IO Phy transfer size
            ioPhyCtl.code = IOPHY_CTL_FRAME_SIZE;
            ioPhyCtl.params.xferFrameSize = pOut[z].phyXferSize;
            ioPhyControl(pOut[z].hIoPhy, &ioPhyCtl);
            // Update IO Buff delay to match Phy transfer size
            ioBuffAdjustDelay(pOut[z].hIoBuff, pOut[z].phyXferSize * (NUM_PRIME_XFERS_OUTPUT+1));

            if (pAstCfg->xOut[z].outBufStatus.markerMode == PAF_OB_MARKER_ENABLED)
            {
                pObj = (PAF_SIO_IALG_Obj *) pAstCfg->xOut[z].outChainData.head->alg;
                pAlgConfig = &pObj->config;
                memset(pAstCfg->xOut[z].outBufConfig.base.pVoid, 0xAA,
                    pAlgConfig->pMemRec[0].size);
            }

            // The index to DEC_OUTNUMBUF_MAP will always come from the primary/master
            // decoder. How should we handle the sourceProgram for multiple decoders?
            // Override as needed
            nbufs = DEC_OUTNUMBUF_MAP(pAstCfg->xDec[zMD].decodeStatus.sourceProgram);
            if (pAstCfg->xOut[z].outBufStatus.numBufOverride[pAstCfg->xDec[zMD].decodeStatus.sourceProgram] > 0)
            {
                nbufs = pAstCfg->xOut[z].outBufStatus.numBufOverride[pAstCfg->xDec[zMD].decodeStatus.sourceProgram];
            }

            TRACE_VERBOSE1("PAF_ASOT_startOutput: AS%d: output started", as+zS);
        }
    }

    return ASOP_IO_SOK;
} /* asopStartOutput */

// -----------------------------------------------------------------------------
// ASOT Decoding Function Helper - SIO Driver Stop
//
//   Name:      PAF_ASOT_stopOutput
//   Purpose:   Decoding Function for terminating output.
//   From:      AST Parameter Function -> decodeProcessing
//              AST Parameter Function -> decodeComplete
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * SIO control errors.
//
Int asopStopOutput(
    const PAF_ASOT_Params *pP, 
    const PAF_ASOT_Patchs *pQ, 
    PAF_ASOT_Config *pAsotCfg
)
{
    PAF_AST_Config *pAstCfg;
    PAF_AST_IoOut  *pOut;
    Int as;                     /* Audio Stream Number (1, 2, etc.) */
    Int z;                      /* output counter */
    Int errno, getVal;
    Int zS, zX;
    PAF_SIO_IALG_Obj    *pObj;
    PAF_SIO_IALG_Config *pAlgConfig;

    pAstCfg = pAsotCfg->pAstCfg; // get pointer to AST common (shared) configuration
    pOut = pAsotCfg->pIoOut; // get pointer to ASOT IO configuration
    as = pAstCfg->as;
    (void)as;  // clear compiler warning in case not used with tracing disabled

    errno = ASOP_IO_SOK;
    for (z=OUTPUT1; z < OUTPUTN; z++) 
    {
        if (pOut[z].hIoPhy) 
        {
            // determine associated encoder and stream
            zS = z;
            (void)zS;
            for (zX = ENCODE1; zX < ENCODEN; zX++) 
            {
                if (pP->outputsFromEncodes[zX] == z) 
                {
                    zS = pP->streamsFromEncodes[zX];
                    break;
                }
            }

            // zero output buffers
            pObj = (PAF_SIO_IALG_Obj *) pAstCfg->xOut[z].outChainData.head->alg;
            pAlgConfig = &pObj->config;
            memset (pAstCfg->xOut[z].outBufConfig.base.pVoid, 0, pAlgConfig->pMemRec[0].size);
        } //pAstCfg->xOut[z].hTxSio
    }//OUTPUT

    return errno;
} //asopStopOutput

/*===========================================================================
 * Initialize I/O components for output processing
============================================================================*/
Int asopIoCompsInit(
    Int16 strAfSampleCount,     // stream audio frame sample count
    PAF_AST_OutBuf *pOutBuf,    // pointer to Output Buffer
    PAF_AST_IoOut *pOutIo       // pointer to Output IO
)
{
    ioBuffParams_t ioBuffParams;
    ioPhyParams_t  ioPhyParams;

    if (pOutIo->hMcaspChan != NULL)
    {
        // Initialize I/O BUFF and I/O PHY components for output task
        ioBuffParams.base         = pOutBuf->outBufConfig.base.pVoid;
        // Set IO buffer size to multiple of audio frame sample count x stride x size of element.
        // This ensures no split buffers will be allocated on Output buffer wrap.
        ioBuffParams.size         = pOutBuf->outBufConfig.allocation/(strAfSampleCount * pOutIo->stride * WORD_SIZE_PCM)*
            (strAfSampleCount * pOutIo->stride * WORD_SIZE_PCM);
        ioBuffParams.sync         = IOBUff_READ_SYNC;
        ioBuffParams.nominalDelay = strAfSampleCount * pOutIo->stride * WORD_SIZE_PCM * (NUM_PRIME_XFERS_OUTPUT+1);
        if (ioBuffInit(pOutIo->hIoBuff, &ioBuffParams) != IOBUFF_NOERR) 
        {
            return ASOP_IO_ERR_IOBUFF_INIT;   // to remove magic number
        }

        ioPhyParams.ioBuffHandle    = pOutIo->hIoBuff;
        ioPhyParams.xferFrameSize   = strAfSampleCount * pOutIo->stride * WORD_SIZE_PCM;
        ioPhyParams.mcaspChanHandle = pOutIo->hMcaspChan;
        ioPhyParams.ioBuffOp        = IOPHY_IOBUFFOP_READ;
        if (ioPhyInit(pOutIo->hIoPhy, &ioPhyParams) != IOPHY_NOERR) 
        {
            return ASOP_IO_ERR_IOPHY_INIT;   // to remove magic number
        }

        pOutIo->phyXferSize = ioPhyParams.xferFrameSize;
        
        pOutIo->ioBuffBuf2AllocCnt = 0; // initialize buffer2 alloc count (indicates Output buffer wrap)
        pOutIo->errIoBuffOvrCnt = 0;    // initialize IO buff overflow count
        pOutIo->errIoBuffUndCnt = 0;    // initialize IO buff underflow count
    }
    
    return ASOP_IO_SOK;
} /* asopIoCompsInit */

/*======================================================================================
 *  This function checks whether the I/O physical layer has been initialized
 *====================================================================================*/
Bool asopIoPhyCheckInit(Void)
{
    if (!d10Initialized)
        return FALSE;
    else 
        return TRUE;
}

/*======================================================================================
 *  I/O physical layer prime operation required by McASP LLD
 *====================================================================================*/
Void asopIoPhyPrime(
    PAF_AST_IoOut *pOut
)
{
    Int32        count;

    pOut->numPrimeXfers = NUM_PRIME_XFERS_OUTPUT;

    for (count = 0; count < pOut->numPrimeXfers; count++)
    {
        ioPhyXferSubmit(pOut->hIoPhy);
    }
} /* asipIoPhyPrime */

// Initialize Output buffer configuration
Int asopInitOutBufConfig(
    PAF_AST_OutBuf *pOutBuf, 
    PAF_AST_IoOut *pOutIo
)
{
    PAF_OutBufConfig *pOutBufCfg;
    ioBuffHandle_t hIoBuff;
    ioBuffInfo_t outBuffInfo;
	GateMP_Handle gateHandle;
	IArg key;
    
    pOutBufCfg = &pOutBuf->outBufConfig;
    hIoBuff = pOutIo->hIoBuff;
    
    pOutBufCfg->stride = pOutIo->stride;
    pOutBufCfg->sizeofElement = WORD_SIZE_PCM;
    pOutBufCfg->precision = 24;
    
    // Get gate handle
    gateHandle = pOutIo->gateHandle;
    // Enter gate
    key = GateMP_enter(gateHandle);
    ioBuffGetInfo(hIoBuff, &outBuffInfo);
    // Leave the gate
    GateMP_leave(gateHandle, key);
    pOutBufCfg->base.pLgInt = outBuffInfo.base;
    pOutBufCfg->sizeofBuffer = outBuffInfo.size;
    
    pOutBuf->pOutBuf = &(pOutBuf->outBufConfig);
    
    return ASOP_IO_SOK;
}

Int asopGetOutBufPtrs(
    PAF_AST_IoOut *pOutIo,
    size_t writeSize
)
{
    void *buff1, *buff2;
    size_t size1, size2;
    Int status;
    GateMP_Handle gateHandle;
    IArg key;

    // Get gate handle
    gateHandle = pOutIo->gateHandle;
    // Enter gate
    key = GateMP_enter(gateHandle);
    status = ioBuffGetWritePtrs(pOutIo->hIoBuff, writeSize,
                                &buff1, &size1, &buff2, &size2);
    // Leave the gate
    GateMP_leave(gateHandle, key);
    if (status == IOBUFF_ERR_OVERFLOW)
    {
        pOutIo->errIoBuffOvrCnt++;
        //System_printf ("asopGetOutBufPtrs: output buff overflow\n"); // debug

        // skip processing since output buffer overflows
        return ASOP_IO_ERR_OUTBUF_OVERFLOW;
    }
    else if (status == IOBUFF_ERR_UNDERFLOW)
    {
        pOutIo->errIoBuffUndCnt++;
        //System_printf ("asopGetOutBufPtrs: output buff underflow\n"); // debug

        // already underflows and remain in underflow
    }
    
    if ((buff2 != NULL) || (size2 != 0))
    {
        // Two buffers allocated indicates split buffer allocation on buffer wrap.
        pOutIo->ioBuffBuf2AllocCnt++; // increment count of allocated buffer2
    }

    // save buffer pointers & sizes for later write complete
    pOutIo->buff1 = buff1;
    pOutIo->size1 = size1;
    pOutIo->buff2 = buff2;
    pOutIo->size2 = size2;

    return ASOP_IO_SOK;
}


// Mark Output buffers write complete
Int asopMarkOutBuffsWriteComplete(
    PAF_AST_OutBuf *pOutBuf, 
    PAF_AST_IoOut *pOutIo
)
{
    ioBuffHandle_t hIoBuff;
    void *buff1, *buff2;
    size_t size1, size2;
	GateMP_Handle gateHandle;
	IArg key;
    
    // get buffer pointers & sizes from previous IO Buff write allocation
    buff1 = pOutIo->buff1;
    size1 = pOutIo->size1;
    buff2 = pOutIo->buff2; // this should always be NULL for Output
    size2 = pOutIo->size2; // this should always be 0 for Output
    
    hIoBuff = pOutIo->hIoBuff;

         // Get gate handle
    gateHandle = pOutIo->gateHandle;
    // Enter gate
    key = GateMP_enter(gateHandle);
    ioBuffWriteComplete(hIoBuff, buff1, size1);
    // Leave the gate
    GateMP_leave(gateHandle, key);
    if (buff2 != NULL) 
    {
        ioBuffWriteComplete(hIoBuff, buff2, size2);
    }
    
    return ASOP_IO_SOK;
}

/*======================================================================================
 *  This function starts an I/O PHY transfer for output
 *====================================================================================*/
Void asopPhyTransferStart(
    PAF_AST_IoOut *pOut
)
{
    if(mcaspCheckOverUnderRun(pOut->hMcaspChan)) 
    {
        //mcaspTxReset();
        //mcaspTxCreate();
        //pOut->hMcaspChan = hMcaspTxChan;
        System_abort("\nMcASP for output underruns! %d!\n");
    }
    else 
    {
        if(ioPhyXferSubmit(pOut->hIoPhy) == IOPHY_ERR_BUFF_UNDERFLOW) 
        {
            // Output buffer underflows!
            //System_abort("\nOutput buffer underflows!\n");
        }
        else {
            // Output buffer operates normally
            ;
        }
    }
}

/* nothing past this point */
