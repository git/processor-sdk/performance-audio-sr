
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ======== dbgIdle.c ========
 */

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/System.h>


#define MEMSTAT_REPORT
#ifdef MEMSTAT_REPORT
#include "paf_heapMgr.h"
#include "paf_alg_print.h"
UInt32 gRunIdleMemStatRpt=0; // export to other threads to allow Idle memstat to run
#endif /* MEMSTAT_REPORT */

/*
 *  ======== idleNotifyInfoChange ========
 *  IDLE function for debugging.
 *  Can be safely removed from build.
 */
Void idleDebug()
{
#ifdef MEMSTAT_REPORT
    if (gRunIdleMemStatRpt == 1)
    {
        // Output current heap memory usage
        PAF_ALG_memStatusPrint("IDLE MEMSTAT REPORT", 
            PAF_HEAP_INT, PAF_HEAP_INT1, PAF_HEAP_EXT, 
            PAF_HEAP_INT1_SHM, PAF_HEAP_EXT_SHM, PAF_HEAP_EXT_NONCACHED_SHM);
        gRunIdleMemStatRpt = 0;
    }
#endif    
}
