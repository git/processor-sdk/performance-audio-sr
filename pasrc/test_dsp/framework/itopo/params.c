
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// PAF_DEVICE_VERSION Symbol Definitions

#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)

#include <xdc/cfg/global.h>

//#include <ti/procsdk_audio/procsdk_audio_typ.h>
#include <procsdk_audio_typ.h>

#include "paftyp.h"
#include "paf_ialg.h"
#include <stdasp.h>
#include <pafsio_ialg.h>

#include "paf_heapMgr.h"

#include "asp1.h"
#include "audioStreamProc_params.h"
#include "audioStreamInpProc.h"
#include "audioStreamOutProc.h"

#include "dbgBenchmark.h" // PCM high-sampling rate + SRC + CAR benchmarking

//
// Framework Declarations
//

//#define PAF_AST_params_fxnsPA PAF_AST_params_fxnsPA17

//
// Audio Data Representation Definitions
//
//   External declarations to patched IROM provide standard functionality.
//

/* audio frame "width" in channels */

const SmInt PAF_AST_params_numchan[1] =
{
    32,
};

///
// Audio Stream Processing Function Table Definition
//

const PAF_AudioFunctions PAF_AST_params_audioFrameFunctions =
{
    &PAF_ASP_dB2ToLinear,
    &PAF_ASP_channelMask,
    &PAF_ASP_programFormat,
    &PAF_ASP_sampleRateHz,
    &PAF_ASP_delay,
};

//
// Source Select Array Declarations -- algorithm keys & sio map
//
//   External declarations to patched IROM provide standard functionality.
//

const PAF_ASP_AlgKey PAF_ASP_params_decAlgKey =
{
    PAF_SOURCE_N,                                           // length
    /* Relies on the fact that ACP_SERIES_* != 0 here */
    0,                                                      // PAF_SOURCE_UNKNOWN
    0,                                                      // PAF_SOURCE_NONE
    0,                                                      // PAF_SOURCE_PASS
    PAF_ASP_ALPHACODE (STD, SNG),                           // PAF_SOURCE_SNG
    0,                                                      // PAF_SOURCE_AUTO
    0,                                                      // PAF_SOURCE_BITSTREAM
    PAF_ASP_ALPHACODE (STD, DTSUHDA),                         // PAF_SOURCE_DTSALL
    PAF_ASP_ALPHACODE (STD, PCM),                           // PAF_SOURCE_PCMAUTO
    PAF_ASP_ALPHACODE (STD, PCM),                           // PAF_SOURCE_PCM
    PAF_ASP_ALPHACODE (STD, PCN),                           // PAF_SOURCE_PC8 /* unused */
    PAF_ASP_ALPHACODE (STD, DDP),                           // PAF_SOURCE_AC3
    PAF_ASP_ALPHACODE (STD, DTSUHDA),                         // PAF_SOURCE_DTS
    PAF_ASP_ALPHACODE (STD, AAC),                           // PAF_SOURCE_AAC
    PAF_ASP_ALPHACODE (STD, MPG),                           // PAF_SOURCE_MPEG /* unused */
    PAF_ASP_ALPHACODE (STD, DTSUHDA),                         // PAF_SOURCE_DTS12
    PAF_ASP_ALPHACODE (STD, DTSUHDA),                         // PAF_SOURCE_DTS13
    PAF_ASP_ALPHACODE (STD, DTSUHDA),                         // PAF_SOURCE_DTS14
    PAF_ASP_ALPHACODE (STD, DTSUHDA),                         // PAF_SOURCE_DTS16
    0,                                                      // PAF_SOURCE_WMA9PRO
    0,                                                      // PAF_SOURCE_MP3
    PAF_ASP_ALPHACODE (STD, DSD),                           // PAF_SOURCE_DSD1
    PAF_ASP_ALPHACODE (STD, DSD),                           // PAF_SOURCE_DSD2
    PAF_ASP_ALPHACODE (STD, DSD),                           // PAF_SOURCE_DSD3
    PAF_ASP_ALPHACODE (STD, DDP),                           // PAF_SOURCE_DDP
    PAF_ASP_ALPHACODE (STD, DTSUHDA),                       // PAF_SOURCE_DTSHD
    PAF_ASP_ALPHACODE (STD, THD),                           // PAF_SOURCE_THD
    PAF_ASP_ALPHACODE (STD, DXP),                           // PAF_SOURCE_DXP
};

const PAF_ASP_AlgKey PAF_ASP_params_encAlgKey =
{
    PAF_SOURCE_N,                                           // length
    /* Relies on the fact that ACP_SERIES_* != 0 here */
    0,                                                      // PAF_SOURCE_UNKNOWN
    0,                                                      // PAF_SOURCE_NONE
    0,                                                      // PAF_SOURCE_PASS
    0,                                                      // PAF_SOURCE_SNG
    0,                                                      // PAF_SOURCE_AUTO
    0,                                                      // PAF_SOURCE_BITSTREAM
    0,                                                      // PAF_SOURCE_DTSALL
    0,                                                      // PAF_SOURCE_PCMAUTO
    PAF_ASP_ALPHACODE (STD, PCE),                           // PAF_SOURCE_PCM
    0,                                                      // PAF_SOURCE_PC8
    0,                                                      // PAF_SOURCE_AC3
    0,                                                      // PAF_SOURCE_DTS
    0,                                                      // PAF_SOURCE_AAC
    0,                                                      // PAF_SOURCE_MPEG
    0,                                                      // PAF_SOURCE_DTS12
    0,                                                      // PAF_SOURCE_DTS13
    0,                                                      // PAF_SOURCE_DTS14
    0,                                                      // PAF_SOURCE_DTS16
    0,                                                      // PAF_SOURCE_WMA9PRO
    PAF_ASP_ALPHACODE (STD, MPE),                           // PAF_SOURCE_MP3
    0,                                                      // PAF_SOURCE_DSD1
    0,                                                      // PAF_SOURCE_DSD2
    0                                                       // PAF_SOURCE_DSD3
};

const PAF_ASP_SioMap PAF_ASP_params_decSioMap =
{
    PAF_SOURCE_N,                                           // length
    PAF_SOURCE_UNKNOWN,                                     // PAF_SOURCE_UNKNOWN -> ...
    PAF_SOURCE_NONE,                                        // PAF_SOURCE_NONE -> ...
    PAF_SOURCE_PASS,                                        // PAF_SOURCE_PASS -> ...
    PAF_SOURCE_PCM,                                         // PAF_SOURCE_SNG -> PCM
    PAF_SOURCE_AUTO,                                        // PAF_SOURCE_AUTO -> ...
    PAF_SOURCE_BITSTREAM,                                   // PAF_SOURCE_BITSTREAM -> ...
    PAF_SOURCE_DTSALL,                                      // PAF_SOURCE_UNUSED1 -> ...
    PAF_SOURCE_PCMAUTO,                                     // PAF_SOURCE_UNUSED2 -> ...
    PAF_SOURCE_PCM,                                         // PAF_SOURCE_PCM -> ...
    PAF_SOURCE_PC8,                                         // PAF_SOURCE_PC8 -> ...
    PAF_SOURCE_AC3,                                         // PAF_SOURCE_AC3 -> ...
    PAF_SOURCE_DTS,                                         // PAF_SOURCE_DTS -> ...
    PAF_SOURCE_AAC,                                         // PAF_SOURCE_AAC -> ...
    PAF_SOURCE_MPEG,                                        // PAF_SOURCE_MPEG -> ...
    PAF_SOURCE_DTS12,                                       // PAF_SOURCE_DTS12 -> ...
    PAF_SOURCE_DTS13,                                       // PAF_SOURCE_DTS13 -> ...
    PAF_SOURCE_DTS14,                                       // PAF_SOURCE_DTS14 -> ...
    PAF_SOURCE_DTS16,                                       // PAF_SOURCE_DTS16 -> ...
    PAF_SOURCE_WMA9PRO,                                     // PAF_SOURCE_WMA9PRO -> ...
    PAF_SOURCE_MP3,                                         // PAF_SOURCE_MP3 -> ...
    PAF_SOURCE_DSD1,                                        // PAF_SOURCE_DSD1 -> ...
    PAF_SOURCE_DSD2,                                        // PAF_SOURCE_DSD2 -> ...
    PAF_SOURCE_DSD3,                                        // PAF_SOURCE_DSD3 -> ...
    PAF_SOURCE_DDP,                                         // PAF_SOURCE_DDP -> ...
    PAF_SOURCE_DTSHD,                                       // PAF_SOURCE_DTSHD -> ...
    PAF_SOURCE_THD,                                         // PAF_SOURCE_THD -> ...
    PAF_SOURCE_DXP,                                         // PAF_SOURCE_DXP -> ...
};

//
// Mapping Declarations -- from *coders to *puts
//
//   External declarations to patched IROM provide standard functionality.
//
const SmInt PAF_AST_streamsFromDecodes_std[DECODEN_MAX] =
{
    0, 1, 2,
};
const SmInt PAF_AST_streamsFromEncodes_std[ENCODEN_MAX] =
{
    0, 1, 2,
};
const SmInt PAF_AST_inputsFromDecodes_std[DECODEN_MAX] =
{
    0, 1, 2,
};
extern const SmInt PAF_AST_outputsFromEncodes_std[ENCODEN_MAX];
#ifndef __TI_EABI__
asm ("_PAF_AST_outputsFromEncodes_std .set _PAF_AST_inputsFromDecodes_std");
#else
asm ("PAF_AST_outputsFromEncodes_std .set PAF_AST_inputsFromDecodes_std");
#endif

//
// Setting of audio stream order
//

const SmInt PAF_AST_streamOrder[1] =
{
    0,
};

//
// Audio Framework Initialization Function Table Declarations
//
//   External declarations to patched IROM provide standard functionality.
//

extern const PAF_ASIT_Fxns PAF_ASIT_params_fxns;

//
// Audio Framework Status Initialization Declarations
//
//   External declarations to patched IROM provide standard functionality.
//

const PAF_InpBufStatus PAF_AST_params_inpBufStatus =
{
    sizeof (PAF_InpBufStatus),                              // size
    0,                                                      // mode
    0,                                                      // sioSelect
    0x80,                                                   // lockOverride = none
    0,                                                      // unused
    PAF_SAMPLERATE_UNKNOWN,                                 // sampleRateOverride
    PAF_SAMPLERATE_NONE,                                    // sampleRateData
    PAF_SAMPLERATE_NONE,                                    // sampleRateMeasured
    PAF_SAMPLERATE_UNKNOWN,                                 // sampleRateStatus
    PAF_IEC_AUDIOMODE_UNKNOWN,                              // nonaudio
    PAF_IEC_PREEMPHASIS_UNKNOWN,                            // emphasisData
    0,                                                      // emphasisOverride
    PAF_IEC_PREEMPHASIS_NO,                                 // emphasisStatus
    0,                                                      // lock
    0,                                                      // scanAtHighSampleRateMode
    0,                                                      // zeroRun
    0,                                                      // rateTrackMode
    24,                                                     // precisionDefault
    -1,                                                     // precisionDetect
    0,                                                      // precisionOverride
    0,                                                      // precisionInput
    100,                                                    // zeroRunTrigger
    100,                                                    // zeroRunRestart
    2048,                                                   // unknownTimeout
    0,                                                      // lastFrameMask
    0,                                                      // lastFrameFlag
    0,                                                      // reportDTS16AsDTSForLargeSampleRate
    0, 0                                                    // reserved
};

const PAF_OutBufStatus PAF_AST_params_outBufStatus =
{
    sizeof (PAF_OutBufStatus),                              // size
    1,                                                      // mode
    0,                                                      // sioSelect
    PAF_SAMPLERATE_UNKNOWN,                                 // sampleRate
    0,                                                      // audio
    PAF_OB_CLOCK_INTERNAL,                                  // clock
    PAF_OB_FLUSH_DISABLE,                                    // flush		!!! FLUSH_ENABLE not supported, yet !!!
    0,                                                      // rateTrackMode // unused
    PAF_OB_MARKER_DISABLED,                                 // markerMode
    0,                                                      // maxNumBufOverride
                                                            // this is the start of numBufOverride[PAF_SOURCE_N]                                                            
    0,                                                      // PAF_SOURCE_UNKNOWN
    0,                                                      // PAF_SOURCE_NONE
    0,                                                      // PAF_SOURCE_PASS
    0,                                                      // PAF_SOURCE_SNG
    0,                                                      // PAF_SOURCE_AUTO
    0,                                                      // PAF_SOURCE_BITSTREAM
    0,                                                      // PAF_SOURCE_DTSALL
    0,                                                      // PAF_SOURCE_PCMAUTO
    0,                                                      // PAF_SOURCE_PCM
    0,                                                      // PAF_SOURCE_PC8
    0,                                                      // PAF_SOURCE_AC3
    0,                                                      // PAF_SOURCE_DTS
    0,                                                      // PAF_SOURCE_AAC
    0,                                                      // PAF_SOURCE_MPEG
    0,                                                      // PAF_SOURCE_DTS12
    0,                                                      // PAF_SOURCE_DTS13
    0,                                                      // PAF_SOURCE_DTS14
    0,                                                      // PAF_SOURCE_DTS16
    0,                                                      // PAF_SOURCE_WMA9PRO
    0,                                                      // PAF_SOURCE_MP3
    0,                                                      // PAF_SOURCE_DSD1,
    0,                                                      // PAF_SOURCE_DSD2,
    0,                                                      // PAF_SOURCE_DSD3,
    0,                                                      // PAF_SOURCE_DDP
    0,                                                      // PAF_SOURCE_DTSHD
    0,                                                      // PAF_SOURCE_THD
    0,                                                      // PAF_SOURCE_DXP                                                          
};

const PAF_EncodeStatus PAF_AST_params_encodeStatus_primary =
{
    sizeof (PAF_EncodeStatus),                              // size
    1,                                                      // mode
    0,                                                      // command2
    PAF_SAMPLERATE_UNKNOWN,                                 // sampleRate
    0,                                                      // channelCount
    0, 0, 0, 0,                                             // Unused                                        
    0, 0, 0, 0,                                             // Unused
    0, 0, 0, 0,                                             // Unused
    0, 0, 0, 0,                                             // Unused
    0, 0, 0, 0,                                             // Unused
    0,                                                      // frameLength
    0,                                                      // encBypass
    PAF_SOURCE_PCM,                                         // select
    // channelConfigurationRequest.full
    PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, PAF_CC_AUX_SURROUND4_UNKNOWN, 0,0,0,0,0,
    // channelConfigurationStream.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0,0,0,0,0,
    // channelConfigurationEncode.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0,0,0,0,0,    
    0, 0, 0, 0,    0, 0, 0, 0,                              // Unused
    -3,                                                     // channelMap.from[0]
    -3,                                                     // channelMap.from[1]
    -3,                                                     // channelMap.from[2]
    -3,                                                     // channelMap.from[3]
    -3,                                                     // channelMap.from[4]
    -3,                                                     // channelMap.from[5]
    -3,                                                     // channelMap.from[6]
    -3,                                                     // channelMap.from[7]
    -3,                                                     // channelMap.from[8]
    -3,                                                     // channelMap.from[9]
    -3,                                                     // channelMap.from[10]
    -3,                                                     // channelMap.from[11]
    -3,                                                     // channelMap.from[12]
    -3,                                                     // channelMap.from[13]
    -3,                                                     // channelMap.from[14]
    -3,                                                     // channelMap.from[15]
    -3,                                                     // channelMap.from[16]
    -3,                                                     // channelMap.from[17]
    -3,                                                     // channelMap.from[18]
    -3,                                                     // channelMap.from[19]
    -3,                                                     // channelMap.from[20]
    -3,                                                     // channelMap.from[21]
    -3,                                                     // channelMap.from[22]
    -3,                                                     // channelMap.from[23]
    -3,                                                     // channelMap.from[24]
    -3,                                                     // channelMap.from[25]
    -3,                                                     // channelMap.from[26]
    -3,                                                     // channelMap.from[27]
    -3,                                                     // channelMap.from[27]
    -3,                                                     // channelMap.from[28]
    -3,                                                     // channelMap.from[30]
    -3,                                                     // channelMap.from[31]
    -3,                                                     // channelMap.to[0]
    -3,                                                     // channelMap.to[1]
    -3,                                                     // channelMap.to[2]
    -3,                                                     // channelMap.to[3]
    -3,                                                     // channelMap.to[4]
    -3,                                                     // channelMap.to[5]
    -3,                                                     // channelMap.to[6]
    -3,                                                     // channelMap.to[7]
    -3,                                                     // channelMap.to[8]
    -3,                                                     // channelMap.to[9]
    -3,                                                     // channelMap.to[10]
    -3,                                                     // channelMap.to[11]
    -3,                                                     // channelMap.to[12]
    -3,                                                     // channelMap.to[13]
    -3,                                                     // channelMap.to[14]
    -3,                                                     // channelMap.to[15]
    -3,                                                     // channelMap.to[16]
    -3,                                                     // channelMap.to[17]
    -3,                                                     // channelMap.to[18]
    -3,                                                     // channelMap.to[19]
    -3,                                                     // channelMap.to[20]
    -3,                                                     // channelMap.to[21]
    -3,                                                     // channelMap.to[22]
    -3,                                                     // channelMap.to[23]
    -3,                                                     // channelMap.to[24]
    -3,                                                     // channelMap.to[25]
    -3,                                                     // channelMap.to[26]
    -3,                                                     // channelMap.to[27]
    -3,                                                     // channelMap.to[28]
    -3,                                                     // channelMap.to[29]
    -3,                                                     // channelMap.to[30]
    -3,                                                     // channelMap.to[31]
    1,                                                      // programFormat.mask
    0,                                                      // programFormat.form
    0,                                                      // listeningFormat.mask
    0,                                                      // listeningFormat.form
    0,                                                      // sampleProcess[0]
    0, 0, 0, 0,                                             // Unused     
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,  // channelConfigurationCompact                                      
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, //channelCompact[PAF_MAXNUMCHAN_HD]
};
                                       



const PAF_EncodeStatus *const PAF_AST_params_encodeStatus[] =
{
    &PAF_AST_params_encodeStatus_primary,
};

const PAF_VolumeStatus PAF_AST_params_volumeStatus =
{
    sizeof (PAF_VolumeStatus),                              // size
    1,                                                      // mode
    PAF_MAXNUMCHAN,                                         // channelCount
    0x0f,                                                   // implementation
    0,                                                      // unused1
    50,                                                     // rampTime: 50 msec/dB (20 dB/sec)
    0,                                                      // unused2
    0,                                                      // unused3
    -2 * 20, 0, 0, 0,                                       // master
    -2 * 0, 0, 0, 0,                                        // trim
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //  
    -2 * 0, 0, 0, 0,                                        // trim - upper16
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //

};

//
// Common Space Parameter Declarations and Definitions
//
//   Local definitions in RAM provide non-standard functionality.
//   The NULL pointer provides standard functionality.
//

/* baseline definition - NULL equivalent */
/* May be used for overrides of IALG_MemSpace */

static const IALG_MemSpace params_memspace_PAi[] = {
    PAF_IALG_NONE,                                          // Scratch
    PAF_IALG_NONE,                                          // Persistant
    PAF_IALG_NONE,                                          // Write once
    PAF_IALG_NONE,                                          // Common 1
    PAF_IALG_NONE,                                          // Common 2
    PAF_IALG_NONE,                                          // Common 3
    PAF_IALG_NONE,                                          // Common 4
    PAF_IALG_NONE,                                          // Common 5
    PAF_IALG_NONE,                                          // Common 6
    PAF_IALG_NONE,                                          // Common 7
    IALG_EXTERNAL,                                          // Common 8
    PAF_IALG_NONE,                                          // Common 9
    PAF_IALG_NONE,                                          // Common 10
    PAF_IALG_NONE,                                          // Common 11
    PAF_IALG_NONE,                                          // Common 12
    PAF_IALG_NONE,                                          // Common 13
    PAF_IALG_NONE,                                          // Common 14
    PAF_IALG_NONE,                                          // Common 15
};

//
// Heap Declarations
//

//#include <pafhjt.h>

//extern int IRAM;
//extern int SDRAM;
//extern int L3RAM;

// .............................................................................
// DIB memory requirements

// primary input
// IALG_SARAM_SHM: MSMC shared memory heap
// IALG_EXTERNAL_SHM: DDR shared memory heap
const IALG_MemRec inpMemTabPrimary[] =
{
    // SDRAM circular buffer
    // 2 buffers * 256 samples/buffer * 8 words/sample * 3 bytes/word
    {
        (6 * 60 * 1024) + 2,                                    // size 360 kB + 1 16-bit word for EDMA padding
        //(4 * 60 * 1024) + 2,                                    // size 240 kB + 1 16-bit word for EDMA padding
        //180 * 1024,                                           // size
        //6 * 60 * 1024,                                        // size
        128,                                                    // alignment
		IALG_SARAM_SHM, //IALG_EXTERNAL_SHM, //IALG_SARAM2, //IALG_SARAM1, //IALG_EXTERNAL,                           // space
        IALG_PERSIST,                                           // attrs
        NULL,                                                   // base
     }

};

const PAF_SIO_IALG_Params inpSioAlgParamsPrimary =
{
    1,
    inpMemTabPrimary
};

const PAF_ASP_LinkInit inpLinkInitPrimary[] =
{
    PAF_ASP_LINKINITPARAMS (STD, IB, TIH, &inpSioAlgParamsPrimary),
    PAF_ASP_LINKNONE
};

const PAF_ASP_LinkInit *const inpLinkInit[] =
{
    inpLinkInitPrimary
};

// .............................................................................
// DOB memory requirements

// primary output
const IALG_MemRec outMemTabPrimary[] =
{
    // SDRAM buffer
    // 4 buffers * PAF_ASOT_MAX_FRAMELENGTH samples * 16 ch (assumed MAX) * 4 bytes
        {
            4 * PAF_ASOT_MAX_FRAMELENGTH * 16 * 4,                                      // size: note:
            //6 * 256 * 32 * 4,                                      // size: note:
            //16 * 256 * 32 * 4,                                      // size: note:
            128,                                                   // alignment
            IALG_SARAM,                                         // space
            IALG_PERSIST,                                          // attrs
            NULL,                                                  // base
        }
};


const PAF_SIO_IALG_Params outSioAlgParamsPrimary =
{
    1,
    outMemTabPrimary
};

const PAF_ASP_LinkInit outLinkInitPrimary[] =
{
    PAF_ASP_LINKINITPARAMS (STD, OB, TIH, &outSioAlgParamsPrimary),
    PAF_ASP_LINKNONE
};

const PAF_ASP_LinkInit * const outLinkInit[] =
{
    outLinkInitPrimary,
};


// .............................................................................
// sourceProgram mapped to DOB num of buffers
const PAF_ASP_outNumBufMap outPrimaryNumBufMap =
{
    4,                                                      // maxNumBuf
    PAF_SOURCE_N,                                           // length
                                                            // this is the start of map[PAF_SOURCE_N]                                                            
    0,                                                      // PAF_SOURCE_UNKNOWN
    0,                                                      // PAF_SOURCE_NONE
    2,                                                      // PAF_SOURCE_PASS
    2,                                                      // PAF_SOURCE_SNG
    0,                                                      // PAF_SOURCE_AUTO
    0,                                                      // PAF_SOURCE_BITSTREAM
    0,                                                      // PAF_SOURCE_DTSALL
    0,                                                      // PAF_SOURCE_PCMAUTO
    2,                                                      // PAF_SOURCE_PCM
    0,                                                      // PAF_SOURCE_PC8
    2,                                                      // PAF_SOURCE_AC3
    2,                                                      // PAF_SOURCE_DTS
    2,                                                      // PAF_SOURCE_AAC
    0,                                                      // PAF_SOURCE_MPEG
    2,                                                      // PAF_SOURCE_DTS12
    2,                                                      // PAF_SOURCE_DTS13
    2,                                                      // PAF_SOURCE_DTS14
    2,                                                      // PAF_SOURCE_DTS16
    0,                                                      // PAF_SOURCE_WMA9PRO
    0,                                                      // PAF_SOURCE_MP3
    2,                                                      // PAF_SOURCE_DSD1,
    2,                                                      // PAF_SOURCE_DSD2,
    2,                                                      // PAF_SOURCE_DSD3,
    4,                                                      // PAF_SOURCE_DDP
    4,                                                      // PAF_SOURCE_DTSHD
    4,                                                      // PAF_SOURCE_THD
    4,                                                      // PAF_SOURCE_DXP
};

const PAF_ASP_outNumBufMap *const outNumBufMap[] =
{
    &outPrimaryNumBufMap,
};

// .............................................................................
const PAF_MetadataBufStatus PAF_AST_params_MetadataBufStatus[] =
{
    PAF_MAX_PRIVATE_MD_SZ,  // bufSize
    PAF_MAX_NUM_PRIVATE_MD, // NumBuf
    128,                    // alignment
    &gPafHeapIdExt          // pHeapIdMdbuf
};

// .............................................................................
//
// This structure defines the memory allocation of audio frame buffers (channel buffers). Users can customize this structure based
// on their requirement. IALG_SARAM specifies the audio frame buffer is allocated in IRAM and IALG_EXTERNAL
// specifies the audio frame buffer is allocated in SDRAM. By allocating few channel buffers in SDRAM, users can save internal memory
// but trading performance
// This structure defined for PAF_MAXNUMCHAN number of channels. But channel buffers will be allocated  for "PAF_AST_params_numchan"
// number of channels only.
//

const PAF_AudioFrameBufStatus    PAF_AST_params_AudioFrameBufStatus[PAF_MAXNUMCHAN] =
{
    IALG_SARAM, // 0
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM, // 16
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM,
    IALG_SARAM, // 31
    //IALG_EXTERNAL, // 16
    //IALG_EXTERNAL,
    //IALG_EXTERNAL,
    //IALG_EXTERNAL,
    //IALG_EXTERNAL,
    //IALG_EXTERNAL,
    //IALG_EXTERNAL,
    //IALG_EXTERNAL,
    //IALG_EXTERNAL,
    //IALG_EXTERNAL,
    //IALG_EXTERNAL,
    //IALG_EXTERNAL,
    //IALG_EXTERNAL,
    //IALG_EXTERNAL,
    //IALG_EXTERNAL,
    //IALG_EXTERNAL // 31
};

const PAF_AST_DecOpCircBufStatus PAF_AST_params_decOpCircBufStatus_primary =
{
    sizeof(PAF_AST_DecOpCircBufStatus),
    PAF_ASOT_FRAMELENGTH
};

const PAF_AST_DecOpCircBufStatus *const PAF_AST_params_decOpCircBufStatus[] =
{
    &PAF_AST_params_decOpCircBufStatus_primary,
};


// Re-visit, remove unused members
// .............................................................................
//
// Audio Stream Parameter Definitions
//
//   Global definition in RAM provides standard & non-standard functionality.
//

const PAF_ASIT_Params asip_params_PAi =
{
    &PAF_ASIT_params_fxns,                                  // fxns
    {                                                       // zone
        0,                                                  // zone.master
        1,                                                  // zone.inputs
        0,                                                  // zone.input1
        1,                                                  // zone.inputN
        1,                                                  // zone.decodes
        0,                                                  // zone.decode1
        1,                                                  // zone.decodeN
        1,                                                  // zone.streams
        0,                                                  // zone.stream1 /* unused */
        1,                                                  // zone.streamN /* unused */
        1,                                                  // zone.encodes
        0,                                                  // zone.encode1
        1,                                                  // zone.encodeN
        1,                                                  // zone.outputs
        0,                                                  // zone.output1
        1,                                                  // zone.outputN
    },
    PAF_AST_inputsFromDecodes_std,
    PAF_AST_outputsFromEncodes_std,
    {                                                       // heap
        &gPafHeapIdInt,                                     // heap.pHeapIdIntern
        &gPafHeapIdExt,                                     // heap.pHeapIdExtern
        &gPafHeapIdInt,                                     // heap.pHeapIdInpbuf
        &gPafHeapIdInt,                                     // heap.pHeapIdOutbuf
        &gPafHeapIdInt,                                     // heap.pHeapIdFrmbuf
        &gPafHeapIdInt1,                                    // heap.pHeapIdIntern1
        &gPafHeapIdInt1Shm,                                 // heap.pHeapIdInt1Shm
        &gPafHeapIdExtShm,                                  // heap.pHeapIdExtShm
        &gPafHeapIdExtNonCachedShm,                         // heap.pHeapIdExtNonCachedShm
        1,                                                  // heap.clear
    },
    {                                                       // common
        params_memspace_PAi,                                // common.space
    },
    NULL,                                                   // z_rx_bufsiz
    NULL,                                                   // z_tx_bufsiz
    PAF_AST_params_numchan,                                 // z_numchan
    PAF_SYS_FRAMELENGTH,                                    // framelength
    &PAF_AST_params_audioFrameFunctions,                    // pAudioFrameFunctions
    &PAF_ASP_chainFxns,                                     // pChainFxns
    &PAF_AST_params_inpBufStatus,                           // pInpBufStatus
    NULL, //PAF_AST_params_decodeStatus,                            // z_pDecodeStatus
    &PAF_AST_params_outBufStatus,                           // pOutBufStatus
    PAF_AST_params_encodeStatus,                            // z_pEncodeStatus
    &PAF_AST_params_volumeStatus,                           // pVolumeStatus
    &PAF_ASP_params_decAlgKey,                              // pDecAlgKey
    &PAF_ASP_params_encAlgKey,                              // pEncAlgKey
    &PAF_ASP_params_decSioMap,                              // pDecSioMap
    PAF_AST_streamsFromDecodes_std,                         // streamsFromDecodes
    PAF_AST_streamsFromEncodes_std,                         // streamsFromEncodes
    PAF_MAX_SYS_FRAMELENGTH,                                // maxFramelength
    PAF_AST_streamOrder,                                    // streamOrder
    inpLinkInit,                                            // i_inpLinkInit
    outLinkInit,                                            // i_outLinkInit
    outNumBufMap,                                           // poutNumBufMap
    PAF_AST_params_MetadataBufStatus,                       // metadata buffer status
    PAF_AST_params_AudioFrameBufStatus,                     // Audio frame buffer status
    PAF_AST_params_decOpCircBufStatus                       // z_pDecOpCircBufStatus
};

extern const PAF_ASOT_Fxns PAF_ASOT_params_fxns;

// Re-visit, remove unused members
const PAF_ASOT_Params asop_params_PAi =
{
    &PAF_ASOT_params_fxns,                                  // fxns
    {                                                       // zone
        0,                                                  // zone.master
        1,                                                  // zone.inputs
        0,                                                  // zone.input1
        1,                                                  // zone.inputN
        1,                                                  // zone.decodes
        0,                                                  // zone.decode1
        1,                                                  // zone.decodeN
        1,                                                  // zone.streams
        0,                                                  // zone.stream1 /* unused */
        1,                                                  // zone.streamN /* unused */
        1,                                                  // zone.encodes
        0,                                                  // zone.encode1
        1,                                                  // zone.encodeN
        1,                                                  // zone.outputs
        0,                                                  // zone.output1
        1,                                                  // zone.outputN
    },
    PAF_AST_inputsFromDecodes_std,
    PAF_AST_outputsFromEncodes_std,
    {                                                       // heap
        &gPafHeapIdInt,                                     // heap.pHeapIdIntern
        &gPafHeapIdExt,                                     // heap.pHeapIdExtern
        &gPafHeapIdInt,                                     // heap.pHeapIdInpbuf
        &gPafHeapIdInt,                                     // heap.pHeapIdOutbuf
        &gPafHeapIdInt,                                     // heap.pHeapIdFrmbuf
        &gPafHeapIdInt1,                                    // heap.pHeapIdIntern1
        &gPafHeapIdInt1Shm,                                 // heap.pHeapIdInt1Shm
        &gPafHeapIdExtShm,                                  // heap.pHeapIdExtShm
        &gPafHeapIdExtNonCachedShm,                         // heap.pHeapIdExtNonCachedShm
        1,                                                  // heap.clear
    },
    {                                                       // common
        params_memspace_PAi,                                // common.space
    },
    NULL,                                                   // z_rx_bufsiz
    NULL,                                                   // z_tx_bufsiz
    PAF_AST_params_numchan,                                 // z_numchan
	PAF_ASOT_FRAMELENGTH,                                   // framelength
    &PAF_AST_params_audioFrameFunctions,                    // pAudioFrameFunctions
    &PAF_ASP_chainFxns,                                     // pChainFxns
    &PAF_AST_params_inpBufStatus,                           // pInpBufStatus
    NULL, //PAF_AST_params_decodeStatus,                            // z_pDecodeStatus
    &PAF_AST_params_outBufStatus,                           // pOutBufStatus
    PAF_AST_params_encodeStatus,                            // z_pEncodeStatus
    &PAF_AST_params_volumeStatus,                           // pVolumeStatus
    &PAF_ASP_params_decAlgKey,                              // pDecAlgKey
    &PAF_ASP_params_encAlgKey,                              // pEncAlgKey
    &PAF_ASP_params_decSioMap,                              // pDecSioMap
    PAF_AST_streamsFromDecodes_std,                         // streamsFromDecodes
    PAF_AST_streamsFromEncodes_std,                         // streamsFromEncodes
	PAF_ASOT_MAX_FRAMELENGTH,                               // maxFramelength
    PAF_AST_streamOrder,                                    // streamOrder
    inpLinkInit,                                            // i_inpLinkInit
    outLinkInit,                                            // i_outLinkInit
    outNumBufMap,                                           // poutNumBufMap
    PAF_AST_params_MetadataBufStatus,                       // metadata buffer status
    PAF_AST_params_AudioFrameBufStatus                      // Audio frame buffer status
};


// EOF
