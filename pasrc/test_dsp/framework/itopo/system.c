
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// Contains definitions for System Stream parameters, status, and configuration.
//

#include <xdc/std.h>

#include <paftyp.h>
#include <pafsys_a.h> 
#include "noasp.h"
#include "systemStream.h"

#include "pfp/pfp.h"
#include "pfp_app.h"        /* contains all PFP ID's */

// System Stream parameters, primary functions
const struct {
    PAF_SST_FxnsMain *main;
    Int count;
    struct {
        PAF_SST_FxnsCompute  *compute;
        PAF_SST_FxnsTransmit *transmit;
    } sub[5];
} systemStreamPrimaryFxns =
{
    &systemStreamMain,
    5,
    {
        { &systemStream1Compute, &systemStream1Transmit, },
#ifndef NOBM
        { &systemStream2Compute, &systemStream2Transmit, },
#else
        { NULL, NULL, },
#endif        
#ifndef NODEM
        { &systemStream3Compute, &systemStream3Transmit, },
#else
        { NULL, NULL, },
#endif        
#ifdef THX
        { &systemStream4Compute, &systemStream4Transmit, },
#else
        { NULL, NULL, },
#endif
        { &systemStream5Compute, &systemStream5Transmit, },
    },
};

// System Stream paramters
const PAF_SST_Params systemStreamParams_PA[1] =
{
    {
        1, 0, 1, 1,                                     // streams, stream1, streamN, ss    
        (const PAF_SST_Fxns *)&systemStreamPrimaryFxns, // fxns
    },
};

// System Stream (SYS) status
PAF_SystemStatus systemStreamStatus[1] = {
    {
        sizeof (PAF_SystemStatus),
        PAF_SYS_MODE_ALL,               // mode
        0,                              // listeningMode
        PAF_SYS_RECREATIONMODE_AUTO,    // recreationMode
        2 + PAF_SYS_SPEAKERSIZE_SMALL,  // speakerMain
        1 + PAF_SYS_SPEAKERSIZE_SMALL,  // speakerCntr
        2 + PAF_SYS_SPEAKERSIZE_SMALL,  // speakerSurr
        2 + PAF_SYS_SPEAKERSIZE_SMALL,  // speakerBack
        1 + PAF_SYS_SPEAKERSIZE_BASS,   // speakerSubw
        PAF_SYS_CCRTYPE_STANDARD,       // channelConfigurationRequestType
        0, 0, 0,                        // switchImage, imageNum, imageNumMax
        0,                              // Unused
        0, 0,                           // cpuLoad, peakCpuLoad
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerWide
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerHead
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerTopfront
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerToprear
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerTopmiddle
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerFrontheight
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerRearheight
        0,                              // unused2
        PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,  // channelConfigurationRequest
#ifdef FULL_SPEAKER // paftyp.h
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerScreen
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerSurr1
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerSurr2
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerRearSurr1
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerRearSurr2
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerCntrSurr
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerLRCntr
        0 + PAF_SYS_SPEAKERSIZE_NONE,   // speakerLRCntrSurr
#else
        0, 0, 0, 0, 0, 0, 0, 0          // unused3
#endif
        0, 0,                           // asitLoad, peakAsitLoad
        0, 0,                           // asotLoad, peakAsotLoad
        0, 0,                           // aipLoad,  peakAipLoad
        0, 0,                           // afpLoad,  peakAfpLoad
        0, 0,                           // ssLoad,   peakSsLoad   
        0, 0,                           // unused4
        0,                              // pfpDisableMask
        0,                              // pfpEnableMask
        PFP_ENABLE_MASK,                // pfpEnabledBf
        0,                              // pfpLatchStatsMask
        0,                              // pfpResetStatsMask
        0,                              // pfpAlphaUpdateMask
        -1.0,                           // pfpAlphaUpdate
    },
};

// System Stream configuration
PAF_SST_Config systemStreamConfig[1] =
{
    {
        0,                              // firstTimeInit
        NULL,                           // acp
        &systemStreamStatus[0],         // pStatus
    },
};
