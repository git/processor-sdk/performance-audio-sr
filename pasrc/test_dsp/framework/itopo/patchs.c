
/*
Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//
//
// Framework Declarations
//

#include <audioStreamProc_patchs.h>
#include <asp0.h>
#include <asp1.h>
#include "audioStreamInpProc.h"
#include "audioStreamOutProc.h"

#include "dbgBenchmark.h" // PCM high-sampling rate + SRC + CAR benchmarking

#ifdef DTS_BUILD
//
// DTS build
//
#define _DTS_
#else
#undef  _DTS_
#endif // DTS_BUILD
   
#ifdef DH_BUILD
//
// DH build
//
#define _SRC4_
#define _CAR_
#define _OAR_
#define _BMDA_

#ifdef _ENABLE_BENCHMARK_PCMHSR_SRC_CAR_
//
// Performing PCM high-sampling rate + SRC + CAR benchmarking.
// This is DH build, but with OAR removed.
#undef  _OAR_
#endif // _ENABLE_BENCHMARK_PCMHSR_SRC_CAR_

#else
#undef  _SRC4_
#undef  _CAR_
#undef  _OAR_
#undef  _BMDA_    
#endif // DH_BUILD


//#define _AE0_
//#define _ML0_


//
// Decoder Definitions
//

#include <pcm.h>
#include <pcm_mds.h>


//
// Audio Stream Processing Declarations & Definitions
//

#include <aspstd.h>

#ifdef _AE0_
#include <ae.h>
#include <ae_mds.h>
#endif

#ifdef _SRC4_
#include <src.h>
#include <src_tih.h>
#define SUC_TIH_init SRC_TIH_init
#define SUC_TIH_ISUC SRC_TIH_ISRC
#endif

#ifdef _CAR_
#include <car.h>
#include <car_tig.h>
#endif

#ifdef _OAR_
#include <oar.h>
#include <oar_tig.h>
#endif

#ifdef _BMDA_
#include <bmda.h>
#include <bmda_tig.h>
#endif

#ifdef _ML0_
#include <ml.h>
#include <ml_mds.h>
#endif

#ifdef _DTS_
#include <dtsuhdb.h>
#include <dtsuhdb_mds.h>
#endif

const PAF_ASP_LinkInit aspLinkInitAllI13[] =
{
#ifdef _AE0_
    PAF_ASP_LINKINIT(CUS,AE,MDS), 
#endif

#ifdef _ML0_
    PAF_ASP_LINKINIT(STD, ML, MDS), 
#endif

#ifdef _SRC4_
//    PAF_ASP_LINKINIT(STD, SRC, MDS),
    PAF_ASP_LINKINITPARAMS(STD, SRC, TIH, &ISRC_PARAMS_DS_8CH_HBW),
#endif

#ifdef _CAR_
    PAF_ASP_LINKINIT(STD, CAR, TIG),
#endif

#ifdef _OAR_
    PAF_ASP_LINKINIT(STD, OAR, TIG),
#endif

#ifdef _BMDA_
    PAF_ASP_LINKINIT(STD, BMDA, TIG),
#endif

#ifdef _DTS_
    PAF_ASP_LINKINIT(STD, DTSUHDB, MDS),
#endif
    PAF_ASP_LINKNONE,
};

const PAF_ASP_LinkInit aspLinkInitNilI13[] =
{
    PAF_ASP_LINKNONE,
};

const PAF_ASP_LinkInit aspLinkInitStdI13[] =
{
#ifdef _AE0_
    PAF_ASP_LINKINIT(CUS,AE,MDS), 
#endif

#ifdef _ML0_
    PAF_ASP_LINKINIT(STD, ML, MDS), 
#endif

#ifdef _SRC4_
//    PAF_ASP_LINKINIT(STD, SRC, MDS),
    PAF_ASP_LINKINITPARAMS(STD, SRC, TIH, &ISRC_PARAMS_DS_8CH_HBW),
#endif

#ifdef _CAR_
    PAF_ASP_LINKINIT(STD, CAR, TIG),
#endif

#ifdef _OAR_
    PAF_ASP_LINKINIT(STD, OAR, TIG),
#endif

#ifdef _BMDA_
    PAF_ASP_LINKINIT(STD, BMDA, TIG),
#endif

#ifdef _DTS_
    PAF_ASP_LINKINIT(STD, DTSUHDB, MDS),
#endif
    PAF_ASP_LINKNONE,
};

const PAF_ASP_LinkInit aspLinkInitCusI13[] =
{
    PAF_ASP_LINKNONE,
};

const PAF_ASP_LinkInit *const patchs_aspLinkInit[1][GEARS] =
{
    {
        aspLinkInitAllI13,
        aspLinkInitNilI13,
        aspLinkInitStdI13,
        aspLinkInitCusI13,
    },
};

//
// Encoder Definitions
//

#include <pce.h>
#include <pce_tii.h>
extern const IPCE_Params IPCE_PARAMS_NODELAY;

const PAF_ASP_LinkInit encLinkInitI13[] =
{
    PAF_ASP_LINKINITPARAMS (STD, PCE, TII, &IPCE_PARAMS_NODELAY),
    PAF_ASP_LINKNONE,
};


const PAF_ASP_LinkInit *const patchs_encLinkInit[] =
{
    encLinkInitI13,
};

//
// Audio Stream Patch Definition
//

extern const PAF_SIO_ParamsN patchs_devinp[];
extern const PAF_SIO_ParamsN patchs_devout[];

const PAF_ASIT_Patchs asip_patchs_PAi =
{
    patchs_devinp,
};

const PAF_ASOT_Patchs asop_patchs_PAi =
{
    patchs_devout,
    patchs_aspLinkInit,
    patchs_encLinkInit,
};

// EOF
