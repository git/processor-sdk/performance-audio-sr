/******************************************************************************
 * FILE PURPOSE: Package specification file for the Performance Audio SDK
 ******************************************************************************
 * FILE NAME: package.xdc
 *
 * DESCRIPTION: 
 *  This file contains the package specification for the PASDK
 *
 * Copyright (C) 2012-2018, Texas Instruments, Inc.
 *****************************************************************************/

package ti.pasdk[1, 0, 0, 0] {
    module Settings;
}

