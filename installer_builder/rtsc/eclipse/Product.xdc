/* --COPYRIGHT--,BSD
 * Copyright (c) $(CPYYEAR), Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/


import xdc.tools.product.IProduct;                          

metaonly module Product inherits IProduct {
    
    override config String name = "Processor SDK Audio";

    override config String id   = "ti.pasdk";

    override config IProduct.UrlDescriptor updateSite = {
        url:"http://software-dl.ti.com/dsps/dsps_public_sw/sdo_ccstudio/TargetContent/Updates/full/site.xml",
        text:"Target Content Updates"
    };

    override config String version = "1.3.0.00";  

    override config String companyName = "Texas Instruments Inc.";
    
    override config String copyRightNotice = "Copyright Texas Instruments 2018";

    override config IProduct.UrlDescriptor productDescriptor = {
        url: "http://www.ti.com",
        text: "PASDK"
    };

    override config IProduct.UrlDescriptor licenseDescriptor = {
        text: "Texas Instruments Incorporated - Technology Software Publicly Available license"
    };

    override config String repositoryArr[] = ["packages"];

    override config String docsLocArr[] = [
        "docs/doxygen/html"
    ];

    override config String bundleName = "pasdk";
}
