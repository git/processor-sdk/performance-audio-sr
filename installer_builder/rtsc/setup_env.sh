#!/bin/bash

ti_tools_path="/home/uda0869574local/ti"
pademo_path="/home/uda0869574local/proclibs/prsdk-pa-demo"

# Environment variables for PAF library make files
export gnu_targets_arm_A15F="${ti_tools_path}/gcc-arm-none-eabi-6-2017-q1-update"
export ti_targets_elf_C66="${ti_tools_path}/ti-cgt-c6000_8.2.2"
export BIOS_INSTALL_DIR="${ti_tools_path}/bios_6_52_00_12"
export XDC_INSTALL_DIR="${ti_tools_path}/xdctools_3_50_03_33_core"
export UIA_INSTALL_DIR="${ti_tools_path}/uia_2_21_02_07"
export IPC_INSTALL_DIR="${ti_tools_path}/ipc_3_47_02_00"
export PDK_INSTALL_DIR="${ti_tools_path}/pdk_k2g_1_0_10"
export EDMA3_INSTALL_DIR="${ti_tools_path}/edma3_lld_2_12_05_30C"
export XDAIS_INSTALL_DIR="${ti_tools_path}/xdais_7_24_00_04"
export DSPLIB_INSTALL_DIR="${ti_tools_path}/dsplib_c66x_3_4_0_0"

export PAROOT="${pademo_path}"

# Environment variables for PASDK make files
export CG_TOOLS_c66x=${ti_targets_elf_C66}
export CG_TOOLS_a15=${gnu_targets_arm_A15F}
export BIOSROOT=${BIOS_INSTALL_DIR}
export XDCROOT=${XDC_INSTALL_DIR}
export IPCROOT=${IPC_INSTALL_DIR}
export PDKROOT=${PDK_INSTALL_DIR}
export EDMA3LLDROOT=${EDMA3_INSTALL_DIR}
export XDAISROOT=${XDAIS_INSTALL_DIR}
export DSPLIBROOT=${DSPLIB_INSTALL_DIR}

export ROOTDIR="${pademo_path}/pasrc/paf"

export PATH=${XDCROOT}:$PATH
